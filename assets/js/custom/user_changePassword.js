$(document).ready(function(){
	$('.changePasswordForm').validate({
		onfocusout: function(e) {
			this.element(e);						  
		},
		onkeyup: false,
	  	rules: {
	    	current_password: {
	      		required: true
    		},
    		new_password: {
	      		required: true
    		},
    		confirm_password: {
	      		required: true,
	      		equalTo : "#new_password"
    		}
	  	},
      	messages:{
			current_password:{
				remote:"Enter current password."
			},
			new_password:{
				remote:"Enter new password."
			},
			confirm_password:{
			required:"This field is required.",
			equalTo:"New password and confirm password must be same."
			}  
		  },
		errorElement : 'div',
		errorPlacement: function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}
	});

})


