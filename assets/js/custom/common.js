$(document).ready(function(){

	$(document).ajaxStart(function() {
    $.blockUI({
        css: {
              border: 'none',
              padding: '15px',
              backgroundColor: '#000',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .5,
              color: '#fff'
            }
      	});
    });
    $(document).ajaxStop($.unblockUI);
});