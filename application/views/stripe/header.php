<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="Petcommander,Petcommander user">
    <meta name="author" content="PIXINVENT">
    <title>Petcommander</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/ico/icon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/fonts/simple-line-icons/style.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/custom/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/pages/data-tables.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.4/cropper.min.css">
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery-lib/2.0.3/jquery.min.js"></script>
    <!-- END: Custom CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/plugins/forms/wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/skins.css">
    <!-- END: Page CSS-->

    <style>
        .horizontal-menu .navbar-light ul#main-menu-navigation > li.active > a {
            border-bottom: 1px solid #605ca8;
            background: #fff;
        }
        .alert-success{
            color:green;
            background-color: white !important;
            text-align: center;
        }
        .alert-danger{
            color:red;
            background-color: white !important;
            text-align: center;
        }
        .error{
            color:red;
        }
        .bg-header{
            background-color: #605ca8 !important; 
            background-image: -webkit-linear-gradient(left, #605ca8 0%, #2574ab 100%);
            box-shadow: 0px 1px 20px 1px rgba(213, 215, 249, 0.6);
        }
        .bg-primary{
            background-color: #605ca8 !important; 
            box-shadow: 0px 1px 20px 1px rgba(213, 215, 249, 0.6);
        }
        .navbar.navbar-brand-center .navbar-header {
            width: 192px;
            position: absolute;
            top: 0;
            left: 125px !important;
            padding: 0;
            margin: 0;
            transform: translate(-50%, 0);
            z-index: 999;
        }
        .form-control:focus {
            color: #4e5154;
            background-color: #fff;
            border-color: #605ca8 !important; 
            outline: 0;
            box-shadow: none;
        }
    </style>

    <script>
      var BASE_URL = '<?php echo base_url();?>';

      var flashdata ='';
      <?php if($this->session->flashdata('success')): ?>
        var flashdata = "<?php echo $this->session->flashdata('success'); ?>";
      <?php elseif($this->session->flashdata('error')): ?>
        var flashdata = "<?php echo $this->session->flashdata('error'); ?>";
      <?php endif; ?>

    </script>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- BEGIN: Header  skin-purple-->
    <nav class="top-header header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-brand-center navbar-dark navbar-light bg-header shadow">
        <div class="navbar-wrapper shadow">
            <div class="navbar-header text-left">
                <ul class="nav navbar-nav flex-row flex-row-reverse">
                    <li class="nav-item">
                         <?php   if(isset($shopname) && !empty($shopname)){ ?>
                            <a class="navbar-brand" href="<?php echo base_url()?>user/login">
                                <h3 class="brand-text text-left"> <?php echo $shopname; ?> </h3>
                            </a>
                        <?php } ?>
                    </li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <!-- <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class=""></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700"><?php echo (isset($user_detail['username']) && !empty($user_detail['username']))? ucfirst($user_detail['username']):'';?></span><span class="avatar avatar-online">
                            <img src="<?php echo base_url(); ?>assets/images/avatars/holder-large.png" alt="">
                            </span></a>
                            <div class="dropdown-menu dropdown-menu-right ">
                                <a class="dropdown-item" href="<?php echo base_url() ?>user/login"><i class="ft-unlock"></i> Login</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> -->
        </div>
    </nav>
    <!-- END: Header-->