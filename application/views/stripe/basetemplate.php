<?php

    if(isset($header) && $header){
        $this->load->view('stripe/header');
    }

    if(isset($sidebar) && $sidebar){
        $this->load->view('stripe/sidebar');
    }

    if(isset($_view)){
        $this->load->view('stripe/'.$_view);
    }

    if(isset($footer) && $footer){
        $this->load->view('stripe/footer');
    }
    
 ?>