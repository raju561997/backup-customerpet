<!DOCTYPE html>
<html lang="en">
<head>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="Petcommander,Petcommander user">
    <meta name="author" content="PIXINVENT">
    <title>Petcommander</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/ico/icon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/components.css">
    <!-- END: Theme CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/fonts/simple-line-icons/style.css">
    <!-- END: Page CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/custom/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/pages/data-tables.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.4/cropper.min.css">
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery-lib/2.0.3/jquery.min.js"></script>
    <!-- END: Custom CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/plugins/forms/wizard.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ;?>assets/css/skins.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
    <!-- END: Page CSS-->
</head>

</head>
<body>
    <div class="main">
        <div class="main-child">
            <div class=" py-3 container">
                <div class="shop-brand">
                     <img class="d-block mx-auto   rounded-circle" src="http://lh3.googleusercontent.com/YPm3QxIMlU2DyCbQ0R7yzalV4xrHW6FtmZM4ebChE69i_Dk68TD0RLDF38qTIPgRj6Ds9uOLa-wKfd81wiKY_Cx_MMC-yzUsgAkt-A=s100" alt="" width="100">
                    <h4 class="font-weight-bold shopBrandName text-center">PET SHOP</h4>
                    <p class="text-center shopslogan">COMPANY SLOGAN</p> 
                </div>
                <h2 class="pb-md-1 payDetail text-center">YOU  ARE ABOUT TO PAY <span class="payamount text-danger">$40.60</span>  FOR THE FOLLOWING APPOINMENT:</h2>
                <div class="lead  mx-auto text-black payment-sub-heading">
                    <p class="m-0 dayCareTxt">Daycare for Bruno on 25/01/2020,Grooming for Bruno on 25/01/2020,Boarding for from 22/01/2020 to 25/01/2020,Grooming for Tommy on 25/01/2020</p>
                </div>

            </div>
        </div>

        <div class="main-child-payment">
            <form action="">
                <div class="main-row mx-auto">
                    <div class="p-2 sub-card">
                        <div class="custom-control custom-radio  mx-auto">
                            <input type="radio" id="customRadio1" value="visa" name="payment_type" class="custom-control-input mr-2">
                            <label class="custom-control-label" for="customRadio1">American Express ending with 4343</label>
                        </div>
                    </div>
                </div>
                <div class="main-row mx-auto">
                    <div class="p-2 sub-card">
                        <div class="custom-control custom-radio  mx-auto">
                            <input type="radio" id="customRadio2" value="mastercard" name="payment_type" class="custom-control-input mr-2">
                            <label class="custom-control-label" for="customRadio2">Mastercard ending with 6767</label>
                        </div>
                    </div>
                </div>
                <div class="main-row mx-auto">
                    <div class="p-2 sub-card">
                        <div class="custom-control custom-radio  mx-auto">
                            <input type="radio" id="customRadio3" name="payment_type" value="new" class="custom-control-input mr-2">
                            <label class="custom-control-label" for="customRadio3">New Card</label>
                        </div>
                        <div class="row col-md-10 pl-md-2">
                            <div class="pl-md-1 pt-1 col-md-12">
                                <input type="text" placeholder="Credit Card Number" class="w-100  inputClass">
                            </div>
                            <div class=" pt-1 col-md-6">
                                <input type="text" placeholder="CVV Number" class="w-100  inputClass">
                            </div>
                            <div class=" pt-1 col-md-6">
                                <input type="text" placeholder="Expiry Date" class="w-100  inputClass">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-row mx-auto pb-3">
                    <div class="sub-card submit-pay-option">
                        <div class="form-check ">
                            <input class="form-check-input creditCheck" type="checkbox" value="" id="flexCheckChecked" >
                            <label class="form-check-label creditLabel" for="flexCheckChecked">
                                Use $20 from wallet balance
                            </label>
                        </div>
                        <div class="pt-1">
                            <button type="submit" id="paybtn"  class="btn btn-success text-black">PAY $20 NOW</button>
                        </div>
                    </div>
                </div>
        </form>
        </div>
            <footer class="footer footer-static footer-light navbar-shadow">
             <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-center d-block d-md-inline-block">&copy;   <?php echo date('Y');?></span><a href="javascript:void(0)"> Griffin Apps</a> All rights reserved.</p>
          </footer>
    </div>
</body>
</html>
<style>
        :root {
        --bodyColor:white;    
        --blackFont: #111;
        --placeColor:gray;
     }

     body{
         background-color: var(--bodyColor) !important;
         width: 100%;
         height: 100vh;
         font-family: 'Roboto', sans-serif;
     }


     .shopBrandName{
         color:var(--blackFont);
         font-weight: 700;
         margin-bottom: 2px;
         
     }

     .shop-brand img{
        border: 4px solid black;
        margin-bottom: 5px;
     }
     .shopslogan{
         font-size: 7px;
         color: var(--blackFont);
     }

    .payDetail{
        font-weight: 600;
        font-size: 1.6rem; /* 25px */
        color: var(--blackFont);
       padding-top: 20px;
    }
    .payment-sub-heading{
        width: 64%;
        text-align: center;
        color: var(--blackFont);
        font-size: 16px;
        font-weight: 500;
    }
    .sub-card{
        width: 54rem;
        margin:0 auto;
        padding: 20px;
    }
    .main-row{
        border-bottom:1px solid black;
        display: flex;
        justify-content: center;
    }
    .main-row.active-row{
        background-color: rgba(0,0,0,.05);
    }
    .main-row:first-child{
        border-top:1px solid black;
    }

    .main-row:last-child{
        border-bottom:none;
    }

    .sub-card .custom-control-input:checked ~ .custom-control-label::before {
       color: #fff;
       border-color: #111;
       background-color: #111;
    }

    .sub-card .custom-control-label::before {
        background-color: #fff;
        border: #111 solid 2px;
    }

    
    .sub-card label{ 
         color: var(--blackFont);
        font-weight: 600;
        font-size: 16px;
        padding-left: 7px;
    }

    .text-danger{
         color: #c70039 !important;
    }

    .sub-card  .custom-radio .custom-control-input:checked ~ .custom-control-label::after {
       background-image: none;
    } 

    .inputClass{
         border: none;
        border-bottom: 1px solid #aba6a6;
        padding: 15px;
    }  
    .inputClass:focus {
          outline: none !important;
          border-bottom: 2px solid #111;
         /* box-shadow: 0 0 10px #719ECE; */
        }  


    .inputClass::-webkit-input-placeholder { /* Edge */
         color: var(--placeColor);
        font-weight: 600;
        font-size:15px;
    }

    .inputClass:-ms-input-placeholder { /* Internet Explorer */
        color: var(--placeColor);
        font-weight: 600;
        font-size:15px;
    }

    .inputClass::placeholder {
         color: var(--placeColor);
         font-weight: 600;
        font-size:15px;
    }

    .creditLabel{
        font-weight: 600;
        color:  var(--blackFont);
        font-size: 14px !important;
    }
    .submit-pay-option label{
        padding-left: 0px !important;
    }

    #paybtn{
        color:var(--blackFont);
        font-weight: 600;
    }

    @media only screen and (max-width: 600px) {
        .payment-sub-heading {
            width: 100%;
        }   
    }

</style>

<script>
    $(document).ready(function () {
        $(document).on('click',"input[name='payment_type']", function () {
            if($('.main-row').hasClass('active-row')){
                $('.main-row').removeClass('active-row');
            }
            $(document).find()
            $(this).parent().parent().parent().addClass('active-row');
            console.log('its working');
        });
    });



</script>