<script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" scr="https://momentjs.com/downloads/moment-timezone.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
<link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">
<style>
.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
    color: #ccc ;
}
.checkboxGroup .help-block{
    color:#dd4b39;
}
.form-group .help-block{
    color:#dd4b39;
}

.addCustomer
{
    margin: 5px 5px;
}
 .error{
   color: #ff4081;
   
   }
.addPet 
{
    margin: 5px 5px;
}
.select2-selection--single{
  color: #646464 ! important;
  border: 1px solid #e6e6e6 ! important;
  height: 34px ! important;
}
.select2-selection__rendered{
  color: #646464 ! important;
  padding-left: 2px;
}
.select2-search__field{
  color: #646464 ! important;
  border: 1px solid #e6e6e6 ! important;
}
.select2-container{
   width: 100% !important;
}
#mix_breed{
    height: 16px;
    width: 16px;
}


.card{
    padding: 5px;
    border: 1px solid #dfe1e6;;
    /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
}

.petLabel{
    background-color: white;
    color: black;
    position: absolute;
    top: -18px;
    left: 85px;
    font-size: 14px;
}
.petLogo{
    position: absolute;
    top: -40px;
    left: 15px;
    font-size: 14px;
    width: 70px;
    height: 70px;
    border-radius: 50%;
}
 .checkboxspan{
   
   display: unset !important;
   font-size: large  !important;
  }
.close-icon
{
    position: absolute;
    top: -15px;
    right: 0px;
    display:block;
    box-sizing:border-box;
    width:20px;
    height:20px;
    border-width:3px;
    border-style: solid;
    border-color:red;
    border-radius:100%;
    background: -webkit-linear-gradient(-45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%), -webkit-linear-gradient(45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%);
    background-color:red;
    box-shadow:0px 0px 2px 1px rgba(0,0,0,0.5);
    transition: all 0.3s ease;
}
.cardCustomeError{
    margin:0px;
    padding: 0px;
}

@keyframes shadow-pulse
{
  0% {
    box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
  }
  100% {
    box-shadow: 0 0 0 15px rgba(0, 0, 0, 0);
  }
}

@keyframes shadow-pulse-big
{
  0% {
    box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.1);
  }
  100% {
    box-shadow: 0 0 0 30px rgba(0, 0, 0, 0);
  }
}

.flash-button
{
  animation: shadow-pulse 1s infinite;
}
.backbtn{
 border-color: #e6dada !important;
}

/*@media screen and (max-width: 600px) {
 .petCard {
   margin-bottom: 40px !important;
  }
}*/

</style>
<?php 
    if (isset($_GET['id'])) {
        $id = get_decode($_GET['id']);
        $data['pet_id'] = $id;

        $pet = $this->db2->get_where('pets',array('id'=>$id))->row();
        $petDetails = $pet;
        if(!empty($petDetails->pet_cover_photo)) {
            $petDetails->pet_cover_photo = loadCloudImage(PETS_IMAGES.$petDetails->pet_cover_photo);
        }else{
            $petDetails->pet_cover_photo = base_url().'assets/default-p.png"';
        }
    
        $app_user_id = $pet->app_user_id;
        $app_users_details = $this->db2->get_where('app_users',array('id'=>$app_user_id))->row_array();
        $allpets = $this->db2->get_where('pets',array('app_user_id'=>$app_user_id,'is_deceased'=>0,'is_deleted'=>0))->result_array();
    }
    if(isset($_GET['date']) && !empty($_GET['date'])){
        $temparr = explode("T", $_GET['date']);
        $appointment_date = $temparr[0];
        $appointment_time = $temparr[1];
        $appointment_time = date("h:i A", strtotime($appointment_time));

    }
?>
<style>
   .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
   color: #ccc ;
   }
   .checkboxGroup .help-block{
   color:#dd4b39;
   }
   .form-group .help-block{
   color:#dd4b39;
   }
   .addCustomer
   {
   margin: 5px 5px;
   }
   .addPet 
   {
   margin: 5px 5px;
   }
   .select2-selection--single{
   color: #646464 ! important;
   border: 1px solid #e6e6e6 ! important;
   height: 34px ! important;
   }
   .select2-selection__rendered{
   color: #646464 ! important;
   padding-left: 2px;
   }
   .select2-search__field{
   color: #646464 ! important;
   border: 1px solid #e6e6e6 ! important;
   }
   .select2-container{
   width: 100% !important;
   }
   #mix_breed{
   height: 16px;
   width: 16px;
   }
   .card{
   padding: 5px;
   border: 1px solid #dfe1e6;;
   /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
   }
   .petLabel{
   background-color: white;
   color: black;
   position: absolute;
   top: -18px;
   left: 85px;
   font-size: 14px;
   }
   .petLogo{
   position: absolute;
   top: -40px;
   left: 15px;
   font-size: 14px;
   width: 70px;
   height: 70px;
   border-radius: 50%;
   }
   .close-icon
   {
   position: absolute;
   top: -15px;
   right: 0px;
   display:block;
   box-sizing:border-box;
   width:20px;
   height:20px;
   border-width:3px;
   border-style: solid;
   border-color:red;
   border-radius:100%;
   background: -webkit-linear-gradient(-45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%), -webkit-linear-gradient(45deg, transparent 0%, transparent 46%, white 46%,  white 56%,transparent 56%, transparent 100%);
   background-color:red;
   box-shadow:0px 0px 2px 1px rgba(0,0,0,0.5);
   transition: all 0.3s ease;
   }
   .cardCustomeError{
   margin:0px;
   padding: 0px;
   }
   @keyframes shadow-pulse
   {
   0% {
   box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
   }
   100% {
   box-shadow: 0 0 0 15px rgba(0, 0, 0, 0);
   }
   }
   @keyframes shadow-pulse-big
   {
   0% {
   box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.1);
   }
   100% {
   box-shadow: 0 0 0 30px rgba(0, 0, 0, 0);
   }
   }
   .flash-button
   {
   animation: shadow-pulse 1s infinite;
   }
   .backbtn{
   border-color: #e6dada !important;
   }
/*   @media screen and (max-width: 600px) {
   .petCard {
   margin-bottom: 40px !important;
   }
   }*/
</style>
<?php 
   if (isset($_GET['id'])) {
       $id = get_decode($_GET['id']);
       $data['pet_id'] = $id;
   
       $pet = $this->db2->get_where('pets',array('id'=>$id))->row();
       $petDetails = $pet;
       if(!empty($petDetails->pet_cover_photo)) {
           $petDetails->pet_cover_photo = loadCloudImage(PETS_IMAGES.$petDetails->pet_cover_photo);
       }else{
           $petDetails->pet_cover_photo = base_url().'assets/default-p.png"';
       }
   
       $app_user_id = $pet->app_user_id;
       $app_users_details = $this->db2->get_where('app_users',array('id'=>$app_user_id))->row_array();
       $allpets = $this->db2->get_where('pets',array('app_user_id'=>$app_user_id,'is_deceased'=>0,'is_deleted'=>0))->result_array();
   }
   if(isset($_GET['date']) && !empty($_GET['date'])){
       $temparr = explode("T", $_GET['date']);
       $appointment_date = $temparr[0];
       $appointment_time = $temparr[1];
       $appointment_time = date("h:i A", strtotime($appointment_time));
   
   }
   ?>
<div id="main">
   <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="breadcrumbs-dark pb-0 pt-1" id="breadcrumbs-wrapper">
         <div class="container">
            <div class="row">
               <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0"><?= lang($pageViewTitle) ?></h5>
                  <ol class="breadcrumbs mb-0">
                     <li class="breadcrumb-item"><a href="<?php echo base_url() ;?>user/dashboard">Home</a>
                     </li>
                     <li class="breadcrumb-item"><a href="<?php echo base_url() ;?>user/getAllAppointments"> Appointments</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <div class="section section-data-tables">
               <div class="row">
                  <div class="col s12">
                     <div id="html-validations" class="card card-tabs">
                        <div class="card-content">
                           <div class="card-title">
                              <div class="row">
                                 <div class="col s12 m6 l10">
                                    <!-- <h4 class="card-title">Add Pet</h4> -->
                                 </div>
                                 <div class="col s12 m6 l2">
                                 </div>
                              </div>
                           </div>
                           <div id="html-view-validations">
                              <?php echo message_box('success');?>
                              <?php echo message_box('error'); ?>
                              <?php echo form_open('User/addNewAppointment', array('class' => 'form-horizontal','id' => 'addNewReservationForm','enctype' => 'multipart/form-data')) ?>
                              <div class="panel_controls">
                                 <input id="app_user_id" name="app_user_id" class="form-control" value="<?php echo $this->session->userdata('id'); ?>" type="hidden" >
                                 
                                 <?php if(!isset($petDetails)):?>
                                 <div class="form-group margin col s12 m12">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    <?= lang('pet') ?>
                                    <span class="required" aria-required="true"></span></span>
                                    <div class="col-sm-5">
                                       <div class="col-sm-5 petsDropDown col s12 m8">
                                         <select class="form-control " name="pet_id[]" id="pet_id" >
                                         <?php echo $petDetails['name']; ?>
                                       </select>
                                       </div>
                                       <div class="col-sm-3  col s12 m3">
                                          <a href="#addPet" class="btn btn-small btn-primary addPet" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Quick Add</a> 
                                       </div>
                                    </div>
                                 </div>
                                 <?php else:?>
                                 <div class="form-group margin col s12 m12">
                                   <span for="field-1" class="col-sm-3 control-label">
                                    <?= lang('pet') ?>
                                    <span class="required" aria-required="true"></span></span>
                                    <div class="col-sm-5">
                                       <div class="col-sm-5 petsDropDown col s12 m12">
                                         <select class="form-control mdb-select " name="pet_id[]" id="pet_id" multiple="multiple" >
                                          <?php foreach ($allpets as $key => $value) { ?>
                                          <option value="<?php echo $value['id'];?>" data-size="<?php echo $value['size'];?>" data-encodedidhref="<?php echo base_url('User/getPetDetails/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($value['id']))); ?>" data-name="<?php echo $value['name'];?>" data-puppypower="<?php echo $value['puppy_power'];?>" data-type="<?php echo $value['type'];?>" <?php if(!empty($value['pet_cover_photo'])) {$value['pet_cover_photo'] = loadCloudImage(PETS_IMAGES.$value['pet_cover_photo']);}else{$value['pet_cover_photo'] = base_url().'assets/default-p.png"';} ;?> data-logo="<?php echo $value['pet_cover_photo'];?>" <?php if($petDetails['id'] == $value['id'] ){echo 'selected';}?> ><?php echo $value['name'];?></option>
                                          <?php  } ;?>
                                       </select>
                                       </div>
                                       
                                    </div>


                                  
                                 </div>
                                 <?php endif?>
                                 <?php if(isset($petDetails) && !empty($petDetails['description'])):?>
                                 <div class="form-group margin">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    Notes From Customer
                                    </span>
                                    <div class="col-sm-5">
                                       <textarea class="form-control description" rows="3" disabled="disabled" ><?php echo $petDetails['description'];?></textarea>   
                                    </div>
                                 </div>
                                 <?php else:?>
                                 <div class="form-group margin descriptionDiv" style="display: none">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    Notes From Customer
                                    </span>
                                    <div class="col-sm-5">
                                       <textarea class="form-control description" rows="3" disabled="disabled"></textarea>   
                                    </div>
                                 </div>
                                 <?php endif?>
                                 <div class="form-group margin col s12 m12">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    <?= lang('appointmentDate') ?> <span class="required">*</span>
                                    </span>
                                    <div class="col-sm-5">
                                       <?php if(isset($appointment_date)) {?>
                                       <input id="datepicker" name="appointment_date" type="text" class="form-control" value="<?php echo date(get_option('date_format'), strtotime($appointment_date)); ?>">
                                       <?php } else {?>
                                       <input id="datepicker" name="appointment_date"  class="form-control" type="text" readonly="readonly">
                                       <?php } ?>
                                       <span id="datepicker-error" class="help-block animated fadeInDown error" for="datepicker" ></span>
                                    </div>
                                 </div>
                                 <div class="" style="margin-top: 25px">
                                    <!-- <div class="maincardDiv col s12 m12 l12">
                                    </div> -->
                                    <div id="card-panel-type1 " class="section">
           
                                    <div class="row maincardDiv">
                                    </div>
                                </div>
                                 </div>
                              </div>
                              <div class="modal-footer" style="float: right;" >
                                 <a href="<?php echo base_url('User/getAllAppointments') ?>" class="btn btn-default btn-flat btn-small backbtn" style="font-weight: normal;color: #646464;" >
                                 Back to Appointments
                                 </a>
                                 <button type="submit" class="btn btn-small submitButton CheckButton " id="btn" >Validate</button>
                              </div>
                           </div>
                           <?php echo form_close() ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="">

</div>


 <div class="modal fade" id='ResponseModal' role="dialog">
    <div class="modal-dialog">
    
      
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p></p>
        
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
         
        </div>
      </div>
      
    </div>
  </div> 

    <div class="modal fade" id='addCustomer' role="dialog">
        <form class='form-horizontal' id="addCustomerform" method="post" action="#">
            <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Customer </h4>
                </div>
                <div class="modal-body">

                <div class="form-group margin">
                    <label for="field-1" class="col-sm-5 control-label">Name<span class="required">*</span></label>
                    <div class="col-sm-5">
                        <input id="username" name="username"  class="form-control" type="text">
                        
                    </div>
                </div>

                <div class="form-group margin">
                    <label for="field-1" class="col-sm-5 control-label">Email</label>
                    <div class="col-sm-5">          
                        <input id="email" name="email"   class="form-control" type="text">
                    </div>
                </div>

                <div class="form-group margin">
                    <label for="field-1" class="col-sm-5 control-label">Phone<span class="required">*</span></label>
                    <div class="col-sm-5">          
                        <input id="phone" name="phone"   class="form-control" type="text">
                    </div>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-success btn-flat">Save</button>
                </div>
            </div>
            </div>
        </form> 
    </div>

    


  <div id="addPet" class="modal modal-fixed-footer" >
   <form class='form-horizontal' id="addPetform" method="post" action="#">
      <div class="modal-content">
                 <h4 class="modal-title">Add New Pet </h4>
         <div class="row">
            
               <input type="hidden" name="app_user_id" id="app_user_id" value="<?php echo $this->session->userdata('id'); ?>">
            
            <div class="form-group margin">
               <span class="input-group-addon"><?= lang('petName') ?> </span>
               <input id="name" name="name" class="form-control" value="" type="text" >
                         <div style=" color: #E13300" class="input-error"></div>
            </div>
           <div class="form-group margin">
              <span for="field-1" class="col-sm-3 control-label">
              <?= lang('petType') ?>
              <span class="required" aria-required="true"></span></span>
              <div class="col-sm-5 ">
                 <label>
                 <input name="type" value="Cat" type="radio" />
                 <span>Cat</span>
                 </label>
                 <label>
                 <input name="type" value="Dog" type="radio" />
                 <span>Dog</span>
                 </label>
                 <div  class="radioError"></div>
              </div>
           </div>
            <div class="form-group margin">
               <span class="input-group-addon"><?= lang('petBread') ?></span>
                   <select  class="form-control breed_id" id="editable-select" name="breed" >
                    </select>
                  <div style=" color: #E13300" class="input-error"></div>
            </div>
           <div class="form-group margin" >
              <span for="field-1" class="col-sm-3 control-label">
              Mix Breed
              <span class="required" aria-required="true"></span></span>
              <label>
              
                 <input type="checkbox" class="grey mix_breed" id="mix_breed" name="mix_breed"   value="1">
                 <span class="checkboxspan"></span>
              </label>
            </div>
            <div class="form-group margin">
               <span class="input-group-addon">Size</span>
                 <select class="form-control " name="size" id="size">
                          
                          
                          <?php foreach ($petSize as $value) {?>
                          <option value="<?php echo $value?>"  class=""><?php echo $value;?></option>
                          <?php }?>
                        </select>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="modal-close btn btn-light-grey">
         Close
         </button>
         <button type="submit" class="btn btn-blue">
         Save
         </button>
      </div>
   </form>
</div>


 


 <div class="modal fade" id='ResponseModal1' role="dialog">
    <div class="modal-dialog">
    
      
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p></p>
          <table class="table table-bordered table-striped serviceOverbookTable">
             
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary overbookSubmit"  style="">Overbook</button>
        </div>
      </div>
      
    </div>
  </div>         


<?php if(isset($appointment_time)):?>
    <script >
        var timepickerTime = '<?php echo $appointment_time;?>';
    </script>
<?php endif;?>
<script type="text/javascript">
   var ispet = "<?php echo $petDetails['name']; ?>"; 

</script>
<script> 
    // var timeZone = "<?php echo get_option('time_zone');?>";
    // moment.tz.setDefault(timeZone);
    // var current_time = moment().format('hh:mm a');


    
    var amount = 0.00;
    var global = 0;
    var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";  
    var petSize = '';
    var format_Money = "<?php echo get_option('currency_format');?>";
    var services = <?php echo json_encode($services)?>;
    var t1 =0;
    var currency_symbol = "<?php echo get_option('currency_symbol');?>";
    var servicesarray = <?php echo json_encode($services); ?>;
     
      function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
                decimalCount = 2;
                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        };


    var finalErrorFlag = 0;
    function customFormValidation(){
        if($('#addNewReservationForm').valid()){
            
            var pet_id = $('#pet_id').val(); 
            finalErrorFlag = 0;
            var customRequiredErr = '';
            var start_date = $('#datepicker').val();
            if(pet_id != '' && pet_id != null && pet_id.constructor === Array){
                $.each(pet_id, function( key, value ) {
                    var start_time = $(document).find(".serviceStartTime[data-pet_id='"+value+"']").val();
                    var end_time = $(document).find(".serviceEndTime[data-pet_id='"+value+"']").val();

                    var servicesArr = $(document).find(".servicesDiv[data-pet_id='"+value+"']").find('.servicesDropdown').val();

                    var selected_groomer = $(document).find(".groomerDiv[data-pet_id='"+value+"']").find('.user_id').val();

                    if( servicesArr == null){
                      $(document).find(".EndTimeError[data-pet_id='"+value+"']").html('Please Select Services');
                      $(document).find(".EndTimeError[data-pet_id='"+value+"']").show();
                      finalErrorFlag = 1;
                    }else{
                      $(document).find(".EndTimeError[data-pet_id='"+value+"']").html('');
                      $(document).find(".EndTimeError[data-pet_id='"+value+"']").hide();

                      $.ajax({
                          url: BASE_URL+'User/checkTimeValidtiy',
                          type: 'POST',
                          dataType: 'json',
                          async : false,
                          data : {
                              appointment_date : start_date,
                              appointment_time : start_time,
                              end_time :end_time,
                              // user_id : function(){
                              //       return $('#user_id').val();
                              //   },
                              user_id : selected_groomer,
                              services :servicesArr,
                              pet_id :value   
                          },
                          success: function (data) { 
                              if(data.err == 0){
                                $(document).find(".EndTimeError[data-pet_id='"+value+"']").html('');
                                $(document).find(".EndTimeError[data-pet_id='"+value+"']").hide();
                              }else{
                                $(document).find(".EndTimeError[data-pet_id='"+value+"']").html(data.message);
                                $(document).find(".EndTimeError[data-pet_id='"+value+"']").show();
                                finalErrorFlag = 1;
                              }
                          },
                          error : function(err)
                          {

                          }
                      });
                    }
                });  
            }
        }else{
            finalErrorFlag = 1;
        }
    }

    function validateFloatKeyPress(el) {

        var v = parseFloat(el.value);
        el.value = (isNaN(v)) ? '' : v.toFixed(2);
    }

   function fillBreedDropDown(type)
   {
      $.ajax({
         url: BASE_URL+'User/getAllBreeds/'+type ,
         type: 'GET',
         dataType: 'json',
         success: function (data) {
               if(data.errcode == 0)
               {
                  $(document).find('.breed_id').html(data.result);
                  $('.breed_id').not('.disabled').formSelect();
   
                  // $("#breed_id").select2();
                  // $("#breed_id").dropdown('open');
               }
               else
               {
                  $(document).find('.breed_id').html('<option value="">No Breed Found</option>');
               }
         },
          error : function(err)
          {
              $(document).find('.breed_id').html('<option value="">No Breed Found</option>');
          }
   
      });
   }
    

   
    function updateEndTimeOnService(value) {
        var pet_id = value;
        if(pet_id != '' && pet_id != null)
        {    

            var time =  $(document).find(".serviceStartTime[data-pet_id='"+value+"']").val();
            var servicesArr = $(document).find(".servicesDiv[data-pet_id='"+value+"']").find('.servicesDropdown').val();

            if(servicesArr != null)
            {

                var time =  $(document).find(".serviceStartTime[data-pet_id='"+value+"']").val();
                time = moment(time, ["h:mm A"]).format();   
                petSize = $('#pet_id').find('option[value="'+value+'"]').attr("data-size");
                
                $.each(servicesArr, function(k1, v1 ) {
                    var str = petSize+'_time_estimate';
                    aService = filterValue(services, "id",v1);
                    var minutes = aService[str];
                    time = moment(time).add(minutes, 'minutes').format();  
                    var end_time = moment(time).format('hh:mm A');

                    $(document).find(".serviceEndTime[data-pet_id='"+value+"']").val(end_time);
                });

            }
            else
            {
                
                $(document).find(".serviceEndTime[data-pet_id='"+value+"']").val(time);
            }
            
            customFormValidation();
        }
    }
    
    function createPetCard(value) {
        var pet_id = value;
        if(pet_id != '' && pet_id != null && pet_id.length > 0)
        {   
            var fromPet = 0;
            if(pet_id.constructor === Array){

            }else{
                var pet_id =pet_id.split();
                fromPet = 1;
            }
            var href = $('#pet_id').find('option[value="'+value+'"]').attr("data-encodedidhref");
            var type = $('#pet_id').find('option[value="'+value+'"]').attr("data-type");
            var pet_name = $('#pet_id').find('option[value="'+value+'"]').attr("data-name");
            var pet_logo = $('#pet_id').find('option[value="'+value+'"]').attr("data-logo");
            var depositNote = $('#app_user_id').find(':selected').attr('data-depositnote');
            if(depositNote == undefined){
                var depositNote = $('#app_user_id').attr('data-depositnote');
            }
            var br = parseInt($(document).find('.petCard').length) !=0  && parseInt($(document).find('.petCard').length) % 3  == 0 ? '<br style="clear:both">' : '';
            if(parseInt($(document).find('.petCard').length) % 2  == 0){
               var divclass =' gradient-45deg-indigo-purple';
            }else{
                
                var divclass =  'gradient-45deg-purple-deep-orange';
            }
            var pethtml = '';
            pethtml +=  '<div class="col s12 m6 l4 card-width petCard">'+
                        '<div class="card card-border '+divclass+'" style="padding: 0px;">'+
                              '<div class="card-content white-text">'+
                                    '<div class="col s12 ">'+
                                    '<a href="javascript:void(0)" class="closebtncard" data-pet_id ="'+value+'" data-pet_name ="'+pet_name+'" ><i style="color:white;" class="material-icons right">close</i></a></div>'+
                                    '<div class="center-align"><a href="'+href+'" target="_blank" class="redirectBtn noUiBlock "><img class="responsive-img circle z-depth-4 center" style="width: 90px;height: 90px;" src="'+pet_logo+'" alt="Avatar" >'+

                                    '<h5 class="white-text mb-1 " style="text-transform: capitalize;">'+pet_name+'</h5></a></div>'+
                                   
                                    '<div class="form-group  " style="margin-top: 30px;">'+
                        
                       
                                                  
                        '<div class="col-sm-7 s7 m7 servicesDiv" data-pet_id ="'+value+'">'+
                         '<span for="field-1" class="col-sm-4 s4 m4 control-label">Services<span class="required"></span></span> </span>'+
                            '<select class="form-control servicesDropdown" multiple>';
                            $.each(servicesarray, function( key1, value1 ) {
                                if(value1.is_deleted == 0 && type == value1.type) {
                                    pethtml += '<option value="'+(value1.id)+'" data-petid="'+value+'" data-cost="'+value1.sizecost+'" data-time="'+value1.sizetime+'">'+(value1.name)+'</option><span for="field-1" style="" class="col-sm-3 control-label"></span>';
                                }
                            });
                            pethtml += '</select>'+
                        '</div>'+
                    '</div>'+

                     '<div class="form-group ">'+
                        '<span for="field-1" class="col-sm-4 control-label">Assigned Groomer <span class="required">*</span></span>'+
                        '<div class="col-sm-7 groomerDiv" data-pet_id ="'+value+'">'+ 
                            '<select class="form-control user_id" name="user_id[]"  data-pet_id ="'+value+'">'+
                              
                            '</select>'+
                            '<span class="help-block animated fadeInDown groomerError" data-pet_id ="'+value+'" style="display: none;"></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group  ">'+
                        '<span for="field-1" class="col-sm-4 control-label">Start Time<span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<div class="bootstrap-timepicker timepicker">'+
                                '<input type="text" class="form-control serviceStartTime" name="app_srt_time[]" data-pet_id ="'+value+'" value="">'+
                                '<span class="help-block animated fadeInDown EndTimeError" data-pet_id ="'+value+'" style="display: none;"></span>'+
                            '</div>'+ 
                        '</div>'+
                    '</div>'+
                    '<div class="form-group  ">'+
                        '<span for="field-1" class="col-sm-4 control-label">End Time<span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control serviceEndTime" name="app_end_time[]" data-pet_id ="'+value+'"  disabled="disabled" value="">'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group  ">'+
                        '<span for="field-1" class="col-sm-4 control-label">Cost<span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<div class="input-icon">'+
                                '<input class="form-control serviceCost" value=""  type="text" data-pet_id ="'+value+'" disabled>'+
                                '<i>'+currency_symbol+'</i>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+

                    '<div class="form-group margin">'+
                        '<span for="field-1" class="col-sm-4 control-label" style="margin-top: 0px">Deposit</span>'+
                        '<div class="col-sm-8">'+
                            '<label class="css-input css-radio css-radio-success push-10-r depositRadio">'+
                                '<input type="radio" value="1" name="deposit'+value+'" class="depositRadio disableRadio" aria-invalid="false" checked="">'+
                                '<span>In ($)</span>'+
                            '</label>'+
                            '<label class="css-input css-radio css-radio-success push-10-r depositRadio">'+
                                '<input type="radio" value="0" name="deposit'+value+'"  class="depositPerRadio disableRadio">'+
                                '<span>In (%)</span>'+
                            '</label>'+
                        '</div>'+ 
                    '</div>'+
                    '<div class="form-group margin depositPerDiv" style="display: none;">'+
                        '<span for="field-1" class="col-sm-4 control-label">Deposit(%) <span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<input class="form-control depositPer" value="" type="text">'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group  deposit_Div" style="margin-bottom:0px">'+
                        '<span for="field-1" class="col-sm-4 control-label">Deposit($)<span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            // '<div class="input-icon">'+
                            '<div>'+
                                '<input class="form-control serviceDeposite" onchange="validateFloatKeyPress(this);" value=""  type="text" data-pet_id ="'+value+'">';
                                // '<i>'+currency_symbol+'</i>'+

                            if(depositNote !=undefined && depositNote.length > 0){
                                pethtml += '<span class="fadeInDown" style="color: #dd4b39;">'+depositNote+'</span>';
                            }
                            pethtml += '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group margin payment_sourceDiv" style="margin-bottom: 0px;display:none">'+
                        '<span for="field-1" class="col-sm-4 control-label" style="padding-top: 0px;">Payment Source</span>'+
                        '<div class="col-sm-7 drop-down">'+
                            '<select class="form-control paymentSource" name="payment_source">'+
                              '<option value="Cash">Cash</option>'+
                              '<option value="Credit/Debit">Credit/Debit</option>'+
                              '<option value="Other">Other</option> '+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group  otherSourceDiv" style="display: none;padding-top: 5px;margin-bottom: 0px;">'+
                        '<span for="field-1" class="col-sm-4 control-label" style="padding-top: 0px;">Other Source <span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<input name="other_source" class="form-control other_source" value=""  type="text">'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group " style="margin: 0px;">'+
                       '<span for="field-1" class="col-sm-4 control-label"><span class="required"></span></span>'+
                        '<div class="col-sm-7">'+
                            '<span class="help-block animated fadeInDown col-md-12 cardCustomeError" data-pet_id ="'+value+'"></span>'+
                        '</div>'+
                    '</div>'+


                              '</div>'+
                        '</div>'+
                  '</div>'+
            '</div>';

            $('.maincardDiv ').append(pethtml);
            // $('.servicesDropdown').fastselect();
            $('.user_id').formSelect();
            $('.servicesDropdown').formSelect();
            $(".serviceStartTime").timepicker({
               showInputs: false,
               minuteStep:15,
               // defaultTime: timepickerTime
            });
            updateEndTimeOnService(value);
            getGroomersOnDate(pet_id);
        }
    }

     function filterValue(obj, key, value) {
        return obj.find(function(v){ return v[key] === value});
     }

    function servicesselected(ele){
        return ele.value;
    }

    function getGroomersOnDate(value='all'){
        $.ajax({
            url: BASE_URL+'User/getGroomersOnDate',
            type: 'Post',
            dataType: 'json',
            async : false,
            data: {
                date:function(){
                    return $('#datepicker').val()
                }
            },
            success: function (data) {
                if(value == 'all'){
                    $('.user_id').html(data.result); 
                }else{
                    $(document).find(".user_id[data-pet_id='"+value+"']").html(data.result);
                }
            },
            error : function(err)
            {
                $(document).find('.pet_list').show();
            }
        });
    }



    </script>
  </script>
    <?php if(isset($petDetails)):?>
    <script >
        var petSize = '<?php echo $petDetails->size;?>';
        t1 = 1;
    </script>
    <?php endif;?>


    <script type="text/javascript">

    var petdetails = '<?php echo $petDetails->id;?>';
    // console.log(petdetails);
    $(document).ready(function(){


        getGroomersOnDate();
        // $('#phone').usPhoneFormat({
        //       format: '(xxx) xxx-xxxx',
        // });
                    $('#pet_id').formSelect();

        // $('#pet_id').fastselect();
        if(petSize){
            var first_pet_id = $(document).find('#pet_id').val();           
            if(first_pet_id != null){
                setTimeout(function(){ 
                    $(document).find('#pet_id').val(first_pet_id).trigger('change'); 
                }, 100);  
            }
        }
        
        // $.unblockUI();
       
        if(t1 == 0){
            // $("#app_user_id").select2();
        }

        $(document).find('.addPet').hide();

        $(document).on('change','.paymentSource',function(){
          if($(this).find(':selected').val() == 'Other'){
            $(this).parents('.card-body').find('.otherSourceDiv').show();
          }else{
            $(this).parents('.card-body').find('.otherSourceDiv').hide();
          }
        });

 

        //date picker 
        // var datepicker_today = '0';
       
        // if(date_today < moment().format('YYYY-MM-DD')){
        //     datepicker_today = '-1d';
        // }
        // else if(date_today > moment().format('YYYY-MM-DD')){
        //     datepicker_today = '+1d';
        // }

        // $('#datepicker').datepicker({
        //     startDate: datepicker_today,
        //     autoclose: true,
        //     format: format
        // }).
        // on('changeDate', function(ev) {

            
        //     getGroomersOnDate();

        //     //validate on change function
        //     bookedPetArray = [];

        //     customFormValidation();
            
        //     $(document).find('.EndTimeError').html('');
        //     $(document).find('.EndTimeError').hide();

            
            
            
        //     $(document).find('.cardBtn').hide();
        //     $(document).find(".cardCustomeError").html('');
        //     $(document).find(".cardCustomeError").hide();
  
        // });

        
        //var serviceStartTimeFlag = 0 
        $(document).on('hide.timepicker','.serviceStartTime',function(){
            var pet_id = $(this).attr("data-pet_id");
            
            $(document).find(".cardBtn[data-pet_id='"+pet_id+"']").hide();
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").html('');
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").hide();
            
            //var time = $(this).val();
            // if(serviceStartTimeFlag == 0){
            //     $(document).find(".cardBtn").hide();
            //     $(document).find(".serviceStartTime").val(time);
            // }
            //serviceStartTimeFlag =1;
            bookedPetArray = $.grep(bookedPetArray, function(value) {
              return value != pet_id;
            });

            updateEndTimeOnService(pet_id);
        });

        $(document).on('change','.user_id',function(){
            bookedPetArray = [];

            customFormValidation();

            var pet_id = $(this).closest('.groomerDiv').attr("data-pet_id");

            
            
            $(document).find(".cardBtn[data-pet_id='"+pet_id+"']").hide();
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").html('');
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").hide();
        });
        
        
        $(document).on('change','.servicesDropdown',function()
        {

            var servicesArr = $(this).val();
            var pet_id = $(this).closest('.servicesDiv').attr("data-pet_id");
            $(document).find(".cardBtn[data-pet_id='"+pet_id+"']").hide();
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").html('');
            $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").hide();
            

            
            petSize = $('#pet_id').find('option[value="'+pet_id+'"]').attr("data-size");
           
            amount = 0;
            $.each(servicesArr, function( key, value ) {
                aService = filterValue(services, "id",value);
                var str = petSize+'_cost';
                var cost = aService[str];
                amount = parseFloat(amount)+parseFloat(cost);  
            });
            bookedPetArray = $.grep(bookedPetArray, function(value) {
              return value != pet_id;
            });
            $(document).find(".serviceCost[data-pet_id='"+pet_id+"']").val(formatMoney(amount, format_Money));
            updateEndTimeOnService(pet_id);
            

        }); 

        

        $("#addPetform").validate({
            excluded: ':disabled',
            onfocusout: function(e) {
                    this.element(e);                          
            },
            rules: {
                app_user_id: {
                    required : true
                },
                name : {
                    required : true
                },
                type : {
                    required: true
                },
                size : {
                    required : true
                },
            },

            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block animated fadeInDown',
            errorPlacement: function(error, element) {
                if (element.parent('.col-sm-5').length) {
                   
                   
                     error.insertAfter(element.closest('.col-sm-5').find('.input-error'))
                } else {
                    console.log(element.parent);
                    if(element.parent('.css-input')){
                        error.insertAfter(element.closest('.col-sm-5').find('.radioError'));
                    }
                    else
                    {
                        error.insertAfter(element);
                    }
                    
                }
            }
        });

        
        $("#addCustomerform").validate({
            excluded: ':disabled',
            onfocusout: function(e) {
                    this.element(e);                          
            },
            rules: {
                username : {
                    required : true
                },
                phone : {
                   required: true,
                   remote : {
                     url: BASE_URL + "User/checkCustomerPhone",
                     type: "POST",
                     dataType: "json",

                   },
                   minlength:10
                },
                email : {
                    //required: true,
                    email:true,
                    remote : {
                          url: BASE_URL + "User/checkCustomerEmail",
                          type: "POST",
                          dataType: "json",
                         
                    }
                },
                
                
            },
            messages : {
                phone : {
                   minlength : "Phone number should be atleast 10 digit",
                   remote : "Phone already exists"
                },
                email : {
                    remote : "Email already exists"
                },
                username : {
                    remote : "Username already exists"
                },
                emergency_contact_phone_number : {
                  minlength : "Emergency Number should be atleast 8 digit"
                 },
            },

            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block animated fadeInDown',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    console.log(element.context.name);
                    
                    if(element.parent('.moduleCheckbox').length > 0){
                       
                        //console.log(element.parent('label').parent('div').html());
                        error.insertAfter(element.closest('.form-group').find('.checkError'));
                        //$(document).find('.checkError').show();
                    }
                    else {
                        error.insertAfter(element);
                    }
                    
                }
            }
        });


        $(document).on('submit','#addCustomerform',function(e){
            e.preventDefault();
            if($(this).valid())
            {
                // $.blockUI({
                //    baseZ: 2000,
                //      css: {
                //          border: 'none',
                //          padding: '15px',
                //          backgroundColor: '#000',
                //          '-webkit-border-radius': '10px',
                //          '-moz-border-radius': '10px',
                //          opacity: .5,
                //          color: '#fff'
                //      }
                // });
               
                $.ajax({
                    url: BASE_URL+'User/addNewQuickCustomer',
                    type: 'Post',
                    dataType: 'json',
                    data: {
                        email:function(){
                            return $('#addCustomerform').find('#email').val()
                        },
                        phone:function(){
                            return $('#addCustomerform').find('#phone').val()
                        },
                        username:function(){
                            return $('#addCustomerform').find('#username').val()
                        }
                    },
                    success: function (data) {
                      
                      if(data.errcode == 0)
                      {
                        $('#app_user_id option[value=""]')
                             .after($("<option></option>")
                                        .attr("value",data.result.id)
                                        .attr('data-username',data.result.username)
                                        .attr('data-email',data.result.email)
                                        .attr('data-phone',data.result.phone)
                                        .text(data.result.username));

                        $('#app_user_id').val([1,data.result.id]).trigger('change');

                        $(document).find('#addCustomer').modal('hide');

                        $('#addCustomerform').find('#email').val('');
                        $('#addCustomerform').find('#phone').val('');
                        $('#addCustomerform').find('#username').val('');

        
        
                        
                        
                        

                        $(document).find('.pet_list').show();

                      }
                      else
                      {
                        $(document).find('.pet_list').show();
                      }
                      // $.unblockUI();
                    },
                    error : function(err)
                    {
                        // $.unblockUI();
                        $(document).find('.pet_list').show();
                    }
                });
                $(document).find('.addPet').show();
            }

        });


        $(document).on('submit','#addPetform',function(e){
            e.preventDefault();
            if($(this).valid())
            {
                // $.blockUI({
                //    baseZ: 2000,
                //      css: {
                //          border: 'none',
                //          padding: '15px',
                //          backgroundColor: '#000',
                //          '-webkit-border-radius': '10px',
                //          '-moz-border-radius': '10px',
                //          opacity: .5,
                //          color: '#fff'
                //      }
                // });
                $.ajax({
                    url: BASE_URL+'User/addNewQuickPet',
                    type: 'Post',
                    dataType: 'json',
                    data: {
                        name:function(){
                            return $('#addPetform').find('#name').val()
                        },
                        size:function(){
                            return $('#addPetform').find('#size').val()
                        },
                        type:function(){
                            return $('#addPetform').find("input[name='type']:checked").val();
                        },
                        app_user_id:function(){
                            return $('#addPetform').find('#app_user_id').val()
                        }, 
                        breed:function(){
                            return $('#addPetform').find('#editable-select').val()
                        },
                        mix_breed:function(){
                            if ($('#addPetform').find('#mix_breed').is(':checked')) {
                               return 1; 
                            }else{
                                return 0; 
                            }
                        },
                    },
                    success: function (data) {
                      $(document).find('#addPet').modal('hide');  
                      
                      if(data.errcode == 0)
                      {
                        var app_user_id = $('#app_user_id').val();
                        $("#app_user_id option[value='"+app_user_id+"']").trigger('change');
                        // $(document).find('#pet_id').val(data.insertArray.id).trigger('change');
                        // $(document).find('#pet_id').val(data.insertArray.id).attr('selected', true);
        
        
                        
                        
                        
                      }
                      else
                      {
                        
                      }
                      // $.unblockUI();
                    },
                    error : function(err)
                    {    
                        // $.unblockUI();
                        $(document).find('.pet_list').show();
                    }
                });
            }
        });

        
        $(document).on('click','.addCustomer',function(){
            $('#addCustomerform')[0].reset();
        });

        $(document).on('click','.addPet',function(){
            $('#addPetform')[0].reset();
            $('#addPet').modal('open');
            if($('#app_user_id').val().length > 0)
            {
                var username = $('#app_user_id').find(':selected').attr('data-username');
                $(document).find('#addPet #username').val(username);
                $(document).find('#addPet #app_user_id').val($('#app_user_id').val());

            }

        });

        var ref_user_id = 0;
        // $(document).on('change','#app_user_id',function(){
            
            
            

            var app_user_id = $('#app_user_id').val();

            if(app_user_id.length > 0){
                // $.blockUI({
                //   baseZ: 2000,
                //     css: {
                //           border: 'none',
                //           padding: '15px',
                //           backgroundColor: '#000',
                //           '-webkit-border-radius': '10px',
                //           '-moz-border-radius': '10px',
                //           opacity: .5,
                //           color: '#fff' 
                //         }
                // });

                
                bookedPetArray = [];
                if(ref_user_id != app_user_id){
                    cardCreated = [];
                }
                $.ajax({
                    url: BASE_URL+'User/getAppUserPetsById/'+app_user_id ,
                    type: 'POST',
                    data : {
                        app_user_id : app_user_id,
                        tempArr :cardCreated  
                    },
                    dataType: 'json',
                    success: function (data) {

                        if(data.errcode == 0)
                        {
                            if(ref_user_id != app_user_id){
                               $('.fstChoiceRemove').click(); 
                               ref_user_id = app_user_id;
                            }
                            $(document).find('.pet_list').show();
                            $(document).find('.petsDropDown').html(data.result);
                            // $('#pet_id').fastselect();
                            $('#pet_id').formSelect();

                            var first_pet_id = $(document).find('#pet_id').val();
                            if(first_pet_id != null){
                                $(document).find('#pet_id').val(first_pet_id[0]).trigger('change');
                            }
                            amount = 0;
                        }
                        else
                        {
                            $('.fstChoiceRemove').click();
                            $(document).find('.pet_list').show();
                            $(document).find('.petsDropDown').html(data.result);
                            // $('#pet_id').fastselect();
                            $('#pet_id').formSelect();
                        }
                        // $.unblockUI();
                    },
                    error : function(err)
                    {
                        // $.unblockUI();
                       $(document).find('.pet_list').show();
                       $('.fstChoiceRemove').click();
                       // $('#pet_id').fastselect();
                       $('#pet_id').formSelect();
                    }
              }); 

              $(document).find('.addPet').show();     
            }
            else
            {
                $('.fstChoiceRemove').click(); 
                $(document).find('.addPet').hide(); 
            }
        // });

        var cardCreated = [];

        $(document).on('change','#pet_id',function(){
            
            
            
            var selectedPets = $(this).val();
                
            $.each(selectedPets,function(key,val){
                if(cardCreated.indexOf(val) < 0 ){
                    cardCreated.push(val); 
                    createPetCard(val);  
                }
            }); 
            
        
             
        });


        $(document).on('click','.closebtncard',function(){
           var pet_id = $(this).attr("data-pet_id");
           var pet_name = $(this).attr("data-pet_name");
           // $(this).closest('.petCard').remove();
           $(".closebtncard[data-pet_id='"+pet_id+"']").closest('.petCard').remove();
                var selectedPets = $("#pet_id").val();

                selectedPets = $.grep(selectedPets, function(value) {
                  return value != pet_id;
                });

                 $.each($("#pet_id_ option:selected"), function () {
                    
                    $(this).prop('selected', false); 
                });
                console.log(selectedPets);
        });

         
        $(document).on('focusout','.serviceDeposite',function(){
            if($(this).val() > 0){
                $(this).closest('.card-body').find('.payment_sourceDiv').show();
            }else{
                $(this).closest('.card-content').find('.paymentSource').val('Cash');
                $(this).closest('.card-content').find('.other_source').val('');
                $(this).closest('.card-content').find('.payment_sourceDiv').hide();
                $(this).closest('.card-content').find('.otherSourceDiv').hide();
            }
        });

        $(document).on('focusout','.depositPer',function(){
            var srevice_cost = $(this).closest('.card-body').find('.serviceCost').val();
            if($(this).val() > 0 && srevice_cost>0){
                var calculate_cost = ((srevice_cost / 100) * $(this).val());
                calculate_cost = parseFloat((calculate_cost).toFixed(2));
                $(this).closest('.card-content').find('.serviceDeposite').val(calculate_cost);
                $(this).closest('.card-content').find('.payment_sourceDiv').show();
            }else{
                $(this).closest('.card-content').find('.paymentSource').val('Cash');
                $(this).closest('.card-content').find('.other_source').val('');
                $(this).closest('.card-content').find('.payment_sourceDiv').hide();
                $(this).closest('.card-content').find('.otherSourceDiv').hide();
                $(this).closest('.card-content').find('.serviceDeposite').val('');
            }
        });
        
        $(document).on('click','.depositRadio',function(){
            $(this).closest('.card-content').find('.depositPer').val('');
            $(this).closest('.card-content').find('.serviceDeposite').val('');
            $(this).closest('.card-content').find('.paymentSource').val('Cash');
            $(this).closest('.card-content').find('.other_source').val('');
            $(this).closest('.card-content').find('.payment_sourceDiv').hide();
            $(this).closest('.card-content').find('.otherSourceDiv').hide();

            if($(this).find('.disableRadio').val() == 1){
                $(this).closest('.card-content').find('.serviceDeposite').prop('disabled', false);
                $(this).closest('.card-content').find('.depositPerDiv').hide();   
            }else{
                $(this).closest('.card-content').find('.serviceDeposite').prop('disabled', true);
                $(this).closest('.card-content').find('.depositPerDiv').show();
            }
        });

        // To remove delected pet on close button click 
        $(document).on('click','.close-icon', function(){
            var pet_id = $(this).attr("data-pet_id");
            $("div[data-value='"+pet_id+"']").find(".fstChoiceRemove").click();
            bookedPetArray = $.grep(bookedPetArray, function(value) {
              return value != pet_id;
            });
        });


        $('#addPetform').find('input[type=radio][name=type]').change(function() {

            $('#addPetform').find('.breed_id').parents('.form-group').show();
            $('#addPetform').find('#mix_breed').parents('.form-group').show();
            $('#addPetform').find('#mix_breed').parents('.checkbox').show();
            if (this.value == 'Cat') {
                fillBreedDropDown('Cat');
            }
            else if (this.value == 'Dog') {
                fillBreedDropDown('Dog');
            }

        });


        
        $('.CheckButton').on('click',function(e) {
            //e.preventDefault();
            customFormValidation();
            if(finalErrorFlag == 0)
            {
                // $.blockUI({
                //   baseZ: 2000,
                //     css: {
                //           border: 'none',
                //           padding: '15px',
                //           backgroundColor: '#000',
                //           '-webkit-border-radius': '10px',
                //           '-moz-border-radius': '10px',
                //           opacity: .5,
                //           color: '#fff' 
                //         }
                // });

                yourArray = [];
                startTimeArray = [];
                selected_groomerArray = [];

                var pet_id = $('#pet_id').val();
                if(pet_id != '' && pet_id != null && pet_id.constructor === Array){
                    var pet_id = $('#pet_id').val();
                    $.each(pet_id, function( key, value ) {
                        var servicesArr = $(document).find(".servicesDiv[data-pet_id='"+value+"']").find('.servicesDropdown').val();
                        
                        // var selected_groomer = $(document).find(".groomerDiv[data-pet_id='"+value+"']").find('.user_id').val();

                        if(servicesArr == null)
                        {
                            $(document).find(".EndTimeError[data-pet_id='"+value+"']").html('Please Select Services');
                            $(document).find(".EndTimeError[data-pet_id='"+value+"']").show();   
                        }

                        if(servicesArr != null && bookedPetArray.indexOf(value) < 0)
                        {
                            $.each(servicesArr, function(k1, v1 ) {
                                yourArray.push(value+' '+v1);   
                            });
                            startTimeArray.push($(document).find(".serviceStartTime[data-pet_id='"+value+"']").val()); 
                            selected_groomerArray.push($(document).find(".groomerDiv[data-pet_id='"+value+"']").find('.user_id').val()); 


                        }
                    });  
                }
                
                $.ajax({
                        url : BASE_URL+'User/appointmentTimeNew',
                        type: 'POST',
                        dataType: 'json',
                        //async : false,
                        data : {
                                appointment_date : function(){
                                    return $('#datepicker').val();
                                },
                                appointment_end_time : function()
                                {
                                    return $('.serviceEndTime').val();
                                },
                                appointment_time : startTimeArray,
                                services : yourArray, 
                                app_user_id : function(){
                                    return $('#app_user_id').val();
                                }, 
                                pet_id : function(){
                                   return $('#pet_id').val(); 
                                },
                                // user_id : function (){
                                //     return $('#user_id').val();
                                // },
                                user_id : selected_groomerArray,
                                payment_source : function (){
                                    return $('.paymentSource').val();
                                },
                                other_source : function (){
                                    return $('.other_source').val();
                                }
                            },
                        success : function(data)
                        {
                            
                            
                            // $.unblockUI();

                            if(data.error == 0)
                            {
                                
                                console.log(data.result);
                                var string = '';
                                var flag = 0;
                                $.each(data.result, function( key, value ) {
                                    
                                    var html = '';
                                    if(value.already_appointment_exist == 1){
                                        html ='';
                                        html ='<button type="submit" class="btn btn-sm cardBtn" data-pet_id ="'+value.petdetails.id+'" style="background-color:gray;color: white;"><i class="fa fa-check" aria-hidden="true"></i> Already Booked</button>';
                                        $(document).find(".bookingDiv[data-pet_id='"+value.petdetails.id+"']").html(html);

                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").html('');
                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").hide();
                                    }else if(value.overbooking == 1){

                                        html ='';
                                        html ='<button type="submit" class="btn btn-sm btn-danger bookingBtn cardBtn flash-button" data-pet_id ="'+value.petdetails.id+'" data-overlapping ="1" data-schedule_id ="0">Overbook</button>';
                                        $(document).find(".bookingDiv[data-pet_id='"+value.petdetails.id+"']").html(html);

                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").html(value.overbookingReason);
                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").show();

                                        $(document).find(".serviceStartTime[data-pet_id='"+value.petdetails.id+"']").val(value.appointment_time);
                                        $(document).find(".serviceEndTime[data-pet_id='"+value.petdetails.id+"']").val(value.appointment_end_time);
                                    }else{
                                        html ='';
                                        html ='<button type="submit" class="btn btn-sm bg-olive btn-flat cardBtn bookingBtn flash-button" data-pet_id ="'+value.petdetails.id+'" data-overlapping="0" data-schedule_id ="'+value.normal_booking_time.allocated_schedule_id+'">Confirm</button>';
                                        $(document).find(".bookingDiv[data-pet_id='"+value.petdetails.id+"']").html(html);
                                        $(document).find(".serviceStartTime[data-pet_id='"+value.petdetails.id+"']").val(value.appointment_time);
                                        $(document).find(".serviceEndTime[data-pet_id='"+value.petdetails.id+"']").val(value.appointment_end_time);
                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").html('');
                                        $(document).find(".cardCustomeError[data-pet_id='"+value.petdetails.id+"']").hide();   
                                    }
                                });
                            }
                            else
                            {

                            }
                        },
                        error : function(error)
                        {
                            // $.unblockUI();
                            console.log(error);
                        }    

                    });

            }
            return false;
        }); 

         var bookedPetArray = [];

        $(document).on('click','.bookingBtn',function(){
            if($('#addNewReservationForm').valid())
            {
                // $.blockUI({
                //   baseZ: 2000,
                //     css: {
                //           border: 'none',
                //           padding: '15px',
                //           backgroundColor: '#000',
                //           '-webkit-border-radius': '10px',
                //           '-moz-border-radius': '10px',
                //           opacity: .5,
                //           color: '#fff' 
                //         }
                // });
                yourArray = [];
                var pet_id = $(this).attr("data-pet_id");
                
                yourArray = $(document).find(".servicesDiv[data-pet_id='"+pet_id+"']").find('.servicesDropdown').val();
                var overlapping_appointment = $(this).attr("data-overlapping");
                var schedule_id = $(this).attr("data-schedule_id");
                var paymentSource = $(this).parents('.card-body').find('.paymentSource').val();
                var other_source = $(this).parents('.card-body').find('.other_source').val();
                $.ajax({
                        url : BASE_URL+'User/appointmentBookingAjax',
                        type: 'POST',
                        dataType: 'json',
                        //async : false,
                        data : {
                                appointment_date : function(){
                                    return $('#datepicker').val();
                                },
                                appointment_end_time : function()
                                {
                                    return $(document).find(".serviceEndTime[data-pet_id='"+pet_id+"']").val();
                                },
                                deposit : function()
                                {
                                    return $(document).find(".serviceDeposite[data-pet_id='"+pet_id+"']").val();
                                },
                                cost : function()
                                {
                                    return $(document).find(".serviceCost[data-pet_id='"+pet_id+"']").val();
                                },
                                overlapping_appointment : overlapping_appointment,
                                allocated_schedule_id : schedule_id,
                                appointment_time : function(){
                                    return $(document).find(".serviceStartTime[data-pet_id='"+pet_id+"']").val();
                                },
                                services : yourArray, 
                                app_user_id : function(){
                                    return $('#app_user_id').val();
                                }, 
                                pet_id : pet_id,
                                user_id : function (){
                                    // return $('#user_id').val();
                                    return $(document).find(".groomerDiv[data-pet_id='"+pet_id+"']").find('.user_id').val();

                                },
                                payment_source : paymentSource,
                                other_source : other_source    
                            },
                        success : function(data)
                        {   
                            // $.unblockUI();
                            if(data.errcode == 0)
                            {
                                bookedPetArray.push(pet_id);
                                $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").html('');
                                $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").hide();
                                $(document).find(".bookingDiv[data-pet_id='"+pet_id+"']").html('');
                                html ='';
                                html ='<button type="submit" class="btn btn-sm btn-primary" data-pet_id ="'+pet_id+'"><i class="fa fa-check" aria-hidden="true"></i> Booked Successfully</button>';
                                $(document).find(".bookingDiv[data-pet_id='"+pet_id+"']").html(html);
                                $(document).find(".CheckButton").click();
                                 
                            }
                            else
                            {
                                $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").html(data.message);
                                $(document).find(".cardCustomeError[data-pet_id='"+pet_id+"']").show();
                            }
                        },
                        error : function(error)
                        {
                            // $.unblockUI();
                            console.log(error);
                        }    

                    });
            }
            return false;
        });

        

        //add New appointment validation
        $("#addNewReservationForm").validate({
            excluded: ':disabled',
            onfocusout: function(e) {
                    this.element(e);                          
            },
            rules: {
                app_user_id : {
                    required: {
                        depends: function(element) {
                            return $("#app_user_id").val() == '';
                        },    
                    },
                },
                appointment_date : {
                    required: {
                        depends: function(element) {
                            return $("#datepicker").val() == '';
                        },    
                    },
                },
                user_id : {
                    required: {
                        depends: function(element) {
                            return $("#user_id").val() == null;
                        },    
                    },
                },
                pet_id : {
                    required: {
                        depends: function(element) {
                            return $("#pet_id").val() == null;
                        },    
                    },
                }
            },
            messages: {
                user_id : {
                    required : 'Please Select Groomer'
                }
            },
            submitHandler: function(event){
                // event.preventDefault();
                // //alert();
                  

                // return false;       
            },        
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorClass: 'help-block animated fadeInDown error',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    if(element.parent('.serviceCheckbox').length > 0){
                        error.insertAfter(element.closest('.col-sm-5').find('.checkError'));
                    }
                    else {
                        error.insertAfter(element);
                    }  
                }
            }
        });

    });

</script>