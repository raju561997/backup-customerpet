
<div id="deletefileModal" class="modal modal-fixed-footer" >
   <div class="modal-content">
      <h4 class="modal-title">Delete File</h4>
      <p>Are you sure you want to delete the file?</p>
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <a href="" class="btn btn-info deleteFileBtn">
      Delete
      </a>
   </div>
</div>
</div>

<div class="modal fade text-left" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel35"> Add Vaccination Record</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addMedication1" method="post" action="<?php echo base_url();?>user/addPetNote">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name"><?= lang('medication_name') ?></label>
                        <input type="text" class="form-control" id="medication_name" name="medication_name" placeholder="Medication Name">
                        <input type="hidden" name="formType" class="form-control" value="edit" id="formType">
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="date_of"><?= lang('date_of') ?></label>
                        <input id="date_of" name="date_of" class="form-control" type="date" placeholder="Date of">
                    </fieldset>

                    <fieldset class="form-group floating-label-form-group">
                        <label for="date_of"><?= lang('expiry_on') ?></label>
                        <input id="expiry_on" name="expiry_on" class="form-control" type="date" placeholder="Expiry on">
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1"><?= lang('notes') ?></label>
                        <textarea class="form-control" id="notes" rows="5" name="notes" placeholder="Notes"></textarea>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label for="eventInput2"><?= lang('vaccinationphoto') ?></label>
                        <?php $class='fileupload-new';?>
                        <div class="fileupload <?php echo $class;?>" data-provides="fileupload">
                            <div class="user-image">
                                <div class="fileupload-new thumbnail ">
                                    <img src="<?php echo base_url();?>assets/vaccination_record.png" alt="" class="img-responsive img1">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail img1">
                                    <img src="<?php echo base_url();?>assets/vaccination_record.png" alt="" class="img-responsive img1">
                                </div>
                                <div class="user-image-buttons">
                                    <span class="btn btn-success btn-file btn-sm" ><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                    <input type="file" name="vaccine_image" accept="image/png,image/jpeg,image/jpg">
                                    <input type="hidden" name="avatar_data1">
                                    </span>
                                    <a href="javascript:void(0)" class="btn btn-danger fileupload-exists btn-bricky btn-sm " data-dismiss="fileupload" >
                                    Remove
                                    </a>
                                    <!-- <a href="javascript:void(0)" class="btn btn-danger fileupload-exists btn-bricky btn-sm deleteButton"  data-href="<?php echo base_url('User/deletePetImage/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($pet['id']))) ?>">
                                    Remove
                                    </a> -->
                                </div>
                                <div class="user-image-buttons webcam" style="margin:10px 0px;">
                                    <a href="javascript:void(0)" class="btn btn-primary webcamModalButton btn-sm" data-toggle="modal">Take Snapshot</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Crop Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <img class="c_resize" src="" style="width: 100%;"  />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary cropSaveBtn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="editModal" class="modal modal-fixed-footer" >
   <form class='form-horizontal' id="editMedication" method="post" action="">
      <div class="modal-content">
         <h4 class="modal-title">Edit Vaccination Record </h4>
         <div class="row">
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('medication_name') ?></span>
               <input id="medication_name" name="medication_name"  class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('date_of') ?></span>
               <input id="editdatepicker" name="date_of"  readonly="readonly" class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('expiry_on') ?></span>
               <input id="editdatepicker1" name="expiry_on"  readonly="readonly" class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('notes') ?></span>
               <textarea id="notes" name="notes"  class="form-control" type="text" rows="5" col="10"></textarea>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="modal-close btn btn-light-grey">
         Close
         </button>
         <button type="submit" class="btn btn-blue">
         Save
         </button>
      </div>
   </form>
</div>

<div id="deleteModal1" class="modal modal-fixed-footer" >
   <div class="modal-content">
      <h4 class="modal-title">Delete Vaccination Note</h4>
      <p>Are you sure you want to delete this Note?</p>
   </div>
   <div class="modal-footer">
      <button type="button" class="modal-close btn btn-light-grey" data-dismiss="modal">Close</button>
      <a href = '' class="btn btn-danger  deleteButtonModel1"><i class="fa fa-trash"></i> Delete</a>
   </div>
</div>



<div class="modal fade text-left" id="sizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Size Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Image size should atleast be 150x150.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="extensionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Extension Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Please make sure you upload an image file.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>

<div class="resized" style="display: none;"></div>

<div class="modal fade text-left" id="addfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <form class='form-horizontal' id="addfileModalform" method="post" action="<?php echo base_url()?>User/addfiles" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Add File</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <fieldset class="form-group">
                        Note: Uploaded document will expire after 1 year.
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="title1">Title</label>
                        <input id="filetitle" name="title"  class="form-control" type="text">
                        <input type="hidden" name="pet_id" value="<?php echo  $pet['id']?>">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="eventInput1">File Name</label>
                        <span class="btn btn-success btn-file btn-sm"><span class="">Select File</span>
                            <input type="file" name="file_name" accept="application/pdf,image/png, image/jpeg,image/jpg" required> 
                        </span>
                        <span class="fileuploadYn"></span>
                        <!-- <span id="file_name-error" class="error" for="file_name"></span> -->
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="deleteModal" class="modal modal-fixed-footer" >
   <div class="modal-content">
      <h4 class="modal-title">Delete Pet Image</h4>
      <p>Are you sure you want to delete this pet image?. This will delete image permanently.</p>
   </div>
   <div class="modal-footer">
      <button type="button" class="modal-close btn btn-light-grey" data-dismiss="modal">Close</button>
      <a href = 'javascript:void(0)' class="btn btn-danger  deleteButtonModel"><i class="fa fa-trash"></i> Delete</a>
   </div>
</div>

<script type="text/javascript">
   
   function imgError(image) {
       image.onerror = "";
       image.src = "http://via.placeholder.com/100x100?text=Image N/A";
       return true;
   }
   
   $(document).ready(function(){
     $(document).find('#table111').DataTable({
      
       "responsive":true,
       "bInfo": false,
       "bAutoWidth": false,
       "bSort" : false,
       "paging": true
       
     });
   
     $('.addnoteBtn').on('click', function(e)
     { 
       $('#addGroomerNoteform')[0].reset();
       $('#addGroomerNote').modal('show');
     });
   
     $('.addfileModalbtn').on('click', function(e)
     { 
   
       $('#addfileModalform')[0].reset();
       $('#addfileModal').modal('show');
     });
   
      
     
    $('#allappointments').show();
   $(document).on('change','.appintmentlisting', function(){
       var listingvalue = $('.appintmentlisting').val();
       if(listingvalue == 'groominglist'){
           $('#groominglist').show();
           $('#daycarelist').hide();
           $('#boardinglist').hide();
           $('#allappointments').hide();
            setTimeout(function(){ 
             $('#table2').DataTable({
             "responsive":true,
             "bInfo": false,
             "bAutoWidth": false,
             "bSort" : false,
             "paging": true,
             "destroy" : true
               }) 
              }, 300);
           
       }else if(listingvalue == 'daycarelist'){
           $('#daycarelist').show();
           $('#groominglist').hide();
           $('#boardinglist').hide();
           $('#allappointments').hide();
            setTimeout(function(){ 
             $('#table14').DataTable({
             "responsive":true,
             "bInfo": false,
             "bAutoWidth": false,
             "bSort" : false,
             "paging": true,
             "destroy" : true
               }) 
              }, 300);
       }else if(listingvalue == 'boardinglist'){
           $('#boardinglist').show();
           $('#groominglist').hide();
           $('#daycarelist').hide();
           $('#allappointments').hide();
            setTimeout(function(){ 
             $('#table20').DataTable({
             "responsive":true,
             "bInfo": false,
             "bAutoWidth": false,
             "bSort" : false,
             "paging": true,
             "destroy" : true
               }) 
              }, 300);
           
   
       }else{
           $('#allappointments').show();
           $('#groominglist').hide();
           $('#daycarelist').hide();
           $('#boardinglist').hide();
           setTimeout(function(){ 
             $('#tableappointment').DataTable({
             "responsive":true,
             "bInfo": false,
             "bAutoWidth": false,
             "bSort" : false,
             "paging": true,
             "destroy" : true
               }) 
              }, 300);
         }
   
    });
   
   
   $(document).on('click','.NoDownload', function(){
   
       $(document).find('#NoDownloadModal').modal('show');
   
   });
   // For print kennel END //
   
   $(document).on('click','.changeStatusButton',function(){
     $(document).find('#changeStatusModal .modal-body p').html($(this).attr('data-bodymsg'));
     $(document).find('#changeStatusModal .modal-title').html($(this).attr('data-title')); 
     $(document).find('#changeStatusModal .statusChangeModalButton').text($(this).attr('data-btntitle')); 
     $(document).find('#changeStatusModal .statusChangeModalButton').attr('href',$(this).attr('data-href')); 
   
   });
   
   $(document).on('click','.deleteFile',function(){
       $(document).find('#deletefileModal').find('.deleteFileBtn').attr('href',$(this).attr('data-url'));
   });
   $(function() {
        $("input:file").change(function (){
          var fileName = $(this).val();
          if(fileName.length>0){
               $('.fileuploadYn').text('File Selected');
               
          }else{
               $('.fileuploadYn').text('Select the file first.');
          }
          //$(".filename").html(fileName);
        });
     });
   
   $(document).on('click','.changePaymentStatusButton',function(){
   
     $(document).find('#paymentmodal .modal-body p').html($(this).attr('data-bodymsg'));
     $(document).find('#paymentmodal .modal-title').html($(this).attr('data-title')); 
     $(document).find('#paymentmodal .statusChangeModalButton').text($(this).attr('data-btntitle')); 
     $(document).find('#paymentmodal .statusChangeModalButton').attr('href',$(this).attr('data-href')); 
   
   });
   
   });
</script>
<!-- Delete Pet  Image Modal -->
<script type="text/javascript">
        function updateAllUnicodeIcon()
        {
           $(document).find('div.emoji').each(function(){
               console.log($(this).attr('data-emoji'));
               var name =  $(this).attr('data-name');
               var emoji = $(this).attr('data-emoji');
               if(emoji.length > 0)
               {
                   var html = unicodeToImage(emoji);
                   console.log(html);
                   $(this).html(name+'  '+html);
               }
           });
        }
   
   
        function  unicodeToImage (input) {
         if (!input) {
           return '';
         }
         if (!Config.rx_colons) {
           Config.init_unified();
         }
         return input.replace(Config.rx_codes, function(m) {
           var $img;
           if (m) {
             $img = $.emojiarea.createIcon($.emojiarea.icons[":"+Config.reversemap[m]+":"]);
             return $img;
           } else {
             return '';
           }
         });
       };
   
   
   $(document).ready(function(){
       updateAllUnicodeIcon();
   
       if($("textarea").length > 0){
           //$("textarea").height( $("textarea")[0].scrollHeight );
       } 
   });
</script>
<!-- New script start for add vaccination records -->
<script type="text/javascript">
   var global = 0;
   $('.take_click').click(function(){
     Webcam.snap( function(data_uri) {
       $(document).find('#myImageWebcam').html('<img src="'+data_uri+'"/>');
       $('.user-image1').find('.fileupload-new').find('img').prop('src', data_uri);
       $('input[name="avatar_data1"]').val(data_uri);
       $(document).find('#WebcamModal').modal('hide');
     } );
   });
   function take_snapshot() {
           Webcam.snap( function(data_uri) {
                $(document).find('#myImageWebcam').html('<img src="'+data_uri+'"/>');
           } );
   }
   $(document).ready(function(){
   
       $(document).find('.webcam').show();
   
       $(document).on('click', '.webcamModalButton', function() {
   
           navigator.getMedia = (navigator.getUserMedia || // use the proper vendor prefix
               navigator.webkitGetUserMedia ||
               navigator.mozGetUserMedia ||
               navigator.msGetUserMedia);
   
           navigator.getMedia({
               video: true
           }, function() {
               // webcam is available
               $(document).find('#changeStatusModalComplete').modal('hide');
               $(document).find('#WebcamModal').modal('show');
           }, function() {
               // webcam is not available
               //$(document).find('.webcam').hide();
   
               alert('Webcam not found or Please allow your webcam');
   
           });
   
           Webcam.set({
               width: 320,
               height: 240,
               dest_width: 640,
               dest_height: 480,
               image_format: 'jpeg',
               jpeg_quality: 90,
               force_flash: false
           });
           Webcam.attach('#my_result');
       });
       var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";
       var momentFormat = "<?php echo momentFormat(get_option('date_format'))?>";
       
   
       jQuery.validator.addMethod("greaterThan",
           function(value, element, params) {
               if ($(params).val().length > 0) {
                   if (!/Invalid|NaN/.test(new Date(value))) {
                       return new Date(value) > new Date($(params).val());
                   }
   
                   return isNaN(value) && isNaN($(params).val()) ||
                       (Number(value) > Number($(params).val()));
               } else {
                   return true;
               }
   
   
           }, 'Must be greater than date.');
   
   
       $(document).on('click', '.deleteButton1', function() {
           $(document).find('#deleteModal1').modal('show');
           $(document).find('.deleteButtonModel1').attr('href', $(this).attr('data-href'));
   
       });
   
   
   
    
         var runMaskInput = function () {
                $('.vet_phone_number,.vet_phone_number1').formatter({
                'pattern': '({{999}}) {{999}}-{{9999}}',
                'persistent': true
              });
            };
            runMaskInput();
   
       $('#datepicker').datepicker({
           autoclose: true,
           format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
       }).
       on('changeDate', function(ev) {
           //validate on change function
           if ($('#datepicker').valid()) {
               $('#datepicker').removeClass('invalid').addClass('success');
           }
       });
   
       $('#datepicker1').datepicker({
   
           autoclose: true,
           format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
       }).
       on('changeDate', function(ev) {
           //validate on change function
           if ($('#datepicker1').valid()) {
               $('#datepicker1').removeClass('invalid').addClass('success');
           }
       });
   
   
       
   
       $('.c_resize1').cropper({
           //aspectRatio: 1,
           minCropBoxWidth: 300,
           minCropBoxHeight: 300,
           crop: function(e) {
   
           }
       });
   
   
       var validate3 = $('#addMedication').validate({
           excluded: ':disabled',
           onfocusout: function(e) {
               this.element(e);
           },
           rules: {
               medication_name: {
                   required: true
               },
               date_of: {
                   //required : true
               },
               expiry_on: {
                   required: true,
                   greaterThan: '#datepicker',
   
               }
   
           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error err');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown error',
           errorPlacement: function(error, element) {
               if (element.parent('.input-group').length) {
                   error.insertAfter(element.parent());
               } else {
                   error.insertAfter(element);
               }
           }
       });
   
         $('#addGroomerNoteform').validate({
           excluded: ':disabled',
           onfocusout: function(e) {
               this.element(e);
           },
           rules: {
              
              
               petnote: {
                   required: true
                
   
               }
   
           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown error',
           errorPlacement: function(error, element) {
               if (element.parent('.input-group').length) {
                   error.insertAfter(element.parent());
               } else {
                   error.insertAfter(element);
               }
           }
       });
       
   
       $(document).on('click', '.addNoteButton', function() {
          $('#addMedication')[0].reset();
           $('#addModal').modal('show');
           // $(document).find('#addModal .fileupload').fileupload('clear');
           validate3.resetForm();
           $(document).find('#addModal form').attr('action', $(this).attr('data-action'));
          
           $(document).find('#addModal #formType').val($(this).attr('data-form'));
       });
   
   
   
       $('input[name="vaccine_image"]').click(function() {
           $(this).val(null);
           $('input[name="avatar_data1"]').val('');
       });
   
      $('input[name="vaccine_image"]').change(function(){
               $('input[name="avatar_data1"]').val('');
               //$('#addModal').modal('hide');
   
               if($(this)[0].files.length <= 0){
                       return;
                   }
                   var f = $(this)[0].files[0];
                   var file_Size = $(this)[0].files[0].size;
                   var reader = new FileReader();
                   
                   // Closure to capture the file information.
                 reader.onload = (function(file) {
                     return function(e) {
                           var image = document.createElement('img');
                           image.addEventListener('error', function() {
                                   $('#extensionModal1').modal('show');
                                   return;
                           });
                           image.addEventListener('load', function() {
                               if(image.width < 300 || image.height < 300){
                                 $(document).find('#checkImageValid').attr('disabled',true);
                                 $('.sizeErrMSg').html('Image size should be minimum 300px x 300px.');
                                   $('#sizeModal1').modal('show');
                                   return;
                               }
                               else
                               {
                                 $(document).find('#checkImageValid').attr('disabled',false);
                               }
                               if(file_Size  > 60000000){
                                 $(document).find('#checkImageValid').attr('disabled',true);
                                 $('.sizeErrMSg').html('The maximum size allowed is 60 MB');
                                 // $(document).find('#changeStatusModalComplete').modal('hide');
                                 $('#sizeModal').modal('show');
                                 return;
                               }
                               else
                               {
                                 $(document).find('#checkImageValid').attr('disabled',false);
                               }
   
                                   $('.user-image1 .fileupload-preview img').prop('src', e.target.result);
                                   $('input[name="avatar_data1"]').val(e.target.result);
                                   $('#addModal').modal('show');
                                   global++;
                           });
                         image.src = e.target.result;
                     }
                 })(f);
            
                   // Read in the image file as a data URL.
                   reader.readAsDataURL(f);
                   //$('#addModal').modal('show');
   
           });
   
       
       $(document).on('click','.deleteButton',function(){
         alert(global);
          if(global == 0)
          {
              $(document).find('#deleteModal').modal('show');
              $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
          }
          else
          {
              $('input[name="avatar_data1"]').val('');
              // $('.fileupload').fileupload('clear');
           }
       });
   
       
       // this is the id of the form
       $(document).on('submit', '#addMedication', function(e) {
           e.preventDefault();
           if ($(this).valid()) {
               var form = $(this);
               var url = form.attr('action');
                // alert(url);
               $.ajax({
                   type: "POST",
                   url: url,
                   dataType: 'json',
                   data: {
   
                       medication_name: function() {
                           return $('#addMedication').find('#medication_name').val();
   
                       },
                       date_of: function() {
                           return $('#addMedication').find('#datepicker').val();
                       },
                       expiry_on: function() {
                           return $('#addMedication').find('#datepicker1').val();
                       },
                       notes: function() {
                           return $('#addMedication').find('#notes').val();
                       },
                       vaccine_image: function() {
                           return $("#addMedication").find("input[name='vaccine_image']").val();
                       },
                       avatar_data1: function() {
                           return $('#addMedication').find("input[name='avatar_data1']").val();
                       },
                       formType: function() {
                           return $('#addMedication').find("#formType").val();
                       },
                   },
                   success: function(data) {
   
                       if (data.errcode == 0) {

                       }
                   }
               });
           }
       });
   
       $.validator.addMethod('filesize', function (value, element, param) {
           
           return this.optional(element) || (element.files[0].size <= param)
       }, 'File size must be less than 25 MB');
   

        $("#addfileModalform").validate({
				onfocusout: function(e) {
					this.element(e);						  
				},
				onkeyup: false,
			    rules: {
                    title: {
                        required: true,
                    },
                    file_name: {
                        required: true,
                        extension:'pdf,png,jpeg,jpg',
                        filesize: 25000000,
                    }
			    },
			    messages: {
                    title:{
                        required: "Enter a title.",
                    },
                    file_name:{
                        required: "Please select the file.",
                    }
			    },
				errorElement : 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
					  $(placement).append(error)
					} else {
						error.insertAfter(element);
					}
			    }
			});

   
       $('#table14').DataTable({
           //Feature control DataTables' server-
            "responsive":true,
           // "datatable-buttons": true,
           // "iDisplayLength": 10,
           "searching": true,
           "paging": true,
           "bInfo": false,
           "bAutoWidth": false,
           "bSort" : false,
           
           
           "language": {
               "emptyTable":"No Records Found"
           },
   
           "order": [], //Initial no order.
   
           // Load data for the table's content from an Ajax source
           
   
           //Set column definition initialisation properties.
           "columnDefs": [
               {
                   "targets": [ -1 ], //last column
                   "orderable": false, //set not orderable
               },
           ],
   
       });
       $('#table20').DataTable({
           //Feature control DataTables' server-
           "responsive":true,
           // "datatable-buttons": true,
           // "iDisplayLength": 10,
           "searching": true,
           "paging": true,
           "bInfo": false,
           "bAutoWidth": false,
           "bSort" : false,
           
           
           "language": {
               "emptyTable":"No Records Found"
           },
   
           "order": [], //Initial no order.
   
           // Load data for the table's content from an Ajax source
           
   
           //Set column definition initialisation properties.
           "columnDefs": [
               {
                   "targets": [ -1 ], //last column
                   "orderable": false, //set not orderable
               },
           ],
   
       });
   
       $('#tableappointment').DataTable({
           //Feature control DataTables' server-
            "responsive":true,
           // "datatable-buttons": true,
           // "iDisplayLength": 10,
           "searching": true,
           "paging": true,
           "bInfo": false,
           "bAutoWidth": false,
           "bSort" : false,
           "destroy" : true,
           
           "language": {
               "emptyTable":"No Records Found"
           },
   
           "order": [], //Initial no order.
   
           // Load data for the table's content from an Ajax source
           
   
           //Set column definition initialisation properties.
           "columnDefs": [
               {
                   "targets": [ -1 ], //last column
                   "orderable": false, //set not orderable
               },
           ],
   
       });
   
        if(window.location.href.indexOf("additional_notes") > -1 || (window.location.href.indexOf("vaccination_records") > -1) || (window.location.href.indexOf("extra_documents") > -1 || (window.location.href.indexOf("add_veterinarian") > -1))) 
         {
            $('.tabs a').removeClass('active');
            var url = $(location).attr('href'),
            parts = url.split("/"),
            last_part = parts[parts.length-1];
            if(last_part == 'additional_notes'){
               $('.tabs a[href="#3"]').addClass('active');
               $("#3").show();
            }
   
            if(last_part == 'vaccination_records')  
            {
               $('.tabs a[href="#4"]').addClass('active'); 
               $("#4").show();
            }
             if(last_part == 'extra_documents')  
            {
               $('.tabs a[href="#7"]').addClass('active'); 
               $("#7").show();
            }
             if(last_part == 'add_veterinarian')  
            {
               $('.tabs a[href="#2"]').addClass('active'); 
               $("#2").show();
            }
         }
   
       $('#editdatepicker').datepicker({
         autoclose: true,
         format: format
       }).
       on('changeDate', function(ev) {
         //validate on change function
         if($('#editdatepicker').valid()){
   
             //$('#editdatepicker1').val(ev.date);
               $('#editdatepicker').removeClass('invalid').addClass('success');   
         }
       });
   
       $('#editdatepicker1').datepicker({    
         autoclose: true,
         format: format
       }).
       on('changeDate', function(ev) {
         //validate on change function
         if($('#datepicker1').valid()){
           $('#datepicker1').removeClass('invalid').addClass('success');   
         }
       });
   
         $(document).on('click','.deleteButton1',function(){
           $(document).find('#deleteModal1').modal('show');
           $(document).find('.deleteButtonModel1').attr('href',$(this).attr('data-href'));
            $('.tabs a[href="#4"]').addClass('active'); 
               $("#4").show();
           // $('.nav-tabs a[href="#4"]').tab('show'); 
         });

         $(document).on('click','.editButton',function(){
         
         $(document).find('#editModal form').attr('action',$(this).attr('data-action'));
           var id = $(this).attr('data-id');
           $.ajax({
             url: BASE_URL+'User/getPetNotesAjax/'+id ,
             type: 'GET',
             dataType: 'json',
             success: function (data) {
             console.log(data.result);
             if(data.err == 0)
             { 
                $(document).find('#editModal').modal('show');
               $(document).find('#editModal #medication_name').val(data.result.medication_name);
               $(document).find('#editModal #notes').val(data.result.notes);
   
               var temp = moment(data.result.date_of);
               var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format(momentFormat);
               $(document).find("#editdatepicker").val("update", date);
               $(document).find("#editdatepicker").val(new Date());
               // alert($("#editdatepicker").val());
               
                var temp = moment(data.result.expiry_on);
                var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format(momentFormat);
                $(document).find("#editdatepicker1").val(date);
               $(document).find("#editdatepicker1").datepicker("update", date);
   
               
               
              
             }
             else
             {
               $(document).find('#editModal').modal('hide');
               console.log('something went wrong');
             }
                   
             },
             error : function(err)
             {
                       $(document).find('#editModal').modal('hide');
                       console.log(err);
             }
         });
       });
   
   
       $('#editMedication').validate({
         excluded: ':disabled',
           onfocusout: function(e) {
             this.element(e);                          
           },
           rules: {
             medication_name:{
               required : true
             },
             date_of:{
               //required : true
             },
             expiry_on:{
               required : true,
               greaterThan : '#editdatepicker',
             }
           },
           highlight: function(element) {
                   $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
                   $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown error',
           errorPlacement: function(error, element) {
                   if (element.parent('.input-group').length) {
                       error.insertAfter(element.parent());
                   } else {
                       error.insertAfter(element);
                   }
           }
       });
     
       $(document).on('change','#groomerType', function(){
           $('.petnote').val('');
           if($('#groomerType option:selected').val() == 'behavior_note'){
             var note = $(this).find(':selected').data('value');
             $('.petnote').val(note);
           }
       });
   
      
      
      $(".tabforpet").on("click",function(){
        var tableName = $(this).attr("href")+'table';
        setTimeout(function(){ 
            $(tableName).DataTable({
              "responsive":true,
              "bInfo": false,
              "bAutoWidth": false,
              "bSort" : false,
              "paging": false,
              "bFilter" : false,
              "destroy" : true
            });
        }, 400);
      });
   
   });
   
   
</script>
<!-- New script End for add vaccination records -->






<script>
jQuery(document).ready(function() {
      var BASE_URL = "<?php echo base_url();?>";
			$("#addMedication1").validate({
				onfocusout: function(e) {
					this.element(e);						  
				},
				onkeyup: false,
			    rules: {
					medication_name: {
	                   
	                    required: true
	                },
	                date_on: {
	                    
	                    required: true
                    },
                    expiry_on: {
	                   
                       required: true
                   },
                   notes: {
                       
                       required: true
                   }
			    },
			    messages: {
                    medication_name:{
			        required: "Enter a medication name.",
                  },
                  date_on:{
			        required: "Enter a date date.",
                  },
                  expiry_on:{
			        required: "Enter a expiry date.",
                  },
                  notes:{
			        required: "Enter a notes.",
			      },
			    },
				errorElement : 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
					  $(placement).append(error)
					} else {
						error.insertAfter(element);
					}
			    }
			});
        });
        
        $(document).ready(function() {
        $('#addRecord').dataTable({
            "responsive": true,
            "searching" : true,
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "order": [[ 0, 'desc' ]],
            "displayLength": 10,

        });
    });

    $(document).ready(function() {
        $('#addNote').dataTable({
            "responsive": true,
            "searching" : true,
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "order": [[ 0, 'desc' ]],
            "displayLength": 10,

        });
    });

    $(document).ready(function() {
        $('#fileList').dataTable({
            "responsive": true,
            "searching" : true,
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "order": [[ 0, 'desc' ]],
            "displayLength": 10,

        });
    });

    $(document).ready(function() {
        $('#contractList').dataTable({
            "responsive": true,
            "searching" : true,
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "order": [[ 0, 'desc' ]],
            "displayLength": 10,

        });
    });
	</script>























<!-- start vaccination cropper -->

<script> 

   $(document).ready(function(){

    //validate add pet form
      $('.c_resize').cropper({
           //aspectRatio: 1,
            minCropBoxWidth: 300,
            minCropBoxHeight: 300,
           crop: function(e) {
          
           }
       });                 
      
      


// add vaccine

    $('input[name="vaccine_image"]').click(function(){
            $(this).val(null);
        });
        
        $('input[name="vaccine_image"]').change(function(){
            if($(this)[0].files.length <= 0){
                return;
            }

            var f = $(this)[0].files[0];
            var reader = new FileReader();
                
            // Closure to capture the file information.
            reader.onload = (function(file) {
                return function(e) {
                    var image = document.createElement('img');
                        image.addEventListener('error', function() {
                                $('#extensionModal').modal('show');
                                return;
                        });
                        image.addEventListener('load', function() {
                            if(image.width < 150 || image.height < 150){
                                $('#sizeModal').modal('show');
                                return;
                            }
                            $('#cropModal').on('shown.bs.modal', function() {
                        $('.c_resize').cropper('replace', e.target.result);
                    });
                    $('.c_resize').prop('src', e.target.result);
                    $('#cropModal').modal('show');
                    });
                    image.src = e.target.result;
                }
            })(f);
        
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
        });
        
        $('.setDefaultImgBtn').click(function(){     
                var url = $('.user-image .fileupload-preview img').prop('src');
            $('input[name="engimage_data"]').val(url);
        });
        
        $('.cropSaveBtn').click(function(){
            var canvas = $('.c_resize').cropper('getCroppedCanvas');
            $('.resized').html(canvas);
            var url = $('.resized canvas')[0].toDataURL();
                
                $('.user-image .fileupload-preview img').prop('src', url);
                $('#cropModal').modal('hide');
            $('input[name="engimage_data"]').val(url);
        });

//  end add vaccine

   
   //delete image modal 
   var global = 0;
   $(document).on('click','.deleteButton',function(){
   
      if(global == 0)
      {  
         $(document).find("#deleteModal").modal('show');
         $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
      }
      else
      {
         $('input[name="avatar_data"]').val('');
         // $('.fileupload').fileupload('clear');
      }
   
   });


   $( function() {
       // run the currently selected effect
       function runEffect() {
         // get effect type from
         var selectedEffect = $( "#effectTypes" ).val();
    
         // Most effect types need no options passed by default
         var options = {};
   
         // Run the effect
         $( ".VaccinationContent" ).toggle( 'blind', options, 500 );
       };
    
       // Set effect from select menu value
       $(document).on('click','.addVaccination',function(){
        
         if($('.vaccination_hide').val() == 0){
            $('.vaccination_hide').val(1);
         }else{
            $('.vaccination_hide').val(0);
         }
         runEffect();
         
      });
   } );

  
   });
   
   
</script>
<!-- Pet Notes only for edit pet view -->
<script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" scr="https://momentjs.com/downloads/moment-timezone.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<style type="text/css">
   .datepicker-dropdown {
   z-index: 1500 !important;
   }
   .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
   color: #ccc ;
   }
</style>

<div class="modal fade" id='deleteModal1' role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Vaccination Note</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure you want to delete this Note?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href = '' class="btn btn-danger  deleteButtonModel1"><i class="fa fa-trash"></i> Delete</a>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id='WebcamModal' role="dialog">
<div class="modal-dialog">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Pic From Webcam</h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="" style="text-align: -webkit-center;">
               <div id="my_result"></div>
               <a href="javascript:void(take_snapshot)" class="btn btn-primary take_click">Take Snapshot</a>
            </div>
            <div class="">
               <div id="myImageWebcam" style="display: none;">
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-default" >Ok</button> -->
         </div>
      </div>
   </div>
</div>
<div id="sizeModal1" class="modal">
        <div class="modal-content">
          <h4>Size Error</h4>
          <p>Image size should atleast be 150x150.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-close btn btn-light-grey setDefaultImgBtn">
                Okay
            </button>
        </div>
    </div>

    <div id="extensionModal1" class="modal">
        <div class="modal-content">
          <h4>Extension Error</h4>
          <p>Please make sure you upload an image file.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-close btn btn-light-grey setDefaultImgBtn">
                Okay
            </button>
        </div>
    </div>
 
<script type="text/javascript">

   $('.take_click').click(function(){
   Webcam.snap( function(data_uri) {
     $(document).find('#myImageWebcam').html('<img src="'+data_uri+'"/>');
     $('.user-image1').find('.fileupload-new').find('img').prop('src', data_uri);
     $('input[name="avatar_data1"]').val(data_uri);
     $(document).find('#WebcamModal').modal('hide');
   } );
   });
   function take_snapshot() {
         Webcam.snap( function(data_uri) {
              $(document).find('#myImageWebcam').html('<img src="'+data_uri+'"/>');
         } );
   }
   $(document).ready(function(){
   $(document).find('.webcam').show();
   
   $(document).on('click','.webcamModalButton',function(){
   
     navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia ||
                        navigator.msGetUserMedia);
   
     navigator.getMedia({video: true}, function() {
       // webcam is available
       $(document).find('#changeStatusModalComplete').modal('hide');
       $(document).find('#WebcamModal').modal('show');
     }, function() {
       
   
       alert('Webcam not found or Please allow your webcam');
   
     });
   
     Webcam.set({
         width: 320,
         height: 240,
         dest_width: 640,
         dest_height: 480,
         image_format: 'jpeg',
         jpeg_quality: 90,
         force_flash: false
       });
     Webcam.attach( '#my_result' );
   });
   var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";  
   var momentFormat = "<?php echo momentFormat(get_option('date_format'))?>";
  
   jQuery.validator.addMethod("greaterThan", 
      function(value, element, params) {
         if($(params).val().length>0){
            if (!/Invalid|NaN/.test(new Date(value))) {
              return new Date(value) > new Date($(params).val());
            }
   
            return isNaN(value) && isNaN($(params).val()) 
              || (Number(value) > Number($(params).val()));
          }else{
            return true;
          }
          
   
      },'Must be greater than date.'); 
   
   
   $(document).on('click','.deleteButton1',function(){
      $(document).find('#deleteModal1').modal('show');
      $(document).find('.deleteButtonModel1').attr('href',$(this).attr('data-href'));
   });
   
   
   
   $('#datepicker').datepicker({ 
      autoclose: true,
      format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
   }).
   on('changeDate', function(ev) {
      //validate on change function
      if($('#datepicker').valid()){
               
         // var temp = moment(data.result.date_of);
         // var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format("YYYY-MM-DD");
         // $('#datepicker1').val(ev.date);
         $('#datepicker').removeClass('invalid').addClass('success');   
      }
   });
   
   $('#datepicker1').datepicker({   
      autoclose: true,
      format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
   }).
   on('changeDate', function(ev) {
               //validate on change function
      if($('#datepicker1').valid()){
         $('#datepicker1').removeClass('invalid').addClass('success');   
      }
   });
   
   
   $('#editdatepicker').datepicker({
      autoclose: true,
      format: format
   }).
   on('changeDate', function(ev) {
      //validate on change function
      if($('#editdatepicker').valid()){
   
           //$('#editdatepicker1').val(ev.date);
               $('#editdatepicker').removeClass('invalid').addClass('success');   
      }
   });
   
   $('#editdatepicker1').datepicker({     
      autoclose: true,
      format: format
   }).
   on('changeDate', function(ev) {
      //validate on change function
      if($('#datepicker1').valid()){
         $('#datepicker1').removeClass('invalid').addClass('success');   
      }
   });
   
 
   
   $(document).on('click','.addNoteButton',function(){
      // $(document).find('#addModal .fileupload').fileupload('clear');
      $('#addModal').modal('show');
   
      validate3.resetForm();
      $(document).find('#addModal form').attr('action',$(this).attr('data-action'));
      $(document).find('#addModal #formType').val($(this).attr('data-form'));
   }); 
   
   $(document).on('click','.deleteButton',function(){
       if(global == 0)
       {
           $(document).find('#deleteModal').modal('show');
           $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
       }
       else
       {
           $('input[name="avatar_data"]').val('');
           // $('.fileupload').fileupload('clear');
         }
      });
   
   
   });
</script>


