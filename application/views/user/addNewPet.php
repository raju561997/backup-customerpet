<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<style>
/* .img1{
    width:225px !important;
    height:225px !important;
} */

.table td {
    border-bottom: none !important;
}
.table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #e3ebf3 !important;
}
.app-content .wizard > .actions > ul > li > a {
    background: #605ca8;
    color: #fff;
    display: block;
    padding: 7px 12px;
    border-radius: 2px;
    border: 1px solid transparent;
}
.app-content .wizard > .steps > ul > li.done .step {
    background-color: #605ca8;
    border-color: #605ca8 !important;
    color: #fff;
}
.app-content .wizard > .steps > ul > li.current .step {
    border-color: #605ca8;
    background-color: #fff;
    color: #605ca8;
}
.input-container input {
    border: none;
    box-sizing: border-box;
    outline: 0;
    padding: .75rem;
    position: relative;
    width: 100%;
}

input[type="date"]::-webkit-calendar-picker-indicator {
    background: transparent;
    bottom: 0;
    color: transparent;
    cursor: pointer;
    height: auto;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: auto;
}
.custom-control-label:before{
  background-color:white;
  border: #605ca8 solid 1px !important;
}
td{
    font-size: 14px;
}
.days li .active {
  background: #605ca8 !important;
}
.custom-control-input:checked~.custom-control-label::before{
  background-color:#605ca8;
  border: #605ca8 solid 1px;
}
.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
    color: #fff;
    background-color: #605ca8;
}
.nav-pills .nav-link, .nav-pills .show > .nav-link {
    color: #2B335E;
}
img{
       width: 200px;
       height: 200px;
   }
.editButton:hover{
    background-color: #605ca8 !important;
    color: white !important;
}
@media only screen and (max-width: 280px) {
    img{
       width: 125px;
       height: auto;
    }
}
@media (min-width: 281px) and (max-width: 320px) {
    img{
       width: 160px;
       height: auto;
    }
}
@media (min-width: 321px) and (max-width: 360px) {
    img{
       width: 190px;
       height: auto;
    }
}
@media (min-width: 361px) and (max-width: 375px) {
    img{
       width: 200px;
       height: auto;
    }
}
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0 d-inline-block">Add Pet</h3>
            </div>
        </div>
        <div class="content-body">
            <!-- Form wizard with vertical tabs section start   steps-validation-->
            <section id="vertical-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="<?php echo base_url(); ?>User/addNewPetData" id="petdetails" class="vertical-tab-steps wizard-circle" method="POST">
                                    <input type="hidden" name="vacc_id[]" id="vacc_id" >

                                        <!-- Step 1 -->
                                        <h6>Step 1</h6>
                                        <fieldset class="mb-2" style="margin-top: 15px;">
                                            <div class="content-header-title mb-2 d-inline-block" style="font-weight: 500; color: #464855; font-size: 1.51rem;">Pet Profile
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="phoneNumber4"><?= lang('petCoverPhoto') ?> </label>
                                                        <?php $class='fileupload-new';?>
                                                        <div class="fileupload <?php echo $class;?>" data-provides="fileupload">
                                                            <div class="user-image">
                                                                <div class="fileupload-new thumbnail">
                                                                    <img src="<?php echo base_url();?>assets/default-p1.png" alt="" class="img1 img-responsive">
                                                                </div>
                                                                <div class="fileupload-preview fileupload-exists thumbnail">
                                                                    <img src="<?php echo base_url();?>assets/default-p1.png" alt="" class="img1 img-responsive">
                                                                </div>
                                                                <div class="user-image-buttons">
                                                                    <span class="btn btn-success btn-file btn-sm"><span class="fileupload-new" >Select image</span><span class="fileupload-exists"style="width:63px!important; float: left; margin-right:4px; margin-left:6px;">Change</span>
                                                                    <input type="file" name="pet_cover_photo" accept="image/png, image/jpeg,image/jpg">
                                                                    <input type="hidden" name="avatar_data">
                                                                    </span>
                                                                    <a href="javascript:void(0)" class="btn btn-danger fileupload-exists btn-sm" data-dismiss="fileupload"  style="float:left; margin-right:8px;width:97px!important;">
                                                                    Remove
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <label for="firstName4">Pet Name </label>
                                                        <input type="text" oninput="this.className = ''" name="name" class="form-control" id="pet_name">
                                                    </div>
                                                    <div class="row mt-3" style="margin-left: 0px;">
                                                        <div class="row form-group col-md-6">
                                                        <label style="margin-top:2%;" class="mr-2">Type :</label>
                                                        <ul class="form-group nav nav-pills nav-pill-bordered" style="float: right !important;">
                                                            <li class="nav-item">
                                                                <a class="nav-link TypeCat" id="home1-tab" data-toggle="pill" href="#home1" aria-expanded="true">Cat</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link TypeDog" id="profile1-tab" data-toggle="pill" href="#profile1" aria-expanded="false">Dog</a>
                                                            </li>
                                                        </ul>
                                                        <input id="type" name="type"  class="form-control typeInput" value="" type="hidden">
                                                        </div>
                                                        <div class="row form-group col-md-6 " style="margin-left: 22px;;">
                                                        <label style="margin-top:2%;" class="mr-2">Gender :</label>
                                                        <ul class="form-group nav nav-pills nav-pill-bordered" style="float: right;">
                                                            <li class="nav-item">
                                                                <a class="nav-link genderMale" id="home1-tab" data-toggle="pill" href="#home1" aria-expanded="true">Male</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link genderFemale" id="profile1-tab" data-toggle="pill" href="#profile1" aria-expanded="false">Female</a>
                                                            </li>
                                                        </ul>
                                                        <input id="gender" name="gender"  class="form-control genderInput" value="" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="emailAddress7">Weight </label>
                                                            <input type="text" id="weight" name="weight" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="phoneNumber4">Size </label>
                                                            <select class="custom-select form-control" name="size" id="eventType4" data-placeholder="Type to search cities" name="eventType4">
                                                                <option value="small">Small</option>
                                                                <option value="medium">Medium</option>
                                                                <option value="large">Large</option>
                                                                <option value="xl">Xl</option>
                                                                <option value="xxl">Xxl</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="date4">Date of Birth </label>
                                                            <input type="date" id="birth_date" name="birth_date" class="form-control">
                                                            
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="phoneNumber4">Color </label>
                                                            <input type="text" id="color" name="color" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="spayed" name="spayed[]">
                                                        <label class="custom-control-label" for="spayed">Spayed / Neutered </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="phoneNumber4">Notes From Customer </label>
                                                        <textarea type="text" name="description" id="description" rows="5" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- Step 2 -->
                                        <h6>Step 2</h6>
                                        <fieldset class="mb-2" style="margin-top: 15px;">
                                            <div class="content-header-title mb-2 d-inline-block" style="font-weight: 500; color: #464855; font-size: 1.51rem;">Veterinarian Details
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="proposalTitle4">Business Name </label>
                                                        <input type="text" name="vet_business_name" class="form-control" id="proposalTitle4">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress8">Veterinarian Name </label>
                                                        <input type="text" name="veterinarian" class="form-control" id="emailAddress8">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="videoUrl4">Primary Phone </label>
                                                        <input name="vet_phone_number" class="form-control vet_phone_number" type="text" value="" maxlength="10" size="10">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="videoUrl4">Secondary Phone </label>
                                                        <input name="vet_phone_number1" class="form-control vet_phone_number1" type="text" value="" maxlength="10" size="10">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jobTitle6">Fax </label>
                                                        <input type="text" name="fax" class="form-control" id="fax">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="shortDescription4">Address </label>
                                                        <textarea name="vet_address" type="text" id="vet_address" rows="5" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!-- Step 3 -->
                                        <h6>Step 3</h6>
                                        <fieldset class="mb-2" style="margin-top: 15px;">
                                            <div class="content-header-title mb-2 d-inline-block" style="font-weight: 500; color: #464855; font-size: 1.51rem;">Vaccinations Record
                                            </div>
                                            <div class="heading-elements" style="float: right;">
                                                <ul class="list-inline mb-0">
                                                    <li><a class="btn btn-sm box-shadow-2 round btn-min-width pull-right addNoteButton addVaccButton  text-white" style="background-color: #605ca8;" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> <?= lang('addNewNotes') ?></a></li>
                                                </ul>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-xl mb-0 vaccination-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="border-top-0">Vaccine Name</th>
                                                                            <th class="border-top-0">Date</th>
                                                                            <th class="border-top-0">Expires</th>
                                                                            <th class="border-top-0">Actions</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Form wizard with vertical tabs section end -->
        </div>
    </div>
</div>
<!-- END: Content-->


<!-- Modal -->
<div class="modal fade text-left" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add Vaccination Record</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addMedication1" method="post" action="">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name">Vaccine Name</label>
                        <input type="text" class="form-control" id="medication_name" name="medication_name" placeholder="Medication Name">
                        <input type="hidden" name="formType" class="form-control" value="add" id="formType">
                    </fieldset>
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                        <label for="date_of">Date</label>
                        <input id="date_of" name="date_of" class="form-control" type="date" placeholder="Date of">
                        </div>
                        <div class="form-group  col-md-6">
                        <label for="date_of">Expires on</label>
                        <input id="expiry_on" name="expiry_on" class="form-control" type="date" placeholder="Expiry on">
                        </div>
                    </div>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1">Notes</label>
                        <textarea class="form-control" id="notes" rows="5" name="notes" placeholder="Notes"></textarea>
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <span class="btn btn-rounded btn-success btn-file btn-sm mr-1" >
                            <span class="fileupload-new">Select File</span>                     
                            <input type="file" name="add_vaccination_file" id="vaccination_file"   accept="application/pdf,image/png, image/jpeg,image/jpg" aria-invalid="false"> 
                        </span>
                        <span class="fileerror"></span>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                   
                      <button class="btn btn-primary d-none submit_loader">
                          <i class="la la-spinner spinner"></i>
                     </button>
                    <input type="submit" id="submit_vaccine" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade text-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Delete Pet Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this pet image?. This will delete image permanently.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <a href="javascript:void(0)" class="btn btn-danger deleteButtonModel"><i class="la la-trash"></i> Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-fixed-footer" id='ResponseModal' >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <p></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="sizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Size Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Image size should atleast be 150x150.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Crop Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <img class="c_resize" src="" style="width: 100%;"  />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary cropSaveBtn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="WebcamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Pic From Webcam</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="" style="text-align: -webkit-center;">
                        <div id="my_result"></div>
                        <a href="javascript:void(take_snapshot)" class="btn btn-primary take_click">Take Snapshot</a>
                    </div>
                    <div class="">
                        <div id="myImageWebcam" style="display: none;">
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="extensionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Extension Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Please make sure you upload an image file.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="deleteVacModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Delete Vaccination Record </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="para">Are you sure you want to delete this record?</h5>
                <input type="hidden" id="delete_pets_notes" name="delete_pets_notes">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary d-none submit_loader_vacc" style="background-color: #605ca8;">
                          <i class="la la-spinner spinner"></i>
                </button>
                <button  id="btn_delete_Vacc" class="btn editButton text-white" style="background-color: #605ca8;" >Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="resized" style="display: none;"></div>


<script src="<?php echo base_url(); ?>assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/forms/wizard-steps.js"></script>
<!-- END: Page JS-->
<script type="text/javascript">
$(document).ready(function(){
    $('.vet_phone_number,.vet_phone_number1').focusout(function() {
        function phoneFormat() {
        phone = phone.replace(/[^0-9]/g, '');
        phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        return phone;
        }
        var phone = $(this).val();
        phone = phoneFormat(phone);
        $(this).val(phone);
    });

    $(document).on('click','.TypeDog',function(){
        $('.typeInput').val("Dog");
    });
    $(document).on('click','.TypeCat',function(){
        $('.typeInput').val("Cat");
    });

    $(document).on('click','.genderFemale',function(){
        $('.genderInput').val("Female")
    });
    $(document).on('click','.genderMale',function(){
        $('.genderInput').val("Male");
    });

    $('#vaccination_file').change(function(e){
        var fileName = e.target.files[0].name;
        $('.fileerror').html('The file "' + fileName +  '" has been selected.');
    });
});

$('input[name="pet_cover_photo"]').change(function(){
      if($(this)[0].files.length <= 0){
            return;
        }
        var f = $(this)[0].files[0];
        var reader = new FileReader();
        
        // Closure to capture the file information.
       reader.onload = (function(file) {
           return function(e) {
                $('.c_resize').cropper({
                    aspectRatio: 1,
                    minCropBoxWidth: 200,
                    minCropBoxHeight: 200,
                    crop: function(e) {
                    }
                });
                var image = document.createElement('img');
                image.addEventListener('error', function() {
                        $('#extensionModal').modal('show');
                        return;
                });
                image.addEventListener('load', function() {
                    if(image.width < 150 || image.height < 150){
                        $('#sizeModal').modal('show');
                        return;
                    }
                    $('#cropModal').on('shown.bs.modal', function() {
                   $('.c_resize').cropper('replace', e.target.result);
               });
               $('.c_resize').prop('src', e.target.result);
               $('#cropModal').modal('show');
               });
               image.src = e.target.result;
           }
      })(f);
   
         // Read in the image file as a data URL.
         reader.readAsDataURL(f);
   
   });
   
   $('.setDefaultImgBtn').click(function(){     
        var url = $('.user-image .fileupload-preview img').prop('src');
      $('input[name="engimage_data"]').val(url);
   });
   
   $('.cropSaveBtn').click(function(){
      var canvas = $('.c_resize').cropper('getCroppedCanvas');
      $('.resized').html(canvas);
      var url = $('.resized canvas')[0].toDataURL();
          
        $('.user-image .fileupload-preview img').prop('src', url);
        $('#cropModal').modal('hide');
      $('input[name="engimage_data"]').val(url);
   });

   //delete image modal 
   $(document).on('click','.deleteButton',function(){
   
      if(global == 0)
      {  
         $(document).find("#deleteModal").modal('show');
         $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
      }
      else
      {
         $('input[name="avatar_data"]').val('');
         // $('.fileupload').fileupload('clear');
      }
   
   });

    var vacc_id = [];
    jQuery(document).ready(function() {
        $("#addMedication1").validate({
            onfocusout: function(e) {
                this.element(e);
            },
            // onkeyup: true,
            rules: {
                medication_name : {
                    required : true
                },
                date_of : {
                    required : true
                },
                expiry_on : {
                    required: true,
                },
                notes : {
                    required: true,
                },
                
                // add_vaccination_file : {
                //     // required: true
                // },
            },
            messages : {
                medication_name : {
                    minlength : "Enter of medication name."
                },
                date_of : {
                    minlength : "Eneter date."
                },
                expiry_on : {
                    minlength : "Enter expiry date."
                },
                notes : {
                    remote : "Enter notes."
                },
                add_vaccination_file : {
                    remote : "Please select vaccination photo."
                },
            },
            submitHandler:function (form){
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'user/addVaccination',
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#submit_vaccine").hide();
                            $(".submit_loader").addClass('d-block');
                        },
                        success: function (response) {
                            console.log(response);
                            if(response.error === 0)
                            {
                               if(response.row)
                               {
                                  $('.vaccination-table tbody').prepend(response.row);
                               }
                               vacc_id.push(response.vaccine_pet_id);
                            //    $('#vacc_id').val(response.vaccine_pet_id);
                            //    var element = $('#vacc_id');
                            //    $('#vacc_id').value = JSON.stringify( ["Hello", "World"] );
                               $('#vacc_id').val(JSON.stringify( vacc_id ))
                               form.reset();
                               $('span.fileerror').html('');
                               toastr.success(response.msg);
                            }

                            if(response.error === 400)
                            {
                                 toastr.error(response.msg);
                            }
                           
                             $(".submit_loader").removeClass('d-block');
                             $("#submit_vaccine").show();
                             $('#addModal').modal('hide');
                            
                        }
                    });
                return false; 
            },
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
        });

        // pets notes delete
        $(document).on('click','.pets_notes_delete', function () {
                var id = $(this).data('pets_notes_id');
                $('#deleteVacModel').modal('show');
                $('#deleteVacModel #delete_pets_notes').val(id);
        });

        $(document).on('click','#btn_delete_Vacc', function () {
           var id =  $('#deleteVacModel #delete_pets_notes').val();
           $.ajax({
               type: "POST",
               url: BASE_URL+'User/delete_vaccination',
               data: {
                   id:id
               },
               beforeSend: function() {
                            $("#btn_delete_Vacc").hide();
                            $(".submit_loader_vacc").addClass('d-block');
               },
               dataType: "json",
               success: function (response) {
                   if(response.error === 0)
                   {
                      $(document).find('#pets_notes_'+response.pets_notes_id).parent().parent().hide();
                    //   console.log(vacc_id);
                    //   console.log(parseInt(response.pets_notes_id));
                       var index = vacc_id.indexOf(parseInt(response.pets_notes_id));
                       console.log(index);
                       if (index > -1) {
                        vacc_id.splice(index, 1);
                      }
                    //  console.log(vacc_id);
                    toastr.success(response.msg);
                    $('#vacc_id').val(JSON.stringify( vacc_id ))
                   }
                   $(".submit_loader_vacc").removeClass('d-block');
                   $("#btn_delete_Vacc").show();
                   $('#deleteVacModel').modal('hide');
                   
               }
           });
        });


        // petdetails
        $('#petdetails').submit(function (e) { 
            e.preventDefault();
        //   alert('sss');
        //     $('<input>').attr({
        //     type: 'hidden',
        //     id: 'foo',
        //     name: 'bar',
        //     value:vacc_id,
        // }).appendTo('#petdetails');
            // $(this).unbind('submit').submit();
        });


        $("#petdetails").validate({
            onfocusout: function(e) {
                this.element(e);
            },
            // onkeyup: true,
            rules: {
                name : {
                    required : true
                },
                date_of : {
                    required : true
                },
                expiry_on : {
                    required: true,
                },
                notes : {
                    required: true,
                }, 
                
                photo : {
                    required: false,
                    minlength:10
                },
            },
            messages : {
                medication_name : {
                    minlength : "Enter of medication name."
                },
                date_of : {
                    minlength : "Eneter date."
                },
                expiry_on : {
                    minlength : "Enter expiry date."
                },
                notes : {
                    remote : "Enter notes."
                },
                photo : {
                    remote : "Please select vaccination photo."
                },
            },
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
        });
    });
</script>



























