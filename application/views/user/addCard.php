
<style type="text/css">
   .error{
   color: #ff4081;
   font-size: .8rem;
   }
</style>

<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Add Card</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url() ;?>user/dashboard">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Add Card
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        
         <!-- END: Main Menu-->
        <div class="row match-height"  >
            <div class="col-md-12">
                <div class="card " style="min-height:500px;">
                    <div class="card-header">
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <?php if ($this->session->flashdata("success")): ?>
                        <div class="alert alert-success">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
                        </div>
                    <?php elseif ($this->session->flashdata("error")): ?>
                        <div class="alert alert-danger">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
                        </div>
                    <?php endif;?>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="changePasswordForm" id="addcardform" method="post" action="<?php echo base_url(); ?>user/addCard">
                                <div class="row justify-content-md-center">
                                    <div class="col-md-6">
                                        <div class="form-body">
                                            <input id="app_user_id" name="app_user_id" class="form-control" value="<?php echo $this->session->userdata('id'); ?>" type="hidden" >
                                            <div class="form-group">
                                                <label for="eventInput1">Card Holder Name</label>
                                                <input type="text" id="cardHolderName" class="form-control" placeholder="Card Holder Name" name="cardHolderName">
                                            </div>

                                            <div class="form-group">
                                                <label for="eventInput2">Card Number</label>
                                                <input type="text" id="cardNumber" class="form-control" placeholder="Card Number" name="cardNumber">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="eventInput2">Expiration Date</label>
                                                <input type="text" id="cardExpriry" class="form-control" placeholder="Expiration Date" name="cardExpriry">
                                            </div>

                                            <div class="form-group">
                                                <label for="eventInput2">CVV</label>
                                                <input type="text" id="cvv" class="form-control" placeholder="CVV" name="cvv">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class=" text-center">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <i class="la la-check-square-o"></i> Save Details
                                    </button>
                                </div> -->
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <i class="la la-check-square-o"></i> <?= lang('save') ?>
                                    </button>
                                    <a type="submit" href="<?php echo base_url('User/getAllCards') ?>" class="btn btn-outline-danger">
                                        <i class="fa fa-times"></i> Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </div>
    <!-- END: Content-->

    <script type="text/javascript">

      $(document).ready(function(){
        $('#addcardform').validate({
            onfocusout: function(e) {
                this.element(e);						  
            },
            onkeyup: false,
            rules: {
                cardHolderName: {
                    required: true
                },
                cardNumber: {
                    required: true
                },
                cardExpriry: {
                    required: true
                },
                cvv: {
                    required: true
                }
            },
            messages:{
                cardHolderName:{
                    remote:"Enter Card Holder Name."
                },
                cardNumber:{
                    remote:"Enter Card Number."
                },
                cardExpriry:{
                required:"Enter Card Expriry."
                },
                cvv:{
                required:"Enter CVV."
                }   
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    })
   </script>