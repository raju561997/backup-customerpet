<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewPet.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/js/gallery/photo-swipe/photoswipe.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/pages/gallery.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" scr="https://momentjs.com/downloads/moment-timezone.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<style>
#appointmentList_filter, #appointmentList_length, #appointmentList_info{
    display: none !important;
}

table.dataTable.no-footer {
    border-bottom: none!important;
}
.table td {
    padding: 0.75rem;
    vertical-align: top !important;
    border-top: 1px solid #e3ebf3 !important;
}
.table td {
    border-bottom: none !important;
}
.table thead th {
    vertical-align: bottom !important;
    border-bottom: 1px solid #e3ebf3 !important;
}
.table thead th {
    vertical-align: bottom !important;
    border-bottom: 1px solid #e3ebf3 !important;
}
.noteTable thead th {
    vertical-align: bottom !important;
    border-bottom: 2px solid #e3ebf3 !important;
}
#noteList, td {
    border-bottom: none !important;
}
.vaccineTable thead th {
    vertical-align: bottom !important;
    border-bottom: 2px solid #e3ebf3 !important;
}
/* Firefox old*/
@-moz-keyframes blink {
    0% {
        opacity:1;
    }
    50% {
        opacity:0;
    }
    100% {
        opacity:1;
    }
} 

@-webkit-keyframes blink {
    0% {
        opacity:1;
    }
    50% {
        opacity:0;
    }
    100% {
        opacity:1;
    }
}
/* IE */
@-ms-keyframes blink {
    0% {
        opacity:1;
    }
    50% {
        opacity:0;
    }
    100% {
        opacity:1;
    }
} 
/* Opera and prob css3 final iteration */
@keyframes blink {
    0% {
        opacity:1;
    }
    50% {
        opacity:0;
    }
    100% {
        opacity:1;
    }
} 
.blink-contract {
    -moz-animation: blink normal 2s infinite ease-in-out; /* Firefox */
    -webkit-animation: blink normal 2s infinite ease-in-out; /* Webkit */
    -ms-animation: blink normal 2s infinite ease-in-out; /* IE */
    animation: blink normal 2s infinite ease-in-out; /* Opera and prob css3 final iteration */
}
element.style {
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0) !important;
    text-anchor: middle !important;
    font: 0 15px "Open Sans" !important;
    fill: #6B6F82 !important;
}
</style>

    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-wrapper">
            <div class="content-header row">
                <app-breadcrumb class="col-12">
                    <div class="row">
                        <div class="content-header-left col-md-6 col-12 mb-2 ng-star-inserted">
                            <h3 class="content-header-title mb-0 d-inline-block">Pet profile</h3>
                        </div>
                        <div class="content-header-right col-md-6 col-12 mb-1">
                            <div class="d-inline-block float-md-right">
                                <a href="<?php echo base_url() ?>" class="btn btn-secondary btn-sm"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Kennel Card</a>
                                <!-- <a href="<?php echo base_url() ?>User/editPet" class="btn text-white editButton"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit
                                </a> -->
                                <a href="javascript:void(0);" type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-placement="top" data-container="body"
                                data-original-title="Book Now" data-content="Book Appointment via calling on below number."><i class="ft-phone-call"></i> Appointment</a>
                            </div>
                        </div>
                    </div>
                </app-breadcrumb>
            </div>
            
            <div class="content-body" >
                <section class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header pr-0">
                                <div class="row mt-1 col-md-12">
                                    <div class="col-md-2">
                                        <div class="petpic">
                                            <div class="pet-circle">
                                                <div class="pet-wrapper">
                                                <img class="img-profile" src="<?php echo base_url(); ?>assets/p.jpeg" />
                                                </div>
                                            </div>
                                        </div> 
                                    </div>

                                    <div class="col-md-8" style="padding-left: 0px;">
                                        <span class="breedtext" style="font-size:15pt;"><?php echo ucfirst($pet['name']);?></span>
                                        <span class="breedtext" style="font-size:10pt; margin-left:10px;"><?php echo ucfirst($pet['breed']);?></span>
                                        <p class="datapet" style="font-size:9pt;margin-top:10px;">Last appointment on <?php echo $lastAppointments;?></p>

                                        <?php if(!empty($appointmentExpiry)){ ?>
                                            <p class="datapet" style="font-size:9pt; margin-top:-10px;">Contract expires on <?php echo date("d/m/Y", strtotime($appointmentExpiry));?></p>
                                        <?php }else{ ?>
                                            <p class="datapet" style="font-size:9pt; margin-top:-10px;"><span class="blink-contract" style="color: red;">Contract expires on <?php echo date("d/m/Y", strtotime($appointmentExpiry));?></span></p>
                                        <?php } ?>

                                    </div>
                                    
                                    <div class="col-md-2" style="text-align: right !important;">
                                        <div id="pie-chart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-content" style="padding-top: 0px;" >
                                <div class="card-body">
                                    <ul class="product-tabs nav nav-tabs nav-underline no-hover-bg mb-2">
                                        <li class="nav-item">
                                            <a aria-controls="Overview" style="background-color: white;color:#333333;" aria-expanded="true" class="nav-link active" data-toggle="tab" href="#Overview" id="Overviews">
                                            Overview
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="veterinarian" style="background-color: #219427;color:white;" aria-expanded="true" class="nav-link" data-toggle="tab" href="#veterinarian" id="veterinarian_details">
                                            Veterinarian Details
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="notes" style="background-color: #60605B;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#notes" id="note">
                                            Additional Notes
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="vaccination" style="background-color: #f39c12;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#vaccination" id="vaccinations">
                                            Vaccination Records
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="appointment" style="background-color:#00c0ef ;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#appointment" id="appointments">
                                            Appointments
                                            </a>
                                        </li>
                                
                                        <li class="nav-item">
                                            <a aria-controls="Pet_gallery" style="background-color:#c33d49f2 ;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#Pet_gallery" id="Pet_gallerys">
                                            Pet Gallery
                                            </a>
                                        </li>
                                    </ul>
                                    
                                    <div class="product-content tab-content ">

                                        <div aria-expanded="true" aria-labelledby="Overviews" class="tab-pane active" id="Overview" role="tabpanel">
                                            <div class="media-list media-bordered">
                                        
                                            </div>
                                        </div>

                                        <div aria-labelledby="veterinarian_details" class="tab-pane" id="veterinarian">
                                            <form class='form-horizontal' id="addveterinarianform" method="post" action="<?php echo base_url()?>User/addveterinarian">
                                                <div class="veterinarianContent">
                                                    <input type="hidden" name="pet_id" value="<?php echo  $pet['id']?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Business Name</label>
                                                                <input name="vet_business_name" class="form-control vet_business_name" type="text" value="<?php echo $veterinarian['business_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Veterinarian Name</label>
                                                                <input name="veterinarian" class="form-control veterinarian" type="text" value="<?php echo $veterinarian['veterinarian']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Primary Phone</label>
                                                                <input name="vet_phone_number" class="form-control vet_phone_number" type="text" value="<?php echo format_telephone($veterinarian['phone']); ?>" maxlength="10" size="10">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Secondary Phone</label>
                                                                <input name="vet_phone_number1" class="form-control vet_phone_number1" type="text" value="<?php echo format_telephone($veterinarian['phone1']); ?>" maxlength="10" size="10">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Fax</label>
                                                                <input name="fax" class="form-control fax" type="text" value="<?php echo $veterinarian['fax']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventInput1">Address</label>
                                                                <textarea name="vet_address"  class="form-control vet_address" type="text" rows="5" col="10"><?php echo $veterinarian['address']; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="float: right;">
                                                        <button type="submit" class="btn text-white mb-1" id="btn" style="background-color:#605ca8" ><?= lang('save') ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div aria-labelledby="note" class="tab-pane" id="notes">
                                            <fieldset class="mb-2" style="margin-top: 15px;">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-content">
                                                                <div class="row form-group" style="float: right; margin-right:2px;">
                                                                    <a class="btn btn-sm box-shadow-2 round btn-min-width pull-right addNoteButton text-white" style="background-color: #605ca8;" data-toggle="modal" data-target="#addnote"><i class="fa fa-plus"></i> Add Note
                                                                    </a>
                                                                </div>
                                                                <div class="table-responsive">
                                                                <?php if(!empty($groomNotes) || !empty($medicationNotes) || !empty($petBehaviorNote['behavior_note']) ):?>
                                                                    <table id="noteList"  class="table noteTable table-hover table-xl mb-0" style="font-size: 14px;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th  class="border-top-0">Type</th>
                                                                                <th class="border-top-0" >Note</th>
                                                                                <th class="border-top-0">Created By</th>
                                                                                <th class="border-top-0">Added On</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php if(!empty($petBehaviorNote['behavior_note'])){ ?>
                                                                            <tr>
                                                                                <td class="text-truncate">Pet Behavior Note</td>
                                                                                <td class="text-truncate"> <?php echo $petBehaviorNote['behavior_note'];?></td>
                                                                                <td class="text-truncate"><?php echo $petBehaviorNote['username'];?></td>
                                                                                <td class="text-truncate"><?php echo date(get_option('date_format').' '.'h:i a',strtotime($petBehaviorNote['added_on']));?></td>
                                                                            </tr>  
                                                                        <?php  } ?>
                                                                        <?php if(!empty($groomNotes)){
                                                                            foreach ($groomNotes as $key => $value) {
                                                                            ?> 
                                                                            <tr>
                                                                                <td class="text-truncate"><?php echo ucwords($value['note_type']);?></td>
                                                                                <td class="text-truncate"> <?php echo $value['notes'];?></td>
                                                                                <td class="text-truncate"><?php echo $value['username'];?></td>
                                                                                <td class="text-truncate"><?php if(!empty($value['added_on'])):?>
                                                                                    <?php echo date(get_option('date_format').' '.'h:i a',strtotime($value['added_on']));?>
                                                                                    <?php endif;?>   
                                                                                </td>
                                                                            </tr>
                                                                        <?php } } ?>
                                                                        <?php if(!empty($medicationNotes)){
                                                                            foreach ($medicationNotes as $key => $value) {
                                                                            ?>
                                                                            <tr>
                                                                                <td class="text-truncate">Medication Notes</td>
                                                                                <td class="text-truncate"> <?php echo $value['notes'];?></td>
                                                                                <td class="text-truncate"><?php echo $value['username'];?></td>
                                                                                <td class="text-truncate"><?php if(!empty($value['added_on'])):?>
                                                                                    <?php echo date(get_option('date_format').' '.'h:i a',strtotime($value['added_on']));?>
                                                                                    <?php endif;?>
                                                                                </td>
                                                                            </tr> 
                                                                        <?php } } ?>
                                                                        </tbody>
                                                                    </table>
                                                                <?php endif;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div aria-labelledby="vaccinations" class="tab-pane" id="vaccination">
                                            <fieldset class="mb-2" style="margin-top: 15px;">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-content">
                                                                <div class="row form-group" style="float: right; margin-right:2px;">
                                                                    <a class="btn btn-sm box-shadow-2 round btn-min-width pull-right addNoteButton text-white" style="background-color: #605ca8;" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Add Record
                                                                    </a>
                                                                </div>
                                                                <div class="table-responsive">
                                                                    <table id="vaccine" class="table vaccineTable table-hover table-xl mb-0" style="font-size: 14px;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0">Sr. No.</th>
                                                                                <th class="border-top-0">Vaccine Name</th>
                                                                                <th class="border-top-0">Date</th>
                                                                                <th class="border-top-0">Expires on</th>
                                                                                <th class="border-top-0">Notes</th>
                                                                                <th class="border-top-0">Updated on</th>
                                                                                <th class="border-top-0">Updated by</th>
                                                                                <th class="border-top-0">Status</th>
                                                                                <th class="border-top-0">Actions</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php if(!empty($petNotes)): $i=0; foreach($petNotes as $item): $i++;?>
                                                                            <tr>
                                                                                <td class="text-truncate"><?php echo $i;?></td>
                                                                                <td class="text-truncate"><?php echo  ucfirst($item['medication_name']); ?></td>
                                                                                <td class="text-truncate"><?php echo  date(get_option('date_format'), strtotime($item['date_of'])); ?>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo  date(get_option('date_format'), strtotime($item['expiry_on'])); ?></td>
                                                                                <td class="text-truncate"><?php echo $item['notes']; ?></td>
                                                                                <td class="text-truncate"><?php echo  date(get_option('date_format').' '.'h:i a', strtotime($item['added_on'])); ?></td>
                                                                                <td class="text-truncate"><?php echo $item['username']; ?></td>
                                                                                <td class="text-truncate">
                                                                                <?php if($item['is_expired'] == 1 || time() > strtotime($item['expiry_on'])):?>
                                                                                    <div class="badge badge-danger badge-square " style="padding-top:7px"><span >Expired</span></div>
                                                                                <?php else:?>
                                                                                    <div class="badge badge-sucess badge-square " style="padding-top:7px"><span >Not Expired</span></div>
                                                                                <?php endif;?>  
                                                                                </td>
                                                                                <td class="text-truncate"><a data-toggle="modal" data-target="#editVaccine" data-id="<?php echo $item['id'];?>" 
                                                                                data-medication_name="<?php echo $item['medication_name'];?>" data-date_of="<?php echo $item['date_of'];?>" data-expiry_on="<?php echo $item['expiry_on'];?>" data-notes="<?php echo $item['notes'];?>"
                                                                                data-image="<?php echo $item['image'];?>" type="button" class="btn btn-sm round btn-outline-success actionBtn editVaccineBtn" >Edit</a>
                                                                                <a data-toggle="modal" data-target="#deleteNote" data-id="<?php echo $item['id'];?>" type="button" class="btn btn-sm round btn-outline-danger actionBtn delNoteBtn" >Delete</a>
                                                                                <a href="<?php echo base_url();?>assets/vaccination_record.png" type="button" class="btn btn-sm round btn-outline-danger editB actionBtn" style="border-color: #605ca8;background-color: transparent;color: #605ca8; ">View</a>
                                                                                </td>
                                                                            </tr>
                                                                        <?php endforeach;?>
                                                                        <?php endif; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        
                                        <div aria-labelledby="appointments" class="tab-pane" id="appointment">

                                            <div class="row">
                                                <div class="col-md-2 form-group" id="selectTriggerFilter">
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="appointmentList" class="table table-hover table-xl mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th class="border-top-0"></th>
                                                            <th class="border-top-0">Type</th>
                                                            <th class="border-top-0">Date</th>
                                                            <th class="border-top-0">Time</th>
                                                            <th class="border-top-0">Status</th>
                                                            <th class="border-top-0">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($appointments as $key => $value) { $i++?>
                                                        <tr>
                                                            <td class="text-truncate">
                                                                <ul class="list-unstyled users-list m-0">
                                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="<?php echo ucfirst($value['name']);?>" class="avatar avatar-sm pull-up">
                                                                        <img class="media-object rounded-circle" src="<?php echo base_url(); ?>assets/images/gallery/2.jpg" alt="Avatar">
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td class="text-truncate"><?php echo ucfirst($value['category']);?></td>
                                                            <td class="text-truncate"><?php echo date('M d Y', strtotime($value['appointment_date']));?></td>
                                                            <td class="text-truncate"><?php echo date('h:i a', strtotime($value['appointment_time']));?> - <?php echo date('h:i a', strtotime($value['appointment_end_time']));?></td>
                                                            <td class="text-truncate">
                                                            <?php if($value['status']=='Checkout'){?>
                                                                <div class="badge badge-danger badge-square " style="border-color: #605ca8; background-color: #605ca8;padding-top:7px;"><span ><?php echo 'Checked Out';?></span></div>
                                                            <?php }elseif($value['status']=='Cancel'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Cancelled';?></span></div>
                                                            <?php }elseif($value['status']=='Pending'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Pending';?></span></div>
                                                            <?php }elseif($value['status']=='Inprocess'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;background-color:#f39c12;"><span ><?php echo 'Inprocess';?></span></div>
                                                            <?php }elseif($value['status']=='Confirm'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;background-color:#4caf50;"><span ><?php echo 'Confirmed';?></span></div>
                                                            <?php }elseif($value['status']=='Complete'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;background-color:#00c0ef;"><span ><?php echo 'Completed';?></span></div>
                                                            <?php }?>
                                                            </td>
                                                            <td class="text-truncate">
                                                                <button type="button" class="btn btn-sm round btn-outline-danger editB" style="border-color: #605ca8;background-color: transparent;color: #605ca8; width:60px;" data-toggle="modal" data-target="#default">View</button>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php foreach ($boarding as $key => $value) { $i++?>
                                                        <tr>
                                                            <td class="text-truncate">
                                                                <ul class="list-unstyled users-list m-0">
                                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="<?php echo ucfirst($value['name']);?>" class="avatar avatar-sm pull-up">
                                                                        <img class="media-object rounded-circle" src="<?php echo base_url(); ?>assets/images/gallery/2.jpg" alt="Avatar">
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td class="text-truncate"><?php echo ucfirst($value['category']);?></td>
                                                            <td class="text-truncate"><?php echo date('M d Y', strtotime($value['start_date']));?></td>
                                                            <td class="text-truncate"><?php echo date('h:i a', strtotime($value['start_time']));?> - <?php echo date('h:i a', strtotime($value['end_time']));?>
                                                            </td>
                                                            <td class="text-truncate">
                                                            <?php if($value['status']=='Checkout'){?>
                                                                <div class="badge badge-danger badge-square " style="border-color: #605ca8; background-color: #605ca8;padding-top:7px;"><span ><?php echo 'Checked Out';?></span></div>
                                                            <?php }elseif($value['status']=='Cancel'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Cancelled';?></span></div>
                                                            <?php }elseif($value['status']=='Pending'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Pending';?></span></div>
                                                            <?php }elseif($value['status']=='Inprocess'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;background-color:#f39c12;"><span ><?php echo 'Inprocess';?></span></div>
                                                            <?php }elseif($value['status']=='Confirm'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px; background-color:#4caf50;"><span ><?php echo 'Confirmed';?></span></div>
                                                            <?php }elseif($value['status']=='Complete'){?>
                                                                    <div class="badge badge-danger badge-square" style="padding-top:7px;background-color:#00c0ef;"><span ><?php echo 'Completed';?></span></div>
                                                            <?php }?>
                                                            </td>
                                                            <td class="text-truncate">
                                                                <button type="button" class="btn btn-sm round btn-outline-danger editB" style="border-color: #605ca8;background-color: transparent;color: #605ca8; width:60px;" data-toggle="modal" data-target="#default">View</button>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div aria-labelledby="Pet_gallerys" class="tab-pane" id="Pet_gallery">
                                            <!-- Image grid -->
                                            <section id="image-gallery" class="card">
                                                <div class="card-content collapse show">

                                                    <div class="card-body  my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                                        <div class="row">
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/1.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/1.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/2.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/2.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/3.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/3.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/4.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/4.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                        </div>
                                                        <div class="row">
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/5.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/5.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/6.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/6.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/7.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/7.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                            <figure class="col-lg-3 col-md-6 col-12" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                                                <a href="<?php echo base_url(); ?>assets/images/gallery/8.jpg" itemprop="contentUrl" data-size="480x360">
                                                                    <img class="img-thumbnail img-fluid" src="<?php echo base_url(); ?>assets/images/gallery/8.jpg" itemprop="thumbnail" alt="Image description" />
                                                                </a>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                    <!--/ Image grid -->

                                                    <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                                    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                                                        <!-- Background of PhotoSwipe. 
                                                It's a separate element as animating opacity is faster than rgba(). -->
                                                        <div class="pswp__bg"></div>

                                                        <!-- Slides wrapper with overflow:hidden. -->
                                                        <div class="pswp__scroll-wrap">

                                                            <!-- Container that holds slides.PhotoSwipe keeps only 3 of them in the DOM to save memory.Don't modify these 3 pswp__item elements, data is added later on. -->
                                                            <div class="pswp__container">
                                                                <div class="pswp__item"></div>
                                                                <div class="pswp__item"></div>
                                                                <div class="pswp__item"></div>
                                                            </div>

                                                            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                                            <div class="pswp__ui pswp__ui--hidden">

                                                                <div class="pswp__top-bar">

                                                                    <!--  Controls are self-explanatory. Order can be changed. -->

                                                                    <div class="pswp__counter"></div>

                                                                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                                                    <button class="pswp__button pswp__button--share" title="Share"></button>

                                                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                                                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                                    <!-- element will get class pswp__preloader-active when preloader is running -->
                                                                    <div class="pswp__preloader">
                                                                        <div class="pswp__preloader__icn">
                                                                            <div class="pswp__preloader__cut">
                                                                                <div class="pswp__preloader__donut"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                                    <div class="pswp__share-tooltip"></div>
                                                                </div>

                                                                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                                </button>

                                                                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                                </button>

                                                                <div class="pswp__caption">
                                                                    <div class="pswp__caption__center"></div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/ PhotoSwipe -->
                                            </section>
                                            <!--/ Image grid -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


    <!-- Modal -->
<div class="modal fade text-left" id="addnote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add New Note</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addNote" method="post" action="<?php echo base_url()?>User/addGroomerNote">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="type">Note Type</label>
                        <select class="form-control" name="groomerType" id="groomerType">
                            <option value="general notes">General Note</option>
                            <option value="feeding notes">Feeding Notes</option>
                            <option value="groomer notes">Groomer Note</option>
                            <option value="medication notes">Medication Note</option>
                            <option value="behavior_note" data-value="Eats poop">Pet Behavioral Note</option>
                        </select>
                        <!-- <input type="hidden" name="type" class="form-control" value="add" id="type"> -->
                        <input type="hidden" name="pet_id" value="<?php echo  $pet['id'];?>">
                        <input type="hidden" name="user_id" value="<?php echo  $this->session->userdata('id');?>">
                    </fieldset>
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1">Note</label>
                        <textarea class="form-control" id="notes" rows="5" name="notes" placeholder="Note"></textarea>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-primary" style="background-color: #605ca8 !important; color:white;" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add Vaccination Record</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addMedication1" method="post" action="<?php echo base_url()?>User/addPetNotes">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name">Vaccine Name</label>
                        <input type="text" class="form-control" id="medication_name" name="medication_name" placeholder="Medication Name">
                        <input type="hidden" name="formType" class="form-control" value="add" id="formType">
                        <input type="hidden" name="pet_id" value="<?php echo  $pet['id'];?>">
                    </fieldset>
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                        <label for="date_of">Date</label>
                        <input id="date_of" name="date_of" class="form-control" type="date" placeholder="Date of">
                        </div>
                        <div class="form-group  col-md-6">
                        <label for="date_of">Expires on</label>
                        <input id="expiry_on" name="expiry_on" class="form-control" type="date" placeholder="Expiry on">
                        </div>
                    </div>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1">Notes</label>
                        <textarea class="form-control" id="notes" rows="5" name="notes" placeholder="Notes"></textarea>
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <span class="btn btn-rounded btn-success btn-file btn-sm mr-1" >
                            <span class="fileupload-new">Select File</span>                     
                            <input type="file" name="add_vaccination_file" id="vaccination_file"   accept="application/pdf,image/png, image/jpeg,image/jpg" aria-invalid="false"> 
                        </span>
                        <span class="fileerror"></span>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="editVaccine" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Edit Vaccination Record</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editMedication" method="post" action="<?php echo base_url()?>User/editPetNotes">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name">Vaccine Name</label>
                        <input type="text" class="form-control" id="medication_name1" name="medication_name" placeholder="Medication Name" >
                        <input type="hidden" name="formType" class="form-control" value="edit" id="formType">
                        <input type="hidden" name="pet_id" value="<?php echo  $pet['id'];?>">
                        <input type="hidden" name="id" class="form-control" id="edit_id">
                    </fieldset>
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                        <label for="date_of">Date</label>
                        <input id="date_of1" name="date_of" class="form-control" type="date" placeholder="Date of">
                        </div>
                        <div class="form-group  col-md-6">
                        <label for="date_of">Expires on</label>
                        <input id="expiry_on1" name="expiry_on" class="form-control" type="date" placeholder="Expiry on">
                        </div>
                    </div>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1">Notes</label>
                        <textarea class="form-control" id="notes1" rows="5" name="notes" placeholder="Notes"></textarea>
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <span class="btn btn-rounded btn-success btn-file btn-sm mr-1" >
                            <span class="fileupload-new">Select File</span>                     
                            <input type="file" name="edit_vaccination_file" id="vaccination_file1"   accept="application/pdf,image/png, image/jpeg,image/jpg" aria-invalid="false"> 
                        </span>
                        <span class="fileerror"></span>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Basic Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Check First Paragraph</h5>
                
                <hr>
                <h5>Some More Text</h5>
                <p>Cupcake sugar plum dessert tart powder chocolate fruitcake jelly. Tootsie roll bonbon toffee danish.
                    Biscuit sweet cake gummies danish. Tootsie roll cotton candy tiramisu lollipop candy cookie biscuit pie.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="col-lg-4 col-md-6 col-sm-12">
    <div class="modal animated slideInDown text-left" id="deleteNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title deleteTitle" id="myModalLabel77"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="para"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">NO</button>
                    <a type="button" id="btn_block_active" class="btn btn-outline-primary">YES</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->


<script src="<?php echo base_url() ;?>assets/js/jquery1.min.js"></script>
<script src="<?php echo base_url() ;?>assets/js/raphael-min.js"></script>
<script src="<?php echo base_url() ;?>assets/js/morris.min.js"></script>
<script src="<?php echo base_url() ;?>assets/vendors/js/gallery/photo-swipe/photoswipe.min.js"></script>
<script src="<?php echo base_url() ;?>assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js"></script>
<script src="<?php echo base_url() ;?>assets/js/scripts/gallery/photo-swipe/photoswipe-script.js"></script>
<script src="<?php echo base_url() ;?>assets/js/moment.js"></script>


<script>

$(document).on('click', '.delNoteBtn', function ()
{   
    var id=$(this).data('id');
    $(".deleteTitle").html("Delete Pet Vaccination Record");
    $("#para").html("Are you sure you want to delete this pet vaccination record?");
    $("#btn_block_active").attr("href",BASE_URL+"User/deletePetNotes?id="+id);
});

$(document).on('click', '.editVaccineBtn', function ()
{   
    var id=$(this).data('id');
    var medication_name = $(this).data('medication_name');
    var date_of = moment(moment($(this).data('date_of'), 'YYYY-MM-DD')).format('YYYY-MM-DD');
    var expiry_on = moment(moment($(this).data('expiry_on'), 'YYYY-MM-DD')).format('YYYY-MM-DD');
    var notes = $(this).data('notes');
    var image = $(this).data('image');

    $(".modal-title").html("Edit Vaccination Record");
    $(document).find("#medication_name1").val(medication_name);
    $(document).find("#date_of1").val(date_of);
    $(document).find("#expiry_on1").val(expiry_on);
    $(document).find("#notes1").val(notes);
    $(document).find("#vaccination_file1").val(image);
    $(document).find("#edit_id").val(id);
});

$('.vet_phone_number,.vet_phone_number1').focusout(function() {

    function phoneFormat() {
    phone = phone.replace(/[^0-9]/g, '');
    phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    return phone;
    }
    var phone = $(this).val();
    phone = phoneFormat(phone);
    $(this).val(phone);
});

$(document).ready(function() {

    var columnNo = 0;
    $('#appointmentList').DataTable( {
        "bSort" : false,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(columnNo == 1){
                    var select = $('<select class="form-control"><option value="">All Appointment</option></select>')
                    .appendTo( $('#selectTriggerFilter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                columnNo++;
            } );
        }
    } );
} );


$(document).ready(function(){
    $('#vaccination_file').change(function(e){
        var fileName = e.target.files[0].name;
        $('.fileerror').html('The file "' + fileName +  '" has been selected.');
    });
});

Morris.Donut({
  element: 'pie-chart',
  data: [
    {label: "Daycare",value: <?php echo $count_daycare;?>},
    {label: "Boarding", value: <?php echo $count_boarding;?>},
    {label: "Grooming", value: <?php echo $count_appointment;?>}
  ]
});  
// $("div svg tspan").attr("style", "font-family: arial");
jQuery(document).ready(function() {
    $("#addMedication1").validate({
        onfocusout: function(e) {
            this.element(e);
        },
        // onkeyup: true,
        rules: {
            medication_name : {
                required : true
            },
            date_of : {
                // required : true
            },
            expiry_on : {
                required: true,
            },
            notes : {
                // required: true,
            }, 
            
            add_vaccination_file : {
                // required: true
            },
        },
        messages : {
            medication_name : {
                minlength : "Enter of medication name."
            },
            date_of : {
                minlength : "Eneter date."
            },
            expiry_on : {
                minlength : "Enter expiry date."
            },
            notes : {
                remote : "Enter notes."
            },
            add_vaccination_file : {
                remote : "Please select vaccination photo."
            },
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
    });

    $(document).ready(function(){
        $('#vaccination_file1').change(function(e){
            var fileName = e.target.files[0].name;
            $('.fileerror').html('The file "' + fileName +  '" has been selected.');
        });
    });

    jQuery(document).ready(function() {
    $("#editMedication").validate({
        onfocusout: function(e) {
            this.element(e);
        },
        // onkeyup: true,
        rules: {
            medication_name1 : {
                required : true
            },
            date_of1 : {
                // required : true
            },
            expiry_on1 : {
                required: true,
            },
            notes1 : {
                // required: true,
            }, 
            
            edit_vaccination_file : {
                // required: true
            },
        },
        messages : {
            medication_name1 : {
                minlength : "Enter of medication name."
            },
            date_of1 : {
                minlength : "Eneter date."
            },
            expiry_on1 : {
                minlength : "Enter expiry date."
            },
            notes1 : {
                remote : "Enter notes."
            },
            edit_vaccination_file : {
                remote : "Please select vaccination photo."
            },
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
    });
});

    $("#addNote").validate({
        onfocusout: function(e) {
            this.element(e);
        },
        // onkeyup: true,
        rules: {
            notes : {
                required: true
            },
        },
        messages : {
            notes : {
                remote : "Please enter notes."
            },
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
    });
});

</script>











