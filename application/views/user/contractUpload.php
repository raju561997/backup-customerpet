    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Upload Contracts</h3>
                </div>
            </div>
            <div class="content-body">
            <div class="row" style=" margin-bottom:3%;">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body" >
                                <form class="form" id="contractUpload" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>User/downloadContract">
                                    <div class="row justify-content-md-center">
                                        <div class="col-md-12">
                                            <div class="form-body">
                                                <div class="form-group mt-2">
                                                    <input type="hidden" id="id" class="form-control" name="pet_id[]" value="<?php echo ucfirst($pet['id']);?>">
                                                    <input type="hidden" id="allow_type" class="form-control" name="allow_type" value="<?php echo $contract['allow_type'];?>">
                                                    
                                                    <label for="eventInput1">Contract for <?php echo ucfirst($pet['name']);?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <?php echo $contract['content'];?>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="d-inline-block custom-control custom-checkbox mr-1">
                                                            <input type="checkbox" name="tnc[]" class="custom-control-input" id="agree" value="agree">
                                                            <label class="custom-control-label" for="agree">I agree to terms and conditions</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div>
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-danger mr-1">
                                            Cancel
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                            Upload
                                        </button>
                                    </div>
                                    
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->



<script>
    $(document).ready(function(){
        $('#contractUpload').validate({
            onfocusout: function(e) {
                this.element(e);						  
            },
            onkeyup: false,
            rules: {
                  type : {
                     required: true
                  },
                  
                  signdate : {
                     required:true
                  },
                  'tnc[]':{
                     required:true
                  },
            },
            messages:{
                type : {
                    remote : "Please select contract"
                  },
                  signdate : {
                    remote : "Please select signed date"
                  },
                  'tnc[]' : {
                    remote : "Please accept terms"
                  },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    })
</script>