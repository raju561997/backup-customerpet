<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" src="<?php echo base_url().'assets/css/signature-pad.css';?>">
  
<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-39365077-1']);
   _gaq.push(['_trackPageview']);
   
   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
</script>
<style type="text/css">
   .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
   color: #ccc ;
   }
   .addPhone{
   margin: 5px;
   }
   .removePhone{
   color: red;
   padding-top: 53px;
   }
   #addsign{
       width: 50%!important;
       margin-top: 15%!important;
       margin-left: 29%!important;
   }
   canvas{
    margin-left: 25%!important;
   }
   h4.modal-title{
    margin-left: 30%!important;
   }
   #signBut{
    margin-left: 35%!important;
    padding-bottom: 3%;
   }
   img{
       width: 120px;
       height: auto;
   }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<style type="text/css">
   .error{
   color: #ff4081;
   font-size: .8rem;
   }
   .checkboxspan{
   display: unset !important;
   font-size: large  !important;
   }
   input[type=date]::-webkit-inner-spin-button {
   -webkit-appearance: none;
   display: none;
   }
   .error{
   color: #ff4081;
   font-size: .8rem;
   }
</style>

<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0 d-inline-block">Edit Profile</h3>
          </div>
        </div>
        
         <!-- END: Main Menu-->
        <div class="row match-height"  >
            <div class="col-md-12">
                <div class="card " style="min-height:500px;">
                    <?php if ($this->session->flashdata("success")): ?>
                        <div class="alert alert-success">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
                        </div>
                    <?php elseif ($this->session->flashdata("error")): ?>
                        <div class="alert alert-danger">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
                        </div>
                    <?php endif;?>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form-horizontal" id="editProfile" method="post" action="<?php echo base_url(); ?>User/editProfile" enctype="multipart/form-data">
                                <div class="row justify-content-md-center">
                                    <div class="col-md-12">
                                        <div class="form-body">
                                            <input id="app_user_id" name="app_user_id" class="form-control" value="<?php echo $this->session->userdata('id'); ?>" type="hidden" >
                                            <input id="id" name="id" class="form-control" value="<?php echo $pet['id']; ?>" type="hidden" > 
                                            <div class="form-group">
                                                <label><?= lang('profile') ?></label>
                                                <div class="col-sm-5">
                                                <?php 
                                                    $class='fileupload-new';
                                                    ?>
                                                <div class="fileupload <?php echo $class;?>" data-provides="fileupload">
                                                    <div class="user-image">
                                                        <div class="fileupload-new thumbnail " style="">
                                                            <img src="<?php echo base_url();?>assets/default-u.jpg" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail">
                                                            <img src="<?php echo base_url();?>assets/default-u.jpg" alt="">
                                                        </div>
                                                        <div class="user-image-buttons">
                                                            <span class="btn btn-rounded btn-success btn-file btn-small"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                                            <input type="file" name="employee_photo" accept="image/png, image/jpeg,image/jpg">
                                                            <input type="hidden" name="avatar_data">
                                                            </span>
                                                            <a href="javascript:void(0)" class="btn btn-rounded btn-danger fileupload-exists btn-bricky btn-small" data-dismiss="fileupload" >
                                                            Remove
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="eventInput1"><?= lang('username') ?></label>
                                                <input type="text" id="username" value="<?php  echo $app_users['username'] ?>" class="form-control" name="username">
                                            </div>

                                            <div class="form-group">
                                                <label for="eventInput2"><?= lang('email') ?></label>
                                                <input type="email" id="email" value="<?php  echo $app_users['email'] ?>" class="form-control"  name="email">
                                            </div>
                                            
                                            <div class="row">
                                                <div class="form-group col-md-7">
                                                    <label for="eventInput2">Primary Phone</label>
                                                    <input type="text" id="phone" value="<?php echo format_telephone($app_users['phone']); ?>" class="form-control" placeholder="Confirm Password" name="phone">
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <a href="#" data-toggle="modal" class="btn btn-small btn-primary addPhone" style="margin-top: 1.8rem !important;background-color: #605ca8 !important;" ><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Phone</a>
                                                </div>
                                            </div>

                                            <div class="form-group numbers_type">
                                                <div class="input-group">
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="number_type" value="Home" <?php if($app_users['number_type'] == 'Home') { echo "checked='checked'"; }?>  class="custom-control-input number_type" id="Home">
                                                        <label class="custom-control-label" for="Home">Home</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="number_type" value="Work" <?php if($app_users['number_type'] == 'Work') { echo "checked='checked'"; }?> class="custom-control-input number_type" id="Work">
                                                        <label class="custom-control-label" for="Work">Work</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="number_type" value="Cell" <?php if($app_users['number_type'] == 'Cell') { echo "checked='checked'"; }?> class="custom-control-input number_type" id="Cell">
                                                        <label class="custom-control-label" for="Cell">Cell</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio">
                                                        <input type="radio" name="number_type" value="Other"  <?php if($app_users['number_type'] == 'Other') { echo "checked='checked'"; }?>   class="custom-control-input number_type" id="Other">
                                                        <label class="custom-control-label" for="Other">Other</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if(!empty($app_users['additional_Phone'])){
                                                $additional_Phones = unserialize($app_users['additional_Phone']);
                                                $i = 100;
                                                foreach ($additional_Phones as $key => $additional_Phone) {      
                                                
                                            ?>
                                        <div>
                                            <div class="row">
                                                <div class="form-group col-md-7">
                                                    <label for="eventInput2">Phone</label>
                                                    <input type="text" value="<?php echo $additional_Phone[0]; ?>" class="form-control phone1"  name="phone1[]">
                                                </div>

                                                <div class="form-group col-md-5" style="margin-top: -1.2% !important;">
                                                    <i class="fas fa-minus-circle removePhone"></i>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="addtional_number_type1[<?php echo $i ;?>]" class="addtional_number_type1 custom-control-input" value="Home"  <?php if($additional_Phone[1] == 'Home') { echo "checked='checked'"; }?> id="Home1">
                                                        <label class="custom-control-label" for="Home1" >Home</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="addtional_number_type1[<?php echo $i ;?>]" class="addtional_number_type1 custom-control-input" value="Work"  <?php if($additional_Phone[1] == 'Work') { echo "checked='checked'"; }?> id="Work1">
                                                        <label class="custom-control-label" for="Work1">Work</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                                        <input type="radio" name="addtional_number_type1[<?php echo $i ;?>]" class="addtional_number_type1 custom-control-input" value="Cell" <?php if($additional_Phone[1] == 'Cell') { echo "checked='checked'"; }?> id="Cell1">
                                                        <label class="custom-control-label" for="Cell1">Cell</label>
                                                    </div>
                                                    <div class="d-inline-block custom-control custom-radio">
                                                        <input type="radio" name="addtional_number_type1[<?php echo $i ;?>]" class="addtional_number_type1 custom-control-input" value="Other"  <?php if($additional_Phone[1] == 'Other') { echo "checked='checked'"; }?> id="Other1">
                                                        <label class="custom-control-label" for="Other1">Other</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                            
                                 <?php $i++; } ?>
                                 <?php } ?>
                                 <?php if(!empty($arrDiscountActive)){?>
                                 <div class="form-group ">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    Discount
                                    </span>
                                    <div class="col-sm-5">
                                       <select class="form-control editPage" disabled="disabled" name="discount" id="discount">
                                          <?php $discountMsg = ''; foreach($arrDiscount as $row){ ?>
                                          <option value="<?php echo $row['id']; ?>" 
                                             data-per="<?php echo $row['discount_percentage']; ?>" 
                                             data-startdate="<?php echo date(get_option('date_format'), strtotime($row['start_date_time'])); ?>" 
                                             data-enddate="<?php echo date(get_option('date_format'), strtotime($row['end_date_time'])); ?>" <?php  if($app_users['active_discount_id'] == $row['id']){ echo"Selected";
                                                $discountMsg = 'This will apply '.$row['discount_percentage'].'% discount from '.date(get_option('date_format'), strtotime($row['start_date_time'])).' to '.date(get_option('date_format'), strtotime($row['end_date_time']));
                                                
                                                  } ?> ><?php echo $row['discount_title'].' ( '.$row['discount_percentage'].'% ) '; ?></option>
                                          <?php } ?>
                                       </select>
                                       <span class="discountMsg"><?php echo $discountMsg; ?></span>
                                    </div>
                                 </div>
                                 <?php } ?>
                                 <?php if(isset($app_users['is_deposited']) && !empty($app_users['is_deposited'])):?>
                                 <div class="form-group ">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    Required Deposit
                                    </span>
                                    <label>
                                    <input type="checkbox" disabled="disabled" class="grey DepositCheck" id="DepositCheck" name="DepositCheck"   value="1" checked='checked'>
                                    <span class="checkboxspan"></span>
                                    </label>
                                 </div>
                                 <div class="form-group  depositnote">
                                    <span for="field-1" class="col-sm-3 control-label">
                                    Deposit Note
                                    </span>
                                    <div class="col-sm-5">
                                       <textarea type="text" class="form-control" style="overflow:hidden;resize:none;" name="deposit_note" disabled="disabled" rows="3" maxlength="120"> <?php echo $app_users['deposit_note']?></textarea>
                                    </div>
                                 </div>
                                 <?php endif;?>
                                 <input type="hidden" class="active_discount_id_edit" value="<?php echo $app_users['active_discount_id'] ?>">
                                 <input type="hidden" class="user_notification_body" value="" name="notification_body">
                                 <input type="hidden" class="user_email_body" value="" name="email_body">

                                 <div class="form-group">
                                    <label>Gender</label>
									<div class="input-group">
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" name="gender" value="Male" <?php if($app_users['gender'] == 'Male') { echo "checked='checked'"; }?>  class="custom-control-input" id="Male">
                                            <label class="custom-control-label" for="Male">Male</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio">
                                            <input type="radio" name="gender" value="Female" <?php if($app_users['gender'] == 'Female') { echo "checked='checked'"; }?>  class="custom-control-input" id="Female">
                                            <label class="custom-control-label" for="Female">Female</label>
                                        </div>
                                    </div>
                                 </div>

                                 <div class="form-group">
                                    <label>Date of birth</label>
                                    <div>
                                        <input id="dob" name="dob" class="form-control" type="date" >
                                        <input id="dobhidden" name="dobhidden" class="form-control" type="hidden" value="<?php  echo $app_users['dob'] ?>">
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label><?= lang('address') ?></label>
                                    <input id="address" name="address" class="form-control" value="<?php  echo $app_users['address'] ?>" type="text" >
                                 </div>
                                 <div class="form-group margin">
                                    <label><?= lang('city') ?></label>
                                    <input id="city" name="city" class="form-control" value="<?php  echo $app_users['city'] ?>" type="text" >
                                 </div>
                                 <div class="form-group">
                                    <label><?= lang('state') ?></label>
                                    <input id="state" name="state" class="form-control" value="<?php  echo $app_users['state'] ?>" type="text" > 
                                 </div>
                                 <div class="form-group">
                                    <label><?= lang('postal_code') ?></label>
                                    <input id="postal_code" name="postal_code" class="form-control" value="<?php  echo $app_users['postal_code'] ?>" type="text" >
                                 </div>
                                 <div class="form-group">
                                    <label>Emergency Contact</label>
                                    <input id="emergency_contact_name" name="emergency_contact_name" class="form-control" value="<?php  echo $app_users['emergency_contact_name'] ?>" type="text" >   
                                 </div>
                                 <div class="form-group">
                                    <label>Emergency Phone</label>
                                    <input id="emergency_contact_phone_number" name="emergency_contact_phone_number" class="form-control" value="<?php  echo $app_users['emergency_contact_phone_number'] ?>" type="text" >
                                 </div>

                                <input type="hidden" name="id" value="<?php if(!empty($pet)) echo $pet['id'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class=" text-right">
                                    <button type="submit" class="btn btn-primary" style="background-color: #605ca8 !important;">
                                        <?= lang('save') ?>
                                    </button>
                                    <a type="submit" href="<?php echo base_url('User/dashboard') ?>" class="btn btn-danger">
                                        Cancel
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </div>
    <!-- END: Content-->


<div id="addsign" class="modal modal-fixed-footer" >
   <form role="form" class="form-horizontal addservices" id="addservicesform" method="post" action="<?php echo base_url()?>User/addservicestoinvoice">
      <div class="modal-content">
         <h4 class="modal-title">Upload Customer Signature</h4>
         <div class="row">
            <div class="input-field col s12">
               <body onselectstart="return false">
               <div id="signature-pad" class="signature-pad">
                  <div class="signature-pad--body">
                     <canvas style="border:1px solid black;"></canvas>
                  </div>
                  <div class="signature-pad--footer">
                     <div class="signature-pad--actions">
                        <div id="signBut">
                           <button type="button" class="button clear" data-action="clear">Clear</button>
                           <button type="button" class="button" data-action="undo">Undo</button>
                           <button type="button" data-cust-id="<?php echo $this->session->userdata('id'); ?>" class="button  savesign"  data-action="save-jpg">Upload</button>
                        </div>
                       
                     </div>
                  </div>
               </div>
            </body>
            </div>
         </div>
      </div>
   </form>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Delete Pet Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this pet image?. This will delete image permanently.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <a href="javascript:void(0)" class="btn btn-danger deleteButtonModel"><i class="la la-trash"></i> Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-fixed-footer" id='ResponseModal' >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <p></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="sizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Size Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Image size should atleast be 150x150.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Crop Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <img class="c_resize" src="" style="width: 100%;"  />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary cropSaveBtn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="WebcamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Pic From Webcam</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="" style="text-align: -webkit-center;">
                        <div id="my_result"></div>
                        <a href="javascript:void(take_snapshot)" class="btn btn-primary take_click">Take Snapshot</a>
                    </div>
                    <div class="">
                        <div id="myImageWebcam" style="display: none;">
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="extensionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Extension Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Please make sure you upload an image file.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="resized" style="display: none;"></div>

<div id="notifyDiscountPage" class="modal fade" data-backdrop="static" tabindex="-1" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close closeNotifyModal" data-dismiss="modal" aria-hidden="true">
            &times;
            </button>
            <h4 class="modal-title">Notify Customer</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align: left;">
                           Email Template
                           <span class="required">
                              <!-- * -->
                           </span>
                        </label>
                        <div class="col-sm-8">
                           <select class="form-control email_template_id" name="email_template_id" id="email_template_id">
                              <option selected="selected" value=''>Select Email Template</option>
                              <?php foreach ($arrEmailTemplate as $key => $value) { ?>
                              <option value='<?php echo $value['id']; ?>'><?php echo $value['email_title']; ?>
                              </option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <br>
               <br>
               <div class="col-md-12" style=" margin-top: 20px;
                  ">
                  <div class="col-md-12 col-sm-12 col-xs-12 ">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align: left;">
                           Notification Text
                           <span class="required">
                              <!-- * -->
                           </span>
                        </label>
                        <div class="col-sm-8">
                           <textarea id="notification_body" placeholder="Enter notification text for mobile..." name="notification_body1" class="form-control notification_body_edit" col="8" rows="5"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default closeNotifyModal" data-dismiss="modal">Close</button>
            <button type="button" data-dismiss="modal" class="saveNotifyData btn btn-primary">
            Ok
            </button>
         </div>
      </div>
   </div>
</div>

<script  src="<?php echo base_url().'assets/js/signature_pad.umd.js';?>"></script>
<script  src="<?php echo base_url().'assets/js/core/app.js';?>"></script>

<script type="text/javascript">
   var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";  
   
   var global = 0;
   
   $(document).ready(function(){
   
          setTimeout(function(){ window.scrollTo(0,0); }, 500);
            var runMaskInput = function () {
                $('#phone,#emergency_contact_phone_number').formatter({
                'pattern': '({{999}}) {{999}}-{{9999}}',
                'persistent': true
              });
            };
            runMaskInput();
         
         
   
         $( ".editPage" ).change(function() {
           var discount_id = $(this).val();
          
           var active_discount_id_edit = $('.active_discount_id_edit').val();
           if(discount_id !='' && discount_id > 0){
             $('.discountMsg').html('This will apply '+$(this).find(':selected').attr('data-per')+'% discount from '+$(this).find(':selected').attr('data-startdate')+' to '+$(this).find(':selected').attr('data-enddate'));
             $('.discountMsg').show();
             if(active_discount_id_edit != discount_id){
                 $('#notifyDiscountPage').modal('show');
             }
           }else{
             $('.discountMsg').hide();
           }
         });
        $(document).on('click','.DepositCheck',function(){
         
           var checked = this.checked;
             if (checked) {
   
               $('.DepositCheckval').val('1'); 
               $('.depositnote').show();
             } else {
                $('.DepositCheckval').val('0');  
                $('.depositnote').hide();  
             }
         });    
       
   
   
        
   
       $(document).on('click','.saveNotifyData',function(){
         $('.user_notification_body').val($('.notification_body_edit').val());
         $('.user_email_body').val($('.email_template_id').val());
       });
   
       $(document).on('click','.closeNotifyModal',function(){
         $('.user_notification_body').val('');
         $('.user_email_body').val('');
       });
   
       // Script used for add multiple phone number dynamically //
       var Phone_count = 1;
       var Phone_count_type = 0;
       $(document).on('click','.addPhone',function(){
        var html ='';

        html+=  '<div>'+
                    '<div class="row">'+
                        '<div class="form-group col-md-7">'+
                            '<label for="eventInput2">Phone</label>'+
                            '<input type="text" class="form-control phone1"  name="phone1['+Phone_count_type+']">'+
                        '</div>'+

                        '<div class="form-group col-md-5" style="margin-top: -1.2% !important;">'+
                            '<i class="fas fa-minus-circle removePhone"></i>'+
                        '</div>'+
                    '</div>'+

                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<div class="d-inline-block custom-control custom-radio mr-1">'+
                                '<input name="addtional_number_type1['+Phone_count_type+']" class="addtional_number_type1 custom-control-input" value="Home" checked="checked"  type="radio">'+
                                '<label class="custom-control-label" >Home</label>'+
                            '</div>'+
                            '<div class="d-inline-block custom-control custom-radio mr-1">'+
                                '<input name="addtional_number_type1['+Phone_count_type+']" class="addtional_number_type1 custom-control-input" value="Work" checked="checked"  type="radio">'+
                                '<label class="custom-control-label" >Work</label>'+
                            '</div>'+
                            '<div class="d-inline-block custom-control custom-radio mr-1">'+
                                '<input name="addtional_number_type1['+Phone_count_type+']" class="addtional_number_type1 custom-control-input" value="Cell" checked="checked"  type="radio">'+
                                '<label class="custom-control-label" for="no">Cell</label>'+
                            '</div>'+
                            '<div class="d-inline-block custom-control custom-radio">'+
                                '<input name="addtional_number_type1['+Phone_count_type+']" class="addtional_number_type1 custom-control-input" value="Other" checked="checked"  type="radio">'+
                                '<label class="custom-control-label" for="no">Other</label>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
       
           $(".numbers_type").after(html);
         Phone_count_type++;
         Phone_count++;
       });
   
   
      
   
       // Script used for Remove phone number //
       $(document).on("click", ".removePhone", function() {
           $(this).fadeOut(600, function(){ $(this).parent().parent().parent().remove();});
       });
   
       $('.c_resize').cropper({
               // aspectRatio: 1,
               minCropBoxWidth: 300,
               minCropBoxHeight: 300,
               crop: function(e) {
              
               }
           });                     
   
   
         
   
      
   
           $(document).on('click','.deleteButton',function(){
   
               //Delete Button 
               if(global == 0)
               { 
                 $(document).find('#deleteModal').modal('show');
                 $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
               }
               else
               {
                 $('.fileupload').fileupload('clear');
                 $('input[name="avatar_data"]').val('');
               }
           });
   
   
   
           $(document).on('click','.deleteButtonModel',function(){
               var url = $(this).attr('data-href');
               if(url.length > 0 && global == 0)
               {
                   $.ajax({
                       url: url ,
                       type: 'GET',
                       dataType: 'json',
                       success: function (data) {
                           $(document).find('#deleteModal').modal('hide');
                           $(document).find('#ResponseModal .modal-title').text(data.Title);
                           $(document).find('#ResponseModal .modal-body p').text(data.message);
                           $(document).find('#ResponseModal').modal('show');
                           //$(document).find('.petCoverPhoto').html('');
                           $('.fileupload').fileupload('clear');
                       },
                       error : function(err)
                       {
                            $(document).find('#deleteModal').modal('hide');
                            console.log(err);
                       }
                   });
               }
               else
               {
                 $('.fileupload').fileupload('clear');
               } 
           });
   
           $.validator.addClassRules('phone1', {
                number: true,
   
           });
           $("#newCustomer").validate({
               excluded: ':disabled',
               onfocusout: function(e) {
                       this.element(e);                          
               },
               rules: {
                   username : {
                       required : true
                   },
                   gender : {
                       required : true
                   },
                   email : {
                       //required: true,
                       email:true,
                       remote : {
                             url: BASE_URL + "Admin/checkCustomerEmail",
                             type: "POST",
                             dataType: "json"
                             // ,
                             // data : {
                             //   'csrf_test_name' : getCookie('csrf_cookie_name')
                             // }
                       }
                   },
                   phone : {
                       required: true,
                       remote : {
                         url: BASE_URL + "Admin/checkCustomerPhone",
                         type: "POST",
                         dataType: "json"
                         // ,
                         // data : {
                         //   'csrf_test_name' : getCookie('csrf_cookie_name')
                         // }
                       },
                       minlength:10
                   }, 
                   
                   mobile : {
                       required: false,
                       minlength:10
                   },
                   number_of_type : {
                       required: true,
                       minlength:10
                   },
                   work : {
                       required: false,
                       minlength:10
                   },
                   address : {
                       //required :true
                   },
                   city : {
                       //required : true
                   },
                   state : {
                      //required: true
                   },
                   postal_code : {
                     //required : true,
                     number: true,
                     minlength: 3
                   },
                   password : {
                       required:true,
                       minlength:4
                   },
                   retype_password : {
                       required:true,
                       minlength:4,
                       equalTo:'#password'
                   },
                   dob : {
                       //required : true
                   },
                    veterinarian : {
                     //required : true
                   },
                   emergency_contact_name : {
                     required:false
                   },
                   emergency_contact_phone_number: {
                     required:false,
                     minlength:10
                   },
                   
               },
               messages : {
                   phone : {
                       minlength : "Phone number should be atleast 10 digit",
                       remote : "Phone already exists"
                   },
                   mobile : {
                       minlength : "mobile number should be atleast 10 digit"
                   },
                   work : {
                       minlength : "number should be atleast 10 digit"
                   },
                   email : {
                       remote : "Email already exists"
                   },
                   username : {
                       remote : "Username already exists"
                   },
                   emergency_contact_phone_number : {
                     minlength : "Emergency Number should be atleast 10 digit"
                    },
               },
   
               highlight: function(element) {
                   $(element).closest('.form-group').addClass('has-error');
               },
               unhighlight: function(element) {
                   $(element).closest('.form-group').removeClass('has-error');
               },
               errorElement: 'span',
               errorClass: 'help-block animated fadeInDown',
               errorPlacement: function(error, element) {
                   if (element.parent('.input-group').length) {
                       error.insertAfter(element.parent());
                   } else {
                       console.log(element.context.name);
                       
                       if(element.parent('.moduleCheckbox').length > 0){
                          
                           //console.log(element.parent('label').parent('div').html());
                           error.insertAfter(element.closest('.form-group').find('.checkError'));
                           //$(document).find('.checkError').show();
                       }
                       // if(element.parent('.css-input')){
                       //   error.insertAfter(element.closest('.col-sm-5').find('.radioError'));
                       // }
                       else {
                           error.insertAfter(element);
                       }
                       
                   }
               }
           });
   
   
           $("#editCustomer").validate({
               excluded: ':disabled',
               onfocusout: function(e) {
                       this.element(e);                          
               },
               rules: {
                   username : {
                       required : true
                   },
                   gender : {
                       //required : true
                   },
                   email : {
                        //   required: true,
                       email:true,
                       remote : {
                             url: BASE_URL + "Admin/checkCustomerEditEmail",
                             type: "POST",
                             dataType: "json",
                             data : {
                               // 'csrf_test_name' : getCookie('csrf_cookie_name'),
                               'id' : function() {
                                   return $(document).find('#id').val();
                               }
                             }
                       }
                   },
                   phone : {
                       required: true,
                       remote : {
                             url: BASE_URL + "Admin/checkCustomerEditPhone",
                             type: "POST",
                             dataType: "json",
                             data : {
                               // 'csrf_test_name' : getCookie('csrf_cookie_name'),
                               'id' : function() {
                                   return $(document).find('#id').val();
                               }
                             }
                       },
                       minlength:10
   
                   },
                   mobile : {
                       //required: false,
                       // number:true,
                       // min:0,
                       minlength:10
   
                   },
                  
                   address : {
                       //required :true
   
                   },
                   city : {
                       //required : true
                   },
                   state : {
                      //required: true
                   },
                   postal_code : {
                     //required : true,
                     number: true,
                     minlength: 3
                   },
                   dob : {
                       //required : true
                   },
                   // veterinarian : {
                   //   required : true
                   // },
                   emergency_contact_name : {
                     required:false
                   },
                   emergency_contact_phone_number: {
                     required:false,
                     // number:true,
                     // min:0,
                     minlength:10
                   },
                   
               },
               messages : {
                   phone : {
                       minlength : "Phone number should be atleast 10 digit",
                       remote : "Phone already exists"
                   },
                   mobile : {
                       minlength : "Phone number should be atleast 10 digit"
                   },
                   work : {
                       minlength : "Number should be atleast 10 digit"
                   },
                   email : {
                       remote : "Email already exists"
                   },
                   username : {
                       remote : "Username already exists"
                   },
                   emergency_contact_phone_number : {
                     minlength : "Emergency Number should be atleast 10 digit"
                   },
               },
   
               highlight: function(element) {
                   $(element).closest('.form-group').addClass('has-error');
               },
               unhighlight: function(element) {
                   $(element).closest('.form-group').removeClass('has-error');
               },
               errorElement: 'span',
               errorClass: 'help-block animated fadeInDown',
               errorPlacement: function(error, element) {
                   if (element.parent('.input-group').length) {
                       error.insertAfter(element.parent());
                   } else {
                       console.log(element.context.name);
                       
                       if(element.parent('.moduleCheckbox').length > 0){
                          
                           //console.log(element.parent('label').parent('div').html());
                           error.insertAfter(element.closest('.form-group').find('.checkError'));
                           //$(document).find('.checkError').show();
                       }
                       else {
                           error.insertAfter(element);
                       }
                       
                   }
               }
           });
           
           function get_Cookie(name) {
           var cookieValue = null;
           if (document.cookie && document.cookie != '') {
               var cookies = document.cookie.split(';');
               for (var i = 0; i < cookies.length; i++) {
                   var cookie = jQuery.trim(cookies[i]);
                   // Does this cookie string begin with the name we want?
                   if (cookie.substring(0, name.length + 1) == (name + '=')) {
                       cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                       break;
                   }
               }
           }
           $('#token').val(cookieValue);
       }
   
        $(document).ready(function(){

    

    
    $('.date-picker').datepicker({
        autoclose: true,
        format: "dd-mmm-yyyy"
    });
    
    $('.editProfile').validate({
        onfocusout: function(e) {
            this.element(e);                          
        },
        onkeyup: false,
        rules: {
            username: {
                required: true,
                minlength: 3,
                remote: {
                    url: BASE_URL + "admin/checkUniqueForEdit",
                    type: "post",
                    dataType: "json",
                    data: {
                        field: 'username'
                    }
                }
            },
            email: {
                required: true,
                minlength: 3,
                remote: {
                    url: BASE_URL + "admin/checkUniqueForEdit",
                    type: "post",
                    dataType: "json",
                    data: {
                        field: 'email'
                    }
                }
            }
        },
        messages: {
            username: {
                remote: "This username is already taken!"
            },
            email: {
                remote: "This email is already taken!"
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
              $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('.c_resize').cropper({
        aspectRatio: 1,
        crop: function(e) {
       
        }
    });

    $(document).on('change','.country_code',function(){
        $('.code_span').html('+'+$(this).val());
    });
    $(document).on('change','.country_code_alternative',function(){
        $('.code_span1').html('+'+$(this).val());
    });

    $('input[name="employee_photo"]').click(function(){
      $(this).val(null);
   });
   
   $('input[name="employee_photo"]').change(function(){
      if($(this)[0].files.length <= 0){
            return;
        }
        var f = $(this)[0].files[0];
        var reader = new FileReader();
        
        // Closure to capture the file information.
       reader.onload = (function(file) {
           return function(e) {
            var image = document.createElement('img');
                image.addEventListener('error', function() {
                        $('#extensionModal').modal('show');
                        return;
                });
                image.addEventListener('load', function() {
                    if(image.width < 150 || image.height < 150){
                        $('#sizeModal').modal('show');
                        return;
                    }
                    $('#cropModal').on('shown.bs.modal', function() {
                   $('.c_resize').cropper('replace', e.target.result);
               });
               $('.c_resize').prop('src', e.target.result);
               $('#cropModal').modal('show');
               });
               image.src = e.target.result;
           }
      })(f);
   
         // Read in the image file as a data URL.
         reader.readAsDataURL(f);
   
   });
   
   $('.setDefaultImgBtn').click(function(){     
        var url = $('.user-image .fileupload-preview img').prop('src');
      $('input[name="engimage_data"]').val(url);
   });
   
   $('.cropSaveBtn').click(function(){
      var canvas = $('.c_resize').cropper('getCroppedCanvas');
      $('.resized').html(canvas);
      var url = $('.resized canvas')[0].toDataURL();
          
        $('.user-image .fileupload-preview img').prop('src', url);
        $('#cropModal').modal('hide');
      $('input[name="engimage_data"]').val(url);
   });
   
   
   //delete image modal 
   $(document).on('click','.deleteButton',function(){
   
      if(global == 0)
      {  
         $(document).find("#deleteModal").modal('show');
         $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
      }
      else
      {
         $('input[name="avatar_data"]').val('');
         // $('.fileupload').fileupload('clear');
      }
   
   });


     $('.addsignbtn').click(function(){        
       $('#addsign').modal('show');
    });
    $('.savesign').click(function(){        
       $('#addsign').modal('hide');
    });

    
})
       });

// $( document ).ready(function() {
//     console.log( "ready!" );
// });

// <input id="dob" name="dob" class="form-control" type="date" value="<?php echo date('m/d/Y',strtotime($app_users['dob'])); ?>" >
$(document).find("#dob").val(<?php echo date('m/d/Y',strtotime($app_users['dob'])); ?>);
   
</script>

<script src="<?php echo base_url() ;?>assets/js/moment.js"></script>

<script>
$( document ).ready(function() {
   var dobhidden= $(document).find( "#dobhidden" ).val();
    var dob = moment(moment(dobhidden, 'YYYY-MM-DD')).format('YYYY-MM-DD');
    $(document).find("#dob").val(dob);
    // alert(a);
});
</script>