<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" scr="https://momentjs.com/downloads/moment-timezone.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<style type="text/css">
   input[type="checkbox"] {
   margin: 3px 0 0;
   line-height: normal;
   width: 15px;
   height: 15px;
   }
   .error{
   color: #ff4081;
   }
   /*.buttonMargin{
   margin-left: 10px;
   }*/
   #addModal,#sizeModal1,#extensionModal1,#WebcamModal,#addGroomerNote,#changeStatusModal{
   overflow-x: hidden;
   overflow-y: auto !important;
   }
   .masonry { /* Masonry container */
   column-count: 4;
   column-gap: 1em;
   }
   .item { /* Masonry bricks or child elements */
   background-color: #eee;
   display: inline-block;
   margin: 0 0 1em;
   width: 100%;
   }
   .NoDownload{
   background-color: gray;
   border-color: gray;
   color: white;
   }
   .NoDownload:hover { 
   background-color: gray;
   border-color: gray;
   color: white;
   }
   @media screen and (max-width: 900px) {
   .petdetailsHeader > .box-tools {
   position: relative !important;
   }
   }
   @media only screen and (max-width: 600px) {
   .modal > #addMedication > .modal-dialog > .modal-content >.modal-footer {
   text-align: left !important;
   }
   }
   @media only screen and (max-width: 600px) {
   .newboarding {
   margin-top: 5px !important;
   }
   }
   #table_filter {
   margin-top: 15px  !important;
   }
   .dataTables_filter{
   margin-right: 10px;
   }
   .dataTables_length{
   margin-left: 10px;
   }
   .dataTables_info{
   display: none;
   }
   .redirectBtn{
   /*background-color: #eee;*/
   /*width: 100%;*/
   text-align: left;
   color: #646464;
   background: none;
   padding: 0px;
   display: inline;
   /*font-weight: 400 !important;*/
   }
   .redirectBtn:hover {
   text-decoration: underline !important;
   color: #646464 ;
   }
   .nav > li > a {
   padding: 10px 12px!important;
   }
   .dt-buttons {
   margin-left: 10px ;
   }
   @media screen and (max-width: 600px) {
   .buttonspets {
   margin-top: 15px;
   text-align: center !important;
   }
   .tabbable-line{
   padding-left: 10px;
   padding-right: 10px;
   padding-bottom: 10px;
   }
   .petname {
   padding-top: 10px;
   text-align: center;
   }
   .buttonMargin{
   margin-top: 5px ;
   }
   .locationdiv{
   padding-right: 15px !important;
   }
   .tabs{
   display: initial !important;
   }
   .tab{
   width: 100% !important;
   }
   }
   hr{
   margin-top: 25px;
   width: 100%;
   height: 1px;
   background: linear-gradient(to left, #a0adb9 0%, #eee 100%);
   }
   .m6{
   margin-bottom: 10px;
   }
   .row .col.s6{
   padding: 0px;
   }
   .petname{
   padding-left: 10px;
   }
   hr{
    margin-top: 12px;
    margin-bottom: 10px;
   }
   .action_btn .btn ,.status {
       margin-top: 5px;
    }
</style>
<body class="vertical-layout vertical-menu-modern 2-columns fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block"><?php echo ucfirst($boardingDetails['category']); ?> Appointment Details</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url() ;?>user/dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Appointments
                            </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-2" style="text-align: right;">
                <?php if($appointments['created_by'] == $this->session->userdata('username')):?>
                <label for="eventInput1">Created by: Self</label>
                <?php else:?>
                <label for="eventInput1">Created by: <?php echo $appointments['created_by'];?></label>
                <?php endif;?>
            </div>
            
            <div class="content-body" >
                <section class="row" style=" margin-bottom:3%;">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div id="html-validations" class="card card-tabs">
                                    <div class="col s12 m12 l12" style="padding-top: 0px;padding-left:0px;padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-4 status" >
                                                <label class="btn btn-sm " style="color:white; background-color:<?php echo $colorArray[$appointments['status']]; ?>"><?php echo statusDisplay($appointments['status']); ?> <?php if(!empty($combinecategory)){echo $combinecategory;} ?> </label> 
                                            </div>
                                            <div class="col-md-8 action_btn" style="text-align: right;">
                                                
                                                    <a href="<?php echo site_url('User/invoice/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($appointments['invoice_no']))); ?>" class="btn btn-danger btn-primary btn-success btn-sm">Invoice</a>

                                                    <?php if($boarding_id > 0){?>

                                                    <a href="<?php echo base_url('User/'.$kennelfunction.'/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($pet['id'])).'/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($boarding_id)).'/'.$id_type) ?>" class="btn btn-secondary btn-primary btn-sm NoDownload"><i class="fa fa-download" aria-hidden="true"></i> Kennel Card</a>

                                                    <?php }else{?>

                                                    <a href="javascript:void(0)" class="btn btn-secondary btn-primary btn-sm NoDownload"><i class="fa fa-download" aria-hidden="true"></i> Kennel Card</a>

                                                    <?php }?>

                                                    <?php if(($appointments['can_be_canceled'] == 0) && ($appointments['appointment_date'] >= date('Y-m-d'))){?>
                                                
                                                    <a href="<?php echo site_url('User/MoveAppointment/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($appointments['invoice_no']))); ?>" class="btn btn-primary btn-sm" style="background-color:<?php echo $colorArray['Move']; ?> !important;color:white;" ></i>Move Appointment</a>
                                                    <?php }?>
                                                    <?php if($appointments['can_be_canceled'] == 0){?>
                                                
                                                    <a href="<?php echo site_url('User/cancelAppointment/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($appointments['invoice_no']))); ?>" style="background-color:<?php echo $colorArray['Cancel']; ?>  !important;color:white;" class="btn btn-danger btn-sm" ></i>Cancel Appointment</a>
                                                    <?php }?>
                                                
                                            </div>
                                        </div>
                                        <?php if(!empty($appointments['location_name'])){ ?>
                                        <div class=" col-md-12 form-group">
                                            <div class="" style="padding-top: 1em;text-align: right;">
                                                
                                                <span for="field-1" class="col-md-3 control-label">
                                                        Assigned Locations
                                                </span>
                                                <span class="label btn-primary" style="padding:0.5% 1%;"><?php echo $appointments['location_name']; ?></span>
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    
                                </div>
                            <div class="card-content">
                                <div class="card-body" >

									<?php if ($this->session->flashdata("success")): ?>
										<div class="alert alert-success">
										<i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
										</div>
									<?php elseif ($this->session->flashdata("error")): ?>
										<div class="alert alert-danger">
										<i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
										</div>
                                    <?php endif;?>
                                    

                                <ul class="product-tabs nav nav-tabs nav-underline no-hover-bg mb-2">
                                        <li class="nav-item">
                                            <a aria-controls="desc" style="background-color: white;color:#333333;" aria-expanded="true" class="nav-link active" data-toggle="tab" href="#desc" id="description">
                                            Overview
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="ratings" style="background-color: #60605B;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#ratings" id="review">
                                            Additional Notes
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="comment" style="background-color: #f39c12;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#comment" id="comments">
                                            Vaccination Records
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a aria-controls="Appointment_gallery" style="background-color:#c33d49f2 ;color:white;" aria-expanded="false" class="nav-link" data-toggle="tab" href="#Appointment_gallery" id="Appointment_gallerys">
                                            Appointment Gallery
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="product-content tab-content ">

                                        <div aria-expanded="true" aria-labelledby="description" class="tab-pane active" id="desc" role="tabpanel">
                                            <div class="media-list media-bordered">
                                                <span class="media-left col-md-3 mt-1">
                                                    <div class="petimage" style="text-align: center; position: relative;">
                                                        <?php if(!empty($petDetails['pet_cover_photo'])):?>
                                                        <img src="<?php echo loadCloudImage(PETS_IMAGES.$petDetails['pet_cover_photo']); ?>" alt="" style="width:135px;height: 135px;border-radius: 50%;">
                                                        <?php else:?>
                                                        <img src="<?php echo base_url()?>assets/default-p.png" style="width:135px;border-radius: 50%;height: 135px;" >
                                                        <?php endif;  ?> 
                                                    </div>
                                                </span>
                                                <div class="petname mt-2" style="font-weight: bold;font-size: 20px;">
                                                    <a href="<?php echo base_url('User/getPetDetails/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($petDetails['id']))); ?>" class="redirectBtn " >
                                                    <?php echo strtoupper($petDetails['name']);?>
                                                    </a>
                                                    
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3"><?= lang('petType') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  format_telephone($petDetails['type']); ?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('gender') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  $petDetails['gender']; ?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3"><?= lang('petWeight') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if($petDetails['weight']>0){echo $petDetails['weight'];}else{echo '';};?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('petBread') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  $petDetails['breed'];?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Mix Breed:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php  if(!empty($petDetails)){ if($petDetails['mix_breed'] == 1) {echo "Yes" ; }else{echo "No";}}?></label>
                                                        <label class="col-md-3" for="eventInput1">Spayed/Neutered:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php  if(!empty($petDetails)){ if($petDetails['spayed'] == 'true') {echo "Yes" ; }else{echo "No";}}?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Size:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  $petDetails['size']?></label>
                                                        <label class="col-md-3" for="eventInput1">Color:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  $petDetails['color']?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Puppy Power:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo  $petDetails['puppy_power']?></label>
                                                        <label class="col-md-3" for="eventInput1">Birth Date:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if(!empty($petDetails) && $petDetails['birth_date'] != null) echo date(get_option('date_format'), strtotime($petDetails['birth_date'])) ?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Notes From Customer:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo $petDetails['description']?></label>
                                                        <label class="col-md-3" for="eventInput1">Pet Behavior:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo $petBehavior['name']?></label>
                                                        <!--  <div class="col-sm-6 col-xs-6 emoji" data-emoji="<?php echo $petBehavior['emoji']?>" data-name="<?php echo $petBehavior['name']?>" >
                                                            -->
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Pet Behavioral Note:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if(!empty($petBehaviorNote) && isset($petBehaviorNote['behavior_note'])) echo $petBehaviorNote['behavior_note'];?></label>
                                                        <label class="col-md-3" for="eventInput1">Deceased/Inactive:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if($petDetails['is_deceased'] == 1){echo "Yes" ; }else{echo "No";}?></label>
                                                    </div>
                                                </div>
                                                <div class="titilename" style="font-weight: bold;font-size: 20px;">
                                                <?php echo strtoupper($boardingDetails['category']); ?> DETAILS
                                                </div>
                                                <hr>
                                                <div class="form-group col-md-12">
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Assigned Groomer:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo $appointments['groomerName'];?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('appointmentDate') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo date(get_option('date_format'), strtotime($appointments['appointment_date'])); ?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Start Time:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo date('h:i a', strtotime($appointments['appointment_time']));?></label>
                                                        <label class="col-md-3" for="eventInput1">End Time:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo date('h:i a', strtotime($appointments['appointment_end_time']));?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Check In Time:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if(!empty($appointments['check_in_time'])){
                                                        if($appointments['status'] == 'Inprocess' || $appointments['status']== 'Complete' || $appointments['status'] == 'Checkout' ){echo date(get_option('date_format').' '.'h:i a', strtotime($appointments['check_in_time']));}}else{echo '';} ?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('appointmentEndTime') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if(!empty($appointments['check_out_time'])){echo date(get_option('date_format').' '.'h:i a', strtotime($appointments['check_out_time']));}else{echo '';} ?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Services:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php $count = count($appointmentService); $i = 0;$str = ''; foreach ($services as $value) { 
                                                        if(in_array($value['id'], $appointmentService)) {
                                                                
                                                        
                                                            $str .= ' '.$value['name'] . ',';
                                                                
                                                        }
                                                        } $str = substr($str, 0, -1); $str = ltrim($str, ' '); ?>
                                                        <?php echo $str;?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('appointmentedBy') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><a href="<?php echo base_url('User/editprofile/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($appointments['app_user_id']))); ?>" class=" redirectBtn" style="font-weight:normal;"></a>
                                                        <?php echo $appointments['username'];?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3"><?= lang('email') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if (strlen($customerdetails['email']) > 15){
                                                        $customerdetails['email'] = chunk_split($customerdetails['email'], 15, ' ');
                                                        }; ?>
                                                        <?php echo $customerdetails['email'];?></label>
                                                        <label class="col-md-3" for="eventInput1"><?= lang('phone') ?>:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php if($customerdetails['number_type'] == 'Home'){
                                                        $icons = '<i class="fa fa-home" style="margin-right:5px;"></i>';
                                                        }else if($customerdetails['number_type'] == 'Cell'){
                                                        $icons = '<i class="fas fa-mobile" style="margin-right:5px;"></i>';
                                                        }else if($customerdetails['number_type'] == 'Work'){
                                                        $icons = '<i class="fa fa-building" style="margin-right:5px;"></i>';
                                                        }else if($customerdetails['number_type'] == 'Other'){
                                                        $icons = '<i class="fa fa-phone-square"" style="margin-right:5px;"></i>';
                                                        }else{
                                                        $icons = '<i class="fa fa-home" style="margin-right:5px;"></i>';
                                                        } ?>
                                                        <?php echo $icons.format_telephone($customerdetails['phone']);?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Notes From Customer:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo $petDetails['description']?></label>
                                                        <label class="col-md-3" for="eventInput1">Add On Services:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php  if(!empty($add_on_services)) {echo $add_on_services.'.';}else{echo '';} ?></label>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <label for="eventInput1" class="col-md-3">Size:</label>
                                                        <label class="col-md-3" for="eventInput1"><?php echo $appointments['upgrade_size'];?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div aria-labelledby="review" class="tab-pane" id="ratings">
                                            <div class="form-group margin">
                                                <?php if(!empty($groomNotes) || !empty($medicationNotes) || !empty($petBehaviorNote['behavior_note']) ):?>
                                                <div >
                                                    <table class="table table-bordered table-striped " id="2table">
                                                    <thead >
                                                        <tr>
                                                            <th class="active">
                                                                Type
                                                            </th>
                                                            <th class="active">
                                                                Note
                                                            </th>
                                                            <th class="active">
                                                                Created By
                                                            </th>
                                                            <th class="active">
                                                                Added On
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if(!empty($petBehaviorNote['behavior_note'])){ ?>
                                                        <tr class="custom-tr">
                                                            <td class="vertical-td">
                                                                Pet Behavior Note
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $petBehaviorNote['behavior_note'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $petBehaviorNote['username'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo date(get_option('date_format').' '.'h:i a',strtotime($petBehaviorNote['added_on']));?>
                                                            </td>
                                                        </tr>
                                                        <?php  } ?>
                                                        <?php if(!empty($groomNotes)){
                                                            foreach ($groomNotes as $key => $value) {
                                                            ?> 
                                                        <tr class="custom-tr">
                                                            <td class="vertical-td">
                                                                <?php echo ucwords($value['note_type']);?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $value['notes'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $value['username'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php if(!empty($value['added_on'])):?>
                                                                <?php echo date(get_option('date_format').' '.'h:i a',strtotime($value['added_on']));?>
                                                                <?php endif;?>    
                                                            </td>
                                                        </tr>
                                                        <?php } } ?>
                                                        <?php if(!empty($medicationNotes)){
                                                            foreach ($medicationNotes as $key => $value) {
                                                            ?> 
                                                        <tr class="custom-tr">
                                                            <td class="vertical-td">
                                                                Medication Notes
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $value['notes'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php echo $value['username'];?>
                                                            </td>
                                                            <td class="vertical-td">
                                                                <?php if(!empty($value['added_on'])):?>
                                                                <?php echo date(get_option('date_format').' '.'h:i a',strtotime($value['added_on']));?>
                                                                <?php endif;?>    
                                                            </td>
                                                        </tr>
                                                        <?php } } ?>
                                                    </tbody>
                                                    </table>
                                                </div>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                        <div aria-labelledby="comments" class="tab-pane" id="comment">
                                            <div class="" >
                                                <a   style="float: right;"  data-placement="top" data-toggle="modal" data-target="#addModal" data-form="edit" data-action="<?php echo base_url()?>User/addPetNote/<?php echo str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($pet['id']))?>" class="btn btn-outline-primary mb-1 addNoteButton"><i class="fa fa-plus"></i> <?= lang('addNewNotes') ?>
                                                </a>
                                            </div>
                                            <div class="table-responsive">
                                            <table class="table table-striped table-bordered responsive dataex-res-controlled" style="width:100%" id="3table">
                                                <thead >
                                                    <tr>
                                                        <th class="active"><?= lang('no') ?></th>
                                                        <th class="active">Image Proof</th>
                                                        <th class="active"><?= lang('medication_name') ?></th>
                                                        <th class="active"><?= lang('date_of') ?></th>
                                                        <th class="active"><?= lang('expiry_on') ?></th>
                                                        <th class="active"><?= lang('notes') ?></th>
                                                        <th class="active">Updated on</th>
                                                        <th class="active">Updated by</th>
                                                        <th class="active"><?= lang('is_expired') ?></th>
                                                        <th class="active"><?= lang('Actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($petNotes)): $i=0; foreach($petNotes as $item): $i++;?>
                                                    <tr class="custom-tr">
                                                        <td class="vertical-td"><?php echo $i;?></td>
                                                        <td class="vertical-td">
                                                            <?php if(isset($item) && $item['image'] != ''): ?>
                                                            <a href="https://storage.googleapis.com/<?php echo BUCKETNAME.'/'.PETS_IMAGES.$item['image']?>" class="image-link">
                                                            <img src="<?php echo loadCloudImage(PETS_IMAGES.$item['image']); ?>" class ="img-responsive" alt="" style="width:80px;height: 80px;">
                                                            </a>
                                                            <?php else:?>
                                                            <img src="<?php echo base_url();?>assets/vaccination_record.png" alt="" style="width:50px;height: 50px;">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="vertical-td"><?php echo  $item['medication_name'] ?></td>
                                                        <td class="vertical-td"><?php echo  date(get_option('date_format'), strtotime($item['date_of'])); ?></td>
                                                        <td class="vertical-td"><?php echo  date(get_option('date_format'), strtotime($item['expiry_on'])); ?></td>
                                                        <td class="vertical-td"><?php echo $item['notes']; ?></td>
                                                        <td><?php echo  date(get_option('date_format').' '.'h:i a', strtotime($item['added_on'])); ?></td>
                                                        <td class="vertical-td"><?php echo $item['username']; ?></td>
                                                        <td class="vertical-td">
                                                            <?php if($item['is_expired'] == 1 || time() > strtotime($item['expiry_on'])):?>
                                                            <label class="label label-danger">Expired</label>
                                                            <?php else:?>
                                                            <label class="label label-primary">Not Expired</label>
                                                            <?php endif;?>  
                                                        </td>
                                                        <td class="vertical-td">
                                                            <div class="btn-group">
                                                                <a href="javascript:void(0)" data-action="<?php echo base_url('User/editPetNotes/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($item['id']))) ?>" data-id = "<?php echo str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($item['id']))?>" class="btn-floating waves-effect waves-light gradient-45deg-light-blue-cyan gradient-shadow tooltipped modal-trigger editButton"
                                                                data-tooltip="Edit"><i class="material-icons">edit</i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <a  href="javascript:void(0)" data-href="<?php echo base_url('User/deletePetNotes/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($item['id'])).'/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($pet['id']))
                                                                    ) ?>" class="btn-floating waves-effect waves-light gradient-45deg-red-pink gradient-shadow modal-trigger tooltipped  modal-trigger deleteButton1" 
                                                                    data-tooltip="Delete"> <i class="material-icons">delete</i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        <div aria-labelledby="Appointment_gallerys" class="tab-pane" id="Appointment_gallery">
                                            <div class="" >
                                            <?php if(!empty($Pet_image)){?>
                                                <div class="row">
                                                    <div class="col-sm-12" data-offset="0">
                                                        <div class="masonry" style="margin:0px 15px">
                                                        <?php if(!empty($Pet_image)):?>
                                                        <?php foreach ($Pet_image as $key => $value) { ?>
                                                        <div>
                                                            <a href="https://storage.googleapis.com/<?php echo BUCKETNAME.'/'.WatermarkImages.$value['image']?>" class="image-link item">
                                                            <img src="https://storage.googleapis.com/<?php echo BUCKETNAME.'/'.WatermarkImages.$value['image']?>"  class="img-responsive"  style="display:block;">
                                                            </a>
                                                        </div>
                                                        <?php } ?>
                                                        <?php endif;?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } else {?>    
                                                <div style="text-align: center;">
                                                    <label >No Images Found</label>
                                                </div>
                                                <?php } ?>  
                                            </div>
                                            
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>             

<div class="modal fade text-left" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel35"> Add Vaccination Record</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addMedication" method="post" action="">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name"><?= lang('medication_name') ?></label>
                        <input type="text" class="form-control" id="medication_name" name="medication_name" placeholder="Medication Name">
                        <input type="hidden" name="formType" class="form-control" value="edit" id="formType">
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="date_of"><?= lang('date_of') ?></label>
                        <input id="datepicker" name="date_of" class="form-control" type="date" placeholder="Date of">
                    </fieldset>

                    <fieldset class="form-group floating-label-form-group">
                        <label for="date_of"><?= lang('expiry_on') ?></label>
                        <input id="datepicker" name="expiry_on" class="form-control" type="date" placeholder="Expiry on">
                    </fieldset>
                    
                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1"><?= lang('notes') ?></label>
                        <textarea class="form-control" id="notes" rows="5" name="notes" placeholder="Notes"></textarea>
                    </fieldset>

                    <fieldset class="form-group floating-label-form-group">
                        <label for="title1"><?= lang('vaccinationphoto') ?></label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="user-image1">
                                <div class="fileupload-new thumbnail ">
                                    <img src="<?php echo base_url();?>assets/vaccination_record.png" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="line-height: 224px; width:225px;height:225px;">
                                    <img src="<?php echo base_url();?>assets/vaccination_record.png" alt="">
                                </div>
                                <div class="user-image-buttons1">
                                    <span class="btn btn-success btn-file btn-sm"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                    <input type="file" name="vaccine_image" accept="image/png, image/jpeg,image/jpg">
                                    <input type="hidden" name="avatar_data1">
                                    </span>
                                    <!--  <a href="javascript:void(0)" class="btn btn-rounded btn-danger fileupload-exists btn-bricky btn-sm deleteButton"  data-href="<?php echo base_url('User/deletePetImage/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($pet['id']))) ?>" style="" >
                                    Remove
                                    </a> -->
                                    <a href="javascript:void(0)" class="btn btn-danger fileupload-exists btn-bricky btn-sm" data-dismiss="fileupload">
                                    Remove
                                    </a>
                                </div>
                                <div class="user-image-buttons webcam" style="margin:10px 0px;">
                                    <a href="javascript:void(0)" class="btn btn-primary webcamModalButton btn-sm" data-toggle="modal">Take Snapshot</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- END: Content-->
    
<div id="editModal" class="modal modal-fixed-footer" >
   <form class='form-horizontal' id="editMedication" method="post" action="">
      <div class="modal-content">
         <h4 class="modal-title">Edit Vaccination Record </h4>
         <div class="row">
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('medication_name') ?></span>
               <input id="medication_name" name="medication_name"  class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('date_of') ?></span>
               <input id="editdatepicker" name="date_of"  readonly="readonly" class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('expiry_on') ?></span>
               <input id="editdatepicker1" name="expiry_on"  readonly="readonly" class="form-control" type="text">
            </div>
            <div class="input-field col s12">
               <span class="input-group-addon"><?= lang('notes') ?></span>
               <textarea id="notes" name="notes"  class="form-control" type="text" rows="5" col="10"></textarea>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="modal-close btn btn-light-grey">
         Close
         </button>
         <button type="submit" class="btn btn-blue">
         Save
         </button>
      </div>
   </form>
</div>
<div id="deleteModal1" class="modal modal-fixed-footer" >
   <div class="modal-content">
      <h4 class="modal-title">Delete Vaccination Note</h4>
      <p>Are you sure you want to delete this Note?</p>
   </div>
   <div class="modal-footer">
      <button type="button" class="modal-close btn btn-light-grey" data-dismiss="modal">Close</button>
      <a href = '' class="btn btn-danger  deleteButtonModel1"><i class="fa fa-trash"></i> Delete</a>
   </div>
</div>
<script type="text/javascript">
   $(function(){
     // $('.image-link').viewbox({
     //   setTitle: true,
     //   margin: 20,
     //   resizeDuration: 300,
     //   openDuration: 200,
     //   closeDuration: 200,
     //   closeButton: true,
     //   navButtons: true,
     //   closeOnSideClick: true,
     //   nextOnContentClick: true
     // });
   });
   
   var timeZone = "<?php echo get_option('time_zone');?>";
   moment.tz.setDefault(timeZone);
   var checkouttime = moment().format('MMM DD YYYY, hh:mm a');
   
   var is_markCompleteOk = 0;
   var image_logo = '<?php echo $image_logo; ?>';
   var company_name = '<?php echo $company_name; ?>';
   function validateFloatKeyPress(el) {
       
       var v = parseFloat(el.value);
       el.value = (isNaN(v)) ? '' : v.toFixed(2);
   }
   var format_Money = "<?php echo get_option('currency_format');?>";
   var boarding_id = "<?php echo $boarding_id;?>";
   
   function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
       try {
           decimalCount = Math.abs(decimalCount);
           decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
           decimalCount = 2;
           const negativeSign = amount < 0 ? "-" : "";
   
           let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
           let j = (i.length > 3) ? i.length % 3 : 0;
   
           return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
       } catch (e) {
           console.log(e)
       }
   };
   function blink_text() {
       $('.blink').fadeOut(500);
       $('.blink').fadeIn(500);
   }
   setInterval(blink_text, 1000);
   
   
   function dataURLtoFile(dataurl, filename) {
       var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
           bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
       while(n--){
           u8arr[n] = bstr.charCodeAt(n);
       }
       return new File([u8arr], filename, {type:mime});
   }
   $(function() {
       // Initializes and creates emoji set from sprite sheet
       // window.emojiPicker = new EmojiPicker({
       //   emojiable_selector: '[data-emojiable=true]',
       //   assetsPath: BASE_URL+'assets/plugin/emoji2/lib/img/',
       //   popupButtonClasses: 'fa fa-smile-o'
       // });
       // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
       // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
       // It can be called as many times as necessary; previously converted input fields will not be converted again
       // window.emojiPicker.discover();
   });
   
   
   
    function updateAllUnicodeIcon()
    {
       $(document).find('div.emoji').each(function(){
           console.log($(this).attr('data-emoji'));
           var name =  $(this).attr('data-name');
           var emoji = $(this).attr('data-emoji');
           if(emoji.length > 0)
           {
               var html = unicodeToImage(emoji);
               console.log(html);
               $(this).html(name+' '+html);
           }
       });
    }
   
   
    function  unicodeToImage (input) {
     if (!input) {
       return '';
     }
     if (!Config.rx_colons) {
       Config.init_unified();
     }
     return input.replace(Config.rx_codes, function(m) {
       var $img;
       if (m) {
         $img = $.emojiarea.createIcon($.emojiarea.icons[":"+Config.reversemap[m]+":"]);
         return $img;
       } else {
         return '';
       }
     });
   };
   
   
   function updateGlobalPrice()
   {
       var total = 0;
   
       var c1 = parseFloat($(document).find('#upgrade_cost').val());
       var c2 = parseFloat($(document).find('#add_on_service_cost').val());
   
       var c3 = parseFloat($(document).find('#additional_cost').val());
       var c4 = parseFloat($(document).find('#currentCost').val());
   
       if(c3 && c3 > 0)
       {
           $(document).find('#totalCost').val(formatMoney(c1+c2+c3+c4, format_Money));
       }
       else 
       {
           $(document).find('#totalCost').val(formatMoney(c1+c2+c4, format_Money));
       }
   }
   
   
   function updateAddOnCost()
   {
       var size =  $('#upgrade_size').val();
       //alert(size);
       var totalPrice = 0;
       //alert('data-'+size+'_cost');
   
   
       $(document).find('.addonCheckbox:checked').each(function(){
          // alert($(this).attr('data-'+size+'_cost'));
           totalPrice += parseFloat($(this).attr('data-'+size+'_cost'));
       });
   
       $(document).find('#add_on_service_cost').val(formatMoney(totalPrice, format_Money));
   }
   
   $('.take_click').click(function(){
     Webcam.snap( function(data_uri) {
       $(document).find('#myImageWebcam').html('<img style="width:225px;"  src="'+data_uri+'"/>');
       $('.fileupload-new').find('img').prop('src', data_uri);
       $('#cropModal').on('shown.bs.modal', function() {
         $('.c_resize').cropper('replace', data_uri);
   
         $('.c_resize').prop('src', data_uri);
       });
       $(document).find('#WebcamModal').modal('hide');
       $('#cropModal').modal('show');
     } );
   });
   function take_snapshot() {
           Webcam.snap( function(data_uri) {
                $(document).find('#myImageWebcam').html('<img style="width:225px;"  src="'+data_uri+'"/>');
           } );
   }
   function getWidthOfText(txt, fontname, fontsize){
       if(getWidthOfText.c === undefined){
           getWidthOfText.c=document.createElement('canvas');
           getWidthOfText.ctx=getWidthOfText.c.getContext('2d');
       }
       getWidthOfText.ctx.font = fontsize + ' ' + fontname;
       return getWidthOfText.ctx.measureText(txt).width;
   }
   function resizeImageSize(ImageWidth,ImageHeight){
     var tempSize = 0;
     if (ImageWidth <= ImageHeight) {
       tempSize = ImageWidth;
     }else{
       tempSize = ImageHeight;
     }
   
     for (; tempSize > 1000;) {
       tempSize = tempSize-100;
       ImageWidth = ImageWidth-100;
       ImageHeight = ImageHeight-100;
     }
     var size_array = [ImageWidth,ImageHeight];
     return size_array;
   
   }
   
   function unique(list) {
       var result = [];
       $.each(list, function(i, e) {
           if ($.inArray(e, result) == -1) result.push(e);
       });
       return result;
   }
   
   
   
   $(document).ready(function(){
   
    // $.unblockUI();
   
   
   
   $(document).on('click','.PrintBtn', function(){
       
       if($('#addContract').valid()) {
           $(".printModalBtn").removeClass("flash-button");
           $(".uploadModalBtn").addClass("flash-button");
           $("#addContractModal").modal("hide");
           $(document).find('#addContract').get(0).submit();
       }
       
   });
   
   // $('#pet_idUpload').multiselect({ 
   //     buttonText: function(options, select) {
   //         if (options.length === 0) {
   //             return 'None selected';
   //         }
   //         if (options.length === select[0].length) {
   //             return 'All selected ('+select[0].length+')';
   //         }
   //         else if (options.length >= 4) {
   //             return options.length + ' selected';
   //         }
   //         else {
   //             var labels = [];
   //             options.each(function() {
   //                 labels.push($(this).text());
   //             });
   //             return labels.join(', ') + '';
   //         }
   //     }
   
   // });
   $(function() {
        $("input:file").change(function (){
          var fileName = $(this).val();
          if(fileName.length>0){
               $('.fileuploadYn').text('File Selected');
               
          }else{
               $('.fileuploadYn').text('');
          }
          //$(".filename").html(fileName);
        });
     });
   
   
   $('#addContract').validate({
       rules: {
           pet_id : {
               required : true                 
           }
       },
       highlight: function(element) {
           $(element).closest('.form-group').addClass('has-error');
       },
       unhighlight: function(element) {
           $(element).closest('.form-group').removeClass('has-error');
       }
   });
   
   
   
   
   $.validator.addMethod('filesize', function (value, element, param) {
       
       return this.optional(element) || (element.files[0].size <= param)
   }, 'File size must be less than 25 MB');
   
   $('#uploadContract').validate({
       rules: {
           pet_id : {
               required : true                 
           },
           file_name: {
             required: true,
             extension:'pdf,png,jpeg,jpg',
             filesize: 25000000,
           }
       },
       highlight: function(element) {
           $(element).closest('.form-group').addClass('has-error');
       },
       unhighlight: function(element) {
           $(element).closest('.form-group').removeClass('has-error');
       }
   });
   
   var datepicker_today = '0';
      
   // if(date_today < moment().format('YYYY-MM-DD')){
   //     datepicker_today = '-1d';
   // }
   // else if(date_today > moment().format('YYYY-MM-DD')){
   //     datepicker_today = '+1d';
   // }
   
   // $('#datepicker12').datepicker({
   //     endDate: datepicker_today,
   //     autoclose: true,
   //     format: format,
   // });
   
   
   // this is the id of the form
   $("#uploadContract").submit(function(e) {
       if($('#uploadContract').valid()){
           var link = $(document).find('.uploadModalBtn').attr("data-link");
           var form = $(this);
           var url = form.attr('action');
           var data = new FormData(document.getElementsByName('#uploadContract')[0]);
           var data = new FormData();
           $.each(jQuery('#contractFile')[0].files, function(i, file) {
               data.append('file', file);
           });
           data.append('pet_id',$('#pet_idUpload').val());
           data.append('signed_date',$('#datepicker12').val());
           data.append('allow_type','Grooming');
           $.ajax({
               url: url,
               data: data,
               cache: false,
               contentType: false,
               processData: false,
               method: 'POST',
               type: 'POST', // For jQuery < 1.9
               success: function(data){
                   $(document).find('#uploadContractModal').modal('hide');
                   $(document).find('.printModalBtn').hide();
                   $(document).find('.uploadModalBtn').hide();
                   // $.unblockUI();
               }
           });
   
           e.preventDefault();
           // avoid to execute the actual submit of the form.
       } 
   });
   
   $(document).on('click','.markCompleteOk',function(){
      is_markCompleteOk = 1;
      $("#markCompleteConfirmation").modal("hide");
      // $("#changeStatusModalComplete").modal('show');
      $('.statusChangeModalButton1').trigger('click');
   });
   
   $(document).on('click','.markCompleteClose',function(){
      $("#markCompleteConfirmation").modal("hide");
      $("#changeStatusModalComplete").modal('show');
   });
   
   
   
   
   $(document).on('click','.locRadio',function(){
       if($(this).find('.disableRadio').val() == 1){
           $('.manual_discountDiv').show();
           $('.manual_discountPerDiv').hide();
           $('#manual_discountPer').val('');
       }else{
           $('.manual_discountDiv').hide();
           $('.manual_discountPerDiv').show();
           $('#manual_discount').val('');
       }
       $('.calculateButton').show();
       $('.checkoutButton,.PaySourceBtn,.checkoutMsgDiv').hide();
   });
   
   $(document).on('click','#additional_cost1,#manual_discount,#manual_discountPer',function(){
       $('.calculateButton').show();
       $('.checkoutButton,.PaySourceBtn,.checkoutMsgDiv').hide();
   });
   
   
   $(document).find('.webcam').show();
   
   $(document).on('click','.webcamModalButton',function(){
   
     navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia ||
                        navigator.msGetUserMedia);
   
     navigator.getMedia({video: true}, function() {
       // webcam is available
       $(document).find('#changeStatusModalComplete').modal('hide');
       $(document).find('#WebcamModal').modal('show');
     }, function() {
       // webcam is not available
       //$(document).find('.webcam').hide();
   
       alert('Webcam not found or Please allow your webcam');
   
     });
   
     Webcam.set({
         width: 320,
         height: 240,
         dest_width: 640,
         dest_height: 480,
         image_format: 'jpeg',
         jpeg_quality: 90,
         force_flash: false
       });
     Webcam.attach( '#my_result' );
   });
   
     // $.validator.addMethod('checkOtherFeildCost', function (value, element, param) {
     //   // return this.optional(element) || (element.files[0].size <= param)
   
     //   return $(document).find('#additional_cost').val() == 0 && $(document).find('#additional_cost').val().length > 0
   
     // }, 'Note is required');
   
     // $.validator.addMethod('checkOtherFeildNote', function (value, element, param) {
     //   // return this.optional(element) || (element.files[0].size <= param)
   
     //   return $(document).find('#note').val().length > 0 
   
     // }, 'Additional Cost is required');
   
    updateAllUnicodeIcon();
   
    $(document).on('change','#paymentSource',function(){
     if($(this).find(':selected').val() == 'Other'){
       $('.otherSourceDiv').show();
     }else{
       $('.otherSourceDiv').hide();
     }
   });
   
   
   $(document).on('click','.checkInButton',function(){
       $(document).find('#checkinModal .printModalBtn').hide();
       $(document).find('#checkinModal .uploadModalBtn').hide();
       $(document).find('#checkinModal .checkinmodalBtn').hide();
       $(document).find('#checkinModal .checkinClose').hide();
   
       var bodymsg = $(this).attr('data-bodymsg');
       var btntitle = $(this).attr('data-btntitle');
       var btnhref = $(this).attr('data-href');
   
       $(document).find('#checkinModal .modal-body p').html(bodymsg);
       var name =  $(this).attr('data-name');
       var emoji = $(this).attr('data-emoji');
       if(emoji != undefined && emoji.length > 0)
       {
           var html = unicodeToImage(emoji);
           $('.emoji1').html(name+' '+html);
       }
       $(document).find('#checkinModal .modal-body p').hide();
      
       var id1 = $(this).attr('data-id');
       $.ajax({
           url: BASE_URL+'Admin/getAppointmentDetailsCommon/'+id1 ,
           type: 'GET',
           dataType: 'json',
           success: function (data) {
              
                   if(data.contract == 1)
                   {
                       // $(document).find('#checkinModal .checkinmodalBtn').hide();
                       $(document).find('#checkinModal .checkinmodalBtn').text(btntitle); 
                       $(document).find('#checkinModal .checkinmodalBtn').attr('href',btnhref);
                       $(document).find('#checkinModal .checkinmodalBtn').show();
   
                       $(document).find('#checkinModal .modal-body p').show();
                       $(document).find('#pet_idDownload').html(data.petDropdown);
                       $(document).find('#pet_idUpload').html(data.petDropdown);
                       $(document).find('#checkinModal .uploadModalBtn').attr('data-link',btnhref);
                       $(document).find('#checkinModal .printModalBtn').show();
                       $(document).find('#checkinModal .uploadModalBtn').show();
                       // $('#pet_idDownload').multiselect('rebuild');
                       // $('#pet_idUpload').multiselect('rebuild');
                       $(document).find('#checkinModal .checkinClose').show();
                   }
                   else
                   {
                       $(document).find('#checkinModal .modal-body p').show();
                       $(document).find('#checkinModal .checkinmodalBtn').text(btntitle); 
                       $(document).find('#checkinModal .checkinmodalBtn').attr('href',btnhref);
                       $(document).find('#checkinModal .printModalBtn').hide();
                       $(document).find('#checkinModal .uploadModalBtn').hide();
                       $(document).find('#checkinModal .checkinmodalBtn').show();
                       $(document).find('#checkinModal .checkinClose').show(); 
                   }
                   
               
               
           },
           error : function(err)
           {
   
           }
       });
   });
   
   var allpetname = [];
   var petName = [];
   var uniqueArray1 = '';
   var appointment_balance = 0;
   $(document).on('click','.appointmentcheckboxes',function(){ 
       petName = [];
       checkedappointmentdetails = [];
       appointment_balance = 0;
       $('.appointmentcheckboxes').each(function () {
           if (this.checked) {
             
               checkedappointmentdetails.push([$(this).attr('data-appointmentId'),$(this).attr('data-category')]);
               petName.push($(this).attr('data-petName'));
               appointment_balance = parseFloat(appointment_balance) + parseFloat($(this).attr('data-appointment_balance'));
               // alert(appointment_balance);
           }
       });
       
       petName = unique(petName);
       $(document).find('#checkoutModal .all_appointments').val(JSON.stringify(checkedappointmentdetails));
       var balance_amount =  $(document).find('#checkoutModal .balance_amount').val();
       balance_amount = parseFloat(appointment_balance) + parseFloat(balance_amount);
   
       $(document).find('#checkoutModal .balance_amount').val(balance_amount);
      
       $(document).find('#checkoutModal .balance_amount').attr("data-balance",balance_amount);
       
   });
   
   $(document).on('click','.nextcalculatebtn',function(){
      
       var currentpetName = $(document).find('#getallactiveappointments .currentpetName').val();
       petName = jQuery.grep(petName, function(value) {
         return value != currentpetName;
       });
       uniqueArray1 = '';
       uniqueArray1 = petName.toString();
       uniqueArray1 = uniqueArray1.replace(/,[s]*/g, ", ");
   
       if(uniqueArray1 != ''){
           $(document).find('#checkoutModal .allpetstatement').html('You are currently checking out '+currentpetName+' along with '+uniqueArray1+'.');
       }else{
           $(document).find('#checkoutModal .allpetstatement').html('');
       }
   
      
       $(document).find('#getallactiveappointments').modal('hide');
       $(document).find('#checkoutModal').modal('show');
       uniqueArray1 = '';
       petName = [];
   });
   // checkout modal script
   $(document).on('click','.checkoutModalButton',function(){
       var currentpetName =  $(this).attr("data-pet_name");
       $(document).find('#getallactiveappointments .currentpetName').val(currentpetName);
       var app_user_id = $(this).attr("data-app_user_id");
       $(document).find('#checkoutModal .appointment_app_user_id').val(app_user_id);
       $('#checkoutForm')[0].reset();
       $('.all_appointments').val('');
       checkedappointmentdetails = [];
     
       //setTimeout(function(){ 
           $.blockUI({
              baseZ: 2000,
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
           });
       //}, 100);
       $('.checkoutButton').hide();
       $('.calculateButton').show();
       $('.step1').show();
       $('.step2').hide();
       $('.manual_discountDiv').show();
       $('.manual_discountPerDiv').hide();
   
       var endingtime = $(this).attr('data-end_time');
       $('.endingtime').html(endingtime);
       
       //var checkouttime = moment().format('MMM DD YYYY, hh:mm a');
       $('.checkouttime').html(checkouttime);
       $('.check_out_time').val(checkouttime);
       
       if(new Date(checkouttime) > new Date(endingtime)){
           $('.checkouttime').css('color', 'red');  
       }else{
           $('.checkouttime').css('color', 'green');
       }
       
       $(document).find('#checkoutModal .modal-title').html($(this).attr('data-title')); 
       $(document).find('#checkoutModal .checkoutButton').text($(this).attr('data-btntitle')); 
       $(document).find('#checkoutModal .appointment_id').val($(this).attr('data-id'));
       $(document).find('#checkoutModal .trans_id').val($(this).attr('data-transId'));
       $(document).find('#checkoutModal .balance_amount').val($(this).attr('data-amount'));
       $(document).find('#checkoutModal .balance_amount').attr("data-balance",$(this).attr('data-amount'));
   
   
       var id1 = $(this).attr('data-id');
       var app_user_id = $(this).attr('data-app_user_id');
   
   
       $.ajax({
           url: BASE_URL+'Admin/getRelatedPet/'+app_user_id+'/'+id1+'/appointment' ,
           type: 'GET',
           dataType: 'json',
           async : false,
           success: function (data) {
             
              if(data){
               // alert();
               // console.log(data.result);
               // $(document).find('.checkoutModalButton').attr('href','#checkoutModal');
               $(document).find('#getallactiveappointments').modal('show');
               $(document).find('#checkoutModal').modal('hide');
               $(document).find('#getallactiveappointments .modal-body p').html('Some of your pets have active appointments currently being served. Please select the ones you want to checkout along:');
               $(document).find('#getallactiveappointments .modal-body div').html(data.result);
              }else{
                   $(document).find('#checkoutModal').modal('show');
                   // $(document).find('.checkoutModalButton').attr('href','#checkoutModal');
              }
           },
           error : function(err)
           {
               // $.unblockUI();
           }
       });
   
       
   
       $.ajax({
           url: BASE_URL+'Admin/getAppointmentDetailsCommon/'+id1 ,
           type: 'GET',
           dataType: 'json',
           async : false,
           success: function (data) {
             
               if(data.contract == 1)
               {
                   $(document).find('#pet_idDownload').html(data.petDropdown);
                   $(document).find('#pet_idUpload').html(data.petDropdown);
                   $(document).find('#checkoutModal .printModalBtn').show();
                   $(document).find('#checkoutModal .uploadModalBtn').show();
                   // $('#pet_idDownload').multiselect('rebuild');
                   // $('#pet_idUpload').multiselect('rebuild');
                   $("#addContractModal").css('zIndex',1051);
                   $("#uploadContractModal").css('zIndex',1051);
               }
               else
               {
                   $(document).find('#checkoutModal .printModalBtn').hide();
                   $(document).find('#checkoutModal .uploadModalBtn').hide();
               }
               if(data.string1){
                   $('.autoDiscount').html(data.string1);
                   $('.autoDiscountDiv').show();
               }else{
                   $('.autoDiscount').html('');
                   $('.autoDiscountDiv').hide();
               }
               // setTimeout(function(){ 
               //   $.unblockUI(); 
               // }, 500);
           },
           error : function(err)
           {
               // $.unblockUI();
           }
       });
   
       // $.ajax({
       //     url: BASE_URL+'Admin/getAutoDiscount/'+id1 ,
       //     type: 'GET',
       //     dataType: 'json',
       //     success: function (data) {
       //         console.log(data);
       //         if(data.errcode == 0){
       //             $('.autoDiscount').html(data.result);
       //             $('.autoDiscountDiv').show();
       //         }else{
       //             $('.autoDiscount').html('');
       //             $('.autoDiscountDiv').hide();
       //         }
       //         // setTimeout(function(){ 
       //         //   $.unblockUI(); 
       //         // }, 500); 
       //     },
       //     error : function(err)
       //     {
       //         $.unblockUI();
       //     }
       // });
       setTimeout(function(){ 
         // $.unblockUI(); 
       }, 1000); 
   });
   
   
   
   $(document).on('click','.changeStatusButton',function(){
       $(document).find('#changeStatusModal .statusChangeModalButton').show();
   
       $(document).find('#changeStatusModal .modal-body p').html($(this).attr('data-bodymsg'));
         $(document).find('#changeStatusModal .modal-title').html($(this).attr('data-title')); 
         $(document).find('#changeStatusModal .statusChangeModalButton').text($(this).attr('data-btntitle')); 
         $(document).find('#changeStatusModal .statusChangeModalButton').attr('href',$(this).attr('data-href'));
   
           var name =  $(this).attr('data-name');
           var emoji = $(this).attr('data-emoji');
           if(emoji != undefined && emoji.length > 0)
           {
               var html = unicodeToImage(emoji);
               $('.emoji1').html(name+' '+html);
           }
       
           if($(this).attr('data-btntitle') == 'Cancel'){
               if($(this).attr('data-deposit') > 0){
                 $(document).find('#changeStatusModal .statusChangeModalButton').text('Credit');
                 $(document).find('#changeStatusModal .cancelRefundBtn').attr('href',$(this).attr('data-href')+'/refund');
                 $(document).find('#changeStatusModal .cancelRefundBtn').show();
               }else{
                 $(document).find('#changeStatusModal .modal-body p').html('Are you sure you want to cancel this appointment?');
                 $(document).find('#changeStatusModal .cancelRefundBtn').hide();
               }
           }else{
             $(document).find('#changeStatusModal .cancelRefundBtn').hide();
           }
   });
   
   
   $(document).find('#additional_cost1').on('focusout',function(){
       var newAmount = 0.00;
       newAmount = parseFloat($(this).val())+parseFloat($('.balance_amount').data('balance'));
   
       $('.balance_amount').val(newAmount);
   
   });
   
   
   $(document).on('click','.changePaymentStatusButton',function(){
   
     $(document).find('#paymentmodal .modal-body p').html($(this).attr('data-bodymsg'));
     $(document).find('#paymentmodal .modal-title').html($(this).attr('data-title')); 
     $(document).find('#paymentmodal .statusChangeModalButton').text($(this).attr('data-btntitle')); 
     $(document).find('#paymentmodal .statusChangeModalButton').attr('href',$(this).attr('data-href')); 
   
   });
   
   
   var validate3 = $(".completeForm").validate({
           excluded: ':disabled',
           ignore: [],
           onfocusout: function(e) {
                   this.element(e);                          
           },
           rules : {
               employee_photo : {
                   required :false,
                   accept: "jpg|jpeg|png"
               },
               additional_cost : {
                   required : function(){
                     return $(document).find('#note').val().length > 0;
                   },
                   number : true,
                   min:0
                   
               },
               note : {
                   required : function(){
                     return $(document).find('#additional_cost').val() > 0;
                   }                    
               },
               location_name : {
                   required : true
               }
   
           },
           messages : {
               
               employee_photo : {
                   required : "Please select image",
                   accept : "Only jpg,jpeg and png are supported"
               },
               additional_cost : {
                   required : 'Please add cost',
                   min : 'Amount should be greater than zero',
                   number : 'it should contain only numbers'
               },
               note : {
                   required : "please add note for the cost"
               }
           }         
   })   
   
   $(document).on('click','.changeStatusButtonComplete',function(){
       $(document).find('.fileupload-new img').attr("src", BASE_URL+'assets/default-p.png');
       $('.completeForm')[0].reset();
       $('#location_name').attr('readonly', false);
       $.blockUI({
          baseZ: 2000,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
       $(document).find('#changeStatusModalComplete #currentCost').val($(this).attr('data-currentcost'));
       $(document).find('#changeStatusModalComplete #totalCost').val($(this).attr('data-currentcost'));
       $(document).find('#changeStatusModalComplete #id').val($(this).attr('data-id'));
       $(document).find('#changeStatusModalComplete #category').val($(this).attr('data-category'));
       $(document).find('#changeStatusModalComplete .statusChangeModalButton').text($(this).attr('data-btntitle')); 
       var id = $(this).attr('data-id');
       $.ajax({
           url: BASE_URL+'Admin/getAppointmentDetailsBeforeComplete/'+id ,
           type: 'GET',
           dataType: 'json',
           success: function (data) {
               if(data.errcode == 0)
               {
                 
   
                   $(document).find('#upgrade_size').html(data.result.pet_size);
   
                   if(data.result.additional_services_count > 0){
                       $(document).find('.addOnServiceRemaing').html(data.result.additional_services);
                   }
                   else{
                     $(document).find('.addOnServiceRemaing').html('No Add On Service Found');  
                   }
   
                   $(document).find('.completeForm').find('.extensionError').hide(); 
                   $(document).find('#changeStatusModalComplete').modal('show');
                   
                   if(data.boardingexists.locationsName){
                     $(document).find('#changeStatusModalComplete #location_name').val(data.boardingexists.locationsName);
                     $('#location_name').attr('readonly', true);
                   }
               }
               else
               {
                  
               }
               // $.unblockUI();
           },
           error : function(err)
           {
               $(document).find('.breed_id').html('<option value="">No Breed Found</option>');
               // $.unblockUI();
           }
   
       });     
   });
   
   
   $(document).on('change','#upgrade_size',function(){
   
      // alert($(this).find(':selected').attr('data-cost'));
       $(document).find('#upgrade_cost').val(formatMoney($(this).find(':selected').attr('data-cost'), format_Money));
   
       $(document).find('.add_on_service_cost').val(formatMoney(0, format_Money));
   
       
       updateAddOnCost();
   
       updateGlobalPrice();
   
   });
   
   
   
   
   $(document).on('change','.addonCheckbox',function(){
   
       $(document).find('.add_on_service_cost').val(formatMoney(0, format_Money));
   
      
   
       updateAddOnCost();
   
       updateGlobalPrice();
   
   
   });
   
   $(document).on('change paste keyup','#additional_cost',function(){
   
   
       // if($('#additional_cost').length > 0 && $('#additional_cost').validate())
       // {
       //     $( "#note" ).rules( "add", {
       //       requried: true
       //     });
       //     updateGlobalPrice();
       // }
       // else
       // {
       //     $( "#note" ).rules( "add", {
       //       requried: false
       //     });
       //     updateGlobalPrice();
       // }
   
   
       updateGlobalPrice();
   
   });
   
   
   $(document).on('change paste keyup','#note',function(){
       
       // if($(this).val().length > 0){
       //     $( "#additional_cost" ).rules( "add", {
       //           requried: true
                 
       //     });
       // }
       // else
       // {
       //     $( "#additional_cost" ).rules( "add", {
       //           requried: false
                 
       //     });
       // }    
   })
   
   
   $(document).find('.UploadimageRemove').on('click',function(){
       $(document).find("input[name='avatar_data']").val('');
       $('.fileupload').fileupload('clear');
       $(document).find('.fileupload-new img').attr("src", BASE_URL+'assets/default-p.png');
   });
   
   $(document).find(".completeForm .statusChangeModalButton1").on('click',function(e){
   
          
       if($('.completeForm').valid()) {
             e.preventDefault();
             var avatar = $(document).find("input[name='avatar_data']").val();
             var ImageWidth = 0;
             var ImageHeight = 0;
             var img1 = new Image();
             img1.src = avatar;
             ImageWidth = img1.width;
             ImageHeight = img1.height;
             $('.completeForm').find('.Uploadimage').hide();
             $('.completeForm').find('.extensionError').hide();
   
             // if(ImageWidth < 300 || ImageHeight < 300){
             //   $(document).find('#checkImageValid').attr('disabled',true);
             //   $('.sizeErrMSg').html('Image size should be minimum 300px x 300px.');
             //     $('#sizeModal').modal('show');
             //     return;
             // }
             
             if (ImageWidth>1000 && ImageHeight>1000) {
               var imageSize = new Array();
               imageSize = resizeImageSize(ImageWidth,ImageHeight);
               ImageWidth = imageSize[0];
               ImageHeight = imageSize[1];
               if (ImageWidth>1500) {ImageWidth = 1500;}
               if (ImageHeight>1100) {ImageHeight = 1100;}
             }
             // We create a canvas and get its context.
             var newCanvas = document.createElement("canvas");
   
             //var newCanvas = $('#newcanvas');
             var ctx = newCanvas.getContext('2d');
   
             // We set the dimensions at the wanted size.
             newCanvas.width = ImageWidth;
             newCanvas.height = ImageHeight;
   
             // We resize the image with the canvas method drawImage();
             var img = new Image();
             var returnSRC;
             var FlagCheck = 0;
             if(avatar.length == 0){
                 avatar='<?php echo base_url();?>assets/default-p.png';
                 FlagCheck=1;
   
             }
             img.src = avatar;
             img.onload = function(){
               if (FlagCheck == 0) {
   
                   ctx.drawImage(img, 0, 0, ImageWidth, ImageHeight);
           
                   if (ImageWidth < 500 && ImageHeight < 500) {
                     font_size = '12px';
                   }else{
                     font_size = '20px';
                   }
                   company_nameWidth = Math.round(getWidthOfText(company_name, 'Arial', font_size));
                   image_logoWidth = 0;
                   company_name_x = 5;
                   font_height = 30;
                   if (image_logo.length > 0) {
                     image_logoWidth = 30;
                     company_name_x = 40;
                     font_height = 40;
                   }
                   white_box_width = (5+image_logoWidth+5+company_nameWidth+5);
                   //set background color
                   ctx.fillStyle = "#FFFFFF";
   
                   //draw background / rect on entire canvas
                   
                   if (ImageWidth < 500 && ImageHeight < 500) {
                     ctx.fillRect(0,(ImageHeight-font_height+10),white_box_width-7,font_height+10);
                   }else{
                     ctx.fillRect(0,(ImageHeight-font_height),white_box_width,font_height);
                   }
   
                   ctx.font = font_size+" Arial ";
                   ctx.fillStyle = "black";
   
                   if(image_logo.length > 0) {
                     var img1 = document.createElement('img');
                     var img1 = new Image();
                     img1.src = image_logo;
                     img1.onload = function(){
                       
                       if (ImageWidth < 500 && ImageHeight < 500) {
                         ctx.drawImage(img1, 5, (ImageHeight-27), 22, 22);
                         ctx.fillText(company_name,company_name_x-7,(ImageHeight-10));
                       }else{
                         ctx.drawImage(img1, 5, (ImageHeight-35), 30, 30);
                         ctx.fillText(company_name,company_name_x,(ImageHeight-10));
                       }
                       
                       returnSRC = newCanvas.toDataURL('image/jpeg',0.9);
                       var data = new FormData();
                       var additional_serviceArr = new Array();
                       $(document).find('.addonCheckbox:checked').each(function(){
                          additional_serviceArr.push($(this).val());
                       });
                       data.append('id',$('#id').val());
                       if (FlagCheck == 0) {
                           data.append('file',dataURLtoFile(returnSRC,'one.jpeg'));
                       }
                       
                       data.append('category',$('#category').val());
                       data.append('upgrade_size',$('#upgrade_size').val());
                       data.append('upgrade_cost',$('#upgrade_cost').val());
                       data.append('additional_service',additional_serviceArr);
                       data.append('add_on_service_cost',$('#add_on_service_cost').val());
                       data.append('note',$('#note').val());
                       data.append('additional_cost',$('#additional_cost').val());
                       data.append('location_name',$('#location_name').val());
   
                       var id = $('#id').val();
   
                       if(is_markCompleteOk == 0){
                         $("#changeStatusModalComplete").modal('hide');
                         $('#markCompleteConfirmation').modal('show');
                       }else{
   
                          $.blockUI({
                          baseZ: 2000,
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
   
                        $.ajax({
   
                            url: BASE_URL+'Admin/completeAppointmentAjax',
                            type: 'POST',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            async : false,
                            data:data,
                            success : function(data) {
                                // $.unblockUI();
                                // if(boarding_id.length>0){
                                //    window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                                // }else{
                                   window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                                // }
                                
                            },
                            error : function(request,error)
                            {
                                // $.unblockUI();
                                // if(boarding_id.length>0){
                                //    window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                                // }else{
                                   window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                                // }
                            }
                        });
                       }
                     }
                   }else{
   
                       ctx.fillText(company_name,company_name_x,(ImageHeight-10));
                       returnSRC = newCanvas.toDataURL('image/jpeg',0.9);
                       var data = new FormData();
                       var additional_serviceArr = new Array();
                       $(document).find('.addonCheckbox:checked').each(function(){
                          additional_serviceArr.push($(this).val());
                       });
                       data.append('id',$('#id').val());
                       if (FlagCheck == 0) {
                           data.append('file',dataURLtoFile(returnSRC,'one.jpeg'));
                       }
                       
                       data.append('category',$('#category').val());
                       data.append('upgrade_size',$('#upgrade_size').val());
                       data.append('upgrade_cost',$('#upgrade_cost').val());
                       data.append('additional_service',additional_serviceArr);
                       data.append('add_on_service_cost',$('#add_on_service_cost').val());
                       data.append('note',$('#note').val());
                       data.append('additional_cost',$('#additional_cost').val());
                       data.append('location_name',$('#location_name').val());
   
                       var id = $('#id').val();
   
                       if(is_markCompleteOk == 0){
                         $("#changeStatusModalComplete").modal('hide');
                         $('#markCompleteConfirmation').modal('show');
                       }else{
   
                          $.blockUI({
                          baseZ: 2000,
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
   
                        $.ajax({
   
                            url: BASE_URL+'Admin/completeAppointmentAjax',
                            type: 'POST',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            async : false,
                            data:data,
                            success : function(data) {
                                // $.unblockUI();
                                // if(boarding_id.length>0){
                                //    window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                                // }else{
                                   window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                                // }
                            },
                            error : function(request,error)
                            {
                                // $.unblockUI();
                                // if(boarding_id.length>0){
                                //    window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                                // }else{
                                   window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                                // }
                            }
                        });
                       }
                   }
   
                     // if (ImageWidth < 1080 || ImageHeight<1080) {
                     //   ctx.filter = 'blur(7px)';
                     //   ctx.drawImage(img, 0, 0, 1080, 1080);
                     //   ctx.filter = 'blur(0px)';
                     // }
                     // ctx.drawImage(img, startX, startY, ImageWidth, ImageHeight);
                     // returnSRC = newCanvas.toDataURL('image/jpeg',0.9);
               }else{
                   var data = new FormData();
                   var additional_serviceArr = new Array();
                   $(document).find('.addonCheckbox:checked').each(function(){
                      additional_serviceArr.push($(this).val());
                   });
                   data.append('id',$('#id').val());
                   data.append('category',$('#category').val());
                   data.append('upgrade_size',$('#upgrade_size').val());
                   data.append('upgrade_cost',$('#upgrade_cost').val());
                   data.append('additional_service',additional_serviceArr);
                   data.append('add_on_service_cost',$('#add_on_service_cost').val());
                   data.append('note',$('#note').val());
                   data.append('additional_cost',$('#additional_cost').val());
                   data.append('location_name',$('#location_name').val());
   
                   var id = $('#id').val();
   
                   if(is_markCompleteOk == 0){
                     $("#changeStatusModalComplete").modal('hide');
                     $('#markCompleteConfirmation').modal('show');
                   }else{
   
                      $.blockUI({
                      baseZ: 2000,
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
   
                    $.ajax({
   
                        url: BASE_URL+'Admin/completeAppointmentAjax',
                        type: 'POST',
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        async : false,
                        data:data,
                        success : function(data) {
                            // $.unblockUI();
                           // if(boarding_id.length>0){
                           //     window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                           //  }else{
                               window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                            // }
                        },
                        error : function(request,error)
                        {
                            // $.unblockUI();
                            // if(boarding_id.length>0){
                            //    window.location.href = BASE_URL+"Admin/viewBoarding/"+boarding_id;
                            // }else{
                               window.location.href = BASE_URL+"Admin/viewAppointment/"+id;
                            // }
                        }
                    });
                   }
               }
                 
             }
       }
       else {
       }
       return false;
   
   });
   
   
   
        
   
   
       $('.c_resize').cropper({
           //// aspectRatio: 1,
           minCropBoxWidth: 300,
           minCropBoxHeight: 300,
           crop: function(e) {
           }
       });                     
   
   
       $('input[name="employee_photo"]').click(function(){
           //alert('dasda');
           $(this).val(null);
       });
   
       $('input[name="employee_photo"]').change(function(){
           //alert('dasda');
           if($(this)[0].files.length <= 0){
                   return;
               }
               var f = $(this)[0].files[0];
               var file_Size = $(this)[0].files[0].size;
               var reader = new FileReader();
               
               // Closure to capture the file information.
             reader.onload = (function(file) {
                 return function(e) {
                   var image = document.createElement('img');
                       image.addEventListener('error', function() {
                               $(document).find('#changeStatusModalComplete').modal('hide');
                               $('#extensionModal').modal('show');
                               return;
                       });
                       image.addEventListener('load', function() {
                           if(image.width < 300 || image.height < 300){
                             $(document).find('#checkImageValid').attr('disabled',true);
                             $('.sizeErrMSg').html('Image size should be minimum 300px x 300px.');
                             $(document).find('#changeStatusModalComplete').modal('hide');
                               $('#sizeModal').modal('show');
                               return;
                           }
                           else
                           {
                             $(document).find('#checkImageValid').attr('disabled',false);
                           }
                           if(file_Size  > 60000000){
                             $(document).find('#checkImageValid').attr('disabled',true);
                             $('.sizeErrMSg').html('The maximum size allowed is 60 MB');
                             $(document).find('#changeStatusModalComplete').modal('hide');
                             $('#sizeModal').modal('show');
                             return;
                           }
                           else
                           {
                             $(document).find('#checkImageValid').attr('disabled',false);
                           }
                           $('#cropModal').on('shown.bs.modal', function() {
                     $('.c_resize').cropper('replace', e.target.result);
                 });
                 $('.c_resize').prop('src', e.target.result);
   
                     $('#cropModal').modal('show');
                     });
                     image.src = e.target.result;
                 }
             })(f);
        
               // Read in the image file as a data URL.
               reader.readAsDataURL(f);
   
       });
   
       $('.setDefaultImgBtn').click(function(){    
           $('#cropModal').modal('hide');
           $('#extensionModal').modal('hide');
           $('#sizeModal').modal('hide');
   
           
   
   
             //$(document).find('#changeStatusModalComplete').modal('show');
             var url = $('.user-image .fileupload-preview img').prop('src');
             if(url && url.length > 1){
               //$('input[name="employee_photo"]').val(url);
             }
             //alert('here');
             
   
       });
   
   
         $('#cropModal').on('hidden.bs.modal', function () {
   
               $(document).find('#changeStatusModalComplete').modal('show')
         })
   
         $('#extensionModal').on('hidden.bs.modal', function () {
   
             $(document).find('#changeStatusModalComplete').modal('show')
         })
   
         $('#sizeModal').on('hidden.bs.modal', function () {
   
             $(document).find('#changeStatusModalComplete').modal('show')
         })
   
       $('.cropSaveBtn').click(function(){
           var canvas = $('.c_resize').cropper('getCroppedCanvas');
           $('.resized').html(canvas);
           var url = $('.resized canvas')[0].toDataURL('image/jpeg',0.6);
                
               $('.user-image .fileupload-preview img').prop('src', url);
               $('.user-image .fileupload-new img').prop('src', url);
               $('#cropModal').modal('hide');
           $('input[name="avatar_data"]').val(url);
           //$(document).find('#changeStatusModalComplete').modal('show');
       });
   
       $("#checkoutForm").validate({
   
           excluded: ':disabled',
           rules: {
               manual_discount : { 
                   number:true,
                   max: function(){
                       return parseFloat($(document).find('.balance_amount').val());
                   }
               },
               manual_discountPer : {
   
                   number:true,
                   max:100
               }    
           },
           messages : {
           
           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown',
           errorPlacement: function(error, element) {
               if (element.parent('.input-group').length) {
                   error.insertAfter(element.parent());
               } else {
                   console.log(element.context.name);
                   
                   if(element.parent('.moduleCheckbox').length > 0){
                      
                       //console.log(element.parent('label').parent('div').html());
                       error.insertAfter(element.closest('.form-group').find('.checkError'));
                       //$(document).find('.checkError').show();
                   }
                   else {
                       error.insertAfter(element.closest('.errordiscount'));
                   }
                   
               }
           }
       });
   
   
   
   
   
   
   
   // $(document).on('click','.viewcompletedappointment',function(){
       
   //     $(document).find('#activecompleteappointments').modal('hide');
       
   //     setTimeout(function(){ 
   //        $.unblockUI();
   //     }, 100);
   
   // });
       
       $(document).on('click','.calculateButton',function(){
            
           if($("#checkoutForm").valid()) {  
               var autodiscount_id = $('#auto_discount').val();
               $(document).find('#checkoutModal .currentdiscount_id').val(autodiscount_id);
   
               var app_user_id = $(document).find('#checkoutModal .appointment_app_user_id').val();
               $.blockUI({
                  baseZ: 2000,
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
               });
                
                   
                   $.ajax({
                     url : BASE_URL+'Admin/getFinalInvoice',
                     type: 'POST',
                     dataType: 'json',
                     async : false,
                     data : {
                       appointment_id : function(){
                         return $('.appointment_id').val();
                       },
                       category : 'Grooming',
                       autodiscount_id : autodiscount_id,
                       additional_cost1 : function()
                       {
                         return $('#additional_cost1').val();
                       },
                       additional_note1 : function()
                       {
                         return $('#additional_note1').val();
                       },
                       manual_discount : function(){
                          return $('#manual_discount').val();
                       },
                       manual_discountPer : function(){
                         return $('#manual_discountPer').val();
                       },
                       app_user_id : app_user_id,
                       allappointments :  checkedappointmentdetails
                     },
                     success : function(data)
                     {
                         if(data.errcode == 0)
                           {
                               // console.log(data);
                              $('.totalCostDiv').html('');
                              $('.adjustmentDiv').html('');
                              $('.calculateButton').hide();
                              $('.checkoutButton').show();
                              $(document).find('#checkoutModal .printModalBtn').hide();
                              $(document).find('#checkoutModal .uploadModalBtn').hide();
   
                              var details = data.result; 
                              var html ='';
                              var html1 = '';
                               if(details.allAppointments !=undefined){
   
                               
                             html +=' <div class="container col-sm-12 col-md-12 "><div id="accordion" >'+
                             '<div class="acc_div">';
                                 
                                   
                                $.each(details.allAppointments, function( key0, value0 ) {
                                   if(value0.category != 'appointment'){
                                       value0.cost = value0.amount;
                                   }
                                   if(value0.category == 'boarding' && value0.overstayAmt != 0){
                                       value0.cost= parseFloat(value0.cost)+parseFloat(value0.overstayAmt);
                                      
                                   }
                                   if(value0.additional_cost1 > 0){
                                       value0.cost= parseFloat(value0.cost)+parseFloat(value0.additional_cost1);
                                   }
                                   if(details.allAppointments.length == 1){
                                       var divclass = "collapse in";
                                   }else{
                                       var divclass = "collapse";
                                   }
                                     html +='<div class="card" >';
                                     html +='<div class="card-header redirecttohref" data-id="'+value0.id+'" style="margin: 5px 0px;background-color:#f0f4f5;padding:5px;cursor: pointer;"  >'+
                               
                                            '<span class="card-link" style="color: #646464;font-weight: 500;">'+value0.petname+'<i class="fas fa-caret-right" style="float: right;margin-top:2px;"></i><span class=" " style="float: right;margin-right: 20px;">$ '+Number(value0.cost).toFixed(2)+'</span></span>'+
                                           '</div>'+
                                       '<div id='+value0.id+' class="'+divclass+' accordion-content">';
                                        
                                       if(value0.category == 'appointment'){
                                           html +='<hr>';
                                          
                                       $.each(value0.appointmentServiceInfo, function( key1, value1 ) {
   
                                               html +='<div class="form-group margin row">'+
                                                       '<div class="col-sm-8">'+value1.service_name+'</div>'+
                                                       '<div class="col-sm-4 appointmentcosting">$ '+value1.service_cost+'</div>'+
                                                   '</div>';
                                       });
                                       html +='<hr>';
                                       if(value0.addonappointmentServiceInfo != ''){
                                         
                                       
                                           $.each(value0.addonappointmentServiceInfo, function( key2, value2 ) {
                                               
                                                       html +='<div class="form-group margin row">'+
                                                       '<div class="col-sm-8">'+value2.service_name+'</div>'+
                                                       '<div class="col-sm-4 appointmentcosting">$ '+value2.service_cost+'</div>'+
                                                       '</div>';
                                               
                                           });
                                            html +='<hr>';
                                       }
                                        if(value0.additional_cost > 0 || value0.additional_cost1 > 0){
                                           
                                           if(value0.additional_cost > 0){
                                               if(value0.note == ''){
                                                   value0.note = 'Additional Cost';
                                               }
                                               html +='<div class="form-group margin row">'+
                                                   '<div class="col-sm-8">'+value0.note+'</div>'+
                                                   '<div class="col-sm-4 appointmentcosting">$ '+value0.additional_cost+'</div>'+
                                               '</div>';
                                           }
                                           if(value0.additional_cost1 > 0){
                                               if(value0.additional_note1 == ''){
                                                   value0.additional_note1 = 'Additional Cost';
                                               }
                                              
                                               html +='<div class="form-group margin row">'+
                                                   '<div class="col-sm-8">'+value0.additional_note1+'</div>'+
                                                   '<div class="col-sm-4 appointmentcosting">$ '+value0.additional_cost1+'</div>'+
                                               '</div>';
                                           }
                                           html +='<hr>';
                                        } 
   
   
                                       }else{
                                            html +='<hr>';
                                           
   
                                           html +='<div class="form-group margin row">'+
                                                       '<div class="col-sm-8">'+value0.category.charAt(0).toUpperCase()+value0.category.slice(1).toLowerCase()+' </div>'+
                                                       '<div class="col-sm-4 appointmentcosting">$ '+value0.amount+'</div>'+
                                                   '</div>';
   
                                           if(value0.category == 'boarding' && value0.overstayAmt != 0){
                                               html +='<div class="form-group margin row">'+
                                                       '<div class="col-sm-8">'+value0.overstayLabel+'</div>'+
                                                       '<div class="col-sm-4 appointmentcosting">$ '+value0.overstayAmt+'</div>'+
                                                   '</div>';
                                           }
                                            html +='<hr>';
                                       if(value0.additional_cost > 0 || value0.additional_cost1 > 0){
                                          
                                           if(value0.additional_cost > 0){
                                               if(value0.note == ''){
                                                   value0.note = 'Additional Cost';
                                               }
                                               html +='<div class="form-group margin row">'+
                                                   '<div class="col-sm-8">'+value0.note+'</div>'+
                                                   '<div class="col-sm-4 appointmentcosting">$ '+value0.additional_cost+'</div>'+
                                               '</div>';
                                           }
                                           if(value0.additional_cost1 > 0){
                                               if(value0.additional_note1 == ''){
                                                   value0.additional_note1 = 'Additional Cost';
                                               }
                                               html +='<div class="form-group margin row">'+
                                                   '<div class="col-sm-8">'+value0.additional_note1+'</div>'+
                                                   '<div class="col-sm-4 appointmentcosting">$ '+value0.additional_cost1+'</div>'+
                                               '</div>';
                                           }
                                           html +='<hr>';
                                        } 
   
                                       }
                                       html +=  '</div>';
                                         
                                       });
                                  html +=  '</div></div></div>';
                                }
                                html +=  '</div></div>';
                               html +='<div class="" style="font-weight: 500;">'+
                                   '<div class="col-sm-8" style="">Total Cost</div>'+
                                   '<div class="col-sm-4 appointmentcosting">$ '+details.cost+'</div>'+
                               '</div>'; 
                              
                               var lineflag = 0;
                               html1 +='<hr><div class="form-group margin row">'+
                                   '<div class="col-sm-8" style="font-weight: 500;">ADJUSTMENT</div>'+
                                   '<div class="col-sm-4"></div>'+
                               '</div><hr>';
                               if(details.deposit > 0){
                                   html1 +='<div class="form-group margin row">'+
                                           '<div class="col-sm-8">Deposit</div>'+
                                           '<div class="col-sm-4">$ '+details.deposit+'</div>'+
                                       '</div>';
                                   lineflag++;
                               }
                               if(details.discount_amount > 0){
                                   var grpdiscount = details.discount_amount;
                                   html1 +='<div class="form-group margin row">'+
                                       '<div class="col-sm-8">Group Discount </div>'+
                                      
                                       '<div class="col-sm-4">$ '+ Number(grpdiscount).toFixed(2)+'</div>'+
                                      
                                   '</div>';
                                   lineflag++;
                               } 
   
                               if(details.manual_discount > 0){
                                   html1 +='<div class="form-group margin row">'+
                                       '<div class="col-sm-8">Manual Discount </div>'+
                                       '<div class="col-sm-4">$ '+details.manual_discount+'</div>'+
                                   '</div>';
                                   lineflag++;
                               } 
                               if(details.credited_amount > 0){
                                   html1 +='<div class="form-group margin row">'+
                                       '<div class="col-sm-8">Credits </div>'+
                                       '<div class="col-sm-4">$ '+details.credited_amount+'</div>'+
                                   '</div>';
                                   lineflag++;
                               }
                               if(lineflag > 0){
                                   html1 +='<hr>';
                               }
                               html1 +='<div class="form-group margin row" style="font-weight: 500;">'+
                                   '<div class="col-sm-8">Payable Amount</div>'+
                                   '<div class="col-sm-4">$ '+details.remaining_cost+'</div>'+
                               '</div><hr>';
   
                               if(details.remaining_cost>0){
                                   html1 +='<div class="form-group margin PaySourceBtn">'+
                                       '<label for="field-1" class="col-sm-6 control-label" style="text-align:left">Source</label>'+
                                       '<div class="col-sm-6 drop-down">'+
                                           '<select class="form-control " name="payment_source" id="paymentSource">'+
                                             '<option value="Cash">Cash</option>'+
                                             '<option value="Credit/Debit">Credit/Debit</option>'+
                                             '<option value="Other">Other</option> '+
                                           '</select>'+
                                       '</div>'+
                                   '</div>'+
                                   '<div class="form-group margin otherSourceDiv" style="display: none;">'+
                                       '<label for="field-1" class="col-sm-6 control-label" style="text-align:left">Other Source <span class="required"></span></label>'+
                                       '<div class="col-sm-6">'+
                                           '<input name="other_source" id="other_source" class="form-control" value=""  type="text">'+
                                       '</div>'+
                                   '</div>';
                               }
                               else if(details.remaining_cost< 0){
                                   html1 +='<div class="form-group margin">'+
                                       '<label for="field-1" class="col-sm-6 control-label" style="text-align:left">Method</label>'+
                                       '<div class="col-sm-6 drop-down">'+
                                           '<select class="form-control " name="payment_source">'+
                                             '<option value="Refund">Refund</option>'+
                                             '<option value="Credit">Credit</option>'+
                                           '</select>'+
                                       '</div>'+
                                   '</div>';
                               }
                              
                              $('.totalCostDiv').html(html);
                              $('.adjustmentDiv').html(html1);
                              $('.step2').show();
                              $('.step1').hide();
                        }
                         else
                         {
   
                         }
                     },
                     error : function(error)
                     {
                         //$.unblockUI();
                         console.log(error);
                     }
                   });
   
              
                // $.unblockUI(); 
              
           }
       });
   $(document).on('click','.redirecttohref',function(){
   
    
       var accordion = $(this);
       var accordionContent = accordion.next('.accordion-content');
       
       // toggle accordion link open class
       accordion.toggleClass("open");
       // toggle accordion content
       accordionContent.slideToggle(500);
       
   });
   
   });
   
   
</script>
<script type="text/javascript">
   var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";
    function get_date(start_time){
       var H = +start_time.substr(0, 2);
       //alert(H);
       var h = (H % 12) || 12;
       var ampm = H < 12 ? " AM" : " PM";
       start_time = h + start_time.substr(2, 3) + ampm;
       return start_time;  
     }
    
      
   $(document).ready(function(){
      
       
       var datepicker_today = '0';
          
     //   if(date_today < moment().format('YYYY-MM-DD')){
     //       datepicker_today = '-1d';
     //   }
     //   else if(date_today > moment().format('YYYY-MM-DD')){
     //       datepicker_today = '+1d';
     //   }
     // $('#datepicker').datepicker({
     //     startDate: datepicker_today,
     //     autoclose: true,
     //     format: format
     //     //daysOfWeekDisabled: shopWorkingDays
     // }).
     // on('changeDate', function(ev) {
     //     //validate on change function
     //       $.ajax({
     //           url: BASE_URL+'Admin/getGroomersOnDate',
     //           type: 'Post',
     //           dataType: 'json',
     //           async : false,
     //           data: {
     //               date:function(){
     //                   return $('#datepicker').val()
     //               }
     //           },
     //           success: function (data) {
     //               $('#user_id').html(data.result); 
     //           },
     //           error : function(err)
     //           {
     //               $(document).find('.pet_list').show();
     //           }
     //       });
     //     if($('.timepicker1').val().length > 0)
     //     {
     //         $(".timepicker1").removeData("previousValue");
     //         $('#moveAppointmentForm').validate().element('.timepicker1');
          
     //     }
   
     //     if($('#datepicker').valid()){
   
     //        $('#datepicker').removeClass('invalid').addClass('success');   
     //     }
   
     //     $(document).find('.AppointmentsDiv').hide();
     //     $(document).find('.remarksDiv').hide();
     //     $(document).find('.submitButton').hide();
     //     $(document).find('.CheckButton').show();
   
     // });
     // $(".timepicker1").timepicker({
     //        showInputs: false,
     //        minuteStep:15,
     //        defaultTime: timepickerTime
     //     }).on('hide.timepicker', function(e) {
   
     //         $(document).find('.AppointmentsDiv').hide();
     //         $(document).find('.remarksDiv').hide();
     //         $(document).find('.submitButton').hide();
     //         $(document).find('.CheckButton').show();
   
             
             
     //         var new_time = moment(e.time.value, ["h:mm A"]).format();
     //         new_time = moment(new_time).add($('#time_def').val(), 'minutes').format();
     //         var end_time = moment(new_time).format('hh:mm a');
             
     //         // alert(end_time);
     //         $(document).find('#appointment_end_time').val(end_time);
     //         $(document).find('.A_time_end').val(end_time);
   
     //         $(".timepicker1").removeData("previousValue");
     //         $('#moveAppointmentForm').validate().element('.timepicker1');
   
     //         if($(".timepicker1").valid())
     //         {
     //             $('.timepicker1').removeClass('invalid').addClass('success');
     //         }
             
     // });
   
   
   
   $(document).on('change','#user_id',function(){
   
       //alert('#user_id');
       $(".timepicker1").removeData("previousValue");
       $('#moveAppointmentForm').validate().element('.timepicker1');
   
       if($(".timepicker1").valid())
       {
           $('.timepicker1').removeClass('invalid').addClass('success');
       }
   
       $(document).find('.AppointmentsDiv').hide();
       $(document).find('.remarksDiv').hide();
       $(document).find('.submitButton').hide();
       $(document).find('.CheckButton').show();
   });
   $('.CheckButton').on('click',function(e) {
     //e.preventDefault();
     // var isUpdated = 1;
     // if($('.hiddenMoveData').attr("data-date") == $('#datepicker').val() && $('.hiddenMoveData').attr("data-userid") == $('#user_id').val() && $('.hiddenMoveData').attr("data-checkintime") == $('#appointment_time').val()){
     //   isUpdated = 0;
     // }
   
     if($('#moveAppointmentForm').valid())
     {
       
         yourArray = [];
         $('input[name="services[]"]:checked').each(function(){
             yourArray.push($(this).val());
         });
         
          $.blockUI({
               baseZ: 2000,
                 css: {
                       border: 'none',
                       padding: '15px',
                       backgroundColor: '#000',
                       '-webkit-border-radius': '10px',
                       '-moz-border-radius': '10px',
                       opacity: .5,
                       color: '#fff' 
                     }
         });
   
         
         $.ajax({
                 url : BASE_URL+'Admin/checkAppointmentValidity',
                 type: 'POST',
                 dataType: 'json',
                 async : false,
                 data : {
                         appointment_date : function(){
                             return $('#datepicker').val();
                         },
                         appointment_end_time : function()
                         {
                             return $('#appointment_end_time').val();
                         },
                         appointment_time : function(){
                              return $('#appointment_time').val();
                         },
                         services : yourArray, 
                         app_user_id : function(){
                             return $('#app_user_id').val();
                         }, 
                         pet_id : function(){
                            return $('#pet_id').val(); 
                         },
                         user_id : function (){
                             return $('#user_id').val();
                         },
                         appointment_id : function(){
                            return $('#appointment_id').val();
                         }    
                     },
                 success : function(data)
                 {
                     // $.unblockUI();
                     console.log(data);
                     if(data.error == 0)
                     {
                         $(document).find('.AppointmentsTable').html('');
                           if(data.result.already_appointment_exist == 1)
                           {
                                $(document).find('.submitButton').hide();
                                $(document).find('.remarkInput').val('Already Booked Appointment');
   
                                $(document).find('.remarksDiv').show();
                           }
                         else if(data.result.shop_appointment_end_time_exceeded == 1){
                           $(document).find('.submitButton').hide();
                             $(document).find('.remarkInput').val('Slot is not available for appointment ');
                             $(document).find('.overBookButton').show();
                             $(document).find('.remarksDiv').show();
                             $(document).find('#allocated_schedule_id').val(0);
                             $(document).find('#overlapping_appointment').val(1);
   
                         }
                         else if (!jQuery.isEmptyObject(data.result.normal_booking_time) || data.result.normal_booking_time.length > 0)
                         {
                             //alert('normal booking');
                             $(document).find('.remarkInput').val('Slot is available for appointment');
                             $(document).find('.remarksDiv').show();
                             $(document).find('#overlapping_appointment').val(0);
                             //$(document).find('input[name="services[]"]').val(data.result.normal_booking_time.order);
                             $(document).find('#allocated_schedule_id').val(data.result.normal_booking_time.allocated_schedule_id);
                             $(document).find('.submitButton').hide();
                             $(document).find('.ConfirmButton').show();
   
                         }
                         else if(!jQuery.isEmptyObject(data.result.appointments) || data.result.appointments.length > 0)
                         {
                             //Show Appointments Table
                              //alert(data.result.appointments);
                             html = '';
                             $.each( data.result.appointments, function( key, value ) {
                                 html += '<tr><td class="vertical-td">'+value['username']+'</td><td class="vertical-td">'+value['petName']+'</td><td class="vertical-td">'+value['service']+'.</td><td class="vertical-td">'+value['appointment_date']+'</td class="vertical-td"><td>'+value['appointment_time']+'</td><td class="vertical-td">'+value['appointment_type']+'</td></tr>';
                             });
                             $(document).find('.AppointmentsTable').html(html);
                             //$(document).find('.overlapping_appointment').val(1);
                             $(document).find('.submitButton').hide();
                             $(document).find('.remarkInput').val('Slot is not available for appointment ');
                             $(document).find('.overBookButton').show();
                             $(document).find('.remarksDiv').show();
                             $(document).find('.AppointmentsDiv').show();
                             $(document).find('#allocated_schedule_id').val(0);
                             $(document).find('#overlapping_appointment').val(1);
   
                         }
                             
                         else
                         {
   
                         }
   
                     }
                     else
                     {
   
                     }
                 },
                 error : function(error)
                 {
                     // $.unblockUI();
                     console.log(error);
                 }    
   
             });
       }
     return false;
   });
   
   
   
   $(document).on('click','.overBookButton',function(){
       $(document).find('overlapping_appointment').val(1);
       // $(document).find('#user_email').prop('disabled',false);    
       $(document).find('#user_phone_no').prop('disabled',false);
       $(document).find('#moveAppointment').get(0).submit();
   });
   
   $(document).on('click','.ConfirmButton',function(){
       $(document).find('overlapping_appointment').val(0);
       // $(document).find('#user_email').prop('disabled',false);    
       $(document).find('#user_phone_no').prop('disabled',false);
       $(document).find('#moveAppointment').get(0).submit();
   });
   $("#moveAppointmentForm").validate({
           excluded: ':disabled',
           onfocusout: function(e) {
                   this.element(e);                          
           },
           rules: {
               user_id: {
                   required : true
               },
               appointment_time : {
                   required : true,
                   remote: {
                       url : BASE_URL+'Admin/checkTimeValidtiyForMove',
                       type: 'POST',
                       dataType: 'json',
                       async : false,
                       data : {
                           appointment_date : function(){
                               return $('#datepicker').val();
                           },
                           appointment_end_time : function()
                           {
                               return $('#appointment_end_time').val();
                           },
                           user_id : function(){
                               return $('#user_id').val();
                           },
                           services : function(){
                               yourArray = [];
                               $('input[name="services[]"]:checked').each(function(){
                                   yourArray.push($(this).val());
                               });
   
                               return yourArray;
   
                           },
                           pet_id : function (){
                               return $('#pet_id').val();
                           },
                           appointment_id : function(){
                                 return $('#appointment_id').val();
                           }   
                       },
                       dataFilter : function(data) {
                         var json = JSON.parse(data);
                         if(json.err == 1000) {
                             return "\"" + json.message + "\"";
                         } else {
                             return true;
                         }
   
                       },    
                       
                   }
               },
               // name : {
               //     required : true
               // },
               // type : {
               //     required: true
               // },
               // size : {
               //     required : true
               // },
           },
   });
    $(document).on('click','.moveButton',function(){
      // alert($('.dateapp').attr('data-date'));
               var appointment_date = $('.dateapp').attr('data-date');
               //alert(appointment_date);
               var user_id = $(this).attr('data-userId');
               var appointmentId = $(this).attr('data-appointmentId');
               var  start_time = $(this).attr('data-start');
               var  end_time = $(this).attr('data-end');
               var formateddate =  $(this).attr('data-formateddate');
               // alert(start_time);
               $.ajax({
   
                   url: BASE_URL+'Admin/checkGroomerAvailability',
                   type: 'POST',
                   dataType: 'json',
                   async : false,
                   data: {
                     date : appointment_date,
                     user_id : user_id,
                     start_time : start_time,
                     end_time : end_time,
                     id : appointmentId,
                   },
                   success : function(data) { 
                       // alert(data.time_def);
                     $html = '';
                     $html += '<select class="form-control" name="user_id" id="user_id">'+
                             '<option value="">Select Groomer</label>'+
                         '</option>';
                     $.each(data.groomers, function (i, value) {
                         $html +='<option value="'+value['id']+'" >'+value.first_name+' '+value.last_name+'</label>'+
                         '</option>';
                     });                                          
                     
                     if (data.groomers.length < 1 ) {
                       $html += '<option value="" style="color: #E13300">Groomer is not available</label></option>';       
                     }
                      $html += '</select>';
                     $('.groomerSelect').html($html);
                     setTimeout(function(){ 
                        $(document).find('#user_id').val(user_id);
                     }, 300);
                   
                     $('#moveAppointment').modal('show');
   
                     $('#app_user_id').val(data.appointment.app_user_id);
                     $('#app_user_name').val(data.appointment.username);
   
                     $('#pet_id').val(data.appointment.pet_id);
                     $('#pet_name').val(data.appointment.petName);
   
                     $(document).find('#appointment_end_time').val(get_date(data.appointment.appointment_end_time));
                     $('.A_time_end').val(get_date(data.appointment.appointment_end_time));
   
                     
                     $('#cost').val(data.appointment.cost);
                     // $('#user_email').val(data.customerdetails.email);
                     $('#user_phone_no').val(data.customerdetails.phone);
                      $html1 ='<label for="field-1" class="col-sm-3  control-label">Services <span class="required">*</span></label>'+
                       '<div class="col-sm-5 checkboxGroup">';
                         $.each(data.services.service_id, function (i, value) {
                           $html1 +='<div class="checkbox">'+
                             '<label class="serviceCheckbox">'+
                               '<input type="checkbox" checked="checked" name="services[]" value="'+value+'" disabled>'+data.services.service_name[i]+'</label>'+
                           '</div>';
                         });    
                         $html1 += '<span class="checkError help-block animated fadeInDown"></span>'+ 
                       '</div>';
                     $('.serviceSelection').html($html1);
                     //alert($html1);
                     // var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                     // var ddSplit = appointment_date.split('-');  //date is mm/dd/yyyy format
                     // $('#datepicker').val(months[ddSplit[1]-1]+ ' ' + ddSplit[2] + ' '+ ddSplit[0]);
                     // $("#datepicker").datepicker( "setDate" , months[ddSplit[1]-1]+ ' ' + ddSplit[2] + ' '+ ddSplit[0]);
                     $('#datepicker').val(formateddate);
                     $("#datepicker").datepicker( "setDate" , formateddate);
                     $('#appointment_time').val(get_date(start_time));
                     $('#appointment_id').val(appointmentId);
   
   
                      $('#time_def').val(data.time_def);
   
                     
                     // $('.hiddenMoveData').attr('data-userid',user_id);
                     // $('.hiddenMoveData').attr('data-date',months[ddSplit[1]-1]+ ' ' + ddSplit[2] + ' '+ ddSplit[0]);
                     // $('.hiddenMoveData').attr("data-checkintime",get_date(start_time));
                   },
             }); // end ajac call
            
         }); //end move button on click function
        $(document).on('click','.checkoutButton',function(){
               
           
           $("#checkoutButtonConfirmation").modal("show");
           $("#checkoutModal").modal('hide');
           
           $("#checkoutForm").submit(function(e){
               e.preventDefault();
           });       
           
           
           // setTimeout(function(){ $.unblockUI(); }, 300);
           
       });
       $(document).on('click','.checkoutButtonConfirmationOk',function(){
           //$.unblockUI();
           $("#checkoutButtonConfirmation").modal("hide");
           //$("#checkoutModal").modal('show');
           // $.blockUI({
           //    baseZ: 2000,
           //      css: {
           //          border: 'none',
           //          padding: '15px',
           //          backgroundColor: '#000',
           //          '-webkit-border-radius': '10px',
           //          '-moz-border-radius': '10px',
           //          opacity: .5,
           //          color: '#fff'
           //      }
           //  });
   
           $(document).find('#checkoutForm').get(0).submit();
   
       });
   
   //new functionality for note and vaccination
   //additional note js
       $(document).on('change','#groomerType', function(){
           $('.petnote').val('');
           if($('#groomerType option:selected').val() == 'behavior_note'){
             var note = $(this).find(':selected').data('value');
             $('.petnote').val(note);
           }
       });
   
         if(window.location.href.indexOf("additional_notes") > -1 || (window.location.href.indexOf("vaccination_records") > -1))
         {
            var url = $(location).attr('href'),
            parts = url.split("/"),
            last_part = parts[parts.length-1];
            if(last_part == 'additional_notes'){
               $('.nav-tabs a[href="#2"]').tab('show');
            }
   
            if(last_part == 'vaccination_records')  
            {
               $('.nav-tabs a[href="#3"]').tab('show'); 
            }
           
         }
   //addvacination 
   var global = 0;
   // var format = "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>";
    var momentFormat = "<?php echo momentFormat(get_option('date_format'))?>";
   
       jQuery.validator.addMethod("greaterThan",
           function(value, element, params) {
               if ($(params).val().length > 0) {
                   if (!/Invalid|NaN/.test(new Date(value))) {
                       return new Date(value) > new Date($(params).val());
                   }
   
                   return isNaN(value) && isNaN($(params).val()) ||
                       (Number(value) > Number($(params).val()));
               } else {
                   return true;
               }
   
   
           }, 'Must be greater than date.');
   
           $('#datepicker0').datepicker({
               autoclose: true,
               format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
           }).
           on('changeDate', function(ev) {
               //validate on change function
               if ($('#datepicker0').valid()) {
   
                   // var temp = moment(data.result.date_of);
                   // var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format("YYYY-MM-DD");
                   // $('#datepicker1').val(ev.date);
                   $('#datepicker0').removeClass('invalid').addClass('success');
               }
           });
   
           $('#datepicker01').datepicker({
               autoclose: true,
               format: "<?php echo dateformat_PHP_to_jQueryUI(get_option('date_format'))?>"
           }).
           on('changeDate', function(ev) {
               //validate on change function
               if ($('#datepicker01').valid()) {
                   $('#datepicker01').removeClass('invalid').addClass('success');
               }
           });
   
   
           var validate3 = $('#addMedication').validate({
           excluded: ':disabled',
           onfocusout: function(e) {
               this.element(e);
           },
           rules: {
               medication_name: {
                   required: true
               },
               date_of: {
                   //required : true
               },
               expiry_on: {
                   required: true,
                //    greaterThan: '#datepicker0',
   
               }
   
           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown',
           errorPlacement: function(error, element) {
               if (element.parent('.input-group').length) {
                   error.insertAfter(element.parent());
               } else {
                   error.insertAfter(element);
               }
           }
       });
   
       $(document).on('click', '.addNoteButton', function() {
         // console.log("SA");
         // console.log($(this).attr('data-action'));
          // alert($(this).attr('data-action'));
          $('#addMedication')[0].reset();
           $('#addModal').modal('open');
           // $(document).find('#addModal .fileupload').fileupload('clear');
           validate3.resetForm();
           $(document).find('#addModal form').attr('action', $(this).attr('data-action'));
          
           $(document).find('#addModal #formType').val($(this).attr('data-form'));
       });
   
   
   
       $('input[name="vaccine_image"]').click(function() {
           $(this).val(null);
           $('input[name="avatar_data1"]').val('');
       });
   
      $('input[name="vaccine_image"]').change(function(){
               $('input[name="avatar_data1"]').val('');
               //$('#addModal').modal('hide');
   
               if($(this)[0].files.length <= 0){
                       return;
                   }
                   var f = $(this)[0].files[0];
                   var file_Size = $(this)[0].files[0].size;
                   var reader = new FileReader();
                   
                   // Closure to capture the file information.
                 reader.onload = (function(file) {
                     return function(e) {
                           var image = document.createElement('img');
                           image.addEventListener('error', function() {
                                   $('#extensionModal1').modal('show');
                                   return;
                           });
                           image.addEventListener('load', function() {
                               if(image.width < 300 || image.height < 300){
                                 $(document).find('#checkImageValid').attr('disabled',true);
                                 $('.sizeErrMSg').html('Image size should be minimum 300px x 300px.');
                                   $('#sizeModal1').modal('show');
                                   return;
                               }
                               else
                               {
                                 $(document).find('#checkImageValid').attr('disabled',false);
                               }
                               if(file_Size  > 60000000){
                                 $(document).find('#checkImageValid').attr('disabled',true);
                                 $('.sizeErrMSg').html('The maximum size allowed is 60 MB');
                                 // $(document).find('#changeStatusModalComplete').modal('hide');
                                 $('#sizeModal').modal('show');
                                 return;
                               }
                               else
                               {
                                 $(document).find('#checkImageValid').attr('disabled',false);
                               }
   
                                   $('.user-image1 .fileupload-preview img').prop('src', e.target.result);
                                   $('input[name="avatar_data1"]').val(e.target.result);
                                   $('#addModal').modal('show');
                                   global++;
                           });
                         image.src = e.target.result;
                     }
                 })(f);
            
                   // Read in the image file as a data URL.
                   reader.readAsDataURL(f);
                   //$('#addModal').modal('show');
   
           });
      $(document).on('click','.deleteButton',function(){
          if(global == 0)
          {
              $(document).find('#deleteModal').modal('show');
              $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
          }
          else
          {
              $('input[name="avatar_data"]').val('');
              $('.fileupload').fileupload('clear');
           }
       });
   
        // this is the id of the form
       $(document).on('submit', '#addMedication', function(e) {
           e.preventDefault();
           if ($(this).valid()) {
               var form = $(this);
               var url = form.attr('action');
                // alert(url);
               $.ajax({
                   type: "POST",
                   url: url,
                   dataType: 'json',
                   data: {
   
                       medication_name: function() {
                           return $('#addMedication').find('#medication_name').val();
   
                       },
                       date_of: function() {
                           return $('#addMedication').find('#datepicker0').val();
                       },
                       expiry_on: function() {
                           return $('#addMedication').find('#datepicker01').val();
                       },
                       notes: function() {
                           return $('#addMedication').find('#notes').val();
                       },
                       vaccine_image: function() {
                           return $("#addMedication").find("input[name='vaccine_image']").val();
                       },
                       avatar_data1: function() {
                           return $('#addMedication').find("input[name='avatar_data1']").val();
                       },
                       formType: function() {
                           return $('#addMedication').find("#formType").val();
                       },
                   },
                   success: function(data) {
   
                       if (data.errcode == 0) {
                          // alert("dsas");
                          // $.unblockUI();
                             if (window.location.href.indexOf("vaccination_records") > -1) {
                                   location.reload();
                                   $('.nav-tabs a[href="#3"]').tab('show'); 
                               }else{
                                 
                                 location = location+'/vaccination_records';
                               }
                        
                           // location.reload();
                       }
                   }
               });
           }
       });
       $('#editdatepicker').datepicker({
         autoclose: true,
         format: format
       }).
       on('changeDate', function(ev) {
         //validate on change function
         if($('#editdatepicker').valid()){
   
             //$('#editdatepicker1').val(ev.date);
               $('#editdatepicker').removeClass('invalid').addClass('success');   
         }
       });
   
       $('#editdatepicker1').datepicker({    
         autoclose: true,
         format: format
       }).
       on('changeDate', function(ev) {
         //validate on change function
         if($('#datepicker1').valid()){
           $('#datepicker1').removeClass('invalid').addClass('success');   
         }
       });
        $(document).on('click','.deleteButton1',function(){
           $(document).find('#deleteModal1').modal('open');
           $(document).find('.deleteButtonModel1').attr('href',$(this).attr('data-href'));
   
           $('.nav-tabs a[href="#3"]').tab('show'); 
         });
   
        $(document).on('click','.editButton',function(){
         $(document).find('#editModal form').attr('action',$(this).attr('data-action'));
           var id = $(this).attr('data-id');
           $.ajax({
             url: BASE_URL+'User/getPetNotesAjax/'+id ,
             type: 'GET',
             dataType: 'json',
             success: function (data) {
             console.log(data.result);
             if(data.err == 0)
             { 
                $(document).find('#editModal').modal('open');
               $(document).find('#editModal #medication_name').val(data.result.medication_name);
               
   
               //$(document).find('#editModal #editdatepicker').val(data.result.date_of);
               var temp = moment(data.result.date_of);
               var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format(momentFormat);
               console.log(momentFormat);
               console.log(temp);
               console.log(date);
               // alert(date);
               $(document).find("#editdatepicker").datepicker("update", date);
               // alert($("#editdatepicker").val());
               
               var temp = moment(data.result.expiry_on);
               var date =  moment(temp.format('YYYY-MM-DD HH:mm:ss')).format(momentFormat);
               console.log(temp);
               console.log(date);
               $(document).find("#editdatepicker1").datepicker("update", date);
   
               
               $(document).find('#editModal #notes').val(data.result.notes);
               $(document).find('#editModal').modal('show');
             }
             else
             {
               $(document).find('#editModal').modal('close');
               console.log('something went wrong');
             }
                   
             },
             error : function(err)
             {
               $(document).find('#editModal').modal('close');
               console.log(err);
             }
         });
       });
          $('#editMedication').validate({
         excluded: ':disabled',
           onfocusout: function(e) {
             this.element(e);                          
           },
           rules: {
             medication_name:{
               required : true
             },
             date_of:{
               //required : true
             },
             expiry_on:{
               required : true,
               greaterThan : '#editdatepicker',
             }
           },
           highlight: function(element) {
                   $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
                   $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown',
           errorPlacement: function(error, element) {
                   if (element.parent('.input-group').length) {
                       error.insertAfter(element.parent());
                   } else {
                       error.insertAfter(element);
                   }
           }
       });
   
       $('.addnoteBtn').on('click', function(e)
       { 
           $('#addGroomerNoteform')[0].reset();
       });
       $(document).on('click','.checkoutModalclose',function(){
          setTimeout(function(){
              $('.sidebar-mini').css('padding', '0px');
          }, 1000);
   
       });
     
        $(".tabforpetappointment").on("click",function(){
        var tableName = $(this).attr("href")+'table';
        setTimeout(function(){ 
            $(tableName).DataTable({
              "responsive":true,
              "bInfo": false,
              "bAutoWidth": false,
              "bSort" : false,
              "paging": false,
              "bFilter" : false,
              "destroy" : true
            });
        }, 400);
      });
       $('#checkoutModal').on('show.bs.modal', function () {
           $('#launcher').hide();
       });
   
       $('#checkoutModal').on('hidden.bs.modal', function () {
           $('#launcher').show();
       });
   
   
        
   
       $(document).on('click','.addNoteButton',function(){
           $('#launcher').hide();
       });
   
       
       $('#addModal').on('hidden.bs.modal', function () {
             $('#launcher').show();
       });
        $(document).on('click','.moveButton',function(){
          $('#launcher').hide();
       });
       $('#moveAppointment').on('hidden.bs.modal', function () {
           $('#launcher').show();
       })
         
   });
</script>
<!-- Script section for Contract print and Upload contract  END -->




