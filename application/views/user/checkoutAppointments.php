<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewPet.css">
<style>
#appHistoryList_filter, #appHistoryList_length, #appHistoryList_info{
    display: none !important;
}
.pagination .page-link {
    color: #605ca8;
    border: 1px solid #605ca8;
}
/* #appHistoryList{
    border: none;
    overflow-x: hidden !important;
} */
.paddingDrop{
    padding-left: 5px;
}
.table td {
    padding: 0.75rem;
    vertical-align: top !important;
    border-top: 1px solid #e3ebf3 !important;
}
.table td {
    border-bottom: none;
}
.table thead th {
    vertical-align: bottom;
    border-top: 1px solid #e3ebf3;
    border-bottom: 1px solid #e3ebf3 !important;
}

</style>
    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Appointment History</h3>
                </div>
            </div>
            <div class="content-body" >
                <section class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body" >
                                    <div class="row">
                                        <div class="col-md-2 form-group" id="selectTriggerFilter">
                                           
                                        </div>

                                        <div class="col-md-2 form-group paddingDrop"  id="selectTriggerFilter1">
                                            
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="appHistoryList" class="table table-hover table-xl mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="border-top-0"></th>
                                                    <th class="border-top-0">Name</th>
                                                    <th class="border-top-0">Type</th>
                                                    <th class="border-top-0">Date</th>
                                                    <th class="border-top-0">Time</th>
                                                    <th class="border-top-0">Status</th>
                                                    <th class="border-top-0">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i=0 ;?>
                                            <?php foreach ($CheckoutAppointments as $key => $value) { $i++?>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="<?php echo ucfirst($value['name']);?>" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="<?php echo base_url(); ?>assets/images/gallery/<?php echo $i; ?>.jpg" alt="Avatar">
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                    <td class="text-truncate"><?php echo ucfirst($value['category']);?></td>
                                                    <td class="text-truncate"><?php echo date('M d Y', strtotime($value['appointment_date']));?></td>
                                                    <td class="text-truncate"><?php echo date('h:i a', strtotime($value['appointment_time']));?> - <?php echo date('h:i a', strtotime($value['appointment_end_time']));?></td>
                                                    <td>
                                                    <?php if($value['status']=='Checkout'){?>
                                                        <div class="badge badge-danger badge-square " style="border-color: #605ca8; background-color: #605ca8;padding-top:7px;"><span ><?php echo 'Checked Out';?></span></div>
                                                    <?php }elseif($value['status']=='Cancel'){?>
                                                        <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Cancelled';?></span></div>
                                                    <?php }?>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <button type="button" class="btn btn-sm round btn-outline-danger editB" style="border-color: #605ca8;background-color: transparent;color: #605ca8; width:60px;" data-toggle="modal" data-target="#default">View</button>
                                                    </td>
                                                </tr>
                                            <?php } ?>    
                                            <?php foreach ($CancelAppointments as $key => $value) {?>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <ul class="list-unstyled users-list m-0">
                                                            <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="<?php echo ucfirst($value['name']);?>" class="avatar avatar-sm pull-up">
                                                                <img class="media-object rounded-circle" src="<?php echo base_url(); ?>assets/images/gallery/6.jpg" alt="Avatar">
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                    <td class="text-truncate"><?php echo ucfirst($value['category']);?></td>
                                                    <td class="text-truncate"><?php echo date('M d Y', strtotime($value['start_date']));?></td>
                                                    <td class="text-truncate"><?php echo date('h:i a', strtotime($value['start_time']));?> - <?php echo date('h:i a', strtotime($value['end_time']));?></td>
                                                    <td class="text-truncate">
                                                    <?php if($value['status']=='Checkout'){?>
                                                    <div class="badge badge-danger badge-square " style="border-color: #605ca8; background-color: #605ca8;padding-top:7px;"><span ><?php echo 'Checked Out';?></span></div>
                                                    <?php }elseif($value['status']=='Cancel'){?>
                                                        <div class="badge badge-danger badge-square" style="padding-top:7px;"><span ><?php echo 'Cancelled';?></span></div>
                                                    <?php }?>
                                                    </td>
                                                    <td class="text-truncate">
                                                        <button type="button" class="btn btn-sm round btn-outline-danger editB" style="border-color: #605ca8;background-color: transparent;color: #605ca8; width:60px;" data-toggle="modal" data-target="#default">View</button>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>  
    
<!-- END: Content-->

<!-- Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Basic Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Check First Paragraph</h5>
                
                <hr>
                <h5>Some More Text</h5>
                <p>Cupcake sugar plum dessert tart powder chocolate fruitcake jelly. Tootsie roll bonbon toffee danish.
                    Biscuit sweet cake gummies danish. Tootsie roll cotton candy tiramisu lollipop candy cookie biscuit pie.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    var columnNo = 0;
    $('#appHistoryList').DataTable( {
        "bSort" : false,
        // "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(columnNo == 2){
                    var select = $('<select class="form-control"><option value="">All Appointment</option></select>')
                    .appendTo( $('#selectTriggerFilter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }

                if(columnNo == 1){
                    var select = $('<select class="form-control"><option value="">All Pets</option></select>')
                    .appendTo( $('#selectTriggerFilter1').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                columnNo++;
            } );
        }
    } );
} );


</script>




