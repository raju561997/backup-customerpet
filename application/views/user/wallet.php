<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/paymentsMethods.css">
<style>
/* #walletList{
    border: none;
    overflow-x: hidden !important;
} */
#walletList_filter,#walletList_length,#walletList_info{
    display: none !important;
}
.table td {
    padding: 0.75rem;
    vertical-align: top !important;
    border-top: 1px solid #e3ebf3 !important;
}
.table td {
    border-bottom: none !important;
}
table.dataTable.no-footer {
    border-bottom: none!important;
}
.table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #e3ebf3 !important;
}
.pagination .page-link {
    color: #605ca8 !important;
    border: 1px solid #605ca8 !important;
}
.page-item.active .page-link {
    z-index: 1 !important;;
    color: #fff !important;;
    background-color: #605ca8 !important;;
    border-color: #605ca8 !important;;
}
.paddingDrop{
    padding-left: 5px;
}

</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/paymentsMethods.css">    
<!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <app-breadcrumb class="col-12">
                    <div class="row">
                        <div class="content-header-left col-md-6 col-12 mb-2 ng-star-inserted">
                            <h3 class="content-header-title mb-0 d-inline-block">Wallet</h3>
                        </div>
                        <div class="content-header-right col-md-6 col-12">
                            <div class="d-inline-block float-md-right">
                                <h3 class="content-header-title mb-0 d-inline-block" >Available Balance: <span style="color:green;">$ <?php echo $appUser[0]['credit_balance'];?></span></h3>
                            </div>
                        </div>
                    </div>
                </app-breadcrumb>
            </div>
           
            <div class="content-body" >
                <section class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body" >
                                    <div class="row">
                                        <div class="col-md-2 form-group" id="selectTriggerFilter">
                                            
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="walletList" class="table table-hover table-xl mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="border-top-0"></th>
                                                    <th class="border-top-0">Service</th>
                                                    <th class="border-top-0">Amount</th>
                                                    <th class="border-top-0">Date</th>
                                                    <th class="border-top-0">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/wallet-topup.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Add Money In Wallet</td>
                                                    <td class="text-truncate">$ 20.00</td>
                                                    <td class="text-truncate">October, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-danger badge-square" style="padding-top:7px;"><span >Failed</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-redeemed.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Payment Made For Daycare</td>
                                                    <td class="text-truncate">$ 20.00</td>
                                                    <td class="text-truncate">September, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-success badge-square " style="padding-top:7px;"><span >Success</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Credit Awarded For Grooming</td>
                                                    <td class="text-truncate">$ 15.00</td>
                                                    <td class="text-truncate">August, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-warning badge-square " style="padding-top:7px;"><span >Pending</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Credit Awarded For  Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">July, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Credit Awarded For  Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">June, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Credit Awarded For  Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">May, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>                         
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script> -->
<script>
$(document).ready(function() {
    var columnNo = 0;
    $('#walletList').DataTable( {
        "bSort" : false,
        "searching" : true,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(columnNo == 3){
                    var select = $('<select class="form-control"><option value="<?php echo date("F, Y") ;?>"><?php echo date("F, Y") ;?></option></select>')
                    .appendTo( $('#selectTriggerFilter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                columnNo++;
            } );
        }
    } );
} );
</script>

<!-- <script>
$(document).ready(function() {
    $('#walletList').dataTable({
        // "responsive": true,
        "bSort" : false,
        "searching" : true,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        // "order": [[ 0, 'desc' ]],
        "displayLength": 10,

    });
});
</script> -->




