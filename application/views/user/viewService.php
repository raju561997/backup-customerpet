<style type="text/css">
    .checkboxspan{
        display: unset !important;
        font-size: large  !important;
   }
   .time_estimate ,.cost{
        border: none !important;

   }
   td{
        padding:0px;
   }
   .form-control:disabled, .form-control[readonly] {
        background-color: #ffff;
        padding:0px;
    }
</style>

<style>
    #servicesList{
        border: none;
        overflow-x: hidden !important; /* Hide horizontal scrollbar */
    }
</style>
<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Service Details</h3>
                </div>
            </div>
            <div class="content-body" >
                <section class="row" style=" margin-bottom:3%;">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body" >
                                    <div class="form-group">

                                        <label for="eventInput1"><?= lang('service_name') ?></label>
                                        <div>
                                            <input id="name" name="name" class="form-control" type="text" style=" border:none;" value="<?php echo $service['name'] ?>" disabled="disabled">
                                        
                                        </div>
                                        <hr>
                                        <label for="eventInput1">Service Cost and Time Estimate</label>
                                    </div>

                                    <!-- Invoices List table -->
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th><span style="font-size:14px;" for="field-1" class="col-sm-3 control-label">
                                                        Pet Size
                                                    </span>
                                                    </th>
                                                    <th><span style="font-size:14px;" for="field-1" class="col-sm-3 control-label">
                                                        Cost
                                                    </span>
                                                    </th>
                                                    <th><span style="font-size:14px;" for="field-1" class="col-sm-3 control-label">
                                                    Time Estimate (minutes)
                                                    </span>
                                                    </th>
                                                </tr>    
                                            </thead>
                                            <tbody>    
                                            <?php if(!empty($petSize)):?>
                                            <?php foreach ($petSize as $key => $value) { ?>
                                                
                                                <tr>
                                                    <td class="form-group">
                                                    <label style="font-size:14px;"><?php echo ucfirst($value);?></label>
                                                    
                                                    </td>
                                                    <td class="form-group">
                                                    <input type="text" name="<?php echo $value?>_cost" value="<?php echo $this->localization->currencyFormat($service[$value.'_cost']);?>" id="<?php echo $value?>_cost"   class="form-control input-sm cost" disabled="disabled">
                                                    </td>
                                                    <td class="form-group">
                                                    <input type="text" name="<?php echo $value?>_time_estimate" value="<?php echo $service[$value.'_time_estimate']?>" id="<?php echo $value?>_time_estimate"  class="form-control input-sm time_estimate" disabled="disabled">
                                                    </td>
                                                
                                                </tr>

                                            <?php } ?>
                                            <?php else:?>
                                                <tr>
                                                <td colspan="3">
                                                    <h6>No Service Found</h6>
                                                </td>
                                                </tr> 
                                            <?php endif?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--/ Invoices table -->

                                    <div class="form-group">
                                            <label for="eventInput1"><?= lang('description') ?></label>
                                        <div>
                                            <textarea id="description" name="description" class="form-control" col="5" rows="5" disabled="disabled"><?php echo $service['description'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="eventInput1"><?= lang('serviceType') ?></label>
                                        <div>
                                            <input id="type" name="type" class="form-control"  type="text"  value="<?php echo $service['type'] ?>" type="text" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="eventInput1">Is Add On Service</label>
                                        <label>
                                        <!-- <input type="checkbox" /> -->
                                        <input type="checkbox" disabled="disabled"  value="false" <?php  if(!empty($service['is_add_on_service'])){ if($service['is_add_on_service'] == '1') {echo "checked='checked'"; }}?>>
                                        <span class="checkboxspan"></span>
                                        </label>
                                    </div>
                                    <?php if(!empty($service['image'])) {?>
                                        <div class="form-group">
                                            <label for="eventInput1"><?= lang('serviceImage') ?></label>
                                            <div>
                                                <img src="<?php echo loadCloudImage(SHOP_IMAGES.$service['image']); ?>" alt="">
                                            </div>
                                        </div>
                                    <?php } ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>                         






