
<style type="text/css">
   .error{
   color: #ff4081;
   font-size: .8rem;
   }
</style>

<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Change Password</h3>
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url() ;?>user/dashboard">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Change Password
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        
         <!-- END: Main Menu-->
        <div class="row match-height"  >
            <div class="col-md-12">
                <div class="card " style="min-height:500px;">
                    <div class="card-header">
                        <h3 class="">Hii, <?php echo $this->session->userdata('username');?>.</h3>
                        <h5 class="center">Change your password here!</h5>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <?php if ($this->session->flashdata("success")): ?>
                        <div class="alert alert-success">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
                        </div>
                    <?php elseif ($this->session->flashdata("error")): ?>
                        <div class="alert alert-danger">
                        <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
                        </div>
                    <?php endif;?>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="changePasswordForm" method="post" action="<?php echo base_url(); ?>user/changePassword">
                                <div class="row justify-content-md-center">
                                    <div class="col-md-6">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="eventInput1">Current Password</label>
                                                <input type="password" id="current_password" class="form-control" placeholder="Current Password" name="current_password">
                                            </div>

                                            <div class="form-group">
                                                <label for="eventInput2">New Password</label>
                                                <input type="password" id="new_password" class="form-control" placeholder="New Password" name="new_password">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="eventInput2">Confirm Password</label>
                                                <input type="password" id="confirm_password" class="form-control" placeholder="Confirm Password" name="confirm_password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" text-center">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <i class="la la-check-square-o"></i> Save Details
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </div>
    <!-- END: Content-->
