<style>
    #servicesList{
        border: none;
        overflow-x: hidden !important; /* Hide horizontal scrollbar */
    }
    .avatar-xs{
        width: 36px;
    }
</style>
<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Services</h3>
                </div>
            </div>
            <div class="content-body" >
                <section class="row" style=" margin-bottom:3%;">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body" >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="accordionWrap3" role="tablist" aria-multiselectable="true">
                                                <div class="card accordion collapse-icon accordion-icon-rotate">
                                                
                                                    <?php if(!empty($smallDog)){?>
                                                    <a id="heading31" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion31" aria-expanded="true" aria-controls="accordion31">

                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($smallDog as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    
                                                    </a>
                                                    <div id="accordion31" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading31" class="card-collapse collapse show" aria-expanded="true">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Dog'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['small_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['small_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                 
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceDog['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($mediumDog)){?>
                                                    <a id="heading32" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion32" aria-expanded="false" aria-controls="accordion32">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($mediumDog as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion32" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading32" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Dog'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['medium_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['medium_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceDog['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($largeDog)){?>
                                                    <a id="heading33" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion33" aria-expanded="false" aria-controls="accordion33">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($largeDog as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion33" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading33" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Dog'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['large_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['large_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceDog['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($xlDog)){?>
                                                    <a id="heading34" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion34" aria-expanded="false" aria-controls="accordion34">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($xlDog as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                        
                                                    </a>
                                                    <div id="accordion34" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading34" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Dog'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['xl_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['xl_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                        <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceDog['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($xxlDog)){?>
                                                    <a id="heading35" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion35" aria-expanded="false" aria-controls="accordion35">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($xxlDog as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion35" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading35" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Dog'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['xxl_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['xxl_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceDog['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($smallCat)){?>
                                                    <a id="heading36" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion36" aria-expanded="false" aria-controls="accordion36">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($smallCat as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion36" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading36" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Cat'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['small_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['small_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceCat['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($mediumCat)){?>
                                                    <a id="heading37" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion37" aria-expanded="false" aria-controls="accordion37">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($mediumCat as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion37" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading37" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Cat'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['medium_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['medium_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                               
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceCat['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($largeCat)){?>
                                                    <a id="heading38" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion38" aria-expanded="false" aria-controls="accordion38">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($largeCat as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion38" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading38" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Cat'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['large_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['large_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceCat['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($xlCat)){?>
                                                    <a id="heading40" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion40" aria-expanded="false" aria-controls="accordion40">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($xlCat as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion40" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading40" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Cat'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['xl_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['xl_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceCat['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>

                                                    <?php if(!empty($xxlCat)){?>
                                                    <a id="heading41" class="card-header bg-header mb-2" data-toggle="collapse" href="#accordion41" aria-expanded="false" aria-controls="accordion41">
                                                        <div class="card-title lead white">Services For  <?php $petArray = array();foreach ($xxlCat as $key => $value){ $petArray[] = ucfirst($value['name']);} ?><?php echo implode( ', ', $petArray );;?></div>
                                                    </a>
                                                    <div id="accordion41" role="tabpanel" data-parent="#accordionWrap3" aria-labelledby="heading41" class="card-collapse collapse" aria-expanded="false">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table id="servicesList" class="table table-hover table-xl mb-0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="border-top-0"></th>
                                                                                <th class="border-top-0">Service Name</th>
                                                                                
                                                                                <th class="border-top-0">Duration</th>
                                                                                <th class="border-top-0">Cost</th>
                                                                                <th class="border-top-0">Active Discount (%)</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($petService as $key => $value){?>
                                                                            <?php if($value['type']=='Cat'){?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate"><?php echo ucfirst($value['name']);?></td>
                                                                                
                                                                                <td class="text-truncate"><?php echo $value['xxl_time_estimate'];?> Minutes</td>
                                                                                <td class="text-truncate">$ <?php echo $value['xxl_cost'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <?php }?>
                                                                        <?php }?>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Daycare</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $daycarService['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-truncate">
                                                                                    <span class="avatar avatar-xs">
                                                                                    <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                                                    </span>
                                                                                </td>
                                                                                <td class="text-truncate">Boarding</td>
                                                                                
                                                                                <td class="text-truncate">1 Day</td>
                                                                                <td class="text-truncate">$ <?php echo $BoardingServiceCat['value'];?></td>
                                                                                <?php if(!empty($discount['discount_percentage'])){ ?>
                                                                                    <td class="text-truncate"><?php echo $discount['discount_percentage'];?></td>
                                                                                <?php }else{?>
                                                                                    <td class="text-truncate">N/A</td>
                                                                                <?php }?>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }?>
                                                
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>                         


	<!-- END: Content-->

<script>

</script>




