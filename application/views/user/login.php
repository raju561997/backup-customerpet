
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="Petcommander,Petcommander user">
    <meta name="author" content="PIXINVENT">
    <title>Petcommander</title>
    <!-- <link rel="apple-touch-icon" href="<?php echo base_url()?>assets/images/icon/apple-icon-120.png"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/ico/icon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/forms/icheck/custom.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/pages/login-register.css">
    <!-- END: Page CSS-->

<style>
    .alert-success{
        color:green;
        background-color: white !important;
    }
    .alert-danger{
        color:red;
        background-color: white !important;
    }
    .error{
        color:red;
    }

    .site-wrapper {
        background: rgba(28, 12, 125, 0.57) none repeat scroll 0 0;
        background-size: cover;
        min-height: 100% !important;
    }
    .site-wrapper::before {
        position: absolute;
        content: '';
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        min-height: 100% !important;
        background: rgba(11, 1, 35, 0.79) none repeat scroll 0 0;
        opacity: 0.9;
        z-index: 0;
    }
    img{
        width: 10%;
    }
    html body a {
        color: #605ca8 !important;
    }
    html body a:hover {
        color: #605ca8 !important;
    }
    .btn-outline-primar:hover {
        background-color: #605ca8 !important;
    }
    input:focus {
        border: 1px solid #605ca8 !important;
    }
    .btn-outline-primary {
        border-color: #605ca8;
        background-color: transparent;
        color: #605ca8;
    }
</style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column bg-full-screen-image blank-page bg-gradient-x-primary" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content site-wrapper">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <div>
                                            <span><img class="img-responsive" src="<?php echo base_url()?>assets/images/icons/icon.png" alt="branding logo"></span><span> Groom Shop</span>
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-1"><span>Login With Pet Commander</span></h6>
                                </div>
                                <?php if ($this->session->flashdata("success")): ?>
                                    <div class="alert alert-success">
                                    <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
                                    </div>
                                <?php elseif ($this->session->flashdata("error")): ?>
                                    <div class="alert alert-danger">
                                    <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
                                    </div>
                                <?php endif;?>
                                <div class="card-content" style="margin-top: -40px !important;">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple login-form" method="POST" action="<?php echo base_url(); ?>user/login" novalidate>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input class="form-control" id="shopno" placeholder="Shop Number" required name="shopno" type="hidden" value="9898989898">
                                                <!-- <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div> -->
                                                <input class="form-control isValidShop" name="isValidShop" type="hidden" value="">
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control" id="emailORphone" placeholder="Email Address or  Phone Number" required name="emailORphone" type="text">
                                                <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control password" id="password" name="password" placeholder="Password" required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>
                                            
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> Login</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="">
                                        <p class="float-xl-right text-center m-0"><a href="<?php echo base_url(); ?>user/forgotPassword" class="card-link">Forgot Password?</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url() ;?>assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url() ;?>assets/js/plugins.js" type="text/javascript"></script>
    
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>assets/js/login.js"></script> -->

    <script>
      var timeout = 2000; // in miliseconds (3*1000)
      $('.alert').delay(timeout).fadeOut(300);

	  jQuery(document).ready(function() {
      var BASE_URL = "<?php echo base_url();?>";
			$(".login-form").validate({
				onfocusout: function(e) {
					this.element(e);						  
				},
				onkeyup: false,
			    rules: {
             shopno : {
                    required : true,
                    number : true,
                    remote : {
                       url: BASE_URL + "user/getShopNameAjax",
                       type: "POST",
                       dataType: "json",
                       dataFilter: function(data) {
                          var json = JSON.parse(data);
                          if(json.err == 1000) {
                            $('.isValidShop').val(1);
                            return true;
                              //return "\"" + json.message + "\"";
                          } else {
                              return true;
                          }

                      },
                    },     
                },
					emailORphone: {
	                   
	                    required: true
	                },
	                password: {
	                    
	                    required: true
	                }
			    },
			    messages: {
			      username:{
			        required: "Enter a email address or  phone number.",
			      },
			    },
				errorElement : 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
					  $(placement).append(error)
					} else {
						error.insertAfter(element);
					}
			    }
			});
		});
	</script>
</body>
<!-- END: Body-->
<!-- BEGIN: Footer-->
   <!--  <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-center d-block d-md-inline-block">&copy; <?php echo date('Y');?><a href="javascript:void(0)"> Griffin Apps</a> All rights reserved.</a></p>
    </footer> -->

</html>