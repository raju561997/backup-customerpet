    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-center d-block d-md-inline-block">&copy; <?php echo date('Y');?></span><a href="javascript:void(0)"> Griffin Apps</a> All rights reserved.</p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?php echo base_url(); ?>assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/extensions/cropper.min.js"></script>
     <script src="<?php echo base_url(); ?>assets/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    
    
    <!-- END: Page Vendor JS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">


    <script src="<?php echo base_url(); ?>assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>

    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo base_url() ;?>assets/vendors/js/charts/chart.min.js"></script>
    <script src="<?php echo base_url() ;?>assets/vendors/js/charts/raphael-min.js"></script>
    <script src="<?php echo base_url() ;?>assets/vendors/js/charts/morris.min.js"></script>
    <script src="<?php echo base_url() ;?>assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ;?>assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js"></script>
    <script src="<?php echo base_url() ;?>assets/data/jvector/visitor-data.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo base_url() ;?>assets/js/core/app-menu.js"></script>
    <script src="<?php echo base_url() ;?>assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo base_url() ;?>assets/vendors/formatter/jquery.formatter.min.js"></script>
    <script src="<?php echo base_url() ;?>assets/vendors/dropify/js/dropify.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/cropper.min.js"></script>
   

    <!-- 
        This JS is downloded.
        NOTE: This JS conflict with other UI LIKE TOOLTIP etx which is used in This materialize theme 
    -->
    <script src="<?php echo base_url() ;?>assets/vendors/jquery-ui.js"></script> 
    <script src="<?php echo base_url(); ?>assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts/form-masks.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url() ;?>assets/js/scripts/form-file-uploads.js"></script>
    <!-- END: Page JS-->
    <script src="<?php echo base_url() ;?>assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="<?php echo base_url() ;?>assets/js/scripts/extensions/toastr.js"></script>
    <script src="<?php echo base_url() ;?>assets/js/scripts/popover/popover.min.js"></script>
    
    <?php
		if(is_file(FCPATH.'assets/js/custom/'.$this->router->fetch_class().'_'.$this->router->fetch_method().'.js')){
			echo '<script src="'.base_url().'assets/js/custom/'.$this->router->fetch_class().'_'.$this->router->fetch_method().'.js"></script>';
		}
    ?>
    <script>
      var timeout = 2000; // in miliseconds (3*1000)
      $('.alert').delay(timeout).fadeOut(300);
      <?php if ($this->session->flashdata("success")): ?>
          toastr.success('<?php echo $this->session->flashdata("success"); ?>');
      <?php elseif ($this->session->flashdata("error")): ?>
          toastr.error('<?php echo $this->session->flashdata("error"); ?>');
      <?php endif;?>
      <?php if($this->session->flashdata("name_error")){ ?>
          toastr.error('<?php echo $this->session->flashdata("name_error"); ?>');
    <?php  } ?>
      <?php if($this->session->flashdata("type_error")){ ?>
          toastr.error('<?php echo $this->session->flashdata("type_error"); ?>');
    <?php  } ?>
    </script>
</body>
<!-- END: Body-->

</html>