<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Pet Commander | Terms and Services</title>
    
     <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon-16x16.png">
     
     <script type="text/javascript">
     var BASE_URL = "<?php echo base_url();?>";
     </script>
     <style type="text/css">
     .input-group .error {
      color:red;
     }

     .alert-danger {
        color: #a94442 !important;
        background-color: #f2dede !important;
        border-color: #ebccd1 !important;
      }

      ::-webkit-input-placeholder {
         text-transform: initial;
      }

      :-moz-placeholder { 
         text-transform: initial;
      }

      ::-moz-placeholder {  
         text-transform: initial;
      }

      :-ms-input-placeholder { 
         text-transform: initial;
      }
      .site-wrapper::before {
       
      }
      .infinyLink:hover,.infinyLink:focus,.infinyLink:visited{
        text-decoration: none;
        color:white;
      }
      @media screen and (max-height: 1024px) and (min-width: 768px){
        .cover-container {
            padding: 50px 15px 30px;
        }
      }  
     </style>

</head>

<body style="background:url(<?php echo base_url();?>assets/one.jpg);background-size: cover;
    background-position: center center;" class="skin-purple">

<!-- start site-wrapper -->
<div class="site-wrapper">

    <!-- start site-wrapper-inner -->
    <div class="clearfix">

        <!-- start cover-container -->
        <div class="cover-container container">


         <div class=" clearfix" style="">
                <div class="col-xs-12 col-sm-12 " style="">
                  
                
                <div class="row">
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="col-sm-12" data-offset="0">
                        <div class="wrap-fpanel" style="border:0px;">
                          <div class="box box-primary" data-collapsed="0" style="border:0px;">
                            <div class="box-header with-border bg-primary-dark">
                              <h3 class="box-title"><?php echo PROJECTNAME; ?>&nbsp;Terms and Services</h3>
                            </div>
                            <div class="panel-body">
                              <div class="form-horizontal">  
                                <div class="panel_controls" style="padding:20px;">
                                 <?php echo $termsConditions; ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>                



                    


                </div>
            </div>
        </div>
    </div>
     <div class="text-center" style="position: fixed;left: 0px;bottom: 10px;text-align: center;width:100%;color: white;z-index:99">
     
         <strong>&copy; <?php echo date('Y'); ?> <a href="javascript:void(0)" style="color: white;" class="infinyLink"><?php echo COMPANYNAME; ?></a></strong> All rights reserved.      
    </div>
</div>    
</body>
</html>    