    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                
                <!-- Emails Products & Avg Deals -->
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Emails</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="card-content collapse show">
                                <div class="card-body pt-0">
                                    <p>Open rate <span class="float-right text-bold-600">89%</span></p>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-1">
                                        <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <p class="pt-1">Sent <span class="float-right"><span class="text-bold-600">310</span>/500</span>
                                    </p>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-1">
                                        <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 48%" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Emails Products & Avg Deals -->

                    <!-- <div class="row">       
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content" >
                                <div class="card-body">
                                    <div class="media d-flex" >
                                        <div class="media-body text-left">
                                            <h3 class="info"><i class="la la-inr"></i>5427.37</h3>
                                            <h6>Current Gold Price Per/Gram</h6>
                                        </div>
                                        <div>
                                            <i class="icon-pie-chart info font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2" >
                                        <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="info"><i class="la la-inr"></i>5257.4</h3>
                                            <h6>Current Gold selling Price Per/Gram</h6>
                                        </div>
                                        <div>
                                            <i class="icon-pie-chart info font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> -->

                <!--card stats start-->
                <!-- <div id="card-stats">
                <div class="row">
                    <div class="col s12 m6 l6 xl4">
                        <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4">
                            <div class="col s8 m8">
                                
                                <p>Ready for trial orders</p>
                            </div>
                            <div class="col s4 m4 right-align">
                                <h5 class="mb-0 white-text"></h5>
                                
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6 xl4">
                        <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                            <div class="padding-4">
                            <div class="col s8 m8">
                            
                                <p>Ready for delivery orders</p>
                            </div>
                            <div class="col s4 m4 right-align">
                                <h5 class="mb-0 white-text"></h5>
                                
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6 xl4">
                        <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text animate fadeRight">
                            <div class="padding-4">
                            <div class="col s8 m8">
                            
                                <p>Off delivery orders</p>
                            </div>
                            <div class="col s4 m4 right-align">
                                <h5 class="mb-0 white-text"></h5>
                                
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div> -->
                <!--card stats end-->


                <!-- <div class="col s12 m12 l12" style="padding:0px; margine-top:25px;">
                <div class="card subscriber-list-card animate fadeRight">
                    <div class="card-content pb-1">
                        <h4 class="card-title mb-0">Orders
                                            </h4>
                    
                </div>

                    <div class="section section-data-tables">
                <table class="table" id="ordertabledashboard" style="width:100%;">

                    <thead>
                    <tr>
                    <th> Name</th>
                    <th style="width:40%">Product</th>
                    <th>Delivery Date</th>
                    <th>Total Cost</th>
                    <th>Status</th>                    
                    </tr>
                    </thead>
                    <tbody>
                                        </tbody>
                </table>
                </div>
                </div>
                </div> -->
                <!--/ eCommerce statistic -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


<script type="text/javascript">
 $(document).ready(function(){
 $('#ordertabledashboard').DataTable({
          "lengthChange": false,
          "responsive":true,
          "bInfo": false,
          //   "bAutoWidth": false,
          //   "bSort" : false,
          "searching": false,
            "paging": true,
         
            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
            ]
  });
});
</script>