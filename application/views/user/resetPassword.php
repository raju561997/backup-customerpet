<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reset Password</title>
	<style>
		.footer {
    margin-left: 0px !important;
}
	</style>
</head>
<body>
	<div class="row match-height" style=" width:97%; margin-top:2.5%;margin-left:1.5%; ">
		<div class="col-md-12">
            <div class="card " style="min-height:460px;">
            
				<div class="card-header">
					<h3 class="">Welcome back, <?php echo ucfirst($userdata['username']); ?>.</h3>
                    <h5 class="center">Reset your password here!</h5>
				</div>
				<div class="card-content collapse show">
					<div class="card-body">
						<form class="resetPassForm" method="POST" action="<?php echo base_url('User/resetPassword/'.$token); ?>">
						<input type="hidden" name="token" value="<?php echo $token; ?>">
						<input type="hidden" name="email" value="<?php echo $userdata['email']; ?>">	
						<div class="row justify-content-md-center">
								<div class="col-md-6">
									<div class="form-body">
										<div class="form-group">
                                            <input type="text" class="form-control passRecUsername" name="shopno" value="9898989898" placeholder="Shop Number" required hidden> 
                                            
											<label for="eventInput1">New Password</label>
											<input type="password" id="new_password" class="form-control" placeholder="New Password" name="new_password">
										</div>

										<div class="form-group">
											<label for="eventInput2">Confirm Password</label>
											<input type="password" id="confirm_password" class="form-control" placeholder="Confirm Password" name="confirm_password">
										</div>
									</div>
								</div>
							</div>
							<div class=" text-center">
								<button type="submit" class="btn btn-outline-primary">
									<i class="la la-check-square-o"></i> Submit
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span>&copy; <?php echo date('Y');?><a href="javascript:void(0)"> Griffin Apps</a> All rights reserved.</span>
        </p> 
        
    </footer>


    <!-- BEGIN: Vendor JS-->
    <script src="<?php echo base_url(); ?>app-asset/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo base_url(); ?>app-asset/vendors/js/charts/chartist.min.js"></script>
    <script src="<?php echo base_url(); ?>app-asset/vendors/js/charts/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>app-asset/vendors/js/charts/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>app-asset/vendors/js/timeline/horizontal-timeline.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo base_url(); ?>app-asset/js/core/app-menu.js"></script>
    <script src="<?php echo base_url(); ?>app-asset/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<!-- END: Page JS-->
	<script>
		jQuery(document).ready(function() {
			$(".resetPassForm").validate({
				onfocusout: function(e) {
					this.element(e);
				},
				onkeyup: false,
			    rules: {
                new_password: {
	                    minlength: 5,
	                    required: true
	                },
                confirm_password: {
	                    minlength: 5,
	                    required: true,
                      equalTo : "#new_password"
	                   }
                    },
			    messages: {
                new_password:{
			        required: "Enter new password",
			      },
               confirm_password:{
			        required: "Enter the password again",
			      }
			    },
				errorElement : 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
					  $(placement).append(error)
					} else {
						error.insertAfter(element);
					}
			    }
			});
		});
    </script>
    
    

</body>
<!-- END: Body-->

</html>