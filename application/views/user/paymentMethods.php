    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/paymentsMethods.css">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Payment Methods</h3>
                </div>
                <div class="content-header-right col-md-6 col-12">
                    <div class="d-inline-block float-md-right">
                        <a class="btn btn-sm text-white marginBTN" style="background-color: #605ca8; padding-top:4px;" data-toggle="modal" data-target="#addCardModel" ><i class="la la-plus"></i> Add Card
                        </a>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <!-- Minimal statistics section start -->
                <section id="minimal-statistics">
                    <div class="row">
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content borderActive">
                                    <div class="card-body" style="padding: 1.5rem 1rem !important;">
                                        <div class="media d-flex">
                                            <div class="media-body text-left col-md-10 col-10">
                                                <h5 class="danger">XXXX XXXX XXXX 2780</h5>
                                                <span>Visa</span>
                                            </div>
                                            <div class="media-body text-right col-md-2 radioAlign col-2">
                                                <h5><a type="button" class="btn btn-icon btn-pure dark delete_cardBtn" style="padding: 0rem 0rem !important;" data-toggle="modal" data-target="#deleteModel" data-id="<?php echo $value['id'];?>" title="Remove Card"><i class="la la-trash danger" ></i></a></h5>
                                                <span><input type="radio" name="radio" value="" data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Make Default" checked></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content borderInactive">
                                    <div class="card-body" style="padding: 1.5rem 1rem !important;">
                                        <div class="media d-flex">
                                            <div class="media-body text-left col-md-10 col-10">
                                                <h5>XXXX XXXX XXXX 5555</h5>
                                                <span>MasterCard</span>
                                            </div>
                                            <div class="media-body text-right col-md-2 radioAlign col-2">
                                                <h5><a type="button" class="btn btn-icon btn-pure dark delete_cardBtn" style="padding: 0rem 0rem !important;" data-toggle="modal" data-target="#deleteModel" data-id="<?php echo $value['id'];?>"><i class="la la-trash" ></i></a></h5>
                                                <span><input type="radio" name="radio" value="" data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Make Default"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content borderInactive">
                                    <div class="card-body" style="padding: 1.5rem 1rem !important;">
                                        <div class="media d-flex">
                                            <div class="media-body text-left col-md-10 col-10">
                                                <h5>XXXX XXXX XXXX 0132</h5>
                                                <span>American Express</span>
                                            </div>
                                            <div class="media-body text-right col-md-2 radioAlign col-2">
                                                <h5><a type="button" class="btn btn-icon btn-pure dark delete_cardBtn" style="padding: 0rem 0rem !important;" data-toggle="modal" data-target="#deleteModel" data-id="<?php echo $value['id'];?>"><i class="la la-trash" ></i></a></h5>
                                                <span><input type="radio" name="radio" value="" data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Make Default"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content borderInactive">
                                    <div class="card-body" style="padding: 1.5rem 1rem !important;">
                                        <div class="media d-flex">
                                            <div class="media-body text-left col-md-10 col-10">
                                                <h5>XXXX XXXX XXXX 1111</h5>
                                                <span>Discover</span>
                                            </div>
                                            <div class="media-body text-right col-md-2 radioAlign col-2">
                                                <h5><a type="button" class="btn btn-icon btn-pure dark delete_cardBtn" style="padding: 0rem 0rem !important;" data-toggle="modal" data-target="#deleteModel" data-id="<?php echo $value['id'];?>"><i class="la la-trash" ></i></a></h5>
                                                <span><input type="radio" name="radio" value="" data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Make Default"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Minimal statistics section end -->
            </div>

            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Transactions</h3>
                </div>
            </div>

            <div class="content-body">
                <section class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body" >
                                    <div class="row">
                                        <div class="col-md-2 form-group" id="selectTriggerFilter">
                                            
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="paymentsList" class="table table-hover table-xl mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="border-top-0"></th>
                                                    <th class="border-top-0">Service</th>
                                                    <th class="border-top-0">Amount</th>
                                                    <th class="border-top-0">Date</th>
                                                    <th class="border-top-0">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Payment Made For Daycare</td>
                                                    <td class="text-truncate">$ 20.00</td>
                                                    <td class="text-truncate">October, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-danger badge-square" style="padding-top:7px;"><span >Failed</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/payment.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Payment Made For Daycare</td>
                                                    <td class="text-truncate">$ 20.00</td>
                                                    <td class="text-truncate">September, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-success badge-square " style="padding-top:7px;"><span >Success</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/deposit.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Deposit Made For Grooming</td>
                                                    <td class="text-truncate">$ 15.00</td>
                                                    <td class="text-truncate">August, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-warning badge-square " style="padding-top:7px;"><span >Pending</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Refunded For Cancelled Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">July, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Refunded For Cancelled Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">June, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-truncate">
                                                        <span class="avatar avatar-xs">
                                                        <img  src="<?php echo base_url(); ?>assets/images/transactions/credit-awarded.png" alt="avatar">
                                                        </span>
                                                    </td>
                                                    <td class="text-truncate">Refunded For Cancelled Boarding</td>
                                                    <td class="text-truncate">$ 25.00</td>
                                                    <td class="text-truncate">May, 2020</td>
                                                    <td class="text-truncate"><div class="badge badge-info badge-square" style="padding-top:7px;"><span >Refunded</span></div></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->




<!-- Modal -->
<div class="modal fade text-left" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="para"></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <a type="button" id="btn_delete_pet" class="btn editButton text-white" style="background-color: #605ca8;">Remove</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="addCardModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add New Card</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addCardDetails" method="post" action="">
                <div class="modal-body">
                    <fieldset class="form-group floating-label-form-group">
                        <label for="number">Card Number</label>
                        <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Card Number" maxlength="16">
                    </fieldset>

                    <fieldset class="form-group floating-label-form-group">
                        <label for="medication_name">Card Holder Name</label>
                        <input type="text" class="form-control" id="holder_name" name="holder_name" placeholder="Card Holder Name">
                    </fieldset>
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="expiry">Card Expiry Date</label>
                            <input id="expiry" name="expiry" class="form-control" type="text" placeholder="Card Expiry Date">
                        </div>
                        <div class="form-group  col-md-6">
                            <label for="date_of">Card CVC Number</label>
                            <input id="cvc" name="cvc" class="form-control" type="text" placeholder="Card CVC Number" maxlength="3">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-danger" data-dismiss="modal" value="Close">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
    
        $(document).on('click', '.delete_cardBtn', function ()
        {   
            var id=$(this).data('id');
            $(".modal-title").html("Remove Card");
            $(".para").html("Are you sure you want to remove this card?");
            $("#btn_delete_pet").attr("href",BASE_URL+"User/removeCard?id="+id);
        });
    });

</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script> -->
<script>
$(document).ready(function() {
    var columnNo = 0;
    $('#paymentsList').DataTable( {
        "bSort" : false,
        "searching" : true,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(columnNo == 3){
                    var select = $('<select class="form-control"><option value="<?php echo date("F, Y") ;?>"><?php echo date("F, Y") ;?></option></select>')
                    .appendTo( $('#selectTriggerFilter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                columnNo++;
            } );
        }
    } );
} );
</script>