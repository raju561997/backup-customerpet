<style>
  .tabsbtn{
      text-align: left !important;
      width:100%;
      color: black;
      background-color: #ffffff;
      text-transform: capitalize;
   }
  .tabsbtn:hover {
    background-color: #605ca8;
    color: white !important;
  }
</style>
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0 d-inline-block">Terms And Services</h3>
          </div>
        </div>
        <div class="card mb-2">
          <a href="https://pet-commander-beta-app.appspot.com/Admin/showMaintermServicePreview" class="btn tabsbtn">
          Pet Commander Terms and Services
          </a>
        </div>
        <div class="card text-left">
          <a href="https://pet-commander-beta-app.appspot.com/123456789/Admin/showtermServicPreview" class="btn tabsbtn"><?= get_option('company_name') ?> Terms and Services
          </a>
        </div>
      </div>
    </div>
    <!-- END: Content-->
