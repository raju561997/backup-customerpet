<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/appointments.css">

    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Active Appointments</h3>
                </div>
            </div>

            <div class="content-body" >
                <section class="row">
                    <?php foreach ($appointments as $key => $value) { $i++?>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header pr-0">
                                    <div class="row mt-1 col-md-12">
                                        <div class="col-md-2">
                                            <div class="petpic">
                                                <div class="pet-circle">
                                                    <div class="pet-wrapper">
                                                    <img class="img-profile" src="<?php echo base_url(); ?>assets/p.jpeg" />
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>

                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <span class="breedtext colorC" style="font-size:15pt;"><?php echo ucfirst($value['petDetails']['name']);?></span>
                                            <p class="datapet" style="font-size:9pt;margin-top:5px;"><?php if($value['category']=='appointment'){echo 'Grooming';}else{echo ucfirst($value['category']);}?>: Nail Trim,Flea Bath, Hair Cut</p>
                                            <p class="datapet colorC" style="font-size:7pt;margin-top:-5px;">CREATED ON: <?php echo date("d/m/Y", strtotime($value['appointment_date']));?></p>
                                            <p><a class="colorC anchor" href="<?php echo base_url(); ?>user/invoice?id=<?php echo $value['invoice_no'];?>" target="_blank"><u>VIEW INVOICE</u></a></p>
                                        </div>
                                        
                                        <div class="col-md-2" style="text-align: right !important;">
                                            <i class="la la-map-marker kennel"><span class="kennel_position">KENNEL 29</span></i>
                                        </div>
                                    </div>
                                    <div class="tm col-md-12">
                                    <?php if($value['category']=='appointment'){ ?>
                                        <ul class="timeline1">
                                            <?php if($value['status']=='Pending'){ ?>
                                                <li class="confirm-tl">CONFIRMED</li>
                                                <li class="checkIn-tl">CHECKED IN</li>
                                                <li class="complete-tl">COMPLETED</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Confirm'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkIn-tl">CHECKED IN</li>
                                                <li class="complete-tl">COMPLETED</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Inprocess'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkedIn-tl">CHECKED IN</li>
                                                <li class="complete-tl">COMPLETED</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Complete'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkedIn-tl">CHECKED IN</li>
                                                <li class="completed-tl">COMPLETED</li>
                                                <!-- if class="active-tl" than checked out-->
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Checkout'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkedIn-tl">CHECKED IN</li>
                                                <li class="completed-tl">COMPLETED</li>
                                                <li class="active-tl">CHECKED OUT</li>
                                            <?php }?>
                                        </ul>
                                    <?php }else{?>
                                        <ul class="timeline">
                                            <?php if($value['status']=='Pending'){ ?>
                                                <li class="confirm-tl">CONFIRMED</li>
                                                <li class="checkIn-tl">CHECKED IN</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Confirm'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkIn-tl">CHECKED IN</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Inprocess'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkedIn-tl">CHECKED IN</li>
                                                <li>CHECKED OUT</li>
                                            <?php }elseif($value['status']=='Checkout'){?>
                                                <li class="confirmed-tl">CONFIRMED</li>
                                                <li class="checkedIn-tl">CHECKED IN</li>
                                                <li class="active-tl">CHECKED OUT</li>
                                            <?php }?>
                                        </ul>
                                    <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
    </div>                         










