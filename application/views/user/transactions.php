<style>
    #transactionList{
        border: none;
        overflow-x: hidden !important; /* Hide horizontal scrollbar */
    }
</style>
<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Transactions</h3>
                </div>
            </div>
            <div class="content-body" >
                <section class="row" style=" margin-bottom:3%;">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body" >

									<?php if ($this->session->flashdata("success")): ?>
										<div class="alert alert-success">
										<i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
										</div>
									<?php elseif ($this->session->flashdata("error")): ?>
										<div class="alert alert-danger">
										<i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
										</div>
                                    <?php endif;?>
                                    
                                    <!-- Modal -->
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="modal animated slideInDown text-left" id="slideInDown" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel77"></h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p id="p"></p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">NO</button>
                                                        <a type="button" id="btn_block_active" class="btn btn-outline-primary">YES</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Modal -->

                                    <!-- Invoices List table -->
                            <div class="table-responsive">

                                <table id="transactionList" class="table table-striped table-bordered responsive dataex-res-controlled" style="width:100%">
                                    <thead>
                                       <tr>
                                         <th class="active">Serial No</th>
                                         <th class="active">Date</th>
                                         <th class="active">Customer Name</th>
                                         <th class="active">Service</th>
                                         <th class="active">Amount</th>
                                         <th class="active">Payment Mode</th>
                                         <th class="active">Payment Status</th>
                                         <th class="active">Payment Type</th>
                                         <th class="active noExport">Actions</th>   
                                       </tr>
                                    </thead>
                                <tbody class='sortable'>
                                <?php if(!empty($transactions)): $i=1; foreach($transactions as $item):?>
                                <tr class="custom-tr">
                                    <td class="vertical-td"><?php echo  $i;?></td>
                                    <td class="vertical-td"><?php echo date(get_option('date_format').' '.'h:i a',strtotime($item['added_on']));?></td>
                                    <td class="vertical-td"><?php echo  ucfirst($item['app_user_name']); ?></td>
                                    <td class="vertical-td">
                                        
                                       <?php echo  ucfirst($item['services']); ?>

                                    </td>
                                    
                                    <td class="vertical-td"><?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat(round(trim($item['amount'], "-"),2)); ?></td>
                                    <td class="vertical-td"><?php echo  ucfirst(paymentStatusDisplay($item['payment_mode']));?></td>
                                    <td class="vertical-td"><?php echo  ucfirst($item['status']);?></td>
                                    <td class="vertical-td"><?php echo  ucfirst($item['type']);?></td>
                                 
                                    <td class="vertical-td">
                                        <div class="btn-group">

                                            <?php 
                                                $ref_id = $item['invoice_id'];
                                                $page_name = 'invoice';
                                                if(!empty($ref_id )){
                                            ?>
                                             <a  href="<?php echo site_url('User/invoice/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($ref_id))) ?>"  type="button" class="btn btn-outline-info btn-sm modal-trigger viewinvoicebtn" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-title="View"><i class="la la-eye"></i>
                                             </a>
                                         <?php } ?>

                                        </div>
                                    </td>
                               
                                </tr>
                            <?php $i++;endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            </tbody>
                            </table>
                                    </div>
                                    <!--/ Invoices table -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



	<!-- END: Content-->

<script>
    $(document).ready(function() {
    $('#transactionList').dataTable({
        "responsive": true,
        "searching" : true,
        "columnDefs": [
            {
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
        ],
        "order": [[ 0, 'desc' ]],
        "displayLength": 25,

    });
});
</script>
</body>
<!-- END: Body-->

</html>



