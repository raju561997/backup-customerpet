<?php

    if(isset($header) && $header){
        $this->load->view('user/header');
    }

    if(isset($sidebar) && $sidebar){
        $this->load->view('user/sidebar');
    }

    if(isset($_view)){
        $this->load->view('user/'.$_view);
    }

    if(isset($footer) && $footer){
        $this->load->view('user/footer');
    }
    
 ?>