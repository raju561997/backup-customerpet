    <!-- BEGIN: Main Menu-->

    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item <?php echo (isset($active_tab) && trim($active_tab) == 'dashboard') ? 'active': ''; ?>"><a class="dropdown-toggle nav-link" href="<?php echo base_url() ;?>user/dashboard"><i class="la la-home"></i><span data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class="nav-item <?php echo (isset($active_tab) && trim($active_tab) == 'getAllPets') ? 'active': ''; ?>" ><a class="dropdown-toggle nav-link" href="<?php echo base_url() ;?>user/getAllPets"><i class="la la-paw"></i><span data-i18n="Pets">My Pets</span></a>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-paste"></i><span data-i18n="Apps">My Appointments</span></a>
                    <ul class="dropdown-menu">
                        <li class="<?php echo (isset($active_tab) && trim($active_tab) == 'getActiveAppointments') ? 'active': ''; ?>" data-menu=""><a class="dropdown-item" href="<?php echo base_url();?>user/getActiveAppointments" data-toggle=""><i class="la la-check-square"></i><span data-i18n="ToDo">Active Appointments</span></a>
                        </li>
                        <li class="<?php echo (isset($active_tab) && trim($active_tab) == 'getCheckoutAppointments') ? 'active': ''; ?>" data-menu=""><a class="dropdown-item" href="<?php echo base_url();?>user/getCheckoutAppointments" data-toggle=""><i class="la la-users"></i><span data-i18n="Contacts">Appointment History</span></a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-dollar"></i><span data-i18n="Payments">My Payments</span></a>
                    <ul class="dropdown-menu">
                        <li class="<?php echo (isset($active_tab) && trim($active_tab) == 'paymentMethods') ? 'active': ''; ?>" data-menu=""><a class="dropdown-item" href="<?php echo base_url();?>user/paymentMethods" data-toggle=""><i class="icon-credit-card"></i><span data-i18n="Contacts">Online</span></a>
                        </li>
                        <li class="<?php echo (isset($active_tab) && trim($active_tab) == 'wallet') ? 'active': ''; ?>" data-menu=""><a class="dropdown-item" href="<?php echo base_url();?>user/wallet" data-toggle=""><i class="icon-wallet"></i><span data-i18n="Wallet">Wallet</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?php echo (isset($active_tab) && trim($active_tab) == 'contract') ? 'active': ''; ?>" ><a class="dropdown-toggle nav-link" href="<?php echo base_url() ;?>user/contract"><i class="la la-file"></i><span data-i18n="Pets">My Contracts</span></a>
                </li>
                <li class="nav-item <?php echo (isset($active_tab) && trim($active_tab) == 'getAllservices') ? 'active': ''; ?>" ><a class="dropdown-toggle nav-link" href="<?php echo base_url() ;?>user/getAllservices"><i class="la la-money"></i><span data-i18n="Pets">My Rate Card</span></a>
                </li>
                <li class="nav-item <?php echo (isset($active_tab) && trim($active_tab) == 'getAlltermservices') ? 'active': ''; ?>" ><a class="dropdown-toggle nav-link" href="<?php echo base_url() ;?>user/getAlltermservices"><i class="la la-file-text"></i><span data-i18n="Pets">Terms and Services</span></a>
                </li>

            </ul>
        </div>
    </div>

    <!-- END: Main Menu-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>