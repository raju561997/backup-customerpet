
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="Petcommander,Petcommander user">
    <meta name="author" content="PIXINVENT">
    <title>Petcommander</title>
    <!-- <link rel="apple-touch-icon" href="<?php echo base_url()?>assets/images/icon/apple-icon-120.png"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/ico/icon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendors/css/forms/icheck/custom.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/core/menu/menu-types/vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/pages/login-register.css">
    <!-- END: Page CSS-->

    <style>
        .alert-success{
            color:green;
            background-color: white !important;
        }
        .alert-danger{
            color:red;
            background-color: white !important;
        }
        .error{
            color:red;
        }

        .site-wrapper {
            background: rgba(28, 12, 125, 0.57) none repeat scroll 0 0;
            background-size: cover;
            min-height: 100% !important;
        }
        .site-wrapper::before {
            position: absolute;
            content: '';
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            min-height: 100% !important;
            background: rgba(11, 1, 35, 0.79) none repeat scroll 0 0;
            opacity: 0.9;
            z-index: 0;
        }
        img{
            width: 10%;
        }
        html body a {
            color: #605ca8 !important;
        }
        html body a:hover {
            color: #605ca8 !important;
        }
        .btn-outline-primar:hover {
            background-color: #605ca8 !important;
        }
        input:focus {
            border: 1px solid #605ca8 !important;
        }
        .btn-outline-primary {
            border-color: #605ca8;
            background-color: transparent;
            color: #605ca8;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content site-wrapper">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0 pb-0">
                                    <div class="card-title text-center">
                                        <span><img class="img-responsive" src="<?php echo base_url()?>assets/images/icons/icon.png" alt="branding logo"></span><span> Groom Shop</span>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-1"><span>We will send you a link to reset password.</span>
                                    </h6>
                                </div>
                                <?php if ($this->session->flashdata("success")): ?>
                                    <div class="alert alert-success">
                                    <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("success"); ?>
                                    </div>
                                <?php elseif ($this->session->flashdata("error")): ?>
                                    <div class="alert alert-danger">
                                    <i class="fa fa-remove-sign"></i><?php echo $this->session->flashdata("error"); ?>
                                    </div>
                                <?php endif;?>
                                <div class="card-content" style="margin-top: -20px !important;">
                                    <div class="card-body">
                                        <form class="form-horizontal forgotPassForm" method="POST" action="<?php echo base_url(); ?>user/forgotPassword" novalidate>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control passRecUsername" name="shopno" value="9898989898" placeholder="Shop Number" required hidden>
                                                <!-- <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div> -->
                                            </fieldset>
                                            <div class="row" style="display:none;">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-4 control-label"><?= lang('shop_name') ?></label>

                                                    <div class="col-sm-5">
                                                        <input class="form-control" id="shopname" placeholder="<?= lang('shop_name') ?>" type="text" name="shopname" readonly>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="email" class="form-control passRecEmail" name="email" placeholder="Email" required>
                                                <div class="form-control-position">
                                                    <i class="la la-envelope"></i>
                                                </div>
                                            </fieldset>
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> Recover
                                                Password</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer border-0">
                                    <p class="float-sm-left text-center"><a href="<?php echo base_url(); ?>" class="card-link">Login</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url() ;?>assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url() ;?>assets/js/plugins.js" type="text/javascript"></script>
    
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>assets/js/login.js"></script> -->

    <script>
    var BASE_URL = "<?php echo base_url();?>";
	jQuery(document).ready(function() {

    $(".forgotPassForm").validate({
            onfocusout: function(e) {
                    this.element(e);                          
            },
            excluded: ':disabled',
            rules: { 
                shopno : {
                    required : true,
                    number : true,
                    remote : {
                       url: BASE_URL + "user/getShopNameAjax",
                       type: "POST",
                       dataType: "json",
                       dataFilter: function(data) {
                          var json = JSON.parse(data);
                          if(json.err == 1000) {
                              return "\"" + json.message + "\"";
                          } else {
                             return true;
                          }

                      },
                    }, 
                },
                shopname : {
                  required : true,
                },
                email : {
                    required: true,
                    email: true  
                },
                
            },

            messages : {
                shopno : {
                    required : "Shop phone number is required ",
                    number : "Shop phone number should be a number",
                       
                },
                email : {
                  required : "Email is required ",
                  email : "Only valid email is allowed"
                },
                shopname : {
                  required : "Shop name is required "
                },
                password : {
                  required : "Password is required "
                }
                
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
      });

		});
	</script>
</body>
<!-- END: Body-->

</html>