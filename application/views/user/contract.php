

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0 d-inline-block">My Contracts</h3>
            </div>
        </div>
        <div class="content-body">
          <div class="row" style=" margin-bottom:3%;">
            <div class="col-12">
              <div class="card">
                <div class="card-content">
                  <div class="card-body" >
                    <div class="table-responsive">
                      <table id="contractList" class="table table-hover table-xl mb-0">
                          <thead>
                              <tr>
                                  <th class="border-top-0 text-center">Pet Name</th>
                                  <th class="border-top-0 text-center">Daycare</th>
                                  <th class="border-top-0 text-center">Boarding</th>
                                  <th class="border-top-0 text-center">Grooming</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($pet as $key => $value){ ;?>
                                <tr>
                                    <td class="text-center"><?php echo ucfirst($value['name']);?></td>
                                    <td class="text-center">
                                        <!-- <div class="row"> -->
                                            <?php if(strtotime(date('y-m-d')) < strtotime($value['daycare_expiry'])){?>
                                                <a href="<?php echo base_url('assets/pdf_contract/'.$value['daycare_contract']);?>" target="_blank"><i class="la la-check" style="color:green;"></i></a>
                                                
                                            <?php } else { ?>
                                                <a href="<?php echo base_url() ?>User/contractUpload/<?php echo $value['id'];?>/Daycare" target="_blank"><i class="la la-close" style="color:red;"></i></a>
                                                
                                            <?php } ?>
                                        <!-- </div> -->
                                    </td>
                                    <td class="text-center">
                                        <!-- <div class="row"> -->
                                            <?php if(strtotime(date('y-m-d')) < strtotime($value['boarding_expiry'])){?>
                                                <a href="<?php echo base_url('assets/pdf_contract/'.$value['boarding_contract']);?>" target="_blank"><i class="la la-check" style="color:green;"></i></a>
                                                
                                            <?php } else { ?>
                                                <a href="<?php echo base_url() ?>User/contractUpload/<?php echo $value['id'];?>/Boarding" target="_blank"><i class="la la-close" style="color:red;"></i></a>
                                                
                                            <?php } ?>
                                        <!-- </div> -->
                                    </td>
                                    <td class="text-center">
                                        <!-- <div class="row"> -->
                                            <?php if(strtotime(date('y-m-d')) < strtotime($value['appointment_expiry'])){?>
                                                <a href="<?php echo base_url('assets/pdf_contract/'.$value['appointment_contract']);?>" target="_blank"><i class="la la-check" style="color:green;"></i></a>
                                                
                                            <?php } else { ?>
                                                <a href="<?php echo base_url() ?>User/contractUpload/<?php echo $value['id'];?>/Grooming" target="_blank"><i class="la la-close" style="color:red;"></i></a>
                                                
                                            <?php } ?>
                                        <!-- </div> -->
                                    </td>
                                </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->
