<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link href="<?php echo base_url()?>assets/plugins/emoji2/lib/css/emoji.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/plugins/emoji2/lib/js/config.js"></script>
<script src="<?php echo base_url()?>assets/plugins/emoji2/lib/js/util.js"></script>
<script src="<?php echo base_url()?>assets/plugins/emoji2/lib/js/jquery.emojiarea.js"></script>
<script src="<?php echo base_url()?>assets/plugins/emoji2/lib/js/emoji-picker.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
   .error{
   color: #ff4081;
   font-size: .8rem;
   }
.input-container input {
    border: none;
    box-sizing: border-box;
    outline: 0;
    padding: .75rem;
    position: relative;
    width: 100%;
}
.custom-control-label:before{
  background-color:white;
  border: #605ca8 solid 1px !important;
}
.custom-control-input:checked~.custom-control-label::before{
  background-color:#605ca8;
  border: #605ca8 solid 1px;
}
.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
    color: #fff;
    background-color: #605ca8;
}
.nav-pills .nav-link, .nav-pills .show > .nav-link {
    color: #2B335E;
}
#button {
    background: #605ca8;
    color: #fff;
    display: block;
    padding: 7px 12px;
    border-radius: 2px;
    border: 1px solid transparent;
}
/* .custom-control-label::before {
    background-color: #605ca8 !important;
    border: #605ca8 solid 1px !important;
} */
input[type="date"]::-webkit-calendar-picker-indicator {
    background: transparent;
    bottom: 0;
    color: transparent;
    cursor: pointer;
    height: auto;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: auto;
}
   input[type="checkbox"]{
    width: 15px; 
    height: 15px; 
    }
    img{
       width: 200px;
       height: 200px;
   }
   .checkboxspan{
   display: unset !important;
   font-size: large  !important;
   }
   input[type=date]::-webkit-inner-spin-button {
   -webkit-appearance: none;
   display: none;
   }
</style>

<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0 d-inline-block">Edit Pet</h3>
          </div>
        </div>
        
         <!-- END: Main Menu-->
        <div class="row match-height"  >
            <div class="col-md-12">
                <div class="card " style="min-height:500px;">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form-horizontal" id="editPet" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>User/editPet">
                                
                                <div class="row justify-content-md-center">
                                    <div class="col-md-12">
                                        <div class="form-body">
                                            <input id="app_user_id" name="app_user_id" class="form-control" value="<?php echo $this->session->userdata('id'); ?>" type="hidden" >
                                            <input id="id" name="id" class="form-control" value="<?php echo $pet['id']; ?>" type="hidden" >
                                            <fieldset class="mb-2">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="phoneNumber4"><?= lang('petCoverPhoto') ?> </label>
                                                            <?php $class='fileupload-new';?>
                                                            <div class="fileupload <?php echo $class;?>" data-provides="fileupload">
                                                                <div class="user-image">
                                                                    <div class="fileupload-new thumbnail">
                                                                        <img src="<?php echo base_url();?>assets/default-p1.png" alt="" class="img1 img-responsive">
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail">
                                                                        <img src="<?php echo base_url();?>assets/default-p1.png" alt="" class="img1 img-responsive">
                                                                    </div>
                                                                    <div class="user-image-buttons">
                                                                        <span class="btn btn-success btn-file btn-sm"><span class="fileupload-new" style="text-align: center !important;">Select image</span><span class="fileupload-exists" style="width:70px!important; float: left;margin-left:3px">Change</span>
                                                                        <input type="file" name="employee_photo" accept="image/png, image/jpeg,image/jpg">
                                                                        <input type="hidden" name="avatar_data">
                                                                        </span>
                                                                        <a href="javascript:void(0)" class="btn btn-danger fileupload-exists btn-sm" data-dismiss="fileupload"  style="float: left; margin-right:8px;width:97px!important;">
                                                                        Remove
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                        <label for="eventInput1"><?= lang('petName') ?></label>
                                                        <input id="name" name="name" class="form-control" value="<?php if(!empty($pet)) echo $pet['name'] ?>" type="text">
                                                        </div>
                                                        <div class="row mt-3" style="margin-left: 0px;">
                                                            <div class="row form-group col-md-6">
                                                                <label style="margin-top:2%;" class="mr-2">Type :</label>
                                                                <ul class="form-group nav nav-pills nav-pill-bordered" style="float: right !important;">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link TypeCat <?php if($pet['type']=='Cat'){echo 'active' ;}?>" id="home1-tab" data-toggle="pill" href="#home1" value="Cat" aria-expanded="true">Cat</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link TypeDog <?php if($pet['type']=='Dog'){echo 'active' ;}?>" value="Dog" id="profile1-tab" data-toggle="pill" href="#profile1" aria-expanded="false">Dog</a>
                                                                    </li>
                                                                </ul>
                                                                <input id="type" name="type"  class="form-control typeInput" value="" type="hidden">
                                                            </div>
                                                            <div class="row form-group col-md-6 " style="margin-left: 22px;;">
                                                                <label style="margin-top:2%;" class="mr-2">Gender :</label>
                                                                <ul class="form-group nav nav-pills nav-pill-bordered" style="float: right;">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link genderMale <?php if($pet['gender']=='Male'){echo 'active' ;}?>" id="home1-tab" data-toggle="pill" href="#home1" aria-expanded="true">Male</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link genderFemale <?php if($pet['gender']=='Female'){echo 'active' ;}?>" id="profile1-tab" data-toggle="pill" href="#profile1" aria-expanded="false">Female</a>
                                                                    </li>
                                                                </ul>
                                                                <input id="gender" name="gender"  class="form-control genderInput" value="" type="hidden">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                            <label for="eventInput2"><?= lang('petWeight') ?></label>
                                                            <input id="weight" name="weight"  class="form-control" value="<?php if(!empty($pet)) echo $pet['weight'] ?>" type="text" >
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="phoneNumber4">Size </label>
                                                                <select class="custom-select form-control" name="size" id="eventType4" data-placeholder="Type to search cities" name="eventType4">
                                                                    <?php $petSizeArray= array('small','medium','large','xl','xxl');?>
                                                                        <?php foreach($petSizeArray as $key =>$size){ 
                                                                            $dropDownSelected=($size == $pet['size'])? 'selected' : '';
                                                                            ?>                    
                                                                            <option value="<?php echo $size; ?>"<?php echo $dropDownSelected; ?>><?php echo $size; ?></option>
                                                                        <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                            <label for="eventInput2">Birth Date</label>
                                                            <input id="birth_date" name="birth_date" class="form-control" value="<?php echo $pet['birth_date'] ?>" type="date">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                            <label for="eventInput2">Color</label>
                                                            <input id="color" name="color" class="form-control" value="<?php if(!empty($pet)) echo ucfirst($pet['color']) ?>" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="form-group custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input" id="customSwitch1" <?php if($pet['spayed']=='true'){echo 'checked' ;}?> name="spayed[]">
                                                            <label class="custom-control-label" for="customSwitch1">Spayed / Neutered </label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="eventInput2">Notes From Customer</label>
                                                            <textarea class="form-control" name="description" id="description" rows="5"><?php if(!empty($pet)) echo $pet['description'];?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="id" value="<?php if(!empty($pet)) echo $pet['id'] ?>">

                                                <div class="row" style="float:right !important; margin-right:0px;">
                                                    <button type="submit" style="margin-right: 10px;" id="button">
                                                    <?= lang('update') ?>
                                                    </button>
                                                    <a type="submit" href="<?php echo base_url('User/getAllPets') ?>" id="button">
                                                    Cancel
                                                    </a>
                                                </div>
                                            </fieldset>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </div>
    <!-- END: Content-->

<!-- Modal -->
<div class="modal fade text-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Delete Pet Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this pet image?. This will delete image permanently.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <a href="javascript:void(0)" class="btn btn-danger deleteButtonModel"><i class="la la-trash"></i> Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-fixed-footer" id='ResponseModal' >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <p></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="sizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Size Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Image size should atleast be 150x150.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Crop Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <img class="c_resize" src="" style="width: 100%;"  />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary setDefaultImgBtn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary cropSaveBtn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="WebcamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Pic From Webcam</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="" style="text-align: -webkit-center;">
                        <div id="my_result"></div>
                        <a href="javascript:void(take_snapshot)" class="btn btn-primary take_click">Take Snapshot</a>
                    </div>
                    <div class="">
                        <div id="myImageWebcam" style="display: none;">
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="extensionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Extension Error</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="text-align: center;max-height: 330px">
                    <p>Please make sure you upload an image file.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger setDefaultImgBtn" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="resized" style="display: none;"></div>

<script src="<?php echo base_url(); ?>assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/forms/wizard-steps.js"></script>

<script> 

   $(document).ready(function(){

        $(document).on('click','.TypeDog',function(){
            $('.typeInput').val("Dog");
        });
        $(document).on('click','.TypeCat',function(){
            $('.typeInput').val("Cat");
        });

        $(document).on('click','.genderFemale',function(){
            $('.genderInput').val("Female");
        });
        $(document).on('click','.genderMale',function(){
            $('.genderInput').val("Male");
        });

      $(document).on('click','#description',function(){
         $('#description-error').show();
      });
      
      $(document).on('focusout','#description',function(){
         $('#description-error').hide();
      });
      jQuery.validator.addMethod("greaterThan", 
         function(value, element, params) {
            if($(params).val().length>0){
               if (!/Invalid|NaN/.test(new Date(value))) {
                 return new Date(value) > new Date($(params).val());
               }
   
               return isNaN(value) && isNaN($(params).val()) 
                 || (Number(value) > Number($(params).val()));
             }else{
               return true;
             }   
   
         },'Must be greater than date.'); 
   
   $('input[name="employee_photo"]').click(function(){
      $(this).val(null);
   });
   
   $('input[name="employee_photo"]').change(function(){
      if($(this)[0].files.length <= 0){
            return;
        }
        var f = $(this)[0].files[0];
        var reader = new FileReader();
        
        // Closure to capture the file information.
       reader.onload = (function(file) {
           return function(e) {
                $('.c_resize').cropper({
                    aspectRatio: 1,
                    minCropBoxWidth: 200,
                    minCropBoxHeight: 200,
                    crop: function(e) {
                    }
                });
                var image = document.createElement('img');
                image.addEventListener('error', function() {
                        $('#extensionModal').modal('show');
                        return;
                });
                image.addEventListener('load', function() {
                    if(image.width < 150 || image.height < 150){
                        $('#sizeModal').modal('show');
                        return;
                    }
                    $('#cropModal').on('shown.bs.modal', function() {
                   $('.c_resize').cropper('replace', e.target.result);
               });
               $('.c_resize').prop('src', e.target.result);
               $('#cropModal').modal('show');
               });
               image.src = e.target.result;
           }
      })(f);
   
         // Read in the image file as a data URL.
         reader.readAsDataURL(f);
   
   });
   
   $('.setDefaultImgBtn').click(function(){     
        var url = $('.user-image .fileupload-preview img').prop('src');
      $('input[name="engimage_data"]').val(url);
   });
   
   $('.cropSaveBtn').click(function(){
      var canvas = $('.c_resize').cropper('getCroppedCanvas');
      $('.resized').html(canvas);
      var url = $('.resized canvas')[0].toDataURL();
          
        $('.user-image .fileupload-preview img').prop('src', url);
        $('#cropModal').modal('hide');
      $('input[name="engimage_data"]').val(url);
   });
   
   //delete image modal 
   $(document).on('click','.deleteButton',function(){
   
      if(global == 0)
      {  
         $(document).find("#deleteModal").modal('show');
         $(document).find('.deleteButtonModel').attr('data-href',$(this).attr('data-href'));
      }
      else
      {
         $('input[name="avatar_data"]').val('');
         // $('.fileupload').fileupload('clear');
      }
   
   });

   $('#newPet').find('input[type=radio][name=type]').change(function() {
   
      $('#newPet').find('.breed_id').parents('.form-group').show();
      $('#newPet').find('#mix_breed').parents('.form-group').show();
      if (this.value == 'Cat') {
         fillBreedDropDown('Cat');
        
      }
      else if (this.value == 'Dog') {
         fillBreedDropDown('Dog');
         // $("#breed_id").val([]);
      }
   
   });
  
   });
   
   
</script>
<!-- Pet Notes only for edit pet view -->
<script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" scr="https://momentjs.com/downloads/moment-timezone.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<style type="text/css">
   .datepicker-dropdown {
   z-index: 1500 !important;
   }
   .datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
   color: #ccc ;
   }
</style>


<script>
    $(document).ready(function(){
        $('#editPet').validate({
            onfocusout: function(e) {
                this.element(e);						  
            },
            onkeyup: false,
            rules: {
                app_user_id: {
                     //required : true
                  },
                  name : {
                     required : true
                  },
                  type : {
                     required: true
                  },
                  gender : {
                     //required: true
                  },
                  weight : {
                     //required: true,
                     number:true,
                     min:0
                  },
                  size : {
                     //required: true
                  },
                  birth_date : {
                     //required:true
                  },
                  color:{
                     //required:true
                  },
            },
            messages:{
                name : {
                    remote : "Please enter pet name"
                  },
                  breed_id : {
                    remote : "Please select pet type first"
                  },
            },
            errorElement : 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

    })
</script>