<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/invoice.css">

   <!-- BEGIN: Content-->
    <div class="app-content content" >
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Invoice</h3>
                </div>
            </div>

            <div class="content-body" >
               <section class="card">
                    <div id="invoice-template" class="card-body p-4">
                        <!-- Invoice Company Details -->
                        <div id="invoice-company-details" class="row">
                            <div class="col-sm-6 text-sm-left">
                              <a href="javascript:void(0);" target="_blank" class="btn btn-print btn-sm">Print
                              </a>
                            </div>
                            <div class="col-sm-6 text-sm-right">
                              <input type="hidden" class="current_invoiceno" value="<?php echo $allappointments[0]['invoice_no'];?>">
                                 <?php if($allappointments[0]['invoice_no'] > 0) { ?>
                                    <p class="text-muted invoice_no">Invoice No: <?php echo $allappointments[0]['invoice_no'];?></p>
                                 <?php } ?>
                            </div>
                        </div>

                        
                        <?php foreach ($allappointments as $key1 => $value1) { 

                           if($value1['category'] == 'appointment'){
                              $value1['category'] = 'Grooming';
                           }

                        ?>  
                       
                           <?php if($value1['category'] == 'Grooming'){ 
                              $page_name = 'viewAppointment';
                           }else if($value1['category'] == 'boarding'){
                              
                              $page_name = 'viewBoarding';
                           }else{
                              $page_name = 'viewDaycare';
                           } 
                           ?>
                           <div id="invoice-customer-details" class="row">
                              <div class="col-sm-6 text-sm-left">
                                 <p class="text-muted bold" style="margin-bottom: 0rem;"><?php echo $value1['petname'];?>: <?php echo ucfirst($value1['category']);?></p>
                              </div>
                              <div class="col-sm-6 text-sm-right">
                                    <p style="margin-bottom: 0rem;" class="badge badge-danger badge-square" style="background-color:<?php echo $colorArray[$value1['status']]; ?>"><?php echo statusDisplay($value1['status']); ?></p>
                                    
                              </div>
                           </div>

                           <div id="invoice-items-details">
                              <div class="row">
                                 <div class="table-responsive col-12">
                                    <table class="table table-bordered mb-0">
                                          <thead>
                                             <tr>
                                                <th class="text-center">SERIVCE</th>
                                                <th class="text-center datetable">DATE</th>
                                                <th class="text-center">AMOUNT</th>
                                                <th class="text-center">DISCOUNT</th>
                                                <th class="text-center">PAYABLE</th>
                                                <?php if($allappointments[0]['is_paid'] == 0 && $flag == 1 && $allappointments[0]['status'] != 'Cancel'){?>
                                                <th>ACTION</th>
                                                <?php } ?>
                                             </tr>
                                          </thead>
                                          <tbody>
                                        
                                          <?php if($value1['category'] == 'Grooming'){ 
                                             foreach ($value1['invoice_items']['services'] as $key => $value) { ?>
                                          <tr>
                                             <td class="text-center editfield service_name"><?php echo ucfirst($value['name']) ?></td>
                                             <td class="text-center">
                                                <?php if(!empty($value1['check_in_time'])) { ?>
                                                <?php echo date(get_option('date_format'), strtotime($value1['check_in_time'])); ?>
                                                <?php if(!empty($value1['check_out_time'])) { ?>
                                                - <?php echo date(get_option('date_format'), strtotime($value1['check_out_time'])); ?>
                                                <?php } } else{ ?>
                                                <?php echo date(get_option('date_format'), strtotime($value1['appointment_date'])); }?>      
                                             </td>
                                             
                                             <td class="text-center service_costTd costcolumn"> 
                                                <span><?php echo get_option('currency_symbol');?></span>
                                                <input type="number" onkeyup="if(this.value<0){this.value= this.value * -1}" style="border:unset;margin-left:5px;" value="<?php echo number_format($value['cost'],2);?>" name="fname" class="editfield service_cost inputgrp" readonly>
                                             </td>

                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php if(isset($value['discount'])){ echo number_format($value['discount'],2); }else{ echo '0.00'; } ?>
                                             </td>

                                             <td class="text-center"><?php echo get_option('currency_symbol');?> <?php echo number_format($value['payable_cost'],2);?></td>
                                             
                                             
                                             <?php if($allappointments[0]['is_paid'] == 0 && $flag == 1 && $allappointments[0]['status'] != 'Cancel'){?>
                                             <td class="text-center actioncolumn" >
                                                <i class='fas fa-edit editrow' data-toggle="tooltip"
                                                   title="Edit" data-type="services"></i>
                                                <i class="fa fa-floppy-o saverowbtn" 
                                                   data-type="services"
                                                   data-toggle="tooltip"
                                                   title="Save"
                                                   data-service_id="<?php echo $value['id']; ?>"
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   aria-hidden="true" style="display:none">
                                                </i>
                                                <?php if(count($value1['invoice_items']['services']) > 1){?>
                                                   <a data-toggle="modal" href="#removeconf" class="removerow" 
                                                      data-type="services"
                                                      data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                      data-service_id="<?php echo $value['id']; ?>"
                                                   >
                                                      <i class="fas fa-minus-circle removerow"
                                                      data-toggle="tooltip"
                                                      title="Remove">
                                                      </i>
                                                   </a>
                                                <?php } ?>
                                                
                                                <i id="" class="fas fa-percent servicediscount" 
                                                   data-toggle="tooltip"
                                                   title="Discount"
                                                   data-keyvalue="<?php echo $key; ?>" 
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   data-service_id="<?php echo $value['id']; ?>"
                                                   data-service_type="<?php echo $value['is_Add_On']; ?>"
                                                   data-cost="<?php echo $value['cost']; ?>">      
                                                </i>
                                             </td>
                                             <?php } ?>
                                          </tr>
                                          <?php } } else{ ?>
                                          <tr>
                                             <td class="text-center"><?php echo ucfirst($value1['category']); ?></td>
                                             <td class="text-center">
                                                <?php if($value1['category'] == 'boarding'){
                                                   echo 
                                                date(get_option('date_format'), strtotime($value1['start_date'])).
                                                ' - '.
                                                date(get_option('date_format'), strtotime($value1['end_date']));
                                                   
                                                }else{
                                                   echo date(get_option('date_format'), strtotime($value1['start_date']));
                                                }?>
                                             </td>

                                             <td class="text-center service_costTd costcolumn"><span> 
                                                <?php echo get_option('currency_symbol');?>
                                                <?php if($value1['category'] == 'boarding'){

                                                   if($value1['pet_type'] == 'Dog'){
                                                      echo '('.$boardingSettings['dogBoardingPricing'].')';
                                                   }else{
                                                      echo '('.$boardingSettings['catBoardingPricing'].')';
                                                   }
                                                }?>

                                                </span>
                                                <input type="number" onkeyup="if(this.value<0){this.value= this.value * -1}" style="border:unset;margin-left:5px;" value="<?php echo number_format($value1['invoice_items']['appointment']['amount'],2); ?>" name="fname" class="editfield service_cost inputgrp" readonly>
                                             </td>

                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php if(isset($value1['invoice_items']['appointment']['discount'])){ echo number_format($value1['invoice_items']['appointment']['discount'],2); }else{ echo '0.00'; } ?>
                                             </td>
                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php echo number_format($value1['invoice_items']['appointment']['payable_cost'],2); ?>
                                             </td>
                                             <?php if($allappointments[0]['is_paid'] == 0 && $flag == 1 && $allappointments[0]['status'] != 'Cancel'){?>
                                             <td class="text-center actioncolumn" >
                                                <i class='fas fa-edit editrow' data-toggle="tooltip"
                                                   title="Edit" data-type="services"></i>
                                                <i class="fa fa-floppy-o saverowbtn" 
                                                   data-toggle="tooltip"
                                                   title="Save"
                                                   data-type="services"
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   aria-hidden="true" style="display:none">
                                                </i>
                                             
                                                <i id="" class="fas fa-percent servicediscount" 
                                                   data-toggle="tooltip"
                                                   title="Discount"
                                                   data-keyvalue="<?php echo $key; ?>" 
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   data-cost="<?php echo $value1['invoice_items']['appointment']['amount']; ?>">      
                                                </i>
                                             </td>
                                             <?php } ?>
                                          </tr>
                                          <?php if($value1['invoice_items']['appointment']['overstay'] != 0) { ?>
                                    

                                          <tr>
                                             <?php if($value1['invoice_items']['appointment']['overstay'] > 0) { ?>
                                             <td >Overstay</td>
                                             <?php }else{ ?>
                                             <td >Understay</td>
                                             <?php } ?>
                                             <td class="text-center datecolumn">
                                                <?php if(!empty($value1['check_in_time'])) { ?>
                                                <?php echo date(get_option('date_format'), strtotime($value1['check_in_time'])); ?>
                                                <?php if(!empty($value1['check_out_time'])) { ?>
                                                - <?php echo date(get_option('date_format'), strtotime($value1['check_out_time'])); ?>
                                                <?php } } else{ ?>
                                                <?php echo date(get_option('date_format'), strtotime($value1['start_date'])); ?>
                                                <?php if(!empty($value1['category'] == 'boarding')) { ?>
                                                - <?php echo date(get_option('date_format'), strtotime($value1['end_date'])); ?>
                                                <?php } }?>
                                             </td>
                                          
                                             <td class="text-center service_costTd costcolumn"><span> <?php echo get_option('currency_symbol');?></span><input type="text" style="border:unset;margin-left:5px;" value="<?php echo $value1['invoice_items']['appointment']['overstay']; ?>" name="fname" class="editfield overstay inputgrp valid" readonly onchange="validateFloatKeyPress(this);">
                                             </td>
                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php echo '0.00'; ?>
                                             </td>

                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php echo $value1['invoice_items']['appointment']['overstay']; ?>
                                             </td>

                                             <?php if($allappointments[0]['is_paid'] == 0 && $flag == 1 && $allappointments[0]['status'] != 'Cancel'){?>
                                             <td class="text-center actioncolumn">
                                                <i class='fas fa-edit editrow' data-type="overstay" 
                                                   data-toggle="tooltip"
                                                   data-appointment_id="<?php echo $value1['id']; ?>"
                                                   data-category="<?php echo $value1['category']; ?>" 
                                                   title="Edit"></i>
                                                <i class="fa fa-floppy-o saverowbtn"
                                                   data-toggle="tooltip"
                                                   title="Save" 
                                                   data-type="overstay"
                                                   data-appointment_id="<?php echo $value1['id']; ?>"
                                                   data-category="<?php echo $value1['category']; ?>" 
                                                   aria-hidden="true" style="display:none"></i>
                                                <a data-toggle="modal" href="#removeconf">
                                                
                                                </a>
                                             </td>
                                             <?php } ?>
                                          </tr>


                                          <?php } } ?>
                                          <?php foreach ($value1['invoice_items']['additional_cost'] as $key => $value) { ?>
                                          <tr>
                                             <td class="text-center editfield additional_note"><?php echo $value[0] ?></td>
                                             <td class="text-center datecolumn">
                                                <?php echo date(get_option('date_format'), strtotime($value[2])); ?>
                                             </td>
                                             <td class="text-center service_costTd costcolumn"><span> <?php echo get_option('currency_symbol');?></span><input type="text" style="border:unset;margin-left:5px;" value="<?php echo number_format($value[1],2); ?>" name="fname" class="editfield additional_cost inputgrp" readonly>

                                             </td>
                                             <td class="text-center datecolumn">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php if(isset($value[3])){echo number_format($value[3],2);}else{echo "0.00";} ?>
                                             </td>
                                             
                                          
                                             <td class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php echo number_format($value[4],2); ?>
                                             </td>

                                             <?php if($allappointments[0]['is_paid'] == 0 && $flag == 1 && $allappointments[0]['status'] != 'Cancel'){?>
                                             <td class="text-center actioncolumn">
                                                <i class='fas fa-edit editrow' data-type="additional_cost" 
                                                   data-toggle="tooltip"
                                                   title="Edit"></i>
                                                <i class="fa fa-floppy-o saverowbtn"
                                                   data-toggle="tooltip"
                                                   title="Save" 
                                                   data-type="additional_cost"
                                                   data-keyvalue="<?php echo $key; ?>" 
                                                   data-additional_note="<?php echo $value[0]; ?>" 
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   aria-hidden="true" style="display:none"></i>
                                                <a data-toggle="modal" href="#removeconf" class="removerow"
                                                   data-keyvalue="<?php echo $key; ?>" 
                                                   data-type="additional_cost"
                                                   data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                   data-additional_note="<?php echo $value[0]; ?>" data-additional_cost="<?php echo $value[1]; ?>"
                                                >
                                                   <i class="fas fa-minus-circle" 
                                                   data-toggle="tooltip"
                                                   title="Remove"></i>
                                                </a>
                                                <?php if($value[1]>0){ ?>
                                                   <i id="" class="fas fa-percent addtionaldiscount" 
                                                      data-toggle="tooltip"
                                                      title="Discount"
                                                      data-keyvalue="<?php echo $key; ?>" 
                                                      data-type="additional_cost"
                                                      data-category="<?php echo $value1['category']; ?>" data-appointment_id="<?php echo $value1['id']; ?>"
                                                      data-additional_note="<?php echo $value[0]; ?>" data-additional_cost="<?php echo $value[1]; ?>">      
                                                   </i>
                                                <?php } ?>
                                             </td>
                                             <?php } ?>
                                          </tr>
                                          <?php } ?>


                                          <tr>
                                             <th colspan="4" class="text-right">Deposit</th>
                                             
                                             <th class="text-center">
                                                <?php echo get_option('currency_symbol');?>
                                                <?php if($value1['deposit']>0){ echo ' -'; }?><?php echo $this->localization->currencyFormat($value1['deposit']); ?>  
                                             </th>
                                             
                                             <?php if($allappointments[0]['is_paid'] == 0 && $allappointments[0]['status'] != 'Cancel'){ ?>
                                             <!-- <th class="text-center"></th> -->
                                             <?php }?>
                                          </tr>

                                       
                                          <tr>
                                             <th colspan="4" class="text-right">Total Payable</th>
                                             <th class="text-center"><span class="app_total_cost" data-appointment_id="<?php echo $value1['id'];?>"><?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat($value1['appointment_cost']); ?></span></th>
                                             <?php if($allappointments[0]['is_paid'] == 0 && $allappointments[0]['status'] != 'Cancel'){ ?>
                                             <!-- <td class="text-center"></td> -->
                                             <?php }?>
                                          </tr>

                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        <?php }?>



                         <!-- Invoice Customer Details -->
                         <div id="invoice-customer-details" class="row pt-3">
                            <div class="col-sm-6 text-sm-left">
                              <p class="text-muted bold" style="margin-bottom: 0rem;">Invoice Transactions</p>
                            </div>
                            <div class="col-sm-6 text-sm-right ">
                              <?php if(($allappointments[0]['is_paid'] == 0) && ($flag == 1) && ($allappointments[0]['status'] == 'Checkout' || $allappointments[0]['status'] == 'Complete' || $allappointments[0]['status'] == 'Inprocess'|| $allappointments[0]['status'] == 'Confirm')){?>
                                 <?php if($manual_discount == 0){ ?>
                                    <a href="#manualdismodal" type="button" data-toggle="modal" title="" class="btn topbtns btn-flat btn-xs btn-primary manualdisbtn" style="float: right;">
                                    Manual Discount
                                    </a>
                                    <?php }else if($manual_discount > 0){ ?>
                                    <a href="#removemanualdisconfmodal" data-href="<?php echo base_url('Admin/remove_manualdiscount/'.$allappointments[0]['invoice_no']) ?>" type="button" data-toggle="modal" class="btn topbtns btn-flat btn-xs btn-danger removemd" style="float: right;">
                                    Remove Manual Discount
                                    </a>
                                 <?php } ?>
                              <?php } ?>
                              <p class="badge badge-danger badge-square transaction" style="margin-bottom: 0rem;">Credit Balance: <?php echo get_option('currency_symbol');?> <?php echo $credit_balance; ?></p>
                            </div>
                        </div>
                        <!-- Invoice Customer Details -->
                        <div id="invoice-items-details">
                           <div class="row">
                              <div class="table-responsive col-12">
                                 <table class="table table-bordered mb-0">
                                       <thead>
                                          <tr>
                                             <th colspan="4" class="text-right">Total Cost</th>
                                             <th class="text-center"><?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat(number_format($total_payable,2)); ?>
                                             </th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php if($manual_discount >0 && $allappointments[0]['status'] != 'Cancel'){ ?>
                                             <tr>
                                                <td class="text-right" colspan="4">Manual Discount</td>
                                                <td class="text-center">
                                                   <?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat($manual_discount); ?>
                                                </td>
                                             </tr>
                                          <?php } ?>
                                          <tr>
                                             <td class="text-right" colspan="4">Total Deposit</td>
                                             <td class="text-center"><?php echo get_option('currency_symbol');?>
                                             <?php if($total_deposit>0){ echo ' -'; }?><?php echo $this->localization->currencyFormat($total_deposit); ?></td>
                                          </tr>

                                          <?php if(!empty($all_transactions)){ 
                                          foreach ($all_transactions as $key => $value) { ?>
                                             <tr>
                                                <td class="text-right" colspan="4"><?php echo $value['payment_name']; ?></td>
                                                <td class="text-center"><?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat($value['amount']); ?></td>
                                             </tr>
                                          <?php } }?>


                                             <?php if($allappointments[0]['is_paid'] == 0){?> 
                                             <tr>
                                                <th colspan="4" class="text-right">Balance Payable</th>
                                                <th class="text-center">
                                                   <?if($allappointments[0]['status'] == 'Cancel'){
                                                      $payable_amount = 0.00;
                                                   }?>
                                                   <?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat($payable_amount); ?>
                                                </th>
                                             </tr>
                                          <?php }?> 

                                          <?php if($allappointments[0]['is_paid'] == 1){?> 
                                          <tr>
                                             <th colspan="4" class="text-right">Total Paid</th>
                                             <th class="text-center"><?php echo get_option('currency_symbol').' '.$this->localization->currencyFormat($total_paid); ?></th>
                                          </tr>
                                          <?php }?>
                                       
                                          <tr>
                                             <th colspan="4" class="text-right">Invoice Status</th>
                                             <?php if($allappointments[0]['is_paid'] == 1){?>
                                                <th class="text-center blink-paid">Paid</th>
                                             <?php }else if($allappointments[0]['status'] == 'Cancel'){?>
                                                <th class="text-center blink-paid">Cancelled</th>
                                             <?php }else{ ?>
                                                <th class="text-center blink-paid" style="color: red;">Unpaid</th>
                                             <?php } ?>
                                          </tr>
                                       </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>     
    
    


    <!-- pay invoice modal (By Aj)-->
<div class="modal fade" id='paymodal' role="dialog" >
   <form class='form-horizontal' id="payInvoiceForm" method="post" action="<?php echo base_url()?>Admin/payInvoice">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close checkoutModalclose" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Pay Invoice</h4>
            </div>
            <div class="modal-body">
               <input name="invoice_id" id="invoice_id" class="form-control" value=""  type="hidden">
               <div class="form-group margin">
                  <label for="field-1" class="col-sm-4 control-label">Amount</label>
                  <div class="col-sm-6 input-icon">
                     <div class="input-icon">
                        <input name="payamount" id="payamount" class="form-control" value="" onchange="validateFloatKeyPress(this);" type="text">
                        <i>$</i>
                     </div>
                     <label id="payamount-error" class="help-block animated fadeInDown" for="payamount"></label>
                  </div>
               </div>

               <div class="form-group margin creditDiv" style="display: none;">
                  <div class="col-sm-8 creditDiv1">
                     <div class="checkbox" style="padding-bottom: 7px;">
                        <label class="moduleCheckbox">
                        <input type="checkbox" id="useCredits" name="credits" value="1">
                        Use customer credit balance (<span>$ 0.00</span>)
                        </label>
                     </div>
                  </div>
               </div>
            
               <div class="form-group margin PaySourceBtn PaySourcediv">
                  <label for="field-1" class="col-sm-4 control-label">Source</label>
                  <div class="col-sm-6 drop-down">
                     <select class="form-control " name="payment_source" id="paymentSource">
                        <option value="Cash">Cash</option>
                        <option value="Credit/Debit">Credit/Debit</option>
                        <option value="Other">Other</option>
                     </select>
                   </div>
               </div>
               <div class="form-group margin otherSourceDiv" style="display: none;">
                  <label for="field-1" class="col-sm-4 control-label">Other Source <span class="required"></span></label>
                  <div class="col-sm-6">
                     <input name="other_source" id="other_source" class="form-control" value=""  type="text">
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" >Close</button>
               <button type="submit" class="btn btn-primary btn-flat">Pay</button>
            </div>
         </div>
      </div>
</div>
</form> 
</div>

<!-- mark paid invoice modal (By Aj)-->
<div class="modal fade" id='markpaidmodal' role="dialog" >
   <form class='form-horizontal' id="markpaidInvoiceForm" method="post" action="<?php echo base_url()?>Admin/markpaidInvoice">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close checkoutModalclose" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Mark Paid</h4>
            </div>
            <div class="modal-body">
               <p>Are you sure you want to mark this invoice as a paid?<br>
                  If there is balance available in invoice, then it will be deducted from/credited to customer's credits.
               </p>
               <input name="invoice_id" id="invoice_id" class="form-control" value="<?php echo $allappointments[0]['invoice_no'];?>"  type="hidden">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" >Close</button>
               <button type="submit" class="btn btn-primary btn-flat">Mark Paid</button>
            </div>
         </div>
      </div>
   </div>
   </form> 
</div>


<div class="modal fade" id='manualdismodal' role="dialog">
   <form role="form" class="form-horizontal addmanualdis" id="addmanualdisform" method="post" action="<?php echo base_url()?>Admin/addmanualdiscount">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
               &times;
               </button>
               <h4 class="modal-title">Manual Discount</h4>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="form-group margin">
                     <label for="field-1" class="col-sm-3 control-label" style="margin-top: 0px">Manual Discount</label>
                     <div class="col-sm-8">
                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="1"  class="discountRadio disableRadio" aria-invalid="false" checked="">
                        <span></span>Discount in ($)
                        </label>
                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="0"  class="discountPerRadio disableRadio">
                        <span></span>Discount in (%)
                        </label>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountDiv" style="padding-top:15px">
                     <label for="field-1" class="col-sm-3 control-label">Discount($) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="manual_discount" id="manual_discount" class="form-control" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i><?php echo get_option('currency_symbol');?></i>
                        </div>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountPerDiv" style="display: none;">
                     <label for="field-1" class="col-sm-3 control-label">Discount(%) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="manual_discountPer" id="manual_discountPer" class="form-control" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i class="fa fa-percent" aria-hidden="true" style="font-size: 12px;color: #646464e6;"></i>
                        </div>
                     </div>
                  </div>
                  <input name="invoice_no" value="<?php echo $allappointments[0]['invoice_no'];?>" type="hidden" >  
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="sumbit"  class="btn btn-primary"> Save</button>
            </div>
         </div>
      </div>
   </form>
</div>



<!--  discount  on additional cost -->

<div class="modal fade" id='addtionaldismodal' role="dialog">
   <form role="form" class="form-horizontal addmanualdis" id="addCustomeDiscount" method="post" action="<?php echo base_url()?>Admin/additionaldiscount">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
               &times;
               </button>
               <h4 class="modal-title">Discount On Additional Cost</h4>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="form-group margin">
                     <label for="field-1" class="col-sm-3 control-label" style="margin-top: 0px">Discount On Additional Cost</label>
                     <div class="col-sm-8">
                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="1"  class="discountRadio disableRadio" aria-invalid="false" checked="">
                       
                        <span></span>Discount in ($)
                        </label>
                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="0"  class="discountPerRadio disableRadio">
                        <span></span>Discount in (%)
                        </label>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountDiv" style="padding-top:15px">
                     <label for="field-1" class="col-sm-3 control-label">Discount($) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="addition_add_discount" id="addition_add_discount" class="form-control" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i><?php echo get_option('currency_symbol');?></i>
                        </div>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountPerDiv " style="display: none;">
                     <label for="field-1" class="col-sm-3 control-label">Discount(%) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="manual_discountPer" id="manual_discountPer" class="form-control discountinpercent" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i class="fa fa-percent" aria-hidden="true" style="font-size: 12px;color: #646464e6;"></i>
                        </div>
                     </div>
                  </div>
                  <input name="invoice_no" value="<?php echo $allappointments[0]['invoice_no'];?>" type="hidden" >  
               </div>
            </div>
            <div class="modal-footer">
                      <input type="hidden" name="appointment_id">
                        <input type="hidden" name="category">
                        <input type="hidden" name="datatype">
                        <input type="hidden" name="datacost" class="datacost">
                        <input type="hidden" name="keyvalue">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="sumbit"  class="btn btn-primary addDiscount"> Save</button>
            </div>
         </div>
      </div>
   </form>
</div>
<!-- end discount  on additional cost -->


<!--  discount  on service cost -->

<div class="modal fade" id='servicedismodal' role="dialog">
   <form role="form" class="form-horizontal" id="addServicedDiscount" method="post" action="<?php echo base_url()?>Admin/servicediscount">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
               &times;
               </button>
               <h4 class="modal-title">Discount On Service Cost</h4>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="form-group margin">
                     <label for="field-1" class="col-sm-3 control-label" style="margin-top: 0px">Discount</label>
                     <div class="col-sm-9">
                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="1"  class="discountRadio disableRadio" aria-invalid="false" checked="">
                        <span></span>Discount in ($)
                        </label>

                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="0"  class="discountPerRadio disableRadio">
                        <span></span>Discount in (%)
                        </label>

                        <label class="css-input css-radio css-radio-success push-10-r locRadio">
                        <input type="radio" name="discount" value="2"  class="discountAutoRadio disableRadio">
                        <span></span>Auto Discount
                        </label>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountDiv">
                     <label for="field-1" class="col-sm-3 control-label">Discount($) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="addition_add_discount" id="addition_add_discount" class="form-control" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i><?php echo get_option('currency_symbol');?></i>
                        </div>
                     </div>
                  </div>
                  <div class="form-group margin manual_discountPerDiv " style="display: none;">
                     <label for="field-1" class="col-sm-3 control-label">Discount(%) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount">
                           <input name="manual_discountPer" id="manual_discountPer" class="form-control discountinpercent" value="" type="text" onchange="validateFloatKeyPress(this);">
                           <i class="fa fa-percent" aria-hidden="true" style="font-size: 12px;color: #646464e6;"></i>
                        </div>
                     </div>
                  </div>
                  <div class="form-group margin discountAutoDiv " style="display: none;">
                     <label for="field-1" class="col-sm-3 control-label">Discount(%) <span class="required"></span></label>
                     <div class="col-sm-5">
                        <div class="input-icon errordiscount autoDiscount">

                        </div>
                     </div>
                  </div>
                  <input name="invoice_no" value="<?php echo $allappointments[0]['invoice_no'];?>" type="hidden" >  
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" name="appointment_id">
               <input type="hidden" name="category">
               <input type="hidden" name="service_id" value="0">
               <input type="hidden" name="datacost" class="datacost">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="sumbit"  class="btn btn-primary addDiscount"> Save</button>
            </div>
         </div>
      </div>
   </form>
</div>
<!-- end discount  on service cost -->


















<div class="modal fade" id='unmergeconfirmmodal' role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Unmerge Invoice</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure you want to unmerge this invoice?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="" class="btn btn-danger unmergeconfBtn">
            Unmerge
            </a>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id='sizeupgradeconf' role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Upgarde Pet Size </h4>
         </div>
         <div class="modal-body">
            <p>Are you sure you would like to change this pet's size?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="" class="btn btn-success usconfBtn">
            Yes
            </a>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id='removeconf' role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title deletefield">Delete Service</h4>
         </div>
         <div class="modal-body">
            <p class="deletefieldptag"> Are you sure you want to remove this service.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button"  class="btn btn-success removeconfBtn" >Yes</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id='removemanualdisconfmodal' role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Remove Manual Discount</h4>
         </div>
         <div class="modal-body">
            <p>Are you sure you want to remove the manual discount?</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="" class="btn btn-danger removemdBtn">
            Remove
            </a>
         </div>
      </div>
   </div>
</div>
<div id="additionalcost" class="modal fade" data-backdrop="static" tabindex="-1" >
   <form role="form" id="addadditionalcostform" class="form-horizontal addadditionalcost" method="post" action="<?php echo base_url()?>Admin/addadditionalcost">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
               &times;
               </button>
               <h4 class="modal-title">Additional Cost</h4>
            </div>
            <div class="modal-body">
               <div class="row">
                  <div class="form-group row additionalcost_div">
                     <label for="field-1" class="col-sm-1 control-label"></label>
                     <div class="col-sm-5">
                        <textarea placeholder="Additional Note"  style="resize: none;"  rows="2" name="note[]" id="note" class="form-control valid"></textarea>
                     </div>
                     

                     <div class="col-sm-4" style="margin-top: 5px;">
                        <div class="input-icon">
                          <input type="text" name="additional_cost[]" placeholder="Add Cost" id="additional_cost" value="<?php echo $this->localization->currencyFormat(0);?>" class="form-control valid" onchange="validateFloatKeyPress(this);">
                          <i><?php echo get_option('currency_symbol');?></i>
                        </div>
                        <label id="additional_cost-error" class="error" for="additional_cost"></label>
                    
                     </div>
                     
                       
                   
                     <div class="col-sm-2" style="margin-top: 10px;padding-left: 0px;">
                        <a href="#" data-toggle="modal" class="btn btn-xs btn-primary additionalcostbtn"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add </a> 
                     </div>

                  </div>
                  <input name="appointment_id" value="" type="hidden" class="appointment_id">  
                  <input name="appointment_categoty" value="" type="hidden" class="appointment_categoty">          
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <button type="sumbit"  class="btn btn-primary"> Save</button>
            </div>
         </div>
      </div>
   </form>
</div>
<div class="modal fade" id='getallactiveappointments' role="dialog" >
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Appointment Status</h4>
         </div>
         <div class="modal-body">
            <p style="color: red;"></p>
            <input type="hidden" class="currentpetName" value = "">
            <span id="scrollMssage2"> This is a scrollable table, please scroll left/right to view full content or tilt your phone for a wider view. </span>
            <div class="table-responsive" style="width: 100%;">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button"  class="btn btn-primary nextbtn" data-array="" data-app_user_id ="<?php echo $allappointments[0]['app_user_id']?>" data-id ="<?php echo $allappointments[0]['id']?>" data-category="<?php echo $allappointments[0]['category']?>"> Merge</button>
         </div>
      </div>
   </div>
</div>

<div id="addservices" class="modal fade" data-backdrop="static" tabindex="-1" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
            </button>
            <h4 class="modal-title">Add/Remove Services</h4>
         </div>
         <div class="modal-body">
            <form role="form" class="form-horizontal addservices" id="addservicesform" method="post" action="<?php echo base_url()?>Admin/addservicestoinvoice">
               <div class="row">
                  <div class="form-group margin serviceSelection">
                     <label for="field-1" class="col-sm-5 control-label petname_service"><span class="required">*</span></label>
                     <div class="col-sm-7 checkboxGroup dogDiv" style="display:none">
                        <?php foreach ($services as $value) { ?>
                        <?php if($value['is_deleted'] == 0 && $value['type'] == 'Dog'):?>
                        <div class="checkbox">
                           <label class="serviceCheckbox"><input type="checkbox" class="checkboxInput" name="services[]"
                              value="<?php echo $value['id'];?>"><?php echo $value['name'];?></label>
                           <input name="appointment_id" value="" type="hidden" class="appointment_id">
                        </div>
                        <?php endif;?>
                        <?php } ?>       
                     </div>
                     <div class="col-sm-7 checkboxGroup catDiv" style="display:none">
                        <?php foreach ($services as $value) { ?>
                        <?php if($value['is_deleted'] == 0 && $value['type'] == 'Cat'):?>
                        <div class="checkbox">
                           <label class="serviceCheckbox"><input type="checkbox" class="checkboxInput" name="services[]"
                              value="<?php echo $value['id'];?>"><?php echo $value['name'];?></label>
                           <input name="appointment_id" value="" type="hidden" class="appointment_id">
                        </div>
                        <?php endif;?>
                        <?php } ?>       
                     </div>
                     <div class="col-sm-7" style="float: right;">
                        <label id="services[]-error" class="help-block animated fadeInDown" for="services[]"></label>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" data-dismiss="modal" class="btn btn-default ">
                     Close
                     </button>
                     <button type="sumbit"  class="btn btn-primary invoicesubmit ">
                     Save
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>



<script type="text/javascript">
   var payableamount = "<?php echo number_format($payable_amount,2 ); ?>";
   var before_manual_discount = "<?php echo number_format($before_manual_discount,2 ); ?>";
   var invoice_no = "<?php echo $allappointments[0]['invoice_no'];?>";
   var arrDiscount = '<?php echo json_encode($arrDiscount);?>';
   var credit_balance = '<?php echo $credit_balance;?>';
   credit_balance = parseFloat(credit_balance);
   payableamount = parseFloat(payableamount);

   
   var currency_symbol = '<?php echo get_option('currency_symbol');?>';
</script>
<script type="text/javascript">
   function validateFloatKeyPress(el) {
      var v = el.value;
      v=  v.replace('- ', '-');
      v = parseFloat(v);
      el.value = (isNaN(v)) ? '' : v.toFixed(2);
   }

   $('.service_cost , .additional_cost, .overstay').on('keypress', function(e) {
       keys = ['0','1','2','3','4','5','6','7','8','9','.','-']
       return keys.indexOf(event.key) > -1
   })



   $(document).ready(function(){
      if(payableamount < 0){
         $(document).find('.manualdisbtn ').css({"background-color": "gray", "border": "1px solid gray"});
         $(document).find('.manualdisbtn ').attr("href", "");
         $(".manualdisbtn").hover(function(){
            //$(this).attr('data-toggle', 'tooltip');
             $(this).css('cursor','pointer').attr('title', 'Payable amount should be greater than $0.00.');
         });

      }
   
       $("#addmanualdisform").validate({
         excluded: ':disabled',
           rules: {
               manual_discount : {
                   number:true,
                   max: function(){
                       return parseFloat(before_manual_discount);
                   }
               },
               manual_discountPer : {

                   number:true,
                   max:100
               }
           },
           messages : {

           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown',
           errorPlacement: function(error, element) {
              
                       error.insertAfter(element.closest('.errordiscount'));
                   }

             
        
     });
       
     $("#addCustomeDiscount").validate({
         excluded: ':disabled',
           rules: {
            addition_add_discount : {
                   number:true,
                   required: true,
                   max: function(){
                       return parseFloat($('.datacost').val());
                   }
               },
               manual_discountPer : {

                   number:true,
                   required: true,
                   max:100
               }
           },
           messages : {

           },
           highlight: function(element) {
               $(element).closest('.form-group').addClass('has-error');
           },
           unhighlight: function(element) {
               $(element).closest('.form-group').removeClass('has-error');
           },
           errorElement: 'span',
           errorClass: 'help-block animated fadeInDown',
           errorPlacement: function(error, element) {
               error.insertAfter(element.closest('.errordiscount'));
            }
     });

   $("#addServicedDiscount").validate({
      excluded: ':disabled',
      rules: {
         addition_add_discount : {
            number:true,
            required: true,
            max: function(){
               return parseFloat($('.datacost').val());
            }
         },
         manual_discountPer : {
            number:true,
            required: true,
            max:100
         }
      },
      messages : {

      },
      highlight: function(element) {
         $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
         $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block animated fadeInDown',
      errorPlacement: function(error, element) {
         error.insertAfter(element.closest('.errordiscount'));
      }
   });
       
   $("#addservicesform").validate({
       excluded: ':disabled',
       onfocusout: function(e) {
               this.element(e);                          
       },
       rules: {
         
            'services[]' : {
   
                   required: true
                 
             }
           
       },
       highlight: function(element) {
                 $(element).closest('.form-group').addClass('has-error');
       },
       unhighlight: function(element) {
                 $(element).closest('.form-group').removeClass('has-error');
       },
         //errorElement: 'span',
         errorClass: 'help-block animated fadeInDown',
        
     });

    $("#addadditionalcostform").validate({
       excluded: ':disabled',
       onfocusout: function(e) {
               this.element(e);                          
       },
       rules: {
         
            'additional_cost[]' : {
   
                   required: true,
                  
                   // number:true

                 
             }
           
       },

      messages : {
          
       
          'additional_cost[]' : {
              requried : 'Please add cost',
              // number : 'it should contain only numbers'
          }
      },    
       highlight: function(element) {
                 $(element).closest('.form-group').addClass('has-error');
       },
       unhighlight: function(element) {
                 $(element).closest('.form-group').removeClass('has-error');
       }
        
     });
   
   function blink_text() {
        $('.blink').fadeOut(500);
        $('.blink').fadeIn(500);
    }
   setInterval(blink_text, 1000);
   
   var checkedappointmentdetails = [];
    
    $(document).on('click','.appointmentcheckboxes',function(){ 
       
        checkedappointmentdetails = [];
        
        $('.appointmentcheckboxes').each(function () {
            if (this.checked) {
                checkedappointmentdetails.push([$(this).attr('data-appointmentId'),$(this).attr('data-category')]);   
            }
        });
        
        
        console.log("checkedappointmentdetails");
        console.log(checkedappointmentdetails);
        $(document).find('.nextbtn ').attr('data-array',JSON.stringify(checkedappointmentdetails));
      
    });
   
   $(document).on('click','.nextbtn',function(){
      var app_user_id = $(this).attr('data-app_user_id');
      var category = $(this).attr('data-category');
      var id = $(this).attr('data-id');
       $.ajax({
               url: BASE_URL+'Admin/getappointmentsforinvoice/'+$('.current_invoiceno').val() ,
               type: 'POST',
               data : {
                        id : id,
                        category : category,
                        app_user_id : app_user_id,
                        allappointments :  checkedappointmentdetails
                      },
               dataType: 'json',
               async : false,
               success: function (data) {
                  console.log(data);
                  if(data){
                     $(document).find('#getallactiveappointments').modal('hide');
                        location.reload();
                  }else{
                     $(document).find('#getallactiveappointments').modal('hide');
                     $.unblockUI();   
                  }
               },
               error : function(err)
               {
                   $.unblockUI();
               }
           });
   
   });
   
    $(document).on('click','.mergebtn',function(){
      var app_user_id = $(this).attr('data-app_user_id');
      var category = $(this).attr('data-category');
      var id = $(this).attr('data-id');
    $.ajax({
            url: BASE_URL+'Admin/getRelatedPet/'+app_user_id+'/'+id+'/'+category ,
            type: 'GET',
            dataType: 'json',
            async : false,
            success: function (data) {
              console.log(data);
               if(data){
                
                    $(document).find('#getallactiveappointments').modal('show');
                    $(document).find('#getallactiveappointments .modal-body p').html('Some of your pets have active appointments currently being served. Please select the ones you want to pay along:');
                    $(document).find('#getallactiveappointments .modal-body div').html(data.result);
                    $('#relatedAppointments').DataTable({
                       "responsive":true,
                       "bInfo": false,
                       "bAutoWidth": false,
                       "bSort" : false,
                       "paging": false,
                       "searching": false,
                       "destroy" : true
                     });
               }else{
                     
               }
            },
            error : function(err)
            {
                $.unblockUI();
            }
        });
   });

   
   
   $(document).on('click','.addonbtn',function(){
      var appointment_id = $(this).attr('data-appointment_id');
      $('.appointment_id').val(appointment_id);
      var appointment_categoty = $(this).attr('data-appointment_categoty');
      $('.appointment_categoty').val(appointment_categoty);
   });

   $(document).on('click','.dropdown-menu li',function(){
      if($(this).text() == 'Upgrade Size'){
         $('#size').val($(this).find('a').attr('data-size'));
      }
   });

     // Script used for add multiple additional cost dynamically //
   
      var cost_count = 1;
     // var cost_note = 1;
      $(document).on('click','.additionalcostbtn',function(){
        var html ='';
        html += '<div class="form-group ">'+
            
              '<label class="control-label col-sm-1"></label>'+
   
         '<div class="col-sm-5">'+
   
         '<textarea placeholder="Additional Note"  style="resize: none;"  rows="2" name="note['+cost_count+']" id="note" class="form-control valid"></textarea>'+
   
   
         '</div>'+
         '<div class="col-sm-4" style="margin-top: 5px;">'+
         '<div class="input-icon">'+
         '<input type="text" name="additional_cost['+cost_count+']" placeholder="Add Cost" id="additional_cost" value="<?php echo $this->localization->currencyFormat(0);?>" class="form-control valid" onchange="validateFloatKeyPress(this);">'+
         '<i><?php echo get_option('currency_symbol');?></i>'+
         '</div>'+
         '<label id="additional_cost-error" class="error" for="additional_cost"></label>'+
         '</div>'+
   
         '<div class="col-sm-2" style="padding-top: 10px;padding-left: 2px;">'+
         '<i class="fas fa-minus-circle removeCost"></i>'+
         '</div>'+
   
          '</div>';
      
          $(".additionalcost_div").after(html);
       // cost_note++;
        cost_count++;
      });
   
      // Script used for Remove additional cost //
      $(document).on("click", ".removeCost", function() {
          $(this).fadeOut(600, function(){ $(this).parent().parent().remove();});
      });
    


       // Script used for Remove rows drom table//
      $(document).on("click", ".removerow", function() {
         var datatype = $(this).attr('data-type');
         var array = {};
         var appointment_id = $(this).attr('data-appointment_id');
         var category = $(this).attr('data-category');
         var html = '';
         if(datatype == 'additional_cost'){
            html = 'Additional Cost';
            var keyvalue =$(this).attr('data-keyvalue');
            var additional_note = $(this).attr('data-additional_note');
            var additional_cost = $(this).attr('data-additional_cost');

            array[datatype] = keyvalue+','+additional_note+','+additional_cost;
         }else if(datatype == 'services'){
            html = 'Service';
            if(category == 'Grooming'){
               var service_id = $(this).attr('data-service_id');
               array[datatype] = service_id; 
            }
         }else if(datatype == 'overstay'){
            html = 'Overstay';
            array[datatype] = appointment_id; 
         }
         $(document).find("#removeconf .deletefieldptag").html('Are you sure you want to remove this '+html+'?');
         $(document).find("#removeconf .deletefield").html('Delete '+html);
         $(".removeconfBtn").click(function(){
         $.blockUI({
             baseZ: 2000,
               css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff'
               }
            });
            $.ajax({
               url: BASE_URL+'Admin/deleteinvoice_items',
               type: 'POST',
               dataType: 'json',
               data: {
                  appointment_id: appointment_id,
                  category: category,
                  data :array
               },
               success: function (data) {
                  
                  if(data.err == 0){
                     console.log("data");
                     console.log(data);
                     $.unblockUI();
                   location.reload();
                  }else{
                     $.unblockUI();
                  }
               },
               error : function(err)
               {
                  $.unblockUI();
               }
            });
         });
      });

      
   
      $('.unmergebtn').on('click', function(e)
      { 
        var href = $(this).attr('data-href');
         $(document).find('#unmergeconfirmmodal .unmergeconfBtn').attr('href',href);
      });

      $(document).on('click','.locRadio',function(){
         $('.manual_discountPerDiv, .manual_discountDiv, .discountAutoDiv').hide();
         $('#manual_discountPer, #manual_discount').val('');
         if($(this).find('.disableRadio').val() == 1){
            $('.manual_discountDiv').show();
         }
         if($(this).find('.disableRadio').val() == 0){
            $('.manual_discountPerDiv').show();
         }

         if($(this).find('.disableRadio').val() == 2){
            $('.discountAutoDiv').show();
         }
      });

      $('.removemd').on('click', function(e)
      { 
        var href = $(this).attr('data-href');
         $(document).find('#removemanualdisconfmodal .removemdBtn').attr('href',href);
      });
      $('.usbtn').on('click', function(e)
      { 
        var href = $(this).attr('data-href');
         $(document).find('#sizeupgradeconf .usconfBtn').attr('href',href);
      });
   
   
        $(document).on('change','#auto_discount', function(){
   
            $.blockUI({
             baseZ: 2000,
               css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff'
               }
            });
   
                  $.ajax({
                    url: BASE_URL+'Admin/changeauto_discount',
                    type: 'POST',
                    dataType: 'json',
                     data: {
                          appointment_id: $(this).find('option:selected').attr('data-appointment_id'),
                          category: $(this).find('option:selected').attr('data-category'),
                          dis_id:$(this).find('option:selected').val(),
                          dis_per:$(this).find('option:selected').attr('data-percentage'),
                          invoice_no:invoice_no
                     },
                    success: function (data) {
                        
                        if(data.err == 0){
                           
                          $.unblockUI();
                         location.reload();
                        }else{
                            $.unblockUI();
                          
                        }
                    },
                    error : function(err)
                    {
                      $.unblockUI();
                      
                    }
                });
    });

    $(".inputgrp").focusout(function(){
     $(this).parent().css("border", 'none');
   }); 
   $(".inputgrp").focus(function(){
      var cost_html = $(this).parent().html();
      if(cost_html.indexOf('readonly') != -1){
         $(this).parent('.service_costTd').css("border", 'none');  
      }else{
         $(this).parent('.service_costTd').css("border", '2px solid #dbaf8a');
      }
   });   
   $(document).on('click','.editrow',function(){  

      $(this).closest('tr').find('.editfield').attr('contenteditable', true); 
      $(this).closest('tr').children('td.editfield').css("background-color", "#dddddd");
      $(this).closest('tr').find('.editfield').parent('td').css("background-color", "#dddddd");
      $(this).closest('tr').find('.inputgrp').css("background-color", "#dddddd");
      $(this).closest('tr').find('.inputgrp').attr("readonly", false);
      $('td.actioncolumn').not($(this).closest('tr td')).css('pointer-events', 'none');
      $(this).closest('tr').find('.saverowbtn').show();
      $(this).hide();
   });
     // Script used for Remove rows drom table//
      $(document).on("click", ".saverowbtn", function() {
         
          var appointment_id = $(this).attr('data-appointment_id');
          var category = $(this).attr('data-category');
          var datatype = $(this).attr('data-type');
          var array = {};
          
          if(datatype == 'additional_cost'){
            
            var keyvalue =$(this).attr('data-keyvalue');
            var additional_note =  $(this).closest('tr').children('td.additional_note').html();
            var additional_cost = $(this).closest('tr').find('.additional_cost').val();
            additional_cost =$.trim(additional_cost);
            additional_cost = additional_cost.replace(",", "");
            additional_note =$.trim(additional_note);
            array[datatype] = keyvalue+','+additional_note+','+additional_cost;
          }else if(datatype == 'services'){
            if(category == 'Grooming'){
               var service_id = $(this).attr('data-service_id');
               var service_name =  $(this).closest('tr').children('td.service_name').html();
               var service_cost = $(this).closest('tr').find('.service_cost').val();
               service_name =$.trim(service_name);
               service_cost =$.trim(service_cost);
               service_cost = service_cost.replace(",", "")
               array[datatype] = service_id+','+service_name.trim()+','+service_cost; 
            }else{
               var service_cost = $(this).closest('tr').find('.service_cost').val();
               service_cost =$.trim(service_cost);
               service_cost = service_cost.replace(",", "");
               array[datatype] = service_cost; 
            }
          }else if(datatype == 'overstay'){
           
            var overstayamt = $(this).closest('tr').find('.overstay').val();
            overstayamt =$.trim(overstayamt);
            overstayamt = overstayamt.replace(",", "");
            array[datatype] = overstayamt; 
          }
         
            $.blockUI({
             baseZ: 2000,
               css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff'
               }
          });
            $.ajax({
                url: BASE_URL+'Admin/editinvoice_items',
                type: 'POST',
                dataType: 'json',
                 data: {
                    appointment_id: appointment_id,
                    category: category,
                     data :array
                  },
                success: function (data) {
                    if(data.err == 0){
                     console.log("data");
                      console.log(data);
                      $.unblockUI();
                     location.reload();
                    }else{
                        $.unblockUI();
                    }
                },
                error : function(err)
                {
                  $.unblockUI();
                }
            });
          
      });

   // script for pay invoice
   $(document).on('click','.paybtn',function(){
      var currentinvoiceid= $('.current_invoiceno').val();      
      $(document).find('#invoice_id').val(currentinvoiceid);
      
      if(parseFloat(payableamount) < 0){
         $(document).find('#payamount').val(0.00);
      }else{
         $(document).find('#payamount').val(payableamount.toFixed(2));
      }
      
      if(credit_balance>0 && payableamount>0){
         $('.creditDiv').find('span').text(currency_symbol +' '+credit_balance.toFixed(2));
         $('.creditDiv').show();
      }
   });

   $("#payInvoiceForm").validate({
      excluded: ':disabled',
      onfocusout: function(e) {
         this.element(e);                          
      },
      rules: {   
         payamount : {
            required: true,
            number:true,
            min: 0.00
         }   
       },
      messages : {
         payamount : {
            requried : 'Please add cost',
            number : 'It should contain only numbers',
            min : 'Amount should be greater than $0.00'
         }
      },
      highlight: function(element) {
         $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
         $(element).closest('.form-group').removeClass('has-error');
      },
      //errorElement: 'span',
      errorClass: 'help-block animated fadeInDown',
   });

   $(document).on('change','#paymentSource',function(){
      if($(this).find(':selected').val() == 'Other'){
        $('.otherSourceDiv').show();
      }else{
        $('.otherSourceDiv').hide();
      }
   });
   
    $(document).on('click','.addservicesbtn',function(){
      var petname = $(this).attr('data-petname');
      var pet_type = $(this).attr('data-pettype');
      var servicearray = $(this).attr('data-servicearray');
       $.each(JSON.parse(servicearray), function( index, value ) {
            $('.checkboxInput[value="' + value + '"]').attr('checked', 'checked');
         });
      $('.catDiv, .dogDiv').hide();
      if(pet_type == 'Dog'){
         $('.dogDiv').show();
      }else{
         $('.catDiv').show();
      }
      $(document).find('.petname_service').html('Services for '+petname);
   });
});    

   
// by raju 
$(document).ready(function () {
   $(document).on("click", ".addtionaldiscount", function() {
      $('#addtionaldismodal').modal('show');
      var appointment_id = $(this).attr('data-appointment_id');
      var category = $(this).attr('data-category');
      var datatype = $(this).attr('data-type');
      var datacost = $(this).attr('data-additional_cost');
      var keyvalue = $(this).attr('data-keyvalue');
      $('input[name="appointment_id"]').val(appointment_id);
      $(' input[name="datacost"]').val(datacost);
      $(' input[name="datatype"]').val(datatype);
      $(' input[name="category"]').val(category);
      $(' input[name="keyvalue"]').val(keyvalue);
   });

   $(document).on("click", ".servicediscount", function() {
      var category = $(this).attr('data-category');
      let type = 0;
      if(category == 'Grooming'){
         $('input[name="service_id"]').val($(this).attr('data-service_id'));
         type = $(this).attr('data-service_type');
      }
      var html= '<select class="form-control" name="auto_discount">'+
         '<option value="0" data-category="" data-appointment_id="">No Discount</option>';
         $.each(JSON.parse(arrDiscount), function( key, value ) {
            if(category == 'Grooming' && ((type == 0 && value['core_service'] == 1) || (type == 1 && value['add_on_service'] == 1))){
               html +='<option value="'+value['discount_percentage']+'">'+value['discount_title']+ ' ('+value['discount_percentage']+'% )</option>';
            }else if(category != 'Grooming'){
               html +='<option value="'+value['discount_percentage']+'">'+value['discount_title']+ ' ('+value['discount_percentage']+'% )</option>';
            }
            
         });
         html +='</select>';
      $('.autoDiscount').html(html);
      $('input[name="appointment_id"]').val($(this).attr('data-appointment_id'));
      $('input[name="datacost"]').val($(this).attr('data-cost'));
      $('input[name="category"]').val(category);
      $('#servicedismodal').modal('show');
   });

   $(document).on("click", "#useCredits", function() {
      var category = $(this).attr('data-category');
      if($(this).prop("checked") == false){
         $(document).find('#payamount').val(payableamount.toFixed(2));
      }else{
         if(payableamount >=credit_balance){
            $(document).find('#payamount').val((payableamount - credit_balance).toFixed(2));
         }else{
            $(document).find('#payamount').val((0).toFixed(2));
         }
      }
   });
});
</script>