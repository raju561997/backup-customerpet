<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/pets.css">
<style>

</style>
    <!-- BEGIN: Content-->
<div class="app-content content" >
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <app-breadcrumb class="col-12">
                <div class="row">
                    <div class="content-header-left col-md-6 col-12 mb-2 ng-star-inserted">
                        <h3 class="content-header-title mb-0 d-inline-block">My Pets</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="d-inline-block float-md-right">
                            <a href="<?php echo base_url() ;?>User/addNewPet" class="btn editButton text-white btn-sm" style="float:right;padding-top:4px;"><i class="la la-plus"></i> Add Pets
                            </a>
                        </div>
                    </div>
                </div>
            </app-breadcrumb>
        </div>

        <div class="content-body" >
            <!-- Simple User Cards with Shadow -->
            <section id="user-cards-with-square-thumbnail" class="row">
                <?php $i=0 ;?>
                <?php foreach ($pets as $key => $value) { $i++?>	
                     
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="card box-shadow-1">
                            <div class="text-center">
                                <div class="card-body mt-1">
                                    <div class="row">
                                        <div class="col-md-6 md6">
                                        
                                            <?php if(date('m-d') == date('m-d', strtotime($value['birth_date']))){?>
                                               <img class="blink-image" src="<?php echo base_url();?>assets/cake.png" style="width: 30px;height: 30px;float:left; margin-left:5px;margin-top:-5px;" alt="animated-cake-image-0020" />
                                            <?php } else{?>
                                            
                                            <?php }?>
                                        </div>
                                        <div class="col-md-6 md6">
                                            <div class="row" style="float: right; margin-right:5px;">
                                                <a href="<?php echo base_url() ?>User/editPet?id=<?php echo $value['id'];?>" type="button" class="btn btn-icon btn-pure dark " style="padding: 0rem 0rem !important; margin-right:5px;"><i class="la la-edit" style="color: #605ca8;"></i></a>
                                                <a type="button" class="btn btn-icon btn-pure dark delete_petBtn" style="padding: 0rem 0rem !important;" data-toggle="modal" data-target="#deleteModel" data-id="<?php echo $value['id'];?>"><i class="la la-trash" style="color: red;"></i></a>
                                                
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row col-md-12">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="petpic">
                                                <?php if($value['active_appointment']==0){?>
                                                    <div class="pet-circle">
                                                        <div class="pet-wrapper">
                                                            <img class="pet-img" src="<?php echo base_url(); ?>assets/p<?php echo $i; ?>.jpeg" />
                                                        </div>
                                                    </div>
                                                <?php }else{?>
                                                    <div class="pet-circle petring">
                                                        <div class="pet-wrapper">
                                                            <img class="pet-img" src="<?php echo base_url(); ?>assets/p<?php echo $i; ?>.jpeg" />
                                                        </div>
                                                    </div>
                                                <?php } ?> 
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"><?php echo ucfirst($value['name']);?></h4>
                                    <?php if(!empty($value['breed'])){?>
                                        <h6 class="card-subtitle text-muted"><?php echo ucfirst($value['breed']);?></h6>
                                    <?php }else{?>
                                        <h6 class="card-subtitle text-muted">N/A</h6>
                                    <?php } ?>
                                </div>
                                <div class="card-body mb-1">
                                    <a href="<?php echo base_url()?>User/getPetDetails?id=<?php echo $value['id'];?>" type="button" class="btn btn-sm editButton text-white"><i class="ft-user"></i> Profile
                                    </a>
                                    
                                    <a href="javascript:void(0);" type="button" class="btn btn-danger btn-sm" data-toggle="popover" data-placement="top" data-container="body"
                                    data-original-title="Book Now" data-content="Call us now to book your appointment - +1 1231231231"><i class="ft-phone-call"></i> Appointment</a>
                                    
                                </div>
                                <div class="list-group list-group-flush text-left">
                                    <ol class="list-group-item">
                                        <a href="javascript:void(0);" class="black"><?php echo $value['total_appointment'];?> Total Appointments
                                        </a>
                                        <a href="javascript:void(0);" class="left-value badge badge-pill badge-info"><?php echo $value['active_appointment'];?> Active
                                        </a>
                                    </ol>
                                    <ol class="list-group-item">
                                        <a href="javascript:void(0);" class="ng-star-inserted black" >Contract Expires on
                                        </a>
                                        <?php if(!empty($value['appointment_contract'])){?>
                                            <a href="javascript:void(0);" class="left-value"><?php echo $value['appointment_contract'];?>
                                            </a>
                                            <!-- <a href="javascript:void(0);" class="left-value">01/01/2000
                                            </a> -->
                                        <?php }else{?>
                                            <a href="javascript:void(0);" class="left-value">N/A
                                            </a>
                                        <?php } ?>
                                    </ol>
                                    <ol class="list-group-item">
                                        <a href="javascript:void(0);" class="ng-star-inserted black">Total Vaccination Records
                                        </a>
                                        <a href="javascript:void(0);" class="left-value badge badge-pill badge-warning"><?php echo $value['total_vaccination']?>, <?php echo $value['active_vaccination'];?> Active
                                        </a>
                                    </ol>
                                    <ol class="list-group-item">
                                        <a href="javascript:void(0);" class="ng-star-inserted black mr-5">Veterinarian Name
                                        </a>
                                        <a href="javascript:void(0);" class="left-value">N/A
                                        </a>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                                                   
                <?php } ?>
            </section>
            <!-- Simple User Card with Border-->
        </div>
    </div>
</div>                         


<!-- Modal -->
<div class="modal fade text-left" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="para"></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <a type="button" id="btn_delete_pet" class="btn editButton text-white">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- END: Content-->

<script>
    $(document).ready(function() {
    
        $(document).on('click', '.delete_petBtn', function ()
        {   
            var id=$(this).data('id');
            $(".modal-title").html("Delete Pet");
            $(".para").html("Are you sure you want to delete this pet?");
            $("#btn_delete_pet").attr("href",BASE_URL+"User/deletePet?id="+id);
        });
    });

</script>
