<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    function getAppoinmentDetail($invoice_no){
         $boardings =   $this->db->select('status,deposit,app_user_id,payment_status,deposit_transaction_id,amount,category')->from('boardings')->where(array('invoice_no' => $invoice_no,'payment_status' => 'pending'))->get()->result_array();
		 $grooming =   $this->db->select('status,deposit,app_user_id,payment_status,cost As amount')->from('appointments')->where(array('invoice_no' => $invoice_no,'payment_status' => 'pending'))->get()->result_array();
        return array_merge($boardings,$grooming);
	}
	
	function getUserDetailByInvoiceId($invoice_no){
		$boardings =   $this->db->select('app_user_id')->from('boardings')->where(array('invoice_no' => $invoice_no))->get()->row_array();
		$grooming =   $this->db->select('app_user_id')->from('appointments')->where(array('invoice_no' => $invoice_no))->get()->row_array();
		
		if(!empty($boardings)){
			return $this->Payment_model->getUserdetail($boardings['app_user_id']);
		}elseif (!empty($grooming)) {
			return $this->Payment_model->getUserdetail($grooming['app_user_id']);
		}else{
			return array();
		}
	}

	function getPayAmount($invoice_no){
		$result = $this->Payment_model->getAppoinmentDetail($invoice_no);
		$pay_amount = 0;
		if(!empty($result)){
			foreach ($result as $key => $value) {
				if($value['status'] == 'Pending'){
					$pay_amount += $value['deposit'];
				}else{
					$pay_amount += $value['amount'];
				}
			}
		}
		return $pay_amount;
	}


    function getShopDetailByshopId($shop_id){
        $result = $this->db2->select('*')->get_where('shops',array('id'=>trim($shop_id)))->row_array();
            return $result;
    }

        
		
	function getUserdetail($app_user_id){
		return $this->db2->select('*')->from('app_users')->where(array('id' => $app_user_id))->get()->row_array();
	}
        

        // function getPendingPaymentDetail($data){
		// $amount = 0;
		// $app_user_id = '';
		// if(!empty($data)){
		// 	foreach ($data as $key => $value) {
		// 			$appoinmentId = get_decode($value['appoinmentId']);
		// 			$customerId = $value['customerId'];
		// 			$category   = $value['category'];
		// 			$table = ($category == 'Grooming') ? 'appointments':'boardings';
		// 			$get_value = $this->db->select('deposit')->from($table)->where(array('id' => $appoinmentId,'app_user_id' => $customerId))->get()->row_array();
		// 			$amount +=$get_value['deposit'];
		// 			$app_user_id = $customerId;			
		// 	}
		// 	$name = $this->db2->select('username')->from('app_users')->where(array('id' => $app_user_id))->get()->row_array();

		// 	return array('amount' => $amount,'username' => ucfirst($name['username']));

		// }
	// }


	

		
	//invoice function along with merge logic
	function invoice($shop_id,$invoice_no){
		$data = array();
		// $db3 = $this->load->database($shop_database_name['dbname'], TRUE);
		$shop_detail = $this->Payment_model->getShopDetailByshopId($shop_id);
		if(!empty($shop_detail)){
	
			$config = array(
				'dsn'	=> '',
				'hostname' => 'localhost',
				'username' => $shop_detail['dbusername'],
				'password' => $shop_detail['dbpassword'],
				'database' => $shop_detail['dbname'],
				'dbdriver' => 'mysqli',
				'dbprefix' => '',
				// 'pconnect' => FALSE,
				'db_debug' => (ENVIRONMENT !== 'production'),
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8mb4',
				'dbcollat' => 'utf8mb4_unicode_ci',
				'swap_pre' => '',
				'encrypt' => FALSE,
				'compress' => TRUE,
				'stricton' => FALSE,
				'failover' => array(),
				'save_queries' => TRUE
			   );
	
			 $DB1 = $this->load->database($config,TRUE);
		}else{
			return array();
		}
	
		$appointments = $DB1->get_where('appointments',array('invoice_no' => $invoice_no,'isDuplicate' =>'0'))->result_array();
		$boardings = $DB1->get_where('boardings',array('invoice_no' => $invoice_no))->result_array();
		$allappointments = array_merge($appointments,$boardings);
		// $data['services'] = $this->Entry_model->getAllServices();
		$AppUserDetails = $this->Management_model->getSingleUserData($allappointments[0]['app_user_id']);
		$data['manual_discount'] = $allappointments[0]['manual_discount'];
		$data['credited_amount'] = 0.00;
		$data['payable_amount'] = 0.00;
		$data['total_deposit'] = 0.00;
		$data['total_payable'] = 0.00;
		$data['total_paid'] = 0.00;
		$data['user_details'] = $this->Management_model->getSingleUserData($allappointments[0]['app_user_id']);
	
		$arrDiscount = $this->Management_model->getAvailableDiscounts();
		$data['arrDiscount'] = $arrDiscount;
		$arrDiscountActive = $this->Management_model->getActiveDiscount($allappointments[0]['app_user_id']);
		if(count($arrDiscountActive)){
			$app_users_active_discount_id =  $arrDiscountActive[0]['discount_id'];
		}else{
			$percentage_temp = 0;
			$percentage = 0;
			$active_discount_id = 0;
			$app_users_active_discount_id = 0;
			foreach($arrDiscount as $key => $value) {
	
				$percentage = $value['discount_percentage'];    
	
				if($percentage > $percentage_temp) {
				   $percentage_temp = $percentage ; 
				   $active_discount_id = $value['id'] ; 
				 
				}
			}
	
			if($active_discount_id > 0){
				$availablForUser = $DB1->get_where('discount_users',array('app_user_id'=>$allappointments[0]['app_user_id'],'discount_id' => $active_discount_id))->row_array();
				if(count($availablForUser)){
					$app_users_active_discount_id = $active_discount_id;
				}
			}
		}
		
		if(!empty($invoice_no)){	
			foreach ($allappointments as $key1 => $value1) {
				$data['total_deposit'] = $data['total_deposit'] + $value1['deposit'];
				$allappointments[$key1]['appointment_cost'] = 0;
				$string1 = '';
				$allappointments[$key1]['invoice_items'] = unserialize($value1['invoice_items']);
				$pet_size = $this->db2->select('*')->get_where('pets',array('id' => $value1['pet_id']))->row_array();
				
				$allappointments[$key1]['category'] = empty($value1['category']) ? 'appointment' : $allappointments[$key1]['category'];
	
				//dicount dropdown logic start
				$current_discountData = array();
				if($app_users_active_discount_id > 0){
					$current_discountData = $DB1->get_where('discounts',array('id' => $app_users_active_discount_id))->row_array();
				}
				//dicount dropdown logic end
	
				if(empty($value1['category'])){
					$value1 = groomingTimeConverter($value1);
					$size_cost = $pet_size['size'].'_cost';
					$k=0;
					$serviceArray = "[";
					foreach($allappointments[$key1]['invoice_items']['services'] as $s_key =>$row){
						$serviceArray .= $row['id'].",";
						
						if($row['is_Add_On'] == 0){
							
							if(!empty($current_discountData) && $current_discountData['core_service']==1 && !isset($row['discount'])){
								$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2);
							}
						}else{
							if(!empty($current_discountData) && $current_discountData['add_on_service']==1 && !isset($row['discount'])){
								$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2); 
							}   
						}
						$allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'] = $row['cost']-$allappointments[$key1]['invoice_items']['services'][$k]['discount'];
						$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'];
						$k++;
					}
					
					$serviceArray = trim($serviceArray,',')."]";
					$allappointments[$key1]['serviceArray'] = $serviceArray;
				}else{
					$value1 = daycareBoardingTimeConverter($value1);
					if(!isset($allappointments[$key1]['invoice_items']['appointment']['discount'])){
						$allappointments[$key1]['invoice_items']['appointment']['discount'] = number_format(($allappointments[$key1]['invoice_items']['appointment']['amount'] / 100) * $current_discountData['discount_percentage'],2);
					}
	
					
					$allappointments[$key1]['invoice_items']['appointment']['payable_cost'] = $allappointments[$key1]['invoice_items']['appointment']['amount']-$allappointments[$key1]['invoice_items']['appointment']['discount'];
					
					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['payable_cost'];
	
					if((abs($allappointments[$key1]['invoice_items']['appointment']['overstay']) > 0)){
						$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['overstay'];
					}	
				}
				
				//additional cost logic
				$i=0;
				foreach($allappointments[$key1]['invoice_items']['additional_cost'] as  $row){
					$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]; 
					if($row[3]){
						$allappointments[$key1]['invoice_items']['additional_cost'][$i][3] = $row[3]; 
						$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]-$row[3]; 
					}  
					
					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['additional_cost'][$i][4];
					   
					$i++;
				}
				//pr($allappointments[$key1]['invoice_items']['additional_cost']);exit;
				//additional cost logic end
				
				$allappointments[$key1]['appointment_cost'] = $allappointments[$key1]['appointment_cost']- $allappointments[$key1]['deposit'];
				$data['payable_amount'] += $allappointments[$key1]['appointment_cost'];
				$allappointments[$key1]['petname'] = ucfirst($pet_size['name']);
				$allappointments[$key1]['pet_type'] = ucfirst($pet_size['type']);
				$allappointments[$key1]['check_in_time'] = $value1['check_in_time'];
				$allappointments[$key1]['check_out_time'] = $value1['check_out_time'];      
			  }
			  $data['total_payable'] = $data['payable_amount'];
			  $data['total_payable'] = $data['total_payable'] + $data['total_deposit'];
			$data['allappointments'] = $allappointments;
			$data['before_manual_discount'] = 0.00;
			if($allappointments[0]['is_paid'] == 0){
				if($allappointments[0]['manual_discountPer'] > 0){
					$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
				}
	
				if($data['manual_discount'] == 0){
					$data['before_manual_discount'] = $data['total_payable'];
				}
			}else{
				if($allappointments[0]['manual_discountPer'] > 0){
					$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
				}else{
					$data['manual_discount'] = $allappointments[0]['manual_discount'];
				}
			}
			$data['payable_amount'] = $data['payable_amount'] - ($data['manual_discount']);
			$data['total_paid'] = $data['total_payable'] - ($data['manual_discount']);
			
	
			//First pay logic
			$data['first_pay'] = 0;
			$first_payappointment = $this->db2->select('count(id) as count')->get_where('transactions',array('invoice_id' => $invoice_no,'transcation_type' =>'payment','type!=' =>'deposit','shop_id'=>$shop_id))->row_array();
			if($first_payappointment['count'] > 0){
				$data['first_pay'] = 1;
			}
	
			//All transactions without deposit
			$this->db2->where_not_in('type',array('deposit','from_transaction'));
			$data['all_transactions'] = $this->db2->select('*')->get_where('transactions',array('invoice_id' => $invoice_no,'shop_id'=>$shop_id))->result_array();
			$data['transaction_amount'] = 0.00;
			foreach ($data['all_transactions'] as $transaction_index => $transaction) {
				$data['transaction_amount'] = $data['transaction_amount'] + ($transaction['amount']);
				$payment_source = '';
				$payment_source = $transaction['payment_source'];
	
				if($transaction['payment_source']=='Cash'){
					$payment_source = $transaction['payment_source'] .' Payment';
				}
	
				if($transaction['payment_source']=='Credit/Debit'){
					$payment_source = $transaction['payment_source'] .' Card Payment';
				}
				if($transaction['transcation_type']=='payout' && $transaction['payment_source']=='Other'){
					$payment_source = 'Refunded';
				}
				if($transaction['payment_source']=='Credit Redeem'){
					$payment_source = 'Credit Redeemed';
				}
				if($transaction['transcation_type']=='payment' && $transaction['payment_source']=='Other'){
					$payment_source = $transaction['payment_source'] .' Payment';
					if(!empty($transaction['other_source'])){
						$payment_source .= ' - '.$transaction['other_source'];
					}
				}
				
	
				$data['all_transactions'][$transaction_index]['payment_name'] = $payment_source .' ('.date(get_option('date_format'), strtotime($transaction['added_on'])).')';
			}
			$data['payable_amount'] = $data['payable_amount'] - $data['transaction_amount'];
			$data['payable_amount'] = number_format($data['payable_amount'], 2);
			
			//show merge btn logic
			$data['activeappointments'] = 0;
			//active appointment
			$DB1->where_in('status',array('Confirm','Inprocess','Complete','Checkout'));
			$activeappointment = $DB1->select('count(id) as count')->get_where('appointments',array('invoice_no!=' => $invoice_no,'isDuplicate' =>'0','is_paid'=>0,'app_user_id' =>$allappointments[0]['app_user_id']))->result_array();
			
			//active boarding
			$DB1->where_in('status',array('Confirm','Inprocess','Checkout'));
			$activeboarding = $DB1->select('count(id) as count')->get_where('boardings',array('invoice_no!=' => $invoice_no,'app_user_id' =>$allappointments[0]['app_user_id'],'is_paid'=>0))->result_array();
			
			$count = $activeappointment[0]['count'] + $activeboarding[0]['count'];
			
			if($count > 0){
				$data['activeappointments'] = 1;
			}
			$data['app_user_name'] = $AppUserDetails[0]['username'];
			$data['app_user_phone'] = $AppUserDetails[0]['phone'];
			$data['credit_balance'] = $AppUserDetails[0]['credit_balance'];
			unset($data['manual_discount']);
			unset($data['credited_amount']);
			unset($data['total_paid']);
			unset($data['credited_amount']);
		}
		// pr($data);exit;
		return $data;
	}
		










// //invoice function along with merge logic
// function invoice($shop_id,$invoice_no){
// 	$data = array();
// 	// $db3 = $this->load->database($shop_database_name['dbname'], TRUE);
// 	$shop_detail = $this->Payment_model->getShopDetailByshopId($shop_id);
// 	if(!empty($shop_detail)){
// 		if(APPLICATION_ENV == 'development'){
//             $instance_name = "";
//         }
//         else{
//             $instance_name = INSTANCE_NAME;                     
//         }
		

// 		$config = array(
// 			'dsn'	=> '',
// 			'hostname' => 'localhost',
// 			'username' => $shop_detail['dbusername'],
// 			'password' => $shop_detail['dbpassword'],
// 			'database' => $shop_detail['dbname'],
// 			'dbdriver' => 'mysqli',
// 			'dbprefix' => '',
// 			// 'pconnect' => FALSE,
// 			'db_debug' => (ENVIRONMENT !== 'production'),
// 			'cache_on' => FALSE,
// 			'cachedir' => '',
// 			'char_set' => 'utf8mb4',
// 			'dbcollat' => 'utf8mb4_unicode_ci',
// 			'swap_pre' => '',
// 			'encrypt' => FALSE,
// 			'compress' => TRUE,
// 			'stricton' => FALSE,
// 			'failover' => array(),
// 			'save_queries' => TRUE
// 		   );

// 		$DB1 = $this->load->database($config,TRUE);
// 	}
	

// 	$appointments = $this->db->get_where('appointments',array('invoice_no' => $invoice_no,'isDuplicate' =>'0'))->result_array();
// 	$boardings = $this->db->get_where('boardings',array('invoice_no' => $invoice_no))->result_array();
// 	$allappointments = array_merge($appointments,$boardings);
// 	// $data['services'] = $this->Entry_model->getAllServices();
// 	$AppUserDetails = $this->Management_model->getSingleUserData($allappointments[0]['app_user_id']);
// 	$data['manual_discount'] = $allappointments[0]['manual_discount'];
// 	$data['credited_amount'] = 0.00;
// 	$data['payable_amount'] = 0.00;
// 	$data['total_deposit'] = 0.00;
// 	$data['total_payable'] = 0.00;
// 	$data['total_paid'] = 0.00;
// 	$data['user_details'] = $this->Management_model->getSingleUserData($allappointments[0]['app_user_id']);

// 	$arrDiscount = $this->Management_model->getAvailableDiscounts();
// 	$data['arrDiscount'] = $arrDiscount;
// 	$arrDiscountActive = $this->Management_model->getActiveDiscount($allappointments[0]['app_user_id']);
// 	if(count($arrDiscountActive)){
// 		$app_users_active_discount_id =  $arrDiscountActive[0]['discount_id'];
// 	}else{
// 		$percentage_temp = 0;
// 		$percentage = 0;
// 		$active_discount_id = 0;
// 		$app_users_active_discount_id = 0;
// 		foreach($arrDiscount as $key => $value) {

// 			$percentage = $value['discount_percentage'];    

// 			if($percentage > $percentage_temp) {
// 			   $percentage_temp = $percentage ; 
// 			   $active_discount_id = $value['id'] ; 
			 
// 			}
// 		}

// 		if($active_discount_id > 0){
// 			$availablForUser = $this->db->get_where('discount_users',array('app_user_id'=>$allappointments[0]['app_user_id'],'discount_id' => $active_discount_id))->row_array();
// 			if(count($availablForUser)){
// 				$app_users_active_discount_id = $active_discount_id;
// 			}
// 		}
// 	}
	
// 	if(!empty($invoice_no)){	
// 		foreach ($allappointments as $key1 => $value1) {
// 			$data['total_deposit'] = $data['total_deposit'] + $value1['deposit'];
// 			$allappointments[$key1]['appointment_cost'] = 0;
// 			$string1 = '';
// 			$allappointments[$key1]['invoice_items'] = unserialize($value1['invoice_items']);
// 			$pet_size = $this->db2->select('*')->get_where('pets',array('id' => $value1['pet_id']))->row_array();
			
// 			$allappointments[$key1]['category'] = empty($value1['category']) ? 'appointment' : $allappointments[$key1]['category'];

// 			//dicount dropdown logic start
// 			$current_discountData = array();
// 			if($app_users_active_discount_id > 0){
// 				$current_discountData = $this->db->get_where('discounts',array('id' => $app_users_active_discount_id))->row_array();
// 			}
// 			//dicount dropdown logic end

// 			if(empty($value1['category'])){
// 				$value1 = groomingTimeConverter($value1);
// 				$size_cost = $pet_size['size'].'_cost';
// 				$k=0;
// 				$serviceArray = "[";
// 				foreach($allappointments[$key1]['invoice_items']['services'] as $s_key =>$row){
// 					$serviceArray .= $row['id'].",";
					
// 					if($row['is_Add_On'] == 0){
						
// 						if(!empty($current_discountData) && $current_discountData['core_service']==1 && !isset($row['discount'])){
// 							$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2);
// 						}
// 					}else{
// 						if(!empty($current_discountData) && $current_discountData['add_on_service']==1 && !isset($row['discount'])){
// 							$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2); 
// 						}   
// 					}
// 					$allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'] = $row['cost']-$allappointments[$key1]['invoice_items']['services'][$k]['discount'];
// 					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'];
// 					$k++;
// 				}
				
// 				$serviceArray = trim($serviceArray,',')."]";
// 				$allappointments[$key1]['serviceArray'] = $serviceArray;
// 			}else{
// 				$value1 = daycareBoardingTimeConverter($value1);
// 				if(!isset($allappointments[$key1]['invoice_items']['appointment']['discount'])){
// 					$allappointments[$key1]['invoice_items']['appointment']['discount'] = number_format(($allappointments[$key1]['invoice_items']['appointment']['amount'] / 100) * $current_discountData['discount_percentage'],2);
// 				}

				
// 				$allappointments[$key1]['invoice_items']['appointment']['payable_cost'] = $allappointments[$key1]['invoice_items']['appointment']['amount']-$allappointments[$key1]['invoice_items']['appointment']['discount'];
				
// 				$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['payable_cost'];

// 				if((abs($allappointments[$key1]['invoice_items']['appointment']['overstay']) > 0)){
// 					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['overstay'];
// 				}	
// 			}
			
// 			//additional cost logic
// 			$i=0;
// 			foreach($allappointments[$key1]['invoice_items']['additional_cost'] as  $row){
// 				$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]; 
// 				if($row[3]){
// 					$allappointments[$key1]['invoice_items']['additional_cost'][$i][3] = $row[3]; 
// 					$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]-$row[3]; 
// 				}  
				
// 				$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['additional_cost'][$i][4];
				   
// 				$i++;
// 			}
// 			//pr($allappointments[$key1]['invoice_items']['additional_cost']);exit;
// 			//additional cost logic end
			
// 			$allappointments[$key1]['appointment_cost'] = $allappointments[$key1]['appointment_cost']- $allappointments[$key1]['deposit'];
// 			$data['payable_amount'] += $allappointments[$key1]['appointment_cost'];
// 			$allappointments[$key1]['petname'] = ucfirst($pet_size['name']);
// 			$allappointments[$key1]['pet_type'] = ucfirst($pet_size['type']);
// 			$allappointments[$key1]['check_in_time'] = $value1['check_in_time'];
// 			$allappointments[$key1]['check_out_time'] = $value1['check_out_time'];      
// 		  }
// 		  $data['total_payable'] = $data['payable_amount'];
// 		  $data['total_payable'] = $data['total_payable'] + $data['total_deposit'];
// 		$data['allappointments'] = $allappointments;
// 		$data['before_manual_discount'] = 0.00;
// 		if($allappointments[0]['is_paid'] == 0){
// 			if($allappointments[0]['manual_discountPer'] > 0){
// 				$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
// 			}

// 			if($data['manual_discount'] == 0){
// 				$data['before_manual_discount'] = $data['total_payable'];
// 			}
// 		}else{
// 			if($allappointments[0]['manual_discountPer'] > 0){
// 				$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
// 			}else{
// 				$data['manual_discount'] = $allappointments[0]['manual_discount'];
// 			}
// 		}
// 		$data['payable_amount'] = $data['payable_amount'] - ($data['manual_discount']);
// 		$data['total_paid'] = $data['total_payable'] - ($data['manual_discount']);
		

// 		//First pay logic
// 		$data['first_pay'] = 0;
// 		$first_payappointment = $this->db2->select('count(id) as count')->get_where('transactions',array('invoice_id' => $invoice_no,'transcation_type' =>'payment','type!=' =>'deposit','shop_id'=>$shop_id))->row_array();
// 		if($first_payappointment['count'] > 0){
// 			$data['first_pay'] = 1;
// 		}

// 		//All transactions without deposit
// 		$this->db2->where_not_in('type',array('deposit','from_transaction'));
// 		$data['all_transactions'] = $this->db2->select('*')->get_where('transactions',array('invoice_id' => $invoice_no,'shop_id'=>$shop_id))->result_array();
// 		$data['transaction_amount'] = 0.00;
// 		foreach ($data['all_transactions'] as $transaction_index => $transaction) {
// 			$data['transaction_amount'] = $data['transaction_amount'] + ($transaction['amount']);
// 			$payment_source = '';
// 			$payment_source = $transaction['payment_source'];

// 			if($transaction['payment_source']=='Cash'){
// 				$payment_source = $transaction['payment_source'] .' Payment';
// 			}

// 			if($transaction['payment_source']=='Credit/Debit'){
// 				$payment_source = $transaction['payment_source'] .' Card Payment';
// 			}
// 			if($transaction['transcation_type']=='payout' && $transaction['payment_source']=='Other'){
// 				$payment_source = 'Refunded';
// 			}
// 			if($transaction['payment_source']=='Credit Redeem'){
// 				$payment_source = 'Credit Redeemed';
// 			}
// 			if($transaction['transcation_type']=='payment' && $transaction['payment_source']=='Other'){
// 				$payment_source = $transaction['payment_source'] .' Payment';
// 				if(!empty($transaction['other_source'])){
// 					$payment_source .= ' - '.$transaction['other_source'];
// 				}
// 			}
			

// 			$data['all_transactions'][$transaction_index]['payment_name'] = $payment_source .' ('.date(get_option('date_format'), strtotime($transaction['added_on'])).')';
// 		}
// 		$data['payable_amount'] = $data['payable_amount'] - $data['transaction_amount'];
// 		$data['payable_amount'] = number_format($data['payable_amount'], 2);
		
// 		//show merge btn logic
// 		$data['activeappointments'] = 0;
// 		//active appointment
// 		$this->db->where_in('status',array('Confirm','Inprocess','Complete','Checkout'));
// 		$activeappointment = $this->db->select('count(id) as count')->get_where('appointments',array('invoice_no!=' => $invoice_no,'isDuplicate' =>'0','is_paid'=>0,'app_user_id' =>$allappointments[0]['app_user_id']))->result_array();
		
// 		//active boarding
// 		$this->db->where_in('status',array('Confirm','Inprocess','Checkout'));
// 		$activeboarding = $this->db->select('count(id) as count')->get_where('boardings',array('invoice_no!=' => $invoice_no,'app_user_id' =>$allappointments[0]['app_user_id'],'is_paid'=>0))->result_array();
		
// 		$count = $activeappointment[0]['count'] + $activeboarding[0]['count'];
		
// 		if($count > 0){
// 			$data['activeappointments'] = 1;
// 		}
// 		$data['app_user_name'] = $AppUserDetails[0]['username'];
// 		$data['app_user_phone'] = $AppUserDetails[0]['phone'];
// 		$data['credit_balance'] = $AppUserDetails[0]['credit_balance'];
// 		unset($data['manual_discount']);
// 		unset($data['credited_amount']);
// 		unset($data['total_paid']);
// 		unset($data['credited_amount']);
// 	}
// 	// pr($data);exit;
// 	return $data;
// }
		
















}

 ?>