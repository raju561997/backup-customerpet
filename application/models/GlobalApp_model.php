<?php defined('BASEPATH') OR exit('No direct script access allowed');

//require (APPPATH.'/libraries/REST_Controller.php');

class GlobalApp_model extends CI_Model {
	protected $table_name;

	//Global function by app id

	function __construct()
	{
		parent::__construct();
		
        $this->load->library('email', array(
            'mailtype' => 'html',
            'newline' => '\r\n'
        ));

        //loading Email Library

        
	}

	function sendUserAppointmentMail($appuser,$postarray)
    {
    	//Send Appointment email to the user
    	    $to = $appuser['email'];

            $subject = 'WoodlesApp - New Appointment';
            $html = "<p>Hi," . $appuser['username'] . "</p>" .
                    "<p>Your Appointment Schedule Request Has been submitted successfully </p>" .
                    "<p>Appointment Details : </p>" .
                    "<p>Appointment Date : ".date('M d Y',strtotime($postarray['appointment_date']))."</p>".
                    "<p>Appointment Time : ".date('h:i a',strtotime($postarray['appointment_time']))."</p>".
                    "<p>Best Regards,</p><p>" . "WoodlesApp Team" . "</p>";


            $this->sendEmail($to,$subject,$html);
    }

    function sendAdminAppointmemtMail($user,$postarray)
    {
    		//Send Appointment email to the shop admin
            $subject = 'WoodlesApp - New Appointment';
            $html = "<p>Hello, Admin</p>" .
                    "<p>You have received new appointment request </p>" .
                    "<p>Appointment Details : </p>" .
                    "<p>Appointment Date : ".date('M d Y',strtotime($postarray['appointment_date']))."</p>".
                    "<p>Appointment Time : ".date('h:i a',strtotime($postarray['appointment_time']))."</p>".
                    "<p>Username : ".$user['username']."</p>".
                    "<p>Email : ".$user['email']."</p>".
                    "<p>Phone : ".$user['phone']."</p>".
                    "<p>Best Regards,</p><p>" . "WoodlesApp Team" . "</p>";


            $this->sendAdminEmail($subject,$html);
    }


    function sendUserAppointmentCancelMail($appuser,$postarray)
    {
    		//Send Appointment cancel email to the user
    	    $to = $appuser['email'];

            $subject = 'WoodlesApp - Appointment Cancelled';
            $html = "<p>Hi," . $appuser['username'] . "</p>" .
                    "<p>Your Appointment  Has been cancelled successfully </p>" .
                    "<p>Appointment Details : </p>" .
                    "<p>Appointment Date : ".date('M d Y',strtotime($postarray['appointment_date']))."</p>".
                    "<p>Appointment Time : ".date('h:i a',strtotime($postarray['appointment_time']))."</p>".
                    "<p>Best Regards,</p><p>" . "WoodlesApp Team" . "</p>";


            $this->sendEmail($to,$subject,$html);
    }

    function sendAdminAppointmemtCancelMail($user,$postarray)
    {
    		
    		//Send Appointment cancel email to the admin
            $subject = 'WoodlesApp - Appointment Cancelled';
            $html = "<p>Hello, Admin</p>" .
                    "<p>Appointment has been cancelled. </p>" .
                    "<p>Appointment Details : </p>" .
                    "<p>Appointment Date : ".date('M d Y',strtotime($postarray['appointment_date']))."</p>".
                    "<p>Appointment Time : ".date('h:i a',strtotime($postarray['appointment_time']))."</p>".
                    "<p>Username : ".$user['username']."</p>".
                    "<p>Email : ".$user['email']."</p>".
                    "<p>Phone : ".$user['phone']."</p>".
                    "<p>Best Regards,</p><p>" . "WoodlesApp Team" . "</p>";


            $this->sendAdminEmail($subject,$html);
    }


	function sendEmail($to,$subject,$html)
	{

		//Email send function to User

		//only smtp email are allowed
		$result = $this->getBrandingInfo();

		$config = array();
        $config['protocol'] = 'smtp';
		$config['smtp_host'] = $result['smtp_host'];
		$config['smtp_port'] = $result['smtp_port'];
		$config['smtp_timeout'] = '30';
		$config['smtp_user'] = $result['smtp_username'];
		$config['smtp_pass'] = $result['smtp_password'];
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		//$this->config->load('email');


        $sender_email = $result['mail_sender'];
        $sender_name = $result['all_other_mails_from'];
		$this->email->from($sender_email, $sender_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($html);
        return $this->email->send();
	}

	function SendAdminEmail($subject,$html)
	{

		//Email send function to Admin

		$result = $this->getBrandingInfo();

		$config = array();
        $config['protocol'] = 'smtp';
		$config['smtp_host'] = $result['smtp_host'];
		$config['smtp_port'] = $result['smtp_port'];
		$config['smtp_timeout'] = '30';
		$config['smtp_user'] = $result['smtp_username'];
		$config['smtp_pass'] = $result['smtp_password'];
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		//$this->config->load('email');

		$to = $result['email'];

        $sender_email = $result['mail_sender'];
        $sender_name = $result['all_other_mails_from'];
		$this->email->from($sender_email, $sender_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($html);
        return $this->email->send();

	}

	function getBrandingInfo()
	{
		//Get Shop details info

		$return = array();
		$result=$this->db->get_where('options')->result_array();

		if(!empty($result)) {
			foreach ($result as $key => $value) {
				$return[$value['name']] = $value['value'];
			}
		}

		return $return;	
	}

	function getLangLong()
	{
		$this->db->where_in('name',array('lat','long'));
		$result=$this->db->get_where('options')->result_array();

		if(!empty($result)) {
			foreach ($result as $key => $value) {
				$return[$value['name']] = $value['value'];
			}
		}

		return $return;
	}

	function checkToken()
	{
		//Rest Api validation for checking token
		if(APPLICATION_ENV == 'development'){
			$header = getallheaders();
			if(array_key_exists('token', $header)){
				$token  = $header['token'];
			}	
			else
			{
				$header = $_SERVER;

				if(array_key_exists('HTTP_TOKEN', $header))
					$token  = $header['HTTP_TOKEN'];
				else
					return false;
			}
					
		}	
		else
		{
			$header = $_SERVER;

			if(array_key_exists('HTTP_TOKEN', $header))
				$token  = $header['HTTP_TOKEN'];
			else
				return false;
		
		}
		
		$result = $this->db2->get_where('app_users',array('access_token' => trim($token)))->row_array();

		return $result;
		
	}

	function getAllServices()
	{
		//Get All service 
		return $this->db->get_where('services',array('is_deleted' => 0,'is_package' => 0))->result_array();
	}


	function checkShopPhoneNo($shop_no)
	{
		//check Shop Phone No Exist for the system
		//$this->db2 = $this->load->database('main', TRUE);
		$result = $this->db2->get_where('shops',array('phone_number' => $shop_no,'is_deleted'=>0))->row_array();
		if(!empty($result))
			return true;
		else
			return false;
	}

	function getShopdetailByphoneNo($shopPhoneNumber)
	{
		//fetch shop details by id

		//$this->db2 = $this->load->database('main', TRUE);
		return  $this->db2->get_where('shops',array('phone_number' => $shopPhoneNumber))->row_array();
	}


	function getShopIdByShopPhoneNumber($shopPhoneNumber)
	{
		//fetch shop id by shop phone number
		//$this->db2 = $this->load->database('main', TRUE);
	 	$result = $this->db2->get_where('shops',array('phone_number' => $shopPhoneNumber))->row_array();
		if(!empty($result))
		{
			return $result['id'];
		}
	}




	function checkUserBelongingToShop($shopPhoneNumber,$app_user_id)
	{

		$shop_id = $this->getShopIdByShopPhoneNumber($shopPhoneNumber);

		//checking user belonging to shop
		//$this->db2 = $this->load->database('main', TRUE);
		$user_shop = $this->db2->get_where('app_users_shops',array('shop_id'=>$shop_id,'app_user_id'=>$app_user_id))->row_array();
		
		if(!empty($user_shop))
		{	
			$this->db2->update('app_users_shops',array('currently_active'=>0),array('app_user_id'=>$app_user_id));
			return $this->db2->update('app_users_shops',array('currently_active'=>1),array('app_user_id'=>$app_user_id,'shop_id'=>$shop_id));

		}
		else
		{
			$this->db2->update('app_users_shops',array('currently_active'=>0),array('app_user_id'=>$app_user_id));
			return $this->db2->insert('app_users_shops',array('shop_id'=>$shop_id,'app_user_id'=>$app_user_id,'currently_active'=>1));
		}
	}

	function fblogin($fb_id,$email)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		//$shop = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		
		
		$result	= $this->db2->get_where('app_users',array('fb_id'=>$fb_id))->row_array();
		
		if(!empty($result))
		{
			$this->db2->update('app_users',array('fb_id'=>$fb_id,'last_login_by'=>'normal'),array('id'=>$result['id']));
			return $this->db2->get_where('app_users',array('fb_id'=>$fb_id))->row_array();
		}
		else
		{
			//Normal User Signed Up with facebook
			$result = $this->db2->get_where('app_users',array('email'=>$email))->row_array();
			if(!empty($result))
			{
				$this->db2->update('app_users',array('fb_id'=>$fb_id,'last_login_by'=>'normal'),array('id'=>$result['id']));
				return $this->db2->get_where('app_users',array('fb_id'=>$fb_id))->row_array();
			}
			else
			{
				return false;
			}
		}	
	}


	 function forgotPasswordEmailCheck($email)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$result = $this->db2->get_where('app_users',array('email'=>$email))->row_array();
		//pr($result);
		if(!empty($result))
			return $result;
		else
			return false;
	}


	function forgotPassword($updateArray ,$app_users,$password)
	{
		//New random password has been generated and mailed to user
		//$this->db2 = $this->load->database('main', TRUE);


		if($this->db2->update('app_users',$updateArray,array('id'=>$app_users['id'])))
		{	
			$shop_Data = $this->db2->get_where('shops',array('id'=>$app_users['registered_shop']))->row_array();
			$emailContent = array('subject' => 'Your password has been reset ','message' =>'
                Hello '.$app_users['username'].',<br><br>
                
                We have received forgot password request. We have reset your password.
                <br><br>
                
                You can now login using below credentials<br><br>
                
                Email : '.$app_users['email'].' <br>
                Password : '. $password.'<br><br>
               	
               	If you are having any issues, please contact:

               	<br><br>
				'.$shop_Data['shopname'].'<br>
				'.$shop_Data['address'].'<br>
				'.$shop_Data['phone_number'].'<br>
				'.get_option('shop_email').'<br>
				<br><br>
                
            
                Thanks,<br><br>
	            Team @'.$shop_Data['shopname'].'<br><br> 
				
				'
				);
              
                //$emailContent = array('subject' => 'New Password Request','message' => $body);
            return  email($app_users['email'], $emailContent);
		}
		else{
			return false;
		}
	}


	function getCardDetailsById($id)
	{
		//$this->db2 = $this->load->database('main', TRUE);

		return $this->db2->get_where('app_users_cards',array('id'=>$id,'is_deleted'=>0))->row_array();
	}

	function checkShopCanTakeAppointment($shop_no)
	{
		//Function to check whether shop is availble to take appointment

		//check if shop has services
		// if($this->db->get('services')->num_rows() == 0)
		// {
		// 	return false;
		// }

		//No bank Account
		if($this->db->get('bank_accounts')->num_rows() == 0)
		{
			return false;
		}

		//Bank Account Has not been verified to recive offline payments
		$bank_accounts = $this->db->get_where('bank_accounts')->row_array();
		if($bank_accounts['status'] !== 'verified')
		{
			return false;
		}

		return true; //if all condition are true
	}


	function checkShopCanTakeBoarding($shop_no)
	{
		if($this->db->get('bank_accounts')->num_rows() == 0)
		{
			return false;
		}

		//Bank Account Has not been verified to recive offline payments
		$bank_accounts = $this->db->get_where('bank_accounts')->row_array();
		if($bank_accounts['status'] !== 'verified')
		{
			return false;
		}

		// $data = array();
		// $data['boardingSettings'] = $this->Management_model->getBoardingSettings();
  //   	$data['boardingPrice']=$this->Management_model->getBoardingPrice();
  //   	$data['boardingInventory'] = $this->Management_model->getBoardingInventory();


  //   	if(empty($data['boardingInventory']))
  //   		return false;

  //   	if($data['boardingSettings']['normalDayCarePricing'] == 0)
  //   		return false;

  //   	if(empty($data['boardingPrice']))
  //   		return false;


		return true;
	}


	function checkForEndDateBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{
			
		$weekday = date('l',strtotime($end_date));
    		
		$result = $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();

		if($result['is_closed'] == 1)	
			return false;

		return true;
				
	}

	function checkForStartDateBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{
		$weekday = date('l',strtotime($start_date));
    		
		$result = $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();

		if($result['is_closed'] == 1)	
			return false;
		return true;
	}

	function checkForStartDateEndDateBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{
		//Check If End Date is not same or less than start Date
		if(date('Y-m-d',strtotime($end_date)) < date('Y-m-d',strtotime($start_date)))
		{
			return false;
		}
		return true;
	}

	function checkForStartTimeBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{
		$weekday = date('l',strtotime($start_date));
    		
		$result = $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();

		$date1 = DateTime::createFromFormat('h:i a', $start_time);
		$date2 = DateTime::createFromFormat('H:i:s', $result['start_time']);
		$date3 = DateTime::createFromFormat('H:i:s', $result['end_time']);
		$date4 = DateTime::createFromFormat('h:i a', $end_time);
		//pr($date1);pr($date3);pr($date2);exit;
		if ($date1 >= $date2 && $date1 < $date3)
		{
		   return true;
		}
		else
		{
			return false;
		}
	}

	function checkForEndTimeBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{

		if($category == 'boarding')
			$weekday = date('l',strtotime($end_date));
		else
			$weekday = date('l',strtotime($start_date));
    		
		$result = $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();

		$date1 = DateTime::createFromFormat('h:i a', $start_time);
		$date2 = DateTime::createFromFormat('H:i:s', $result['start_time']);
		$date3 = DateTime::createFromFormat('H:i:s', $result['end_time']);
		$date4 = DateTime::createFromFormat('h:i a', $end_time);
		//pr($date1);pr($date3);pr($date2);exit;
		if ($date4 >= $date2 && $date4 <= $date3)
		{
		   return true;
		}
		else
		{
			return false;
		}
	}

	function checkForStartTimeEndTimeBoarding($start_time,$end_time,$start_date,$end_date,$category)
	{
		$date1 = DateTime::createFromFormat('h:i a', $start_time);
		$date4 = DateTime::createFromFormat('h:i a', $end_time);

		if($date4 > $date1 )
			return true;
		else
			return false;
	}

	function login($email,$password,$username)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		//app user login
		if(!empty($email))
			$result	= $this->db2->get_where('app_users',array('email'=>$email))->row_array();

		if(!empty($username))
			$result	= $this->db2->get_where('app_users',array('username'=>$username))->row_array();

		if(!empty($result))
		{

			if(password_verify($password,$result['password']))
			{	

				//Check whether user belong to same shop
				return $result;

			}
			else
				return false;	
		}
		else
			return false;
	}


	function changeShopNumber($appuser,$new_shop_no,$shop_no)
	{
		$new_shop_id = $this->getShopIdByShopPhoneNumber($new_shop_no);
		$shop_id = $this->getShopIdByShopPhoneNumber($shop_no);
		//$this->db2 = $this->load->database('main', TRUE);

		$this->db2->update('app_users',array('registered_shop' => $new_shop_id),array('id'=>$appuser['id']));

		$userShop = $this->db2->get_where('app_users_shops',array('shop_id'=>$new_shop_id,'app_user_id'=>$appuser['id']))->row_array();
		
		if(!empty($userShop))
		{
			$this->db2->update('app_users_shops',array('currently_active'=>0),array('app_user_id'=>$appuser['id']));
			return $this->db2->update('app_users_shops',array('currently_active'=>1),array('app_user_id'=>$appuser['id'],'shop_id'=>$new_shop_id));

		}
		else
		{
			$this->db2->update('app_users_shops',array('currently_active'=>0),array('app_user_id'=>$appuser['id']));
			return $this->db2->insert('app_users_shops',array('shop_id'=>$new_shop_id,'app_user_id'=>$appuser['id'],'currently_active'=>1));
		}	
	}



}	