<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry_model extends CI_Model { 
	function getAllPromos(){
		//get all promo
		$this->db->order_by('id','desc');

		$query = $this->db->get_where('promos');
		return $query->result_array();
	}

	function getAllPackages()
	{
		//get all service
		$this->db->order_by('services.id','desc');
		return $this->db->get_where('services',array('is_deleted'=>0,'is_package' => 1))->result_array();
		
	}
	


	function getAllPets(){
		//get all pets
		$return = array();
		//$this->db2 = $this->load->database('main', TRUE);
		
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('pets.*,app_users.is_banned');
		$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted' => 0));

		$query = $this->db2->get('pets')->result_array();

		if(!empty($query)){
			foreach ($query as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['bread'] = $this->Global_model->getBreedNameById($value['breed_id']); 	
				array_push($return,$temp);
			}
		}	
		return $return;
		//return $query->result();
	}
	protected $petDT_column = array(
	    'pets.id',
	    '',
	    'pets.name',
	    'app_users.username',
	    'pets.type',
	    'breeds.name',
	    '',
	);
	function getAllPetsAjax($searchVal = '', $sortColIndex = 0, $sortBy = 'desc', $limit = 0, $offset = 0){
		//get all pets
		 // echo $sortColIndex.'id: '.$sortBy.'hereeeeeeee';
		$return = array();
		//$this->db2 = $this->load->database('main', TRUE);
		
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('pets.*,app_users.is_banned,app_users.username,breeds.name as breadName');
		//$this->db2->from('pets');
		//$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->join('breeds','pets.breed_id=breeds.id', 'left');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted' => 0));

        if(strlen($searchVal)){
            $searchCondition = "(
                pets.name like '%$searchVal%' or
                app_users.username like '%$searchVal%' or
                breeds.name like '%$searchVal%'
            )";
            $this->db2->where($searchCondition);
        }

        $this->db2->limit($limit, $offset);
        // echo "$sortColIndex<br>";
        // echo $this->petDT_column[$sortColIndex];
        $this->db2->order_by($this->petDT_column[$sortColIndex], $sortBy);
        $query = $this->db2->get('pets')->result_array();
       //echo $this->db2->last_query();exit;
        /*if(!empty($query)){
			foreach ($query as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['bread'] = $this->Global_model->getBreedNameById($value['breed_id']); 	
				array_push($return,$temp);
			}
		}	*/
		return $query;
	}


	function getAllPetsAjaxCount($searchVal = '')
	{
		$return = array();
		//$this->db2 = $this->load->database('main', TRUE);
		
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('count(pets.id) as petCount');
		//$this->db2->from('pets');
		//$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->join('breeds','pets.breed_id=breeds.id', 'left');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted' => 0));

        if(strlen($searchVal)){
            $searchCondition = "(
                pets.name like '%$searchVal%' or
                app_users.username like '%$searchVal%' or
                breeds.name like '%$searchVal%'
            )";
            $this->db2->where($searchCondition);
        }

   
        $query = $this->db2->get('pets')->row_array();
        // echo $this->db2->last_query();
       	
		return $query['petCount'];
	}


	function getoverlappingDetails($appointment,$services)
	{

		$return  = array();
    	$return['confirm'] = array();
    	$return['pending'] = array();
    	// $return['shop_appointment_end_time_exceeded'] = 0;
    	// $return['shop_closed'] = 0;
    	// $return['already_appointment_exist'] = 0;
    	$return['overbookingArray'] = array();
    	// $return['shop_end_time'] = '';
    	// $return['appointment_end_after_midnight'] = 0;

		if(!empty($services));
    			$service = implode(',',$services);

    	$result= $this->getOverBookingAppointments($appointment['appointment_time'],$appointment['appointment_end_time'],$appointment['appointment_date'],$service,'Confirm');
    		//pr($this->db->last_query());exit;

    		//pr($result);echo $this->db->last_query();

    		if(!empty($result))
    		{
    			
    			foreach ($result as $key => $value) {

    				 if($value['overbookedInvConfirm'] > 0)
    				 {
    				 	$temp = array();
    				 	$temp['id']=$value['id'];
    				 	$temp['service_name'] = $value['Service_Name'];
    				 	$temp['overbookedInvConfirm'] = $value['overbookedInvConfirm'];
    				 	$temp['inventory'] = $value['inventory'];
    				 	array_push($return['confirm'], $temp);
    				 }



    			}
    		}

    		//pr($result);

    		//Validate with Pending Appointment
    		

    		$result2= $this->getOverBookingAppointments($appointment['appointment_time'],$appointment['appointment_end_time'],$appointment['appointment_date'],$service,'Pending');
    		//pr($result2);
    		if(!empty($result2))
    		{
    			
    			foreach ($result2 as $key => $value) {

    				 if($value['overbookedInvPending'] > 0)
    				 {
    				 	$temp = array();
    				 	$temp['id']=$value['id'];
    				 	$temp['service_name'] = $value['Service_Name'];
    				 	$temp['overbookedInvPending'] = $value['overbookedInvPending'];
    				 	$temp['inventory'] = $value['inventory'];
    				 	array_push($return['pending'], $temp);
    				 }



    			}
    		}

    		//echo $this->db->last_query();

    		if(!empty($return['confirm']) && empty($return['pending']))
    	  		$return['overbookingArray'] = $return['confirm'];

    		if(empty($return['confirm']) && !empty($return['pending']))
    			$return['overbookingArray'] = $return['pending'];

    		if(!empty($return['confirm']) && !empty($return['pending'])){

	    		$array = merge_by_id($return['confirm'],$return['pending']);

	    		if(!empty($array))
	    		{
	    			$return['overbookingArray'] = $array;
	    		}
	    		//pr($array);exit;
	    	}
	    return $return;			
	}

	function getTimeExtendedDetails($appointment)
	{
		$return = array();
		$return['shop_appointment_end_time_exceeded'] = 0;
		$return['shop_end_time'] = '';


		$date = $appointment['appointment_date'];
		$time = $appointment['appointment_time'];
		$time2 = $appointment['appointment_end_time'];
		$weekday = date('l',strtotime($date));
		
		$result = $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();

		if($result['is_closed'] == 1)
			$return['shop_closed'] = 1;
		
		$date1 = DateTime::createFromFormat('H:i:s', $time2);
		$date2 = DateTime::createFromFormat('H:i:s', $result['start_time']);
		$date3 = DateTime::createFromFormat('H:i:s', $result['end_time']);

		if($date3 < $date1)
		{
			$return['shop_appointment_end_time_exceeded'] = 1;
			$return['shop_end_time'] = date('h:i a',strtotime($result['end_time']));
		}
		//pr($return);exit;
		return $return;

	}


	function getOverBookingAppointments($appointment_time,$appointment_end_time,$appointment_date,$service,$status)
	{
		if($status == 'Confirm'){
		return $this->db->query('SELECT services.id,services.name as Service_Name , IFNULL(COUNT(appointment_services.service_id),0) - services.inventory as overbookedInv'.$status.',services.inventory,COUNT(appointment_services.service_id) as count, GROUP_CONCAT(appointments.id) FROM appointments JOIN appointment_services ON appointments.id = appointment_services.appointment_id JOIN services ON appointment_services.service_id = services.id WHERE (  Time(appointments.appointment_end_time) between "'.$appointment_time.'" and "'.$appointment_end_time.'"  or Time(appointments.appointment_time) between "'.$appointment_time.'" and"'.$appointment_end_time.'") AND DATE(appointments.appointment_date) = "'.$appointment_date.'" And services.id in ('.$service.')   And appointments.status = "'.$status.'" GROUP BY services.id')->result_array();
		}
		else
		{
			return $this->db->query('SELECT services.id,services.name as Service_Name , COUNT(appointment_services.service_id) as overbookedInv'.$status.',services.inventory,COUNT(appointment_services.service_id) as count, GROUP_CONCAT(appointments.id) FROM appointments JOIN appointment_services ON appointments.id = appointment_services.appointment_id JOIN services ON appointment_services.service_id = services.id WHERE (  Time(appointments.appointment_end_time) between "'.$appointment_time.'" and "'.$appointment_end_time.'"  or Time(appointments.appointment_time) between "'.$appointment_time.'" and"'.$appointment_end_time.'") AND DATE(appointments.appointment_date) = "'.$appointment_date.'" And services.id in ('.$service.')   And appointments.status = "'.$status.'" GROUP BY services.id')->result_array();
		}
	}


	function getPetsViewDetails($id)
	{
		
		//$this->db2 = $this->load->database('main', TRUE);
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('pets.*,app_users.username as customerName,app_users.is_banned');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		//$this->db2->join('breeds','pets.breed_id=breeds.id');


		$result=$this->db2->get_where('pets',array('pets.id'=>$id))->row_array();
		// pr($result);exit;
		if(!empty($result))
		{
			$result['bread'] = $this->Appuser_model->getbreedName($result['breed']);
		}

		return $result;
		
	}

	function getAllAppUser(){
		//get all app user 

		$shop_id = $this->session->userdata('shop_id');

		//$this->db2 = $this->load->database('main', TRUE);
		
		$this->db2->select('app_users.*');
		$this->db2->order_by('app_users.id','desc');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id));
		$query = $this->db2->get('app_users');
		return $query->result_array();
	}

	protected $usrDT_column = array(
        'app_users.id',
        '',
        'app_users.username',
        'app_users.email',
        'app_users.phone',
        '',
    );
	function getAllAppUserAjax($searchVal = '', $sortColIndex = 0, $sortBy = 'desc', $limit = 0, $offset = 0){
		//get all app user 

        $shop_id = $this->session->userdata('shop_id');
		$this->db2->select('app_users.*');
		$this->db2->from('app_users');
		// $this->db2->order_by('app_users.id','desc');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id));
        if(strlen($searchVal)){
            $searchCondition = "(
                app_users.username like '%$searchVal%' or
                app_users.email like '%$searchVal%' or
                app_users.phone like '%$searchVal%'
            )";
            $this->db2->where($searchCondition);
        }

        $this->db2->limit($limit, $offset);
        $this->db2->order_by($this->usrDT_column[$sortColIndex], $sortBy);
        $query = $this->db2->get();
        return $query->result_array();
	}

	function  getAllAppUserAjaxCount ($searchVal = '')
	{
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('count(app_users.id) as CountRows');
		$this->db2->from('app_users');
		// $this->db2->order_by('app_users.id','desc');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id));
        if(strlen($searchVal)){
            $searchCondition = "(
                app_users.username like '%$searchVal%' or
                app_users.email like '%$searchVal%' or
                app_users.phone like '%$searchVal%'
            )";
            $this->db2->where($searchCondition);
        }

        // $this->db2->limit($limit, $offset);
        // $this->db2->order_by($this->usrDT_column[$sortColIndex], $sortBy);
        $query = $this->db2->get()->row_array();
        return $query['CountRows'];
	}

	/*function getAllAppointments()
	{
		//get all appointment
		
		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       	$this->db->where_in('appointments.status',array('Pending','Confirm','Inprocess'));
       	$this->db->where(array('appointments.isDuplicate'=> '0'));
        $this->db->order_by('DATE(appointments.appointment_date)');
        $this->db->order_by('appointments.appointment_time');
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments')->result_array();
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                if ($value['isDuplicate'] == '1') {
	        		continue;
	        	}
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                array_push($returnArray,$temp);

            }

             //Global helper function to convert array as object.    

        }
        //pr();exit;
        return  $returnArray;
	}*/
	function getAllAppointments($search,$searchColumn,$start_date,$end_date,$request,$request_groomer)
	{
		
		$returnArray = array();
        $this->db->select('appointments.id');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       
       	if($request == 'upcoming'){
       		$this->db->where_in('appointments.status',array('Pending','Confirm','Inprocess'));
       	}elseif($request == 'completed'){
       		$this->db->where_in('appointments.status',array('Complete','Checkout'));
       	}else{
       		$this->db->where_in('appointments.status',array('Cancel'));
       	}

       	$this->db->where(array('appointments.isDuplicate'=> '0'));
       	if($request_groomer != 0){
       		$this->db->where(array('appointments.user_id'=> $request_groomer));
       	}
       	
       	if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('appointments.appointment_date>='=> $start_date,'appointments.appointment_date<='=> $end_date));
       		}
       	}
       	
       	if(!empty($search))
		{
			
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				if(!empty($result['pet_id'])){

					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}


			if($searchColumn == 3)
			{
				$this->db->like('appointment_date',$search);
			}		
		}

        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments')->num_rows();
        //echo $this->db->last_query;exit;
        
        return  $query;
	}

	protected $appointmentDT_column = array(
	    'appointments.id',
	    '',
	    '',
	    '',
	    '',
	    'appointments.appointment_date',
	    'appointments.appointment_time',
	    '',
	    '',
	);

	

	function getAllAppointmentsFilter($start,$limit,$search,$searchColumn,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date,$request,$request_groomer)
	{

		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');

       	if($request == 'upcoming'){
       		$this->db->where_in('appointments.status',array('Pending','Confirm','Inprocess'));
       	}elseif($request == 'completed'){
       		$this->db->where_in('appointments.status',array('Complete','Checkout'));
       	}else{
       		$this->db->where_in('appointments.status',array('Cancel'));
       	}

       	$this->db->where(array('appointments.isDuplicate'=> '0'));
       	if($request_groomer != 0){
       		$this->db->where(array('appointments.user_id'=> $request_groomer));
       	}
       	if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('appointments.appointment_date>='=> $start_date,'appointments.appointment_date<='=> $end_date));
       		}
       	}
       	
       	if(!empty($search))
		{
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				if(!empty($result['pet_id'])){
					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}

			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}


			if($searchColumn == 3)
			{
				$this->db->like('appointment_date',$search);
			}		
		}

        // $this->db->order_by('DATE(appointments.appointment_date)');
        $this->db->order_by($this->appointmentDT_column[$sortColIndex],$sortBy);
        $this->db->group_by('appointments.id');
        $this->db->limit($limit,$start);
        $query = $this->db->get_where('appointments')->result_array();
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                $temp = array();
                $temp = groomingTimeConverter($value);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                $temp['groomerName'] = $this->Global_model->getGroomerName($value['user_id']);
                array_push($returnArray,$temp);
            }
        }
        return  $returnArray;
	}

	function getAllCompletedAppointmentsFilter($start,$limit,$search,$searchColumn,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date)
	{

		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       	$this->db->where_in('appointments.status',array('Complete','Checkout'));
		$this->db->where(array('appointments.isDuplicate'=> '0'));

		if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('appointments.appointment_date>='=> $start_date,'appointments.appointment_date<='=> $end_date));
       		}
       	}

		if(!empty($search))
		{
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				if(!empty($result['pet_id'])){
					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}

			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();

				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}

			if($searchColumn == 3)
			{
				$this->db->like('appointment_date',$search);
			}		
		}
		$this->db->order_by($this->appointmentDT_column[$sortColIndex], $sortBy);
        $this->db->group_by('appointments.id');
        $this->db->limit($limit,$start);
        $query = $this->db->get_where('appointments')->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                $temp['groomerName'] = $this->Global_model->getGroomerName($value['user_id']);
                array_push($returnArray,$temp);
            }
        }
        return  $returnArray;
	}

	/*function getAllCompletedAppointments()
	{

		
		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       	$this->db->where_in('appointments.status',array('Complete','Checkout'));
       	$this->db->where(array('appointments.isDuplicate'=> '0'));
        $this->db->order_by('DATE(appointments.appointment_date)','DESC');
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments')->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                $temp['groomerName'] = $this->Global_model->getGroomerName($value['user_id']);
                array_push($returnArray,$temp);

            }

             //Global helper function to convert array as object.    

        }
        
        return  $returnArray;
	}

	function getAllCancelledAppointments()
	{
		
		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       	$this->db->where_in('appointments.status',array('Cancel'));
       	$this->db->where(array('appointments.isDuplicate'=> '0'));
        $this->db->order_by('DATE(appointments.appointment_date)','DESC');
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments')->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                $temp['groomerName'] = $this->Global_model->getGroomerName($value['user_id']);
                array_push($returnArray,$temp);

            }

             //Global helper function to convert array as object.    

        }
        
        return  $returnArray;
	}*/

	function getAllCancelledAppointmentsFilter($start,$limit,$search,$searchColumn,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date)
	{

		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
        $this->db->where(array('appointments.isDuplicate'=> '0'));
       	$this->db->where_in('appointments.status',array('Cancel'));
       	if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('appointments.appointment_date>='=> $start_date,'appointments.appointment_date<='=> $end_date));
       		}
       	}
        $this->db->order_by('DATE(appointments.appointment_date)','DESC');
        $this->db->group_by('appointments.id');
        if(!empty($search))
		{
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();

				if(!empty($result['pet_id'])){
					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}

			if($searchColumn == 3)
			{
				$this->db->like('appointment_date',$search);
			}		
		}
		$this->db->order_by($this->appointmentDT_column[$sortColIndex], $sortBy);
        $this->db->group_by('appointments.id');
        $this->db->limit($limit,$start);
        $query = $this->db->get_where('appointments')->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                $temp['groomerName'] = $this->Global_model->getGroomerName($value['user_id']);	
                array_push($returnArray,$temp);
            }
        }
        
        return  $returnArray;
	}

	function getAllBoardingDayCareForCalendar($category)
	{
		$return = array();
		$this->db->order_by('DATE(start_date)');
		$this->db->where_in('status');
		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
			$this->db->where('end_date >=', $_GET['start']);
        	// $this->db->where('start_date <= ', $_GET['end']);
        	// $this->db->where('end_date >=', $_GET['start']);
        	$this->db->where('start_date <= ', $_GET['end']);


		// $this->db->where('start_date BETWEEN '.$_GET['start'].' AND '.$_GET['end']);
		// $this->db->or_where('end_date BETWEEN '.$_GET['start'].' AND '.$_GET['end']);
        	$this->db->where(array('category'=>$category));
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = daycareBoardingTimeConverter($value);
	           	
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;

	}


	function getAllBoardingDayCare($category,$search,$searchColumn,$start_date,$end_date,$request)
	{
		if($request == 'upcoming'){
       		$this->db->where_in('boardings.status',array('Pending','Confirm','Inprocess'));
       	}elseif($request == 'completed'){
       		$this->db->where_in('boardings.status',array('Complete','Checkout'));
       	}else{
       		$this->db->where_in('boardings.status',array('Cancel'));
       	}

		if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('boardings.start_date>='=> $start_date,'boardings.start_date<='=> $end_date));
       		}
       	}
		if(!empty($search))
		{
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				if(!empty($result['pet_id'])){

					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}

			if($searchColumn == 3)
			{
				$this->db->like('start_date',$search);
			}		
		}

		if(!empty($category))
		{
			if($category == 'daycare')
				$this->db->where('category','daycare');
			else
				$this->db->where('category','boarding');
		}
		$result = $this->db->get_where('boardings')->num_rows();
		
		return $result;
	}

	protected $boardingDT_column = array(
	    'boardings.id',
	    '',
	    '',
	    '',
	    'boardings.start_date',
	    'boardings.start_time',
	    '',
	    '',
	    '',
	    '',
	);
	function getAllBoardingDayCarefilter($start,$limit,$search,$searchColumn,$category,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date,$request)
	{

		$return = array();
		if($request == 'upcoming'){
       		$this->db->where_in('boardings.status',array('Pending','Confirm','Inprocess'));
       	}elseif($request == 'completed'){
       		$this->db->where_in('boardings.status',array('Complete','Checkout'));
       	}else{
       		$this->db->where_in('boardings.status',array('Cancel'));
       	}

		if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('boardings.start_date>='=> $start_date,'boardings.start_date<='=> $end_date));
       		}
       	}
		if(!empty($search))
		{
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				if(!empty($result['pet_id'])){

					$result['pet_id'] = rtrim($result['pet_id'],',');
					$this->db->where('pet_id in ('.$result['pet_id'].')');
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}

			if($searchColumn == 3)
			{
				$this->db->like('start_date',$search);
			}		
		}

		if(!empty($category))
		{
			if($category == 'daycare')
				$this->db->where('category','daycare');
			else
				$this->db->where('category','boarding');
		}
		//$this->db->order_by($this->boardingDT_column[$sortColIndex], $sortBy);
		 $this->db->order_by('boardings.start_date',$sortBy);
		$this->db->limit($limit,$start);
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['assigned_inventory_id'],$value['assigned_inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}


	/*function getAllCompleteBoardingDayCare()
	{
		$return = array();
		$this->db->order_by('DATE(start_date)','DESC');
		$this->db->where_in('status',array('Complete','Checkout'));
		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['assigned_inventory_id'],$value['assigned_inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}*/

	/*function getAllCompletedBoardingFilter($start,$limit,$search,$searchColumn,$category,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date)
	{



		$return = array();
		$this->db->order_by('DATE(start_date)','DESC');
		$this->db->where_in('status',array('Complete','Checkout'));
		if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('boardings.start_date>='=> $start_date,'boardings.start_date<='=> $end_date));
       		}
       	}
		if(!empty($category))
		{
			if($category == 'daycare')
				$this->db->where('category','daycare');
			else
				$this->db->where('category','boarding');
		}

		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
		if(!empty($search))
		{
			//$this->db2 = $this->load->database('main', TRUE);
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				// echo $this->db2->last_query();
				// pr($result);

				// echo $result['pet_id'];

				if(!empty($result['pet_id'])){

					// if($result['count'] == 1){
					// 	$this->db->where_in('pet_id',$result['pet_id']);
					// }
					// else
					// {
						$result['pet_id'] = rtrim($result['pet_id'],',');
						$this->db->where('pet_id in ('.$result['pet_id'].')');
					// }
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				// echo $this->db2->last_query();
				// pr($result);

				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}


			if($searchColumn == 3)
			{
				$this->db->like('start_date',$search);
			}		
		}
		$this->db->order_by($this->boardingDT_column[$sortColIndex], $sortBy);
		$this->db->limit($limit,$start);
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['assigned_inventory_id'],$value['assigned_inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}*/

	/*function getAllCancelledBoardingFilter($start,$limit,$search,$searchColumn,$category,$sortColIndex = 0,$sortBy = 'desc',$start_date,$end_date){
		$return = array();
		$this->db->order_by('DATE(start_date)','DESC');
		$this->db->where_in('status',array('Cancel'));
		if(!empty($start_date)&&!empty($end_date)){
       		$start_date = date("Y-m-d", strtotime($start_date));
       		$end_date = date("Y-m-d", strtotime($end_date));
       		if($start_date < $end_date){
       			$this->db->where(array('boardings.start_date>='=> $start_date,'boardings.start_date<='=> $end_date));
       		}
       	}

		if(!empty($category))
		{
			if($category == 'daycare')
				$this->db->where('category','daycare');
			else
				$this->db->where('category','boarding');
		}
		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
		if(!empty($search))
		{
			//$this->db2 = $this->load->database('main', TRUE);
			if($searchColumn == 1){
				$this->db2->select('GROUP_CONCAT(pets.id) as pet_id');
				
				$this->db2->like('name',$search);
				$result = $this->db2->get_where('pets')->row_array();
				// echo $this->db2->last_query();
				// pr($result);

				// echo $result['pet_id'];

				if(!empty($result['pet_id'])){

					// if($result['count'] == 1){
					// 	$this->db->where_in('pet_id',$result['pet_id']);
					// }
					// else
					// {
						$result['pet_id'] = rtrim($result['pet_id'],',');
						$this->db->where('pet_id in ('.$result['pet_id'].')');
					// }
				}
				else
				{
					$this->db->where('pet_id',0);
				}
			}


			if($searchColumn == 2){
				$this->db2->select('GROUP_CONCAT(app_users.id) as app_user_id');
				
				$this->db2->like('username',$search);
				$result = $this->db2->get_where('app_users')->row_array();
				// echo $this->db2->last_query();
				// pr($result);

				if(!empty($result['app_user_id'])){
					$result['app_user_id'] = rtrim($result['app_user_id'],',');
					$this->db->where('app_user_id in ('.$result['app_user_id'].')');
				}
				else
				{
					$this->db->where('app_user_id',0);
				}	
			}


			if($searchColumn == 3)
			{
				$this->db->like('start_date',$search);
			}		
		}
		$this->db->order_by($this->boardingDT_column[$sortColIndex], $sortBy);
		$this->db->limit($limit,$start);
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['assigned_inventory_id'],$value['assigned_inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}*/

	/*function getAllCancelledBoardingDayCare()
	{
		$return = array();
		$this->db->order_by('DATE(start_date)','DESC');
		$this->db->where_in('status',array('Cancel'));
		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['assigned_inventory_id'],$value['assigned_inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}*/

	function getAllServices()
	{
		//get all service
		$this->db->order_by('services.id','desc');
		// return $this->db->get_where('services',array('is_deleted'=>0,'is_package' => 0))->result_array();
		return $this->db->get_where('services',array('is_deleted'=>0,'is_package' => 0))->result_array();
	}

	function getAllServicesForSystemUser()
	{
		$this->db->order_by('services.id','desc');
		return $this->db->get_where('services',array('is_deleted'=>0,'is_package' => 0))->result_array();
		// echo $this->db->last_query();exit;
	} 

	function getSingleAppointmentId($id)
	{
		//get single appointment infos
		$this->db->select('appointments.* ,');
		$this->db->order_by('appointments.id','desc');
		$query = $this->db->get_where('appointments',array('appointments.id'=>$id))->row_array();
		
		if(!empty($query))
		{
			
			$query = groomingTimeConverter($query);

			
			
			$query['petName'] = $this->Global_model->getPetName($query['pet_id']);
			$query['username'] = $this->Global_model->getUsername($query['app_user_id']);
			 $query['spayed'] ='';
			 $query['description'] = '';
            $data = $this->db2->select('spayed,description')->get_where('pets',array('id'=>$query['pet_id']))->row_array();
            if(!empty($data)){
                $query['spayed'] =$data['spayed'];
                $query['description'] = $data['description'];
            }
		}

		return $query;
	}


	function AppointmentWithServerdate($id)
	{
		//get single appointment infos
		$this->db->select('appointments.* ,');
		$this->db->order_by('appointments.id','desc');
		$query = $this->db->get_where('appointments',array('appointments.id'=>$id))->row_array();
		
				// if(!empty($query))
				// {
					
				// 	$query['petName'] = $this->Global_model->getPetName($query['pet_id']);
				// 	$query['username'] = $this->Global_model->getUsername($query['app_user_id']);
				// 	$query['spayed'] ='';
				// 	$query['description'] = '';
				// 	$data = $this->db2->select('spayed,description')->get_where('pets',array('id'=>$query['pet_id']))->row_array();
		  //           if(!empty($data)){
		  //               $query['spayed'] =$data['spayed'];
		  //               $query['description'] = $data['description'];
		  //           }
				// }

		return $query;
	}

	function getSingleboardingId($id)
	{
		//get single boarding infos
		$this->db->select('boardings.* ,');
		$this->db->order_by('boardings.id','desc');
		$query = $this->db->get_where('boardings',array('boardings.id'=>$id))->row_array();
		
		if(!empty($query))
		{
			$query['petName'] = $this->Global_model->getPetName($query['pet_id']);
			$query['username'] = $this->Global_model->getUsername($query['app_user_id']);
			 $query['spayed'] ='';
			 $query['description'] = '';
            $data = $this->db2->select('spayed,description')->get_where('pets',array('id'=>$query['pet_id']))->row_array();
            if(!empty($data)){
                $query['spayed'] =$data['spayed'];
                $query['description'] = $data['description'];
            }
		}

		return $query;
	}

	function getAppointmentServiceById($id)
	{
		//get all appointment service by appointment id
		$return = array();
		$query = $this->db->get_where('appointment_services',array('appointment_services.appointment_id'=>$id))->result_array();;
		if(!empty($query))
		{
			foreach ($query as $key => $value) {
				array_push($return, $value['service_id']);
			}
		}
		
		return $return;
	}

	function getAppointmentAddOnServiceById($id)
	{
		//get all appointment service by appointment id
		$return = array();
		$query = $this->db->get_where('appointment_addon_services',array('appointment_addon_services.appointment_id'=>$id,'service_id !='=>'0'))->result_array();
		
		if(!empty($query))
		{
			foreach ($query as $key => $value) {
				array_push($return, $value['service_id']);
			}
		}
		
		return $return;
	}

	function getpetNotesDetails($id,$shop_id)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$return = array();
		// $shop_id = $this->session->userdata('shop_id');
		$this->db2->order_by('id','DESC');
		$result = $this->db2->get_where('pets_notes',array('pet_id'=>$id,'shop_id'=>$shop_id))->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = $value;
				if($value['added_by'] == 'customer')
				{
					$temp['username'] = $this->Management_model->getUsername($value['added_by_user']);
				}
				else
				{
					$temp['username'] = $this->Management_model->getGroomerName($value['added_by_user']);
				}
				array_push($return,$temp);
			}

			
		}

		return $return;

	}


	function getPetNotesAjax($id)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$shop_id = $this->session->userdata('shop_id');
		//$this->db2->order_by('id','DESC');
		return $this->db2->get_where('pets_notes',array('id'=>$id,'shop_id'=>$shop_id))->row_array();
	}

	function getAppointmentImage($id,$category)
	{
		if($category == 'appointment')
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id,'category'=>$category))->result_array();
		else{
			$this->db->group_start();
			$this->db->where('category','daycare');
			$this->db->or_where('category','boarding');
			$this->db->group_end();
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id))->result_array();

		}

	}

	function getPetImage($id)
	{
		return $this->db->get_where('pet_appointment_images',array('pet_id'=>$id))->result_array();

	}

	function getAppointmentCountByStatus()
	{
		$temp = array();
		$status = array('Confirm','Cancel','Inprocess','Complete','Checkout','Pending');

		foreach ($status as $key ) {
			$count = 0;
			$this->db->where('MONTH(appointment_date)','MONTH(CURRENT_DATE())',FALSE);
			$result = $this->db->select('count(*) as count')->get_where('appointments',array('status'=>$key))->row_array();
			//pr($result);
			$count += $result['count'];
			//echo $this->db->last_query();
			$this->db->group_start();
			$this->db->where('MONTH(start_date)','MONTH(CURRENT_DATE())',FALSE);
			$this->db->or_where('MONTH(end_date)','MONTH(CURRENT_DATE())',FALSE);
			$this->db->group_end();
			$result = $this->db->select('count(*) as count')->get_where('boardings',array('status'=>$key))->row_array();
			//echo $this->db->last_query();
			//pr($result);
			$count += $result['count'];
			$temp[$key] = $count;
			//exit;
		}

		return $temp;
	}

	function getGroomerScheduleForADayForEventsAjax($id,$date)
	{
		$date1 = date('Y-m-d H:i:s', strtotime($date.' 00:00:00'));
		$date2 = date('Y-m-d H:i:s', strtotime($date.' 23:59:59'));
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');

		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
        $this->db->order_by('appointments.id','desc');
        $this->db->group_by('appointments.id');
        $this->db->where(array('STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") >='=>$date1,'STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") <='=>$date2));
        $query = $this->db->get_where('appointments',array('user_id'=>$id))->result_array();
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = $value;
                $temp = groomingTimeConverter($temp);
                $temp['date'] = user_date($temp['date'],'Y-m-d H:i:s');
                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
                array_push($returnArray,$temp);

            }
        }

        return $returnArray;



		//SELECT `appointments`.*, `appointment_services`.`start`, `appointment_services`.`end` FROM `appointment_services` JOIN `appointments` ON `appointments`.`id` = `appointment_services`.`appointment_id` WHERE `appointments`.`status` NOT IN('Cancel') AND `appointment_services`.`service_id` = '1' AND date(appointment_services.start) = '2018-05-09' AND `appointments`.`user_id` = '5'

		// $returnArray = array();
		// $query = $this->db->get_where('services',array('is_deleted'=>0))->result_array();
		
		// if(!empty($query))
		// {	
		// 	foreach ($query as $key => $value) {
		// 		$temp = array();
		// 		$temp = $value;
		// 		$this->db->select('appointments.*,appointment_services.start,appointment_services.end');
		// 		$this->db->join('appointments','appointments.id = appointment_services.appointment_id');
		// 		$this->db->where_not_in('appointments.status',array('Cancel'));
		// 		$r = $this->db->get_where('appointment_services',array('appointment_services.service_id'=>$value['id'],'date(appointment_services.start)'=>$date,'appointments.user_id'=>$id))->result_array();	
				
		// 		echo $this->db->last_query();exit;
		// 		if(!empty($r)){
		// 			foreach ($r as $k => $v) {
								
		// 				$temp['appointments'] = $v['appointments'];	
		// 			}
		// 		}	

		// 	}
		// }	



	}

	function getDayCareScheduleForForEventsAjax($date)
	{
		$date1 = date('Y-m-d H:i:s', strtotime($date.' 00:00:00'));
		$date2 = date('Y-m-d H:i:s', strtotime($date.' 23:59:59'));
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');

		$returnArray = array();
		$this->db->order_by('DATE(start_date)');
		$this->db->where(array('STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") >='=>$date1,'category'=>'daycare','STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") <='=>$date2));
		$result = $this->db->get_where('boardings')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				//$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory']);
				
				array_push($returnArray,$temp);
			}
		}
		return $returnArray;
	}


	function getBoardingForForEventsAjax($date)
	{
		$date1 = date('Y-m-d H:i:s', strtotime($date.' 00:00:00'));
		$date2 = date('Y-m-d H:i:s', strtotime($date.' 23:59:59'));
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');

		$returnArray = array();
		$this->db->order_by('DATE(start_date)');
		$this->db->where(array('STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") <='=>$date2,'category'=>'boarding','STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") >='=>$date1));
		//$this->db->where('category','boarding');

		$result = $this->db->get_where('boardings')->result_array();
		
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp = daycareBoardingTimeConverter($temp);
				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
				//$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory']);
				
				array_push($returnArray,$temp);
			}
		}
		return $returnArray;
	}		

    function getShopdetailByphoneNo($shopPhoneNumber)
    {

        return  $this->db2->get_where('shops',array('phone_number' => $shopPhoneNumber))->row_array();
    }

}