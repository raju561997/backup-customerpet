<?php

require 'vendor/twilio-php-master/Twilio/autoload.php';
use Twilio\Rest\Client;



require 'vendor/autoload.php';
use Google\Cloud\Core\ServiceBuilder;
use google\appengine\api\cloud_storage\CloudStorageTools;
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_model extends CI_Model { 

	function getSingleUserData($id)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		//get single user data app user
		$this->db2->order_by('app_users.id','DESC');
		$query = $this->db2->get_where('app_users',array('id'=>$id));

		return $query->result_array();
	}

	function getUserAllPets($id)
	{
		// $this->db2->order_by('pets.id','DESC');
		// $this->db2->select('pets.*,app_users.username,breeds.name as breadName');
		// $this->db2->join('app_users','pets.app_user_id=app_users.id');
		// $this->db2->join('breeds','pets.breed_id=breeds.id');
		// $this->db2->where(array('pets.app_user_id' => $id,'pets.is_deleted' => 0));
		// $return = $this->db2->get('pets')->result_array();

		//get all user all pets
		$return = array();
		//$this->db2 = $this->load->database('main', TRUE);
		$this->db2->order_by('pets.id','DESC');
		$query = $this->db2->get_where('pets',array('app_user_id'=>$id,'is_deleted'=>0))->result_array();
		if(!empty($query)){
			foreach ($query as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['bread'] = $this->Global_model->getBreedNameById($value['breed_id']); 	
				array_push($return,$temp);
			}
		}	
		return $return;
	}
	function getservicesbytype()
    {
       $this->db->order_by('services.id','DESC');
        $this->db->SELECT('name,type');
        $this->db->FROM ('services');
        $this->db->where('is_deleted',0);
        $query = $this->db->get()->result_array();
        return $query;
    }
	
	function getAllServicesForListing()
	{
		//get all service for listing
		$this->db->order_by('services.id','DESC');
		$query = $this->db->get_where('services',array('is_deleted'=>0,'is_package' => 0));
		return $query->result();
	} 
	function getAllPackageForListing()
	{
		$this->db->order_by('services.id','desc');
		$query =$this->db->get_where('services',array('is_deleted'=>0,'is_package'=>1));
		return $query->result();
	} 
	function addPackage($post)
	{
		//add new service

		if(isset($post['employee_photo']))
        	unset($post['employee_photo']);
        
        $avatarData = $post['avatar_data'];
        unset($post['avatar_data']);
    	
    	if(isset($post['oldimage'])){
			$avatarOldData = $post['oldimage'];
			unset($post['oldimage']);
    	}
    	$services = $post['description'];
		$post['description'] = implode(',',$services);
		// pr($post);exit;
		$id = singleInsert('services',$post);
		singleInsert('temp_services',$post);

		//Add Service to groomer
		$this->db->select('admin_users.*');
		$this->db->join('admin_users_groups','admin_users.id = admin_users_groups.user_id');
		$result = $this->db->get_where('admin_users',array('admin_users_groups.group_id'=>8))->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				//Assign New Service with default value
				$temp = array();
                $temp['service_id'] = $id;
                $temp['commission'] = 0;
                $temp['pet_multiplier'] = 1;
                $temp['user_id'] = $value['id'];
                $temp['updated_on'] = date('Y-m-d H:i:s');
                singleInsert('groomer_services',$temp);

                $groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id']))->result_array();
                $service = $this->db->get_where('services',array('is_deleted'=>0,'id !='=>$id))->row_array();

				
			}
		}

		if(!empty($avatarData))
        {
        	$file = shop_images_for_services_base64($id,$avatarData);
            $pet= array(
                'id'   => $id,
                'image' => $file,
            );
            $this->db->where('id', $id);
            return $this->db->update('services', $pet);

        }

        			

        if(!empty($id))
        	return true;
        else
        	return false;
	}

	function editPackage($post,$id)
	{

		if(isset($post['employee_photo']))
        	unset($post['employee_photo']);
        
        $avatarData = $post['avatar_data'];
        unset($post['avatar_data']);
    	
    	if(isset($post['oldimage'])){
			$avatarOldData = $post['oldimage'];
			unset($post['oldimage']);
    	}

    	if(isset($post['is_add_on_service']))
    		$post['is_add_on_service'] = 1;
    	else
    		$post['is_add_on_service'] = 0;

    	$service  = $this->db->get('services',array('id'=>$id))->row_array();
    	$pricechange = $post['pricechange'];
		if(isset($post['pricechange'])){
    		unset($post['pricechange']);
    	}

    	$query = $this->db->get_where('services',array('id'=>$id))->row_array();
    	$queryid = $query['id'];
    	if(isset($query['id'])){
    		unset($query['id']);
    	}

    	$query['name']= $post['name'];
       	$services= $post['description'];
       	$query['description'] = implode(',',$services);
       	// pr($query);exit;
       	$post['description'] = implode(',',$services);
       	$query['is_add_on_service']= $post['is_add_on_service'];

    	$this->db->update('temp_services',$query,array('id'=>$queryid));
    	 
    	// //edit service
		 if(singleEdit('services',$post,array('id'=>$id)))
		 {
		 		if($pricechange == 1){
		    		
		    		$this->db->select('appointments.id,appointments.pet_id,appointment_services.appointment_id,appointment_services.service_id,appointments.appointment_date');
				    $this->db->from('appointments');
				    $this->db->join('appointment_services', 'appointment_services.appointment_id = appointments.id'); 
				    $this->db->where('appointments.id = appointment_services.appointment_id');
				    $this->db->where('appointments.appointment_date > CURDATE()');
				    $this->db->where('appointment_services.service_id ='.$id);
				    $query = $this->db->get()->result_array();
				  
		    		foreach ($query as $key => $value) {
		    			$serviceids = $this->db->get_where('appointment_services',array('appointment_id'=> $value['id']))->result_array();
		    			
		    			$str = '';
		    			foreach ($serviceids as $serviceid) {
		    				$str .= $serviceid['service_id'].',';   
		                }
		               
		                $finalstr = rtrim($str,',');
						
			   			$this->db2->select('size');
						$this->db2->from('pets');
					 	$this->db2->where('id',$value['pet_id']);
					 	$petsize = $this->db2->get()->row_array();
					 	$size_cost = $petsize['size'].'_cost';
					 	

						$sumofservicecost = $this->db->query("select SUM($size_cost) as newcost from services where id in ($finalstr)")->row_array();
						
						
						$this->db->update('appointments',array('cost'=> $sumofservicecost['newcost']),array('id'=> $value['id']));

						
	    		}
	    		$this->db->update('temp_services',$post,array('id'=>$id));
	    	}else{
	    			$this->db->select('appointments.id,appointments.pet_id,appointment_services.appointment_id,appointment_services.service_id,appointments.appointment_date');
				    $this->db->from('appointments');
				    $this->db->join('appointment_services', 'appointment_services.appointment_id = appointments.id'); 
				    $this->db->where('appointments.id = appointment_services.appointment_id');
				    $this->db->where('appointments.appointment_date > CURDATE()');
				    $this->db->where('appointment_services.service_id ='.$id);
				    $query = $this->db->get()->result_array();
				  
		    		foreach ($query as $key => $value) {
		    			$serviceids = $this->db->get_where('appointment_services',array('appointment_id'=> $value['id']))->result_array();
		    			
		    			$str = '';
		    			foreach ($serviceids as $serviceid) {
		    				$str .= $serviceid['service_id'].',';   
		                }
		               
		                $finalstr = rtrim($str,',');
						
			   			$this->db2->select('size');
						$this->db2->from('pets');
					 	$this->db2->where('id',$value['pet_id']);
					 	$petsize = $this->db2->get()->row_array();
					 	$size_cost = $petsize['size'].'_cost';
					 	

						$sumofservicecost = $this->db->query("select SUM($size_cost) as newcost from temp_services where id in ($finalstr)")->row_array();
						
						$array = array('cost' => $sumofservicecost['newcost'], 
										'temp_service_entry' => 1
										);
						$this->db->update('appointments',$array,array('id'=> $value['id']));
						// echo $this->db->last_query();exit;
						
	    		}

	    	}

		 	
		 		if(!empty($avatarData))
		 		{
		        	$service  = $this->db->get('services',array('id'=>$id))->row_array();
		        	//add Image to service		
		            $path = SHOP_IMAGES;


		        
		            $file = shop_images_for_services_base64($id,$avatarData);
		            $pet= array(
		                'id'   => $id,
		                'image' => $file,
		            );
		          
		            if(!empty($service['image']))
		            {
		      
		           //  	if(file_exists("gs://".BUCKETNAME."/".SHOP_IMAGES."/".$service['image']))
        					// unlink("gs://".BUCKETNAME."/".SHOP_IMAGES."/".$service['image']);


						    $this->cloud_image_delete(SHOP_IMAGES.$service['image']);
			            
		            }

		            $this->db->where('id', $id);
		          

		            return $this->db->update('services', $pet);	
		         }

		    	
		    }
		    else
		    {
		    	return false;
		    }	

		return true;    
	}

	function getSinglepackage($id)
    {
        //get single service by id
        return getSingleData('services',array('id'=>$id,'is_deleted' => 0,'is_package' => 1));
    }
	function vaccinationMessage($id){

		//get all vaccination records
		$this->db2->order_by('pets_notes.expiry_on','ASC');
		$query = $this->db2->get_where('pets_notes',array('pet_id'=>$id))->result_array();

		//echo $this->db2->last_query();exit;
		if(!empty($query)){
			$message = '';
			foreach ($query as $key => $value) {
				if(date("Y-m-d", strtotime($value['expiry_on'])) < date("Y-m-d") || $value['is_expired'] == 1){
					$message .= "<br><b>Vaccination:</b> <span class='blink'>".$value['medication_name']." Expired! (exp: ".date(get_option('date_format'), strtotime($value['expiry_on'])).")</span>";
					//return $message;
				}else{
					$message .= "<br><b>Vaccination:</b> ".$value['medication_name']." Valid (exp: ".date(get_option('date_format'), strtotime($value['expiry_on'])).")";
					// $message .= "<br><b>Vaccination Note:<b> ".$value['medication_name'];
					//return $message;
				}
				
			}
			return $message;
		}else{
			return "<br><b>Vaccination:</b> <span class='blink'>Does Not Exist</span>";
		}
	}


	function getAllUserAppointments($id)
	{
		//get all user  appointment 
		
		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
        $this->db->order_by('appointments.id','desc');
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments',array('app_user_id' => $id))->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = groomingTimeConverter($value);

                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
                array_push($returnArray,$temp);
            }
             //Global helper function to convert array as object.    
        }
        
        return  $returnArray;
	}

	function getSamePetAppointments($pet_id,$user_id){
		$result = $this->db->query("SELECT * FROM `appointments` WHERE `app_user_id` = ".$user_id." AND `pet_id`=".$pet_id."")->result_array();
		// echo $this->db->last_query();
		foreach ($result as $key => $value) {
			$pet = $this->db2->get_where('pets',array('id'=>$value['pet_id'],'app_user_id'=>$value['app_user_id']))->row_array();
			$result[$key]['name']= $pet['name'];
			$result[$key]['category']='grooming';
		}
		
		return $result;
	}

	function getSamePetBoarding($pet_id,$user_id){
		$result = $this->db->query("SELECT * FROM `boardings` WHERE `app_user_id` = ".$user_id." AND `pet_id`=".$pet_id."")->result_array();
		// echo $this->db->last_query();
		foreach ($result as $key => $value) {
			$pet = $this->db2->get_where('pets',array('id'=>$value['pet_id'],'app_user_id'=>$value['app_user_id']))->row_array();
			$result[$key]['name']= $pet['name'];
		}
		
		return $result;
	}

	function lastAppointments($pet_id,$user_id){
		$result = $this->db->order_by('appointment_date', 'DESC')->get_where('appointments',array('pet_id'=>$pet_id,'app_user_id'=>$user_id))->row_array();

		$result1 = $this->db->order_by('start_date', 'DESC')->get_where('boardings',array('pet_id'=>$pet_id,'app_user_id'=>$user_id))->row_array();

		if($result['appointment_date']>$result1['start_date']){
			return $result['appointment_date'];
		}else{
			return $result['start_date'];
		}
	}

	function getActiveAppointmentForSamePet($pet_id, $id=0)
	{
		$this->db->where_in('status',array('Inprocess'));
		$result = $this->db->get_where('boardings',array('pet_id'=>$pet_id,'id !='=>$id))->row_array();
		return $result;
	}

	function getActiveBoardingForSamePet($pet_id)
	{
		$this->db->where_in('status',array('Complete'));
		$this->db->order_by('date(appointments.appointment_date)','desc');
		$result = $this->db->get_where('appointments',array('pet_id'=>$pet_id))->row_array();
		return $result;
	}

	function getAllPetsAppointments($id)
	{
		$returnArray = array();
        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
        $this->db->order_by('date(appointments.appointment_date)','desc');
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments',array('pet_id' => $id))->result_array();
        
        if(!empty($query))
        {
            foreach ($query as $key => $value) {
                
                $temp = array();
                $temp = groomingTimeConverter($value);

                // $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);

                array_push($returnArray,$temp);
            }
            //Global helper function to convert array as object.    

        }
        return  $returnArray;
	}

	
	

	function getAllUserBoardings($id)
	{
		$return = array();
		$this->db->order_by('DATE(start_date)');
		$this->db->where_in('status',array('Pending','Confirm','Inprocess','Cancel','Checkout'));
		$result = $this->db->get_where('boardings',array('app_user_id'=>$id))->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = daycareBoardingTimeConverter($value);

				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}

	function getAllPetsBoardings($id)
	{
		$return = array();
		$this->db->order_by('DATE(start_date) DESC');
		$this->db->where_in('status',array('Pending','Confirm','Inprocess','Checkout'));
		$result = $this->db->get_where('boardings',array('pet_id'=>$id))->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['petName'] = $this->Management_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
				
				$temp['inventoryName'] = $this->Management_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
				
				array_push($return,$temp);
			}
		}

		return $return;
	}

	function getAllSystemUser()
	{
		//get All system users 
		//$woodlesUsers = array(1,6,8);
		//Only WoodlesApp Admin User
		// $this->db->select('admin_users.*,admin_groups.description as userRole,admin_groups.id as admin_groups_id, admin_users_groups.add_new_entry as add_new_entry_id');
		$this->db->select('admin_users.*');
		// $this->db->join('admin_users_groups','admin_users.id = admin_users_groups.user_id');
		// $this->db->join('admin_groups','admin_groups.id=admin_users_groups.group_id');
		//$this->db->where_in('admin_groups.id',$woodlesUsers);
		$this->db->where('admin_users.is_owner !=','1');
		$this->db->where('admin_users.is_deleted !=','1');
		$this->db->group_by('admin_users.id');
		$this->db->order_by('admin_users.id','DESC');
		$query = $this->db->get_where('admin_users');
		return $query->result();
	}

	// function getAllWoodlesUserGroups()
	// {
	// 	//get all users group such as ('admin','shop admin','manager')
	// 	//$woodlesUsers = array(1,6,8);
	// 	//$this->db->where_in('admin_groups.id',$woodlesUsers);
	// 	$query = $this->db->get_where('admin_groups');
	// 	return $query->result();
	// }


	// function getAllWoodlesUserModules()
	// {
	// 	//get all admin modules
	// 	$query = $this->db->get_where('admin_modules');
	// 	return $query->result();
	// }

	// function getUserModulesOnlyId($id)
	// {
	// 	//get user module by id
	// 	$this->db->select('GROUP_CONCAT(admin_users_modules.module_id) as module_id');
	// 	$this->db->group_by('id',$id);
	// 	$query = $this->db->get_where('admin_users_modules',array('id',$id))->row_array();
	// 	return $query['module_id'];
	// }

	function check_update_email($email , $id)
	{
		//check update email

		if($this->db->get_where('admin_users',array('email' => $email,'id !=' => $id))->num_rows() === 0)
			return TRUE;
		else
			return FALSE;			
	}

	function check_update_username($email , $id)
	{
		//check update username
		if($this->db->get_where('admin_users',array('username' => $email,'id !=' => $id))->num_rows() === 0)
			return TRUE;
		else
			return FALSE;			
	}

	function addService($post)
	{
		//add new service

		if(isset($post['employee_photo']))
        	unset($post['employee_photo']);
        
        $avatarData = $post['avatar_data'];
        unset($post['avatar_data']);
    	
    	if(isset($post['oldimage'])){
			$avatarOldData = $post['oldimage'];
			unset($post['oldimage']);
    	}

		$id = singleInsert('services',$post);
		singleInsert('temp_services',$post);

		//Add Service to groomer
		$this->db->select('admin_users.*');
		// $this->db->join('admin_users_groups','admin_users.id = admin_users_groups.user_id');
		$result = $this->db->get_where('admin_users',array('admin_users.groomer'=>1))->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				//Assign New Service with default value
				$temp = array();
                $temp['service_id'] = $id;
                $temp['commission'] = 0;
                $temp['pet_multiplier'] = 1;
                $temp['user_id'] = $value['id'];
                $temp['updated_on'] = date('Y-m-d H:i:s');
                singleInsert('groomer_services',$temp);

                $groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id']))->result_array();
                $service = $this->db->get_where('services',array('is_deleted'=>0,'id !='=>$id))->row_array();

				// if(!empty($groomer_schedule))
				// {
				// 	foreach ($groomer_schedule as $k => $v) {
				// 		//pr($v);
				// 		$this->db->select('count(appointments.id) as count');
				// 		$r = $this->db->get_where('appointments',array('appointment_date'=>$v['date'],'status !='=>'Cancel'))->row_array();
				// 		if($r['count'] == 0){
				// 			$temp = array();
				// 			$temp['date'] = $v['date'];
				// 			$temp['start'] = $v['start'];
				// 			$temp['end'] = $v['end'];
				// 			$temp['service_id'] = $id;
				// 			$temp['user_id'] = $v['user_id'];
				// 			$temp['pet_multiplier'] = 1;
				// 			singleInsert('groomer_bookings',$temp);
				// 		}
				// 		else
				// 		{
				// 			//Appointment Exist
				// 			$t = $this->db->get_where('groomer_bookings',array('user_id'=>$value['id'],'date'=>$v['date'],'service_id'=>$service['id']))->result_array();
				// 			$s = $this->db->get_where('groomer_services',array('user_id'=>$value['id'],'service_id'=>$service['id']))->row_array();
				// 			if(!empty($t))
				// 			{
				// 				foreach ($t as $a => $b) {
				// 					//pr($b);
				// 					if($b['pet_multiplier'] > 0)
				// 					{
				// 						if($s['pet_multiplier'] == $b['pet_multiplier'])
				// 						{
				// 							//Means No booking for that slot
				// 							$temp = array();
				// 							$temp['date'] = $b['date'];
				// 							$temp['start'] = $b['start'];
				// 							$temp['end'] = $b['end'];
				// 							$temp['service_id'] = $id;
				// 							$temp['user_id'] = $b['user_id'];
				// 							$temp['pet_multiplier'] = 1;
											
				// 							singleInsert('groomer_bookings',$temp);
				// 						}
				// 						else
				// 						{
				// 							//Mean pet multiplier for slot
				// 							$temp = array();
				// 							$temp['date'] = $b['date'];
				// 							$temp['start'] = $b['start'];
				// 							$temp['end'] = $b['end'];
				// 							$temp['service_id'] = $id;
				// 							$temp['user_id'] = $b['user_id'];
				// 							$temp['pet_multiplier'] = 0;
											
				// 							singleInsert('groomer_bookings',$temp);
				// 						}
				// 					}
				// 					else
				// 					{
				// 						//No Booking for slot
				// 						$temp = array();
				// 						$temp['date'] = $b['date'];
				// 						$temp['start'] = $b['start'];
				// 						$temp['end'] = $b['end'];
				// 						$temp['service_id'] = $id;
				// 						$temp['user_id'] = $b['user_id'];
				// 						$temp['pet_multiplier'] = 0;
										
				// 						singleInsert('groomer_bookings',$temp);
				// 					}
				// 				}
				// 			}
				// 		}	
				// 	}
					
				// }


			}
		}

		
		



		// if(!empty($_FILES['employee_photo']['name']))
  //       {
  //       	//add Image to service		
  //           $path = SHOP_IMAGES;
  //           //echo SHOP_IMAGES;exit;
            
  //           $file = shop_images_for_services($id);
  //           $pet= array(
  //               'id'   => $id,
  //               'image' => $file,
  //           );
  //           $this->db->where('id', $id);
  //           return $this->db->update('services', $pet);		            
  //       }

		if(!empty($avatarData))
        {
        	$file = shop_images_for_services_base64($id,$avatarData);
            $pet= array(
                'id'   => $id,
                'image' => $file,
            );
            $this->db->where('id', $id);
            return $this->db->update('services', $pet);

        }

        			

        if(!empty($id))
        	return true;
        else
        	return false;
	}

	function getPetuploadedfiles($pet_id)
    {
    	$return = array();
    	$return= $this->db2->order_by('id','DESC')->get_where('file_upload',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->result_array();
    	return $return;
    }

	function editService($post,$id)
	{

		if(isset($post['employee_photo']))
        	unset($post['employee_photo']);
        
        $avatarData = $post['avatar_data'];
        unset($post['avatar_data']);
    	
    	if(isset($post['oldimage'])){
			$avatarOldData = $post['oldimage'];
			unset($post['oldimage']);
    	}

    	if(isset($post['is_add_on_service']))
    		$post['is_add_on_service'] = 1;
    	else
    		$post['is_add_on_service'] = 0;

    	$service  = $this->db->get('services',array('id'=>$id))->row_array();
    	// pr($post);exit;
    	$pricechange = $post['pricechange'];
		if(isset($post['pricechange'])){
    		unset($post['pricechange']);
    	}

    	$query = $this->db->get_where('services',array('id'=>$id))->row_array();
    	$queryid = $query['id'];
    	if(isset($query['id'])){
    		unset($query['id']);
    	}

    	$query['name']= $post['name'];
       	$query['description']= $post['description'];
       	$query['is_add_on_service']= $post['is_add_on_service'];

    	$this->db->update('temp_services',$query,array('id'=>$queryid));
    	// echo $this->db->last_query();
    	// pr($query);exit;
    	// //edit service
    	// pr($pricechange);
		 if(singleEdit('services',$post,array('id'=>$id)))
		 {
		 		if($pricechange == 1){
		 			
		    		$this->db->select('appointments.id,appointments.pet_id,appointment_services.appointment_id,appointment_services.service_id,appointments.appointment_date');
				    $this->db->from('appointments');
				    $this->db->join('appointment_services', 'appointment_services.appointment_id = appointments.id'); 
				    $this->db->where('appointments.id = appointment_services.appointment_id');
				    $this->db->where('appointments.appointment_date >= CURDATE()');
				    $this->db->where('appointment_services.service_id ='.$id);
				    $query = $this->db->get()->result_array();
				  	// pr($query);exit;
		    		foreach ($query as $key => $value) {
		    			$serviceids = $this->db->get_where('appointment_services',array('appointment_id'=> $value['id']))->result_array();
		    			
		    			$str = '';
		    			foreach ($serviceids as $serviceid) {
		    				$str .= $serviceid['service_id'].',';   
		                }
		               
		                $finalstr = rtrim($str,',');
						
			   			$this->db2->select('size');
						$this->db2->from('pets');
					 	$this->db2->where('id',$value['pet_id']);
					 	$petsize = $this->db2->get()->row_array();
					 	$size_cost = $petsize['size'].'_cost';
					 	

						$sumofservicecost = $this->db->query("select SUM($size_cost) as newcost from services where id in ($finalstr)")->row_array();
						
						
						$this->db->update('appointments',array('cost'=> $sumofservicecost['newcost']),array('id'=> $value['id']));

						
	    		}
	    		$this->db->update('temp_services',$post,array('id'=>$id));
				 //echo $this->db->last_query();exit;
				// pr($query);exit;
	    	}else{
	    		// echo "axaxax";exit;
	    			$this->db->select('appointments.id,appointments.pet_id,appointment_services.appointment_id,appointment_services.service_id,appointments.appointment_date');
				    $this->db->from('appointments');
				    $this->db->join('appointment_services', 'appointment_services.appointment_id = appointments.id'); 
				    $this->db->where('appointments.id = appointment_services.appointment_id');
				    $this->db->where('appointments.appointment_date >= CURDATE()');
				    $this->db->where('appointment_services.service_id ='.$id);
				    $query = $this->db->get()->result_array();
				     // echo $this->db->last_query();exit;
				  	// pr($query);exit;
		    		foreach ($query as $key => $value) {
		    			$serviceids = $this->db->get_where('appointment_services',array('appointment_id'=> $value['id']))->result_array();
		    			
		    			$str = '';
		    			foreach ($serviceids as $serviceid) {
		    				$str .= $serviceid['service_id'].',';   
		                }
		               
		                $finalstr = rtrim($str,',');
						
			   			$this->db2->select('size');
						$this->db2->from('pets');
					 	$this->db2->where('id',$value['pet_id']);
					 	$petsize = $this->db2->get()->row_array();
					 	$size_cost = $petsize['size'].'_cost';
					 	

						$sumofservicecost = $this->db->query("select SUM($size_cost) as newcost from temp_services where id in ($finalstr)")->row_array();
						
						$array = array('cost' => $sumofservicecost['newcost'], 
										'temp_service_entry' => 1
										);
						$this->db->update('appointments',$array,array('id'=> $value['id']));
						// echo $this->db->last_query();exit;
						
	    		}

	    	}

		 	
		 		if(!empty($avatarData))
		 		{
		        	$service  = $this->db->get('services',array('id'=>$id))->row_array();
		        	//add Image to service		
		            $path = SHOP_IMAGES;


		        
		            $file = shop_images_for_services_base64($id,$avatarData);
		            $pet= array(
		                'id'   => $id,
		                'image' => $file,
		            );
		          
		            if(!empty($service['image']))
		            {
		      
		           //  	if(file_exists("gs://".BUCKETNAME."/".SHOP_IMAGES."/".$service['image']))
        					// unlink("gs://".BUCKETNAME."/".SHOP_IMAGES."/".$service['image']);

					    $this->cloud_image_delete(SHOP_IMAGES.$service['image']);
					   
		            }

		            $this->db->where('id', $id);
		          

		            return $this->db->update('services', $pet);	
		            


		    	}

		    	
		    }
		    else
		    {
		    	return false;
		    }	

		return true;    
	}

	function UpdateGroomerBookingTable($size,$service_id)
	{
		$this->db->where('date(date) >=',date('Y-m-d'));
		$r = $this->db->get_where('groomer_bookings',array('service_id'=>$service_id,'size'=>$size,'pet_multiplier >'=>0))->result_array();
		//pr($r);
		if(!empty($r))
		{	
			foreach ($r as $key => $value) {
				$this->db->update('groomer_bookings',array('pet_multiplier'=>0),array('id'=>$value['id']));			
			}

		}
		//exit;
		return true;	
	}

	function getSingleService($id)
	{
		//get single service by id
		return getSingleData('services',array('id'=>$id,'is_deleted' => 0));
	}


	function saveShopAvailabilitySettings($post)
	{
		//echo date('H:i:s',strtotime('0:00 Am'));
		//exit;
		// pr($post);
		// echo $post['end_time'][1];
		// pr(date('H:i:s',strtotime($post['end_time'][1])));exit;
		//pr($post['start_time']);
		if(!empty($post['weekday']))
		{
			for ($i=1; $i < 8; $i++) { 
				$array = array();

				
				if($post['is_closed'][$i] == 0 )
				{
					$array['start_time'] = date('H:i:s',strtotime($post['start_time'][$i]));
					$array['end_time'] = date('H:i:s',strtotime($post['end_time'][$i]));
					$array['is_closed'] = 0;
					//pr($array);
					$this->db->update('shop_availability',$array,array('id'=>$i));
				}
				else
				{
					$array['start_time'] = '';
					$array['end_time'] = '';
					$array['is_closed'] = 1;
					$this->db->update('shop_availability',$array,array('id'=>$i));
				}

				
			}
			//exit;
			return true;
		}
		else
		{
			return false;
		}

	}

	function saveBoardingPricings($post,$category)
    {
        //pr($post);exit;


        if($category == 'daycare')
        {
            if(isset($post['closedDayCarePricing']) && !empty($post['closedDayCarePricing']))
            {
                $this->db->update('boardings_settings',array('value'=>$post['closedDayCarePricing']),array('name'=>'closedDayCarePricing'));
            }
            else
            {
                $post['closedDayCarePricing'] = 0;
            }

            $this->db->update('boardings_settings',array('value'=>$post['normalDayCarePricing']),array('name'=>'normalDayCarePricing'));
        
            $this->db->update('boardings_settings',array('value'=>$post['dayCareDescription']),array('name'=>'dayCareDescription'));

            $this->db->update('boardings_settings',array('value'=>$post['dayCareTitle']),array('name'=>'dayCareTitle'));

            if (!empty($post['avatar_data'])){    
                $dayCareImage = uploadDayCareBoardingImages_base64($post['avatar_data']);
                    $this->db->update('boardings_settings',array('value'=>$dayCareImage),array('name'=>'dayCareImage'));
            }
        }
        else
        {
            $this->db->update('boardings_settings',array('value'=>$post['boardingDescription']),array('name'=>'boardingDescription'));

            $this->db->update('boardings_settings',array('value'=>$post['boardingTitle']),array('name'=>'boardingTitle'));


            $this->db->update('boardings_settings',array('value'=>$post['catBoardingPricing']),array('name'=>'catBoardingPricing'));

            $this->db->update('boardings_settings',array('value'=>$post['dogBoardingPricing']),array('name'=>'dogBoardingPricing'));

            $this->db->update('boardings_settings',array('value'=>$post['boarding_start_time']),array('name'=>'boarding_start_time'));

            $this->db->update('boardings_settings',array('value'=>$post['boarding_end_time']),array('name'=>'boarding_end_time'));

            if (!empty($post['avatar_data1'])){
                $boardingImage = uploadDayCareBoardingImages_base64($post['avatar_data1']);
                $this->db->update('boardings_settings',array('value'=>$boardingImage),array('name'=>'boardingImage'));
            }

            

        }

        return true;    
    }



	function checkForBoardingIdExist($id)
	{
		$result = $this->db->get_where('boarding_inventory',array('id'=>$id))->row_array();
		if(!empty($result))
			return true;
		return false;
	}


	// function saveShopBoardingSettings($post)
	// {
	// 	//Save Boarding time /Day Care timings for shop
	// 	if(!empty($post['weekdayBoarding']))
	// 	{
	// 		for ($i=1; $i < 8; $i++) { 
	// 			$array = array();

				
	// 			if($post['is_closed'][$i] == 0 )
	// 			{
	// 				$array['start_time'] = date('H:i:s',strtotime($post['start_time'][$i]));
	// 				$array['end_time'] = date('H:i:s',strtotime($post['end_time'][$i]));
	// 				$array['is_closed'] = 0;
	// 				$this->db->update('shop_boarding_availability',$array,array('id'=>$i));
	// 			}
	// 			else
	// 			{
	// 				$array['start_time'] = '';
	// 				$array['end_time'] = '';
	// 				$array['is_closed'] = 1;
	// 				$this->db->update('shop_boarding_availability',$array,array('id'=>$i));
	// 			}


	// 		}
	// 		return true;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }

	function getBoardingSettings()
	{
		//key Value Pair For Boarding Setting
		$return = array();
		$result = $this->db->get_where('boardings_settings')->result_array();
		if(!empty($result)) {
			foreach ($result as $key => $value) {
				$return[$value['name']] = $value['value'];
			}
		}

		return $return;	
	}

	function getBoardingPrice()
	{
		//Return All Prices
		return $this->db->get_where('boarding_price')->result_array();
	}


	function getshopAvailabilty()
	{
		return $this->db->get_where('shop_availability')->result_array();
	}


	function getshopAvailabiltyForBoarding()
	{
		//timings for boarding / day care 
		return $this->db->get_where('shop_boarding_availability')->result_array();
	}

	function getpreviousTimeForBoarding()
	{
		//Time For displaying in slider
		$result = $this->db->get_where('shop_boarding_availability')->result_array();

		$return = array();
		
		foreach ($result as $key => $value) {	
			$array = array();

			$hours1 = date('H', strtotime($value['start_time']));				
			$minutes1 = date('i', strtotime($value['start_time']));
			$hours2 = date('H', strtotime($value['end_time']));				
			$minutes2 = date('i', strtotime($value['end_time']));

			//echo $hours1;exit;
			
			$array['start'] = $hours1*60 + $minutes1;
			$array['end'] = $hours2*60 + $minutes2;
			array_push($return,$array);

		}

		return json_encode($return);
	}


	function getpreviousTime()
	{
		$result = $this->db->get_where('shop_availability')->result_array();

		$return = array();
		
		foreach ($result as $key => $value) {	
			$array = array();

			$hours1 = date('H', strtotime($value['start_time']));				
			$minutes1 = date('i', strtotime($value['start_time']));
			$hours2 = date('H', strtotime($value['end_time']));				
			$minutes2 = date('i', strtotime($value['end_time']));

			//echo $hours1;exit;
			
			$array['start'] = $hours1*60 + $minutes1;
			$array['end'] = $hours2*60 + $minutes2;
			array_push($return,$array);

		}

		return json_encode($return);
	}



	function calcuateEndTime($start_time,$service)
	{
		//echo "start time".$start_time."<br>";
		$endtime = $start_time;//Inital Setting End time according start time
		foreach ($service as $key ) {
			$time = $this->getServiceTimeByServiceId($key);//Get Service Time in Minutes
			if($time > 0){
				$temp = strtotime($endtime);			
				$endtime = date("H:i", strtotime('+'.$time.' minutes', $temp)); //Adding Service Time to end time
				//echo "service time for $key : $time min<br>";
			}	
		}
		
		//echo $endtime;
		return  date("H:i", strtotime($endtime));
	}


	function getServiceTimeByServiceId($id)
	{
		$this->db->select('services.time_estimate');
		$result = $this->db->get_where('services',array('id'=>$id))->row_array();
		return $result['time_estimate'];
	}


	function getShopWorkingDaysForDatePicker()
	{
		$string = '';//Datepicker require string contain week day no sunday is 0 and saturday 6
		//Getting Index like (012) for datepicker 
		$weekdays = $this->db->get_where('shop_availability',array('is_closed'=>1))->result_array();
		if(!empty($weekdays))
		{
			foreach ($weekdays as $key => $value) {
				if($value['id'] == 7)
					$string .= '0';
				else
					$string .= $value['id'];	
			}
		}

		return json_encode($string);

	}



	function getServiceArray($services)
	{
		$str = '';
		$count = count($services); 
		$i = 1;
		if(!empty($services))
		{
			foreach ($services as $key) {
				$serviceName = $this->db->get_where('services',array('id'=>$key))->row_array();
				if($count == $i)
					$str .= $serviceName['name'];
				else
					$str .= $serviceName['name'].', ';
				
				$i++; 
			}
		}

		return $str;
	}

	function sendMoveBoardingEmailToUser($appointmentDetails)
	{

		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		

		if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
		{
			include_once(APPPATH."libraries/PushNotifications.php");
			$FCMPUSH = new PushNotifications();
			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],$appointmentDetails['category'],$this->session->userdata('shop_id')));

			$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
            $data = array('body'=>' Your Appointment for your pet '.$petDetails['name'].' has been transferred','title'=>'Appointment Transferred','badge'=>($badge+1),'data'=>$dataArray);
            
            $insert = array();
            $insert['app_user_id'] = $AppUserDetails[0]['id'];
            $insert['pet_id'] = $petDetails['id'];
            $insert['title'] = $data['title'];
            $insert['body'] = $data['body'];
            $insert['data'] = serialize($dataArray);
            $insert['added_on'] = date('Y-m-d H:i:s');
            $insert['notification_type'] = 'appointments';
            $insert['shop_id'] = $this->session->userdata('shop_id');
            $data['data']['notification'] =  $insert;
        
            $this->db2->insert('notifications',$insert);
            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
		}
		 
		$timeconvertboarding = daycareBoardingTimeConverter($appointmentDetails);
		$emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' Appointment has been transferred successfully - '.PROJECTNAME.' ','message' =>'
            Hello '.$appointmentDetails['username'].',<br><br>
            
            Your '.$appointmentDetails['category'].' Request for your pet '.$petDetails['name'].' has been transferred as follows:
            <br><br>
            Check In: '.date("M d Y, h:i a", strtotime($timeconvertboarding['start_date'].' '.$timeconvertboarding['start_time'])).'<br>
            Check Out: '.date("M d Y, h:i a", strtotime($timeconvertboarding['end_date'].' '.$timeconvertboarding['end_time'])).'<br>
			                   
            <br>
            If you are having any issues, please contact: 	
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['address'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

           
            Thanks,
            <br><br>

            Team @'.$shop_Data['shopname'].'<br><br>
       
            
            '
            );

		// pr($emailContent);exit;
	    email($appointmentDetails['email'], $emailContent,$shop_Data['id']);
	}

	function sendAppointmentEmailToUser($appointmentDetails, $petDetails)
	{
		
		// pr($appointmentDetails);
		// pr($timeconvert);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		$services=$this->Entry_model->getAppointmentServiceById($appointmentDetails['id']);
		$serviceStr = $this->getServiceArray($services);
	
		//$this->db2 = $this->load->database('main', TRUE);

		if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
		{
			include_once(APPPATH."libraries/PushNotifications.php");
			$FCMPUSH = new PushNotifications();
			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));

			$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
            $data = array('body'=>' Your Appointment for your pet '.$petDetails['name'].' has been confirmed','title'=>'Appointment Confirmed','badge'=>($badge+1),'data'=>$dataArray);
            
            $insert = array();
            $insert['app_user_id'] = $AppUserDetails[0]['id'];
            $insert['pet_id'] = $petDetails['id'];
            $insert['title'] = $data['title'];
            $insert['body'] = $data['body'];
            $insert['data'] = serialize($dataArray);
            $insert['added_on'] = date('Y-m-d H:i:s');
            $insert['notification_type'] = 'appointments';
            $insert['shop_id'] = $this->session->userdata('shop_id');
            $data['data']['notification'] =  $insert;
            $this->db2->insert('notifications',$insert);
            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
		}

		

		$timeconvert = groomingTimeConverter($appointmentDetails);
		if(!empty($appointmentDetails['user_email'])){
			$emailContent = array('subject' => 'Your Appointment has been confirmed successfully - '.PROJECTNAME.' ','message' =>'
	                    Hello '.ucfirst($AppUserDetails[0]['username']).',<br><br>
	                    
	                    The appointment for your pet '.ucfirst($petDetails['name']).' has been confirmed as follows:
	                    <br><br>
						 Check In: '.date("M d Y, h:i a", strtotime($timeconvert['appointment_date'].' '.$timeconvert['appointment_time'])).'<br>
						 Appointment Services: '.$serviceStr.'
						<br><br>
						If you are having any issues, please contact:

						<br><br>
	                    '.$shop_Data['shopname'].'<br>
	                   	'.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br>

						Thanks,
						<br><br>
						Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );
	                  
			// pr($emailContent);exit;

	                    //$emailContent = array('subject' => 'New Password Request','message' => $body);
						//Appointment Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($appointmentDetails['cost']).'<br>
						 //*Service prices may vary and additional fees may be added to your appointment upon completion 

	    	email($appointmentDetails['user_email'], $emailContent,$shop_Data['id']);
	    }

	      // pr($emailContent);exit;
	}

	function sendMoveAppointmentEmailToUser($appointmentDetails, $services)
	{
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$serviceStr = $this->getServiceArray($services);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		//$this->db2 = $this->load->database('main', TRUE);

		if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
		{
			include_once(APPPATH."libraries/PushNotifications.php");
			$FCMPUSH = new PushNotifications();
			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));

			$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
            $data = array('body'=>' Your Appointment for your pet '.$petDetails['name'].' has been transferred','title'=>'Appointment Transferred','badge'=>($badge+1),'data'=>$dataArray);
            
            $insert = array();
            $insert['app_user_id'] = $AppUserDetails[0]['id'];
            $insert['pet_id'] = $petDetails['id'];
            $insert['title'] = $data['title'];
            $insert['body'] = $data['body'];
            $insert['data'] = serialize($dataArray);
            $insert['added_on'] = date('Y-m-d H:i:s');
            $insert['notification_type'] = 'appointments';
            $insert['shop_id'] = $this->session->userdata('shop_id');
            $data['data']['notification'] =  $insert;
            $this->db2->insert('notifications',$insert);
            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
		}

		// $timeconvert = groomingTimeConverter($appointmentDetails);
		// pr($appointmentDetails);
		// pr($timeconvert);
		$emailContent = array('subject' => 'Your Appointment has been transferred successfully - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$AppUserDetails[0]['username'].',<br><br>
	                    
	                    The appointment for your pet '.$petDetails['name'].' has been transferred as follows: 
	                    <br><br>
						
						Check In: '.date("M d Y, h:i a", strtotime($appointmentDetails['appointment_date'].' '.$appointmentDetails['appointment_time'])).'<br>
						Check Out: '.date("M d Y, h:i a", strtotime($appointmentDetails['appointment_date'].' '.$appointmentDetails['appointment_end_time'])).'<br>
												 
						 Appointment Services : '.$serviceStr.'<br>
						 Appointment Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($appointmentDetails['cost']).'<br>
						                   
	                    <br>
	                    If you are having any issues, please contact:

	                    <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

	                   	Thanks,<br><br>
                    	Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );

		// pr($emailContent);exit;
	                  
	                    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    email($appointmentDetails['user_email'], $emailContent,$shop_Data['id']);
	}

	function assignedInventoryDashboard($postArray)
	{
		$appointmentDetails = $this->getBoardingDayCareData($postArray['boarding']);
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);

		include_once(APPPATH."libraries/PushNotifications.php");

		if(!empty($petDetails['pet_cover_photo']))
			$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));
		else
			$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));

		$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
		
		if($appointmentDetails['category'] = 'boarding')
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).' - '.date(get_option('date_format'), strtotime($appointmentDetails['end_date'])).'<br>';
		else
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).'<br>';

		$time = date('h:i a', strtotime($appointmentDetails['start_time'])).' - '.date('h:i a', strtotime($appointmentDetails['end_time'])).'<br><br>';

		if($appointmentDetails['status'] == 'Pending')
		{
			//Send Mail If Appointment is confirmed
			$emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' request has been confirmed successfully - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$appointmentDetails['username'].',<br><br>
	                    
	                    Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been confirmed. 
	                    <br><br>
						 Date : '.$date.'
						 Time : '.$time.'
						
						Total '.$appointmentDetails['category'].' Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($appointmentDetails['amount']).'<br>
						                   
	                    <br>
	                    
	                   
	                    
	                    If you are having any issues, please email us at '.ContactEmail.'
	                    <br><br>
	                    Thanks,<br><br>
	                    '
	                    );
			email($appointmentDetails['email'], $emailContent,$this->session->userdata('shop_id'));

			if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
			{
				
				$FCMPUSH = new PushNotifications();
	            $data = array('body'=>' Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been confirmed','title'=>$appointmentDetails['category'].' Request Confirmed','badge'=>($badge+1),'data'=>$dataArray);
	            
	            $notifications = array();
	            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
	            $notifications['pet_id'] = $petDetails['id'];
	            $notifications['title'] = $data['title'];
	            $notifications['body'] = $data['body'];
	            $notifications['data'] = serialize($dataArray);
	            $notifications['added_on'] = date('Y-m-d H:i:s');
	            $notifications['notification_type'] = 'appointments';
	            $notifications['shop_id'] = $this->session->userdata('shop_id');
	            $notifications['notification_type'] = 'appointments';
	            $data['data']['notification'] =  $notifications;
	            $this->db2->insert('notifications',$notifications);
	            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
			}


		}
		return $this->db->update('boardings',array('status'=>'Confirm','inventory'=>$postArray['id']),array('id'=>$appointmentDetails['id']));
	}


	function changeBoardingStatus($id , $status, $cancelType = 'credit')
	{
		if(empty($cancelType)){
			$cancelType = 'credit';
		}
		$appointmentDetails = $this->getBoardingDayCareData($id);
		//pr($appointmentDetails);
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		$currentDateTime0 = date('Y-m-d H:i:s');
		$currentDateTime = server_date($currentDateTime0,'Y-m-d H:i:s');

		$timeconvertboarding = daycareBoardingTimeConverter($appointmentDetails);

		if($appointmentDetails['category'] == 'boarding')
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).' - '.date(get_option('date_format'), strtotime($appointmentDetails['end_date'])).'<br>';
		else
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).'<br>';

		$tempDate = $appointmentDetails['end_date'];

                    		if($appointmentDetails['category'] == 'daycare')
                        		$tempDate = $appointmentDetails['start_date'];

		$time = date('h:i a', strtotime($appointmentDetails['start_time'])).' - '.date('h:i a', strtotime($appointmentDetails['end_time'])).'<br><br>';

		include_once(APPPATH."libraries/PushNotifications.php");

		$temp_status='';
        if($status=='undo'){
            $temp_status ='undo';
            $status ='Confirm';
        }
		// pr($status);exit;
		//Fetching all appointment details required

		if($this->db->update('boardings',array('status'=>$status),array('id'=>$id)))
		{
			//Trigger mail according to appointment status
			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));

			$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));

			if($temp_status == 'undo' ){
                $status ='Undo';
            }

			switch ($status) {
				case 'Inprocess':

					if($appointmentDetails['category'] == 'daycare')
						$tempDate = $postArray['start_date'];



					$app_time = '';
					if($appointmentDetails['category'] == 'boarding'){
						$app_time =	'Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($currentDateTime0)).'<br>
						Pick-up Time: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}else{
						$app_time =	'Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($currentDateTime0)).'<br>
						Check Out: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}


					$this->db->update('boardings',array('can_be_canceled'=> 1,'check_in_time'=>$currentDateTime),array('id'=>$id));

					$emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' appointment has now been checked in - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$appointmentDetails['username'].',<br><br>
	                    
	                    Your '.ucfirst($appointmentDetails['category']).' Request for your pet '.$petDetails['name'].' has been Checked In:
	                    <br><br>
						 '.$app_time.'<br>
						
						If you are having any issues, please contact: 	

						<br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

	                    Thanks, <br><br>
	                    Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );

					
						if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
						{	
							$bodyContent = ' Your '.$appointmentDetails['category'].' appointment for '.$petDetails['name'].' is now checked in';
							$titleContent = ucfirst($appointmentDetails['category']).' Appointment Checked In';
							if (strtolower($appointmentDetails['category']) == 'boarding') {
								$bodyContent = ' Your '.$appointmentDetails['category'].' reservation for '.$petDetails['name'].' is now checked in';
								$titleContent = ucfirst($appointmentDetails['category']).' Reservation Checked In';
							}
							
							$FCMPUSH = new PushNotifications();
				            $data = array('body'=>$bodyContent ,'title'=>$titleContent,'badge'=>($badge+1),'data'=>$dataArray);
				            
				            $notifications = array();
				            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
				            $notifications['pet_id'] = $petDetails['id'];
				            $notifications['title'] = $data['title'];
				            $notifications['body'] = $data['body'];
				            $notifications['data'] = serialize($dataArray);
				            $notifications['added_on'] = date('Y-m-d H:i:s');
				            $notifications['shop_id'] = $this->session->userdata('shop_id');
				            $notifications['notification_type'] = 'appointments';
				            $data['data']['notification'] =  $notifications;
				            $this->db2->insert('notifications',$notifications);
				            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
						}
	                  	
					break;	


				case 'Undo':
					


					$app_time = '';
					if($appointmentDetails['category'] == 'boarding'){
						$app_time =	'Drop-off Time: '.date("M d Y, h:i a", strtotime($appointmentDetails['start_date'].' '.$appointmentDetails['start_time'])).'<br>
						Pick-up Time: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}else{
						$app_time =	'Check In: '.date("M d Y, h:i a", strtotime($appointmentDetails['start_date'].' '.$appointmentDetails['start_time'])).'<br>
						Check Out: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}



					$this->db->update('boardings',array('can_be_canceled'=> 0),array('id'=>$id));
                    $emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' appointment status has been changed and set to confirmed successfully - '.PROJECTNAME.' ','message' =>'
                        Hello '.$appointmentDetails['username'].',<br><br>
                        
                        The '.ucfirst($appointmentDetails['category']).' Request for your pet '.$petDetails['name'].' has been changed and set to confirmed as follows:

                        <br><br>
                        '.$app_time.'<br>
                     	
                     	If you are having any issues, please contact: 	

                        <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

                        Thanks,
                        <br><br>
                        Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );

             

                        if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
                        {
                            
                            $FCMPUSH = new PushNotifications();
                            $data = array('body'=>' Your '.$appointmentDetails['category'].' appointment status for your pet '.$petDetails['name'].' has been changed and set to confirmed','title'=>$appointmentDetails['category'].' Request Confirmed','badge'=>($badge+1),'data'=>$dataArray);
                            
                            $notifications = array();
                            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
                            $notifications['pet_id'] = $petDetails['id'];
                            $notifications['title'] = $data['title'];
                            $notifications['body'] = $data['body'];
                            $notifications['data'] = serialize($dataArray);
                            $notifications['added_on'] = date('Y-m-d H:i:s');
                            $notifications['notification_type']='appointment';
                            $notifications['shop_id'] = $this->session->userdata('shop_id');
                            $notifications['notification_type'] = 'appointments';
                            $data['data']['notification'] =  $notifications;


                            $notificationData = $this->db2->order_by('id','DESC')->get_where('notifications',array('pet_id'=>$notifications['pet_id'],'app_user_id' => $notifications['app_user_id'],'shop_id' => $notifications['shop_id']),1)->row_array();

                            if(!empty($notificationData)){
                                  $this->db2->delete('notifications',array('pet_id'=>$notifications['pet_id'],'app_user_id' => $notifications['app_user_id'],'shop_id' => $notifications['shop_id'],'id' => $notificationData['id']));
                              }

                            $this->db2->insert('notifications',$notifications);
                            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
                        }
                          
                    break;

				case 'Cancel':

					$payment = $this->db2->get_where('transactions',array('reference_id'=>$appointmentDetails['id'],'category'=>$appointmentDetails['category'],'shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();

					$this->db->update('boardings',array('payment_status'=>'pending','status'=>'Cancel','can_be_canceled'=> 1,'refund_status'=>'pending','inventory'=>0,'updated_on'=>$currentDateTime),array('id'=>$appointmentDetails['id']));


					$emailMsg = '';
					if($appointmentDetails['deposit'] > 0){
						if($cancelType == 'credit'){
							// echo "d";
							$credit_balance = $AppUserDetails[0]['credit_balance'] + $appointmentDetails['deposit'];
							$this->db2->update('app_users',array('credit_balance'=>$credit_balance),array('id'=>$AppUserDetails[0]['id']));

							$newPayment = $payment;
							unset($newPayment['id']);
							$newPayment['payment_source'] = 'Credit Awarded';
							$newPayment['amount'] = $appointmentDetails['deposit'];
							$newPayment['status'] = 'success';
							$newPayment['added_on'] = $currentDateTime;
							$newPayment['invoice_id'] = $appointmentDetails['invoice_no'];
							$this->db2->insert('transactions',$newPayment);
							// echo $this->db2->last_query();exit;
							$emailMsg = 'Amount Credited : $'.number_format($appointmentDetails['deposit'],2).'<br><br>
							The total credit balance on your profile is $'.number_format($credit_balance,2).'<br>'; 
						}else{

							if($appointmentDetails['payment_method'] == 'online'){
								 \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
				                $charge = \Stripe\Charge::create(array(
				                  "amount" => round($appointmentDetails['deposit']*100,2),
				                  "currency" => "usd",
				                  "customer" => $AppUserDetails[0]['customerStripeToken'], // obtained with Stripe.js
				                  "description" => "Charge for Appointment"
				                ));

					            $chargeArray = $charge->__toArray(true);

					            $this->db2->update('transactions',array('status'=>'refunded','payment_source'=>'Other','transcation_type'=>'payout','added_on'=>$currentDateTime,'transaction_id'=>$chargeArray['id']),array('reference_id'=>$id,'category'=>$appointmentDetails['category'],'type'=>'deposit','invoice_id' => $appointmentDetails['invoice_no']));
								// echo $this->db2->last_query();exit;
								$emailMsg = 'Amount Refunded : $'.number_format($appointmentDetails['deposit'],2).'<br>';
							}else{
								$this->db2->update('transactions',array('status'=>'refunded','payment_source'=>'Other','transcation_type'=>'payout','added_on'=>$currentDateTime),array('reference_id'=>$id,'category'=>$appointmentDetails['category'],'type'=>'deposit','invoice_id' => $appointmentDetails['invoice_no']));
								// echo $this->db2->last_query();exit;
								$emailMsg = 'Amount Refunded : $'.number_format($appointmentDetails['deposit'],2).'<br>';
							}
						}
					}

    				if($appointmentDetails['overlapping_appointment'] == 0 && $appointmentDetails['category'] == 'daycare')
			    	{	

				    		
						$location = $this->db->get_where('daycare_schedule',array('playarea_id'=>$appointmentDetails['inventory']))->row_array();
						if(!empty($location))
						{
							//$insertArray['inventory'] = $location['id'];
							$this->db->update('daycare_schedule',array('current_capacity'=>($location['current_capacity']+1)),array('id'=>$location['id']));
						}
								
			    	}


			    	if($appointmentDetails['overlapping_appointment'] == 0 && $appointmentDetails['category'] == 'boarding')
			    	{
			    		$this->db->where(array('start <='=>$appointmentDetails['end_date'],'end >='=>$appointmentDetails['start_date']));
			    		$location = $this->db->get_where('boarding_schedule',array('inventory_id'=>$appointmentDetails['inventory']))->row_array();
			    		//pr($location);exit;
						if(!empty($location))
						{
							//$insertArray['inventory'] = $location['id'];
							$this->db->update('boarding_schedule',array('is_booked'=>0),array('start <'=>$appointmentDetails['end_date'],'end >'=>$appointmentDetails['start_date'],'inventory_id'=>$appointmentDetails['inventory']));
						}
			    	}

					$app_time = '';
					if($appointmentDetails['category'] == 'boarding'){
						$app_time =	'Drop-off Time: '.date("M d Y, h:i a", strtotime($appointmentDetails['start_date'].' '.$appointmentDetails['start_time'])).'<br>
						Pick-up Time: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}else{
						$app_time =	'Check In: '.date("M d Y, h:i a", strtotime($appointmentDetails['start_date'].' '.$appointmentDetails['start_time'])).'<br>
						Check Out: '.date("M d Y, h:i a", strtotime($tempDate.' '.$appointmentDetails['end_time'])).'<br>';
					}



    				$emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' request has been Cancelled - '.PROJECTNAME.' ','message' =>'
                    Hello '.$appointmentDetails['username'].',<br><br>
                    
                    Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been cancelled. 
                    <br><br>
					'.$app_time.'
					'.$emailMsg.'<br>
					If you are having any issues, please contact:
					
                   	<br><br>
                    '.$shop_Data['shopname'].'<br>
                    '.$shop_Data['address'].'<br>
                    '.$shop_Data['phone_number'].'<br>
                    '.get_option('shop_email').'<br>
                    <br><br> 

                    Thanks,<br><br>
	               	
	               	Team @'.$shop_Data['shopname'].'<br><br>
	                    
                    '
                    );



                    if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
					{
						
						$FCMPUSH = new PushNotifications();
			            $data = array('body'=>' Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been cancelled ','title'=>ucfirst($appointmentDetails['category']).' Request Cancelled','badge'=>($badge+1),'data'=>$dataArray);
			            
			            $notifications = array();
			            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
			            $notifications['pet_id'] = $petDetails['id'];
			            $notifications['title']  = $data['title'];
			            $notifications['body']   = $data['body'];
			            $notifications['data']   = serialize($dataArray);
			            $notifications['added_on'] = date('Y-m-d H:i:s');
			            $notifications['shop_id']  = $this->session->userdata('shop_id');
			            $notifications['notification_type'] = 'appointments';
			            $data['data']['notification'] =  $notifications;
			            $this->db2->insert('notifications',$notifications);
			            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
					}
					break;
				default:
					
					break;
			}

			return email($AppUserDetails[0]['email'], $emailContent,$shop_Data['id']);
		}
		else
		{
			return false;
		}
	}


	function send_sms($cust_no,$sms_content,$image)
    {

            // Your Account SID and Auth Token from twilio.com/console
        $account_sid = 'ACcacc75fb70379b4983406113ff63a21f';
        $auth_token = '99fc0b0227e88e411c9ccab7c680395e';
        // In production, these should be environment variables. E.g.:
        // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

        // A Twilio number you own with SMS capabilities
        // $twilio_number = "+15005550006";
        
        try 
        {
           
	        $cust_no = "+1".$cust_no ;
	        // $cust_no = "+91".$cust_no ;
	        $twilio = new Client($account_sid, $auth_token);
	        if(!empty($image)){
		        $message = $twilio->messages
		                  ->create($cust_no, // to
		                           array(
		                               "body" => $sms_content,
		                               "from" => "+12535255225",
		                               "mediaUrl" => $image
		                           )
		                  );

	      	}else{
	        	$message = $twilio->messages
		                  ->create($cust_no, // to
		                           array(
		                               "body" => $sms_content,
		                               "from" => "+12535255225"
		                               
		                           )
		                  );
	        }

	        $shopdetails = $this->db2->get_where('shops',array('id' => $this->session->userdata('shop_id')))->row_array();

	    	$sms_count = $shopdetails['sms_count']+1;

	        $this->db2->update('shops',array('sms_count'=>$sms_count),array('id'=>$this->session->userdata('shop_id')));


        }
        catch (Exception $e)
        {
            
            
        }

    }

   
	function completeAppointment($post)
	{

		// pr($post);exit;
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));

		if (isset($post['additional_service'])) {
			$post['additional_service'] = explode(",", $post['additional_service']);
		}
		$cloud = new ServiceBuilder([
             		'keyFilePath' => FCPATH.ClientJsonFileName
        		]);

    	$storage = $cloud->storage();
		
		// include_once(APPPATH."libraries/PushNotifications.php");
		if($post['category'] == 'appointment')
		{

			
			include_once(APPPATH."libraries/PushNotifications.php");
			
			$id = $post['id'];
			$appointmentDetails = $this->Entry_model->getSingleAppointmentId($id);
			$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
			$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
			$AppUserDetails[0]['additional_Phone'] = unserialize($AppUserDetails[0]['additional_Phone']);
			// pr($AppUserDetails);exit;
			$payment = $this->db2->get_where('transactions',array('transcation_type'=>'payment','shop_id'=>$this->session->userdata('shop_id'),'reference_id'=>$id,'category'=>'appointment'))->row_array();

			if (empty($AppUserDetails[0]['customerStripeToken']) && $appointmentDetails['payment_method']== 'online') {
				return false;
			}
			$services = $this->Entry_model->getAppointmentServiceById($id);
			$serviceStr = $this->getServiceArray($services);

			$insert = $post;
			unset($insert['id']);
			unset($insert['category']);
			
			//timeconversion
			$timeconvert = groomingTimeConverter($appointmentDetails);
			$file = $_FILES;

			if(isset($post['employee_photo']))
			{
				unset($insert['employee_photo']);
			}

			if(isset($file) && !empty($file))
			{
				
				$tempFile = $file['file']['tmp_name'];

                $name = $file['file']['name'];

                $fileExt = pathinfo( $name , PATHINFO_EXTENSION);
               
    			$t = generateRandomString(8);
        		$fdata['file_name'] = $t.'.png';

    			$objectName = PETS_IMAGES.$fdata['file_name'];

	           
				$file_name = PETS_IMAGES.$fdata['file_name'];
			

				 $options = [
			          // 'resumable' => true,
			          'name' => $file_name,
			          'metadata' => [
			              'contentLanguage' => 'en',
			              'contentType' => 'image/png',
			          ],
			          'predefinedAcl' => 'publicRead'
			      ];
		     
			      $object = $storage->bucket(BUCKETNAME)->upload(
			          fopen($file['file']['tmp_name'], 'r'),
			          $options
			      );

					$imagedata = file_get_contents($file['file']['tmp_name']);
					$base64 = base64_encode($imagedata);
					$fileArray = array('nothing',$base64);
					//create a new image
				    $newImg = imagecreatefromstring(base64_decode($fileArray[1]));

				    ob_start();
				    imagepng($newImg);
				    $imagedata = ob_get_contents();
				    ob_end_clean();

				    
				    $metadata = ['contentType' => 'image/png'];
				    $storage->bucket(BUCKETNAME)->upload($imagedata, [
				        'name' => WatermarkImages.$fdata['file_name'],
				        'metadata' => $metadata,
				        'predefinedAcl' => 'publicRead'
				    ]);
				
				$insertArray = array();
	            $insertArray['image'] =  $fdata['file_name'];
	            $insertArray['reference_id'] = $post['id'];
	            $insertArray['category'] = 'appointment';
	            $insertArray['pet_id'] = $appointmentDetails['pet_id'];
	            $insertArray['added_on'] = date('Y-m-d H:i:s');
	            $insertArray['is_primary'] = 1;
    			$this->db->insert('pet_appointment_images',$insertArray);
    			$this->db2->update('pets',array('pet_cover_photo'=>$insertArray['image']),array('id'=>$appointmentDetails['pet_id']));
    		
			}

			if(isset($insert['avatar_data']))
				unset($insert['avatar_data']);

			
			if(isset($insert['additional_service']) && !empty($insert['additional_service']))
			{
				unset($insert['additional_service']);

				foreach ($post['additional_service'] as $key) {

					$this->db->insert('appointment_addon_services',array('appointment_id'=>$post['id'],'service_id'=>$key));
				}
			}

			if($petDetails['size'] != $insert['upgrade_size'])
			{
				$this->db2->update('pets',array('size'=>$insert['upgrade_size']),array('id'=>$appointmentDetails['pet_id']));
				$insert['is_upgraded'] = 1;
				$petDetails['size'] = $insert['upgrade_size'];
			}

			$additional_service = explode(',', $_POST['additional_service']);
            $appointmentService = array_merge($services,$additional_service);
            
            $commission = 0;
           	foreach ($appointmentService as $key ) {
               $service = $this->db->get_where('services',array('id'=>$key))->row_array();
                $groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$appointmentDetails['user_id'],'service_id'=>$key))->row_array();

                if(!empty($groomer_services))
                	$commission += ($service[$petDetails['size'].'_cost'] * ($groomer_services['commission']/100));  
           	}

           if($commission > 0)
            {
                $this->db->update('appointments',array('commission'=>$commission),array('id'=>$appointmentDetails['id']));
            }
			$cost = floatval($post['upgrade_cost']) + floatval($post['add_on_service_cost']) + floatval($post['additional_cost']);

			$app_status = 'Complete';
		    
			
			if($appointmentDetails['payment_method'] == 'offline')
			{
				//Update appointment cost
				$insert['cost'] = round(floatval($appointmentDetails['cost'] + $cost),2);
				$insert['can_be_canceled'] = 1;
				$this->db->update('appointments',$insert,array('id'=>$post['id']));

				//Update transactions amount
				$payment['amount'] = round(floatval($payment['amount'] + $cost),2);
				$this->db2->update('transactions',array('status'=>'pending','amount'=>$payment['amount']),array('id'=>$payment['id']));

				$pushMessage = ' The amount due upon arrival is '.get_option('currency_symbol').' '.$payment['amount'];

				$fees = 0.00;
				$remainingCost = $payment['amount'];

			}else{
				$payment['amount'] = $payment['amount'] + $cost;
				$amountwith_stripe_charge = round(floatval(($payment['amount'] + $payment['commission']  + 0.30) / (1 - 0.029)),2);

				$stripe_charge = $amountwith_stripe_charge - ($payment['amount'] + $payment['commission']);
				$fees = $stripe_charge;
				$stripe_charge_diff = $stripe_charge - $payment['stripe_charge'];
				$insert['cost'] = floatval($appointmentDetails['cost'] + $stripe_charge_diff);

				$this->db->update('appointments',$insert,array('id'=>$post['id']));
					
				$this->db2->update('transactions',array('status'=>'pending','amount'=>$payment['amount'],'stripe_charge'=>$stripe_charge),array('id'=>$payment['id']));

				$appointmentDetails = $this->Entry_model->getSingleAppointmentId($id);
	            $payment = $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id')))->row_array();

				$Amount = floatval($appointmentDetails['cost'] + $appointmentDetails['upgrade_cost'] + $appointmentDetails['add_on_service_cost'] + $appointmentDetails['additional_cost']);
				$finalAmount = round(floatval($Amount),2);
				$remainingCost = $finalAmount;
				try 
	            {
	            	$chargeArray = array();
	            	
	                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	                $charge = \Stripe\Charge::create(array(
	                  "amount" => round($finalAmount*100,2),
	                  "currency" => "usd",
	                  "customer" => $AppUserDetails[0]['customerStripeToken'], // obtained with Stripe.js
	                  "description" => "Charge for Appointment"
	                ));

		            $chargeArray = $charge->__toArray(true);
		            $status = 'success';
	                $this->db->update('appointments',array('can_be_canceled'=> 1,'payment_status'=>'success'),array('id'=>$id));

					$this->db2->update('transactions',array('status'=>$status,'transaction_id'=>$chargeArray['id']),array('id'=>$payment['id']));
	            }
	            catch (Exception $e)
	            {
	                $error = $e->getMessage();
	                $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
	            }

	            $pushMessage = ' A total amount of '.get_option('currency_symbol').' '.$finalAmount.' will be deducted.';
			}
            

			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));


			
		   	//get all service name with cost
		   	$appointmentServiceInfo = array();
		   	$appointmentService = $this->Entry_model->getAppointmentServiceById($appointmentDetails['id']);
            $pet_size = $this->db2->select('size')->get_where('pets',array('id' => $appointmentDetails['pet_id']))->row_array();

            $size_cost = $pet_size['size'].'_cost';
            $i=0;

            foreach($appointmentService as  $value){
                $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
                if(!empty($serviceData)){
                    $appointmentServiceInfo[$i]['service_name'] = $serviceData['name'];
                    $appointmentServiceInfo[$i]['service_cost'] = $serviceData[$size_cost];
                    $i++;
                }
            }

            if($appointmentDetails['add_on_service_cost'] != 0){
        		$appointmentAddOnService = $this->Entry_model->getAppointmentAddOnServiceById($id);
        		foreach($appointmentAddOnService as  $value){
                    $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
	                if(!empty($serviceData)){
	                    $appointmentServiceInfo[$i]['service_name'] = $serviceData['name'].'*';
	                    $appointmentServiceInfo[$i]['service_cost'] = $serviceData[$size_cost];
	                    $i++;
	                }
                }
        	}
            //get all service name with cost---end

           	$chargesDetails .= 'Charges Details: <br>';
           	$NewServiceCost = 0;
           	foreach ($appointmentServiceInfo as $key => $value) {
           		$NewServiceCost += $value['service_cost'];
           		$chargesDetails .= $value['service_name'].': '.get_option('currency_symbol').' '.$this->localization->currencyFormat($value['service_cost']).'<br>';
           	}
           	
           	$chargesDetails .= 'Fees: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($fees).'<br>';
           	$chargesDetails .= 'Total Service Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($NewServiceCost).'<br>';
           	$chargesDetails .= 'Additional Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($post['additional_cost']).'<br>';
           	$chargesDetails .= 'Total Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['cost']).'<br>';
          
   			
           	// pr($app_status);exit;
			if($this->db->update('appointments',array('status'=>$app_status,'can_be_canceled'=> 1),array('id'=>$id)))
			{

				// echo $this->db->last_query();
					$emailContent = array('subject' => 'Your Appointment has been completed successfully - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$AppUserDetails[0]['username'].',<br><br>
	                    
	                    The appointment for your pet '.$petDetails['name'].' has been completed as follows:
	                    <br><br>
						Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($appointmentDetails['check_in_time'])).'<br>
						Appointment Services : '.$serviceStr.'<br>               
	                    <br>

						'.$chargesDetails.'<br>                  
	                   
	                    If you are having any issues, please contact:

	                    <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>

	                    <br><br>
	                    Thanks,
	                    <br><br>

	                    Team @'.$shop_Data['shopname'].'<br><br>
	                    '
	                    );
					

					if(!empty($fdata['file_name'])){
						$image = "https://storage.googleapis.com/".BUCKETNAME."/".WatermarkImages.$fdata['file_name'];

					}
						//$image = "https://storage.googleapis.com/pet-commander-beta-app/CUSTOMERS_AND_PETS_IMAGES/KKgCjabuTO.png";
						// pr($image);exit;
						$sms_content = ucfirst($petDetails['name'])." is ready to be picked up! See you soon!";


					if($AppUserDetails[0]['number_type'] == 'Cell'){

                    	$this->send_sms($AppUserDetails[0]['phone'],$sms_content,$image);

                    }
                    if(!empty($AppUserDetails[0]['additional_Phone'])){

                    	foreach ($AppUserDetails[0]['additional_Phone'] as $key => $value) {
                    		
                    		if($value[1] == 'Cell'){
                    			$AppUserDetails[0]['additional_phone'][$key] = $value[0];
                    			$this->send_sms($AppUserDetails[0]['additional_phone'][$key],$sms_content,$image);
                    		}
                    	}
                    	
                    }
                    
						if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
						{
							
							$FCMPUSH = new PushNotifications();
							$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
				            $data = array('body'=>' '.$petDetails['name'].' is ready for pickup!'.$pushMessage,'title'=>'Pet Commander','badge'=>($badge+1),'data'=>$dataArray);
				            
				            $notifications = array();
				            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
				            $notifications['pet_id'] = $petDetails['id'];
				            $notifications['title'] = $data['title'];
				            $notifications['body'] = $data['body'];
				            $notifications['data'] = serialize($dataArray);
				            $notifications['added_on'] = date('Y-m-d H:i:s');
				            $notifications['shop_id'] = $this->session->userdata('shop_id');

				            if(isset($file) && !empty($file))
				            	$notifications['notification_type'] = 'appointment_completed';
				            else
				            	$notifications['notification_type'] = 'appointments';

				            $this->db2->insert('notifications',$notifications);
				            $data['data']['notification'] =  $notifications;
				            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
						}

				// pr($emailContent);exit;
				return email($appointmentDetails['user_email'], $emailContent,$shop_Data['id']);	
			}

		}
	}
	function refundMoney($data){
		 // pr($data);exit;
		include_once(APPPATH."libraries/PushNotifications.php");
		
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));

		$AppUserDetails = $this->getSingleUserData($data['app_user_id']);
		$cardDetails = $this->getUserDefaultCard($AppUserDetails[0]['id']);
	
		//get user default card details for refund

		$bankAccount = $this->db->get_where('bank_accounts',array('status'=>'verified','is_default'=>1))->row_array();
		// echo $this->db->last_query();exit;
		// pr($bankAccount);exit;
	if($data['refund_type'] == 'online'){
		$payment = $this->db2->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id'),'app_user_id'=>$AppUserDetails[0]['id'],'id' => $data['transaction_id']))->row_array();
	}else{
		// echo "Vf";
		 $payment = $this->db2->order_by('transactions.id','DESC')->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id'),'app_user_id'=>$AppUserDetails[0]['id']),1)->row_array();
	}
		  // pr($payment);exit;
		if((!empty($cardDetails) || !empty($bankAccount)) || ($data['refund_type'] == 'offline'))
		{

			$finalAmount = round(floatval(($data['amount'] + 0.30) / (1 - 0.029)),2);
	
			\Stripe\Stripe::setApiKey(STRIPESECRETKEY);
			// pr($finalAmount);exit;

			if($data['offline_mode'] == 'credit'){
				$offline_mode = 'credit';
				$this->db2->update('app_users',array('credit_balance'=> $data['amount']),array('id'=>$data['app_user_id']));
				// echo $this->db2->last_query();exit;
			}else{
				$offline_mode = 'cash';
			}
			// pr($data['refund_type']);exit;
			unset($data['offline_mode']);
			try 
            {
            	$refundArray['id'] = '';
            	if($data['refund_type'] == 'online'){
	            	if (empty($AppUserDetails[0]['customerStripeToken'])) {
	            		$this->message->custom_error_msg(referrer(),"Does not found Customer Stripe Token");
	            	}
	            	if (empty($payment['transaction_id'])) {
	            		$this->message->custom_error_msg(referrer(),"Does not found transaction id");
	            	}

					require_once(FCPATH.'vendor/autoload.php');
	        		$charge = \Stripe\Charge::create(array(
	                  "amount" => ($finalAmount*100),
	                  "currency" => "usd",
	                  "customer" => $AppUserDetails[0]['customerStripeToken'], // obtained with Stripe.js
	                  "description" => "Charge for Appointment "
	        		));

	        		$chargeArray = $charge->__toArray(true);
	        		// Take refund amount + stripe chare from shop  acc
	        		
	        		require_once(FCPATH.'vendor/autoload.php');
	                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	                $refund = \Stripe\Refund::create(array(
	                    "charge" => $payment['transaction_id'],
	                    "amount" => round($data['amount']*100,2)
	                ));
	                $refundArray = $refund->__toArray(true);
	                // pr( $refundArray);exit;
	                // amount refund to customer
        		}
        		// pr($data);exit;
        		if((empty($payment['pet_id'])) && (empty($payment['invoice_id']))){
        			$payment['pet_id'] = 0;
        			$payment['invoice_id'] = 0;
        			

        		}
        		$data['category'] = 'appointment';
                $insert = array();
                $insert['transaction_id'] = $refundArray['id'];
                $insert['transcation_type'] = 'payout';
                $insert['status'] = 'refunded';
                $insert['message'] = '';
               	$insert['amount'] = $data['amount'];
                $insert['payment_mode'] = $data['refund_type'];
                $insert['category'] = $data['category'];
                $insert['shop_id'] = $this->session->userdata('shop_id');
                $insert['reference_id'] = $payment['reference_id'];
                $insert['added_on']= date('Y-m-d H:i:s');
                
                $insert['added_on']= server_date($insert['added_on'],'Y-m-d H:i:s');
                
                $insert['app_user_id'] = $AppUserDetails[0]['id'];
                $insert['stripe_charge'] = 0;
			   	$insert['commission'] = 0;
				$insert['timezone'] = get_option('time_zone');
			   	$insert['cancel_by_id'] = 0;
			   	$insert['start'] = $payment['start'];
			   	$insert['end'] = $payment['end'];
			   	$insert['cancel_by'] = 'shop';
			   	$insert['pet_id'] = $payment['pet_id'];
			   	$insert['note'] = $data['note'];
                // $insert['invoice_id'] = $payment['invoice_id'];
                $insert['type'] = 'from_transaction';
               	 // pr($insert);exit;
                $this->db2->insert('transactions',$insert);
                // echo  $this->db2->last_query();exit;
      			if(($data['refund_type'] == 'offline') && ($offline_mode == 'cash')){
      				return true;
      			}

        		if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
				{
					
					$FCMPUSH = new PushNotifications();
		            $data1 = array('body'=>'Refund has been initiated','title'=>'Amount Refunded','badge'=>($badge+1),'data'=>$dataArray);
		            
		            $notifications = array();
		            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
		            $notifications['pet_id'] = 0;
		            $notifications['title'] = $data1['title'];
		            $notifications['body'] = $data1['body'];
		            $notifications['data'] = serialize($dataArray);
		            $notifications['added_on'] = date('Y-m-d H:i:s');
		            $notifications['shop_id'] = $this->session->userdata('shop_id');
		            $notifications['notification_type'] = 'appointments';
		           	$data1['data']['notification'] =  $notifications;
		            $this->db2->insert('notifications',$notifications);
		            $FCMPUSH->android($data1,$AppUserDetails[0]['fcm_id']);
				}
				$emailmsg = '';
			if($offline_mode == 'credit'){
				$amount = $data['amount'] + $AppUserDetails[0]['credit_balance'];
				$emailmsg = 'Credited Balance:'.' $'.$amount.'<br>';
			}

				$emailContent = array('subject' => 'Amount Refunded','message' =>'
	        	Hello '.$AppUserDetails[0]['username'].',<br><br>
	        
		        
				'.$shop_Data['shopname'].' has issued you a refund in the amount of $'.round($data['amount']).' in your credit balance.<br>
				'.$emailmsg.'<br>

		        <br><br>
		        If you are having any issues, please contact: 
		        <br><br>
                '.$shop_Data['shopname'].'<br>
                '.$shop_Data['address'].'<br>
                '.$shop_Data['phone_number'].'<br>
                '.get_option('shop_email').'<br>
                <br><br>
		        
		        Thanks,<br><br>
		        Team @'.PROJECTNAME.'<br><br>
                '
                );

                  
			   // pr($emailContent);exit;
			   // pr($notifications);
			   //  pr($data1);exit;
			    return email($AppUserDetails[0]['email'], $emailContent,$shop_Data['id']);
            }
            catch (Exception $e)
            {
                $error = $e->getMessage();
        		// $this->db2->update('transactions',array('transaction_id'=>'','status'=>'failed','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id']));
                
        		$this->message->custom_error_msg(referrer(),$error);
                //$this->response(array('error' => true, 'message' => $error,'errcode' => 405));	
            }
		}else{
        	// $this->db2->update('transactions',array('transaction_id'=>'','status'=>'failed','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id']));
			
			$this->message->custom_error_msg(referrer(),'No Card Found.Refund process is cancelled');
		}
	
	
		
	
}
	// function refundMoney($data){
	// 	 // pr($data);exit;
	// 	include_once(APPPATH."libraries/PushNotifications.php");
	// 	$temp1_arr = explode("_", $data['select_amount']);
	// 	$data['appointment_id'] = $temp1_arr[0];
	// 	$data['category'] = $temp1_arr[1];
	// 	// pr($temp1_arr);exit;
	// 	if(empty($temp1_arr[0])){
	// 		// echo "cdc";exit;
	// 		$data['appointment_id'] = 0;
	// 		$data['category'] = 'appointment';
	// 	}

	// 	$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));

	// 	$AppUserDetails = $this->getSingleUserData($data['app_user_id']);
	// 	$cardDetails = $this->getUserDefaultCard($AppUserDetails[0]['id']);
	// 	//$cardDetails = $this->getUserDefaultCard(23);
	// 	//get user default card details for refund

	// 	$bankAccount = $this->db->get_where('bank_accounts',array('status'=>'verified','is_default'=>1))->row_array();
	// 	//get shop default bank account details

	// 	$dataArray = array();
	// 	$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($data['appointment_id'],$data['category'],$this->session->userdata('shop_id')));

	// 	// $this->db2->order_by('app_users.id','DESC');
	// 	$payment = $this->db2->order_by('transactions.id','DESC')->get_where('transactions',array('transcation_type'=>'payment','reference_id'=>$data['appointment_id'],'shop_id'=>$this->session->userdata('shop_id'),'app_user_id'=>$AppUserDetails[0]['id']),1)->row_array();

	// 	// echo $this->db2->last_query();exit;
	// 	$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));

	// 	/*echo "<br> user details";
	// 	pr($AppUserDetails);
	// 	echo "<br> card details";
	// 	pr($cardDetails);
	// 	echo "<br> shop details";
	// 	pr($bankAccount);
	// 	echo "<br> dataArray details";
	// 	pr($dataArray);
	// 	echo "<br> payment Acc";
	// 	pr($payment);
	// 	echo "<br> data post details";
	// 	pr($data);exit;*/

	// 	if((!empty($cardDetails) || !empty($bankAccount)) || ($data['refund_type'] == 'offline'))
	// 	{
	// 		$finalAmount = round(floatval(($data['amount'] + 0.30) / (1 - 0.029)),2);
	
	// 		\Stripe\Stripe::setApiKey(STRIPESECRETKEY);


	// 		if($data['offline_mode'] == 'credit'){
	// 			$this->db2->update('app_users',array('credit_balance'=> $data['amount']),array('id'=>$data['app_user_id']));
	// 			// echo $this->db2->last_query();exit;
	// 		}
	// 		unset($data['offline_mode']);
	// 		try 
 //            {
 //            	$refundArray['id'] = '';
 //            	if($data['refund_type'] == 'online'){
	//             	if (empty($AppUserDetails[0]['customerStripeToken'])) {
	//             		$this->message->custom_error_msg(referrer(),"Does not found Customer Stripe Token");
	//             	}
	//             	if (empty($payment['transaction_id'])) {
	//             		$this->message->custom_error_msg(referrer(),"Does not found transaction id");
	//             	}

	// 				require_once(FCPATH.'vendor/autoload.php');
	//         		$charge = \Stripe\Charge::create(array(
	//                   "amount" => ($finalAmount*100),
	//                   "currency" => "usd",
	//                   "customer" => $AppUserDetails[0]['customerStripeToken'], // obtained with Stripe.js
	//                   "description" => "Charge for Appointment "
	//         		));

	//         		$chargeArray = $charge->__toArray(true);
	//         		// Take refund amount + stripe chare from shop  acc
	        		
	//         		require_once(FCPATH.'vendor/autoload.php');
	//                 \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	//                 $refund = \Stripe\Refund::create(array(
	//                     "charge" => $payment['transaction_id'],
	//                     "amount" => round($data['amount']*100,2)
	//                 ));
	//                 $refundArray = $refund->__toArray(true);
	//                 // amount refund to customer
 //        		}

 //        		if((empty($payment['pet_id'])) && (empty($payment['invoice_id']))){
 //        			$payment['pet_id'] = 0;
 //        			$payment['invoice_id'] = 0;
 //        		}
 //                $insert = array();
 //                $insert['transaction_id'] = $refundArray['id'];
 //                $insert['transcation_type'] = 'payout';
 //                $insert['status'] = 'refunded';
 //                $insert['message'] = '';
 //               	$insert['amount'] = $data['amount'];
 //                $insert['payment_mode'] = $data['refund_type'];
 //                $insert['category'] = $data['category'];
 //                $insert['shop_id'] = $this->session->userdata('shop_id');
 //                $insert['reference_id'] = $data['appointment_id'];
 //                $insert['added_on']= date('Y-m-d H:i:s');
                
 //                $insert['added_on']= server_date($insert['added_on'],'Y-m-d H:i:s');
                
 //                $insert['app_user_id'] = $AppUserDetails[0]['id'];
 //                $insert['stripe_charge'] = 0;
	// 		   	$insert['commission'] = 0;
	// 			$insert['timezone'] = get_option('time_zone');
	// 		   	$insert['cancel_by_id'] = 0;
	// 		   	$insert['start'] = $payment['start'];
	// 		   	$insert['end'] = $payment['end'];
	// 		   	$insert['cancel_by'] = 'shop';
	// 		   	$insert['pet_id'] = $payment['pet_id'];
	// 		   	$insert['note'] = $data['note'];
 //                $insert['invoice_id'] = $payment['invoice_id'];
 //                $insert['type'] = 'from_transaction';
 //               	 // pr($insert);exit;
 //                $this->db2->insert('transactions',$insert);
 //                // echo  $this->db2->last_query();exit;
 //      			if($data['refund_type'] == 'offline'){
 //      				return true;
 //      			}

 //        		if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
	// 			{
					
	// 				$FCMPUSH = new PushNotifications();
	// 	            $data1 = array('body'=>'Refund has been initiated','title'=>'Amount Refunded','badge'=>($badge+1),'data'=>$dataArray);
		            
	// 	            $notifications = array();
	// 	            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
	// 	            $notifications['pet_id'] = 0;
	// 	            $notifications['title'] = $data1['title'];
	// 	            $notifications['body'] = $data1['body'];
	// 	            $notifications['data'] = serialize($dataArray);
	// 	            $notifications['added_on'] = date('Y-m-d H:i:s');
	// 	            $notifications['shop_id'] = $this->session->userdata('shop_id');
	// 	            $notifications['notification_type'] = 'appointments';
	// 	           	$data1['data']['notification'] =  $notifications;
	// 	            $this->db2->insert('notifications',$notifications);
	// 	            $FCMPUSH->android($data1,$AppUserDetails[0]['fcm_id']);
	// 			}
				

	// 			$emailContent = array('subject' => 'Amount Refunded','message' =>'
	//         	Hello '.$AppUserDetails[0]['username'].',<br><br>
	        
		        
	// 			'.$shop_Data['shopname'].' has issued you a refund in the amount of $'.round($appointmentDetails['cost']).'<br>
	// 	        <br><br>
	// 	        If you are having any issues, please contact: 
	// 	        <br><br>
 //                '.$shop_Data['shopname'].'<br>
 //                '.$shop_Data['address'].'<br>
 //                '.$shop_Data['phone_number'].'<br>
 //                '.get_option('shop_email').'<br>
 //                <br><br>
		        
	// 	        Thanks,<br><br>
	// 	        Team @'.PROJECTNAME.'<br><br>
 //                '
 //                );

                  
	// 		    // pr($emailContent);exit;
	// 		   // pr($notifications);
	// 		   //  pr($data1);exit;
	// 		    return email($AppUserDetails[0]['email'], $emailContent);
 //            }
 //            catch (Exception $e)
 //            {
 //                $error = $e->getMessage();
 //        		// $this->db2->update('transactions',array('transaction_id'=>'','status'=>'failed','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id']));
                
 //        		$this->message->custom_error_msg(referrer(),$error);
 //                //$this->response(array('error' => true, 'message' => $error,'errcode' => 405));	
 //            }
	// 	}else{
 //        	// $this->db2->update('transactions',array('transaction_id'=>'','status'=>'failed','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id']));
			
	// 		$this->message->custom_error_msg(referrer(),'No Card Found.Refund process is cancelled');
	// 	}
	// }




	function changeAppointmentStatus($id , $status, $cancelType='credit')
	{
		if(empty($cancelType)){
			$cancelType = 'credit';
		}

		//Update Appointment status
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		$appointmentDetails = $this->Entry_model->getSingleAppointmentId($id);
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$services = $this->Entry_model->getAppointmentServiceById($id);
		$serviceStr = $this->getServiceArray($services);
		$currentDateTime0 = date('Y-m-d H:i:s');
		$currentDateTime = server_date($currentDateTime0,'Y-m-d H:i:s');
		//timeconversion
		$timeconvert = groomingTimeConverter($appointmentDetails);
		//$payment = $this->getPaymentsDetails($id,'appointments');
		include_once(APPPATH."libraries/PushNotifications.php");

		$temp_status='';
        if($status=='undo'){
            $temp_status ='undo';
            $status ='Confirm';
        }

		
		//Fetching all appointment details required
		//pr($this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));exit;
		if($this->db->update('appointments',array('status'=>$status),array('id'=>$id)))
		{

		if(!empty($petDetails['pet_cover_photo']))
			$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));
		else
			$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'appointment',$this->session->userdata('shop_id')));

		$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));

			if($temp_status == 'undo' ){
                $status ='Undo';
            }
			//Trigger mail according to appointment status
			switch ($status) {

				case 'Inprocess':
					
					$this->db->update('appointments',array('can_be_canceled'=> 1,'check_in_time'=>$currentDateTime),array('id'=>$id));
					//echo $this->db->last_query();exit;
					// pr($appointmentDetails);
					$emailContent = array('subject' => 'Your Appointment has now been checked in - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$AppUserDetails[0]['username'].',<br><br>
	                    
	                    The appointment for your pet '.$petDetails['name'].' has been Checked In:
	                    <br><br>
						Check In: '.date(get_option('date_format').', '.'h:i a', strtotime($currentDateTime0)).'<br>
					
						Appointment Services : '.$serviceStr.'<br>
						<br>
						                   
	                    
	                    If you are having any issues, please contact: 	

                        
                        <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                   	'.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

                        Thanks,<br><br>
                        Team @'.$shop_Data['shopname'].'<br><br>
	                    '
	                    );
					 	

						//Appointment Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($appointmentDetails['cost']).'<br>
						  //*Service prices may vary and additional fees may be added to your appointment upon completion <br>

						if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
						{
							$bodyContent = ' Your appointment for '.$petDetails['name'].' is now checked in';
							$titleContent = 'Appointment Checked In';
							$FCMPUSH = new PushNotifications();
				            $data = array('body'=>$bodyContent ,'title'=>$titleContent,'badge'=>($badge+1),'data'=>$dataArray);
							$FCMPUSH = new PushNotifications();
				            
				            $notifications = array();
				            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
				            $notifications['pet_id'] = $petDetails['id'];
				            $notifications['title'] = $data['title'];
				            $notifications['body'] = $data['body'];
				            $notifications['data'] = serialize($dataArray);
				            $notifications['added_on'] = date('Y-m-d H:i:s');
				            $notifications['shop_id'] = $this->session->userdata('shop_id');
				            $notifications['notification_type'] = 'appointments';
				            $data['data']['notification'] =  $notifications;
				            $this->db2->insert('notifications',$notifications);
				            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
						}
	                  	
					break;	

			    case 'Undo':

			    	$this->db->update('appointments',array('can_be_canceled'=> 0),array('id'=>$id));
                    $emailContent = array('subject' => 'Your Appointment status has been changed successfully and set to confirmed - '.PROJECTNAME.' ','message' =>'
                        Hello '.$AppUserDetails[0]['username'].',<br><br>
                        
                        The appointment for your pet '.$petDetails['name'].' has been changed and set to confirmed as follows:
                        <br><br>
                        
                        Check In: '.date("M d Y, h:i a", strtotime($appointmentDetails['appointment_date'].' '.$appointmentDetails['appointment_time'])).'<br>
                         Appointment Services : '.$serviceStr.'<br><br>
                         
                        If you are having any issues, please contact: 	

                        
                        <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                   	'.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

                        Thanks,<br><br>
                        Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );
                        

                        if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
                        {
                            
                            $FCMPUSH = new PushNotifications();
                            $data = array('body'=>' Your Appointment status for your pet '.$petDetails['name'].' has been changed and set to confirmed','title'=>'Appointment Confirmed','badge'=>($badge+1),'data'=>$dataArray);
                            
                            $notifications = array();
                            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
                            $notifications['pet_id'] = $petDetails['id'];
                            $notifications['title'] = $data['title'];
                            $notifications['body'] = $data['body'];
                            $notifications['data'] = serialize($dataArray);
                            $notifications['added_on'] = date('Y-m-d H:i:s');
                            $notifications['shop_id'] = $this->session->userdata('shop_id');
                            $notifications['notification_type'] = 'appointments';
                            $data['data']['notification'] =  $notifications;


                           $notificationData = $this->db2->order_by('id','DESC')->get_where('notifications',array('pet_id'=>$notifications['pet_id'],'app_user_id' => $notifications['app_user_id'],'shop_id' => $notifications['shop_id']),1)->row_array();

                           if(!empty($notificationData)){
                                   $this->db2->delete('notifications',array('pet_id'=>$notifications['pet_id'],'app_user_id' => $notifications['app_user_id'],'shop_id' => $notifications['shop_id'],'id' => $notificationData['id']));
                               }

                            $this->db2->insert('notifications',$notifications);
                            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
                            
                        }    
                          
                    break;

				case 'Cancel':

					
					$payment = $this->db2->get_where('transactions',array('reference_id'=>$appointmentDetails['id'],'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();

					$this->db->update('appointments',array('payment_status'=>'pending','status'=>'Cancel','can_be_canceled'=> 1,'refund_status'=>'pending','added'=>$currentDateTime),array('id'=>$id));

					$emailMsg = '';
					if($appointmentDetails['deposit'] > 0){
						if($cancelType == 'credit'){
							$credit_balance = $AppUserDetails[0]['credit_balance'] + $appointmentDetails['deposit'];
							$this->db2->update('app_users',array('credit_balance'=>$credit_balance),array('id'=>$AppUserDetails[0]['id']));

							$newPayment = $payment;
							unset($newPayment['id']);
							$newPayment['payment_source'] = 'Credit Awarded';
							$newPayment['amount'] = $appointmentDetails['deposit'];
							$newPayment['status'] = 'success';
							$newPayment['added_on'] = $currentDateTime;
							$this->db2->insert('transactions',$newPayment);
							$emailMsg = 'Amount Credited : $'.number_format($appointmentDetails['deposit'],2).'<br><br>
							The total credit balance on your profile is $'.number_format($credit_balance,2).'<br>'; 
						}else{

							if($appointmentDetails['payment_method'] == 'online'){
								 \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
				                $charge = \Stripe\Charge::create(array(
				                  "amount" => round($appointmentDetails['deposit']*100,2),
				                  "currency" => "usd",
				                  "customer" => $AppUserDetails[0]['customerStripeToken'], // obtained with Stripe.js
				                  "description" => "Charge for Appointment"
				                ));

					            $chargeArray = $charge->__toArray(true);

					            $this->db2->update('transactions',array('status'=>'refunded','payment_source'=>'Other','transcation_type'=>'payout','added_on'=>$currentDateTime,'transaction_id'=>$chargeArray['id']),array('reference_id'=>$id,'category'=>'appointment','type'=>'deposit'));
								$emailMsg = 'Amount Refunded : $'.$appointmentDetails['deposit'].'<br>';
							}else{
								$this->db2->update('transactions',array('status'=>'refunded','payment_source'=>'Other','transcation_type'=>'payout','added_on'=>$currentDateTime),array('reference_id'=>$id,'category'=>'appointment','type'=>'deposit'));
								$emailMsg = 'Amount Refunded : $'.$appointmentDetails['deposit'].'<br>';
							}

							
						}
					}

					
    				$emailContent = array('subject' => 'Your Appointment has been Cancelled - '.PROJECTNAME.' ','message' =>'
                    Hello '.$AppUserDetails[0]['username'].',<br><br>
                    
                    The appointment for your pet '.$petDetails['name'].' has been cancelled as follows:
                   <br><br>
					
					Check In: '.date("M d Y, h:i a", strtotime($appointmentDetails['appointment_date'].' '.$appointmentDetails['appointment_time'])).'<br>
					 Appointment Services : '.$serviceStr.'<br>
					 '.$emailMsg.'     
                    <br>
                    If you are having any issues, please contact:
            
                    <br><br>
                    '.$shop_Data['shopname'].'<br>
                    '.$shop_Data['address'].'<br>
                    '.$shop_Data['phone_number'].'<br>
                    '.get_option('shop_email').'<br>
                    <br><br>

                    Thanks, <br><br>
	               	Team @'.$shop_Data['shopname'].'<br><br>
                    
                    '
                    );

					if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
					{
						
						$FCMPUSH = new PushNotifications();
			            $data = array('body'=>' Your Appointment for your pet '.$petDetails['name'].' has been cancelled by shop','title'=>'Appointment Cancelled','badge'=>($badge+1),'data'=>$dataArray);
			            
			            $notifications = array();
			            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
			            $notifications['pet_id'] = $petDetails['id'];
			            $notifications['title'] = $data['title'];
			            $notifications['body'] = $data['body'];
			            $notifications['data'] = serialize($dataArray);
			            $notifications['added_on'] = date('Y-m-d H:i:s');
			            $notifications['shop_id'] = $this->session->userdata('shop_id');
			            $notifications['notification_type'] = 'appointments';
			            $data['data']['notification'] =  $notifications;
			            $this->db2->insert('notifications',$notifications);
			            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
					}
	                  
				break;

				default:

					break;
			}

			return email($appointmentDetails['user_email'], $emailContent,$shop_Data['id']);
		}
		else
		{
			return false;
		}
	}


	function updateBankAccount($accountArray,$externalAccountArray,$data,$alreadyExist)
	{
		//Add New bank account set it as default external account for payout

		if($data['account_type'] !== 'company')
		{
			$data['business_name'] = null;
		}


		if($alreadyExist == 0) {

			$insertArray = array('bankAccountHolderName'=>$data['account_holder_first_name'].' '.$data['account_holder_last_name'],
					'bankAccountHolderType'=>$data['account_type'],
					'bankAccountNo'=>$externalAccountArray['last4'],
					'bankName'=>$externalAccountArray['bank_name'],
					'bankAccountToken'=>$accountArray['id'],
					'externalAccountToken'=>$externalAccountArray['id'],
					'is_default'=>1,
					'status'=>$accountArray['legal_entity']['verification']['status'],
					'businessName'=>$data['business_name']			
			);
		}
		else
		{
			//Set other bank default account zero 
			$this->db->update('bank_accounts',array('is_default' => 0));

			$insertArray = array('bankAccountHolderName'=>$data['account_holder_first_name'].' '.$data['account_holder_last_name'],
					'bankAccountHolderType'=>$data['account_type'],
					'bankAccountNo'=>$externalAccountArray['last4'],
					'bankName'=>$externalAccountArray['bank_name'],
					'bankAccountToken'=>$accountArray['id'],
					'externalAccountToken'=>$externalAccountArray['id'],
					'is_default'=>1,
					'status'=>$accountArray['status'],
					'businessName'=>$data['business_name']			
			);
		}	

		//$this->db2 = $this->load->database('main', TRUE);

		$shop_id = $this->session->userdata('shop_id');
		//sync in master db for webhook 
		$this->db2->update('shops',array('bankAccountToken' => $accountArray['id'],'externalAccountToken'=>$externalAccountArray['id']),array('id'=>$shop_id));

		//Adding New details in bank account database
		return $this->db->insert('bank_accounts',$insertArray);
	}


	function getBankAccounts()
	{
		$this->db->order_by('id','DESC');
		return  $this->db->get_where('bank_accounts',array('is_deleted' => 0))->result_array();
	}

	function getAllcredits($start_date,$end_date)
    {
        $return = array();
        $where ='';
        if(!empty($start_date) && !empty($end_date)){
        	$where = "and date(added_on) >= '$start_date' and date(added_on) <= '$end_date'";

        }
        $result = $this->db2->query("SELECT * FROM transactions WHERE transcation_type = 'payment' AND status = 'success' AND (`payment_source` = 'Credit Awarded' OR `payment_source` = 'Credit Redeem') AND 'app_user_id' = ".$this->session->userdata('id')." AND shop_id ='".$this->session->userdata('shop_id')."' ".$where." ORDER BY added_on DESC")->result_array();
       
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');
                $temp['app_user_name'] = $this->getUsername($value['app_user_id']);
                $temp['type'] = $value['payment_source'];
             	$result['petName'] = $this->getPetName($value['pet_id']);
                $temp['services'] = $this->getappointmentsdetailsforpayment($value);
                array_push($return,$temp);
            }
        }
      
        return $return;
    }
    

	function getAllPayments()
    {
    	
        $return = array();
     

        $result = $this->db2->query("SELECT * FROM  transactions WHERE transcation_type = 'payment' AND status = 'success' AND payment_source NOT IN ('Credit Awarded', 'Credit Redeem')  AND 'app_user_id' = ".$this->session->userdata('id')." AND shop_id = ".$this->session->userdata('shop_id')." ORDER BY added_on DESC")->result_array();
    
        if(!empty($result))
        {
        	
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');

                if(!empty($temp['quick_invoice_id']) && $temp['quick_invoice_id'] >0){
                	$result = $this->db->get_where('quick_invoice',array('id'=>$value['quick_invoice_id']))->row_array();
                	if(!empty($result['customer_name']))
                		$result['app_user_name'] = $result['customer_name'];

                	$temp =array_merge($temp,$result);
                }else{
	                $temp['app_user_name'] = $this->Management_model->getUsername($value['app_user_id']);
	                $temp['services'] = $this->getappointmentsdetailsforpayment($value);
	           
	            }
                array_push($return,$temp);
            }
        }
        return $return;
    }

    function getAllPayouts()
    {
    	
        $return = array();
        
         $result = $this->db2->query("SELECT * FROM  transactions WHERE transcation_type = 'payout' AND status = 'success' AND payment_source NOT IN ('Credit Awarded', 'Credit Redeem')  AND shop_id = ".$this->session->userdata('shop_id')." AND app_user_id = ".$this->session->userdata('id')." ORDER BY added_on DESC")->result_array();

       
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['type'] = 'Refunded';
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');
                $temp['app_user_name'] = $this->Management_model->getUsername($value['app_user_id']);
                if($value['category'] == 'appointment')
                	$temp['appointment']=$this->Appuser_model->getSingleAppointmentId($value['reference_id']);
                else
                	$temp['appointment']=$this->getBoardingDayCareData($value['id']);

                // $temp['services'] = $this->getServiceForPayment($value['reference_id'],$value['category']);
                $temp['services'] = $this->getappointmentsdetailsforpayment($value);
                // pr( $temp['services']);exit;
                array_push($return,$temp);
            }
        }
        //pr($return);exit;
        return $return;

    }


    function getPaymentsByFilter($start_date,$end_date)
    {
    	$return = array();
    	$return['payments'] = array();
    	$return['cost'] = 0;

    	

    	$result = $this->db2->query("SELECT * FROM  transactions WHERE transcation_type = 'payment' AND status = 'success' AND payment_source NOT IN ('Credit Awarded', 'Credit Redeem')  AND shop_id = ".$this->session->userdata('shop_id')." and date(added_on) >= '".$start_date."' and date(added_on) <= '".$end_date."' ORDER BY added_on DESC")->result_array();
        
        // pr($result);exit;
        $cost = 0;
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');

                if(!empty($temp['quick_invoice_id']) && $temp['quick_invoice_id'] >0){
                	$result = $this->db->get_where('quick_invoice',array('id'=>$value['quick_invoice_id']))->row_array();
                	if(!empty($result['customer_name']))
                		$result['app_user_name'] = $result['customer_name'];

                	$temp =array_merge($temp,$result);
                	$return['cost'] += floatval($value['amount']);
                }else{
	                $temp['app_user_name'] = $this->Management_model->getUsername($value['app_user_id']);

	                $return['cost'] += floatval($value['amount']);
	                 $temp['services'] = $this->getappointmentsdetailsforpayment($value);
	                // if($value['category'] == 'appointment'){
	                // 	if($value['is_add_on_payment'] == 0)
	                // 		$temp['services'] = $this->getServiceForPayment($value['reference_id'],$value['category']);
	                // 	else
	                // 		$temp['services'] = $this->getAddOnServices($value['reference_id'],$value['category']);

	                // 	if(empty($temp['services']))
	                // 		$temp['services'] = ucfirst($value['category']).' Additional Payment';
	                // }
	                // else
	                // {
	                // 	if($value['is_add_on_payment'] == 0)
	                // 		$temp['services'] = ucfirst($value['category']);
	                // 	else
	                // 		$temp['services'] = ucfirst($value['category']).' Additional Payment';
	                // }
	            }
                array_push($return['payments'],$temp);
            }
        }
        //pr($return);

        return $return;
    }

   
    function addnewquickinvoice()
	{
		$postarray = $_POST;
		if(empty($postarray['customer_name'])){
			$postarray['customer_name'] = 'Walk-In';
		}
		if(empty($postarray['cust_phone_no'])){
			$postarray['cust_phone_no'] = 0;
		}
		if(empty($postarray['cost'])){
			$postarray['cost'] = 0;
		}
		$cost = $postarray['cost'];
		$payment_source = $postarray['payment_source'];
		$other_source = $postarray['other_source'];
		$postarray['cust_phone_no'] = preg_replace("/[^a-zA-Z0-9]+/", "", $postarray['cust_phone_no']);
		
		$services = implode(", ", $postarray['services']);
		$added_on = date('Y-m-d H:i:s',strtotime($postarray['added_on']));
		$added_on = server_date($added_on,'Y-m-d H:i:s');
		$postarray['date'] = $added_on;
		
		unset($postarray['services']);
		unset($postarray['payment_source']);
		unset($postarray['other_source']);
		unset($postarray['petSizeInvoice']);
		unset($postarray['added_on']);
		
		
		$postarray['services'] = $services;
		
		$postarray['created_by'] = $this->session->userdata('username');
		$postarray['invoice_no'] = $this->getinvoiceno();

        $this->db->insert('quick_invoice',$postarray);
		$insert_id = $this->db->insert_id();
		
		$transactionsArray =  array(
			'quick_invoice_id' => $insert_id,
			'amount' => $cost,
			'shop_id' => $this->session->userdata('shop_id'),
			'reference_id' => 0,
			'added_on' => $added_on,
			'type' => 'full',
			'payment_source' => $payment_source,
			'other_source' => $other_source,
			'status' => 'success'
		);
		return $this->db2->insert('transactions',$transactionsArray);
	}

    function getServiceForPayment($id,$category)
    {
    	if($category == 'appointment')
    	{
    		$this->db->select('GROUP_CONCAT(services.name SEPARATOR ", ") as services');
    		//$this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        	$this->db->join('services','services.id = appointment_services.service_id');
    		$result = $this->db->get_where('appointment_services',array('appointment_id'=>$id))->row_array();
    		if(!empty($result))
    		{
    			 return $result['services'];
    		}
    	}
    	else
    	{
    		return ucfirst($category);
    	}
    }

  


    function getRefundsByFilter($start_date,$end_date)
    {
    	$return = array();
    	$return['payments'] = array();
    	$return['cost'] = 0;
        // $this->db2->order_by('Date(added_on)','DESC');
        // $this->db2->group_start();
        // $this->db2->where(array('transcation_type'=>'payment','payment_mode'=>'offline'));
        // $this->db2->or_where(array('transcation_type'=>'payout','payment_mode'=>'online'));
        // $this->db2->group_end();
        // $this->db2->where_in('status',array('refunded'));
        // $result = $this->db2->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id')))->result_array();

        

    	 $cost = 0;
        // $result = $this->db2->query("SELECT * FROM `transactions` WHERE `transcation_type` = 'payment' AND `payment_mode` = 'offline' AND `status` IN('refunded') AND `shop_id` = '".$this->session->userdata('shop_id')."'  and date(added_on) >= '".$start_date."' and date(added_on) <= '".$end_date."'  UNION All SELECT * FROM `transactions` WHERE `transcation_type` = 'payout' AND `payment_mode` = 'online' AND `status` IN('refunded') AND `shop_id` = '".$this->session->userdata('shop_id')."'  and date(added_on) >= '".$start_date."' and date(added_on) <= '".$end_date."' ")->result_array();


        $result = $this->db2->query("SELECT * FROM `transactions` WHERE `transcation_type` = 'payment' AND `payment_mode` = 'offline' AND `status` IN('refunded') AND `shop_id` = '".$this->session->userdata('shop_id')."'  and date(added_on) >= '".$start_date."' and date(added_on) <= '".$end_date."'  UNION All SELECT * FROM `transactions` WHERE `transcation_type` = 'payout' AND `status` IN('refunded') AND `shop_id` = '".$this->session->userdata('shop_id')."'and date(added_on) >= '".$start_date."' and date(added_on) <= '".$end_date."' ")->result_array();
        // echo $this->db2->last_query();exit;
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');

                $temp['app_user_name'] = $this->Management_model->getUsername($value['app_user_id']);
                if($value['category'] == 'appointment')
                	$temp['appointment']=$this->Entry_model->getSingleAppointmentId($value['reference_id']);
                else
                	$temp['appointment']=$this->getBoardingDayCareData($value['id']);

                 $return['cost'] += floatval($value['amount']);

                // $temp['services'] = $this->getServiceForPayment($value['reference_id'],$value['category']);
                $temp['services'] = $this->getappointmentsdetailsforpayment($value);
                array_push($return['payments'],$temp);
            }
        }
        //pr($return);exit;
        return $return;
    }


    function getUsernameById($id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);

        $result = $this->db2->get_where('app_users',array('id'=>$id))->row_array();
        if(!empty($result))
            return $result['username'];
        else
            return 'N/A';
    }

    function PaymentsDetails($id,$category){
    	$payment = $this->db2->get_where('transactions',array('transcation_type'=>'payment','shop_id'=>$this->session->userdata('shop_id'),'reference_id'=>$id,'category'=>$category))->row_array();
    	// echo $this->db2->last_query();
    	// echo "<br>";
    	//pr($payment);
    	return $payment['stripe_charge'];
    }

    function getShopNameById($id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);

        $result = $this->db2->get_where('shops',array('id'=>$id))->row_array();
        if(!empty($result))
            return $result['shopname'];
        else
            return 'N/A';
    }

    function getBoardingInventory()
    {
    	return $this->db->get_where('boarding_inventory')->result_array();
    }

    function getboardingAdditionalParametersForPriceCalculation()
    {
    	$this->db->order_by('id','DESC');
    	return $this->db->get_where('boarding_price')->row_array();

    }

   // function checkBoardingExistForSamePet($pet_id,$start_time,$end_time,$start_date,$end_date,$category)
   // {
   //     //pr($_POST);
       
   //     if($category == 'boarding')
   //      {
   //          //For Boarding
   //           $result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$end_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status not in ("Cancel","Checkout") ')->result_array();
   //           //echo $this->db->last_query();exit;
   //      }
   //      else
   //      {
   //          //For Day Care

            
   //         	$date = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
			// $startDate = server_date($date,'Y-m-d H:i:s');
			// $date = date('Y-m-d H:i:s', strtotime($start_date.' '.$end_time));
			// $endDate = server_date($date,'Y-m-d H:i:s');

   //          $result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$startDate.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$endDate.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status != "Cancel" ')->result_array();
            
   //          //$result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$start_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status != "Cancel" ')->result_array();
            
   //          //echo $this->db->last_query();exit;
   //      }
   //     if(!empty($result)){
   //         return true;
   //     }    
   //     else
   //     {
   //         return false;
   //     }
   // }


    function checkBoardingExistForSamePet($pet_id,$start_time,$end_time,$start_date,$end_date,$category,$appointment_id)
  	{
      //pr($_POST);

        $date = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
        $startDate = server_date($date,'Y-m-d H:i:s');
        $date = date('Y-m-d H:i:s', strtotime($end_date.' '.$end_time));
        $endDate = server_date($date,'Y-m-d H:i:s');

        $result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE("'.$startDate.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE("'.$endDate.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and id != '.$appointment_id.' and status not in ("Cancel","Checkout") ')->result_array();

        // pr($result);
        // echo $this->db->last_query();exit;
      if(!empty($result)){
          return true;
      }
      else
      {
          return false;
      }
  	}
  //   function checkBoardingExistForPet($pet_id,$start_time,$end_time,$start_date,$end_date,$category,$appointment_id)
  //   {

  //   	$date1 =server_date(date('Y-m-d H:i:s',strtotime($start_date.' '.$start_time)),'Y-m-d H:i:s');
  //   	$date2 =server_date(date('Y-m-d H:i:s',strtotime($end_date.' '.$end_time)),'Y-m-d H:i:s');

  //   	$start_date = date('Y-m-d',strtotime($date1));
  //   	$start_time = date('H:i:s',strtotime($date1));
  //   	$end_date = date('Y-m-d',strtotime($date2));
  //   	$end_time = date('H:i:s',strtotime($date2));

  //   	if($category == 'boarding')
		// {
		// 	//For Boarding
		//  	$result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$end_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and id != '.$appointment_id.' and  (pet_id = '.$pet_id.' ) and status not in ("Cancel","Checkout")')->result_array();

		//  	//Check For Appointment
		//  	$appointment = $this->db->query('SELECT * FROM appointments WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$end_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status not in ("Cancel","Checkout")')->result_array();

		//  	//echo $this->db->last_query();exit;

		// }
		// else
		// {
		// 	//For Day Care
		// 	$result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$start_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and id != '.$appointment_id.' and status != "Cancel" ')->result_array();


		// 	//Check For Appointment
		// 	$appointment = $this->db->query('SELECT * FROM appointments WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$start_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status != "Cancel"')->result_array();
		// }
  //   	//echo $this->db->last_query();
  //   	//pr($result);pr($appointment);exit;
  //   	if(!empty($result) || !empty($appointment)){
  //   		return true;
  //   	}	
  //   	else
  //   	{
  //   		return false;
  //   	}
  //   }
    

    function checkAppointmentExistForPet($pet_id,$start_time,$end_time,$start_date,$end_date)
    {
    	if(empty($pet_id))
    		$pet_id = 0;
		
		$startDate = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
    	$startDate = server_date($startDate,'Y-m-d H:i:s');
    	$Date = date('Y-m-d', strtotime($startDate));
    	
	 	//$appointment = $this->db->query('SELECT * FROM appointments WHERE STR_TO_DATE("'.$startDate.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE("'.$endDate.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and isDuplicate = "0" and status not in ("Cancel")')->result_array();

	 	$appointment = $this->db->query('SELECT * FROM appointments WHERE appointment_date = "'.$Date.'" and  (pet_id = '.$pet_id.' ) and isDuplicate="0" and status not in ("Cancel")')->result_array();
	 	
	 	$newAppointment = array();
	 	if(!empty($appointment)){
			$result = daycareBoardingTimeConverter($result);
			$startDate = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
			$endDate = date('Y-m-d H:i:s', strtotime($end_date.' '.$end_time));
			foreach ($appointment as $key => $value) {
				$value = groomingTimeConverter($value);

				$startDate1 = date('Y-m-d H:i:s', strtotime($value['appointment_date'].' '.$value['appointment_time']));
				$endDate1 = date('Y-m-d H:i:s', strtotime($value['appointment_date'].' '.$value['appointment_end_time']));

				if($startDate < $endDate1 && $endDate > $startDate1){
					$newAppointment[] = $value;
				}
			}
		}

    	return $newAppointment;
    }
    function checkBoardingForPet($pet_id,$start_time,$end_time,$start_date,$end_date){
    	if(empty($pet_id))
    		$pet_id = 0;

		//For Boarding
		//$this->db->limit(1);
		$startDate = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
    	$startDate = server_date($startDate,'Y-m-d H:i:s');
    	$endDate = date('Y-m-d H:i:s', strtotime($end_date.' '.$end_time));
    	$endDate = server_date($endDate,'Y-m-d H:i:s');

		$newArray = array();
	 	$result=$this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE("'.$startDate.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE("'.$endDate.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status not in ("Cancel")')->result_array();

	 	if(!empty($result)){
    		$newArray = $result;
    		return $newArray;
    	}
	 	return $newArray;
    }
  
    
    function checkBoardingForOverlapping($pet_id,$start_time,$end_time,$start_date,$end_date)
    {
    	//For Tempory
    	$temp = array();
    	$temp['isOverbooking'] = 0;
    	
    	$count = $this->getInventoryCountForBoarding();
    	//echo 'orginal count'.$count;
    	$confirmBoarding = $this->getTotalboardingForChecking($pet_id,$start_time,$end_time,$start_date,$end_date,'Confirm','boarding');
    	
    	$confirmDayCare =  $this->getTotalboardingForChecking($pet_id,$start_time,$end_time,$start_date,$end_date,'Confirm','daycare');
    	//pr($this->db->last_query());exit;
    	$pendingBoarding =  $this->getTotalboardingForChecking($pet_id,$start_time,$end_time,$start_date,$end_date,'Pending','boarding');
    	
    	$pendingDayCare =  $this->getTotalboardingForChecking($pet_id,$start_time,$end_time,$start_date,$end_date,'Pending','daycare');
    	
    	// echo count($confirmBoarding);
    	// echo "confirmed boardings"."<br>";
    	if(!empty($confirmBoarding))
    	{	
    		if($count >= 0)
    			$t = 1;
    		else
    			$t = 0;

    		$count = $count-count($confirmBoarding);

    		if($count >= 0){
				$temp['confirm']['boardingOverbookedInventory'] = 0;
			}
			else {
				if($t == 1)
					$temp['confirm']['boardingOverbookedInventory'] = abs($count);
				else
					$temp['confirm']['boardingOverbookedInventory'] = count($confirmBoarding);

				$temp['isOverbooking'] = 1;	
			}

    	}
    	else
    		$temp['confirm']['boardingOverbookedInventory'] = 0;

    	// echo count($confirmDayCare);
    	// echo "confirmed daycare"."<br>";
    	if(!empty($confirmDayCare))
    	{	

    		if($count >= 0)
    			$t = 1;
    		else
    			$t = 0;

    		$count = $count-count($confirmDayCare);
    		if($count >= 0){
				$temp['confirm']['dayCareOverbookedInventory'] = 0;
			}
			else {
				if($t == 1)
					$temp['confirm']['dayCareOverbookedInventory'] = abs($count);
				else
					$temp['confirm']['dayCareOverbookedInventory'] = count($confirmDayCare);

				$temp['isOverbooking'] = 1;	
			}

    	}
    	else
    		$temp['confirm']['dayCareOverbookedInventory'] = 0;

    	// echo count($pendingBoarding);
    	// echo "pending boarding"."<br>";
    	if(!empty($pendingBoarding))
    	{	

    		if($count >= 0)
    			$t = 1;
    		else
    			$t = 0;

    		$count = $count-count($pendingBoarding);
    		if($count >= 0){
				$temp['pending']['boardingOverbookedInventory'] = 0;
			}
			else {
				if($t == 1)
					$temp['pending']['boardingOverbookedInventory'] =abs($count);
				else
					$temp['pending']['boardingOverbookedInventory'] =count($pendingBoarding);

				$temp['isOverbooking'] = 1;	
			}

    	}
    	else
    		$temp['pending']['boardingOverbookedInventory'] = 0;

    	// echo count($pendingDayCare);
    	// echo "pending day care"."<br>";

    	if(!empty($pendingDayCare))
    	{	

    		if($count >= 0)
    			$t = 1;
    		else
    			$t = 0;

    		$count = $count-count($pendingDayCare);
    		if($count >= 0){
				$temp['pending']['dayCareOverbookedInventory'] = 0;
			}
			else {
				if($t == 1)
					$temp['pending']['dayCareOverbookedInventory'] = abs($count);
				else
					$temp['pending']['dayCareOverbookedInventory'] = count($pendingDayCare);

				$temp['isOverbooking'] = 1;	
			}

    	}
    	else
    		$temp['pending']['dayCareOverbookedInventory'] = 0;


    	//echo $count;exit;
    	return $temp;
    }

    function getInventoryCountForBoarding()
    {
    	//Inventory Count
    	$result = $this->db->select('count(id) as inventoryCount')->get_where('boarding_inventory',array('is_blocked'=>0,'is_deleted'=>0))->row_array();
    	return $result['inventoryCount'];
    }

    function randomString($length = 3) {
	    return substr(str_shuffle(implode(array_merge(range('A','Z'), range(0,9)))), 0, $length);
	}
    function addNewBoardingAjax($postArray,$category,$appUser)
    {
    	
    	$insertArray = $postArray;
    	// pr($postArray);exit;
    	$boardingSettings = $this->getBoardingSettings();
    	$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
 
    	unset($insertArray['payment_source']);
		unset($insertArray['other_source']);

		if($category == 'daycare'){
	    	$insertArray['start_date'] = createDate($postArray['start_date']);
	    	$insertArray['end_date'] = createDate($postArray['start_date']);
	    		
	    	$insertArray['start_time'] = date("H:i", strtotime($postArray['start_time']));
	    	$insertArray['end_time'] = date("H:i", strtotime($postArray['end_time']));
	    }
	    else
	    {
	    	$insertArray['start_date'] = createDate($postArray['start_date']);
	    	$insertArray['end_date'] = createDate($postArray['end_date']);
	    	$insertArray['start_time'] = date("H:i", strtotime($postArray['start_time']));
	    	$insertArray['end_time'] = date("H:i", strtotime($postArray['end_time']));

	    	$datediff = strtotime($insertArray['end_date']) - strtotime($insertArray['start_date']);
    		$datediff = $datediff / (60 * 60 * 24);
	    }

    	
    	//Directly assign locations to pet
	    $insertArray['timezone'] = get_option('time_zone');
    		
    	$petDetails = $this->Entry_model->getPetsViewDetails($insertArray['pet_id']);
    	
    	if($category == 'daycare')
    	{	
    		$insertArray['amount'] = $boardingSettings['normalDayCarePricing'];
    		$insertArray['inventory_type'] = 'playarea';
    		$insertArray['assigned_inventory_type'] = 'playarea';
    		$insertArray['inventory'] = $insertArray['assigned_inventory_id'];

    		if($insertArray['overlapping_appointment'] == 0){
    			$location = $this->db->get_where('daycare_schedule',array('current_capacity >'=>0,'date'=>$insertArray['start_date']))->row_array();
    			if(!empty($location))
				{
					$insertArray['inventory'] = $location['playarea_id'];
					$insertArray['assigned_inventory_id'] = $location['playarea_id'];
					$this->db->update('daycare_schedule',array('current_capacity'=>($location['current_capacity']-1)),array('id'=>$location['id']));
				}
				else
				{
					$insertArray['inventory'] = 0;
					$insertArray['overlapping_appointment'] = 1;
					$insertArray['assigned_inventory_id'] = 0;	
				}
    		}		
    	}
    	else
    	{
    		if($petDetails['type'] == 'Cat')
			{
				$insertArray['amount'] = floatval($boardingSettings['catBoardingPricing']*$datediff);
			}
			else
			{
				$insertArray['amount'] = floatval($boardingSettings['dogBoardingPricing']*$datediff);
			}

    		

    		$insertArray['inventory_type'] = 'boarding';
    		$insertArray['assigned_inventory_type'] = 'boarding';
    		$insertArray['inventory'] = $insertArray['assigned_inventory_id'];

			
			if($insertArray['overlapping_appointment'] == 0){
	    		$start_date = $insertArray['start_date'];
	    		$end_date =  $insertArray['end_date'];

	    		$startDate = date('Y-m-d H:i:s', strtotime($start_date.' '.$insertArray['start_time']));
				$startDate = server_date($startDate,'Y-m-d');
				
				$endDate = date('Y-m-d H:i:s', strtotime($end_date.' '.$insertArray['end_time']));
				$endDate = server_date($endDate,'Y-m-d');
				


	    		$size_priority = getSizePriority($petDetails['size']);

	    		$result = $this->db->query("select boarding_inventory.* from boarding_inventory where id not in (SELECT boardings.inventory from boardings where boardings.status not in ('Cancel', 'Checkout')  and boardings.inventory_type = 'boarding' and category = 'boarding' and boardings.start_date < '".$endDate."' and boardings.end_date > '".$startDate."') and size_priority >= '".$size_priority."' order by size_priority asc limit 1")->row_array();

	    		if(!empty($result))
	    		{
	    			$insertArray['inventory'] = $result['id'];
					$insertArray['inventory_type'] = 'boarding';
					$insertArray['assigned_inventory_id'] = $result['id'];
					$insertArray['assigned_inventory_type'] = 'boarding';

					$this->db->update('boarding_schedule',array('is_booked'=>1),array('start<'=>$end_date,'end >'=>$start_date,'inventory_id'=>$result['id']));
					//echo $this->db->last_query();
	    		}
	    		else
	    		{
	    			$insertArray['inventory'] = 0;
					$insertArray['inventory_type'] = 'playarea';
					$insertArray['overlapping_appointment'] = 1;
					$insertArray['assigned_inventory_id'] = 0;
					$insertArray['assigned_inventory_type'] = 'playarea';
	    		}
	    	}
    	}

    	$insertArray['status'] = 'Confirm';
    	$insertArray['payment_method'] = 'offline';
    	$insertArray['added_on'] =  date('Y-m-d H:i:s');
    	$insertArray['updated_on'] =  date('Y-m-d H:i:s');
    	$insertArray['transfer_status'] = 'pending';
    	$insertArray['created_by'] =$this->session->userdata('username');

        $insertArray['discount_amount'] = 0;
        $insertArray['discount_id'] = 0;

        if(empty($insertArray['deposit'])){
    		$insertArray['deposit'] = 0 ;
    	}

    	if(isset($insertArray['id']))
    		unset($insertArray['id']);

    	//this is for convert system time into UTC time 
        $insertArray = boardingServerTimeConverter($insertArray);
        $insertArray['inserted_cost'] = $insertArray['amount'];
        $insertArray['phone'] = $appUser[0]['phone'];
        $insertArray['email'] = $appUser[0]['email'];
        $insertArray['invoice_no'] = $this->getinvoiceno();
        $insertArray['sms_code'] = $this->randomString();
        $insertArray['sms_code'] = $insertArray['sms_code'].$this->session->userdata('shop_id').'B';
        // pr($insertArray['sms_code']);exit;
       
    	if($insertArray['id'] = singleInsert('boardings',$insertArray))
    	{	
			$insert = array();
			$insert['category'] = $insertArray['category'];
			$insert['shop_id'] = $this->session->userdata('registered_shop');
			$insert['amount'] = $insertArray['amount'];
			$insert['reference_id'] = $insertArray['id'];
			$insert['added_on'] = date('Y-m-d H:i:s');
			$insert['status'] = 'pending';
			$insert['transcation_type']='payment';
			$insert['app_user_id'] = $insertArray['app_user_id'];
			$insert['payment_mode'] = 'offline';
			$insert['ref_app_id'] = 0;
			$insert['invoice_id'] = $insertArray['invoice_no'];
			$insert['start'] = date('Y-m-d H:i:s',strtotime($insertArray['start_date'].' '.$insertArray['start_time']));

	   		if($category == 'daycare')
	   			$insert['end'] = date('Y-m-d H:i:s',strtotime($insertArray['start_date'].' '.$insertArray['end_time']));
	   		else
	   			$insert['end'] = date('Y-m-d H:i:s',strtotime($insertArray['end_date'].' '.$insertArray['end_time']));

	   		$insert['timezone'] = get_option('time_zone');
	   		$insert['cancel_by_id'] = 0;
	   		$insert['pet_id'] = $insertArray['pet_id'];
	   		if($postArray['deposit'] > 0){
		   		$insert['amount'] = $insert['amount'] - $postArray['deposit'];
		   		$insert['type'] = 'balance';
		   	}

			$this->db2->insert('transactions',$insert);

			if($postArray['deposit'] > 0){
    			$insert['amount'] = $postArray['deposit'];
    			$insert['type'] = 'deposit';
    			$insert['payment_source'] = $postArray['payment_source'];
    			$insert['other_source'] = $postArray['other_source'];
    			$insert['status'] = 'success';
    			$this->db2->insert('transactions',$insert);
    		}
			
			$AppUserDetails = $this->getSingleUserData($postArray['app_user_id']);

			if($insert['category'] == 'boarding'){
				$endMsgLine = date(get_option('date_format'), strtotime($insertArray['end_date'])).' '.date('h:i a', strtotime($insertArray['end_time'])).'<br>';
				$title = 'Boarding Request Confirmed';
				$PushMessage = 'Your '.ucfirst($insertArray['category']).' Request for your pet '.$petDetails['name'].' has been confirmed.';
			}
			else
			{
				$endMsgLine = date(get_option('date_format'), strtotime($insertArray['start_date'])).' '.date('h:i a', strtotime($insertArray['end_time'])).'<br>';
				$title = 'Daycare Request Confirmed';
				$PushMessage = 'Your '.ucfirst($insertArray['category']).' Request for your pet '.$petDetails['name'].' has been confirmed.';
			}

			//pr($AppUserDetails);
			if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
			{
				include_once(APPPATH."libraries/PushNotifications.php");
				$FCMPUSH = new PushNotifications();

				if(!empty($petDetails['pet_cover_photo']))
					$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($insertArray['id'],'boarding',$this->session->userdata('shop_id')));
				else
					$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($insertArray['id'],'boarding',$this->session->userdata('shop_id')));
				

				$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
	            $data = array('body'=>$PushMessage,'title'=>$title,'badge'=>($badge+1),'data'=>$dataArray);
	            
	            $notifications = array();
	            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
	            $notifications['pet_id'] = $insertArray['pet_id'];
	            $notifications['title'] = $data['title'];
	            $notifications['body'] = $data['body'];
	            $notifications['data'] = serialize($dataArray);
	            $notifications['added_on'] = date('Y-m-d H:i:s');
	            $notifications['shop_id'] = $this->session->userdata('shop_id');
	            $notifications['notification_type'] = 'appointments';
	            $data['data']['notification'] =  $notifications;
	            $this->db2->insert('notifications',$notifications);
	            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
	            //pr($notifications);
			}

				$tempDate = $postArray['end_date'];

				if($insertArray['category'] == 'daycare')
					$tempDate = $postArray['start_date'];

				$app_time = '';
				if($insertArray['category'] == 'boarding'){
					$app_time=	'Drop-off Time: '.date("M d Y, h:i a", strtotime($postArray['start_date'].' '.$postArray['start_time'])).'<br>
					Pick-up Time: '.date("M d Y, h:i a", strtotime($tempDate.' '.$postArray['end_time'])).'<br>';
				}else{
					$app_time=	'Check In: '.date("M d Y, h:i a", strtotime($postArray['start_date'].' '.$postArray['start_time'])).'<br>
					Check Out: '.date("M d Y, h:i a", strtotime($tempDate.' '.$postArray['end_time'])).'<br>';
				}

			if(!empty($insertArray['email'])){
				$emailContent = array('subject' => 'Your '.ucfirst($insertArray['category']).' Request has been confirmed successfully - '.PROJECTNAME.' ','message' =>'
	                Hello '.$AppUserDetails[0]['username'].',<br><br>
	                
	                Your '.ucfirst($insertArray['category']).' Request for your pet '.ucfirst($petDetails['name']).' has been confirmed. 
	                <br><br>
					'.$app_time.'
	                <br>
	                If you are having any issues, please contact: 	
	               	<br><br>
                    '.$shop_Data['shopname'].'<br>
                    '.$shop_Data['address'].'<br>
                    '.$shop_Data['phone_number'].'<br>
                    '.get_option('shop_email').'<br>
                    <br><br>
	                
	                Thanks,<br><br>
	               	
	               	Team @'.$shop_Data['shopname'].'<br><br>
                    
                    '
                    );

				// pr($emailContent);exit;
		    	email($insertArray['email'], $emailContent,$shop_Data['id']);
		    }
		}
		
		return true;
	}

    function addNewBoarding($postArray,$category)
    {
    	$normalBookingArr = array();
    	//$postArray['normalBookingId'] = '12693 12695';
    	if(!empty($postArray['normalBookingId'])){
    		$postArray['normalBookingId'] = trim($postArray['normalBookingId']);
    		$normalBookingArr = explode(" ", $postArray['normalBookingId']);
    	}
    	
    	$pet_ids = array();
    	foreach ($normalBookingArr as $key => $value) {
    		$pet_ids[$key]['pet_id'] = $value;
    		$pet_ids[$key]['inventory_id'] = '';

    	}
    	$pet_ids1 = array();
    	$i = 0;
    	foreach (json_decode($postArray['overBookingarray']) as $key => $value) {
    		if(!empty($value->inventory_id)){
    			$pet_ids1[$i]['pet_id'] = $value->pet_id;
    			$pet_ids1[$i]['inventory_id'] = $value->inventory_id;
    			$i++;
    		}
    	}
    	$pet_ids = array_merge($pet_ids, $pet_ids1);

    	
    	$insertArray = $postArray;

    	if(isset($insertArray['normalBookingId']))
    		unset($insertArray['normalBookingId']);

    	if(isset($insertArray['overBookingarray']))
    		unset($insertArray['overBookingarray']);

    	$boardingSettings = $this->getBoardingSettings();
    	
    	$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
    	$insertArray['phone'] = preg_replace("/[^a-zA-Z0-9]+/", "", $insertArray['phone']);
    	$this->db2->update('app_users',array('emergency_contact_name'=>$insertArray['emergency_contact_name'],'emergency_contact_phone_number'=>$insertArray['emergency_contact_phone_number'],'phone'=>$insertArray['phone']),array('id'=>$insertArray['app_user_id']));

    	unset($insertArray['emergency_contact_name']);
    	unset($insertArray['emergency_contact_phone_number']);

    	unset($insertArray['payment_source']);
		unset($insertArray['other_source']);

		if($category == 'daycare'){
    		$startDateForDiscount = $postArray['start_date'];
	    	$insertArray['start_date'] = createDate($postArray['start_date']);
	    	
	    	$insertArray['end_date'] = createDate($postArray['start_date']); 
	    	
	    		
	    	$insertArray['start_time'] = date("H:i", strtotime($postArray['start_time']));
	    	$insertArray['end_time'] = date("H:i", strtotime($postArray['end_time']));
	    }
	    else
	    {
	    	$startDateForDiscount = $postArray['start_date_boarding'];
	    	$insertArray['start_date'] = createDate($postArray['start_date_boarding']);
	    	$insertArray['end_date'] = createDate($postArray['end_date_boarding']);
	    	$insertArray['start_time'] = date("H:i", strtotime($postArray['start_time_boarding']));
	    	$insertArray['end_time'] = date("H:i", strtotime($postArray['end_time_boarding']));

	    	unset($insertArray['start_date_boarding']);
	    	unset($insertArray['end_date_boarding']);
	    	unset($insertArray['start_time_boarding']);
	    	unset($insertArray['end_time_boarding']);

	    	$datediff = strtotime($insertArray['end_date']) - strtotime($insertArray['start_date']);
    		$datediff = $datediff / (60 * 60 * 24);
	    }

    	
    	//Directly assign locations to pet
	    $insertArray['timezone'] = get_option('time_zone');
	    $insertArray['deposit'] = floatval($insertArray['deposit']/count($pet_ids));
	    
    	foreach ($pet_ids as $pets) {
    		
	    	$petDetails = $this->Entry_model->getPetsViewDetails($pets['pet_id']);
	    	$insertArray['pet_id'] = $pets['pet_id'];
	    	if($category == 'daycare')
	    	{	
	    		$insertArray['amount'] = $boardingSettings['normalDayCarePricing'];
	    		if(!empty($pets['inventory_id']))
				{
					$insertArray['inventory'] = $pets['inventory_id'];
					$insertArray['inventory_type'] = 'playarea';
					$insertArray['overbooking'] = 1;
					$insertArray['assigned_inventory_id'] = $pets['inventory_id'];
					$insertArray['assigned_inventory_type'] = 'playarea';
				}else{
					$location = $this->db->get_where('daycare_schedule',array('current_capacity >'=>0,'date'=>$insertArray['start_date']))->row_array();
					if(!empty($location))
					{
						
						$insertArray['inventory'] = $location['playarea_id'];
						$insertArray['inventory_type'] = 'playarea';
						$insertArray['assigned_inventory_id'] = $location['playarea_id'];
						$insertArray['assigned_inventory_type'] = 'playarea';
						$this->db->update('daycare_schedule',array('current_capacity'=>($location['current_capacity']-1)),array('id'=>$location['id']));
					}
					else
					{
						$insertArray['inventory'] = 0;
						$insertArray['inventory_type'] = 'playarea';
						$insertArray['overlapping_appointment'] = 1;
						$insertArray['assigned_inventory_id'] = 0;
						$insertArray['assigned_inventory_type'] = 'playarea';
					}
				}		
	    	}
	    	else
	    	{
	    		if($petDetails['type'] == 'Cat')
				{
					$insertArray['amount'] = floatval($boardingSettings['catBoardingPricing']*$datediff);
				}
				else
				{
					$insertArray['amount'] = floatval($boardingSettings['dogBoardingPricing']*$datediff);
				}
	    		
				if(!empty($pets['inventory_id']))
				{
					$insertArray['inventory'] = $pets['inventory_id'];
					$insertArray['inventory_type'] = 'boarding';
					$insertArray['overbooking'] = 1;
					$insertArray['assigned_inventory_id'] = $pets['inventory_id'];
					$insertArray['assigned_inventory_type'] = 'boarding';
				}else{
		    		$start_date = $insertArray['start_date'];
		    		$end_date =  $insertArray['end_date'];	
		    		$size_priority = getSizePriority($petDetails['size']);

		    		$result = $this->db->query("select boarding_inventory.* from boarding_inventory where id not in (SELECT boardings.inventory from boardings where boardings.status != 'Cancel' and boardings.inventory_type = 'boarding' and category = 'boarding' and boardings.start_date < '".$end_date."' and boardings.end_date > '".$start_date."') and size_priority >= '".$size_priority."' order by size_priority asc limit 1")->row_array();
		    		if(!empty($result))
		    		{
		    			$insertArray['inventory'] = $result['id'];
						$insertArray['inventory_type'] = 'boarding';
						$insertArray['assigned_inventory_id'] = $result['id'];
						$insertArray['assigned_inventory_type'] = 'boarding';

						$this->db->update('boarding_schedule',array('is_booked'=>1),array('start<'=>$end_date,'end >'=>$start_date,'inventory_id'=>$result['id']));
						//echo $this->db->last_query();
		    		}
		    		else
		    		{
		    			$insertArray['inventory'] = 0;
						$insertArray['inventory_type'] = 'playarea';
						$insertArray['overlapping_appointment'] = 1;
						$insertArray['assigned_inventory_id'] = 0;
						$insertArray['assigned_inventory_type'] = 'playarea';
		    		}
		    	}
	    	}

	    	$insertArray['status'] = 'Confirm';
	    	$insertArray['payment_method'] = 'offline';
	    	$insertArray['added_on'] =  date('Y-m-d H:i:s');
	    	$insertArray['updated_on'] =  date('Y-m-d H:i:s');
	    	$insertArray['transfer_status'] = 'pending';
	    	$insertArray['created_by'] =$this->session->userdata('first_name');

	        $insertArray['discount_amount'] = 0;
	        $insertArray['discount_id'] = 0;

	        if(empty($insertArray['deposit'])){
	    		$insertArray['deposit'] = 0 ;
	    	}

	    	if(isset($insertArray['id']))
	    		unset($insertArray['id']);

	    	if($insertArray['id'] = singleInsert('boardings',$insertArray))
	    	{	
				$insert = array();
				$insert['category'] = $insertArray['category'];
				$insert['shop_id'] = $this->session->userdata('shop_id');
				$insert['amount'] = $insertArray['amount'];
				$insert['reference_id'] = $insertArray['id'];
				$insert['added_on'] = date('Y-m-d H:i:s');
				$insert['status'] = 'pending';
				$insert['transcation_type']='payment';
				$insert['app_user_id'] = $insertArray['app_user_id'];
				$insert['payment_mode'] = 'offline';
				$insert['ref_app_id'] = 0;
				$insert['invoice_id'] = $insertArray['id'];
				$insert['start'] = date('Y-m-d H:i:s',strtotime($insertArray['start_date'].' '.$insertArray['start_time']));

		   		if($category == 'daycare')
		   			$insert['end'] = date('Y-m-d H:i:s',strtotime($insertArray['start_date'].' '.$insertArray['end_time']));
		   		else
		   			$insert['end'] = date('Y-m-d H:i:s',strtotime($insertArray['end_date'].' '.$insertArray['end_time']));

		   		$insert['timezone'] = get_option('time_zone');
		   		$insert['cancel_by_id'] = 0;
		   		$insert['pet_id'] = $pets['pet_id'];
		   		if($postArray['deposit'] > 0){
			   		$insert['amount'] = $insert['amount'] - $postArray['deposit'];
			   		$insert['type'] = 'balance';
			   	}

				$this->db2->insert('transactions',$insert);

				if($postArray['deposit'] > 0){
	    			$insert['amount'] = $postArray['deposit'];
	    			$insert['type'] = 'deposit';
	    			$insert['payment_source'] = $postArray['payment_source'];
	    			$insert['other_source'] = $postArray['other_source'];
	    			$insert['status'] = 'success';
	    			$this->db2->insert('transactions',$insert);
	    		}
				
				$AppUserDetails = $this->getSingleUserData($postArray['app_user_id']);

				if($insert['category'] == 'boarding'){
					$endMsgLine = date(get_option('date_format'), strtotime($insertArray['end_date'])).' '.date('h:i a', strtotime($insertArray['end_time'])).'<br>';
					$title = 'Boarding Request Confirmed';
					$PushMessage = 'Your '.ucfirst($insertArray['category']).' Request for your pet '.$petDetails['name'].' has been confirmed.';
				}
				else
				{
					$endMsgLine = date(get_option('date_format'), strtotime($insertArray['start_date'])).' '.date('h:i a', strtotime($insertArray['end_time'])).'<br>';
					$title = 'Daycare Request Confirmed';
					$PushMessage = 'Your '.ucfirst($insertArray['category']).' Request for your pet '.$petDetails['name'].' has been confirmed.';
				}

				//pr($AppUserDetails);
				if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
				{
					include_once(APPPATH."libraries/PushNotifications.php");
					$FCMPUSH = new PushNotifications();

					if(!empty($petDetails['pet_cover_photo']))
						$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($insertArray['id'],'boarding',$this->session->userdata('shop_id')));
					else
						$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($insertArray['id'],'boarding',$this->session->userdata('shop_id')));
					

					$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));
		            $data = array('body'=>$PushMessage,'title'=>$title,'badge'=>($badge+1),'data'=>$dataArray);
		            
		            $notifications = array();
		            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
		            $notifications['pet_id'] = $pets['pet_id'];
		            $notifications['title'] = $data['title'];
		            $notifications['body'] = $data['body'];
		            $notifications['data'] = serialize($dataArray);
		            $notifications['added_on'] = date('Y-m-d H:i:s');
		            $notifications['shop_id'] = $this->session->userdata('shop_id');
		            $notifications['notification_type'] = 'appointments';
		            $data['data']['notification'] =  $notifications;
		            $this->db2->insert('notifications',$notifications);
		            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
		            //pr($notifications);
				}

					$tempDate = $insertArray['end_date'];
					if($insertArray['category'] == 'daycare')
						$tempDate = $insertArray['start_date'];

				if(!empty($insertArray['email'])){
					$emailContent = array('subject' => 'Your '.ucfirst($insertArray['category']).' Request has been confirmed successfully - '.PROJECTNAME.' ','message' =>'
		                Hello '.$AppUserDetails[0]['username'].',<br><br>
		                
		                Your '.ucfirst($insertArray['category']).' Request for your pet '.$petDetails['name'].' has been confirmed. 
		                <br><br>
						 Check In Date : '.date(get_option('date_format'), strtotime($insertArray['start_date'])).'<br>
						Check In Time : '.date('h:i a', strtotime($insertArray['start_time'])).'<br>
						Check Out Date : '.date(get_option('date_format'), strtotime($tempDate)).'<br>
						Check Out Time : '.date('h:i a', strtotime($insertArray['end_time'])).'<br>
						                   
		                <br><br>
		                If you are having any issues, please contact: 	
		               	<br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>
		                
		                Thanks,<br><br>
		               	
		               	Team @'.$shop_Data['shopname'].'<br><br>
	                    
	                    '
	                    );
			    	email($insertArray['email'], $emailContent,$shop_Data['id']);
			    }
			}
		}
		return true;
	}	

	function getUserDiscountData($app_user_id = 0,$appointment_date = '',$allow_type = 'boarding'){
       $appointment_date = date('Y-m-d',strtotime($appointment_date));
       //$appointment_date = date('Y-m-d');
       $this->db->select();
       $this->db->from('discount_users dusers');
       $this->db->join('discounts d',"d.id = dusers.discount_id");
       //$this->db->where(array('dusers.app_user_id' => $app_user_id);
       $this->db->like('allow_type', $allow_type);
       $this->db->where(array('dusers.app_user_id' => $app_user_id,'d.is_deleted' => '0'));
      $this->db->where(array('d.start_date_time <=' => $appointment_date,'d.end_date_time >=' => $appointment_date));

       //$this->db->where('date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
       $result = $this->db->get()->result_array();
       return $result;
   }


	function getTotalboardingForChecking($pet_id,$start_time,$end_time,$start_date,$end_date,$status,$category)
	{

		if($category == 'boarding')
		{

		 	return $this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$end_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (status = "'.$status.'" ) and category = "'.$category.'"')->result_array();

		}
		else
		{
			return $this->db->query('SELECT * FROM boardings WHERE STR_TO_DATE(CONCAT("'.$start_date.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(end_date, " ", end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$start_date.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(start_date, " ", start_time), "%Y-%m-%d %H:%i:%s") and  (status = "'.$status.'" ) and category = "'.$category.'"')->result_array();
		} 
	}

	function getBoardingDayCareData($id)
	{
		$result = $this->db->get_where('boardings',array('id'=>$id))->row_array();
		if(!empty($result))
		{
			$result = daycareBoardingTimeConverter($result);

			$result['petName'] = $this->Management_model->getPetName($result['pet_id']);
            $result['username'] = $this->Management_model->getUsername($result['app_user_id']);
            $result['inventoryName'] = $this->Management_model->getBoardingInevntoryName($result['assigned_inventory_id'],$result['assigned_inventory_type']);
            $result['spayed'] = '';
            $result['description'] = '';
           $data = $this->getPetIsSpayed($result['pet_id']);
           if(!empty($data)){
               $result['spayed'] = $data['spayed'];
               $result['description'] = $data['description'];
           }
		}
		return $result;
	}

	function getDropDownForBoarding($id)
	{
		$boarding = $this->getBoardingDayCareData($id);

		return $this->db->query("Select boarding_inventory.* FROM boarding_inventory where boarding_inventory.is_deleted = 0 and boarding_inventory.is_blocked = 0 and id not in (SELECT boarding_inventory.id FROM boardings join boarding_inventory on boardings.inventory = boarding_inventory.id WHERE STR_TO_DATE(CONCAT('".$boarding['start_date']."', ' ','".$boarding['start_time']."'), '%Y-%m-%d %H:%i:%s') < STR_TO_DATE(CONCAT(end_date, ' ', end_time), '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(CONCAT('".$boarding['end_date']."', ' ','".$boarding['end_time']."'), '%Y-%m-%d %H:%i:%s') > STR_TO_DATE(CONCAT(start_date, ' ', start_time), '%Y-%m-%d %H:%i:%s') and boardings.status != 'Cancel')")->result_array();
	}

	function assignedInventory($post,$id)
	{

		$appointmentDetails = $this->getBoardingDayCareData($id);
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));

		if($appointmentDetails['category'] = 'boarding')
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).' - '.date(get_option('date_format'), strtotime($appointmentDetails['end_date'])).'<br>';
		else
			$date =  date(get_option('date_format'), strtotime($appointmentDetails['start_date'])).'<br>';

		$time = date('h:i a', strtotime($appointmentDetails['start_time'])).' - '.date('h:i a', strtotime($appointmentDetails['end_time'])).'<br><br>';
		
		if($appointmentDetails['status'] == 'Pending')
		{
			include_once(APPPATH."libraries/PushNotifications.php");
			
			if(!empty($petDetails['pet_cover_photo']))
				$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));
			else
				$dataArray = array('image'=>'','result'=>$this->getAppointmentForNotification($appointmentDetails['id'],'boarding',$this->session->userdata('shop_id')));
			
			$badge = $this->getBagdeCountForUser($AppUserDetails[0]['id'],$this->session->userdata('shop_id'));

			//Send Mail If Appointment is confirmed
			$emailContent = array('subject' => 'Your '.$appointmentDetails['category'].' request has been confirmed successfully - '.PROJECTNAME.' ','message' =>'
	                    Hello '.$appointmentDetails['username'].',<br><br>
	                    
	                    Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been confirmed. 
	                    <br><br>
						 Date : '.$date.'
						 Time : '.$time.'
						
						Total '.$appointmentDetails['category'].' Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($appointmentDetails['amount']).'<br>
						                   
	                    <br><br>
	                    
	                   	If you are having any issues, please contact: 	

	                    <br><br>
	                    '.$shop_Data['shopname'].'<br>
	                    '.$shop_Data['address'].'<br>
	                    '.$shop_Data['phone_number'].'<br>
	                    '.get_option('shop_email').'<br>
	                    <br><br>

	                    Thanks, <br><br>
	                    Team @'.$shop_Data['shopname'].'<br><br>
	               
	                    
	                    '
	                    );
			email($appointmentDetails['email'], $emailContent,$shop_Data['id']);

			if(!empty($AppUserDetails[0]['fcm_id'] && $this->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
			{
				
				$FCMPUSH = new PushNotifications();
	            $data = array('body'=>' Your '.$appointmentDetails['category'].' request for your pet '.$petDetails['name'].' has been confirmed','title'=>$appointmentDetails['category'].' Request Confirmed','badge'=>($badge+1),'data'=>$dataArray);
	            
	            $notifications = array();
	            $notifications['app_user_id'] = $AppUserDetails[0]['id'];
	            $notifications['pet_id'] = $petDetails['id'];
	            $notifications['title'] = $data['title'];
	            $notifications['body'] = $data['body'];
	            $notifications['data'] = serialize($dataArray);
	            $notifications['added_on'] = date('Y-m-d H:i:s');
	            $notifications['shop_id'] = $this->session->userdata('shop_id');
	            $notifications['notification_type'] = 'appointments';
	            $data['data']['notification'] =  $notifications;
	            $this->db2->insert('notifications',$notifications);
	            $FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
			}
		}

		return $this->db->update('boardings',array('inventory'=>$post['inventory']),array('id'=>$id));
	}

	function getCurrentInventoryForDay($date)
	{
		$return = array();
		$result = $this->db->query("SELECT boarding_inventory.id as inventoryId,boardings.* ,boardings.id as boardingId FROM boardings join boarding_inventory on boardings.inventory = boarding_inventory.id WHERE STR_TO_DATE('".$date."', '%Y-%m-%d') <= STR_TO_DATE(end_date, '%Y-%m-%d ') AND STR_TO_DATE('".$date."', '%Y-%m-%d') >= STR_TO_DATE(start_date, '%Y-%m-%d') and boardings.status != 'Cancel'")->result_array();
	
		$inventory = $this->getBoardingInventory();

		if(!empty($inventory))
		{
			foreach ($inventory as $key => $value) {
				$temp = array();
				$temp = $value;
				$check = $this->search($result,$value['id'],'inventory',$date);
				if(!empty($check))
					$temp['boarding'] = $check;
				else
					$temp['boarding'] = array();
				//pr($temp);	
				array_push($return,$temp);
			}
		}
		return $return;
	}

	function search($array,$feild,$column_name,$date)
	{	

		$temp =array();
		if(!empty($array))
		{
		    foreach ($array as $data => $value)
		    {
		        if ($value[$column_name] == $feild){
		        	unset($value['id']);
		        	
		        	$start_date = date('Y-m-d',strtotime($value['start_date']));
		        	$end_date = date('Y-m-d',strtotime($value['end_date']));
		        	$start_time = $value['start_time'];
		        	$end_time = $value['end_time'];
		        	$date = date('Y-m-d',strtotime($date));
		        	//echo $date."<br>"; echo $start_date."<br>"; echo $end_date."<br>";
		        	if(strtotime($date) == strtotime($start_date) || strtotime($date) == strtotime($end_date))
		        	{
		        		$value['wholeDay'] = 0;

		        		if(strtotime($date) == strtotime($start_date) && strtotime($date) != strtotime($end_date)){
		        			$value['timings'] = date('h:i a',strtotime($start_time)).'-'. date('h:i a',strtotime('12 a.m'));			
		        		}
		        		else if(strtotime($date) == strtotime($end_date) && strtotime($date) != strtotime($start_date)){
		        			$value['timings'] = date('h:i a',strtotime('12 a.m'))." - ".date('h:i a',strtotime($end_time));			
		        		}

		        		else if(strtotime($date) == strtotime($end_date) && strtotime($date) == strtotime($start_date))
		        			$value['timings'] = date('h:i a',strtotime($start_time)).'-'. date('h:i a',strtotime($end_time));	
		        	}
		        	else
		        	{
		        		$value['wholeDay'] = 1;
		        	}

		        	$value['petDetails'] = $this->Entry_model->getPetsViewDetails($value['pet_id']);
		        	$value['url'] = base_url().'Admin/viewBoarding/'.get_encode($value['boardingId']);
		        	array_push($temp,$value);
		        }	
		    }
		    return $temp;
		}    
	}

	function getBoardingDetailsForAssignment($id,$date)
	{
		$result = $this->db->query("SELECT boardings.* FROM boardings  WHERE STR_TO_DATE('".$date."', '%Y-%m-%d') <= STR_TO_DATE(end_date, '%Y-%m-%d ') AND STR_TO_DATE('".$date."', '%Y-%m-%d') >= STR_TO_DATE(start_date, '%Y-%m-%d') and boardings.status != 'Cancel' and boardings.inventory=0")->result_array();
		$return  = array();
		//pr($result);
		foreach ($result as $key => $value) {
			$result2 = $this->getBoardingByInventoryIdAndDate($value['start_date'],$value['end_date'],$id);
			//echo $this->db->last_query();
			//pr($result2);
			if(!empty($result2))
			{	
				
				foreach ($result2 as $k => $v) {
					//echo $value['start_date']."<br>"; echo  $v['end_date']."<br>"; echo $value['end_date']."<br>"; echo $v['start_date']."<br>";
					if($v['start_date'] <= $value['end_date'] && $v['end_date'] >= $value['start_date'])
					{
						//Overlapping Exists
						//echo "Overlapping";
					}
					else
					{
						$value['petDetails'] = $this->Entry_model->getPetsViewDetails($value['pet_id']);
						$value['startDate'] =  date(get_option('date_format'), strtotime($value['start_date'])).' '.date('h:i a', strtotime($value['start_time']));
						$value['endDates'] =  date(get_option('date_format'), strtotime($value['end_date'])).' '.date('h:i a', strtotime($value['end_time']));
						$value['customerName'] = $this->Management_model->getUsername($value['app_user_id']);
						array_push($return,$value);
					}	
				}
			}
			else
			{
				$value['petDetails'] = $this->Entry_model->getPetsViewDetails($value['pet_id']);
				$value['startDate'] =  date(get_option('date_format'), strtotime($value['start_date'])).' '.date('h:i a', strtotime($value['start_time']));
				$value['endDate'] =  date(get_option('date_format'), strtotime($value['end_date'])).' '.date('h:i a', strtotime($value['end_time']));
				$value['customerName'] = $this->Management_model->getUsername($value['app_user_id']);
				array_push($return,$value);
			}

		}
		return $return;

	}


	function getBoardingByInventoryIdAndDate($start_date,$end_date,$inventoryId)
	{
		return $this->db->query("SELECT boardings.* FROM boardings join boarding_inventory on boardings.inventory = boarding_inventory.id WHERE STR_TO_DATE('".$start_date."', '%Y-%m-%d') < STR_TO_DATE(CONCAT(end_date, ' ', end_time), '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('".$end_date."', '%Y-%m-%d') > STR_TO_DATE(CONCAT(start_date, ' ', start_time), '%Y-%m-%d %H:%i:%s') and boardings.status != 'Cancel' and boardings.inventory=".$inventoryId)->result_array();
	}

	function deleteGalleryImage($id)
	{
		$gallery = $this->getGalleryImage($id);
		
		if($gallery['category'] == 'appointment')
			$appointment = $this->Entry_model->getSingleAppointmentId($gallery['reference_id']);
		else
			$appointment = $this->getBoardingDayCareData($gallery['reference_id']);

		if(!empty($gallery['image'])){

			if(!empty($appointment))
			{
				$pet =  $this->Entry_model->getPetsViewDetails($appointment['pet_id']);
				if($pet['pet_cover_photo'] == $gallery['image'])
				{
					$this->db2->update('pets',array('pet_cover_photo'=>''),array('id'=>$pet['id']));
				}
			}

        	//Check if user has already added image image,then delete

            // if(file_exists(FCPATH.UPLOAD_PETS.$gallery['image']))
            //     unlink(FCPATH.UPLOAD_PETS.$gallery['image']);
                        
            // if(file_exists(FCPATH.UPLOAD_PETS.'large'.'/'.$gallery['image']))
            //     unlink(FCPATH.UPLOAD_PETS.'large'.'/'.$gallery['image']);
            
            // if(file_exists(FCPATH.UPLOAD_PETS.'medium'.'/'.$gallery['image']))
            //     unlink(FCPATH.UPLOAD_PETS.'medium'.'/'.$gallery['image']);

            // if(file_exists(FCPATH.UPLOAD_PETS.'small'.'/'.$gallery['image']))
            //     unlink(FCPATH.UPLOAD_PETS.'small'.'/'.$gallery['image']);

            // if(file_exists(FCPATH.UPLOAD_PETS.'xlarge'.'/'.$gallery['image']))
            //     unlink(FCPATH.UPLOAD_PETS.'xlarge'.'/'.$gallery['image']);

          //   if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$gallery['image']))
        		// unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$gallery['image']);


			if(!empty($gallery['image']))
            {
			    $this->cloud_image_delete(PETS_IMAGES.$gallery['image']);
            }
            if(!empty($gallery['image']))
            {
			    $this->cloud_image_delete(WatermarkImages.$gallery['image']);
            }

        	// if(file_exists("gs://".BUCKETNAME."/".WatermarkImages.$gallery['image']))
        	// 	unlink("gs://".BUCKETNAME."/".WatermarkImages.$gallery['image']);

        	
         } 

		return $this->db->delete('pet_appointment_images',array('id'=>$id));
	}


	function getGalleryImage($id)
	{
		return $this->db->get_where('pet_appointment_images',array('id'=>$id))->row_array();
	}

	function getAppointmentByGalleryId($id)
	{
		
	}

	function getBagdeCountForUser($id,$shop_id)
	{   
		//$this->db2 = $this->load->database('main', TRUE);
		$this->db2->select('count(*) as Count');
		$result = $this->db2->get_where('notifications',array('app_user_id'=>$id,'is_read'=>0,'shop_id'=>$shop_id))->row_array();
		return $result['Count'];
	}

	function checkIfAppUserBelongToSameShop($shop_id ,$app_user_id)
    {
        //$this->db2 = $this->load->database('main', TRUE);
        $shopUserCheck = $this->db2->get_where('app_users_shops',array('app_user_id'=>$app_user_id,'currently_active'=>1,'shop_id'=>$shop_id))->row_array();
        if(empty($shopUserCheck))
            return false;
        else
            return true;
    }

    function updateGroomerNotes($groomerNotes,$pet_id)
    {
    	$notes = $this->getPetGroomerNotes($pet_id);

    	if(empty($notes) && !empty($groomerNotes))
    		return $this->db2->insert('groomer_notes',array('notes' => $groomerNotes,'pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id'),'added_on'=>date('Y-m-d H:i:s'),'user_id'=>$this->session->userdata('id')));
    	else
    	{


    		if(!empty($groomerNotes))
    			return $this->db2->update('groomer_notes',array('notes' => $groomerNotes,'added_on'=>date('Y-m-d H:i:s'),'user_id'=>$this->session->userdata('id')),array('id'=>$notes['id']));
    		else 
    		{	
    			return $this->db2->delete('groomer_notes',array('id'=>$notes['id']));    			
    		}
    	}	
    }

    function updateMedicationNotes($groomerNotes,$pet_id)
    {
    	$notes = $this->getPetMedicationNotes($pet_id);

    	if(empty($notes) && !empty($groomerNotes))
    		return $this->db2->insert('medication_notes',array('notes' => $groomerNotes,'pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id'),'user_id'=>$this->session->userdata('id'),'added_on'=>date('Y-m-d H:i:s')));
    	else
    	{


    		if(!empty($groomerNotes))
    			return $this->db2->update('medication_notes',array('notes' => $groomerNotes,'added_on'=>date('Y-m-d H:i:s'),'user_id'=>$this->session->userdata('id')),array('id'=>$notes['id']));
    		else 
    		{	
    			return $this->db2->delete('medication_notes',array('id'=>$notes['id']));    			
    		}
    	}	
    }





    function updatePetBehavior($petBehavior,$behavior_note,$pet_id,$user_id)
    {
		$behavior = $this->getPetBehavior($pet_id);
		// pr($behavior);exit;
		if(empty($petBehavior))
		     $petBehavior = 0;


		if(empty($behavior_note))
		     $behavior_note = '';


		$date = date('Y-m-d H:i:s');

		if(empty($behavior)){
		  return $this->db2->insert('pet_behavior',array('behavior_id' => $petBehavior,'behavior_note' => $behavior_note,'pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id'),'added_on'=>$date,'user_id'=>$user_id));
		}
		else
		{
		 return $this->db2->update('pet_behavior',array('behavior_id' => $petBehavior,'behavior_note' => $behavior_note,'added_on'=>$date,'user_id'=>$user_id),array('id'=>$behavior['id']));
		}
    }

    function getPetGroomerNotes($pet_id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	$return = array();
    	$return= $this->db2->get_where('groomer_notes',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->row_array();
    	if(!empty($return))
    	{
    		if(!empty($return['user_id']))
    		{
    			$return['username'] = $this->Management_model->getGroomerName($return['user_id']);
    		}
    	}

    	return $return;
    }

    function getPetGroomerNotesNew($pet_id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	$return = array();
    	$return= $this->db2->order_by('id','DESC')->get_where('groomer_notes',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->result_array();
    	// pr($return);exit;
    	if(!empty($return))
	    {
    		foreach ($return as $key => $value) {
    			// pr($value);exit;
	    		if(!empty($value['user_id']))
	    		{
	    			$return[$key]['username'] = $this->Management_model->getGroomerName($value['user_id']);

	    		}
	    	}
    	}
    	// pr($return);exit;
    	return $return;
    }

    function getVeterinarian($pet_id){
    	$return = array();
		$return = $this->db2->get_where('veterinarian',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->row_array();
		// echo $this->db2->last_query();exit;
    	return $return;
    }

    function getAppuserNew($id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	$return = array();
    	$return= $this->db2->order_by('id','DESC')->get_where('appuser_notes',array('app_user_id'=>$id,'shop_id'=>$this->session->userdata('shop_id')))->result_array();

    	if(!empty($return))
	    {
    		foreach ($return as $key => $value) {
    			//pr($value);exit;
	    		if(!empty($value['user_id']))
	    		{
	    			$return[$key]['username'] = $this->Management_model->getGroomerName($value['user_id']);

	    		}
	    	}
    	}
    	return $return;
    }

    function getPetMedicationNotesNew($pet_id)
    {
    	$return = array();
    	$return= $this->db2->order_by('id','DESC')->get_where('medication_notes',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->result_array();
    	if(!empty($return))
    	{
    		foreach ($return as $key => $value) {
	    		if(!empty($value['user_id']))
	    		{
	    			$return[$key]['username'] = $this->Management_model->getGroomerName($value['user_id']);
	    		}
	    	}
    	}

    	return $return;
    }

    function getPetMedicationNotes($pet_id)
    {
    	$return = array();
    	$return= $this->db2->get_where('medication_notes',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->row_array();
    	if(!empty($return))
    	{
    		if(!empty($return['user_id']))
    		{
    			$return['username'] = $this->Management_model->getGroomerName($return['user_id']);
    		}
    	}

    	return $return;
    }


    function getPetBehavior($pet_id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	return $this->db2->get_where('pet_behavior',array('pet_id'=>$pet_id,'shop_id'=>$this->session->userdata('shop_id')))->row_array();
    }

     function getPetIsSpayed($pet_id=0){
       return $this->db2->select('spayed,description')->get_where('pets',array('id'=>$pet_id))->row_array();
   }

    function getPetBehaviorWithEmoji($pet_id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	$this->db2->select('behaviors.*');
    	$this->db2->join('behaviors','behaviors.id = pet_behavior.behavior_id');
    	return $this->db2->get_where('pet_behavior',array('pet_behavior.pet_id'=>$pet_id,'pet_behavior.shop_id'=>$this->session->userdata('shop_id')))->row_array();
    }

    function getShopNoByid()
    {
    	//$this->db2 = $this->load->database('main', TRUE);
    	return $this->db2->get_where('shops',array('id'=>$id))->row_array();
    }

    function getAppointmentImagesById($id,$category)
	{
		if($category == 'appointment')
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id,'category'=>$category))->result_array();
		else
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id,'category != '=>'appointment'))->result_array();
	}

	function getAllAppointmentsServicesByAppointmentId($id)
	{
		
		//get All services for appointment by id
		$this->db->select('services.*');
		$this->db->join('services','services.id = appointment_services.service_id');
		return $this->db->get_where('appointment_services',array('appointment_id'=>$id))->result_array();
	}

	function getPetDetails($user_id,$shop_id,$postarray)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		
		//$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		// $this->db2->select('pets.*,breeds.name as bread');
		$this->db2->where(array('pets.id'=> $postarray['id']));
		// $this->db2->join('breeds','pets.breed_id = breeds.id');
		$query = $this->db2->get('pets')->row_array();

	
		if(!empty($query))
		{
			$query['bread'] = $this->Appuser_model->getbreedName($query['breed_id']);
			$query['notes'] = $this->getAllPetNotes($query['id'],$shop_id);
		}

		return $query;
	}

	function getAllPetNotes($id,$shop_id)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$this->db2->select('pets_notes.*,DATE_FORMAT(date_of, "%Y-%m-%dT%TZ") AS date_of_formatted , DATE_FORMAT(expiry_on, "%Y-%m-%dT%TZ") AS expiry_on_formatted');
		$result=  $this->db2->get_where('pets_notes',array('pet_id'=>$id,'shop_id'=>$shop_id))->result_array();
		$return = array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = $value;
				if($value['added_by'] == 'customer')
				{
					$temp['username'] = $this->Management_model->getUsername($value['added_by_user']);
				}
				else
				{
					$temp['username'] = $this->Management_model->getGroomerName($value['added_by_user']);
				}
				array_push($return,$temp);
			}

			
		}
		return $return;
	}



    function getAppointmentForNotification($id,$category,$shop_id)
    {
    	if($category == 'appointment')
    	{
    		//$shop = $this->getShopNoByid($shop_id);
    		$returnArray = array();
			$return = array();
			$result = $this->db->get_where('appointments',array('id'=>$id))->row_array();
			//echo $this->db->last_query();
			if(!empty($result))
			{
					$temp = array();
					$temp = $result;
					$check = $this->getAppointmentImagesById($result['id'],'appointment');
					
					if(!empty($check))
						$temp['showGallery'] = 1;
					else
						$temp['showGallery'] = 0;

					$temp['category'] = 'appointment';
					$services_arr = $this->getAllAppointmentsServicesByAppointmentId($result['id']);

					if($result['add_on_service_cost'] > 0)
					{
						$temp['add_on_services'] = $this->Appuser_model->getAllAppointmentsAddOnServicesByAppointmentId($result
							['id']);
						$temp['services'] =    array_merge($services_arr,$temp['add_on_services']);
					}else{
						$temp['services'] = $services_arr;
					}
					
					$temp['payments'] = $this->Appuser_model->getPaymentsDetails($result['id'],'appointment',$shop_id);

					$temp['petDetails'] = $this->getPetDetails($result['app_user_id'],$shop_id,array('id'=>$result['pet_id']));
					$latLongArray = $this->GlobalApp_model->getLangLong();
					$temp['lat'] = $latLongArray['lat'];
					$temp['long'] = $latLongArray['long'];			
				
			}
    	}
    	else
    	{
    		$return = array();
			$this->db->select('boardings.* , boardings.start_date as appointment_date');
			//$this->db->order_by('id','DESC');
			
			$result = $this->db->get_where('boardings',array('id'=>$id))->row_array();
			//echo $this->db->last_query();
			//pr($result);exit;
			if(!empty($result))
			{

					$temp = array();
					$temp = $result;

					$check = $this->getAppointmentImagesById($result['id'],'daycare/boarding');
					
					if(!empty($check))
						$temp['showGallery'] = 1;
					else
						$temp['showGallery'] = 0;

					$temp['payments'] = $this->Appuser_model->getPaymentsDetails($result['id'],$result['category'],$shop_id);




					$temp['services'] = array(array('name' => $result['category']));
					$temp['petDetails'] = $this->getPetDetails($result['app_user_id'],$shop_id,array('id'=>$result['pet_id']));
					$temp['appointment_date'] = $result['start_date'];
					$temp['appointment_end_date'] = $result['end_date'];
					$temp['appointment_time'] = $result['start_time'];
					$temp['appointment_end_time'] = $result['end_time'];
					$temp['cost'] = $result['amount'];
					$latLongArray = $this->GlobalApp_model->getLangLong();
					$temp['lat'] = $latLongArray['lat'];
					$temp['long'] = $latLongArray['long'];
		
				
			}
    	}
    	$temp['refund_cost'] = 0;
    	$refundResult = $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$shop_id,'transcation_type'=>'payout','status'=>'refunded'))->result_array();
		if (!empty($refundResult)) {
			foreach ($refundResult as $key => $value) {
				$temp['refund_cost'] = $temp['refund_cost'] + $value['amount'];
			}	
		}
    	return $temp;
    }

    function getPetForNotification($id,$shop_id)
    {
    	//$this->db2 = $this->load->database('main', TRUE);
		$return = array();
		//$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$this->db2->select('pets.*');
		$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		//$this->db2->join('breeds','pets.breed_id =breeds.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted'=>0,'pets.id'=>$id));
		
		$query = $this->db2->get('pets')->row_array();
		if(!empty($query))
		{
		
			$temp = array();
			$temp = $query;
			$temp['notes'] = $this->getAllPetNotes($query['id'],$shop_id);
			$temp['bread'] = $this->Appuser_model->getbreedName($query['breed_id']);
			 //All Pet Notes in an subarray
			array_push($return,$temp);
			
		}

		return $temp;
    }

    function getPetsHavingBirthdayToday()
    {
    	$return = array();
		//$this->db2 = $this->load->database('main', TRUE);
		
		$shop_id = $this->session->userdata('shop_id');
		$this->db2->select('pets.name,pets.pet_cover_photo,pets.id,app_users.username,app_users.email,app_users.phone,pets.type');
		$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted' => 0,'pets.is_deceased'=>0));
		$this->db2->where('DATE(pets.birth_date)',date('Y-m-d'));
		$query = $this->db2->get('pets')->result_array();

			
		return $query;
    }


    function getAppointmentsToday()
    {

    	//$statusArray = array('Pending','Confirm','Inprocess','Cancel','Complete','Checkout','All');
    	$statusArray = array('Pending','Confirm','Inprocess','Cancel','Complete','Checkout');

		$returnArray = array();
		$date1 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 00:00:00'));
		$date2 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 23:59:59'));
		
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');
		
        foreach ($statusArray as $k) {
        	# code...
	        $returnArray[$k] = array();
	        $this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
	        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
	        $this->db->join('services','services.id = appointment_services.service_id');

	       
	       	$this->db->where(array('appointments.status'=> $k));

			
	        $this->db->order_by('DATE(appointments.appointment_date)');
	        //$this->db->where('DATE(appointments.appointment_date)',$date);
	        if($k == 'Inprocess'){
	        	//$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(check_in_time, "%Y-%m-%d %H:%i:%s")');
	        	//$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(check_in_time, "%Y-%m-%d %H:%i:%s")');

	        }elseif($k == 'Checkout'){
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        }elseif($k == 'Cancel'){
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(added, "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(added, "%Y-%m-%d %H:%i:%s")');
	        }else{
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") <= STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") >= STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_end_time), "%Y-%m-%d %H:%i:%s")');
	        }
	        
			//$this->db->where('order_date <=', $second_date);
	        $this->db->where(array('appointments.isDuplicate'=> '0'));
	        $this->db->group_by('appointments.id');
	        $query = $this->db->get_where('appointments')->result_array();
	  		
	        if(!empty($query))
	        {
	            foreach ($query as $key => $value) {
	                
	                $temp = array();
	                $temp = groomingTimeConverter($value);

	                // $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
	                // $temp['username'] = $this->Global_model->getUsername($value['app_user_id']);
	                $temp['groomerName'] = $this->Management_model->getGroomerName($value['user_id']);
	                array_push($returnArray[$k],$temp);

	            }

	             //Global helper function to convert array as object.    

	        }

    	}
    	$returnArray['All'] = array();
    	foreach ($returnArray as $key => $value) {
    		if(!empty($value))
             	$returnArray['All'] = array_merge($returnArray['All'],$value);

        }
       
       
        return  $returnArray;
    }

    function getAppointmentCalendarView($date)
    {
    	$returnArray = array();
    	$this->db->select('appointments.* ,GROUP_CONCAT(services.name) as service,concat(appointments.appointment_date," ",appointments.appointment_time) as date');
        $this->db->join('appointment_services','appointments.id = appointment_services.appointment_id');
        $this->db->join('services','services.id = appointment_services.service_id');
       	//$this->db->where(array('appointments.status'=> $k));
		//$this->db->where('DATE(appointments.appointment_date) >=','CURDATE()',FALSE);
        $this->db->order_by('DATE(appointments.appointment_date)');
        $this->db->where(array('DATE(appointments.appointment_date)'=>$date));
        $this->db->group_by('appointments.id');
        $query = $this->db->get_where('appointments')->result_array();
        
	        if(!empty($query))
	        {
	            foreach ($query as $key => $value) {
	                if ($value['isDuplicate'] == '1') {
	        		continue;
	        		}
	                $temp = array();
	                $temp = groomingTimeConverter($value);

	                $temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
	                $temp['service'] = str_replace(",", ", ", $temp['service']);
	                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
	                $temp['groomerName'] = $this->Management_model->getGroomerName($value['user_id']);
	                array_push($returnArray,$temp);

	            }

	             //Global helper function to convert array as object.    
	        }
	    return $returnArray;    
    }


    function getBoardingCalendarView($date)
    {
    	$return = array();
    	$this->db->order_by('DATE(start_date)');
		
		$this->db->group_start();
		$this->db->where(array('DATE(start_date) <='=>$date));
		$this->db->where(array('DATE(end_date) >='=>$date));
		$this->db->group_end();
		$this->db->where(array('category'=>'boarding'));
		$result = $this->db->get_where('boardings')->result_array();
		///echo $this->db->last_query();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = daycareBoardingTimeConverter($value);

				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
				
				array_push($return,$temp);
			}
		}
		return $return;  
    }

    function getDaycareCalendarView($date)
    {
    	$return = array();
    	$this->db->order_by('DATE(start_date)');
		
		$this->db->group_start();
		$this->db->where(array('DATE(start_date) <='=>$date));
		$this->db->where(array('DATE(end_date) >='=>$date));
		$this->db->group_end();
		$this->db->where(array('category'=>'daycare'));
		$result = $this->db->get_where('boardings')->result_array();
		//echo $this->db->last_query();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = daycareBoardingTimeConverter($value);

				$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
				$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
				
				array_push($return,$temp);
			}
		}
		//pr($return);
		return $return;  
    }




    function getBoardingsToday()
    {
    	$statusArray = array('Confirm','Inprocess','Cancel','Checkout');

		$returnArray = array();
		$date1 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 00:00:00'));
		$date2 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 23:59:59'));
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');

        foreach ($statusArray as $k) {
	        $returnArray[$k] = array();	

			$this->db->order_by('DATE(start_date)');
			if($k == 'Inprocess'){
	       
	        }elseif($k == 'Checkout'){
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        }else{
	        	// $this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(boardings.start_date, " ", boardings.start_time), "%Y-%m-%d %H:%i:%s")');
	        	// $this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(boardings.start_date, " ", boardings.end_time), "%Y-%m-%d %H:%i:%s")');

	        	$this->db->where('DATE(start_date) <=',date('Y-m-d'));
				$this->db->where('DATE(end_date) >=',date('Y-m-d'));
	        }

	        //$this->db->where('DATE(start_date) <=',$date);
			//$this->db->where('DATE(end_date) >=',$date);

			$this->db->where(array('category'=>'boarding','boardings.status'=> $k));
			$result = $this->db->get_where('boardings')->result_array();
			// echo $this->db->last_query();exit;
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					$temp = array();
					$temp = daycareBoardingTimeConverter($value);

					$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
	                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
					$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
					
					array_push($returnArray[$k],$temp);
				}
			}
		}
		$returnArray['All'] = array();
    	foreach ($returnArray as $key => $value) {
    		if(!empty($value))
             	$returnArray['All'] = array_merge($returnArray['All'],$value);

        }
		return $returnArray;
    }

    function getDayCareToday()
    {
    	$statusArray = array('Confirm','Inprocess','Cancel','Checkout');

		$returnArray = array();
		$date1 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 00:00:00'));
		$date2 = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 23:59:59'));
		
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');
		
        foreach ($statusArray as $k) {
	        $returnArray[$k] = array();	

			$this->db->order_by('DATE(start_date)');
			//$this->db->group_start();
			if($k == 'Inprocess'){
	        	//$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(check_in_time, "%Y-%m-%d %H:%i:%s")');
	        	//$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(check_in_time, "%Y-%m-%d %H:%i:%s")');
	        }elseif($k == 'Checkout'){
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(check_out_time, "%Y-%m-%d %H:%i:%s")');
	        }elseif($k == 'Cancel'){
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(updated_on, "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(updated_on, "%Y-%m-%d %H:%i:%s")');
	        }else{
	        	$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(boardings.start_date, " ", boardings.start_time), "%Y-%m-%d %H:%i:%s")');
	        	$this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(boardings.start_date, " ", boardings.end_time), "%Y-%m-%d %H:%i:%s")');
	        }
			// $this->db->where('DATE(start_date) <=',$date);
			// $this->db->where('DATE(end_date) >=',$date);
			//$this->db->group_end();
			$this->db->where(array('category'=>'daycare','boardings.status'=> $k));
			$result = $this->db->get_where('boardings')->result_array();
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					$temp = array();
					$temp = daycareBoardingTimeConverter($value);

					$temp['petName'] = $this->Global_model->getPetName($value['pet_id']);
	                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
					$temp['inventoryName'] = $this->Global_model->getBoardingInevntoryName($value['inventory'],$value['inventory_type']);
					
					array_push($returnArray[$k],$temp);
				}
			}
		}
		
		$returnArray['All'] = array();
    	foreach ($returnArray as $key => $value) {
    		if(!empty($value))
             	$returnArray['All'] = array_merge($returnArray['All'],$value);

        }

		return $returnArray;
    }


    function checkServices()
    {
    	$result = $this->db->get_where('services',array('is_deleted'=>0))->result_array();
    	if(!empty($result))
    		return true;
    	return false;
    }

    function checkBankAccount()
    {
    	$result = $this->db->get_where('bank_accounts',array('is_deleted'=>0))->result_array();
    	if(!empty($result))
    		return true;
    	return false;
    }

    function checkVerifiedBankAccount()
    {
    	$result = $this->db->get_where('bank_accounts',array('is_deleted'=>0,'status'=>'verified'))->result_array();
    	if(!empty($result))
    		return true;
    	return false;
    }

    function checkIfServiceHasAppointment($id)
    {
    	$this->db->select('count(appointments.id) as count');
    	//$this->db->from('appointments');
    	$this->db->join('appointment_services','appointment_services.appointment_id=appointments.id');
    	$this->db->where_not_in('status',array('Cancel','Complete'));
    	$result = $this->db->get_where('appointments',array('appointment_services.service_id'=>$id))->row_array();

    	if(!empty($result))
    	{
    		if($result['count'] > 0)
    			return false;

    	}
    	
    	return true;
    }


    function checkIFShopIsBlocked($shopno)
	{
		//$this->db2 = $this->load->database('main', TRUE);

		$shop = $this->db2->get_where('shops',array('phone_number'=>$shopno))->row_array();

		if($shop['is_blocked'] == 1)
			return false;
		else
			return true;	
	
	}


	function getUserDefaultCard($id)
	{
		//$this->db2 = $this->load->database('main', TRUE);

		return $this->db2->get_where('app_users_cards',array('is_default'=>1,'app_user_id'=>$id))->row_array();
	}

	// function checkIFUserHasAccess($url)
	// {
	// 	$module = $this->session->userdata('user_module');
	// 	if(!empty($module))
	// 	{
	// 		$modules = explode(',', $module);

	// 		if(in_array($url, $modules))
	// 			return true;
	// 	}

	// 	return false;


	// }

	function getShopTimingsByCategoryAndDate($category,$date)
	{
		$weekday = date('l',strtotime($date));
		if($category == 'appointment')
	    	return $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();
    	else
    		return $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();		
	}

	function getPaymentsDetails($id,$category)
	{
		// return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payment'))->row_array(); old sanjeev
		return $this->db2->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payment'))->row_array();
		// return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payment','is_add_on_payment'=>0))->row_array();
	}

	function getAdditionalPayment($id,$category)
	{
		// return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payment','is_add_on_payment'=>1))->row_array(); old sanjeev
		return $this->db2->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payment'))->row_array();
	}

	function getPayoutsDetails($id,$category)   
	{
		// return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payout'))->row_array(); old sanjeev
		return $this->db2->get_where('transactions',array('shop_id'=>$this->session->userdata('shop_id'),'transcation_type'=>'payout'))->row_array();
	}

	function getAddOnServices($id,$category)
	{
		$this->db->select('group_concat(services.name) as getAddOnServices');
		$this->db->join('appointment_addon_services','appointments.id=appointment_addon_services.appointment_id');
		$this->db->join('services','services.id=appointment_addon_services.service_id');
		$this->db->group_by('appointments.id');
		$r = $this->db->get_where('appointments',array('appointments.id'=>$id))->row_array();
		//pr($r);exit;
		$r['getAddOnServices'] = str_replace(',',', ',$r['getAddOnServices']);
		if(!empty($r))
			return $r['getAddOnServices'];
		else
			return '';	 
	}

	function getGroomerServices($id)
	{
		$this->db->select('groomer_services.*,services.name');
		$this->db->join('services','groomer_services.service_id=services.id');


		return $this->db->get_where('groomer_services',array('services.is_deleted'=>0,'services.is_package'=>0,'groomer_services.user_id'=>$id))->result_array();

		 // echo  $this->db->last_query();exit;
	}

	function addSchedule($postarray,$id,$admin_users)
	{
		$days = array(
			'',
		    'Monday',
		    'Tuesday',
		    'Wednesday',
		    'Thursday',
		    'Friday',
		    'Saturday',
		    'Sunday'
		);
		//echo $postarray['selectedDate'];
		$selectedDate = date('Y-m-d',strtotime($postarray['selectedDate']));
		
		$weekdayNo = date('N',strtotime($selectedDate));
		$weekDay = date('l');
		$start = date('H:i:s',strtotime($postarray['start_time']));
		$end = date('H:i:s',strtotime($postarray['end_time']));
		$temp = array();
		$temp['date'] = $selectedDate;
		$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
		$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));
		$temp['user_id'] = $id;
		$temp['added_on'] = date('Y-m-d H:i:s');
		$temp['created_by'] = $this->session->userdata('id');
		$t12 = $temp;

		$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$id))->result_array();
			// echo  $this->db->last_query();exit;
		if(!empty($postarray['weekday']))
		{
			foreach ($postarray['weekday'] as $key ) {

				if($key == $weekdayNo)
				{
					
					if(empty($temp))
						$temp = $t12;


					
					$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$selectedDate))->result_array();
					// pr($check);exit;
					
					if(!empty($check))
					{
						//check for than appointment
						
						//$x = 15;
						$x = get_option('stagger_time');
						$x = 0;
						$i = 0;

						
						foreach ($check as $a => $b) {
							$appointment_check = $this->db->get_where('appointments',array('user_id'=>$temp['user_id'],'Date(appointment_date)'=>$temp['date'],'status !='=>'Cancel','allocated_schedule_id'=>$b['id']))->result_array();
							if($i != 0)
							{
								$temp['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
							}

							
							if(empty($appointment_check))
							{
								$queryArr = $temp;
								$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
								$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
								$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
								//$queryArr['date'] = server_date($temp['date'],'Y-m-d');

								$this->db->update('groomer_schedule',$queryArr,array('id'=>$b['id']));
							}

							$i++;
						}
						
					}
					else
					{
						// pr($admin_users['upper_limit']);exit;
						// pr($temp);exit;
						if($admin_users['upper_limit'] == 1){
							$queryArr = $temp;
							$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
							$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
							$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
							//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
							singleInsert('groomer_schedule',$queryArr);
						}
						else
							{
								//$s = $temp['start'];
								//$x = 15;
								$x = get_option('stagger_time');
								$x = 0;
								for ($i=0; $i <$admin_users['upper_limit'] ; $i++) { 
									
									$temp2 = array();
									$temp2 = $temp;

									if($i != 0){
										$temp2['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
									}
									//pr($temp2);
									if(strtotime($temp2['start']) < strtotime($temp2['end'])){
										$queryArr = $temp2;
										$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
										$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
										$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
										//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
										singleInsert('groomer_schedule',$queryArr);
									}

									
								}
							}
					}

					// exit;
				}
				else 
				{
					$temp = array();
					$temp['created_by'] = $this->session->userdata('id');
					
					$newDate = date('Y-m-d',strtotime($days[$key].' this week',strtotime($selectedDate)));
					
					if($selectedDate < $newDate)
					{	
						$temp['date'] = $newDate;
						$temp['start'] = $newDate.' '.$start;
						$temp['end'] = $newDate.' '.$end;
						$temp['user_id'] = $id;
						$temp['added_on'] = date('Y-m-d H:i:s');
						
						
						$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$temp['date']))->result_array();
						
						if(!empty($check))
						{
							//check for than appointment
							
							//$x = 15;
							$x = get_option('stagger_time');
							$x = 0;
							$i = 0;

							foreach ($check as $a => $b) {
							$appointment_check = $this->db->get_where('appointments',array('user_id'=>$temp['user_id'],'Date(appointment_date)'=>$temp['date'],'status !='=>'Cancel','allocated_schedule_id'=>$b['id']))->result_array();
								if($i != 0)
								{
									$temp['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
								}

								if(empty($appointment_check))
								{
									$queryArr = $temp;
									$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
									$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
									$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
									//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
									$this->db->update('groomer_schedule',$queryArr,array('id'=>$b['id']));
								}

								$i++;
							}
						}
						else
						{

							if($admin_users['upper_limit'] == 1){
								$queryArr = $temp;
								$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
								$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
								$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
								//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
								singleInsert('groomer_schedule',$queryArr);
							}
							else
							{
								//$s = $temp['start'];
								//$x = 15;
								$x = get_option('stagger_time');
								$x = 0;
								for ($i=0; $i <$admin_users['upper_limit'] ; $i++) { 

									
									$temp2 = array();
									$temp2 = $temp;

									if($i != 0){
										$temp2['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
									}
									if(strtotime($temp2['start']) < strtotime($temp2['end'])){
										$queryArr = $temp2;
										$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
										$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
										$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
										//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
										singleInsert('groomer_schedule',$queryArr);
									}
									
								}
							}	
						}
					}	
				}		
			}
		}

	
		if(!empty($postarray['no_of_weeks']) && $postarray['no_of_weeks'] > 1)
		{
			 
			for ($i = 2; $i <= $postarray['no_of_weeks']; $i++) { 
				if($i == 2)
				{
					$firstDay = date('Y-m-d',strtotime($selectedDate.' next monday'));
					$firstNewDate = $firstDay;
					//$firstDay = $newDate;
					//echo $firstDay.' Next Monday'."<br>";
				}
				else
				{
					$days = (($i-2)*7);
					$firstDay = date('Y-m-d',strtotime($firstNewDate.' '.'+'.$days.' days'));
				}

				if(!empty($postarray['weekday']))						
				{
					foreach ($postarray['weekday'] as $key ) {
						
						if($key == 1)
							$tempDate = $firstDay;
						else
							$tempDate = date('Y-m-d',strtotime($firstDay) + (24*3600*($key-1).' days'));

						$temp = array();
						
						$temp['date'] = $tempDate;
						$temp['start'] = $tempDate.' '.$start;
						$temp['end'] = $tempDate.' '.$end;
						$temp['user_id'] = $id;
						$temp['added_on'] = date('Y-m-d H:i:s');
						$temp['created_by'] = $this->session->userdata('id');
						//pr($temp);
						$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'date'=>$temp['date']))->result_array();
						if(!empty($check))
						{
							//check for than appointment
							//$x = 15;
							$x = get_option('stagger_time');
							$x = 0;
							$j = 0;

							
							foreach ($check as $a => $b) {
							$appointment_check = $this->db->get_where('appointments',array('user_id'=>$temp['user_id'],'Date(appointment_date)'=>$temp['date'],'status !='=>'Cancel','allocated_schedule_id'=>$b['id']))->result_array();
								if($i != 0)
								{
									$temp['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
								}

								//pr($temp);pr($appointment_check);
								if(empty($appointment_check))
								{
									$queryArr = $temp;
									$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
									$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
									$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
									//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
									$this->db->update('groomer_schedule',$queryArr,array('id'=>$b['id']));
								}

								$j++;
							}	
						}
						else
						{
							if($admin_users['upper_limit'] == 1){
								$queryArr = $temp;
								$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
								$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
								$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
								//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
								singleInsert('groomer_schedule',$queryArr);
							}
							else
							{
								//$s = $temp['start'];
								//$x = 15;
								$x = get_option('stagger_time');
								$x = 0;
								for ($j=0; $j <$admin_users['upper_limit'] ; $j++) { 

									
									$temp2 = array();
									$temp2 = $temp;

									if($j != 0){
										$temp2['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$j).' minutes'));
									}

									if(strtotime($temp2['start']) < strtotime($temp2['end'])){
										$queryArr = $temp2;
										$queryArr['start'] = server_date($temp['start'],'Y-m-d H:i:s');
										$queryArr['end'] = server_date($temp['end'],'Y-m-d H:i:s');
										$queryArr['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
										//$queryArr['date'] = server_date($temp['date'],'Y-m-d');
										singleInsert('groomer_schedule',$queryArr);
									}
								}
							}
						}
					}	
				}
			}
		}
		//exit;
		return true;
	}




	public function addDaySchedule($postarray)
	{
		$days = array(
			'',
		    'Monday',
		    'Tuesday',
		    'Wednesday',
		    'Thursday',
		    'Friday',
		    'Saturday',
		    'Sunday'
		);
		$selectedDate = date('Y-m-d',strtotime($postarray['selectedDate']));
		
		$playarea = $this->db->get_where('playareas',array('is_deleted'=>0))->result_array();

		if(!empty($playarea))
		{
			foreach ($playarea as $a => $b) {
				$weekdayNo = date('N',strtotime($selectedDate));
				$weekDay = date('l');
				$start = date('H:i:s',strtotime($postarray['start_time']));
				$end = date('H:i:s',strtotime($postarray['end_time']));
				$temp = array();
				$temp['date'] = $selectedDate;
				$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
				$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));
				
				$temp['added_on'] = date('Y-m-d H:i:s');

				
				if(!empty($postarray['weekday']))
				{
					foreach ($postarray['weekday'] as $key ) {

						if($key == $weekdayNo)
						{
						    $check  = $this->db->get_where('daycare_schedule',array('Date(date)'=>$selectedDate,'playarea_id'=>$b['id']))->row_array();
		
							if(!empty($check))
							{
								//check for than appointment
								$appointment_check = $this->db->get_where('boardings',array('Date(start_date)'=>$temp['date'],'status !='=>'Cancel','category'=>'daycare','inventory'=>$b['id']))->result_array();
								if(empty($appointment_check))
								{
									$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
									$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
									$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
									$this->db->update('daycare_schedule',$temp,array('id'=>$check['id']));
								}
							}
							else
							{
								$temp['playarea_id'] = $b['id'];
								$temp['orginal_capacity'] = $b['capacity'];
								$temp['current_capacity'] = $b['capacity'];

								$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
								$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));
								$temp['added_on'] = date('Y-m-d H:i:s');

								$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
								$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
								$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');

								singleInsert('daycare_schedule',$temp);
							}
						}
						else 
						{
							$temp = array();
							
							$newDate = date('Y-m-d',strtotime($days[$key].' this week',strtotime($selectedDate)));
							if($selectedDate < $newDate)
							{	
								$temp['date'] = $newDate;
								$temp['start'] = $newDate.' '.$start;
								$temp['end'] = $newDate.' '.$end;
								
								$temp['added_on'] = date('Y-m-d H:i:s');
								$check  = $this->db->get_where('daycare_schedule',array('Date(date)'=>$temp['date'],'playarea_id'=>$b['id']))->row_array();
								if(!empty($check))
								{
									//check for than appointment
									$appointment_check = $this->db->get_where('boardings',array('Date(start_date)'=>$temp['date'],'status !='=>'Cancel','category'=>'daycare','inventory'=>$b['id']))->result_array();
									if(empty($appointment_check))
									{
										$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
										$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
										$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
										$this->db->update('daycare_schedule',$temp,array('id'=>$check['id']));
									}
								}
								else
								{
									$temp['playarea_id'] = $b['id'];
									$temp['orginal_capacity'] = $b['capacity'];
									$temp['current_capacity'] = $b['capacity'];

									$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
									$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
									$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');

									singleInsert('daycare_schedule',$temp);
								}
							}	
						}	
					}
				}
				
				if(!empty($postarray['no_of_weeks']) && $postarray['no_of_weeks'] > 1)
				{
					 
					for ($i = 2; $i <= $postarray['no_of_weeks']; $i++) { 
						if($i == 2)
						{
							$firstDay = date('Y-m-d',strtotime($selectedDate.' next monday'));
							$firstNewDate = $firstDay;
						}
						else
						{
							$days = (($i-2)*7);
							$firstDay = date('Y-m-d',strtotime($firstNewDate.' '.'+'.$days.' days'));
						}
						
						if(!empty($postarray['weekday']))
						{
							foreach ($postarray['weekday'] as $key ) {
								
								if($key == 1)
									$tempDate = $firstDay;
								else
									$tempDate = date('Y-m-d',strtotime($firstDay) + (24*3600*($key-1)));

								$temp = array();
								$temp['date'] = $tempDate;
								$temp['start'] = $tempDate.' '.$start;
								$temp['end'] = $tempDate.' '.$end;
								
								$temp['added_on'] = date('Y-m-d H:i:s');
								$check  = $this->db->get_where('daycare_schedule',array('Date(date)'=>$temp['date'],'playarea_id'=>$b['id']))->row_array();
								if(!empty($check))
								{
									//check for than appointment
									$appointment_check = $this->db->get_where('boardings',array('Date(start_date)'=>$temp['date'],'status !='=>'Cancel','category'=>'daycare','inventory'=>$b['id']))->result_array();
									if(empty($appointment_check))
									{
										$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
										$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
										$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
										$this->db->update('daycare_schedule',$temp,array('id'=>$check['id']));
									}
								}
								else
								{
									$temp['playarea_id'] = $b['id'];
									$temp['orginal_capacity'] = $b['capacity'];
									$temp['current_capacity'] = $b['capacity'];

									$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
									$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
									$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');

									singleInsert('daycare_schedule',$temp);	
								}
							}	
						}
					}
				}
			}
			return true;
		}
		return false;	
	}

	function addBoardingSchedule($postarray)
	{
		$days = array(
			'',
		    'Monday',
		    'Tuesday',
		    'Wednesday',
		    'Thursday',
		    'Friday',
		    'Saturday',
		    'Sunday'
		);
		
		$boarding_inventory = $this->db->get_where('boarding_inventory',array('is_deleted'=>0))->result_array();
		
		if(!empty($boarding_inventory))
		{
			foreach ($boarding_inventory as $a => $b) {

				$selectedDate = date('Y-m-d',strtotime($postarray['selectedDate']));
				$firstDate = date('Y-m-d',strtotime($postarray['selectedDate']));
			
				$weekdayNo = date('N',strtotime($firstDate));
				$weekDay = date('l');
				
				$temp = array();
				$temp['start'] = date('Y-m-d',strtotime($firstDate));
				$temp['end'] = date('Y-m-d',strtotime($firstDate.' '.' +1 day'));
				
				$temp['added_on'] = date('Y-m-d H:i:s');
			
				if(!empty($postarray['weekday']))
				{
					foreach ($postarray['weekday'] as $key ) {
						if($key == $weekdayNo)
						{
							$check  = $this->db->get_where('boarding_schedule',array('Date(start)'=>$firstDate,'inventory_id'=>$b['id']))->row_array();
			
							if(!empty($check))
							{
								
							}
							else
							{
								$temp['inventory_id'] = $b['id'];
								$temp['is_booked'] = 0;
								$temp['size'] = $b['size'];
								$temp['size_priority'] = $b['size_priority'];
								$temp['start'] = date('Y-m-d',strtotime($firstDate));
								$temp['end'] = date('Y-m-d',strtotime($firstDate.' '.' +1 day'));
								$temp['added_on'] = date('Y-m-d H:i:s');
								singleInsert('boarding_schedule',$temp);
							}
						}
						else 
						{
							$temp = array();
							$newDate = date('Y-m-d',strtotime($days[$key].' this week',strtotime($firstDate)));
							if($firstDate < $newDate)
							{	
								$temp['start'] = $newDate;
								$temp['end'] = date('Y-m-d',strtotime($newDate.' +1 day'));
								$temp['added_on'] = date('Y-m-d H:i:s');
								$check  = $this->db->get_where('boarding_schedule',array('Date(start)'=>$temp['start'],'inventory_id'=>$b['id']))->row_array();
								
								if(!empty($check))
								{

								}
								else
								{
									$temp['inventory_id'] = $b['id'];
									$temp['is_booked'] = 0;
									$temp['size'] = $b['size'];
									$temp['size_priority'] = $b['size_priority'];
									singleInsert('boarding_schedule',$temp);
								}
							}	
						}
					}
				}
				
			

				if(!empty($postarray['no_of_weeks']) && $postarray['no_of_weeks'] > 1)
				{
					 
					for ($i = 2; $i <= $postarray['no_of_weeks']; $i++) { 
						if($i == 2)
						{
							$firstDay = date('Y-m-d',strtotime($selectedDate.' next monday'));
							$firstNewDate = $firstDay;
						}
						else
						{
							$Multiplydays = (($i-2)*7);
							$firstDay = date('Y-m-d',strtotime($firstNewDate.' '.'+'.$Multiplydays.' days'));
						}
						if(!empty($postarray['weekday']))
						{
							foreach ($postarray['weekday'] as $key ) {
								
								if($key == 1)
									$tempDate = $firstDay;
								else
									$tempDate = date('Y-m-d',strtotime($firstDay) + (24*3600*($key-1)));

								$temp = array();
								
								$temp['start'] = $tempDate;
								$temp['end'] = date('Y-m-d',strtotime($tempDate.' +1 day'));
								
								$temp['added_on'] = date('Y-m-d H:i:s');
								$check  = $this->db->get_where('boarding_schedule',array('Date(start)'=>$temp['start'],'inventory_id'=>$b['id']))->row_array();
									
								if(!empty($check))
								{
									
								}
								else
								{
									$temp['inventory_id'] = $b['id'];
									$temp['is_booked'] = 0;
									$temp['size'] = $b['size'];
									$temp['size_priority'] = $b['size_priority'];
									singleInsert('boarding_schedule',$temp);
								}
							}		
						}
					}
				}
			}
			return true;
		}
		else
			return false;
		
	}

	function editSchedule($postarray,$id,$admin_users)
	{	
		$selectedDate = date('Y-m-d',strtotime($postarray['selectedDate']));
		$start = date('H:i:s',strtotime($postarray['start_time']));
		$end = date('H:i:s',strtotime($postarray['end_time']));
		$temp = array();
		$temp['date'] = $selectedDate;
		$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
		$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));

		$temp['user_id'] = $id;
		$temp['added_on'] = date('Y-m-d H:i:s');

		$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
		$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
		$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');

		//check if appointment exists for groomer or schedule is present
		$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$temp['date']))->result_array();
		$globalAppointmentCheck =  $this->db->get_where('appointments',array('user_id'=>$id,'Date(appointment_date)'=>$temp['date'],'status' != 'Cancel'))->result_array();
		if(!empty($check) && empty($globalAppointmentCheck))
		{

			// if(empty($globalAppointmentCheck))
			// {
			// 	$this->editGroomerBookingTime($groomer_services,$temp['date'],$temp['start'],$temp['end'],$temp['user_id'],$admin_users['upper_limit']);
			// }

			//check for than appointment
			$i = 0; 
			//$x = 15;
			$x = get_option('stagger_time');
			$x = 0;
			foreach ($check as $a => $b) {
			$appointment_check = $this->db->get_where('appointments',array('user_id'=>$temp['user_id'],'Date(appointment_date)'=>$temp['date'],'status !='=>'Cancel','allocated_schedule_id'=>$b['id']))->result_array();
				

				if($i != 0)
				{
					$temp['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
				}

				//pr($temp);pr($appointment_check);
				if(empty($appointment_check))
				{
					
					$this->db->update('groomer_schedule',$temp,array('id'=>$b['id']));
				}

				$i++;
			}

				
			return true;
			
		}
		else
			return false;

		
						
	}

	function editMultipleSchedule($data)
	{	
		$date_array = explode(",", $data['date']);
		if ($date_array) {
			foreach ($date_array as $date) {
				// pr($date);exit;
				$selectedDate = date('Y-m-d',strtotime($date));
				$start = date('H:i:s',strtotime($data['start_time']));
				$end = date('H:i:s',strtotime($data['end_time']));
				$temp = array();
				$temp['date'] = $selectedDate;
				$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
				$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));
				$id = $data['user_id'];
				$temp['user_id'] = $id;
				$temp['added_on'] = date('Y-m-d H:i:s');

				$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
				$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
				$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');

				//check if appointment exists for groomer or schedule is present
				$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$temp['date']))->result_array();

				if(!empty($check))
				{

					
					//check for than appointment
					$i = 0; 
					//$x = 15;
					$x = get_option('stagger_time');
					$x = 0;
					foreach ($check as $a => $b) {

						if($i != 0)
						{
							$temp['start'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.($x*$i).' minutes'));
						}

						//pr($temp);pr($appointment_check);
						$this->db->update('groomer_schedule',$temp,array('id'=>$b['id']));


						$i++;
					}
				}
				else{
					singleInsert('groomer_schedule',$temp);
				}
			}
			return true;
		}else{
			return false;
		}				
	}

	function editDayCareSchedule($postarray)
	{
		$selectedDate = date('Y-m-d',strtotime($postarray['selectedDate']));
		$start = date('H:i:s',strtotime($postarray['start_time']));
		$end = date('H:i:s',strtotime($postarray['end_time']));
		$temp = array();
		$temp['date'] = $selectedDate;
		$temp['start'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$start));
		$temp['end'] = date('Y-m-d H:i:s',strtotime($selectedDate.' '.$end));
		
		$temp['added_on'] = date('Y-m-d H:i:s');

		//check if appointment exists for groomer or schedule is present
		$check  = $this->db->get_where('daycare_schedule',array('Date(date)'=>$selectedDate))->row_array();
		if(!empty($check))
		{
			$appointment_check = $this->db->get_where('boardings',array('Date(start_date)'=>$temp['date'],'status !='=>'Cancel'))->result_array();
			if(empty($appointment_check))
			{
				$temp['start'] = server_date($temp['start'],'Y-m-d H:i:s');
				$temp['end'] = server_date($temp['end'],'Y-m-d H:i:s');
				$temp['added_on'] = server_date($temp['added_on'],'Y-m-d H:i:s');
				return $this->db->update('daycare_schedule',$temp,array('id'=>$check['id']));
			}
			else
			{
				return false;
			}
			
			
		}
		else
			return false;
	}




	function deleteDayCareSchedule($date)
	{
		$selectedDate = date('Y-m-d',strtotime($date));
		

		//check if appointment exists for groomer or schedule is present
		


		$check  = $this->db->get_where('daycare_schedule',array('Date(date)'=>$selectedDate))->row_array();
		if(!empty($check))
		{
			$appointment_check = $this->db->get_where('boardings',array('Date(start_date)'=>$selectedDate,'status !='=>'Cancel','category'=>'daycare'))->result_array();
			if(empty($appointment_check)){
				
				
				return $this->db->delete('daycare_schedule',array('Date(date)'=>$selectedDate));
			}
			else
			{
				return false;
			}		
		}
		return false;
	}


	function deleteBoardingSchedule($date)
	{
		$selectedDate = date('Y-m-d',strtotime($date));
		

		//check if appointment exists for groomer or schedule is present
		


		$check  = $this->db->get_where('boarding_schedule',array('Date(start)'=>$selectedDate))->row_array();
		if(!empty($check))
		{
			$appointment_check = $this->db->get_where('boardings',array('Date(start_date) <='=>$selectedDate,'Date(end_date) >'=>$selectedDate,'status !='=>'Cancel','category'=>'boarding'))->result_array();
			//pr($appointment_check);
			if(empty($appointment_check)){
				
				
				return $this->db->delete('boarding_schedule',array('Date(start)'=>$selectedDate));
			}
			else
			{
				return false;
			}		
		}
		return false;
	}
	function getTimeZone($zip_id){
		$check =  $this->db2->get_where('timezonebyzipcode',array('zip'=>$zip_id))->row_array();
		$previoustimeZone = get_option('time_zone');
		
		if(!empty($check) && strtolower($previoustimeZone) != strtolower($check['timezone'])){
			$timezones = $this->Settings_model->timezones();
			foreach ($timezones as $key => $value) {
				if(strtolower($key) == strtolower($check['timezone'])){
					return $check;
				}
			}	
		}
		return array();
	}

	function deleteSchedule($date,$id,$admin_users)
	{
		$selectedDate = date('Y-m-d',strtotime($date));
		

		//check if appointment exists for groomer or schedule is present
		


		$check  = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$selectedDate))->result_array();
		$globalAppointmentCheck =  $this->db->get_where('appointments',array('user_id'=>$id,'Date(appointment_date)'=>$temp['date'],'status' != 'Cancel'))->result_array();
		if(!empty($check) && empty($globalAppointmentCheck))
		{
			$appointment_check = $this->db->get_where('appointments',array('user_id'=>$check['user_id'],'Date(appointment_date)'=>$check['date'],'status !='=>'Cancel','allocated_schedule_id'=>$b['id']))->result_array();
			if(empty($appointment_check)){
				
				//$this->db->delete('groomer_bookings',array('date'=>$selectedDate,'user_id'=>$check['id']));
				$this->db->delete('groomer_schedule_bookings',array('user_id'=>$id,'Date(date)'=>$selectedDate));
				return $this->db->delete('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$selectedDate));
			}		
		}
		return false;
	}

	function getGroomerSchedule($id)
	{
		$return = array();
		$currentDate = date('Y-m-d');
		$this->db->where('date >=',$currentDate);
		$result = $this->db->order_by('date','ASC')->get_where('groomer_schedule',array('user_id'=>$id))->result_array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				
				$temp = groomer_scheduleTimeConverter($temp);
				
				
				//$temp['date'] = user_date($value['date'],'Y-m-d');

				$maxPetMultiplier = intval($this->getMaxMulitplierByUserIdAndServices($id));
				//echo $maxPetMultiplier."<br>";
				if($maxPetMultiplier > 1)
					$timeAvailable = (((strtotime($value['end']) - strtotime($value['start']))/60)*$maxPetMultiplier);
				else
					$timeAvailable = ((strtotime($value['end']) - strtotime($value['start']))/60);
				
				$temp['working_hrs'] = $timeAvailable; 
				$Appointments = $this->db->get_where('appointments',array('appointment_date'=>$value['date'],'user_id'=>$id))->result_array();
				$temp['booking_time'] = 0;
				if(!empty($Appointments))
				{
					//pr($Appointments);
					$availableTime = $timeAvailable;
					foreach ($Appointments as $k => $v) {
						$availableTime -= ((strtotime($v['appointment_end_time']) - strtotime($v['appointment_time'])) / 60);
						$temp['booking_time'] += ((strtotime($v['appointment_end_time']) - strtotime($v['appointment_time'])) / 60);
					}
					$temp['available_time'] = 0;
					if($availableTime > $time_estimate){
						$temp['booking_status'] = 'yellow';
						$temp['available_time'] = $availableTime;
						
					}	
					else
						$temp['booking_status'] = 'red';

				}
				else
				{
					$temp['booking_status'] = 'green';
					$temp['available_time'] = $timeAvailable;
				}
				
				array_push($return,$temp);
			}
		}
		//pr($return);
		return $return;
	}



	function getdaycareSchedule()
	{

		$return = array();
		$date = date('Y-m-d');
		$this->db->select('sum(daycare_schedule.orginal_capacity) as orginal_capacity,sum(daycare_schedule.current_capacity) as current_capacity,daycare_schedule.start,daycare_schedule.end,daycare_schedule.date');
		$this->db->group_by('date(daycare_schedule.date)');
		$this->db->where('date >=',$date);
		$result = $this->db->get_where('daycare_schedule')->result_array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = daycare_scheduleTimeConverter($value);
					
				if($value['current_capacity'] != $value['orginal_capacity'])
				{
					if( $value['current_capacity'] <= 0){
						
						$temp['booking_status'] = 'red';
					}	
					else
						$temp['booking_status'] = 'yellow';
	
				}
				else
				{
					$temp['booking_status'] = 'green';

					if( $value['current_capacity'] <= 0){
						
						$temp['booking_status'] = 'red';
					}
				}
				
				array_push($return,$temp);
			}
		}
		return $return;
	}


	function getboardingSchedule()
	{
		$return = array();
		$this->db->select('count(inventory_id) as orginal_capacity,sum(boarding_schedule.is_booked) as current_capacity,boarding_schedule.start,boarding_schedule.end');
		$this->db->group_by('date(boarding_schedule.start)');
		$result = $this->db->get_where('boarding_schedule')->result_array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
			
				
				//$timeAvailable = (((strtotime($value['end']) - strtotime($value['start']))/60)*$value['orginal_capacity']);
				//echo (strtotime($value['end']) - strtotime($value['start']));exit;
				
				//$temp['working_hrs'] = $timeAvailable; 
				// $Appointments = $this->db->get_where('boardings',array('start_date'=>$value['date'],'category'=>'daycare'))->result_array();
				// //$temp['booking_time'] = 0;
				// if(!empty($Appointments))
				// {
					//pr($Appointments);
					
				if($value['current_capacity'] == $value['orginal_capacity'])
				{
					
						
						$temp['booking_status'] = 'red';
						//$temp['available_time'] = $availableTime;
					

						
				}
				else
				{

					

					if( $value['current_capacity'] == 0){
						
						$temp['booking_status'] = 'green';
						//$temp['available_time'] = $availableTime;
						
					}
					else
					{
						$temp['current_capacity'] = $value['orginal_capacity'] - $value['current_capacity'];
						$temp['booking_status'] = 'yellow';
					}

				 	
				//$temp['available_time'] = $timeAvailable;
				}
				
				array_push($return,$temp);



			}
			

		}
		// pr($return);
		// exit;
		return $return;
	}

	function getMaxMulitplierByUserIdAndServices($user_id)
	{
		$this->db->select('max(groomer_services.pet_multiplier) as pet_max');
		//$this->db->where_in('service_id',$services);
		$result = $this->db->get_where('groomer_services',array('user_id'=>$user_id))->row_array();
		return $result['pet_max'];
	}

	function getGroomerScheduleForDay($id,$date)
	{
		$result = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'Date(date)'=>$date))->row_array();
		if(!empty($result)){
			$result = groomer_scheduleTimeConverter($result);
		}
		
		return $result;
	}

	function getDayCareScheduleForDay($date)
	{
		$result = $this->db->get_where('daycare_schedule',array('Date(date)'=>$date))->row_array();
		if(!empty($result)){
			$result = daycare_scheduleTimeConverter($result);
		}

		return $result;
	}

	function getBoardingScheduleForDay($date)
	{
		return $this->db->get_where('boarding_schedule',array('Date(start)'=>$date))->row_array();
	}
	

	function addGroomerBookingTime($groomer_services,$selectedDate,$start,$end,$upper_limit,$user_id)
	{
		//Check if there is an appointment for that da
		

		// if(!empty($groomer_services)){
		// 	foreach ($groomer_services as $key => $value) {
		// 		$temp = array();
		// 		$temp['date'] = $selectedDate;
		// 		$temp['start'] = $start;
		// 		$temp['end'] = $end;
		// 		$temp['service_id'] = $value['service_id'];
		// 		$temp['user_id'] = $value['user_id'];
		// 		$temp['pet_multiplier'] = $value['pet_multiplier'];
		// 		singleInsert('groomer_bookings',$temp);
		// 	}
				
		// }

		if($upper_limit == 1)
		{
			$temp = array();
			$temp = array();
			$temp['date'] = $selectedDate;
			$temp['start'] = $start;
			//$x = 15;
			$x = get_option('stagger_time');
			$temp['end'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.$x.' minutes'));;
			
			$$temp['user_id'] = $user_id;
			$temp['upper_limit'] = 1;
			singleInsert('groomer_schedule_bookings',$temp);

			$temp2['user_id'] = $user_id;		
			$temp2['upper_limit'] = $i;	
			$temp2['date'] = $selectedDate;
			$temp2['start'] = $temp['end'];
			$temp2['end'] = $end;
			singleInsert('groomer_schedule_bookings',$temp2);
		}
		else
		{
			//$x = 15;
			$x = get_option('stagger_time');
			for($i=1;$i<=$upper_limit;$i++)
			{


				$temp = array();

				$temp['user_id'] = $user_id;		
				$temp['upper_limit'] = $i;	
				$temp['date'] = $selectedDate;

				$temp2 = array();
				$temp2['user_id'] = $user_id;		
				$temp2['upper_limit'] = $i;	
				$temp2['date'] = $selectedDate;
				$temp2['end'] = $end;


				if($i == 1)
					$temp['start'] = $start;
				else
				{
					$temp['start'] = $newStart; 
				}

				if($i == $upper_limit){
					$temp['end'] = date('Y-m-d H:i:s',strtotime($start.' +'.($i*$x).' minutes'));
					$temp2['start'] = $temp['end'];
				}	
				else
				{
					$temp['end'] = date('Y-m-d H:i:s',strtotime($start.' +'.($i*$x).' minutes'));
					$newStart = $temp['end'];
				}
					
					
				singleInsert('groomer_schedule_bookings',$temp);	

				if($i == $upper_limit)
					singleInsert('groomer_schedule_bookings',$temp2);		

			}
		}


	}

	function editGroomerBookingTime($groomer_services,$selectedDate,$start,$end,$user_id,$upper_limit)
	{
		//first check appointment for time

		// $groomerScheduleForDay = $this->db->get_where('groomer_bookings',array('user_id'=> $user_id,'date'=>$selectedDate))->result_array();
		// if(!empty($groomerScheduleForDay))
		// {
		// 	foreach ($groomerScheduleForDay as $key => $value) {
		// 		//Start Time and end time for appointment is same
		// 		$temp = array();
		// 		//$temp['date'] = $selectedDate;
		// 		$temp['start'] = $start;
		// 		$temp['end'] = $end;
		// 		$this->db->update('groomer_bookings',$temp,array('id'=>$value['id']));
		// 	}
		// }


		$this->db->delete('groomer_schedule_bookings',array('date'=>$selectedDate,'user_id'=>$user_id));
		if($upper_limit == 1)
		{
			$temp = array();
			$temp = array();
			$temp['date'] = $selectedDate;
			$temp['start'] = $start;
			//$x = 15;
			$x = get_option('stagger_time');
			$temp['end'] = date('Y-m-d H:i:s',strtotime($temp['start'].' + '.$x.' minutes'));;
			
			$$temp['user_id'] = $user_id;
			$temp['upper_limit'] = 1;
			singleInsert('groomer_schedule_bookings',$temp);

			$temp2['user_id'] = $user_id;		
			$temp2['upper_limit'] = $i;	
			$temp2['date'] = $selectedDate;
			$temp2['start'] = $temp['end'];
			$temp2['end'] = $end;
			singleInsert('groomer_schedule_bookings',$temp2);
		}
		else
		{
			//$x = 15;
			$x = get_option('stagger_time');
			for($i=1;$i<=$upper_limit;$i++)
			{


				$temp = array();

				$temp['user_id'] = $user_id;		
				$temp['upper_limit'] = $i;	
				$temp['date'] = $selectedDate;

				$temp2 = array();
				$temp2['user_id'] = $user_id;		
				$temp2['upper_limit'] = $i;	
				$temp2['date'] = $selectedDate;
				$temp2['end'] = $end;


				if($i == 1)
					$temp['start'] = $start;
				else
				{
					$temp['start'] = $newStart; 
				}

				if($i == $upper_limit){
					$temp['end'] = date('Y-m-d H:i:s',strtotime($start.' +'.($i*$x).' minutes'));
					$temp2['start'] = $temp['end'];
				}	
				else
				{
					$temp['end'] = date('Y-m-d H:i:s',strtotime($start.' +'.($i*$x).' minutes'));
					$newStart = $temp['end'];
				}
					
					
				singleInsert('groomer_schedule_bookings',$temp);	

				if($i == $upper_limit)
					singleInsert('groomer_schedule_bookings',$temp2);		

			}
		}

	}

	function getBestTime($date,$services,$pet,$user_id,$start_time,$end_time,$time_estimate,$array)
	{
		//pr($pet);
		$count = count($array);
		$return = array();
		$i = 0;
		//echo $count; 
		//
		$start_time = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));
		$end_time = date('Y-m-d H:i:s',strtotime($date.' '.$end_time));
		foreach ($array as $value) {
	    	# code...
		    // $this->db->where(array('user_id'=>$user_id,'date'=>$date,'pet_multiplier >'=>0));
		    // // $this->db->group_start();
		    // // $this->db->where(array('start <='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start <='=>$start_time,'end <='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->group_end();

		    // $this->db->group_start();
		    // $this->db->where(array('start <='=>$end_time,'end >='=> $start_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start <='=>$start_time,'end <='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // $this->db->group_end();



		    // $t = $this->db->get_where('groomer_bookings',array('service_id'=>$value))->result_array();

			$t = $this->db->query("SELECT *
					FROM `groomer_bookings`
					WHERE `user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					AND ('".$start_time."' < groomer_bookings.end and '".$end_time."' > groomer_bookings.start)
					UNION All 
					SELECT *
					FROM `groomer_bookings` Where
					`user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					And ('".$start_time."' >= groomer_bookings.start and '".$end_time."' <= groomer_bookings.end)")->result_array();

		    //echo $this->db->last_query();
		    //pr($t);
		    if(!empty($t))
		    {
		    	foreach ($t as $k => $v) {
		    		if(empty($v['size']) || ($pet['size'] == $v['size']))
		    		{
		    			//pr($v);
		    			//$n = 1;
		    			$service = $this->db->get_where('services',array('id'=>$v['service_id']))->row_array();
		    			
		    			if($start_time <= $v['start'])
		    				$start = $v['start'];
		    			else
		    				$start = $start_time;

		    			//echo $start;exit;
		    			$end = date('Y-m-d H:i:s',strtotime($start.' +'.$service[$pet['size'].'_time_estimate'].' minutes'));
		    			//pr($pet['size']);exit;
		    			//echo $service[$pet['size'].'_time_estimate'];
		    			$return['start'] =  $start;
		    			//Slot is available
		    			//if($this->checkforNextSlot($date,$services,$pet,$user_id,$start_time,$end_time,$time_estimate,$array,$value))
		    			
		    			if($count > 1)
		    			{
			    			for($j=1;$j<$count;$j++)
			    			{
			    				//echo "ddas";
			    				if($j==1)
			    					$r['start'] = $end;
			    				else
			    					$r['start'] = $r['end'];
			    				
			    				$s = $this->db->get_where('services',array('id'=>$array[$j]))->row_array();
			    				$temp = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));

			    				$a = $this->db->query("SELECT *
						            FROM `groomer_bookings`
									WHERE `user_id` = '".$user_id."'
									AND `date` = '".$date."'
									AND `pet_multiplier` >0
									AND `service_id` = ".$array[$j]."
									AND ('".$r['start']."' < groomer_bookings.end and '".$temp."' > groomer_bookings.start)
									UNION All 
									SELECT *
									FROM `groomer_bookings` Where
									`user_id` = '".$user_id."'
									AND `date` = '".$date."'
									AND `pet_multiplier` >0
									AND `service_id` = ".$array[$j]."
									And ('".$r['start']."' >= groomer_bookings.start and '".$temp."' <= groomer_bookings.end)

									")->row_array();

			   					// echo $this->db->last_query();
			   					
			   					//  pr($a);exit;
			   					if(!empty($a)){
			   						if(empty($a['size']) || ($pet['size'] == $a['size']))
			   						{
			   							//echo $s[$pet['size'].'_time_estimate'];	
			   							$r['start'] = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));
			   							$r['end'] = $r['start'];
			   							
			   						}
			   						else
			   						{
			   							return false;
			   						}	
			   					}	
			   					else
			   					{
			   						return false;
			   					}			



			    				//Checking For Next Slot

			    			}
		    				$return['end'] = $r['end'];
		    			}
		    			else
		    				$return['end'] = $end;

		    			
		    			//exit;
		    			return $return;

		    		}

		    		

		    	}
		    	
		    }

		    return false;

	    }
	}


	function pc_permute($items,$date,$services,$pet,$user_id,$start_time,$end_time,$time_estimate,$perms = array( )) 
	{
	  $return  = array();
	  if (empty($items)) { 
	       if($r = $this->getBestTime($date,$services,$pet,$user_id,$start_time,$end_time,$time_estimate,$perms))
		    {
		    	//pr($r);
		    	//pr($perms);
		    	$return['start'] = $r['start'];
		    	$return['end'] = $r['end'];

		    	$order = implode (", ",$perms);

		    	$return['order'] = $order;
		    	
		    	return $return;
		    	//exit;

		    }
	    }  else {

	    	if(empty($return))
	    	{
		        for ($i = count($items) - 1; $i >= 0; --$i) {
		             $newitems = $items;
		             $newperms = $perms;
		             list($foo) = array_splice($newitems, $i, 1);
		             array_unshift($newperms, $foo);
		             //pr($newperms);exit;
		            if($r = $this->pc_permute($newitems,$date,$services,$pet,$user_id,$start_time,$end_time,$time_estimate,$newperms))
		             {
				    	if(!empty($r))
				    		return $r;
				    }
		         }
		    }
		    else
		    {
		    	return $return;
		    }     

	    }
	}

	function getlocations()
	{
		$return = array();
		// $return['daycare']=$this->db->get_where('boarding_inventory',array('category'=>'daycare','is_deleted'=>0))->result_array();
		// $return['boardings']=$this->db->get_where('boarding_inventory',array('category'=>'boarding','is_deleted'=>0))->result_array();
		
		$playarea = array();
		$daycare = array();
		$boarding = array();

		$result = $this->db->get_where('playareas')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['boardings'] = $this->getBoardingDataWithPetById($value['id'],'playarea');
				array_push($playarea,$temp); 
			}
		}
		$return['playarea'] = $playarea;

		$result = $this->db->get_where('daycare_locations')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['boardings'] = $this->getBoardingDataWithPetById($value['id'],'daycare');
				array_push($daycare,$temp); 
			}
		}


		$return['daycare'] = $daycare;

		$result = $this->db->get_where('boarding_inventory')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['boardings'] = $this->getBoardingDataWithPetById($value['id'],'boarding');
				array_push($boarding,$temp); 
			}
		}


		$return['boarding'] = $boarding;




		return $return;
	}


	function getBoardingDataWithPetById($id,$location_type)
	{
		$return = array();
		$this->db->where_in('status',array('Inprocess','Complete'));
		$r = $this->db->get_where('boardings',array('assigned_inventory_id'=>$id,'assigned_inventory_type'=>$location_type))->result_array();
		//pr($r);exit;
		if(!empty($r))
		{

			foreach ($r as $key => $value) {
				$temp = array();
				$temp = $value;
				$pet = $this->db2->get_where('pets',array('id'=>$value['pet_id']))->row_array();
				$temp['petName'] = $pet['name'];
				$temp['pet_cover_photo'] = $pet['pet_cover_photo'];
                $temp['username'] = $this->Management_model->getUsername($value['app_user_id']);
                array_push($return,$temp);
			}
		}

		return $return;
	}




	function AddDayCareScheduleForPlayArea($capacity,$playarea_id)
	{
		$this->db->where('date(date) >=',date('Y-m-d'));
		$result = $this->db->get_where('daycare_schedule')->result_array();
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp =$value;
				unset($temp['id']);
				$temp['playarea_id'] = $playarea_id;
				$temp['orginal_capacity'] =  $capacity;
				$temp['current_capacity'] = $capacity;
				$temp['added_on'] = date('Y-m-d H:i:s');
				$this->db->insert('daycare_schedule',$temp);
			}
			 
		}
		return true;	

	}


	function AddBoardingScheduleFortheBoarding($size,$size_priority,$boarding_inventory_id)
	{

		$boarding_schedule = $this->db->get_where('boarding_inventory',array('id !='=>$boarding_inventory_id))->row_array();
		//pr($boarding_schedule);
		if(!empty($boarding_schedule))
		{
			    $this->db->where('date(start) >=',date('Y-m-d'));
				$result = $this->db->get_where('boarding_schedule',array('inventory_id'=>$boarding_schedule['id']))->result_array();
				//pr($result);
				if(!empty($result))
				{
					foreach ($result as $key => $value) {
						$temp =$value;
						unset($temp['id']);
						$temp['inventory_id'] = $boarding_inventory_id;
						$temp['is_booked'] = 0;
						$temp['added_on'] = date('Y-m-d H:i:s');
						$temp['size'] = $size;
						$temp['size_priority'] = $size_priority;
						$this->db->insert('boarding_schedule',$temp);
					}
					 
				}
		}		
		return true;	
	}


	function editDayCareScheduleForPlayArea($capacity,$id)
	{
		$this->db->where('date(date) >=',date('Y-m-d'));
		$result = $this->db->get_where('daycare_schedule',array('playarea_id'=>$id))->result_array();
		//pr($result);
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp =array();
				unset($temp['id']);
				//$temp['playarea_id'] = $playarea_id;
				//pr($value);
				if($value['orginal_capacity']  == $value['current_capacity']){
					$temp['orginal_capacity'] =  $capacity;
					$temp['current_capacity'] = $capacity;
				}
				else
				{
					//echo $capacity - $value['orginal_capacity'];
					$diff = $capacity - $value['orginal_capacity'];
					if($diff > 0)
					{
						$temp['orginal_capacity'] =  intval($value['orginal_capacity']+$diff);
						$temp['current_capacity'] = intval($value['current_capacity']+$diff);
					}
					else
					{
						$diff = $value['orginal_capacity'] - $capacity ;
						$temp['orginal_capacity'] =  intval($value['orginal_capacity']-$diff);
						$temp['current_capacity'] = intval($value['current_capacity']-$diff);
					} 
				}
				$temp['added_on'] = date('Y-m-d H:i:s');
				$this->db->update('daycare_schedule',$temp,array('id'=>$value['id']));
			}
			 
		}
		return true;	
	}

	// function getDayCareCapacity()
	// {
	// 	$r = $this->db->get_where('playarea',array('category'=>'daycare','type'=>'playarea'))->row_array();
	// 	return $r['capacity'];
	// }

	// function getboardingCapacity()
	// {
	// 	$r = $this->db->get_where('boarding_inventory',array('category'=>'boarding','type'=>'playarea'))->row_array();
	// 	return $r['capacity'];
	// }

	function getDailyData($tab_name,$startDate = '', $endDate='')
	{

		if(empty($date))
			$date = date('Y-m-d');

		$return = array();

		// $ingroups_id = array('8');
		// if($tab_name == 'daily'){
		// 	$ingroups_id = array('6', '8');
		// }
		//$ingroups_id = implode(",", $ingroups_id);

		if($this->session->userdata('groomer') == 1)
		{
			
			// $result = $this->db->select('admin_users.*, admin_users_groups.id as admin_users_groups_id, admin_groups.id as admin_groups_id , GROUP_CONCAT(admin_users_modules.module_id) as modules_id ')
			$result = $this->db->select('admin_users.*,  GROUP_CONCAT(users_modules.module_id) as modules_id ')
				->from('admin_users')
				// ->join('admin_users_groups', 'admin_users.id = admin_users_groups.user_id', 'left')
				// ->join('admin_groups', 'admin_users_groups.group_id = admin_groups.id', 'left')
				->join('users_modules', 'users_modules.user_id = admin_users.id', 'left')
				// ->join('admin_modules', 'admin_users_modules.module_id = admin_modules.id', 'left')
				->where(array('admin_users.id'=>$this->session->userdata('id')))
				// ->where_in('admin_groups.id',$ingroups_id)
				->group_by('admin_users.id')
				->get()->result_array();
		
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					$temp = array();
					$temp = $value;
					$temp['amount'] = 0;
					$temp['commission'] = 0;
					$this->db->select('*');
					$this->db->where_in('status',array('Inprocess','Complete','Checkout'));
					if(empty($startDate)){
						$this->db->where(array('date(appointment_date)'=>$date));
					}else{
						$this->db->where(array('date(appointment_date)>='=>$startDate,'date(appointment_date)<='=>$endDate));
					}

					$appointment_completed = $this->db->get_where('appointments',array('user_id'=>$value['id'],'payment_status'=>'success'))->result_array();
					if(!empty($appointment_completed)){
						foreach ($appointment_completed as $k => $v) {
							$temp['commission'] += $v['commission'];
							if($v['payment_method'] == 'offline'){
								$temp['amount'] += ($v['cost'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
								
							}
							else
							{
								$payment =$this->db2->get_where('transactions',array('reference_id'=>$v['id'],'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();
								$temp['amount'] += ($payment['amount'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
							}
						}
					}
					$temp['appointment_count'] = count($appointment_completed);
					if(empty($startDate)){
						$this->db->where(array('date(date)'=>$date,'user_id'=>$value['id']));
					}else{
						$this->db->where(array('date(date)>='=>$startDate,'date(date)<='=>$endDate,'user_id'=>$value['id']));
					}
					$groomerTracking = $this->db->get_where('groomer_tracking')->row_array();

					$temp['check_in'] = 'N/A';
					$temp['check_out'] = 'N/A';
					if(!empty($groomerTracking))
					{
						$temp['check_in'] = date('h:i a',strtotime($groomerTracking['start']));

						if(!empty($groomerTracking['end']))
							$temp['check_out'] = date('h:i a',strtotime($groomerTracking['end']));
					}

					if(empty($startDate)){
						$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id'],'date'=>$date))->row_array();
					}else{
						$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id'],'date(date)>='=>$startDate,'date(date)<='=>$endDate))->row_array();
					}

					$temp['start'] = 'N/A';
					$temp['end'] = 'N/A';

					if(!empty($groomer_schedule))
					{
						$temp['start'] = date('h:i a',strtotime($groomer_schedule['start']));
						$temp['end'] = date('h:i a',strtotime($groomer_schedule['end']));
					}

					array_push($return,$temp);
				}


			}
		}
		else
		{
			
			// $result = $this->db->select('admin_users.*, admin_users_groups.id as admin_users_groups_id, admin_groups.id as admin_groups_id , GROUP_CONCAT(admin_users_modules.module_id) as modules_id ')
			// 	->from('admin_users')
			// 	->join('admin_users_groups', 'admin_users.id = admin_users_groups.user_id', 'left')
			// 	->join('admin_groups', 'admin_users_groups.group_id = admin_groups.id', 'left')
			// 	->join('admin_users_modules', 'admin_users_modules.user_id = admin_users.id', 'left')
			// 	->join('admin_modules', 'admin_users_modules.module_id = admin_modules.id', 'left')
			// 	->where_in('admin_groups.id',$ingroups_id)
			// 	->group_by('admin_users.id')
			// 	->get()->result_array();
			$result = $this->db->select('admin_users.*,  GROUP_CONCAT(users_modules.module_id) as modules_id ')
				->from('admin_users')
				// ->join('admin_users_groups', 'admin_users.id = admin_users_groups.user_id', 'left')
				// ->join('admin_groups', 'admin_users_groups.group_id = admin_groups.id', 'left')
				->join('users_modules', 'users_modules.user_id = admin_users.id', 'left')
				// ->join('admin_modules', 'admin_users_modules.module_id = admin_modules.id', 'left')
				->where(array('admin_users.id'=>$this->session->userdata('id')))
				// ->where_in('admin_groups.id',$ingroups_id)
				->group_by('admin_users.id')
				->get()->result_array();
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					$temp = array();
					$temp = $value;
					$temp['amount'] = 0;
					$temp['commission'] = 0;

					//----------------------------------------
						//  query for fetch reasult from appointments table for schrdule in and schrdule out records
					//----------------------------------------

					$this->db->select('*');
					$this->db->where_in('status',array('Inprocess','Complete','Checkout'));
					if(empty($startDate)){
						$this->db->where(array('date(appointment_date)'=>$date));
					}else{
						$this->db->where(array('date(appointment_date)>='=>$startDate,'date(appointment_date)<='=>$endDate));
					}
					$appointment_completed = $this->db->get_where('appointments',array('user_id'=>$value['id'],'payment_status'=>'success'))->result_array();
					
					if(!empty($appointment_completed)){
						foreach ($appointment_completed as $k => $v) {
							$temp['commission'] += $v['commission'];
							if($v['payment_method'] == 'offline'){
								$temp['amount'] += ($v['cost'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
								
							}
							else
							{
								$payment =$this->db2->get_where('transactions',array('reference_id'=>$v['id'],'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();
								$temp['amount'] += ($payment['amount'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
							}

						}
					}
					$temp['appointment_count'] = count($appointment_completed);

					// ----------------------------------------
						  //query for fetch reasult from groomer_tracking table for clock in and clock out records
					// ----------------------------------------
					if(empty($startDate)){
						$this->db->where(array('date(date)'=>$date,'user_id'=>$value['id']));
					}else{
						$this->db->where(array('date(date)>='=>$startDate,'date(date)<='=>$endDate,'user_id'=>$value['id']));
					}
					$groomerTracking = $this->db->get_where('groomer_tracking')->row_array();


					$temp['check_in'] = 'N/A';
					$temp['check_out'] = 'N/A';
					if(!empty($groomerTracking))
					{
						$temp['check_in'] = date('h:i a',strtotime($groomerTracking['start']));

						if(!empty($groomerTracking['end']))
							$temp['check_out'] = date('h:i a',strtotime($groomerTracking['end']));
					}

					if(empty($startDate)){
						$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id'],'date'=>$date))->row_array();
					}else{
						$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id'],'date(date)>='=>$startDate,'date(date)<='=>$endDate))->row_array();
					}

					$temp['start'] = 'N/A';
					$temp['end'] = 'N/A';

					if(!empty($groomer_schedule))
					{
						$temp['start'] = date('h:i a',strtotime($groomer_schedule['start']));
						$temp['end'] = date('h:i a',strtotime($groomer_schedule['end']));
					}
					
					array_push($return,$temp);

				}


			}	
		}

		return $return;
	}

	function dailyDataById($id,$startDate = '', $endDate='')
	{
		/*echo"id = ".$id."<br>";
		echo"start date = ".$startDate."<br>";
		echo"end date = ".$endDate."<br>";exit;*/
		if(empty($startDate)){
			$startDate = date('Y-m-d', strtotime('-1 months'));
			$endDate = date('Y-m-d');
		}else{
			$startDate = date("Y-m-d", strtotime($startDate));
			$endDate = date("Y-m-d", strtotime($endDate));
		}
	
		$return = array();

		for ($i=0; $i < 31; $i++) { 

			$temp = array();
			
			$temp['amount'] = 0;
			$temp['commission'] = 0;
			$temp['totalHours'] = 0;

			
			$this->db->select('*');
			$this->db->where_in('status',array('Inprocess','Complete','Checkout'));
			// $this->db->where('status',array('date(appointment_date)'=>$endDate);
			$this->db->where(array('date(appointment_date)>='=>$startDate));
			$this->db->where(array('date(appointment_date)<='=>$endDate));
			$appointment_completed = $this->db->get_where('appointments',array('user_id'=>$id,'payment_status'=>'success'))->result_array();

			// echo $this->db->last_query();exit;
			// pr($appointment_completed);exit;
			// echo "$startDate";
			// print_r($appointment_completed);exit;
			if(!empty($appointment_completed)){
				foreach ($appointment_completed as $k => $v) {
					$temp['commission'] += $v['commission'];
					if($v['payment_method'] == 'offline'){
						$temp['amount'] += ($v['cost'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
						
					}
					else
					{
						$payment =$this->db2->get_where('transactions',array('reference_id'=>$v['id'],'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();
						$temp['amount'] += ($payment['amount'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
					}
				}
			}
			$temp['appointment_count'] = count($appointment_completed);

			// ----------------------------------------
				  //query for fetch reasult from groomer_tracking table for clock in and clock out records
			// ----------------------------------------


			/*$this->db->where(array('date(date)'=>$startDate,'user_id'=>$id));
			//$groomerTracking = $this->db->get_where('groomer_tracking')->row_array();
			$groomerTracking = $this->db->get_where('groomer_tracking')->result_array();

		 	//echo $this->db->last_query();
		 	//echo"<pre>";
		 	//print_r($groomerTracking);exit;

			$temp['check_in'] = 'N/A';
			$temp['check_out'] = 'N/A';

			if(!empty($groomerTracking)){
				$temp['check_in'] = date('h:i a',strtotime($groomerTracking['start']));
				$check_in = strtotime($groomerTracking['start']);

				if(!empty($groomerTracking['end'])){
					$check_out = strtotime($groomerTracking['end']);
					$temp['check_out'] = date('h:i a',strtotime($groomerTracking['end']));
				}else{
					$check_out = strtotime($groomerTracking['start'].'+ 4 hours');
				}
				
				$hrInMin = round(abs($check_out - $check_in) / 60,0);
				$temp['totalHours'] = $hrInMin;
			}*/


			//echo"here 2";exit;
			$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$id,'date(date)'=>$startDate))->row_array();

			$temp['start'] = 'N/A';
			$temp['end'] = 'N/A';

			if(!empty($groomer_schedule))
			{
				//pr($groomer_schedule);
				$groomer_schedule['start'] = user_date($groomer_schedule['start'],'Y-m-d H:i:s');
				$groomer_schedule['end'] = user_date($groomer_schedule['end'],'Y-m-d H:i:s');
				//pr($groomer_schedule);exit;
				$temp['start'] = date('h:i a',strtotime($groomer_schedule['start']));
				$temp['end'] = date('h:i a',strtotime($groomer_schedule['end']));
			}
			$temp['startDate'] = date(get_option('date_format'), strtotime($startDate));



			$this->db->where(array('date(date)'=>$startDate,'user_id'=>$id));
			//$groomerTracking = $this->db->get_where('groomer_tracking')->row_array();
			$groomerTracking_data = $this->db->get_where('groomer_tracking')->result_array();

		 	//echo $this->db->last_query();
		 	//echo"<pre>";
		 	//print_r($groomerTracking);exit;

			$temp['check_in'] = 'N/A';
			$temp['check_out'] = 'N/A';
			$flg = 0;
			//pr($groomerTracking_data);exit;
			foreach($groomerTracking_data as $groomerTracking){

				if(!empty($groomerTracking)){
					
					$groomerTracking['start'] = user_date($groomerTracking['start'],'Y-m-d H:i:s');
					$groomerTracking['end'] = user_date($groomerTracking['end'],'Y-m-d H:i:s');


					$temp['check_in'] = date('h:i a',strtotime($groomerTracking['start']));
					$check_in = strtotime($groomerTracking['start']);

					if(!empty($groomerTracking['end'])){
						$check_out = strtotime($groomerTracking['end']);
						$temp['check_out'] = date('h:i a',strtotime($groomerTracking['end']));
						$hrInMin = round(abs($check_out - $check_in) / 60,0);
					    $temp['totalHours'] = $hrInMin;
					}else{
						//$check_out = strtotime($groomerTracking['start'].'+ 4 hours');
						$temp['check_out'] = 'N/A';
						$temp['totalHours'] = 0;
					}
					
					//$hrInMin = round(abs($check_out - $check_in) / 60,0);
					//$temp['totalHours'] = $hrInMin;
					$flg = 1;
					array_push($return,$temp);
				}
			}

			if($flg==0){
				array_push($return,$temp);
			}

			if ($startDate == $endDate || $startDate > $endDate) {
				break;
			}
			$startDate = date('Y-m-d', strtotime('+1 days',strtotime($startDate)));
		}
		return $return;
	}
	function getDailyDataByDate($tab_name,$startDate = '', $endDate='')
	{

		if(empty($startDate)){
			$startDate = date('Y-m-d', strtotime('-30 days'));
			$endDate = date('Y-m-d');
		}

		$return = array();

		// if($this->session->userdata('groomer') == 1)
		// {
			
		// 	// $result = $this->db->select('admin_users.*, admin_users_groups.id as admin_users_groups_id, admin_groups.id as admin_groups_id , GROUP_CONCAT(admin_users_modules.module_id) as modules_id ')
		// 	$result = $this->db->select('admin_users.* , GROUP_CONCAT(users_modules.module_id) as modules_id ')
		// 		->from('admin_users')
		// 		// ->join('admin_users_groups', 'admin_users.id = admin_users_groups.user_id', 'left')
		// 		// ->join('admin_groups', 'admin_users_groups.group_id = admin_groups.id', 'left')
		// 		// ->join('admin_users_modules', 'admin_users_modules.user_id = admin_users.id', 'left')
		// 		// ->join('admin_modules', 'admin_users_modules.module_id = admin_modules.id', 'left')
		// 		// ->where(array('admin_users.id'=>$this->session->userdata('id'),'admin_groups.id'=>8))
		// 		->join('users_modules', 'users_modules.user_id = admin_users.id', 'left')
		// 		->where(array('admin_users.id'=>$this->session->userdata('id')))
		// 		->group_by('admin_users.id')
		// 		->get()->result_array();



		// }else{
			$result = $this->db->select('admin_users.*,GROUP_CONCAT(users_modules.module_id) as modules_id ')
			// $result = $this->db->select('admin_users.*, admin_users_groups.id as admin_users_groups_id, admin_groups.id as admin_groups_id , GROUP_CONCAT(admin_users_modules.module_id) as modules_id ')
				->from('admin_users')
				// ->join('admin_users_groups', 'admin_users.id = admin_users_groups.user_id', 'left')
				// ->join('admin_groups', 'admin_users_groups.group_id = admin_groups.id', 'left')
				// ->join('admin_users_modules', 'admin_users_modules.user_id = admin_users.id', 'left')
				// ->join('admin_modules', 'admin_users_modules.module_id = admin_modules.id', 'left')
				->join('users_modules', 'users_modules.user_id = admin_users.id', 'left')
				// ->where_in('admin_groups.id',array('6', '8'))
				->where('admin_users.id!=',1)
				->group_by('admin_users.id')
				->get()->result_array();

		// }	
		// echo $this->db->last_query();exit;


		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['amount'] = 0;
				$temp['commission'] = 0;
				$temp['totalHours'] = 0;

				//----------------------------------------
					//  query for fetch reasult from appointments table for schrdule in and schrdule out records
				//----------------------------------------

				$this->db->select('*');
				$this->db->where_in('status',array('Inprocess','Complete','Checkout'));
				$this->db->where(array('date(appointment_date)>='=>$startDate,'date(appointment_date)<='=>$endDate));
				$appointment_completed = $this->db->get_where('appointments',array('user_id'=>$value['id'],'payment_status'=>'success'))->result_array();
				// pr($appointment_completed);

				 // echo  $this->db->last_query();
				
				if(!empty($appointment_completed)){
					foreach ($appointment_completed as $k => $v) {
						$temp['commission'] += $v['commission'];
						if($v['payment_method'] == 'offline'){
							$temp['amount'] += ($v['cost'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
							
						}
						else
						{
							$payment =$this->db2->get_where('transactions',array('reference_id'=>$v['id'],'category'=>'appointment','shop_id'=>$this->session->userdata('shop_id'),'is_add_on_payment'=>0))->row_array();
							$temp['amount'] += ($payment['amount'] + $v['additional_cost'] + $v['upgrade_cost']+ $v['add_on_service_cost']);
						}
					}
				}
				$temp['appointment_count'] = count($appointment_completed);

				// ----------------------------------------
					  //query for fetch reasult from groomer_tracking table for clock in and clock out records
				// ----------------------------------------
				$this->db->where(array('date(date)>='=>$startDate,'date(date)<='=>$endDate,'user_id'=>$value['id']));
				$groomerTracking = $this->db->get_where('groomer_tracking')->result_array();

				// echo $this->db->last_query();
				// echo "<pre>";
				// print_r($groomerTracking);exit;

				/*if(!empty($groomerTracking)){
					foreach ($groomerTracking as $k => $v) {
						//print_r($v);exit;
						$check_in = strtotime($v['start']);

						if(!empty($v['end'])){
							$check_out = strtotime($v['end']);
						}else{
							$check_out = strtotime($v['start'].'+ 4 hours');
						}
						
						$hrInMin = round(abs($check_out - $check_in) / 60,0);
						$temp['totalHours'] += $hrInMin;
					}
				}
				*/

				if(!empty($groomerTracking)){
					foreach ($groomerTracking as $k => $v) {
						//print_r($v);exit;
						$check_in = strtotime($v['start']);

						if(!empty($v['end'])){
							$check_out = strtotime($v['end']);
							$hrInMin = round(abs($check_out - $check_in) / 60,0);
						    $temp['totalHours'] += $hrInMin;
						}else{
							//$check_out = strtotime($v['start'].'+ 4 hours');
							$temp['totalHours'] += 0;
						}
						
						//$hrInMin = round(abs($check_out - $check_in) / 60,0);
						//$temp['totalHours'] += $hrInMin;
					}
				}
				

				// $groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$value['id'],'date(date)>='=>$startDate,'date(date)<='=>$endDate))->result_array();

				// $temp['start'] = 'N/A';
				// $temp['end'] = 'N/A';

				// if(!empty($groomer_schedule))
				// {
				// 	$temp['start'] = date('h:i a',strtotime($groomer_schedule['start']));
				// 	$temp['end'] = date('h:i a',strtotime($groomer_schedule['end']));
				// }
				
				array_push($return,$temp);
			}
		}

		return $return;
	}
	
	protected $trans_DT_column = array(
	    'added_on',
	    '',
	    '',
	    '',
	    '',
	    '',
	    '',
	);
	function getfinancialAjax($searchVal = '', $sortColIndex = 0, $sortBy = 'desc', $limit = 0, $offset = 0)
	{
		$searchCondition = '';
		if(strlen($searchVal)){
            $searchCondition = "AND (
                transactions.category like '%$searchVal%'
               
            )";
        }
        $order_by = $this->trans_DT_column[$sortColIndex].' '.$sortBy;
		$return = array();
		//$result = $this->db2->query("SELECT * FROM ( SELECT * FROM transactions WHERE transcation_type = 'payment' AND status = 'success' AND payment_mode = 'offline'  AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 $searchCondition) as a UNION All SELECT b.* FROM ( SELECT * FROM transactions WHERE transcation_type = 'payout' AND status = 'success' AND payment_mode = 'online' AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 $searchCondition) as b ORDER BY $order_by LIMIT $offset, $limit")->result_array();

		$result = $this->db2->query("SELECT * FROM transactions WHERE status = 'success' AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 AND type != 'deposit' and payment_source !='Credit Redeem' AND transcation_type IN ('payment','payout') $searchCondition ORDER BY $order_by LIMIT $offset, $limit")->result_array();
		// echo $this->db2->last_query();
		 // pr($result);exit;
        $services = $this->Entry_model->getAllServices();
        if(!empty($result))
        {
            foreach ($result as $key => $value) {

            	// pr($value);exit;
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');
				$temp['commission'] = 0;
				if(!empty($temp['quick_invoice_id']) && $temp['quick_invoice_id'] >0){

                	$result = $this->db->get_where('quick_invoice',array('id'=>$value['quick_invoice_id']))->row_array();
                	
                	if(!empty($result['customer_name']))
                		$result['app_user_name'] = $result['customer_name'];

                	$temp =array_merge($temp,$result);
                	$temp['services'] = 'Quick Invoice';
                	$temp['username'] = 'N/A';
                }else{
                	// $deposit = $this->db2->get_where('transactions',array('invoice_id'=>$value['invoice_id'],'category'=>$value['category'],'type'=>'deposit'))->row_array();
     //            	$this->db->select('deposit');
					// $this->db->from('appointments ');
					// $this->db->join('boardings',"boardings.invoice_no = appointments.invoice_no");
					// $this->db->where(array('appointments.invoice_no' => $value['invoice_id'],'appointments.isDuplicate' => '0'));
					// $result = $this->db->get()->result_array();

				    // $this->db->select('appointments.deposit as ad,boardings.deposit as bd');
				    // $this->db->from('appointments');
				    // $this->db->join('boardings', 'boardings.invoice_no = appointments.invoice_no'); 
				    // $this->db->where(array('appointments.invoice_no' => $value['invoice_id'],'appointments.isDuplicate' => '0'));
				    // $result = $this->db->get()->result_array();

				    $deposit = $this->db->select('sum(deposit)')->get_where('appointments',array('invoice_no'=>$value['invoice_id']))->result_array();
				    // echo $this->db->last_query();
				    $deposit1 = $this->db->select('sum(deposit)')->get_where('boardings',array('invoice_no'=>$value['invoice_id']))->result_array();

				    $deposit = $deposit[0]['sum(deposit)']+$deposit1[0]['sum(deposit)'];

				    $deposit = number_format((float)$deposit, 2, '.', '');
				   // for
				   //  $deposit = $result[0]['ad'] + $result[0]['bd'];

				    // pr($deposit);
				      // pr($deposit);
                	// exit;
	               	$CreditRedeem = $this->db2->get_where('transactions',array('reference_id'=>$value['reference_id'],'category'=>$value['category'],'payment_source'=>'Credit Redeem'))->row_array();
	               	// echo $this->db2->last_query();exit;

                   		if(!empty($deposit) || !empty($CreditRedeem)){
                       		$temp['amount'] = $temp['amount'] + $deposit+ $CreditRedeem['amount'];
                   		}
	                if($value['category'] == 'appointment'){
	                	$temp['appointment'] = $this->Entry_model->getSingleAppointmentId($value['reference_id']);
	                	
	                	$temp['username'] = $this->Management_model->getGroomerName($temp['appointment']['user_id']);

	                	
	                	$temp['commission'] = $temp['appointment']['commission'];

	                	$temp['services'] = $temp['category'];
	                }
	                else{
	                	$temp['appointment']=$this->getBoardingDayCareData($value['reference_id']);

	                	$temp['username'] = 'N/A';
	                	$temp['services'] = $temp['category'];
	                }
	                //cancel appointment with deposite(for credit) 
	                if(($temp['appointment']['status'] == 'Cancel') && ( $deposit > 0)){
	                	$temp['amount'] = $deposit;
	                }

	                $temp['app_user_name'] = $this->Management_model->getUsername($temp['appointment']['app_user_id']);
	            }
	            // pr($temp);exit;
                array_push($return,$temp);
            }
        }
        //pr($return);exit;
        return $return;

	}

	function getfinancialAjaxCount($searchVal = '')
	{
		$searchCondition = '';
		if(strlen($searchVal)){
            $searchCondition = "AND (
                transactions.category like '%$searchVal%'
               
            )";
        }
		$return = array();
		$result = $this->db2->query("SELECT * FROM transactions WHERE status = 'success' AND shop_id = ".$this->session->userdata('shop_id')." AND type != 'deposit' and is_add_on_payment = 0 $searchCondition ORDER BY added_on DESC")->result_array();
        
        $count = count($result);
        return $count;
	}

	function getfinancial()
	{
		$return = array();
		$result = $this->db2->query("SELECT * FROM ( SELECT * FROM transactions WHERE transcation_type = 'payment' AND status = 'success' AND payment_mode = 'offline'  AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0) as a UNION All SELECT b.* FROM ( SELECT * FROM transactions WHERE transcation_type = 'payout' AND status = 'success' AND payment_mode = 'online' AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0) as b ORDER BY added_on DESC")->result_array();
        // echo $this->db2->last_query();
        // pr($result);exit;
        
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $additionalPayment = $this->getAdditionalPayment($value['reference_id'],$value['category']);


                if(!empty($additionalPayment) && $additionalPayment['status']=='success')
                	$temp['amount'] = $value['amount'] + $additionalPayment['amount'];


                $services = $this->Entry_model->getAllServices();
				$temp['commission'] = 0;

				if(!empty($temp['quick_invoice_id']) && $temp['quick_invoice_id'] >0){
                	$result = $this->db->get_where('quick_invoice',array('id'=>$value['quick_invoice_id']))->row_array();
                	if(!empty($result['customer_name']))
                		$result['app_user_name'] = $result['customer_name'];

                	$temp =array_merge($temp,$result);
                }else{
	                if($value['category'] == 'appointment'){
	                	$temp['appointment']=$this->Entry_model->getSingleAppointmentId($value['reference_id']);
	                	
	                	$temp['username'] = $this->Management_model->getGroomerName($temp['appointment']['user_id']);

	                	
	                	$temp['commission'] = $temp['appointment']['commission'];

	                	$appointmentService =$this->Entry_model->getAppointmentServiceById($value['reference_id']);
	                	$count = count($appointmentService); 
						$i = 0;$str = ''; 
						foreach ($services as $value) {                       
	                        if(in_array($value['id'], $appointmentService)) {
	                            $str .= ' '.$value['name'] . ', ';     
	                        }
	                    }
	                    $str = substr($str, 0, -1); $str = ltrim($str, ' ');
	                    $temp['services'] = $str;
	                    
	                	//$temp['amount'] = $temp['appointment']['cost']+$temp['appointment']['upgrade_cost']+$temp['appointment']['additional_cost']+$temp['additional_ser']
	                }
	                else{
	                	$temp['appointment']=$this->getBoardingDayCareData($value['reference_id']);

	                	$temp['username'] = 'N/A';
	                	$temp['services'] = $temp['category']; 
	                }
	                $temp['app_user_name'] = $this->Management_model->getUsername($temp['appointment']['app_user_id']);
                	$temp['pet_name'] = $this->Global_model->getPetName($temp['appointment']['pet_id']);
	            }

                

                

                // if($value['category'] == 'appointment'){
                // 	if($value['is_add_on_payment'] == 0)
                // 		$temp['services'] = $this->getServiceForPayment($value['reference_id'],$value['category']);
                // 	else
                // 		$temp['services'] = $this->getAddOnServices($value['reference_id'],$value['category']);

                // 	if(empty($temp['services']))
                // 		$temp['services'] = ucfirst($value['category']).' Additional Payment';
                // }
                // else
                // {
                // 	if($value['is_add_on_payment'] == 0)
                // 		$temp['services'] = $value['category'];
                // 	else
                // 		$temp['services'] = ucfirst($value['category']).' Additional Payment';
                // }


                array_push($return,$temp);
            }
        }

        return $return;

	}

	function getfinancialByDate($start_date,$end_date)
	{
		$start_date = date('Y-m-d H:i:s', strtotime($start_date.' 00:00:00'));
		$end_date = date('Y-m-d H:i:s', strtotime($end_date.' 23:59:59'));
		$start_date = server_date($start_date,'Y-m-d H:i:s');
		$end_date = server_date($end_date,'Y-m-d H:i:s');
		$return = array();
		//$result = $this->db2->query("SELECT * FROM ( SELECT * FROM transactions WHERE transcation_type = 'payment' AND status = 'success' AND payment_mode = 'offline'  AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 and date(added_on) <= '".$end_date."' and date(added_on) >= '".$start_date."'  ) as a UNION All SELECT b.* FROM ( SELECT * FROM transactions WHERE transcation_type = 'payout' AND status = 'success' AND payment_mode = 'online' AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 and date(added_on) <= '".$end_date."' and date(added_on) >= '".$start_date."') as b ORDER BY added_on DESC")->result_array();

		$result = $this->db2->query("SELECT * FROM transactions WHERE transcation_type IN ('payment','payout') AND status = 'success' AND shop_id = ".$this->session->userdata('shop_id')." and is_add_on_payment = 0 AND type != 'deposit' and payment_source !='Credit Redeem' and added_on <= '".$end_date."' and added_on >= '".$start_date."' ORDER BY added_on DESC")->result_array();
		$return['finaical'] = array(); 
		$return['totalamount'] = 0;
		$return['online'] = 0;
		$return['cash'] = 0;
		$return['credit'] = 0;
		$return['other'] = 0;
       
        $services = $this->Entry_model->getAllServices();
        if(!empty($result))
        {
            foreach ($result as $key => $value) {
                $temp = array();
                $temp = $value;
                $temp['added_on'] = user_date($temp['added_on'],'Y-m-d H:i:s');
                $return['totalamount'] = $return['totalamount'] + $temp['amount'];
                if($value['payment_mode'] == 'offline'){
                	if(strtolower($value['payment_source'])  == 'cash'){
                		$return['cash'] = $return['cash'] + $temp['amount'];
                	}elseif(strtolower($value['payment_source'])  == 'credit/debit'){
                		$return['credit'] = $return['credit'] + $temp['amount'];
                	}else{
                		$return['other'] = $return['other'] + $temp['amount'];
                	}
                	
                }else{
                	$return['online'] = $return['online'] + $temp['amount'];
                }
            	$temp['commission'] = 0;
				if(!empty($temp['quick_invoice_id']) && $temp['quick_invoice_id'] >0){
                	$result = $this->db->get_where('quick_invoice',array('id'=>$value['quick_invoice_id']))->row_array();
                	
                	if(!empty($result['customer_name']))
                		$result['app_user_name'] = $result['customer_name'];

                	$temp =array_merge($temp,$result);
                	//pr($temp);exit;
                	$temp['services'] = 'Quick Invoice';
                	$temp['username'] = 'N/A';
                }else{

                	$deposit = $this->db2->get_where('transactions',array('reference_id'=>$value['reference_id'],'category'=>$value['category'],'type'=>'deposit'))->row_array();
	               	$CreditRedeem = $this->db2->get_where('transactions',array('reference_id'=>$value['reference_id'],'category'=>$value['category'],'payment_source'=>'Credit Redeem'))->row_array();
                   if(!empty($deposit) || !empty($CreditRedeem)){
                       $temp['amount'] = $temp['amount'] + $deposit['amount']+ $CreditRedeem['amount'];
                   }

	                if($value['category'] == 'appointment'){
	                	$temp['appointment']=$this->Entry_model->getSingleAppointmentId($value['reference_id']);
	                	
	                	$temp['username'] = $this->Management_model->getGroomerName($temp['appointment']['user_id']);
	                	$appointmentService =$this->Entry_model->getAppointmentServiceById($value['reference_id']);

	                	$return['totalCommission'] += $temp['appointment']['commission'];
	                	$temp['commission'] = $temp['appointment']['commission'];

	                	// $temp['services'] = $temp['category'];
	                }
	                else{
	                	$temp['appointment']=$this->getBoardingDayCareData($value['reference_id']);

	                	$temp['username'] = 'N/A';
	                	// $temp['services'] = $temp['category'];
	                }
	                $temp['services'] = $this->getappointmentsdetailsforpayment($value);
	                $temp['app_user_name'] = $this->Management_model->getUsername($temp['appointment']['app_user_id']);
                }		           
                array_push($return['finaical'],$temp);
            }

        } 
        return $return;
	}

	function sendDiscountNotificationOnAndroid($app_user_id = 0,$notification_body = '', $fcm_id = ''){

		include_once(APPPATH."libraries/PushNotifications.php");
		$FCMPUSH = new PushNotifications();
		$dataArray = array('image'=>'','result'=>array('category' => 'appointment'),'appointment',$this->session->userdata('shop_id'));

		$badge = $this->getBagdeCountForUser($app_user_id,$this->session->userdata('shop_id'));
	    $data = array('body'=>$notification_body,'title'=>$notification_body,'badge'=>($badge+1),'data'=>$dataArray);
	    
	    $insert = array();
	    $insert['app_user_id'] = $app_user_id;
	    $insert['pet_id'] = 0;
	    $insert['title'] = $data['title'];
	    $insert['body'] = $data['body'];
	    $insert['data'] = serialize($dataArray);
	    $insert['added_on'] = date('Y-m-d H:i:s');
	    $insert['notification_type'] = 'discount';
	    $insert['shop_id'] = $this->session->userdata('shop_id');
	    $data['data']['notification'] =  $insert;
	    $this->db2->insert('notifications',$insert);
	    $FCMPUSH->android($data,$fcm_id);


	}

   function getAvailableDiscounts(){
	   	$todaysdate = date('Y-m-d');
	   	$this->db->select('id,discount_title,discount_percentage,start_date_time,end_date_time');
	   	$this->db->from('discounts');
	   	$this->db->where(array('is_deleted' => '0','start_date_time <='=> $todaysdate,'end_date_time >='=> $todaysdate));
	   	$this->db->or_where('start_date_time >=',$todaysdate);
	   
	   	return $this->db->get()->result_array();
   }

   function getActiveDiscount($app_user_id = 0){
   	$arrDiscount = $this->getAvailableDiscounts();
   	if(count($arrDiscount)){
   	    $ids = array();
		foreach ($arrDiscount as $row)
	    {
	        $ids[] = $row['id'];
	    }
	    $this->db->select('discount_id');
	    $this->db->from('discount_users');
	    $this->db->where(array('is_active' => '1', 'app_user_id' => $app_user_id));
	    $this->db->where_in('discount_id', $ids);
	    return $this->db->get()->result_array();
	}else{
		return array();
	}

   }

     /*function getAvailableDiscounts_1(){
	   	$todaysdate = date('Y-m-d');
	   	$this->db->select('d.id,d.discount_title,du.is_active');
	   	$this->db->from('discounts d');
	   	$this->db->join('discount_users du','d.id = du.discount_id');
	   	$this->db->where(array('d.is_deleted' => 0,'d.start_date_time <='=> $todaysdate,'d.end_date_time >='=> $todaysdate));
	   	$this->db->or_where('d.start_date_time >=', $todaysdate);
	   	//$this->db->order_by('du.is_active','DESC');
	    $this->db->group_by('d.id');
	   	return  $this->db->get()->result_array();
   }*/


   function addNewReservationTemp($data,$pet){

   		$insertArray = $data;
       	$insertArray['appointment_date'] = createDate($insertArray['appointment_date']);
        
		if(isset($insertArray['services']))					
        	unset($insertArray['services']);

        if(isset($insertArray['id']))					
        	unset($insertArray['id']);

        unset($insertArray['payment_source']);
        unset($insertArray['other_source']);

        $insertArray['appointment_time'] = date("H:i:s", strtotime($insertArray['appointment_time']));
		$insertArray['appointment_end_time'] = date("H:i:s", strtotime($insertArray['appointment_end_time']));

		$insertArray['status'] = 'Confirm';
		$insertArray['created_by'] = $this->session->userdata('first_name');
		$insertArray['timezone'] = get_option('time_zone');

		if(date("Y-m-d") == date('Y-m-d',strtotime($insertArray['appointment_date'])))
		{
			$insertArray['can_be_canceled'] = 1;
		}

		
		$start = date('Y-m-d H:i:s',strtotime($insertArray['appointment_date'].' '.$insertArray['appointment_time']));

    	$insertArray['discount_amount'] = 0;
        $insertArray['discount_id'] = 0;
    	$insertArray['deposit'] = 0 ;
    	$insertArray['credited_amount'] = 0 ;
    	$insertArray['isDuplicate'] = '1';
    	$insertArray['status'] = 'Cancel';
    	$insertArray['user_phone_no'] = preg_replace("/[^a-zA-Z0-9]+/", "", $insertArray['user_phone_no']);
		$this->db->insert('appointments',$insertArray);
		//echo $this->db->last_query();
		
		$appointmentId = $this->db->insert_id();

		/*********** New logic start   ******************/
		// if puppy power ic greater than 1 then create multiple records with isDublicate = 1
		if($insertArray['overlapping_appointment'] != 1 && $pet['puppy_power'] > 1)
		{
			$groomer_schedule  = $this->db->get_where('groomer_schedule',array('user_id'=>$insertArray['user_id'],'Date(date)'=>$insertArray['appointment_date']))->result_array();
			$insertArray1 = $insertArray;
			$powerCount = 1;
			foreach ($groomer_schedule as $key => $value) {
				
				if($value['id'] != $insertArray['allocated_schedule_id']){
					
					$insertArray1['allocated_schedule_id'] = $value['id'];
					$insertArray1['isDuplicate'] = '1';
					$insertArray1['Confirm'] = '1'; 
					$powerCount++;
					$this->db->insert('appointments',$insertArray1);
				}
				if($powerCount == $pet['puppy_power'] || $powerCount > $pet['puppy_power']) {
					break;
				}
			}
		}
		/*********** End New logic ******************/

		$i = 0; $commission = 0;
		foreach ($data['services'] as $key ) {
			
			$service = $this->db->get_where('services',array('id'=>$key))->row_array();
				$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$insertArray['user_id'],'service_id'=>$key))->row_array();

				if(!empty($groomer_services))
					$commission += ($service[$pet['size'].'_cost'] * ($groomer_services['commission']/100));

			if($i > 0)
					$start = $end;
	
				$end = date('Y-m-d H:i:s',strtotime($start.' +'.$service[$pet['size'].'_time_estimate'].' minutes'));


			$this->db->insert('appointment_services',array('service_id' => $key,'appointment_id'=>$appointmentId,'start'=>$start,'end' => $end));
			$i++;			 
		}

		if($commission > 0)
	    {
	    	$this->db->update('appointments',array('commission'=>$commission),array('id'=>$appointmentId));
	    }
	    return $appointmentId;
   	}

   /*Appointment schedule management start*/

   	function getBestAvailableTimeForSlot($date,$services,$pet_id,$appuser,$shop_no,$user_id,$start_time,$end_time)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 

		$array = explode(',', $services);
	   	$time_estimate = 0;
	   	foreach ($services as $key ) {
	        $temp = $this->db->get_where('services',array('id'=>$key))->row_array();
	        
	        $time_estimate += $temp[$pet['size'].'_time_estimate'];
	        $cost += $temp[$pet['size'].'_cost'];
	    }
	    
	    $end_time = date('H:i:s',strtotime($start_time.' + '.$time_estimate.' minutes'));

	    
	    if($return = $this->getBestAvailableSlotWithTime($array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate))
	    {
	    	//pr($end_time);exit;
	   		$return['time_estimate'] = $time_estimate;
	    	$return['cost'] = $cost;

	    	$temp_start = date('H:i:s',strtotime($return['start']));
	    	$temp_end = date('H:i:s',strtotime($return['end']));


	    	
	    	


			/*

			$temp_start = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$temp_start)),'H:i:s');
	    	$date1 = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$temp_start)),'Y-m-d');
	    	$temp_end = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$temp_end)),'H:i:s');

	    	// New logic for blocker (hard blocker and Lunch time)
	    	$str2 = "SELECT * FROM `groomer_schedule` WHERE `user_id` = '".$user_id."' AND `date` = '".$date."' 
	    	AND (
	    			( TIME('".$temp_start."') >= TIME(`hard_blocker_start`)
                 	AND TIME('".$temp_start."') < TIME(`hard_blocker_end`))

		            OR (TIME('".$temp_end."') > TIME(`hard_blocker_start`)
		                 AND TIME('".$temp_end."') <= TIME(`hard_blocker_end`))

            		OR(`hard_blocker_start` >= '".$temp_start."' AND `hard_blocker_end` < '".$temp_end."' )
           )";

           $query2 = $this->db->query($str2);
           $result2 = $query2->result_array();

          
           if(!empty($result2)){
           		return false;
           }

           $str2 = "SELECT * FROM `groomer_schedule` WHERE `user_id` = '".$user_id."' AND `date` = '".$date."' 
	    	AND (
	    			( TIME('".$temp_start."') >= TIME(`lunch_time_start`)
                 	AND TIME('".$temp_start."') < TIME(`lunch_time_end`))

		            OR (TIME('".$temp_end."') > TIME(`lunch_time_start`)
		                 AND TIME('".$temp_end."') <= TIME(`lunch_time_end`))

            		OR(`lunch_time_start` >= '".$temp_start."' AND `lunch_time_end` < '".$temp_end."' )
           )";

           $query2 = $this->db->query($str2);
           $result2 = $query2->result_array();
          
           if(!empty($result2)){
           		return false;
           }

           // New logic for blocker (soft blocker)
	    	$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date, 'blocker_start !='=> '', 'blocker_start <='=>$temp_start,'blocker_end >'=>$temp_start))->row_array();

	    	if (!empty($groomer_schedule)) {
	    		if ($groomer_schedule['blocker_end'] < $end_time){
		    		$return['start'] = $date .' '.$groomer_schedule['blocker_end'];
		    		$return['end'] = date('Y-m-d H:i:s',strtotime($return['start'].' + '.$time_estimate.' minutes'));
		    	}else{
		    		return false;
		    	}
	    	}
	    	*/

	    	$date1 = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$start_time)),'Y-m-d');
	    	$start_time = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$start_time)),'H:i:s');
	    	$end_time = server_date(date('Y-m-d H:i:s',strtotime($date.' '.$end_time)),'H:i:s');
	    	$appointment = $this->db->query('SELECT * FROM appointments WHERE STR_TO_DATE(CONCAT("'.$date1.'", " " ,"'.$start_time.'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$date1.'", " ","'.$end_time.'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") and  (pet_id = '.$pet_id.' ) and status not in ("Cancel","Checkout")')->result_array();

	    	
	    	if(!empty($appointment)){
	    		$return['already_appointment_exist'] = 1;
	    	}

	   		return $return;
	    }
	   	else
	    {
	   		return false;	
	    }

	    //Direct Order for services
	    //Means a,b,c
	    // if($r = getBestTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array))
	    // {
	    // 	$return['time_estimate'] = $time_estimate;
	    // 	$return['cost'] = $cost;
	    // 	$return['start'] = $r['start'];
	    // 	$return['end'] = $r['end'];

	}



	function getBestAvailableSlotWithTime($array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate)
	{

		
		$dataReturn  = array();

		$return['working_time'] = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date))->row_array();
		$return['working_time'] = groomer_scheduleTimeConverter($return['working_time']);


	    $temp = array();
	    $temp['start'] = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));

	    $dataReturn['start'] = $temp['start'];
	    $dataReturn['end']	= date('Y-m-d H:i:s',strtotime($temp['start'].' + '.$time_estimate.' minutes'));
	   
		$availableTime = $this->getAvailableTimeForBookingForSlotSelection($temp['start'],$date,$user_id,$start,$end,$array,$pet['size'],$time_estimate);


		$availableArray = array();
		
		if(!empty($availableTime))
		{

			foreach ($availableTime as $key => $value) {
				if(!empty($value['available_time']))
				{
					foreach ($value['available_time'] as $v) {
						# code...
						$t = array();
						$t = $v;
						$t['id'] = $value['id'];
						array_push($availableArray,$t);
					}
					
				
				}

			}
		}
		
		$NewavailableArray = array();
		$blocker_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['hard_blocker_start']));
		$blocker_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['hard_blocker_end']));
		$lunch_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['lunch_time_start']));
		$lunch_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['lunch_time_end']));
		$soft_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['blocker_start']));
		$soft_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['blocker_end']));

		foreach ($availableArray as $key => $v) {
			$tempflag = 0;
			if(($v['start'] < $blocker_start && $v['end'] > $blocker_start) || ($v['start'] < $blocker_end && $v['end'] > $blocker_end)){
				if($v['start'] < $blocker_start){
					$tempflag =1; 
					$t = array();
					$t['start'] = $v['start'];
					$t['end'] = $blocker_start;
					$t['id'] = $v['id'];
					array_push($NewavailableArray,$t);
					
				}
				if($v['end'] > $blocker_end){
					$tempflag =1;
					$t = array();
					$t['start'] = $blocker_end;
					$t['end'] = $v['end'];
					
					$t['id'] = $v['id'];
					array_push($NewavailableArray,$t);
				}
			}
			if(!$tempflag){
				array_push($NewavailableArray,$v);
			}
		}
		
		$availableArray = array();
		foreach ($NewavailableArray as $key => $v) {
			$tempflag = 0;
			if(($v['start'] < $lunch_start && $v['end'] > $lunch_start) || ($v['start'] < $lunch_end && $v['end'] > $lunch_end)){
				if($v['start'] < $lunch_start){
					$tempflag =1; 
					$t = array();
					$t['start'] = $v['start'];
					$t['end'] = $lunch_start;
					$t['id'] = $v['id'];
					array_push($availableArray,$t);	
				}
				if($v['end'] > $lunch_end){
					$tempflag =1;
					$t = array();
					$t['start'] = $lunch_end;
					$t['end'] = $v['end'];
					$t['id'] = $v['id'];
					array_push($availableArray,$t);
				}
			}
			if(!$tempflag){
				array_push($availableArray,$v);
			}
		}

		$servicesOrder = implode (",",$array);

		$appointment = array();
		$appointment['order'] = $servicesOrder;
		
		//New logic written by ajay-start
		$return['appointments'] = array();
		$dataReturn['overbooking'] = 0;

		
		$firstSlotId = $availableArray[0]['id'];
		$stagger_time = get_option('stagger_time');
		//pr($availableArray);exit;
		if(!empty($availableArray)){
			foreach ($availableArray as $key => $value) {

				
				$appointment['allocated_schedule_id'] = $value['id'];
				$appointment['start'] = $temp['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($temp['start'].' + '.$time_estimate.' minutes'));
				$powerError = 0;
				
				
				if($value['start'] <= $appointment['start'] && $appointment['end'] <= $value['end'])
				{	
					$schedule_start = $appointment['start'];
					$schedule_end = date('Y-m-d H:i:s',strtotime($schedule_start.' + 60 minutes'));

					do{

						//check soft blocke
						$flg_done = 0;
						if( $soft_start <= $appointment['start'] && $soft_end > $appointment['start'] ){
							$flg_done = 1;
							$appointment['start'] = $soft_end;
							$appointment['end'] =  date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));

						}
						
						// check stagger time for 2nd schedule slot
						if($firstSlotId != $value['id']){


							$BackAppointmentCheck = date('H:i:s',strtotime($appointment['start'].' -'.$stagger_time.' minutes'));
							$AheadAppoinmentCheck = date('H:i:s',strtotime($appointment['start'].' +'.$stagger_time.' minutes'));

							$BackAppointmentCheck = date('Y-m-d H:i:s',strtotime($date.' '.$BackAppointmentCheck));
							$AheadAppoinmentCheck = date('Y-m-d H:i:s',strtotime($date.' '.$AheadAppoinmentCheck));

							$BackAppointmentCheck = server_date($BackAppointmentCheck,'Y-m-d H:i:s');
							$AheadAppoinmentCheck = server_date($AheadAppoinmentCheck,'Y-m-d H:i:s');
							$appointment_start = server_date($appointment['start'],'Y-m-d H:i:s');


							$this->db->where('STR_TO_DATE("'.$BackAppointmentCheck.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');
	    					$this->db->where('STR_TO_DATE("'.$appointment_start.'", "%Y-%m-%d %H:%i:%s") >= STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');
							//$this->db->where(array('appointment_time >'=>$BackAppointmentCheck,'appointment_time <='=>date('H:i:s',strtotime($appointment_start))));
							$CheckAppointment1 = $this->db->get_where('appointments',array('user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array();
							
							
							
							$this->db->where('STR_TO_DATE("'.$AheadAppoinmentCheck.'", "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');
	    					$this->db->where('STR_TO_DATE("'.$appointment_start.'", "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');

							//$this->db->where(array('appointment_time <'=>$AheadAppoinmentCheck,'appointment_time >'=>date('H:i:s',strtotime($appointment_start))));
							$CheckAppointment2 = $this->db->get_where('appointments',array('user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array();
							
							
							
							if(!empty($CheckAppointment1) || !empty($CheckAppointment2)){
								$flg_done = 1;
								$appointment['start'] = date('H:i:s',strtotime($appointment['start'].' +'.$stagger_time.' minutes'));
								$appointment['end'] =  date('H:i:s',strtotime($appointment['end'].' +'.$stagger_time.' minutes'));
							}
						}
						
						// Check schedule time
						$powerError = 0;
						
						if($value['end'] >= $appointment['end'] && $schedule_end >= $appointment['start'])
						{	

							if($pet['puppy_power'] > 1){
								$count = 0;
								
								foreach ($availableArray as $key1 => $value1){
									if($value1['start'] <= $appointment['start'] && $value1['end'] >= $appointment['end'] ){
										$count++;
									}
								}

								//Check power
								//echo $count;exit;
								if($count >= $pet['puppy_power']){
									$dataReturn['normal_booking_time'] = $appointment;
									return $dataReturn;
								}else{
									$flg_done = 1;
									$powerError = 1;
								}
							}else{
								$dataReturn['normal_booking_time'] = $appointment;

								return $dataReturn; 
							}	
						}else{
							$flg_done = 1;
							// increase THE APPOINTMENT START TIME TO BREAK THE LOOP
							$appointment['start'] = $value['end'];
							$appointment['end'] =  date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
						}

						
					}
					while($appointment['start'] < $schedule_end && $powerError == 0);	
				}
			}
			
			$dataReturn['appointments'] = array();
			$dataReturn['normal_booking_time'] = array();
			$dataReturn['overbooking'] = 1;
			if($powerError>0){
				//$dataReturn['overbookingReason'] = "Puppy Power is more than upper limit.";
				$dataReturn['overbookingReason'] = "Pet(s) with larger Puppy Power are booked at this time";
			}else{
				$dataReturn['overbookingReason'] = "Groomer has appointments already booked";
			}
			//pr($dataReturn);exit;
			return $dataReturn;
		}
		return false;
		//New logic written by ajay-End





		//pr($availableArray);exit;

		//Real code comment by ajay

		/*foreach ($availableArray as $key => $value) {

			//echo"temp start :".$temp['start']exit;
			// echo "time1".$value['start'];
			// echo "<br>time2".$temp['start'];
			if($value['start'] <= $temp['start'])
			{
				//Chec
				//echo "here";
				//pr($value);pr($temp);
				$appointment['start'] = $temp['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
				$appointment['allocated_schedule_id'] = $value['id'];

				//pr($appointment);
				//First Check If Appointment

				$this->db->where(array('appointments.appointment_time <'=> date('H:i:s',strtotime($appointment['end'])), 'appointments.appointment_end_time >'=> date('H:i:s',strtotime($appointment['start']))));

				$checkSameSlotAppointment = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$value['id'],'overlapping_appointment'=>0))->result_array(); 


				$return = array();
				$return = $this->finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate);
				if(!empty($return))
					return $return;
				
			}
			else if($value['start'] > $temp['start'] && $temp['end'] > $value['start'])
			{
				// pr($value);pr($temp);exit;

				// echo "here1";
				$appointment['start'] = $value['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
				$appointment['allocated_schedule_id'] = $value['id'];
				//pr($appointment);

				$return = array();
				$return = $this->finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate);
				if(!empty($return))
					return $return;
			}		
		}*/
	




	}



	function getAvailableTimeForBookingForSlotSelection($start,$date,$user_id,$schedule_start,$schedule_end,$service,$pet_size,$time_estimate)
	{
		$array = array();
		$return = array();
		$result = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date(date)'=>$date))->result_array();
		
		if(!empty($result))
		{
			foreach ($result as $key => $value) 
			{
				$array = array();
				
				$value = groomer_scheduleTimeConverter($value);
				
				$array = $value;
				$array['time_estimate'] = $time_estimate;
				$array['available_time'] = $this->getAvailableTimeByAfterAppointment($date,$user_id,$value['start'],$value['end'],$service,$pet_size,$time_estimate,$value,$start);			
				array_push($return,$array);
				
			}	
		}

		return $return;
	}


		function finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate)
	{
		do
		{
			//echo "loop".$appointment['start'];
			$this->db->where(array('appointments.appointment_time <'=> date('H:i:s',strtotime($appointment['end'])), 'appointments.appointment_end_time >'=> date('H:i:s',strtotime($appointment['start']))));

			$checkSameSlotAppointment = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$value['id'],'overlapping_appointment'=>0))->result_array(); 

			// echo $this->db->last_query();
			// pr($checkSameSlotAppointment);

			//$x = 15;
			$x = get_option('stagger_time');
			if(empty($checkSameSlotAppointment))
			{
				
				$BackAppointmentCheck = date('H:i:s',strtotime($appointment['start'].' -'.$x.' minutes'));
				$AheadAppoinmentCheck = date('H:i:s',strtotime($appointment['start'].' +'.$x.' minutes'));

				$this->db->where(array('appointment_time >'=>$BackAppointmentCheck,'appointment_time <='=>date('H:i:s',strtotime($appointment['start']))));
				$CheckAppointment1 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 
				
				$this->db->where(array('appointment_time <'=>$AheadAppoinmentCheck,'appointment_time >'=>date('H:i:s',strtotime($appointment['start']))));
				$CheckAppointment2 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 	
				
				//pr($CheckAppointment1); pr($CheckAppointment2);pr($appointment);exit;

				if(empty($CheckAppointment1) && empty($CheckAppointment2))
					return $appointment; 

			}
			$appointment['start'] = date('Y-m-d H:i:s',strtotime($appointment['start'].' +'.$x.' minutes'));
			$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));	

		}
		while(date('H:i:s',strtotime($temp['end'])) > date('H:i:s',strtotime($appointment['start'])));
		//while(date('H:i:s',strtotime($temp['end'])) > date('H:i:s',strtotime($appointment['start'])));
	}



	function getAvailableTimeByAfterAppointment($date,$user_id,$schedule_start,$schedule_end,$service,$pet_size,$time_estimate,$groomer_schedule,$start)
	{

		//$appointment_date = server_date($start,'Y-m-d');
		$date1 = date('Y-m-d H:i:s',strtotime($date.' 00:00:00'));
		$date2 = date('Y-m-d H:i:s',strtotime($date.' 23:59:59'));
		
		$date1 = server_date($date1,'Y-m-d H:i:s');
		$date2 = server_date($date2,'Y-m-d H:i:s');
		

		$array = array();
		$this->db->order_by('appointments.appointment_time');
		$this->db->where('STR_TO_DATE("'.$date1.'", "%Y-%m-%d %H:%i:%s") <= STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_time), "%Y-%m-%d %H:%i:%s")');
	    $this->db->where('STR_TO_DATE("'.$date2.'", "%Y-%m-%d %H:%i:%s") >= STR_TO_DATE(CONCAT(appointments.appointment_date, " ", appointments.appointment_end_time), "%Y-%m-%d %H:%i:%s")');
		$appointment = $this->db->get_where('appointments',array('user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$groomer_schedule['id'],'overlapping_appointment'=>0))->result_array();
		
		
		// foreach to convet UTC time in system time 
		foreach ($appointment as $key => $value) {
			$appointment[$key] = groomingTimeConverter($value);
		}
		
		if(empty($appointment))
		{
			$temp = array();
			$temp['start'] = $schedule_start;
			$temp['end'] = $schedule_end;
			array_push($array,$temp); 
			//return $array;
		}
		else
		{
			$count = count($appointment);
		
			if($count == 1)
			{

				//Single Appointment
				$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[0]['appointment_date'].' '.$appointment[0]['appointment_time']));
				//$appointmentStartTime1 = user_date($appointmentStartTime1,'Y-m-d H:i:s');

				$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[0]['appointment_date'].' '.$appointment[0]['appointment_end_time']));
				//$appointmentEndTime2 = user_date($appointmentEndTime2,'Y-m-d H:i:s');

				
				if($appointmentStartTime1 == $schedule_start)
				{
					
					$temp['start'] = $appointmentEndTime2;
					$temp['end'] = $schedule_end;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					
					if($diff >= $time_estimate)
					{
						
						array_push($array,$temp); 
						 
					}
					else
					{
						//Slot is not available
					}
				}
				else if($appointmentEndTime2 == $schedule_end)
				{
					$temp['start'] = $schedule_start;
					$temp['end'] = $appointmentStartTime1;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						 array_push($array,$temp);  
						 //return $array;
					}
					else
					{
						//Slot is not available

					}
				}
				else
				{	
					//Appointment is in middle 
					$temp['start'] = $schedule_start;
					$temp['end'] = $appointmentStartTime1;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						array_push($array,$temp); 

					}
					else
					{
						
						//Slot is not available

					}
					
					$temp['start'] = $appointmentEndTime2;
					$temp['end'] = $schedule_end;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						array_push($array,$temp); 
					}
					else
					{
						//Slot is not available
					}
				}

				//return $array;	

			}
			else
			{
				//echo "More than one";
				
				for($i=0;$i<$count;$i++)
				{
					//echo $i."<br><br>";

					if($i+1 == $count)
					{
						$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_time']));
						//$appointmentStartTime1 = user_date($appointmentStartTime1,'Y-m-d H:i:s');

						$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_end_time']));
						//$appointmentEndTime2 = user_date($appointmentEndTime2,'Y-m-d H:i:s');

						if($schedule_end != $appointmentEndTime2)
						{
							$gapStart = $appointmentEndTime2;
							$gapEnd = $schedule_end;
							
							$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
							
							if($diff >= $time_estimate)
							{
								$temp['start'] = $gapStart;
								$temp['end'] = $gapEnd;
								array_push($array,$temp); 
							}
							else
							{
								//Slot is not available
							}
						}
						else
						{
							//Appointment is at the end
						}
					}
					else
					{
						//echo $count;

						$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_time']));
						//$appointmentStartTime1 = user_date($appointmentStartTime1,'Y-m-d H:i:s');
						$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_end_time']));
						//$appointmentEndTime2 = user_date($appointmentEndTime2,'Y-m-d H:i:s');

						$appointmentStartTimeNext1 = date('Y-m-d H:i:s',strtotime($appointment[$i+1]['appointment_date'].' '.$appointment[$i+1]['appointment_time']));
						//$appointmentStartTimeNext1 = user_date($appointmentStartTimeNext1,'Y-m-d H:i:s');
						$appointmentEndTimeNext2 = date('Y-m-d H:i:s',strtotime($appointment[$i+1]['appointment_date'].' '.$appointment[$i+1]['appointment_end_time']));
						//$appointmentEndTimeNext2 = user_date($appointmentEndTimeNext2,'Y-m-d H:i:s');

						if($schedule_start != $appointmentStartTime1 && $schedule_start < $appointmentStartTime1)
						{
							//echo "1";
							//Means 1st appointment is greater than schedule time
							
							if($i == 0)
								$gapStart = $schedule_start;
							else
								$gapStart = $appointmentEndTime2;

							if($i == 0)
								$gapEnd = $appointmentStartTime1;
							else
								$gapEnd = $appointmentStartTimeNext1;

							// pr($gapStart);pr($gapEnd);
							$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
							if($diff >= $time_estimate)
							{
								$temp['start'] = $gapStart;
								$temp['end'] = $gapEnd;
								array_push($array,$temp);
								//pr($temp);
								//Add Gap after 1 st slott 
							}
							else
							{
								//Slot is not available
							}

							if($appointmentEndTime2 ==  $appointmentStartTimeNext1)
							{
								//No Gap
							}
							else
							{
								//echo '2';
								$gapStart = $appointmentEndTime2;
								$gapEnd = $appointmentStartTimeNext1;
								//pr($gapStart);pr($gapEnd);
								$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
								if($diff >= $time_estimate)
								{
									$temp['start'] = $gapStart;
									$temp['end'] = $gapEnd;
									array_push($array,$temp); 
									//pr($temp);
								}
								else
								{
									//Slot is not available
								}
							}
						}
						else 
						{
							//First appointment is at the start


							if($appointmentEndTime2 ==  $appointmentStartTimeNext1)
							{
								//No Gap go for next loop
							}
							else if($appointmentEndTime2 < $appointmentStartTimeNext1)
							{

								$gapStart = $appointmentEndTime2;
								$gapEnd = $appointmentStartTimeNext1;
								//pr($gapStart);pr($gapEnd);exit;

								$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
								
								if($diff >= $time_estimate)
								{
									$temp['start'] = $gapStart;
									$temp['end'] = $gapEnd;
									
									array_push($array,$temp); 

								}
								else
								{
									//Slot is not available
								}
							}
						}
					}
				}	
			}	
		}
		
		if(!empty($array))
			return $array;
		else
			return false;

	}



   /*Appointment schedule management end*/

   /*Appointment cost calculation start*/
   	function calculateAppointmentCost($postarray){
   		$result = array();
   		$appointmentDetails = $this->Entry_model->getSingleAppointmentId($postarray['appointment_id']);
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);

		$cost = floatval($appointmentDetails['cost'] + $postarray['additional_cost1']);
		$result['cost'] = $cost;
		$result['app_user_id'] = $AppUserDetails[0]['id'];
		$result['credit_balance'] = $AppUserDetails[0]['credit_balance'];
		/* Appointment Discount Logic Start */
		$arrDiscount = $this->getUserDiscountArr($appointmentDetails['app_user_id'],date('Y-m-d'),'appointment');
		$arrDiscountActive = $this->Management_model->getActiveDiscount($appointmentDetails['app_user_id']);

		$discount_percentage = 0;
		$result['discount_id'] = 0;
		foreach ($arrDiscount as $row) {
			if($arrDiscountActive[0]['discount_id'] == $row['discount_id']){
				$discount_percentage = $row['discount_percentage'];
		       	$result['discount_id'] = $row['discount_id'];
				break;
			}
		   	if($row['discount_percentage'] >= $discount_percentage){
		       $discount_percentage = $row['discount_percentage'];
		       $result['discount_id'] = $row['discount_id'];
		   	}
		}

		$result['discount_amount'] = 0;
		if($discount_percentage > 0){
		   $result['discount_amount'] = floatval((($cost / 100) * $discount_percentage));
		   $cost = floatval($cost - $result['discount_amount']);
		}
    	/* Appointment Discount Logic End */
    	
    	/* MAnnual Discount Logic End */
    	$result['manual_discount'] = 0;
		if($postarray['manual_discountPer'] > 0){
 			$result['manual_discount'] = (($cost / 100) * $postarray['manual_discountPer']);
 			
 		}else{
 			$result['manual_discount'] = $postarray['manual_discount'];	
 		}
 		
 		$result['deposit'] = $appointmentDetails['deposit'];
 		$cost = floatval($cost - $result['manual_discount']-$appointmentDetails['deposit']);

 		/* MAnnual Discount Logic End */
 		if($cost > 0){

 			if($AppUserDetails[0]['credit_balance'] > $cost){
 				$result['credited_amount'] = $cost;
 				$result['remaining_cost'] = 0;
 			}else{
 				$result['remaining_cost'] = $cost - $AppUserDetails[0]['credit_balance'];
 				$result['credited_amount'] = $AppUserDetails[0]['credit_balance'];
 			}	
 		}else{
 			$result['remaining_cost'] = $cost;
			$result['credited_amount'] = 0.00;
 		}
 		
 		$result['cost'] = number_format((float)$result['cost'], 2, '.', '');

 		$result['remaining_cost'] = number_format((float)$result['remaining_cost'], 2, '.', '');
 		$result['remaining_cost'] = str_replace("-0.00", "0.00", $result['remaining_cost']);

 		$result['appointmentDetails'] = $appointmentDetails;

 		$services=$this->Entry_model->getAllServices();
 		$appointmentService=$this->Entry_model->getAppointmentServiceById($appointmentDetails['id']);

 		
 		$pet_size = $this->db2->select('size')->get_where('pets',array('id' => $appointmentDetails['pet_id']))->row_array();
        $size_cost = $pet_size['size'].'_cost';
        
        $i=0;
        foreach($appointmentService as  $value){
        	if($appointmentDetails['temp_service_entry'] == 1){
        		$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $value))->row_array();
        	}else{
            	$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
        	}
            if(!empty($serviceData)){
               	$result['appointmentService'][$i]['service_name'] = $serviceData['name'];
               	$result['appointmentService'][$i]['service_cost'] = $serviceData[$size_cost];
                $i++;
            }
        }
        
        $result['add_on_services'] = array();

		if($appointmentDetails['add_on_service_cost'] > 0){
	    	$appointmentAddOnService =$this->Entry_model->getAppointmentAddOnServiceById($appointmentDetails['id']);
	    	$i = 0;
    		foreach($appointmentAddOnService as  $value){
                if($appointmentDetails['temp_service_entry'] == 1){
                   $addonserviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $value))->row_array();
               	}else{
                   $addonserviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
               	}
                if(!empty($addonserviceData)){
                    $result['add_on_services'][$i]['service_name'] = $addonserviceData['name'];
                    $result['add_on_services'][$i]['service_cost'] = $addonserviceData[$size_cost];
                    $i++;
                }
            }
    	}
		$result['postarray'] = $postarray;
		$result['postarray']['additional_note1'] = trim($result['postarray']['additional_note1']);
		$result['appointmentDetails']['note'] = trim($result['appointmentDetails']['note']);
		
	 	return $result;
	}
   /*Appointment cost calculation end*/

   /*Boarding/Daycare cost calculation start*/
   	function calculateBoardingCost($postarray){
   		$result = array();
   		$appointmentDetails = $this->getBoardingDayCareData($postarray['appointment_id']);
   		
		$AppUserDetails = $this->getSingleUserData($appointmentDetails['app_user_id']);
		$petDetails = $this->Entry_model->getPetsViewDetails($appointmentDetails['pet_id']);
		
		$cost = floatval($appointmentDetails['amount'] + $postarray['additional_cost']);

		/* Overstay for boarding Logic Start */
		$result['end_date'] = $appointmentDetails['end_date'];
		$result['overstayAmt'] = 0;
		if(!empty($postarray['overstay']) && $postarray['category'] == 'boarding'){
 			$boardingSettings = $this->Management_model->getBoardingSettings();
			if(strtolower($petDetails['type']) == 'dog')
 				$BoardingPrice = $boardingSettings['dogBoardingPricing'];
 			else
 				$BoardingPrice = $boardingSettings['catBoardingPricing'];

			$postarray['overstay'] = date('Y-m-d',strtotime($postarray['overstay']));
			$result['end_date'] = $postarray['overstay'];
 			if($postarray['overstay'] >= $appointmentDetails['end_date']){
 				$datediff = strtotime($postarray['overstay']) - strtotime($appointmentDetails['end_date']);
				$datediff = round($datediff / (60 * 60 * 24));
				$overstayAmt = $BoardingPrice * $datediff;
 				$cost = $cost + $overstayAmt;
 				$result['overstayLabel'] = 'Overstay';
 			}
 			else{
 				$datediff = strtotime($appointmentDetails['end_date']) - strtotime($postarray['overstay']);
				$datediff = round($datediff / (60 * 60 * 24));
				if($postarray['overstay'] == $appointmentDetails['start_date']){
					if($datediff == 1){
						$datediff = 0;
					}else{
						$datediff = $datediff - 1;
					}
				}
				$overstayAmt = $BoardingPrice * $datediff;
 				$cost = $cost - $overstayAmt;
 				$overstayAmt = 0 - $overstayAmt;
 				$result['overstayLabel'] = 'Understay';
 			}
 			$result['overstayAmt'] = number_format((float)$overstayAmt, 2, '.', '');
 		}
 		/* Overstay for boarding Logic End */
 		
		$result['app_user_id'] = $AppUserDetails[0]['id'];
		$result['credit_balance'] = $AppUserDetails[0]['credit_balance'];
		$result['additional_cost_array'] = array();
		
		
		// check invoice 
 		$check_appointment = $this->Management_model->checkAppointmentExistForPet($petDetails['id'],$appointmentDetails['start_time'],$appointmentDetails['end_time'],$appointmentDetails['start_date'],$appointmentDetails['end_date']);
 		
 		$result['appointment_id'] = 0;
 		//$result['appointmentdetails1'] = array();
	    if(!empty($check_appointment)){
	    	$i = 0;
	    	foreach ($check_appointment as $key => $value) {
    			$cost = $cost + $value['cost'];
    			$appointmentDetails['deposit'] = $appointmentDetails['deposit'] + $value['deposit'];
    			if($value['additional_cost'] > 0){
	    			$result['additional_cost_array'][$i]['additional_cost'] = $value['additional_cost'];
					$result['additional_cost_array'][$i]['additional_note'] = $value['note'];
					$i++;
				}
	    	}
	    	$appointmentDetails['deposit'] = number_format((float)$appointmentDetails['deposit'], 2, '.', '');
	    	
	    	$result['appointmentdetails1'] = $this->appointment_boarding($check_appointment);      
	    }
	      
	    
	    $result['cost'] = $cost;
		/* Appointment Discount Logic Start */
		$arrDiscount = $this->getUserDiscountArr($appointmentDetails['app_user_id'],date('Y-m-d'),$postarray['category']);
		$arrDiscountActive = $this->Management_model->getActiveDiscount($appointmentDetails['app_user_id']);

        $discount_percentage = 0;
        $result['discount_id'] = 0;

        foreach ($arrDiscount as $row) {
        	if($arrDiscountActive[0]['discount_id'] == $row['discount_id']){
				$discount_percentage = $row['discount_percentage'];
		       	$result['discount_id'] = $row['discount_id'];
				break;
			}
            if($row['discount_percentage'] >= $discount_percentage){
                $discount_percentage = $row['discount_percentage'];
		       	$result['discount_id'] = $row['discount_id'];
            }
        }

        $result['discount_amount'] = 0;
        if($discount_percentage > 0){
            $result['discount_amount'] = floatval((($cost / 100) * $discount_percentage));
		   $cost = floatval($cost - $result['discount_amount']);
        }
        $result['discount_amount'] = number_format((float)$result['discount_amount'], 2, '.', '');
    	/* Appointment Discount Logic End */
    	
    	
    	/* MAnnual Discount Logic start */
    	$result['manual_discount'] = 0;
		if($postarray['manual_discountPer'] > 0){
 			$result['manual_discount'] = (($cost / 100) * $postarray['manual_discountPer']);
 			
 		}else{
 			$result['manual_discount'] = $postarray['manual_discount'];	
 		}
 		$result['manual_discount'] = number_format((float)$result['manual_discount'], 2, '.', '');
 		
 		$result['deposit'] = number_format((float)$appointmentDetails['deposit'], 2, '.', '');
 		$cost = floatval($cost - $result['manual_discount']-$appointmentDetails['deposit']);

 		/* MAnnual Discount Logic End */

	    /* credited Logic start */  
	    if($cost > 0){

 			if($AppUserDetails[0]['credit_balance'] > $cost){
 				$result['credited_amount'] = $cost;
 				$result['remaining_cost'] = 0.00;
 			}else{
 				$result['remaining_cost'] = $cost - $AppUserDetails[0]['credit_balance'];
 				$result['credited_amount'] = $AppUserDetails[0]['credit_balance'];
 			}	
 		}else{
 			$result['remaining_cost'] = $cost;
			$result['credited_amount'] = 0.00;
 		}
 		/* credited Logic End */  

 		$result['cost'] = number_format((float)$result['cost'], 2, '.', '');
 		$result['remaining_cost'] = number_format((float)$result['remaining_cost'], 2, '.', '');
 		$result['remaining_cost'] = str_replace("-0.00", "0.00", $result['remaining_cost']);
 		$result['appointmentDetails'] = $appointmentDetails;
 		$result['postarray'] = $postarray;
		$result['postarray']['additional_note'] = trim($result['postarray']['additional_note']);
		
 		return $result;
   	}
   /*Appointment cost calculation end*/

   	function appointment_boarding($appointments){
		$data = array();
		$data['appointmentServiceInfo'] = array();
		$data['addonappointmentServiceInfo'] = array();

		foreach ($appointments as $key1 => $value1) {
			$data['app_additional_cost'] += $value1['additional_cost'];
            $appointmentService = $this->Entry_model->getAppointmentServiceById($value1['id']);
          	
            $pet_size = $this->db2->select('size')->get_where('pets',array('id' => $value1['pet_id']))->row_array();
            $size_cost = $pet_size['size'].'_cost';
                    
            $temp = array();
            foreach ($appointmentService as $key2 => $value2) {
                if($value1['temp_service_entry'] == 1){
                    $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $value2))->row_array();
                }else{
                    $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value2))->row_array();
                }
                if(!empty($serviceData)){
                    $temp[$key2] = array('service_id'=>$value2,'service_name'=>$serviceData['name'],'service_cost'=>$serviceData[$size_cost]);   
                }   
            }
            array_push($data['appointmentServiceInfo'], $temp);

            $temp = array();
            $appointmentAddOnService = $this->Entry_model->getAppointmentAddOnServiceById($value1['id']);

            if($value1['add_on_service_cost']>0){

            	foreach ($appointmentAddOnService as $key2 => $value2) {
            		
                    if($value1['temp_service_entry'] == 1){
                        $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $value2))->row_array();
                    }else{
                        $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value2))->row_array();
                    }
                    if(!empty($serviceData)){
                        $temp[$key2] = array('service_id'=>$value2,'service_name'=>$serviceData['name'],'service_cost'=>$serviceData[$size_cost]);   
                    }
            	}
            	array_push($data['addonappointmentServiceInfo'], $temp);
            }
            
        }

       return $data;  
    }




    

	function getUserDiscountArr($app_user_id = 0,$appointment_date = '',$allow_type = 'appointment'){
		$appointment_date = date('Y-m-d',strtotime($appointment_date));
		//$appointment_date = date('Y-m-d');
		$this->db->select();
		$this->db->from('discount_users dusers');
		$this->db->join('discounts d',"d.id = dusers.discount_id");
		//$this->db->where(array('dusers.app_user_id' => $app_user_id);
		$this->db->like('allow_type', $allow_type);
		$this->db->where(array('dusers.app_user_id' => $app_user_id,'d.is_deleted' => '0'));
			$this->db->where(array('d.start_date_time <=' => $appointment_date,'d.end_date_time >=' => $appointment_date));

		//$this->db->where('date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$result = $this->db->get()->result_array();
		//echo $this->db->last_query();exit;
		
		return $result;
	}

	function getinvoiceno(){
       $this->db->select_max("invoice_no");
       $this->db->from("appointments");
       $lastappointment = $this->db->get()->row_array();
       // echo $this->db->last_query();
       $this->db->select_max("invoice_no");
       $this->db->from("boardings");
       // $this->db->order_by('id',"DESC");
       $lastboarding = $this->db->get()->row_array();

       // echo $this->db->last_query();
       $this->db->select_max("invoice_no");
       $this->db->from("quick_invoice");
       $lastquick_invoice = $this->db->get()->row_array();
       // echo $this->db->last_query();exit;

       if(($lastappointment['invoice_no'] >= $lastboarding['invoice_no']) && ($lastappointment['invoice_no'] >= $lastquick_invoice['invoice_no'])){

           $invoice_no = $lastappointment['invoice_no']+1;
       }else if(($lastappointment['invoice_no'] <= $lastboarding['invoice_no']) && ($lastboarding['invoice_no'] >= $lastquick_invoice['invoice_no'])){

           $invoice_no = $lastboarding['invoice_no']+1;
       }else{
           $invoice_no = $lastquick_invoice['invoice_no']+1;
       }
       return $invoice_no;
   }

   function getFinalInvoice($postarray){
        //this will generate merged invoice
   	// pr($postarray);exit;
    //     pr($_POST);exit;
   		// $_POST = $postarray;
        	// echo "DScsd";
   		 // pr($postarray);exit;
        if(!empty($postarray['appointment_id'])){
        	//  pr($_POST);
        	// exit;
        	$currentappointment = array($postarray['appointment_id'],$postarray['category']);
        	
        	if(empty($postarray['allappointments'])){
        		$postarray['allappointments'][0] = $currentappointment;
        	}else{
        		array_unshift($postarray['allappointments'],$currentappointment);
        	}
        	
			$AppUserDetails = $this->getSingleUserData($postarray['app_user_id']);
			 // pr($AppUserDetails);
			if(!empty($postarray['autodiscount_id'])){
					
					$id= $postarray['app_user_id'];
					$this->db->update('discount_users',array('is_active' => '0'),array('app_user_id' => $id));
					$chkIsAvail = $this->db->get_where('discount_users',array('discount_id' => $postarray['autodiscount_id'], 'app_user_id' => $id))->row_array();
					
					if(count($chkIsAvail) == 0){
						$this->db->insert('discount_users',array( 'discount_id' => $postarray['autodiscount_id'], 'app_user_id' => $id,'is_active' => '1'));
					}else{
						$this->db->update('discount_users',array('is_active' => '1'),array('app_user_id' => $id,'discount_id' => $postarray['autodiscount_id']));
					}
					
			}

			$result = array();
			$result['autodiscount_id'] = $postarray['autodiscount_id'];
			$activeappointmentDetails = array();
			$i = 0;
			$overstayAmt = 0;
			$discount_amount = 0;
			$appointmentamount = 0;
		

			foreach ($postarray['allappointments'] as $key => $value) {
				
				if($value[1] == 'Grooming'){
					
					$activeappointmentDetails[$i] = $this->Entry_model->getSingleAppointmentId($value[0]);
					$allappointmentcost += floatval($activeappointmentDetails[$i]['cost']);
					$allappointmentdeposit += floatval($activeappointmentDetails[$i]['deposit']);
					$additional_cost += floatval($activeappointmentDetails[$i]['additional_cost']);
					$appointmentService = $this->Entry_model->getAppointmentServiceById($activeappointmentDetails[$i]['id']);

		            $appointmentServiceInfo = array();
		            $addonappointmentServiceInfo = array();
	                
	                $pet_size = $this->db2->select('size,name')->get_where('pets',array('id' => $activeappointmentDetails[$i]['pet_id']))->row_array();

	                $size_cost = $pet_size['size'].'_cost';
	                $k=0;

	                foreach($appointmentService as  $row){
	                    // $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $row))->row_array();
	                    if($activeappointmentDetails[$i]['temp_service_entry'] == 1){
	                		$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $row))->row_array();
	                	}else{
	                    	$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $row))->row_array();
	                	}
	                    if(!empty($serviceData)){
	                        $appointmentServiceInfo[$k]['service_name'] = $serviceData['name'];
	                        $appointmentServiceInfo[$k]['service_cost'] = $serviceData[$size_cost];
	                        $k++;
	                    }
	                }
	                $j=0;
	            	if($activeappointmentDetails[$i]['add_on_service_cost'] != 0){
	            		$add_on_services = $this->getAddOnServices($activeappointmentDetails[$i]['id'],'appointment');
	            		$appointmentAddOnService=$this->Entry_model->getAppointmentAddOnServiceById($activeappointmentDetails[$i]['id']);
	            		foreach($appointmentAddOnService as  $row2){
	                        // $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $row2))->row_array();
	                        if($activeappointmentDetails[$i]['temp_service_entry'] == 1){
		                		$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('temp_services',array('id' => $row2))->row_array();
		                	}else{
		                    	$serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $row2))->row_array();
		                	}
	                        if(!empty($serviceData)){
	                            $addonappointmentServiceInfo[$j]['service_name'] = $serviceData['name'].'(Add on service)';
	                            $addonappointmentServiceInfo[$j]['service_cost'] = $serviceData[$size_cost];
	                            $j++;
	                        }
	                    }

	            	}
	            	$activeappointmentDetails[$i]['petname'] = ucfirst($pet_size['name']);
	            	$activeappointmentDetails[$i]['appointmentServiceInfo'] = $appointmentServiceInfo;
					$activeappointmentDetails[$i]['addonappointmentServiceInfo'] = $addonappointmentServiceInfo;
					$appointmentamount = $activeappointmentDetails[$i]['cost'];
					$activeappointmentDetails[$i]['category'] = 'appointment';
					
				}else{
					// echo "fds";
		   			$activeappointmentDetails[$i] = $this->getBoardingDayCareData($value[0]);

		   			$allappointmentcost += floatval($activeappointmentDetails[$i]['amount']);
					$allappointmentdeposit += floatval($activeappointmentDetails[$i]['deposit']);
					/* Overstay for allboarding Logic Start */
					
					$activeappointmentDetails[$i]['overstayAmt'] = 0 ;
					$activeappointmentDetails[$i]['overstayLabel'] = '';
					$petDetails = $this->Entry_model->getPetsViewDetails($activeappointmentDetails[$i]['pet_id']);
					if($activeappointmentDetails[$i]['category'] == 'boarding'){
						
			 			$boardingSettings = $this->getBoardingSettings();
			 			
						if(strtolower($petDetails['type']) == 'dog')
			 				$BoardingPrice = $boardingSettings['dogBoardingPricing'];
			 			else
			 				$BoardingPrice = $boardingSettings['catBoardingPricing'];



						$todaydate = date('Y-m-d');
						
			 			if($todaydate >= $activeappointmentDetails[$i]['end_date']){
			 				// echo "fvdf";
			 				$datediff = strtotime($todaydate) - strtotime($activeappointmentDetails[$i]['end_date']);
							$datediff = round($datediff / (60 * 60 * 24));
							$activeappointmentDetails[$i]['overstayAmt'] = $BoardingPrice * $datediff;
							$activeappointmentDetails[$i]['overstayAmt'] = number_format((float)$activeappointmentDetails[$i]['overstayAmt'], 2, '.', '');
							$activeappointmentDetails[$i]['overstayLabel'] = 'Overstay';
							$activeappointmentDetails[$i]['end_date'] = $todaydate;
						}
			 			else{
			 				// echo "dsv";
			 				$datediff = strtotime($activeappointmentDetails[$i]['end_date']) - strtotime($todaydate);
							$datediff = round($datediff / (60 * 60 * 24));
							if($todaydate == $activeappointmentDetails[$i]['start_date']){
								if($datediff == 1){
									$datediff = 0;
								}else{
									$datediff = $datediff - 1;
								}
							}
							  $activeappointmentDetails[$i]['overstayAmt'] = $BoardingPrice * $datediff;
							  $activeappointmentDetails[$i]['overstayAmt'] = 0 - $activeappointmentDetails[$i]['overstayAmt'];
							  $activeappointmentDetails[$i]['overstayAmt'] = number_format((float)$activeappointmentDetails[$i]['overstayAmt'], 2, '.', '');

							  $activeappointmentDetails[$i]['overstayLabel'] = 'Understay';
							  $activeappointmentDetails[$i]['end_date'] = $todaydate;
			 				
			 			}

			 			// pr($activeappointmentDetails[$i]['overstayAmt']);
			 			
			 			$overstayAmt += $activeappointmentDetails[$i]['overstayAmt'];
			 			// pr($overstayAmt);exit;
			 			/* Overstay for boarding Logic End */
			 		}
			 		$activeappointmentDetails[$i]['petname'] = ucfirst($petDetails['name']);
			 		$appointmentamount = $activeappointmentDetails[$i]['amount'];
		   		}
		   		
	   				/* Appointment Discount Logic Start */
	   				$arrDiscount = $this->getUserDiscountArr($postarray['app_user_id'],date('Y-m-d'),$activeappointmentDetails[$i]['category']);
					$arrDiscountActive = $this->getActiveDiscount($postarray['app_user_id']);
			        foreach ($arrDiscount as $row) {
			        	if($arrDiscountActive[0]['discount_id'] == $row['discount_id']){
							$discount_percentage = $row['discount_percentage'];
					       	$discount_id = $row['discount_id'];
							break;
						}
			            if($row['discount_percentage'] >= $discount_percentage){
			                $discount_percentage = $row['discount_percentage'];
					       	$discount_id = $row['discount_id'];
			            }
			        }
			        // pr($postarray);exit;
			        if($discount_percentage > 0){

			        	
			        	$singlediscount_amount = floatval((($appointmentamount / 100) * $discount_percentage));
			            // $discount_amount += $singlediscount_amount;
					    $activeappointmentDetails[$i]['singlediscount_amount'] = $singlediscount_amount;
			        }

			        /* Appointment Discount Logic End */

		   		$i++;
		   		
			}
			// pr($appointmentamount1);
			// pr($appointmentamount);exit;
			// pr($discount_amount);exit;
	      	$activeappointmentDetails[0]['additional_cost1'] = $postarray['additional_cost1'];
	      	$activeappointmentDetails[0]['additional_note1'] = $postarray['additional_note1'];
			$result['allAppointments'] = $activeappointmentDetails;
			$result['overstayAmt'] = $overstayAmt;
			// $result['additional_cost1'] = $_POST['additional_cost1'];
			// $result['additional_note1'] = $_POST['additional_note1'];
			$result['additional_cost'] = $additional_cost;
			$result['cost'] = floatval($allappointmentcost + $overstayAmt + $postarray['additional_cost1']);
			$result['cost'] = number_format((float)$result['cost'], 2, '.', '');
			$result['deposit'] = number_format((float)$allappointmentdeposit, 2, '.', '');

			
			$discount_amount = floatval((($result['cost'] / 100) * $discount_percentage));
			$result['discount_amount'] = $discount_amount;
			$total_cost = floatval($result['cost'] - $result['discount_amount']);

	    	/* MAnnual Discount Logic start */
	    	// pr($postarray['manual_discountPer']);exit;
	    	// pr($total_cost);
	    	$manual_discount = 0;
			if($postarray['manual_discountPer'] > 0){
	 			$manual_discount = (($total_cost / 100) * $postarray['manual_discountPer']);
	 			
	 		}else{
	 			$manual_discount = $postarray['manual_discount'];	
	 		}
	 		$result['manual_discount'] = number_format((float)$manual_discount, 2, '.', '');
	 		// pr($result['manual_discount']);exit;
	 		$total_cost = floatval($total_cost - $result['manual_discount']);
	 		// pr($total_cost);exit;
	 		/* MAnnual Discount Logic End */

	 		$total_cost = floatval($total_cost - $result['deposit']);//all deposite subtract
			 /* credited Logic start */  
		    if($total_cost > 0){
		    	
	 			if($AppUserDetails[0]['credit_balance'] > $total_cost){
	 				// echo "d";
	 				// pr($result['credited_amount']);exit;
	 				$result['credited_amount'] = $total_cost;
	 				$result['remaining_cost'] = 0.00;
	 				$result['credit_balance'] = $AppUserDetails[0]['credit_balance'];
	 			}else{
	 				// echo "D";
	 				$result['remaining_cost'] = $total_cost - $AppUserDetails[0]['credit_balance'];
	 				// pr($AppUserDetails[0]['credit_balance']);
	 				// pr($result['remaining_cost']);
	 				$result['credited_amount'] = $AppUserDetails[0]['credit_balance'];
	 				$result['credit_balance'] = $AppUserDetails[0]['credit_balance'];
	 			}	
	 		}else{
	 			$result['remaining_cost'] = $total_cost;
				$result['credited_amount'] = 0.00;
				$result['credit_balance'] = $AppUserDetails[0]['credit_balance'];
	 		}
 	 // pr($result['remaining_cost']);exit;
 		/* credited Logic End */  
 		// if($result['remaining_cost'] < 0 && $result['discount_amount'] > 0 ){
 		// 	$result['remaining_cost'] = $result['remaining_cost'] - $result['discount_amount'];

 		// 	$result['remaining_cost'] = number_format((float)$result['remaining_cost'], 2, '.', '');
 		// 	   // pr($result['remaining_cost']);exit;
 		// }else{
 			$result['remaining_cost'] = number_format((float)$result['remaining_cost'], 2, '.', '');
 		// }
	    
	    }
    	return $result;

	}

	function sendcheckoutemail($result){
		$postarray = $result['allAppointments'];
		// pr($result);
		 // pr($postarray);exit;
		$AppUserDetails = $this->getSingleUserData($postarray[0]['app_user_id']);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shopno'));
		
		$currentDateTime0 = date('Y-m-d H:i:s');
		$currentDateTime = server_date($currentDateTime0,'Y-m-d H:i:s');

		$emailMsg = '';
		 	$finalservicearray = array();
		 	$petName=array();
			
			 	foreach ($postarray as $key => $value) {
					
					$keyarray = array();
					
					if(empty($value['cost'])){
						
						if($value['category'] == 'boarding'){
							$boardingcost += $value['inserted_cost'] ;
						}else{
							$daycarecost += $value['inserted_cost'] ;
						}
					}
					$allappointmentcost += $value['inserted_cost'];

					$petName[$key] = ucfirst($value['petName']);
				
					$servicearray = array_merge($value['appointmentServiceInfo'],$value['addonappointmentServiceInfo']);
					foreach ($servicearray as $key1 => $value1) {

						array_push($keyarray ,$value1['service_name']);
					
					}
				
					$servicekey = '';
					$servicekey = implode(', ',$keyarray);

					if($value['category'] == 'daycare' || $value['category'] == 'boarding'){
						$emailMsg .= ucfirst($value['category']).' for '.$value['petName'].'<br>
						Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($value['check_in_time'])).'<br>
						Check Out: '.date(get_option('date_format').', '.'h:i a', strtotime($currentDateTime0)).'<br><br>';
					}
					else{	

						$emailMsg .= 'Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($value['check_in_time'])).'<br>
						Appointment services for '.$value['petName'].':<br>'.trim($servicekey).'<br><br>';	
					}
					foreach ($servicearray as $key1 => $value1) {
						if(array_key_exists($value1['service_name'],$finalservicearray)){

							$finalservicearray[$value1['service_name']] += $value1['service_cost'];
						}else{
							
							$finalservicearray[$value1['service_name']] = $value1['service_cost'];
						}

					}

				}

				$petName = array_unique($petName);
				$str = implode(', ',$petName);
				$portion = strrchr($str, ', ');

				$str = str_replace($portion, (" and" . substr($portion, 1, -1)), $str);

				$chargesDetails = '';
			 	$chargesDetails .= 'Charges Details: <br>';
       			
       			
				$boardingcost = $boardingcost;
				if($boardingcost > 0){
					$chargesDetails .= 'Boarding: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($boardingcost).'<br>';
				} 
				if($daycarecost > 0){
					$chargesDetails .= 'Daycare: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($daycarecost).'<br>';
				}
				if(!empty($finalservicearray)){
					foreach ($finalservicearray as $key2 => $value2) {
						$chargesDetails .= $key2.': '.get_option('currency_symbol').' '.$this->localization->currencyFormat($value2).'<br>';
					}
				}

				$total_service_cost = $result['cost'] - $result['additional_cost'];
				
       			$chargesDetails .= 'Fees: '.get_option('currency_symbol').' '.$this->localization->currencyFormat(0).'<br>';
       			$chargesDetails .= 'Total Service Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($total_service_cost).'<br>';
       			$chargesDetails .= 'Additional Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($result['additional_cost']).'<br>';

       			
       			$emailMsg1 = '';
       		
       			
       				if($result['credited_amount'] >0 ){
				   		
							$emailMsg1 .= 'Credits Redeemed: $ '.$result['credited_amount'].'<br>';
							

								$emailMsg1 .='The remaining credit balance on your profile is $'.number_format($AppUserDetails[0]['credit_balance'],2).'<br>'; 

				   	}else if($result['awarded_source'] == 'refunded'){
				   		$emailMsg1 .= '
				   		Amount Refunded: '.get_option('currency_symbol').' '.$this->localization->currencyFormat(abs($result['remaining_cost'])).'<br>'; 
				   	}else if($result['awarded_source'] == 'Credit Awarded'){
			   			$emailMsg1 .= '
			   			Amount Credited: $ '.abs(number_format($result['remaining_cost'],2)).'<br>';
						$emailMsg1 .='The total credit balance on your profile is $'.$AppUserDetails[0]['credit_balance'].'<br>'; 
				   	}
       			

       			$chargesDetails .= 'Total Cost: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($result['remaining_cost']).'<br>';
       		
				 if(str_word_count($str) > 1){
				 	$subject = 'Your Appointments has been checked out successfully - '.PROJECTNAME.' ';
				 	$title = 'Your Appointments for pets '.$str.' have been checked out successfully.';
				 }else{
				 	$subject = 'Your Appointment has been checked out successfully - '.PROJECTNAME.' ';
				 	$title = 'Your Appointment for pet '.$petName[0].' has been checked out successfully.';
				 }

				 if(count($postarray) > 1){

						$emailContent = array('subject' => $subject.PROJECTNAME.' ','message' =>'
							Hello '.ucfirst($AppUserDetails[0]['username']).',<br><br>

							
							'.$title.'
							<br><br>
							Date : '.date("M d Y", strtotime($currentDateTime0)).'<br><br>

							'.$emailMsg.'<br>

							'.$chargesDetails.'
							'.$emailMsg1.'

							<br>
							If you are having any issues, please contact:     

							<br><br>
							'.$shop_Data['shopname'].'<br>
							'.$shop_Data['phone_number'].'<br>
							'.$shop_Data['address'].'<br>
							'.get_option('shop_email').'<br>

							Hope to see you again soon.

							<br><br>
							Thanks,<br><br>
							Team @'.$shop_Data['shopname'].'<br><br>
			           		'
		                );
		            }else{
		            	$emailMsg = '';
		            	$singleemailMsg = '';
		            	if($postarray[0]['category'] == 'appointment'){

							$singleemailMsg .= 'Date : '.date("M d Y", strtotime($currentDateTime0)).'<br>
							Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($postarray[0]['check_in_time'])).'<br>
							Appointment services:'.trim($servicekey).'<br>';
							;	
		            	}else{
		            		$singleemailMsg .= 'Check In: '.date(get_option('date_format').' '.'h:i a', strtotime($postarray[0]['check_in_time'])).'<br>
							Check Out: '.date(get_option('date_format').', '.'h:i a', strtotime($currentDateTime0)).'<br>';
		            	}
		            	$emailContent = array('subject' => $subject.PROJECTNAME.' ','message' =>'
							Hello '.ucfirst($AppUserDetails[0]['username']).',<br><br>

							'.$title.'
							<br><br>
							'.$singleemailMsg.'<br>
							'.$chargesDetails.'
							'.$emailMsg1.'<br>
							
							If you are having any issues, please contact:     

							<br><br>
							'.$shop_Data['shopname'].'<br>
							'.$shop_Data['phone_number'].'<br>
							'.$shop_Data['address'].'<br>
							'.get_option('shop_email').'<br>

							Hope to see you again soon.

							<br><br>
							Thanks,<br><br>
							Team @'.$shop_Data['shopname'].'<br><br>
			           		'
		                );
		            }

           		// pr($emailContent);exit;
				email($AppUserDetails[0]['email'], $emailContent,$shop_Data['id']);	
				
			 	

	}

	function checkoutallappointments(){
        // check out all combined appointments
    	
        if($this->input->post())
		{	
			
			$_POST['all_appointment_array'] = json_decode($_POST['all_appointment_array'][0]);
    		
			$data = $this->input->post();

			if($data['appointment_category'] == 'appointment'){

				$currentappointmentDetails = $this->Entry_model->getSingleAppointmentId($data['appointment_id']);
				$category = 'Grooming';
			}else{
				$currentappointmentDetails = $this->getBoardingDayCareData($data['appointment_id']);
				$category = $currentappointmentDetails['category'];
			}


			if(isset($data['avatar_data']))
				unset($data['avatar_data']);


	 		$array = array(

	 			'appointment_id'=>$currentappointmentDetails['id'],
	 			'category'=> $category,
	 			'autodiscount_id'=>$data['currentdiscount_id'],
	 			
	 			'additional_cost1'=>$data['additional_cost1'],
	 			'additional_note1'=>$data['additional_note1'],
	 			'manual_discount'=>$data['manual_discount'],
	 			'manual_discountPer'=>$data['manual_discountPer'],
	 			'app_user_id'=>$currentappointmentDetails['app_user_id'],
	 			'allappointments' => $_POST['all_appointment_array'],
	 			
	 		);
	 		$result = $this->getFinalInvoice($array);
	 		if(empty($data['manual_discountPer'])){
	 			$data['manual_discountPer'] = 0;
	 		}
	 		if(empty($result['autodiscount_id'])){
	 			$result['autodiscount_id'] = 0;
	 		}
	 		if(empty($result['credited_amount'])){
	 			$result['credited_amount'] = 0;
	 		}
	 		if(empty($result['overstay'])){
	 			$result['overstay'] = 0;
	 		}
	 		$result['allAppointments'][0]['credited_amount'] = $result['credited_amount'];
	 		$data['check_out_time'] = date('Y-m-d H:i:s');
	 		$data['check_out_time'] = server_date($data['check_out_time'],'Y-m-d H:i:s');
	 		$result['additional_cost'] += $data['additional_cost1'];
	 		if($data['appointment_category'] == 'appointment'){
		 		$appointmentUpdateArr = array(
		 			
		 			'manual_discount'=> $result['manual_discount'],
		 			'manual_discountPer'=>$data['manual_discountPer'],
		 			'discount_id'=>$result['autodiscount_id'],
		 			'discount_amount'=>$result['discount_amount'],
		 			'additional_cost1'=>$data['additional_cost1'],
		 			'additional_note1'=>$data['additional_note1'],
		 			'check_out_time'=>$data['check_out_time'],
		 			'status'=>'Checkout',
		 			'payment_status'=>'success',
		 			'credited_amount'=>$result['credited_amount']
		 		);
		 		$this->db->update('appointments',$appointmentUpdateArr,array('id'=>$currentappointmentDetails['id']));
		 	}else{
		 		$appointmentUpdateArr = array(
		 			
		 			'manual_discount'=>$result['manual_discount'],
		 			'overstay'=>$result['overstay'],
		 			'manual_discountPer'=>$data['manual_discountPer'],
		 			'discount_id'=>$result['autodiscount_id'],
		 			'discount_amount'=>$result['discount_amount'],
		 			'additional_cost'=>$data['additional_cost1'],
		 			'note'=>$data['additional_note1'],
		 			'check_out_time'=>$data['check_out_time'],
		 			'status'=>'Checkout',
		 			'payment_status'=>'success',
		 			'credited_amount'=>$result['credited_amount']
		 		);
		 		$this->db->update('boardings',$appointmentUpdateArr,array('id'=>$currentappointmentDetails['id']));
		 	}

		 	
		 	$appointmentsUpdate = array(
	 			
	 			'check_out_time'=>$data['check_out_time'],
	 			'status'=>'Checkout',

	 			'invoice_no'=> $currentappointmentDetails['invoice_no']
	 		);
	 		
			foreach ($result['allAppointments'] as $key => $value) {
				if($value['category'] == 'appointment'){
					if(isset($appointmentsUpdate['overstay'])){
						unset($appointmentsUpdate['overstay']);
					}
					$this->db->update('appointments',$appointmentsUpdate,array('id'=>$value['id']));
					$this->db->delete('appointments', array('isDuplicate'=>'1','pet_id'=>$value['pet_id'],'appointment_date<='=>$value['appointment_date']));
				}else{
					
					$appointmentsUpdate['overstay'] = $value['overstayAmt'];
					$this->db->update('boardings',$appointmentsUpdate,array('id'=>$value['id']));
				}
			}
		 	 
		 	$payment = $this->db2->get_where('transactions',array('id'=>$data['trans_id']))->row_array();

	 		$payment['amount'] = $result['remaining_cost'];
	 		$payment['status'] = 'success';
	 		if($payment['amount'] < 0){
	 			if($data['payment_source'] == 'Refund'){
	 				$data['payment_source']= 'Other';
	 				$payment['transcation_type']= 'payout';
	 				$payment['status'] = 'refunded';

	 				$result['awarded_source'] = 'refunded';
	 			}else{
	 				$data['payment_source']= 'Credit Awarded';
	 				$result['awarded_source'] = 'Credit Awarded';
	 			}	
	 		}

	 		if($payment['amount'] == 0){
	 			$data['payment_source']= 'Other';
	 		}
	 		
		 	if($this->db2->update('transactions',array('amount'=>$payment['amount'],'payment_source'=>$data['payment_source'],'other_source'=>$data['other_source'],'transcation_type'=>$payment['transcation_type'],'status'=>$payment['status'],'added_on'=>$data['check_out_time']),array('id'=>$data['trans_id'],'type!='=>'deposit')))
			 {
				
				if($result['credited_amount'] > 0){
					$result['credit_balance'] = floatval($result['credit_balance'] - $result['credited_amount']);
					
					$this->db2->update('app_users',array('credit_balance'=>$result['credit_balance']),array('id'=>$result['allAppointments'][0]['app_user_id']));
					 
					$newPayment = $payment;

					unset($newPayment['id']);

					$newPayment['payment_source'] = 'Credit Redeem';
					$newPayment['amount'] = $result['credited_amount'];
					$newPayment['type'] = 'balance';
					$newPayment['status'] = 'success';
					$newPayment['added_on'] = $data['check_out_time'];
					$this->db2->insert('transactions',$newPayment);
				}

				if($payment['amount'] < 0 && $data['payment_source'] != 'Refund'){
					$payment['amount'] = $result['credit_balance'] + trim($payment['amount'], "-");
                    $this->db2->update('app_users',array('credit_balance'=>$payment['amount']),array('id'=>$result['allAppointments'][0]['app_user_id']));
				}

					$this->sendcheckoutemail($result);


					$newarray = array();
					$newarray['invoiceid'] = $currentappointmentDetails['invoice_no'].'_'.$this->session->userdata('shop_id');
					$newarray['current_appointmentment_id'] = get_encode($currentappointmentDetails['invoice_no']);
					$newarray['appointment_category'] = $data['appointment_category'];
					$newarray['appointment_id'] =  $data['appointment_id'];
                    // $newarray['appointmentArray'] = $this->invoice(get_encode($currentappointmentDetails['invoice_no']));

                    return($newarray);
     //                invoiceData($invoiceid,'insert',$appointmentArray);

     //                if($data['appointment_category'] == 'appointment'){

					// 	$this->message->update_success('Admin/viewAppointment/'.get_encode($data['appointment_id']));
					// }else{
					// 		if($data['appointment_category'] == 'boarding'){
				 //                $viewpage = 'viewBoarding';
				                
				 //            }else{
				 //               $viewpage = 'viewDaycare';
				 //           	}
				       
					// 	$this->message->update_success('Admin/'.$viewpage.'/'.get_encode($data['appointment_id']));
					// }

			}
			// else
			// {
			// 	$this->message->custom_error_msg(referrer(),'Cannot update Appointment status.Please try again'); 
			// }
	    }
	    // else
	    // {
	    // 	$this->message->custom_error_msg(referrer(), 'Cannot access function directly');
	    // }    
        
    }

  
    function getAllWoodlesDbModules()
	{
		//get all admin modules
		$query = $this->db2->get_where('modules');
		return $query->result();
	}

	
    function getappointmentsdetailsforpayment($data){
	    if(!empty($data)){
	    	// pr($data);exit;
	    	$value = $data;
	    	$temp = array();
	    	if($value['invoice_id'] > 0){
 	    	 // pr($data);exit;
					$appointments = $this->db->get_where('appointments',array('invoice_no' => $value['invoice_id']))->result_array();
        
			        $boardings = $this->db->get_where('boardings',array('invoice_no' => $value['invoice_id']))->result_array();
			       
			        $allappointments = array_merge($appointments,$boardings);

			        $temp['services'] = '';
			        // pr(count($allappointments));exit;
			        $i = 0;$j= 0;$k =0;
			     if(count($allappointments) > 1){

			      foreach ($allappointments as $key1 => $value1) {
			      	// echo "Vdfv";exit;
			      	if($value1['category'] == 'boarding'){
			      		 // $i++;
			      		$boarding[$i] = $value1;
			      		$i++;
			      		
	                	
	                }
	                else if($value1['category'] == 'daycare')
	                {
	                	
	                	$daycare[$j] = $value1;
			      		$j++;
	                	
	                }else {
	                	
	                	$grooming[$k] = $value1;
			      		$k++;
			      		
	                }
	               
			      }
			  
				      if((!empty($grooming)) && ($k > 0)){
				      	$temp['services'] .= 'Grooming x '.$k.', ';
				      }
				       if((!empty($daycare)) && ($j > 0)){
				       	$temp['services'] .= 'Daycare x '.$j.', ';
				      } if((!empty($boarding)) && ($i > 0)){
				      	$temp['services'] .= 'Boarding x '.$i.', ';
				      }
				      $temp['services'] = trim($temp['services'],', ');
			     	 // pr($temp['services']);exit;
			     }else{
	                if($value['category'] == 'appointment'){
	                	if($value['is_add_on_payment'] == 0){
	                		$temp['services'] = $this->getServiceForPayment($value['reference_id'],$value['category']);
	                	}

	                	else{
	                		$temp['services'] = $this->getAddOnServices($value['reference_id'],$value['category']);
	                	}

	                	if(empty($temp['services']))
	                		$temp['services'] = 'Grooming Additional Payment';
	                }
	                else
	                {
	                	if($value['is_add_on_payment'] == 0)
	                		$temp['services'] = ucfirst($value['category']);
	                	else
	                		$temp['services'] = ucfirst($value['category']).' Additional Payment';
	                }
	            }
	            // pr($temp['services']);exit;
	         }
	            // pr($temp['services']);exit;
	            return $temp['services'];


	   
	}
	}
	function cloud_image_delete($object){
		$cloud = new ServiceBuilder([
            'keyFilePath' => FCPATH.ClientJsonFileName
       	]);
       	$storage = $cloud->storage();
      
        try {
            $bucket = $storage->bucket('pet-commander-beta-app');
            $object = $bucket->object($object);
            $object->delete();
            
        }
        //catch exception
        catch(Exception $e) {
            
        }
	}
	//global
	function getGroomerName($id)
    {

        $t = $this->db->get_where('admin_users',array('id'=>$id))->row_array();
        if(!empty($t))
            return $t['first_name'].' '.$t['last_name'];
        else
            return 'N/A';
    } 

    function getUsername($id)  
    {
        //load Client name from super admin database
        //$this->db2 = $this->load->database('main', TRUE);
        $result = $this->db2->get_where('app_users',array('id'=>$id))->row_array();
        return $result['username'];
    }
     function getPetName($id)
    {

        //load pet name from super admin database
        //$this->db2 = $this->load->database('main', TRUE);

        $result = $this->db2->get_where('pets',array('id'=>$id))->row_array();
        

        return $result['name'];


    }
     function getBoardingInevntoryName($id,$Inventorytype)
    {
        //Get Boarding Inventory Name By id
        if($id > 0)
        {
            if($Inventorytype == 'playarea')
            {
                $result = $this->db->get_where('playareas',array('id'=>$id))->row_array();
                
            }

            if($Inventorytype == 'daycare')
            {
                $result = $this->db->get_where('daycare_locations',array('id'=>$id))->row_array();
                
            }

            if($Inventorytype == 'boarding')
            {
                $result = $this->db->get_where('boarding_inventory',array('id'=>$id))->row_array();
                
            }
            

            return $result['name'];


        }
        else
        {
            return 'N/A';
        }
	}
	
//invoice function along with merge logic
function invoice($invoice_no){
	$data = array();
	$appointments = $this->db->get_where('appointments',array('invoice_no' => $invoice_no,'isDuplicate' =>'0'))->result_array();
	$boardings = $this->db->get_where('boardings',array('invoice_no' => $invoice_no))->result_array();
	$allappointments = array_merge($appointments,$boardings);
	$data['services'] = $this->Entry_model->getAllServices();
	$AppUserDetails = $this->getSingleUserData($allappointments[0]['app_user_id']);
	$data['manual_discount'] = $allappointments[0]['manual_discount'];
	$data['credited_amount'] = 0.00;
	$data['payable_amount'] = 0.00;
	$data['total_deposit'] = 0.00;
	$data['total_payable'] = 0.00;
	$data['total_paid'] = 0.00;
	$data['user_details'] = $this->Management_model->getSingleUserData($allappointments[0]['app_user_id']);

	$arrDiscount = $this->Management_model->getAvailableDiscounts();
	$data['arrDiscount'] = $arrDiscount;
	$arrDiscountActive = $this->Management_model->getActiveDiscount($allappointments[0]['app_user_id']);
	if(count($arrDiscountActive)){
		$app_users_active_discount_id =  $arrDiscountActive[0]['discount_id'];
	}else{
		$percentage_temp = 0;
		$percentage = 0;
		$active_discount_id = 0;
		$app_users_active_discount_id = 0;
		foreach($arrDiscount as $key => $value) {

			$percentage = $value['discount_percentage'];    

			if($percentage > $percentage_temp) {
			   $percentage_temp = $percentage ; 
			   $active_discount_id = $value['id'] ; 
			 
			}
		}

		if($active_discount_id > 0){
			$availablForUser = $this->db->get_where('discount_users',array('app_user_id'=>$allappointments[0]['app_user_id'],'discount_id' => $active_discount_id))->row_array();
			if(count($availablForUser)){
				$app_users_active_discount_id = $active_discount_id;
			}
		}
	}
	/*$string1 = '<select class="form-control" id="auto_discount" >';
	if(!empty($arrDiscount))
	{
		$string1 .= '<option value="0" data-category="'.$allappointments[$key1]['category'].'" data-appointment_id="'.$value1['id'].'">No Discount</option>';
		   foreach ($arrDiscount as $dis_key => $row) {
			   if (strpos($row['allow_type'], $allappointments[$key1]['category']) !== false) {
				   $string1 .= '<option value="'.$row['id'].'" data-percentage ="'.$row['discount_percentage'].'"  data-category="'.$allappointments[$key1]['category'].'" data-appointment_id="'.$value1['id'].'">'.$row['discount_title'].' ( '.$row['discount_percentage'].'% ) '.'</option>';
			   }
		   }
	}else{
		$string1 .= '<option value="">No Active Discount</option>';
	}
	$string1 .= '</select>';
	$allappointments[$key1]['string1'] = $string1;*/
// pr($allappointments);exit;
	if(!empty($invoice_no)){	
		foreach ($allappointments as $key1 => $value1) {
			$data['total_deposit'] = $data['total_deposit'] + $value1['deposit'];
			$allappointments[$key1]['appointment_cost'] = 0;
			$string1 = '';
			$allappointments[$key1]['invoice_items'] = unserialize($value1['invoice_items']);
			$pet_size = $this->db2->select('*')->get_where('pets',array('id' => $value1['pet_id']))->row_array();
			
			$allappointments[$key1]['category'] = empty($value1['category']) ? 'appointment' : $allappointments[$key1]['category'];

			//dicount dropdown logic start
			$current_discountData = array();
			if($app_users_active_discount_id > 0){
				$current_discountData = $this->db->get_where('discounts',array('id' => $app_users_active_discount_id))->row_array();
			}
			//dicount dropdown logic end

			if(empty($value1['category'])){
				$value1 = groomingTimeConverter($value1);
				$size_cost = $pet_size['size'].'_cost';
				$k=0;
				$serviceArray = "[";
				foreach($allappointments[$key1]['invoice_items']['services'] as $s_key =>$row){
					$serviceArray .= $row['id'].",";
					
					if($row['is_Add_On'] == 0){
						
						if(!empty($current_discountData) && $current_discountData['core_service']==1 && !isset($row['discount'])){
							$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2);
						}
					}else{
						if(!empty($current_discountData) && $current_discountData['add_on_service']==1 && !isset($row['discount'])){
							$allappointments[$key1]['invoice_items']['services'][$k]['discount'] = number_format(($row['cost'] / 100) * $current_discountData['discount_percentage'],2); 
						}   
					}
					$allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'] = $row['cost']-$allappointments[$key1]['invoice_items']['services'][$k]['discount'];
					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['services'][$k]['payable_cost'];
					$k++;
				}
				
				$serviceArray = trim($serviceArray,',')."]";
				$allappointments[$key1]['serviceArray'] = $serviceArray;
			}else{
				$value1 = daycareBoardingTimeConverter($value1);
				if(!isset($allappointments[$key1]['invoice_items']['appointment']['discount'])){
					$allappointments[$key1]['invoice_items']['appointment']['discount'] = number_format(($allappointments[$key1]['invoice_items']['appointment']['amount'] / 100) * $current_discountData['discount_percentage'],2);
				}

				
				$allappointments[$key1]['invoice_items']['appointment']['payable_cost'] = $allappointments[$key1]['invoice_items']['appointment']['amount']-$allappointments[$key1]['invoice_items']['appointment']['discount'];
				
				$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['payable_cost'];

				if((abs($allappointments[$key1]['invoice_items']['appointment']['overstay']) > 0)){
					$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['appointment']['overstay'];
				}	
			}
			
			//additional cost logic
			$i=0;
			foreach($allappointments[$key1]['invoice_items']['additional_cost'] as  $row){
				$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]; 
				if($row[3]){
					$allappointments[$key1]['invoice_items']['additional_cost'][$i][3] = $row[3]; 
					$allappointments[$key1]['invoice_items']['additional_cost'][$i][4] = $row[1]-$row[3]; 
				}  
				
				$allappointments[$key1]['appointment_cost'] += $allappointments[$key1]['invoice_items']['additional_cost'][$i][4];
				   
				$i++;
			}
			//pr($allappointments[$key1]['invoice_items']['additional_cost']);exit;
			//additional cost logic end
			
			$allappointments[$key1]['appointment_cost'] = $allappointments[$key1]['appointment_cost']- $allappointments[$key1]['deposit'];
			$data['payable_amount'] += $allappointments[$key1]['appointment_cost'];
			$allappointments[$key1]['petname'] = ucfirst($pet_size['name']);
			$allappointments[$key1]['pet_type'] = ucfirst($pet_size['type']);
			$allappointments[$key1]['check_in_time'] = $value1['check_in_time'];
			$allappointments[$key1]['check_out_time'] = $value1['check_out_time'];      
		  }
		  $data['total_payable'] = $data['payable_amount'];
		  $data['total_payable'] = $data['total_payable'] + $data['total_deposit'];
		$data['allappointments'] = $allappointments;
		$data['before_manual_discount'] = 0.00;
		if($allappointments[0]['is_paid'] == 0){
			if($allappointments[0]['manual_discountPer'] > 0){
				$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
			}

			if($data['manual_discount'] == 0){
				$data['before_manual_discount'] = $data['total_payable'];
			}
		}else{
			if($allappointments[0]['manual_discountPer'] > 0){
				$data['manual_discount'] = (($data['total_payable'] / 100) * $allappointments[0]['manual_discountPer']);	
			}else{
				$data['manual_discount'] = $allappointments[0]['manual_discount'];
			}
		}
		$data['payable_amount'] = $data['payable_amount'] - ($data['manual_discount']);
		$data['total_paid'] = $data['total_payable'] - ($data['manual_discount']);
		

		//First pay logic
		$data['first_pay'] = 0;
		$first_payappointment = $this->db2->select('count(id) as count')->get_where('transactions',array('invoice_id' => $invoice_no,'transcation_type' =>'payment','type!=' =>'deposit','shop_id'=>$this->session->userdata('shop_id')))->row_array();
		if($first_payappointment['count'] > 0){
			$data['first_pay'] = 1;
		}

		//All transactions without deposit
		$this->db2->where_not_in('type',array('deposit','from_transaction'));
		$data['all_transactions'] = $this->db2->select('*')->get_where('transactions',array('invoice_id' => $invoice_no,'shop_id'=>$this->session->userdata('shop_id')))->result_array();
		$data['transaction_amount'] = 0.00;
		foreach ($data['all_transactions'] as $transaction_index => $transaction) {
			$data['transaction_amount'] = $data['transaction_amount'] + ($transaction['amount']);
			$payment_source = '';
			$payment_source = $transaction['payment_source'];

			if($transaction['payment_source']=='Cash'){
				$payment_source = $transaction['payment_source'] .' Payment';
			}

			if($transaction['payment_source']=='Credit/Debit'){
				$payment_source = $transaction['payment_source'] .' Card Payment';
			}
			if($transaction['transcation_type']=='payout' && $transaction['payment_source']=='Other'){
				$payment_source = 'Refunded';
			}
			if($transaction['payment_source']=='Credit Redeem'){
				$payment_source = 'Credit Redeemed';
			}
			if($transaction['transcation_type']=='payment' && $transaction['payment_source']=='Other'){
				$payment_source = $transaction['payment_source'] .' Payment';
				if(!empty($transaction['other_source'])){
					$payment_source .= ' - '.$transaction['other_source'];
				}
			}
			

			$data['all_transactions'][$transaction_index]['payment_name'] = $payment_source .' ('.date(get_option('date_format'), strtotime($transaction['added_on'])).')';
		}
		$data['payable_amount'] = $data['payable_amount'] - $data['transaction_amount'];
		$data['payable_amount'] = number_format($data['payable_amount'], 2);
		/*if($allappointments[0]['is_paid'] == 0){
			
			if($data['payable_amount'] > 0 && $AppUserDetails[0]['credit_balance'] > 0){
				if($AppUserDetails[0]['credit_balance'] > $data['payable_amount']){
					 $data['credited_amount'] = $data['payable_amount'];
					 $data['payable_amount'] = 0;
				 }else{
					 $data['payable_amount'] = $data['payable_amount'] - $AppUserDetails[0]['credit_balance'];
					 $data['credited_amount'] = $AppUserDetails[0]['credit_balance'];
				 }	
			 }
		}*/

		
		//show merge btn logic
		$data['activeappointments'] = 0;
		//active appointment
		$this->db->where_in('status',array('Confirm','Inprocess','Complete','Checkout'));
		$activeappointment = $this->db->select('count(id) as count')->get_where('appointments',array('invoice_no!=' => $invoice_no,'isDuplicate' =>'0','is_paid'=>0,'app_user_id' =>$allappointments[0]['app_user_id']))->result_array();
		
		//active boarding
		$this->db->where_in('status',array('Confirm','Inprocess','Checkout'));
		$activeboarding = $this->db->select('count(id) as count')->get_where('boardings',array('invoice_no!=' => $invoice_no,'app_user_id' =>$allappointments[0]['app_user_id'],'is_paid'=>0))->result_array();
		
		$count = $activeappointment[0]['count'] + $activeboarding[0]['count'];
		
		if($count > 0){
			$data['activeappointments'] = 1;
		}
		$data['app_user_name'] = $AppUserDetails[0]['username'];
		$data['app_user_phone'] = $AppUserDetails[0]['phone'];
		$data['credit_balance'] = $AppUserDetails[0]['credit_balance'];
	}
	// pr($data);exit;
	return $data;
}

function getPetSize()
{
	$result = $this->db2->query('SHOW COLUMNS FROM pets WHERE field="size"')->row_array();
	$enum = substr($result['Type'], 6, -2);
	$values = explode("','", $enum);
	
	return $values;
}



}