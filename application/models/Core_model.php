<?php

Class Core_Model extends CI_Model{
	
	// (KC) Get user data by user id 
    function getUserData($id = 0){
        return $this->db->get_where('users', array('id' => $id))->row_array();
    }

}
?>