<?php defined('BASEPATH') OR exit('No direct script access allowed');

//require (APPPATH.'/libraries/REST_Controller.php');

class Appuser_model extends CI_Model {
	protected $table_name;

	//Used For Api 
	function __construct()
	{
		parent::__construct();
		//$this->db2 = $this->load->database('main', TRUE);
		$this->table_name = 'app_users';

	}


	function login($emailORphone,$password,$shop_no,$link)
	{
		
		//app user login
		if(!empty($emailORphone)){
			// $where = "phone='$emailORphone' OR email='$emailORphone' AND 'link'='$link'";
			$where = "phone='$emailORphone' OR email='$emailORphone'";
			$result	= $this->db2->get_where($this->table_name,$where)->row_array();
		}

		if(!empty($result))
		{

			if(password_verify($password,$result['password']))
			{	

				//Check whether user belong to same shop
				return $result;

			}
			else
				return false;	
		}
		else
			return false;
	}


	function fblogin($fb_id,$shop_phone)
	{
		//$shop = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		
		if(!empty($fb_id))
			$result	= $this->db2->get_where($this->table_name,array('fb_id'=>$fb_id))->row_array();
		
		if(!empty($result))
		{
			return $result;
		}	
	}



	

	function getUserData($id)
	{
		return $this->db2->get_where($this->table_name,array('id'=>$id))->row_array();
	}



	function checkEmail($email)
	{
		//checking if email exist in the system
		$result = $this->db2->get_where($this->table_name,array('email'=>$email))->row_array();
	
		if(empty($result))
			return true;
		else
			return false;
		
	}


	function forgotPasswordEmailCheck($email)
	{
		$result = $this->db2->get_where($this->table_name,array('email'=>$email))->row_array();
		//pr($result);
		if(!empty($result))
			return $result;
		else
			return false;
	}

	function checkfbId($fb_id)
	{
		$result = $this->db2->get_where($this->table_name,array('fb_id'=>$fb_id))->row_array();
	
		if(empty($result))
			return true;
		else
			return false;
	}


	function logout($userId,$shop_no)
	{
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		
		return $this->db2->update('app_users_shops',array('currently_active' => 0),array('app_user_id'=>$userId,'shop_id'=>$shop_no));
	}


	function signup($postarray,$shop_no)
	{
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		$options = [
            'cost' => 5,
        ];

        //create new hash password
        $insert['username'] = $postarray['username'];
        $insert['email']= $postarray['email'];
		$insert['password'] =  password_hash($postarray['password'], PASSWORD_BCRYPT, $options);
		$insert['email_verification_token'] =  md5(time() . 'woodelestoken'); 
		$insert['access_token'] = generateRandomString();
		$insert['registered_shop'] = $shop_id;
		$this->db2->insert($this->table_name,$insert);
		if($insert['id'] = $this->db2->insert_id())
		{
			//$this->db->update('app_users_shops')
			$link = base_url().'Customer/emailVerify/'.$insert['email_verification_token'];
			
			$post = array();
			$post['shop_id'] = $shop_id;	
			$post['app_user_id'] = $insert['id'];
			$post['currently_active'] = 1;
			$this->db2->insert('app_users_shops',$post);

			$emailContent = array("subject" => "Verify Your Email Address - ".PROJECTNAME,"message" =>"
                    Hello ".$insert['username'].",<br><br>
                    
                    Welcome to ".PROJECTNAME."!  We appreciate you signing up for our service! 
                    <br>
					 
					 To continue using Pet Commander's mobile app we require you to confirm your email address.                  
                    <br>
                    
                    <a href=".$link.">click here</a><br><br>

                    If you are having any issues, please contact: 

                    <br><br>
               
                    ".$shop_Data['shopname']."<br>
                    ".$shop_Data['phone_number']."<br>
                    ".$shop_Data['address']."<br>
                    ".get_option('shop_email')."<br>
                    <br><br>
                    Thanks,
                    <br><br>
                    Team @".$shop_Data['shopname']."<br>
                    "
                    );
                  
                    //$emailContent = array('subject' => 'New Password Request','message' => $body);
            email($insert['email'],'support@griffinapps.com',$emailContent);

            return $insert; 

		}
		else
			return false;	

	}




	function fbsignup($postarray,$shop_no)
	{
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		$options = [
            'cost' => 5,
        ];

        //create new hash password
        $insert['username'] = $postarray['username'];
        $insert['email']= $postarray['email'];
        $insert['fb_id']=$postarray['fb_id'];
        //create new hash password  

        // comment 21-08-2018
		 $insert['email_verification_token'] =  md5(time() . 'woodelestoken'); 
		//$insert['email_verification_token'] = '';
		$insert['access_token'] = generateRandomString();
		$insert['registered_shop'] = $shop_id;
		$this->db2->insert($this->table_name,$insert);
		if($insert['id'] = $this->db2->insert_id())
		{

			$this->db2->update('app_users',array('email_verification_token'=>null),array('email_verification_token'=>$insert['email_verification_token']));
			//$this->db->update('app_users_shops')
			$link = base_url().'Customer/emailVerify/'.$insert['email_verification_token'];
			$post = array();
			$post['shop_id'] =  $shop_id;	
			$post['app_user_id'] = $insert['id'];
			$post['currently_active'] = 1;
			$this->db2->insert('app_users_shops',$post);

			$emailContent = array('subject' => 'Verify Your Email Address - '.PROJECTNAME.'','message' =>'
                    Hello '.$insert['username'].',<br><br>
                    
                    Welcome to '.PROJECTNAME.', We are very thankful for signing for our service
                    <br>
					 
					 For continue using our services, we require you to confirm your email address.                  
                    <br>
                    
                    <a href='.$link.'>click here</a><br><br>
                    
                    If you are having any issues, please contact: 	

                        
                    <br><br>
                    '.$shop_Data['shopname'].'<br>
                    '.$shop_Data['phone_number'].'<br>
                    '.$shop_Data['address'].'<br>
                    '.get_option('shop_email').'<br>
                    <br><br>

                    Thanks,<br><br>
                    Team @'.$shop_Data['shopname'].'<br><br>
					'
					);
                  
                   // comment 21-08-2018
            // email($insert['email'], $emailContent);

            return $insert; 

		}
		else
			return false;
	}


	// function forgotPassword($updateArray ,$app_users,$password,$shopno)
	// {

	// 	$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shopno);
	// 	// pr($shop_Data);exit;
	// 	//New random password has been generated and mailed to user
	// 	if($this->db2->update($this->table_name,$updateArray,array('id'=>$app_users['id'])))
	// 	{	
			
	// 		$emailContent = array('subject' => 'Your password has been reset ','message' =>'
	//                     Hello '.$app_users['username'].',<br><br>
	                    
	//                     We have received your forgot password request. We have reset your password.

	//                     <br><br>
	                    
	//                     You can now login using below credentials
	// 					<br><br>
	//                     Shop Phone Number : '.$shopno.'<br>
	//                     Email : '.$app_users['email'].' <br>
	//                     Password : '. $password.'<br><br>
	                   
	//                     If you are having any issues, please contact:

	//                     <br><br>
	// 					'.$shop_Data['shopname'].'<br>
	// 					'.$shop_Data['phone_number'].'<br>
	// 					'.$shop_Data['address'].'<br>
	// 					'.get_option('shop_email').'<br>
	// 					<br><br>
	                
	//                     Thanks,
	//                     <br><br>
	//                		Team @'.$shop_Data['shopname'].'<br><br>
						
	// 					'
	// 					);
	                
	//                     //$emailContent = array('subject' => 'New Password Request','message' => $body);
	//                 return  email($app_users['email'], $emailContent,$app_users['registered_shop']);
	// 	}
	// 	else
	// 		return false;
	// }

	// Forgot Password email trigger
    function forgotPassword($updateArray ,$app_users,$shopno){
        if(trim($app_users['email']) != ''){
			$userArray = $this->db2->get_where($this->table_name, array('email' => trim($app_users['email'])))->row_array();
		}

        if(empty($userArray)){
            return false;
        }
        else{
			$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shopno);

            $change_password_token = md5($userArray['id'].'_'.generateRandomString());
            $userArray['link'] = base_url() . 'User/resetPassword/' . $change_password_token;
			
			$this->db2->update($this->table_name,array('reset_password_token' => $change_password_token),array('id'=>$app_users['id']));
           
            // send reset pass link
            $emailContent = array('subject' => 'Recover your Pet Commander Password!','message' =>'
				Hello '.$app_users['username'].',<br><br>
                
                If you have forgotten your password, you can click on the link below to set a new one.<br/><br/>
                
                '.$userArray['link'].'<br/><br/>
                
                Thanks,
	            <br><br>
				Team @'.$shop_Data['shopname'].'<br><br>');
				
            return email($app_users['email'],$emailContent,$app_users['registered_shop']);
        }
    }

    //token in email
    public function getUserByToken($token = '')
    {
        $result = $this->db2->get_where($this->table_name,array('reset_password_token' => trim($token)))->row_array();

        if ($result <= 0) {
            return $this->db2->get_where($this->table_name,array('reset_password_token' => trim($token)))->row_array();
        } else {
            return $result;
        }
    }

    // Reset password after forgot password
    public function resetPassword($postArray)
    {
		$app_users = $this->db2->get_where($this->table_name,array('email' => trim($postArray["email"])))->row_array();
		
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($postArray['shopno']);

        if(!empty($app_users)) {
            $result = $this->db2->get_where($this->table_name, array('reset_password_token' => $postArray['token']))->row_array();
            $name = $result['name'];

            $emailContent = array('subject' => 'Your Pet Commander Password Reset Successfully!','message' =>'
            Hello '.ucfirst($name).' ,<br/><br/>
            
            Your Pet Commander Password Reset Successfully!<br/><br/>
            
            Thanks,
	        <br><br>
			Team @'.$shop_Data['shopname'].'<br><br>');
            email($app_users['email'], $emailContent,$app_users['registered_shop']);

			$options = [
				'cost' => 5,
			];
			$newPassword =  password_hash($postArray['new_password'], PASSWORD_BCRYPT, $options);

            return $this->db2->update($this->table_name, array('password' => $newPassword, 'reset_password_token' => ''), array('reset_password_token' => $postArray['token']));
        } 
    }

	function homePageDetails($user_id,$shop_no)
	{
		$return = array();

		$services = $this->GlobalApp_model->getAllServices();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$tempServices = array();
		

		$data = array();

		$return['boardingSettings'] = $this->Management_model->getBoardingSettings();
        $return['boardingPrice']=$this->Management_model->getBoardingPrice();
        //	$return['boardingInventory'] = $this->Management_model->getBoardingInventory();
        $tempArray = array();
        if(get_option('appointment_switch') == 1)
		{
			if(!empty($services))
			{
				foreach ($services as $key => $value) {
					$temp = array();
					$temp = $value;
					$temp['is_boarding'] = FALSE;
					array_push($tempServices, $temp);
				}
			}
		}

		if(!empty($return['boardingSettings']) && $this->checkDayCareProfileComplete() && get_option('daycare_switch') == 1)
        {
        	
	        	$temp = array();
	        	$temp['id'] = 0;
	        	$temp['name'] = $return['boardingSettings']['dayCareTitle'];
	        	$temp['type'] = 'Day Care';
	        	$temp['image'] = $return['boardingSettings']['dayCareImage'];
	        	$temp['cost'] =  $return['boardingSettings']['normalDayCarePricing'];
	        	$temp['time_estimate'] = 0;
	        	$temp['is_deleted'] = 0;
	        	$temp['description'] = $return['boardingSettings']['dayCareDescription'];
	        	$temp['is_boarding'] = TRUE;
	        	array_push($tempServices, $temp);
	        	array_push($tempArray, $temp);
	        
	   	}

	   	if(!empty($return['boardingSettings']) && $this->checkBoardingProfileIsComplete() && get_option('boarding_switch') == 1)     	
        {	
        		
	        	$temp = array();
	        	$temp['id'] = 0;
	        	$temp['name'] = $return['boardingSettings']['boardingTitle'];
	        	$temp['type'] = 'Boarding';
	        	$temp['image'] = $return['boardingSettings']['boardingImage'];
	        	$temp['cost'] =  0;
	        	$temp['time_estimate'] = 0;
	        	$temp['is_deleted'] = 0;
	        	$temp['description'] = $return['boardingSettings']['boardingDescription'];
	        	$temp['is_boarding'] = TRUE;
	        	array_push($tempServices, $temp);
	        	array_push($tempArray, $temp);
	        
        }

        $return['dayCareBoarding'] = $tempArray;
		$return['services'] = $tempServices;
		$return['shopInfo'] = $this->GlobalApp_model->getBrandingInfo();
		$return['shopAvailabilty']= $this->Management_model->getshopAvailabilty();
        $return['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
		$return['petImageUploadPath'] = base_url().UPLOAD_PETS;
		$return['serviceImageUploadPath'] = base_url().SHOP_IMAGES;
		$return['shopImageUploadPath'] = base_url().SHOP_IMAGES;
		$return['imageSharingPathForGalleryImage'] = 'https://storage.googleapis.com/'.BUCKETNAME.'/'.WatermarkImages;
		$return['notificationsCount'] = $this->getAllNotificationCount(array('id'=>$user_id),$shop_id);
		$shop = $this->Appuser_model->getShopDetailsForAjax($shop_no);
		$root = "http://".$_SERVER['HTTP_HOST'] ;
        $root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        $return['shopName'] = $shop['shopname'];  
        $return['superAdminTermCondtionUrl'] =  $root.'Superadmin/showtermServicPreview';
        // used for app where baseurl contains shop no
        // $return['groomerShopTermConditionUrl'] = base_url().'Admin/showtermServicPreview';
        $return['groomerShopTermConditionUrl'] = base_url().$shop_no.'/'.'Admin/showtermServicPreview';
		return $return;
	}

	function getPets($user_id,$shop_no,$id)
	{
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$this->db2->select('pets.*');
		$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('pets.id'=>$id,'pets.is_deleted'=>0,'pets.app_user_id'=>$user_id));
		$query = $this->db2->get('pets')->row_array();
		return $query;

	}

	function getAllPets($user_id,$shop_no)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$return = array();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$this->db2->select('pets.*');
		$this->db2->order_by('pets.id','desc');
		$this->db2->join('app_users','pets.app_user_id=app_users.id');
		// $this->db2->join('breeds','pets.breed_id =breeds.id');
		$this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		$this->db2->where(array('pets.is_deleted'=>0,'pets.app_user_id'=>$user_id));
		// $this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted'=>0,'pets.app_user_id'=>$user_id));   old: change by sanjeev
		$query = $this->db2->get('pets')->result_array();
		
		if(!empty($query))
		{
			foreach ($query as $key => $value) {
				$temp = array();
				$temp = $value;

				$temp['active_appointment'] = $this->db->where(array('pet_id'=> $value['id'],'app_user_id'=> $user_id,'status!='=>'Cancel','status!='=>'Checkout
				'))->count_all_results('appointments') + $this->db->where(array('pet_id'=> $value['id'],'app_user_id'=> $user_id,'status!='=>'Cancel','status!='=>'Checkout'))->count_all_results('boardings');

				$temp['total_appointment'] = $this->db->where(array('pet_id'=> $value['id'],'app_user_id'=> $user_id))->count_all_results('appointments') + $this->db->where(array('pet_id'=> $value['id'],'app_user_id'=> $user_id))->count_all_results('boardings');

				$temp['active_vaccination'] = $this->db2->where(array('pet_id'=> $value['id'],'added_by_user'=> $user_id,'expiry_on >'=>date('y-m-d')))->count_all_results('pets_notes');

				$temp['total_vaccination'] = $this->db2->where(array('pet_id'=> $value['id'],'added_by_user'=> $user_id))->count_all_results('pets_notes');

				$temp['bread'] = $value['breed'];
				$temp['notes'] = $this->getAllPetNotes($value['id'],$shop_id); //All Pet Notes in an subarray
				$temp['veterinarian'] = $this->db2->get_where('veterinarian',array('pet_id'=>$value['id']))->row_array();
				array_push($return,$temp);
			}
		}

		return $return;

	}
	
	function contractUpload($pet_id,$allow_type,$filename){
		if($allow_type =='Grooming'){
			$insert['appointment_contract']=$filename;
			$insert['appointment_expiry']= date('Y-m-d', strtotime('+1 years'));
		}else{
			$insert['boarding_contract']=$filename;
			$insert['boarding_expiry']= date('Y-m-d', strtotime('+1 years'));
			
			$insert['daycare_contract']=$filename;
			$insert['daycare_expiry']= date('Y-m-d', strtotime('+1 years'));
		}
		if($this->db2->update('pets',$insert,array('id'=>$pet_id))){
			return true;
		}else{
			return false;
		}
	}

	function getAllPetsForAppointmentAddByAppointmentDate($user_id,$shop_no)
	{
		$return = array();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		// $this->db2->select('pets.*,breeds.name as bread');
		// $this->db2->order_by('pets.id','desc');
		// $this->db2->join('app_users','pets.app_user_id=app_users.id');
		// $this->db2->join('breeds','pets.breed_id =breeds.id');
		// $this->db2->join('app_users_shops','app_users_shops.app_user_id = app_users.id');
		// //$this->db2->join('transactions','transactions.pet_id = pet_id','left');
		// //$this->db2->order_by('ISNULL(max(transactions.start),null)');
		// $this->db2->where(array('app_users_shops.shop_id'=> $shop_id,'pets.is_deleted'=>0,'pets.app_user_id'=>$user_id));
		
		// $query = $this->db2->get('pets')->result_array();

		$query = $this->db2->query('SELECT `pets`.*, `breeds`.`name` as `bread` , (select date(transactions.start) from transactions where transactions.shop_id = '.$shop_id.' and transactions.pet_id = pets.id and transactions.transcation_type = "payment" ORDER by transactions.start DESC limit 1) as last_appointment_date FROM `pets` JOIN `app_users` ON `pets`.`app_user_id`=`app_users`.`id` JOIN `breeds` ON `pets`.`breed_id` =`breeds`.`id` JOIN `app_users_shops` ON `app_users_shops`.`app_user_id` = `app_users`.`id` WHERE `app_users_shops`.`shop_id` = '.$shop_id.' AND `pets`.`is_deleted` =0 AND `pets`.`app_user_id` = '.$user_id.' AND pets.is_deceased = 0 ORDER BY last_appointment_date')->result_array();

		//echo $this->db2->last_query();exit;
		if(!empty($query))
		{
			foreach ($query as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['notes'] = $this->getAllPetNotes($value['id'],$shop_id); //All Pet Notes in an subarray
				// $temp['last_appointment_date'] = $this->getLastAppoinmentForPet($value['id'],$shop_id);
				array_push($return,$temp);
			}
		}
		//pr($return);exit;
		return $return;
	}

	function getLastAppoinmentForPet($pet_id,$shop_id)
	{
		$this->db2->select('max(transactions.start) as appointment_date');
		$result = $this->db2->get_where('transactions',array('shop_id'=>$shop_id,'pet_id'=>$pet_id,'transcation_type'=>'payment','status'=>'success','is_add_on_payment'=>0))->row_array();
		//pr()
		if(!empty($result) && !empty($result['appointment_date']))
			return $result['appointment_date'];
		else
			return 'N/A';
	}

	function getAllPetNotes($id,$shop_id)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$this->db2->select('pets_notes.*,DATE_FORMAT(date_of, "%Y-%m-%dT%TZ") AS date_of_formatted , DATE_FORMAT(expiry_on, "%Y-%m-%dT%TZ") AS expiry_on_formatted');
		return $this->db2->get_where('pets_notes',array('pet_id'=>$id,'shop_id'=>$shop_id))->result_array();
	}


	function getPetDetails($user_id,$shop_no,$postarray)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		// $this->db2->select('pets.*,breeds.name as bread');
		$this->db2->where(array('pets.id'=> $postarray['id']));
		// $this->db2->join('breeds','pets.breed_id = breeds.id');
		$query = $this->db2->get('pets')->row_array();
		
		if(!empty($query))
		{
			$query['bread'] = $query['breed'];
			$query['notes'] = $this->getAllPetNotes($query['id'],$shop_id);
		}
		return $query;
	}

	function appointmentExpiry($id){
		$result = $this->db2->query("SELECT * FROM `pets` WHERE `appointment_expiry` > CURRENT_DATE AND id='$id'")->row_array() ;
		return $result['appointment_expiry'];
	}

	function getbreedName($id)
	{
		$r = $this->db2->get_where('breeds',array('id'=>$id))->row_array();
		if(empty($r))
			return 'N/A';
		else
			return $r['name'];
	}


	function addNewPet($user_id,$shop_no,$postarray,$vacc_id)
	{
		$insert =  array();
		$insert['app_user_id'] = $user_id;
		$insert['name'] = $postarray['name'];
        $insert['type']= $postarray['type'];
        $insert['gender']=$postarray['gender'];
        $insert['weight'] = $postarray['weight'];
        $insert['breed']= $postarray['breed'];
        $insert['color']= $postarray['color'];
        $insert['birth_date']= date('Y-m-d',strtotime($postarray['birth_date']));
        $insert['added']=date('Y-m-d H:i:s');
		$insert['size'] = $postarray['size'];
		if(empty($postarray['spayed'])){
			$insert['spayed'] = 'false';
		}else{
			$insert['spayed'] = 'true';
		}
        $insert['mix_breed'] = $postarray['mix_breed'];

        if(!empty($postarray['pet_cover_photo']))
        {
        	$insert['pet_cover_photo'] = $postarray['pet_cover_photo']; 
        }

        if(!empty($postarray['description']) && $postarray['description'] != 'null')
        	$insert['description'] = $postarray['description'];
        else
        	$insert['description'] = '';

        if ($this->db2->insert('pets',$insert)) {
        	$pet_id = $this->db2->insert_id();
        	$shop_id= $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

        	if(empty($postarray['veterinarian']) && empty($postarray['vet_business_name']) && empty($postarray['vet_phone_number'])){

		    }else{
		    	$vetArray = array();
		        $vetArray['veterinarian'] = $postarray['veterinarian'];
	            $vetArray['business_name'] = $postarray['vet_business_name'];
	            $vetArray['phone'] = $postarray['vet_phone_number'];
	            $vetArray['phone1'] = $postarray['vet_phone_number1'];
	            $vetArray['fax'] = $postarray['fax'];
	            $vetArray['address'] = $postarray['address'];
	            $vetArray['added_on'] = date('Y-m-d :H:i:s');
	            $vetArray['shop_id'] = $shop_id;
	            $vetArray['pet_id'] = $pet_id;
		        $this->db2->insert('veterinarian',$vetArray);
		    }

		    if(!empty($vacc_id))
        	{
				$vac_id = str_replace(array('[',']'),'',$vacc_id);
				$this->db2->query("UPDATE `pets_notes` SET `pet_id`=$pet_id WHERE id IN ($vac_id)");
        	}
        	return true;	
        }
        return false;

	}

// old
	// function addNewPet($user_id,$shop_no,$postarray)
	// {
	// 	$insert =  array();
	// 	$insert['app_user_id'] = $user_id;
	// 	$insert['name'] = $postarray['name'];
    //     $insert['type']= $postarray['type'];
    //     $insert['gender']=$postarray['gender'];
    //     $insert['weight'] = $postarray['weight'];
    //     $insert['breed']= $postarray['breed'];
    //     $insert['color']= $postarray['color'];
    //     $insert['birth_date']= date('Y-m-d',strtotime($postarray['birth_date']));
    //     $insert['added']=date('Y-m-d H:i:s');
	// 	$insert['size'] = $postarray['size'];
	// 	if(empty($postarray['spayed'])){
	// 		$insert['spayed'] = 'false';
	// 	}else{
	// 		$insert['spayed'] = 'true';
	// 	}
    //     $insert['mix_breed'] = $postarray['mix_breed'];

    //     if(!empty($postarray['pet_cover_photo']))
    //     {
    //     	$insert['pet_cover_photo'] = $postarray['pet_cover_photo']; 
    //     }

    //     if(!empty($postarray['description']) && $postarray['description'] != 'null')
    //     	$insert['description'] = $postarray['description'];
    //     else
    //     	$insert['description'] = '';

    //     if ($this->db2->insert('pets',$insert)) {
    //     	$pet_id = $this->db2->insert_id();
    //     	$shop_id= $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

    //     	if(empty($postarray['veterinarian']) && empty($postarray['vet_business_name']) && empty($postarray['vet_phone_number'])){

	// 	    }else{
	// 	    	$vetArray = array();
	// 	        $vetArray['veterinarian'] = $postarray['veterinarian'];
	//             $vetArray['business_name'] = $postarray['business_name'];
	//             $vetArray['phone'] = $postarray['phone'];
	//             $vetArray['phone1'] = $postarray['phone1'];
	//             $vetArray['fax'] = $postarray['fax'];
	//             $vetArray['address'] = $postarray['address'];
	//             $vetArray['added_on'] = date('Y-m-d :H:i:s');
	//             $vetArray['shop_id'] = $shop_id;
	//             $vetArray['pet_id'] = $pet_id;
	// 	        $this->db2->insert('veterinarian',$vetArray);
	// 	    }

	// 	    if(!empty($postarray['notes']))
    //     	{

    //     		if(!is_array($postarray['notes']))
    //     		{
    //     			$postarray['notes'] = json_decode($postarray['notes'],true);
    //     		}

    //     		foreach ($postarray['notes'] as $key => $value) 
    //     		{
    //     			$temp = array();
    //     			$temp['medication_name'] = $value['medication_name'];
    //     			$temp['date_of'] =  date('Y-m-d H:i:s',strtotime($value['date_of']));
    //     			$temp['expiry_on'] =  date('Y-m-d H:i:s',strtotime($value['expiry_on']));
    //     			$temp['added_on'] = date('Y-m-d H:i:s');
    //     			$temp['added_by_user'] = $user_id;
    //     			$temp['added_by'] = 'customer';
    //     			$temp['image'] = '';
        			
    //     			// if($temp[])

    //     			if(time() > strtotime($value['expiry_on']))
	// 	        	{
	// 	        		$temp['is_expired'] = 1;
	// 	        	}
	// 	        	else
	// 	        	{
	// 	        		$temp['is_expired'] = 0;
	// 	        	}

	// 	        	$temp['notes'] = '';
    //     			if(isset($value['notes']) && !empty($value['notes']))
    //     				$temp['notes'] = $value['notes'];
        			
    //     			$temp['image'] = '';
    //     			if(!empty($value['image']))    
	//                 {        
	//                    	$value['image'] = str_replace("data:image/octet-stream;base64", "data:image/jpeg;base64", $value['image']);
	//   				   	$temp['image'] = upload_pet_photo_base_64('',$value['image']);
	                    
	//                 }
	                
    // 				$temp['pet_id'] = $pet_id;
    // 				$temp['shop_id'] = $shop_id;
	// 				$temp['added_on']=date('Y-m-d H:i:s');
	// 				$temp['added_by_user'] = $user_id;
	// 				 //App User id who has added notes
    // 				$this->db2->insert('pets_notes',$temp);
    //     		}
    //     	}

    //     	/*if (!empty($postarray['medication_name'])) {
    //     		$temp = array();
	//         	$temp['pet_id'] = $pet_id;
	//         	$temp['medication_name'] = $postarray['medication_name'];
	// 			$temp['date_of'] =  date('Y-m-d H:i:s',strtotime($postarray['date_of_formatted']));
	// 			$temp['expiry_on'] =  date('Y-m-d H:i:s',strtotime($postarray['expiry_on_formatted']));
	// 			$temp['added_on'] = date('Y-m-d H:i:s');
	// 			$temp['added_by_user'] = $user_id;
	// 			$temp['added_by'] = 'customer';
	// 			$temp['image'] = '';
	// 			$temp['shop_id']= $shop_id;
	// 			if(isset($postarray['vaccination_image']) && !empty($postarray['vaccination_image']))    
	// 	        {    
	// 	        	$postarray['vaccination_image'] = str_replace("octet-stream", "jpeg", $postarray['vaccination_image']); 
	// 				$temp['image'] = upload_pet_photo_base_64('',$postarray['vaccination_image']);
	// 				//$temp['image'] = $postarray['image'];
	// 	        }
	// 	        $temp['notes'] ='';
	// 	        if(isset($postarray['notes']) && !empty($postarray['notes']))
	// 				$temp['notes'] = $postarray['notes'];


	// 	        return $this->db2->insert('pets_notes',$temp);
    //     	}*/
    //     	return true;	
    //     }
    //     return false;

	// }


	function editNewPet($user_id,$shop_no,$postarray)
	{
		 // pr($user_id);exit;
		//var_dump($postarray['notes']);
		$insert =  array();
		$insert['app_user_id'] = $user_id;
		$insert['name'] = $postarray['name'];
        $insert['type']= $postarray['type'];
        $insert['gender']=$postarray['gender'];
        $insert['weight'] = $postarray['weight'];
        $insert['breed']= $postarray['breed'];
        $insert['color']= $postarray['color'];
		$insert['birth_date']= date('Y-m-d',strtotime($postarray['birth_date']));
		if(empty($postarray['spayed'])){
			$insert['spayed'] = 'false';
		}else{
			$insert['spayed'] = 'true';
		}
		// pr($insert);exit;
        $insert['mix_breed'] = $postarray['mix_breed'];
        $insert['puppy_power'] = $postarray['puppy_power'];
        if(!empty($postarray['description']) && $postarray['description'] != 'null' )
        	$insert['description'] = $postarray['description'];
        else
        	$insert['description'] = '';
        //$insert['added']=date('Y-m-d H:i:s');

        if(isset($postarray['is_deleted']) && $postarray['is_deleted'] == 1)
        {
        	$insert['pet_cover_photo'] = '';
        }


        if(!empty($postarray['pet_cover_photo']))
        {
        	$insert['pet_cover_photo'] = $postarray['pet_cover_photo']; 
        }


        if($this->db2->update('pets',$insert,array('id'=>$postarray['id'])))
        {
        	$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
        	$get_vet = $this->db2->get_where('veterinarian',array('pet_id'=>$postarray['id']))->row_array();
        	$vetArray = array();
	        $vetArray['veterinarian'] = $postarray['veterinarian_name'];
            $vetArray['business_name'] = $postarray['business_name'];
            $vetArray['phone'] = $postarray['phone'];
            $vetArray['phone1'] = $postarray['phone1'];
            $vetArray['fax'] = $postarray['fax'];
	        

	        if(!empty($get_vet)){
    			$this->db2->update('veterinarian',$vetArray,array('pet_id' =>$postarray['id']));
			}else{
				if(empty($postarray['veterinarian_name']) && empty($postarray['business_name']) && empty($postarray['phone'])){

		    	}else{
		            $vetArray['added_on'] = date('Y-m-d :H:i:s');
		            $vetArray['shop_id'] = $shop_id;
		            $vetArray['pet_id'] = $postarray['id'];
		        	$this->db2->insert('veterinarian',$vetArray);
		        }
			}
        	if(!empty($postarray['notes']))
        	{

        		if(!is_array($postarray['notes']))
        		{
        			$postarray['notes'] = json_decode($postarray['notes'],true);
        		}

        		foreach ($postarray['notes'] as $key => $value) 
        		{
        			$temp = array();
        			if(isset($value['is_deleted']) && $value['is_deleted'] == 1)
        			{
        				$this->db2->delete('pets_notes',array('id'=>$value['id']));
        			}
        			else
        			{
        				
	        			$temp['medication_name'] = $value['medication_name'];
	        			$temp['date_of'] =  date('Y-m-d H:i:s',strtotime($value['date_of']));
	        			$temp['expiry_on'] =  date('Y-m-d H:i:s',strtotime($value['expiry_on']));
	        			$temp['added_on'] = date('Y-m-d H:i:s');
	        			$temp['added_by_user'] = $user_id;
	        			$temp['added_by'] = 'customer';
	        			$temp['image'] = '';
	        			
	        			// if($temp[])

	        			if(time() > strtotime($value['expiry_on']))
			        	{
			        		$temp['is_expired'] = 1;
			        	}
			        	else
			        	{
			        		$temp['is_expired'] = 0;
			        	}

	        			if(isset($value['notes']) && !empty($value['notes']))
	        				$temp['notes'] = $value['notes'];
	        			

	        			//if(isset($value['id']))

	        			if(isset($value['id']) && $this->checkNotesExist($value['id']))
	        			{
	        				$this->db2->update('pets_notes',$temp,array('id'=>$value['id']));
	        			}
	        			else
	        			{

	        				if(!empty($value['image']))    
			                {        
			                   
			  				   	 $temp['image'] = upload_pet_photo_base_64($value['id'],$value['image']);
			                    
			                }
			               
	        				
	        				$temp['pet_id'] = $postarray['id'];
	        				$temp['shop_id'] = $shop_id;
        					$temp['added_on']=date('Y-m-d H:i:s');
        					$temp['added_by_user'] = $user_id;
        					 //App User id who has added notes
	        				$this->db2->insert('pets_notes',$temp);
	        			}
        			}
        		}

        	}
        	return true;
        }
        return false;

	}


	function checkNotesExist($id)
	{
		$result = $this->db2->get_where('pets_notes',array('id'=>$id))->row_array();
		if(!empty($result))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function deletePet($user_id,$shop_no,$postarray)
	{
		return $this->db2->update('pets',array('is_deleted' => 1),array('id'=>$postarray['id']));
	}

	function addPetNote($user_id,$postarray,$shopno)
	{
		$insert =  array();
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['medication_name'] = $postarray['medication_name'];
       
        if(empty($postarray['date_of'])){
    		$insert['date_of'] = date('Y-m-d H:i:s');
    	}else{
    		 $insert['date_of']= date('Y-m-d H:i:s',strtotime($postarray['date_of']));
    	}
        $insert['expiry_on']=date('Y-m-d H:i:s',strtotime($postarray['expiry_on']));
        $insert['notes']=$postarray['notes'];
        $insert['shop_id']=$this->GlobalApp_model->getShopIdByShopPhoneNumber($shopno);
        $insert['added_on']=date('Y-m-d H:i:s');
        $insert['added_by_user'] = $user_id; //App User id who has added notes
        $insert['added_by'] = 'customer';
        return $this->db2->insert('pets_notes',$insert);

	}


	function editPetNote($user_id,$postarray,$shopno)
	{
		$insert =  array();
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['medication_name'] = $postarray['medication_name'];
        $insert['date_of']= date('Y-m-d H:i:s',strtotime($postarray['date_of']));
        $insert['expiry_on']=date('Y-m-d H:i:s',strtotime($postarray['expiry_on']));
        $insert['notes']=$postarray['notes'];       
        $insert['added_by_user'] = $user_id;
        
        return $this->db2->update('pets_notes',$insert,array('id'=>$postarray['id']));

	}

	function deletePetNote($user_id,$postarray,$shopno)
	{
		return $this->db2->delete('pets_notes',array('id'=>$postarray['id']));
	}

	function getAllCards($user_id)
	{
		return $this->db2->get_where('app_users_cards',array('app_user_id'=>$user_id,'is_deleted'=>0))->result_array();
	}

	function addCard($user_id,$cardArray,$result)
	{

		//$this->db2->update('app_users_cards',array('is_default'=>0),array('app_user_id'=>$user_id));

		//pr($cardArray);exit;

		$insert = array();
		$insert['cardToken'] = $cardArray['id'];
		
		if(!empty($cardArray['name']))
		{
			$insert['cardHolderName'] = $cardArray['name'];
		}
		else
		{
			$insert['cardHolderName'] = 'N/A';
		}

		//Added feild Brand ie visa for mobile app
		if(!empty($cardArray['brand']) || $cardArray['brand'] == 'Unknown')
		{
			$insert['brand'] = $cardArray['brand'];
		}
		else
		{
			$insert['brand'] = "N/A";
		}


		$insert['card_fingerprint'] = $cardArray['fingerprint'];


		$insert['cardNumber'] = $cardArray['last4'];
		$insert['cardExpriry'] = $cardArray['exp_month'].'/'. $cardArray['exp_year'];
		
		if(!empty($result))
			$insert['is_default'] = 0;
		else
			$insert['is_default'] = 1;

		$insert['app_user_id'] = $user_id;

		return $this->db2->insert('app_users_cards',$insert);

	}

	function makeDefaultCard($app_userArray,$cardDetailArray)
	{
		if($this->db2->update('app_users_cards',array('is_default'=>0),array('app_user_id'=>$app_userArray['id'])))
		{
			//pr($cardDetailArray);exit;
			return $this->db2->update('app_users_cards',array('is_default'=>1),array('id'=>$cardDetailArray['id']));
		}
		else
			return false;
	}


	function deleteCard($user_id,$card_id,$CustomerArray,$resultArray)
	{
		//Complete delete from database
		if(!empty($resultArray['default_source']))
		{
			$this->db2->update('app_users_cards',array('is_default'=>0),array('app_user_id'=>$user_id));
			$this->db2->update('app_users_cards',array('is_default'=>1),array('cardToken'=>$resultArray['default_source']));
		}

		return $this->db2->delete('app_users_cards',array('id'=>$card_id));
	}

	function softdeleteCard($user_id,$card_id)
	{
		//Soft delete to keep record for history purpose
		return $this->db2->update('app_users_cards',array('is_deleted'=>1),array('id'=>$card_id));
	}


	function getAllAppointments($user_id,$shop_no,$appuser,$pageData)
	{
		//Get All User Appointment for a user

		$limit = 100;
		if($pageData == 0)
		{
			$start = 0;	
		}
		else
		{
			$start = $pageData*100;
		}
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$returnArray = array();
		$return = array();
		
		//$this->db->order_by('id','DESC');
		// $this->db->limit($limit,$start);
		// $this->db->where_in('status',array('Pending','Confirm','Inprocess'));
		// $this->db->where('DATE(appointment_date) >=','CURDATE()',FALSE);
		// $result = $this->db->get_where('appointments',array('app_user_id'=>$user_id))->result_array();
		// //echo $this->db->last_query();
		// if(!empty($result))
		// {
		// 	foreach ($result as $key => $value) {
		// 		$temp = array();
		// 		$temp = $value;

		// 		$check = $this->getAppointmentImagesById($value['id'],'appointment');
				
		// 		if(!empty($check))
		// 			$temp['showGallery'] = 1;
		// 		else
		// 			$temp['showGallery'] = 0;

		// 		$temp['category'] = 'appointment';
		// 		$temp['services'] = $this->getAllAppointmentsServicesByAppointmentId($value['id']);
		// 		$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
		// 		array_push($return,$temp);			
		// 	}
		// }
		


		// $return1 = array();
		// $this->db->select('boardings.* , boardings.start_date as appointment_date');
		// //$this->db->order_by('id','DESC');
		// $this->db->limit($limit,$start);
		// $this->db->where_in('status',array('Pending','Confirm','Inprocess'));
		// $this->db->group_start();
		// $this->db->where('DATE(start_date) >=','CURDATE()',FALSE);
		// $this->db->or_where('DATE(end_date) >=','CURDATE()',FALSE);
		// $this->db->group_end();
		// $result = $this->db->get_where('boardings',array('app_user_id'=>$user_id))->result_array();
		// //echo $this->db->last_query();
		// if(!empty($result))
		// {
		// 	foreach ($result as $key => $value) {
		// 		$temp = array();
		// 		$temp = $value;

		// 		$check = $this->getAppointmentImagesById($value['id'],'daycare/boarding');
				
		// 		if(!empty($check))
		// 			$temp['showGallery'] = 1;
		// 		else
		// 			$temp['showGallery'] = 0;

		// 		$temp['services'] = array(array('name' => $value['category']));
		// 		$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
		// 		$temp['appointment_date'] = $value['start_date'];
		// 		$temp['appointment_end_date'] = $value['end_date'];
		// 		$temp['appointment_time'] = $value['start_time'];
		// 		$temp['appointment_end_time'] = $value['end_time'];
		// 		$temp['cost'] = $value['amount'];
		// 		array_push($return1,$temp);			
		// 	}
		// }

		// $returnArray = array_merge($return,$return1);

		// //array_multisort($returnArray,SORT_DESC,SORT_NUMERIC);
		// usort($returnArray, array($this,'cmp'));
		// //pr($returnArray);exit;

		$fullResult = $this->db->query("Select * From( (select appointments.id as id,appointments.appointment_date as appointment_date,'appointment' As category from appointments WHERE status IN('Pending', 'Confirm', 'Inprocess','Complete','Checkout') AND DATE(appointment_date) >=CURDATE() AND app_user_id = ".$user_id.") UNION ALL (select boardings.id as id,boardings.start_date as appointment_date,boardings.category As category from boardings WHERE status IN('Pending', 'Confirm', 'Inprocess','Complete','Checkout') AND (DATE(start_date) >=CURDATE() OR DATE(end_date) >=CURDATE() ) AND app_user_id = ".$user_id.") ) result order by appointment_date limit ".$start.",".$limit)->result_array();
		// echo $this->db->last_query();exit;
		$latLongArray = $this->GlobalApp_model->getLangLong();
		// pr($fullResult);exit;
		if(!empty($fullResult))
		{
			foreach ($fullResult as $k => $v) {
				
				if($v['category'] == 'appointment')
				{
					$result = $this->db->get_where('appointments',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'appointment');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['category'] = 'appointment';
							$temp['services'] = array();

                            $services_arr= $this->getAllAppointmentsServicesByAppointmentId($value['id']);

                            $add_on_services = $this->getAllAppointmentsAddOnServicesByAppointmentId($value['id']);

                            if(!empty($add_on_services)){
                                $temp['services'] =    array_merge($services_arr,$add_on_services);
                            }else{
                                $temp['services'] = $services_arr;
                            }
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['payments'] = $this->getPaymentsDetails($value['id'],'appointment',$shop_id);

							// $temp['Appo_id'] = $value['id'];
							// $temp['cat'] = 'appointment';
							// $temp['shop_no'] = $shop_no;

							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}	
				}
				else
				{
					$result = $this->db->get_where('boardings',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'daycare/boarding');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['services'] = array(array('name' => $value['category']));
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['payments'] = $this->getPaymentsDetails($value['id'],$value['category'],$shop_id);
							// $temp['Appo_id'] = $value['id'];
							// $temp['cat'] = $value['category'];
							// $temp['shop_no'] = $shop_no;
							
							$temp['appointment_date'] = $value['start_date'];
							$temp['appointment_end_date'] = $value['end_date'];
							$temp['appointment_time'] = $value['start_time'];
							$temp['appointment_end_time'] = $value['end_time'];
							$temp['cost'] = $value['amount'];
							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}
				}	

			}
		}
		// pr($return);exit;
		return $return;	

	}

	function getCheckoutAppointments1($user_id,$shop_no){
		$result = $this->db->query("SELECT * FROM `appointments` WHERE `app_user_id` = ".$user_id." AND `status` IN ('Cancel', 'Checkout')")->result_array();

		foreach ($result as $key => $value) {
			$pet = $this->db2->get_where('pets',array('id'=>$value['pet_id'],'app_user_id'=>$value['app_user_id']))->row_array();
			$result[$key]['name']= $pet['name'];
			$result[$key]['category']='grooming';
		}
		
		return $result;
	}

	function getCancelAppointments($user_id,$shop_no){

		$result = $this->db->query("SELECT * FROM `boardings` WHERE `app_user_id` = ".$user_id." AND `status` IN ('Cancel', 'Checkout')")->result_array();
		foreach ($result as $key => $value) {
			$pet = $this->db2->get_where('pets',array('id'=>$value['pet_id'],'app_user_id'=>$value['app_user_id']))->row_array();
			$result[$key]['name']= $pet['name'];
		}
		return $result;
	}

	function getCheckoutAppointments($user_id,$shop_no,$appuser,$pageData)
	{
		//Get All User Appointment for a user

		$limit = 10;
		if($pageData == 0)
		{
			$start = 0;	
		}
		else
		{
			$start = $pageData*10;
		}
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$returnArray = array();
		$return = array();

		$fullResult = $this->db->query("Select * From( (select appointments.id as id,appointments.appointment_date as appointment_date,'appointment' As category from appointments WHERE status='Checkout' AND app_user_id = ".$user_id.") UNION ALL (select boardings.id as id,boardings.start_date as appointment_date,boardings.category As category from boardings WHERE status='Checkout' AND app_user_id = ".$user_id.") ) result order by appointment_date limit ".$start.",".$limit)->result_array();
		// echo $this->db->last_query();exit;
		$latLongArray = $this->GlobalApp_model->getLangLong();
		// pr($fullResult);exit;
		if(!empty($fullResult))
		{
			foreach ($fullResult as $k => $v) {
				
				if($v['category'] == 'appointment')
				{
					$result = $this->db->get_where('appointments',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'appointment');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['category'] = 'appointment';
							$temp['services'] = array();

                            $services_arr= $this->getAllAppointmentsServicesByAppointmentId($value['id']);

                            $add_on_services = $this->getAllAppointmentsAddOnServicesByAppointmentId($value['id']);

                            if(!empty($add_on_services)){
                                $temp['services'] =    array_merge($services_arr,$add_on_services);
                            }else{
                                $temp['services'] = $services_arr;
                            }
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['payments'] = $this->getPaymentsDetails($value['id'],'appointment',$shop_id);

							// $temp['Appo_id'] = $value['id'];
							// $temp['cat'] = 'appointment';
							// $temp['shop_no'] = $shop_no;

							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}	
				}
				else
				{
					$result = $this->db->get_where('boardings',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'daycare/boarding');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['services'] = array(array('name' => $value['category']));
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['payments'] = $this->getPaymentsDetails($value['id'],$value['category'],$shop_id);
							// $temp['Appo_id'] = $value['id'];
							// $temp['cat'] = $value['category'];
							// $temp['shop_no'] = $shop_no;
							
							$temp['appointment_date'] = $value['start_date'];
							$temp['appointment_end_date'] = $value['end_date'];
							$temp['appointment_time'] = $value['start_time'];
							$temp['appointment_end_time'] = $value['end_time'];
							$temp['cost'] = $value['amount'];
							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}
				}	

			}
		}
		// pr($return);exit;
		return $return;	

	}

	function getAppointmentImagesById($id,$category)
	{
		if($category == 'appointment')
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id,'category'=>$category))->result_array();
		else
			return $this->db->get_where('pet_appointment_images',array('reference_id'=>$id,'category != '=>'appointment'))->result_array();
	}

	function getAllAppointmentsforFilter($user_id,$shop_no,$appuser,$pageData,$status)
	{



		$limit = 10;
		if($pageData == 0)
		{
			$start = 0;
		}
		else
		{
			$start = $pageData*10;			
		}



		$returnArray = array();
		$return = array();
		// //$this->db->order_by('id','DESC');
		// $this->db->limit($limit,$start);
		// $this->db->where('status',$status);
		// if($status == 'Pending' || $status == 'Confirm' || $status == 'Inprocess' ){
		// 	$this->db->where('DATE(appointment_date) >=','CURDATE()',FALSE);
		// }	
		// else
		// {
		// 	$this->db->order_by('date(appointment_date)','DESC');
		// 	//$this->db->where('DATE(appointment_date) >=','CURDATE()',FALSE);
		// }

		// $result = $this->db->get_where('appointments',array('app_user_id'=>$user_id))->result_array();
		// //echo $this->db->last_query();
		// if(!empty($result))
		// {
		// 	foreach ($result as $key => $value) {
		// 		$temp = array();
		// 		$temp = $value;
		// 		$check = $this->getAppointmentImagesById($value['id'],'appointment');
				
		// 		if(!empty($check))
		// 			$temp['showGallery'] = 1;
		// 		else
		// 			$temp['showGallery'] = 0;

		// 		$temp['category'] = 'appointment';
		// 		$temp['services'] = $this->getAllAppointmentsServicesByAppointmentId($value['id']);
		// 		$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
		// 		array_push($return,$temp);			
		// 	}
		// }
		


		// $return1 = array();
		// $this->db->select('boardings.* , boardings.start_date as appointment_date');
		// $this->db->order_by('id','DESC');
		// $this->db->limit($limit,$start);
		// $this->db->where('status',$status);
		// if($status == 'Pending' || $status == 'Confirm' || $status == 'Inprocess' ){
		// 	$this->db->order_by('DATE(end_date)', 'ASC');
		// 	$this->db->order_by('DATE(start_date)','ASC');
			
		// }
		// else
		// {
		// 	$this->db->order_by('date(start_date)','DESC');
		// }

		// $result = $this->db->get_where('boardings',array('app_user_id'=>$user_id))->result_array();
		// //echo $this->db->last_query();
		// if(!empty($result))
		// {
		// 	foreach ($result as $key => $value) {
		// 		$temp = array();
		// 		$temp = $value;

		// 		$check = $this->getAppointmentImagesById($value['id'],'daycare/boarding');
				
		// 		if(!empty($check))
		// 			$temp['showGallery'] = 1;
		// 		else
		// 			$temp['showGallery'] = 0;

		// 		$temp['services'] = array(array('name' => $value['category']));
		// 		$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
		// 		$temp['appointment_date'] = $value['start_date'];
		// 		$temp['appointment_end_date'] = $value['end_date'];
		// 		$temp['appointment_time'] = $value['start_time'];
		// 		$temp['appointment_end_time'] = $value['end_time'];
		// 		$temp['cost'] = $value['amount'];
		// 		array_push($return1,$temp);			
		// 	}
		// }

		// $returnArray = array_merge($return,$return1);

		// //array_multisort($returnArray,SORT_DESC,SORT_NUMERIC);
		// usort($returnArray, array($this,'cmp'));
		// //pr($returnArray);exit;
		$latLongArray = $this->GlobalApp_model->getLangLong();
		if($status == 'Pending' || $status == 'Confirm' || $status == 'Inprocess' ){


			$fullResult = $this->db->query("Select * From( (select appointments.id as id,appointments.appointment_date as appointment_date,'appointment' As category ,appointments.status as status from appointments WHERE status IN('".$status."') AND  app_user_id = ".$user_id.") UNION ALL (select boardings.id as id,boardings.start_date as appointment_date,boardings.category As category,boardings.status as status from boardings WHERE status IN('".$status."') AND app_user_id = ".$user_id.") ) result order by appointment_date limit ".$start.",".$limit)->result_array();
		}
		else
		{
			$fullResult = $this->db->query("Select * From( (select appointments.id as id,appointments.appointment_date as appointment_date,'appointment' As category ,appointments.status as status from appointments WHERE status IN('".$status."') AND app_user_id = ".$user_id.") UNION ALL (select boardings.id as id,boardings.start_date as appointment_date,boardings.category As category,boardings.status as status from boardings WHERE status IN('".$status."') AND  app_user_id = ".$user_id.") ) result order by appointment_date DESC limit ".$start.",".$limit)->result_array();
		}

		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);


		if(!empty($fullResult))
		{
			foreach ($fullResult as $k => $v) {
				
				if($v['category'] == 'appointment')
				{
					$result = $this->db->get_where('appointments',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'appointment');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['category'] = 'appointment';
							$temp['add_on_services'] = array(); 

							if($value['add_on_service_cost'] > 0)
							{
								$temp['add_on_services'] = $this->getAllAppointmentsAddOnServicesByAppointmentId($value['id']);
							}

							$temp['payments'] = $this->getPaymentsDetails($value['id'],'appointment',$shop_id);

							$temp['services'] = $this->getAllAppointmentsServicesByAppointmentId($value['id']);
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}	
				}
				else
				{
					$result = $this->db->get_where('boardings',array('id'=>$v['id']))->result_array();
					//echo $this->db->last_query();
					if(!empty($result))
					{
						foreach ($result as $key => $value) {
							$temp = array();
							$temp = $value;

							$check = $this->getAppointmentImagesById($value['id'],'daycare/boarding');
							
							if(!empty($check))
								$temp['showGallery'] = 1;
							else
								$temp['showGallery'] = 0;

							$temp['payments'] = $this->getPaymentsDetails($value['id'],$value['category'],$shop_id);

							$temp['services'] = array(array('name' => $value['category']));
							$temp['petDetails'] = $this->getPetDetails($user_id,$shop_no,array('id'=>$value['pet_id']));
							$temp['appointment_date'] = $value['start_date'];
							$temp['appointment_end_date'] = $value['end_date'];
							$temp['appointment_time'] = $value['start_time'];
							$temp['appointment_end_time'] = $value['end_time'];
							$temp['cost'] = $value['amount'];
							$temp['lat'] = $latLongArray['lat'];
							$temp['long'] = $latLongArray['long'];
							array_push($return,$temp);			
						}
					}
				}	

			}
		}

		return $return;	
	}

	
	function cmp($a, $b){
    	
    	if ($a['appointment_date'] == $b['appointment_date']) return 0;
    	return strtotime($a['appointment_date']) - strtotime($b['appointment_date']);

    	//return ($ad-$bd);
	}


	function getAllAppointmentsServicesByAppointmentId($id)
	{
		
		//get All services for appointment by id
		$this->db->select('services.*');
		$this->db->join('services','services.id = appointment_services.service_id');
		return $this->db->get_where('appointment_services',array('appointment_id'=>$id))->result_array();
	}


	function getAllAppointmentsAddOnServicesByAppointmentId($id)
	{
		$this->db->select('services.*');
		$this->db->join('services','services.id = appointment_addon_services.service_id');
		return $this->db->get_where('appointment_addon_services',array('appointment_id'=>$id))->result_array();
	}

	function checkAppointmentExsistForPet($postarray,$appuser,$shop_no)
	{
		//Checking if appointment exist for same pet within time
		$appointment_date = date('Y-m-d',strtotime($postarray['appointment_date']));
		$appointment_time = date('H:i:s',strtotime($postarray['appointment_time']));
		$appointment_end_time = date('H:i:s',strtotime($postarray['appointment_end_time']));
		$pet_id = $postarray['pet_id'];

    	$result= $this->db->query('SELECT appointments.* FROM appointments WHERE  Time(appointments.appointment_end_time) > "'.$appointment_time.'" And Time(appointments.appointment_time) < "'.$appointment_end_time.'"  AND DATE(appointments.appointment_date) = "'.$appointment_date.'" AND appointments.pet_id = '.$pet_id.' and (appointments.status = "Pending" or appointments.status = "Confirm")')->result_array();
    	
    	if(!empty($result))
    		return true;
    	else
    		return false;
	}


	function checkAppointmentForClosedDay($postarray,$appuser,$shop_no)
	{
		//Check Whether Shop is opened or not
		$appointment_date = date('Y-m-d',strtotime($postarray['appointment_date']));
		$appointment_time = date('H:i:s',strtotime($postarray['appointment_time']));
		$appointment_end_time = date('H:i:s',strtotime($postarray['appointment_end_time']));

    	if(!empty($appointment_date)){
    		$weekday = date('l',strtotime($appointment_date));
    		
    		$result = $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();

    		if($result['is_closed'] == 1)
    			return true;
    	}

    	return false;
	}


	function checkAppointmentForStartTime($postarray,$appuser,$shop_no)
	{
		//Check Appointment For Start time
		$appointment_date = date('Y-m-d',strtotime($postarray['appointment_date']));
		$appointment_time = date('H:i:s',strtotime($postarray['appointment_time']));
		$appointment_end_time = date('H:i:s',strtotime($postarray['appointment_end_time']));


		$weekday = date('l',strtotime($appointment_date));
		
		$result = $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();
		$date1 = DateTime::createFromFormat('H:i:s',$appointment_time);
		$date2 = DateTime::createFromFormat('H:i:s', $result['start_time']);
		$date3 = DateTime::createFromFormat('H:i:s', $result['end_time']);
		$date4 = DateTime::createFromFormat('H:i:s', $appointment_end_time);
		//if start time lies with shop timming

		if ($date1 >= $date2 && $date1 < $date3 && $date4 > $date1)
		   return false;	
		else
			return true;
		
			
    	
	}


	function getWorkingHours($category,$appointment_date)
	{
		$appointment_date = date('Y-m-d',strtotime($appointment_date));

		$weekday = date('l',strtotime($appointment_date));
		
		if($category == 'appointment')		
			$result = $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();
		else
			$result = $this->db->get_where('shop_boarding_availability',array('weekday'=>strtolower($weekday)))->row_array();

		return date('h:i a',strtotime($result['start_time'])).' - '.date('h:i a',strtotime($result['end_time']));
		
	}


	function checkUserCard($appuser)
	{
		//check if user has added card
		if(($this->db2->get_where('app_users_cards',array('is_default'=>1,'is_deleted'=>0,'app_user_id'=>$appuser['id']))->num_rows()) > 0)
		{	
			return true;
		}	
		return false;
	}


	function checkForOverBooking($appuser,$postarray,$shop_no)
	{
		$appointment_date = date('Y-m-d',strtotime($postarray['appointment_date']));
		$appointment_time = date('H:i:s',strtotime($postarray['appointment_time']));
		$appointment_end_time = date('H:i:s',strtotime($postarray['appointment_end_time']));	

		$result = $this->Entry_model->getOverBookingAppointments($appointment_time,$appointment_end_time,$appointment_date,$postarray['services'],'Confirm');
		
		$result1 = $this->Entry_model->getOverBookingAppointments($appointment_time,$appointment_end_time,$appointment_date,$postarray['services'],'Pending');
		
		if(!empty($result) || !empty($result1))
			return true;

		return false;

	}

	function getUserDefaultCard($appuser)
	{
		return $this->db2->get_where('app_users_cards',array('is_default'=>1,'app_user_id'=>$appuser['id']))->row_array();
	}


	function addPendingBoarding($appuser,$postarray,$finalAmount,$shop_no)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		
		$insert['start_date'] = date('Y-m-d',strtotime($postarray['start_date']));
		$insert['start_time'] = date('H:i:s',strtotime($postarray['start_time']));
		$insert['end_time'] = date('H:i:s',strtotime($postarray['end_time']));
		$insert['app_user_id'] = $appuser['id'];
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['amount'] = $finalAmount;
		$insert['email'] = $appuser['email'];
		$insert['category'] = $postarray['category'];

		if($insert['category'] == 'daycare'){
    		$insert['end_date'] = date('Y-m-d',strtotime($postarray['start_date'])); 
    	}
    	else
			$insert['end_date'] = date('Y-m-d',strtotime($postarray['end_date']));
		
		$insert['added_on'] =  date('Y-m-d H:i:s');
    	$insert['updated_on'] =  date('Y-m-d H:i:s');

		if(!empty($appuser['phone']))
			$insert['phone'] = $appuser['phone'];

		if(date("Y-m-d") >= date('Y-m-d',strtotime($postarray['start_date'])))
    	{
    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
    		$insert['can_be_canceled'] = 1;
    	}




		$insert['overlapping_appointment'] = $postarray['overlapping_appointment'];
		//$insert['extented_time'] = $postarray['extented_time'];

		
		$insert['status'] = 'Pending';
		$emailMsg = 'has been added successfully';
		$AdminSubj = 'You Have Received A New '.ucfirst($postarray['category']).' Request - '.PROJECTNAME.' ';
		$AdminMsg = 'You have received a new '.ucfirst($postarray['category']).' request with overbooking status,you need to confirm it one day before appointment date<br>Details are mention below';
		$notificationsTitle = 'Added';
		
		

		$insert['payment_method'] = 'online';
		$insert['payment_status'] = 'pending';
		$insert['transfer_status'] = 'pending';

		$insert['created_by'] = '';
		$insert['booked_from'] = 'mobile';
		//Adding In Groomer Database
	   	$this->db->insert('boardings',$insert);
	   	$insert['id'] = $this->db->insert_id();

	  
	   	

	   
	   	$payment = array();
	   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	   	$payment['category'] = $postarray['category'];
	   	$payment['transaction_id'] = '';
	   	$payment['amount'] = $postarray['cost'];
	   	$payment['stripe_charge'] = $postarray['stripe_charge'];
	   	$payment['commission'] = $postarray['commission'];
	   	$payment['reference_id'] = $insert['id'];
	   	$payment['added_on'] = date('Y-m-d H:i:s');
	   	$payment['status'] = 'pending';
	   	$payment['transcation_type'] = 'payment';
	   	$payment['app_user_id'] = $appuser['id'];
	   	$payment['payment_mode'] = 'online';
	   	//Adding Woodles Superadmin Add database 
	   	$this->db2->insert('transactions',$payment);

	   	//Confirmation Email To User
	   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$insert['pet_id']));
		$AppUserDetails = $appuser;
		//$serviceStr = $this->getServiceArray($service);
		$admin_user = $this->db->get_where('admin_users')->row_array();

		if($insert['category'] == 'boarding'){
			$endMsgLine = date(get_option('date_format'), strtotime($insert['end_date'])).'<br>';
		}
		else
			$endMsgLine = date(get_option('date_format'), strtotime($insert['start_date'])).'<br>';


		$emailContent = array('subject' => 'Your '.ucfirst($insert['category']).' Request '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
        	Hello '.$AppUserDetails['username'].',<br><br>
        
	        Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg.'. 
	        <br><br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total cost deducted on confirmation: '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br>
	        
	        If you are having any issues, please contact: 	

                        
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.$shop_Data['address'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

            Thanks,<br><br>
            Team @'.$shop_Data['shopname'].'<br><br>
			'
			);
	                  
	    
	    email($appuser['email'],'support@griffinapps.com',$emailContent);

	    $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

	 //    if(!empty($AppUserDetails['fcm_id'] && $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
		// {
		// 	include_once(APPPATH."libraries/PushNotifications.php");
		// 	$FCMPUSH = new PushNotifications();
		// 	if(!empty($petDetails['pet_cover_photo']))
		// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$postarray);
		// 	else
		// 		$dataArray = array('image'=>'','result'=>$postarray);

		// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
  //           $data = array('body'=>' Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg,'title'=>ucfirst($insert['category']).' Request '.$notificationsTitle,'badge'=>($badge+1),'data'=>$dataArray);
            
  //           $notifications = array();
  //           $notifications['app_user_id'] = $AppUserDetails['id'];
  //           $notifications['pet_id'] = $petDetails['id'];
  //           $notifications['title'] = $data['title'];
  //           $notifications['body'] = $data['body'];
  //           $notifications['data'] = serialize($dataArray);
  //           $notifications['added_on'] = date('Y-m-d H:i:s');
  //           $notifications['shop_id'] = $shop_id;

  //           $this->db2->insert('notifications',$notifications);
  //           $FCMPUSH->android($data,$AppUserDetails['fcm_id']);
		// }

	    //New Appointment Mail to Groomer

	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'<br>
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total cost to be recived : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br>
	        
	        If you are having any issues, please email us at '.ContactEmail.'
	        <br><br>
	        Thanks,<br><br>
	        Team @'.PROJECTNAME.'<br><br>
	        '
	        );
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email(get_option('email'),'support@griffinapps.com',$emailContent);
	}


	function addNewDayCare($appuser,$postarray,$chargeArray,$finalAmount,$shop_no)
	{
		$str = '';
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		for($i=0;$i<$postarray['daycareCount'];$i++)
		{	
			$insert = array();
			
			$insert['start_date'] = date('Y-m-d',strtotime($postarray['date'][$i]));
			$insert['start_time'] = date('H:i:s',strtotime($postarray['start_time'][$i]));
			$insert['end_time'] = date('H:i:s',strtotime($postarray['end_time'][$i]));
			$insert['app_user_id'] = $appuser['id'];
			$insert['pet_id'] = $postarray['pet_id'];
			$insert['amount'] = floatval($finalAmount/$postarray['daycareCount']);
			$insert['email'] = $appuser['email'];
			$insert['category'] = $postarray['category'];
			$insert['timezone'] = get_option('time_zone');
			
	    	$insert['end_date'] = $insert['start_date']; 
	    	
			
			$insert['added_on'] =  date('Y-m-d H:i:s');
	    	$insert['updated_on'] =  date('Y-m-d H:i:s');
	    	$insert['can_be_canceled'] = 0;

	    	$insert['discount_amount'] = $postarray['discountAmount'];
	    	$insert['discount_id'] = $postarray['discountId'];

			if(!empty($appuser['phone']))
				$insert['phone'] = $appuser['phone'];

			if(date("Y-m-d") >= date('Y-m-d',strtotime($insert['start_date'])))
	    	{
	    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
	    		$insert['can_be_canceled'] = 1;
	    	}




			$insert['overlapping_appointment'] = 0;
			//$insert['extented_time'] = $postarray['extented_time'];

			
			$insert['status'] = 'Confirm';
			$emailMsg = 'has been confirmed successfully';
			$AdminSubj = 'You Have Received A New '.ucfirst($postarray['category']).' Request - '.PROJECTNAME.'';
			$AdminMsg = 'You have received a new '.ucfirst($postarray['category']).' request with details mention below';
			$notificationsTitle = 'Confirmed';
				
			

			$insert['payment_method'] = 'online';
			$insert['payment_status'] = 'success';
			$insert['transfer_status'] = 'pending';

			// $result = $this->db->query("Select boarding_inventory.* FROM boarding_inventory where boarding_inventory.is_deleted = 0 and boarding_inventory.is_blocked = 0 and id not in (SELECT boarding_inventory.id FROM boardings join boarding_inventory on boardings.inventory = boarding_inventory.id WHERE STR_TO_DATE(CONCAT('".$insert['start_date']."', ' ','".$insert['start_time']."'), '%Y-%m-%d %H:%i:%s') < STR_TO_DATE(CONCAT(end_date, ' ', end_time), '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(CONCAT('".$insert['end_date']."', ' ','".$insert['end_time']."'), '%Y-%m-%d %H:%i:%s') > STR_TO_DATE(CONCAT(start_date, ' ', start_time), '%Y-%m-%d %H:%i:%s') and boardings.status != 'Cancel')")->row_array();
			if($insert['category'] == 'daycare')
			{
				$location = $this->db->get_where('daycare_schedule',array('current_capacity >'=>0,'date'=>$insert['start_date']))->row_array();
				if(!empty($location))
				{
					$insert['inventory'] = $location['playarea_id'];
					$insert['inventory_type'] = 'playarea';
					$insert['assigned_inventory_id'] = $location['playarea_id'];
					$insert['assigned_inventory_type'] = 'playarea';

					$this->db->update('daycare_schedule',array('current_capacity'=>($location['current_capacity']-1)),array('id'=>$location['id']));
				}
				else
				{
					$insert['inventory'] = 0;
				}	
			}	

			//Adding In Groomer Database
			if($insert['category'] == 'daycare')
				$insert['inventory_type'] = 'playarea';
		   	else
		   		$insert['inventory_type'] = 'boardings';


		   	$insert['created_by'] = '';
			$insert['booked_from'] = 'mobile';

		   	$this->db->insert('boardings',$insert);
		   	
		   	$insert['id'] = $this->db->insert_id();
		   	//pr($insert);exit;
		  
		   	

		   
		   	$payment = array();
		   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		   	$payment['category'] = $postarray['category'];
		   	$payment['transaction_id'] = $chargeArray['id'];
		   	$payment['amount'] = floatval($postarray['cost']/$postarray['daycareCount']);
		   	$payment['stripe_charge'] = floatval($postarray['stripe_charge']/$postarray['daycareCount']);
		   	$payment['commission'] = floatval($postarray['commission']/$postarray['daycareCount']);
		   	$payment['reference_id'] = $insert['id'];
		   	$payment['added_on'] = date('Y-m-d H:i:s');
		   	$payment['status'] = 'success';
		   	$payment['transcation_type'] = 'payment';
		   	$payment['app_user_id'] = $appuser['id'];
		   	$payment['payment_mode'] = 'online';
		    $payment['start'] = date('Y-m-d H:i:s',strtotime($insert['start_date'].' '.$insert['start_time']));
	   		$payment['end'] = date('Y-m-d H:i:s',strtotime($insert['start_date'].' '.$insert['end_time']));
	   		$payment['timezone'] = get_option('time_zone');
	   		$payment['cancel_by_id'] = 0;
	   		$payment['pet_id'] = $postarray['pet_id'];
		   	//Adding Woodles Superadmin Add database 
		   	$this->db2->insert('transactions',$payment);

		


		   	//Confirmation Email To User
		   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$insert['pet_id']));
			$AppUserDetails = $appuser;
			//$serviceStr = $this->getServiceArray($service);
			$admin_user = $this->db->get_where('admin_users')->row_array();

			if($insert['category'] == 'boarding'){
				$endMsgLine = date(get_option('date_format'), strtotime($insert['end_date'])).'<br>';
			}
			else
				$endMsgLine = date(get_option('date_format'), strtotime($insert['start_date'])).'<br>';

			$str .= '<tr><td style="padding: 6px;text-align:center;">'.date(get_option('date_format'), strtotime($insert['start_date'])).' '.date('h:i a', strtotime($insert['start_time'])).'</td>
				<td style="padding: 6px;text-align:center;">'.date(get_option('date_format'), strtotime($insert['end_date'])).' '.date('h:i a', strtotime($insert['end_time'])).'</td></tr>';
			
		}

		


		$emailContent = array('subject' => 'Your '.ucfirst($insert['category']).' Request '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
	        	Hello '.$AppUserDetails['username'].',<br><br>
	        
		        Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg.'. 
		        <br><br>
		        Details of daycare are as follows <br><br>
				<table style="width: 50%; border-collapse: collapse;border:1px solid #eee;">
					<thead>
				    <tr>
				      <th style="padding: 6px;text-align:center;">Check In</th>
				      <th style="padding: 6px;text-align:center;">Check Out</th>
				    </tr>
				  </thead>
				  <tfoot>
				    <tr>
				      <td style="padding: 6px;text-align:center;">Amount Paid</td>
				      <td style="padding: 6px;text-align:center;">$ '.$finalAmount.'</td>
				    </tr>
				  </tfoot>
				  <tbody>
				    '.$str.'
				  </tbody>
				</table>
		        <br><br>
		        
		        If you are having any issues, please contact: 	

                        
                <br><br>
                '.$shop_Data['shopname'].'<br>
                '.$shop_Data['phone_number'].'<br>
                '.$shop_Data['address'].'<br>
                '.get_option('shop_email').'<br>
                <br><br>

                Thanks,<br><br>
                Team @'.$shop_Data['shopname'].'<br><br>
				'
				);
		                  
		    
		    email($appuser['email'],'support@griffinapps.com',$emailContent);

		  
		    //New Appointment Mail to Groomer

		   
			
			    


	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br><br>

	        Details of daycare are as follows <br><br>
				<table style="width: 50%; border-collapse: collapse;border:1px solid #eee">
					<thead>
				    <tr>
				      <th style="padding: 6px;text-align:center;">Check In</th>
				      <th style="padding: 6px;text-align:center;">Check Out</th>
				    </tr>
				  </thead>
				  <tfoot>
				    <tr>
				      <td style="padding: 6px;text-align:center;">Amount Paid</td>
				      <td style="padding: 6px;text-align:center;">$ '.$finalAmount.'</td>
				    </tr>
				  </tfoot>
				  <tbody>
				    '.$str.'
				  </tbody>
				</table>
		        <br><br>
	        
	        If you are having any issues, please contact: 	

                        
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.$shop_Data['address'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

            Thanks,<br><br>
            Team @'.$shop_Data['shopname'].'<br><br>
			'
			);
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email(get_option('email'),'support@griffinapps.com', $emailContent);
		
		
		    
	}


	function addNewBoarding($appuser,$postarray,$chargeArray,$finalAmount,$shop_no)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$postarray['pet_id']));
		$insert['start_date'] = date('Y-m-d',strtotime($postarray['start_date']));
		$insert['start_time'] = date('H:i:s',strtotime($postarray['start_time']));
		$insert['end_time'] = date('H:i:s',strtotime($postarray['end_time']));
		$insert['app_user_id'] = $appuser['id'];
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['amount'] = $finalAmount;
		$insert['email'] = $appuser['email'];
		$insert['category'] = $postarray['category'];		
		$insert['end_date'] = date('Y-m-d',strtotime($postarray['end_date']));

		$insert['added_on'] =  date('Y-m-d H:i:s');
    	$insert['updated_on'] =  date('Y-m-d H:i:s');
    	$insert['can_be_canceled'] = 0;

    	$insert['discount_amount'] = $postarray['discountAmount'];
	    $insert['discount_id'] = $postarray['discountId'];

    	$insert['timezone'] = get_option('time_zone');

		if(!empty($appuser['phone']))
			$insert['phone'] = $appuser['phone'];
		else
			$insert['phone'] = '';

		if(date("Y-m-d") >= date('Y-m-d',strtotime($postarray['start_date'])))
    	{
    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
    		$insert['can_be_canceled'] = 1;
    	}


		$insert['overlapping_appointment'] = 0;
		//$insert['extented_time'] = $postarray['extented_time'];

		
		$insert['status'] = 'Confirm';
		$emailMsg = 'has been confirmed successfully';
		$AdminSubj = 'You Have Received A New '.ucfirst($postarray['category']).' Request - '.PROJECTNAME.'';
		$AdminMsg = 'You have received a new '.ucfirst($postarray['category']).' request with details mention below';
		$notificationsTitle = 'Confirmed';


		$start_date = $insert['start_date'];
		$end_date =  $insert['end_date'];	
		$size_priority = getSizePriority($petDetails['size']);

		$result = $this->db->query("select boarding_inventory.* from boarding_inventory where id not in (SELECT boardings.inventory from boardings where boardings.status != 'Cancel' and boardings.inventory_type = 'boarding' and category = 'boarding' and boardings.start_date < '".$end_date."' and boardings.end_date > '".$start_date."') and size_priority >= '".$size_priority."' order by size_priority asc limit 1")->row_array();
		//pr($result);
		if(!empty($result))
		{
			$insert['inventory'] = $result['id'];
			$insert['inventory_type'] = 'boarding';
			$insert['assigned_inventory_id'] = $result['id'];
			$insert['assigned_inventory_type'] = 'boarding';

			$this->db->update('boarding_schedule',array('is_booked'=>1),array('start<'=>$end_date,'end >'=>$start_date,'inventory_id'=>$result['id']));
			//echo $this->db->last_query();
		}
		else
		{
			$insert['inventory'] = 0;
			$insert['inventory_type'] = 'playarea';
			$insert['overlapping_appointment'] = 1;
			$insert['assigned_inventory_id'] = 0;
			$insert['assigned_inventory_type'] = 'playarea';
    	}
			
		

		$insert['payment_method'] = 'online';
		$insert['payment_status'] = 'pending';
		$insert['transfer_status'] = 'pending';

		// 
		$insert['created_by'] = '';
		$insert['booked_from'] = 'mobile';
		//Adding In Groomer Database
	   	$this->db->insert('boardings',$insert);
	   	$insert['id'] = $this->db->insert_id();
	   	//pr($insert);exit;
	  
	   	

	   
	   	$payment = array();
	   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	   	$payment['category'] = $postarray['category'];
	   	$payment['transaction_id'] = $chargeArray['id'];
	   	$payment['amount'] = $postarray['cost'];
	   	$payment['stripe_charge'] = $postarray['stripe_charge'];
	   	$payment['commission'] = $postarray['commission'];
	   	$payment['reference_id'] = $insert['id'];
	   	$payment['added_on'] = date('Y-m-d H:i:s');
	   	$payment['status'] = 'pending';
	   	$payment['transcation_type'] = 'payment';
	   	$payment['app_user_id'] = $appuser['id'];
	   	$payment['payment_mode'] = 'online';
	   	$payment['start'] = date('Y-m-d H:i:s',strtotime($insert['start_date'].' '.$insert['start_time']));
   		$payment['end'] = date('Y-m-d H:i:s',strtotime($insert['end_date'].' '.$insert['end_time']));
   		$payment['timezone'] = get_option('time_zone');
   		$payment['cancel_by_id'] = 0;
   		$payment['pet_id'] = $postarray['pet_id'];
	   	//Adding Woodles Superadmin Add database 
	   	$this->db2->insert('transactions',$payment);

	   	//Confirmation Email To User
	   	
		$AppUserDetails = $appuser;
		//$serviceStr = $this->getServiceArray($service);
		$admin_user = $this->db->get_where('admin_users')->row_array();

		if($insert['category'] == 'boarding'){
			$endMsgLine = date(get_option('date_format'), strtotime($insert['end_date'])).'<br>';
		}
		


		$emailContent = array('subject' => 'Your '.ucfirst($insert['category']).' Request '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
        	Hello '.$AppUserDetails['username'].',<br><br>
        
	        Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg.'. 
	        <br><br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br><br>
	        If you are having any issues, please contact: 	

                        
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.$shop_Data['address'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

            Thanks,<br><br>
            Team @'.$shop_Data['shopname'].'<br><br>
			'
			);
	                  
	    
	    email($appuser['email'],'support@griffinapps.com', $emailContent);

	    $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

	    //New Appointment Mail to Groomer

	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'<br>
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br>
	        
	        If you are having any issues, please email us at '.ContactEmail.'
	        <br><br>
	        Thanks,<br><br>
	        Team @'.PROJECTNAME.'<br><br>
	        '
	        );
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email(get_option('email'),'support@griffinapps.com', $emailContent);
	}



	function addBoarding($appuser,$postarray,$chargeArray,$finalAmount,$shop_no)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		
		$insert['start_date'] = date('Y-m-d',strtotime($postarray['start_date']));
		$insert['start_time'] = date('H:i:s',strtotime($postarray['start_time']));
		$insert['end_time'] = date('H:i:s',strtotime($postarray['end_time']));
		$insert['app_user_id'] = $appuser['id'];
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['amount'] = $finalAmount;
		$insert['email'] = $appuser['email'];
		$insert['category'] = $postarray['category'];

		if($insert['category'] == 'daycare'){
    		$insert['end_date'] = date('Y-m-d',strtotime($postarray['start_date'])); 
    	}
    	else
			$insert['end_date'] = date('Y-m-d',strtotime($postarray['end_date']));
		
		$insert['added_on'] =  date('Y-m-d H:i:s');
    	$insert['updated_on'] =  date('Y-m-d H:i:s');
    	$insert['can_be_canceled'] = 0;

		if(!empty($appuser['phone']))
			$insert['phone'] = $appuser['phone'];

		if(date("Y-m-d") >= date('Y-m-d',strtotime($postarray['start_date'])))
    	{
    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
    		$insert['can_be_canceled'] = 1;
    	}




		$insert['overlapping_appointment'] = $postarray['overlapping_appointment'];
		//$insert['extented_time'] = $postarray['extented_time'];

		if($insert['overlapping_appointment'] == 1)
		{
			$insert['status'] = 'Pending';
			$emailMsg = 'has been added successfully';
			$AdminSubj = 'You Have Received A New '.ucfirst($postarray['category']).' Request - '.PROJECTNAME.' ';
			$AdminMsg = 'You have received a new '.ucfirst($postarray['category']).' request with overbooking status,you need to confirm it one day before appointment date<br>Details are mention below';
			$notificationsTitle = 'Added';
		}
		else
		{
			$insert['status'] = 'Confirm';
			$emailMsg = 'has been confirmed successfully';
			$AdminSubj = 'You Have Received A New '.ucfirst($postarray['category']).' Request - '.PROJECTNAME.'';
			$AdminMsg = 'You have received a new '.ucfirst($postarray['category']).' request with details mention below';
			$notificationsTitle = 'Confirmed';
			
		}

		$insert['payment_method'] = 'online';
		$insert['payment_status'] = 'success';
		$insert['transfer_status'] = 'pending';

		$result = $this->db->query("Select boarding_inventory.* FROM boarding_inventory where boarding_inventory.is_deleted = 0 and boarding_inventory.is_blocked = 0 and id not in (SELECT boarding_inventory.id FROM boardings join boarding_inventory on boardings.inventory = boarding_inventory.id WHERE STR_TO_DATE(CONCAT('".$insert['start_date']."', ' ','".$insert['start_time']."'), '%Y-%m-%d %H:%i:%s') < STR_TO_DATE(CONCAT(end_date, ' ', end_time), '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(CONCAT('".$insert['end_date']."', ' ','".$insert['end_time']."'), '%Y-%m-%d %H:%i:%s') > STR_TO_DATE(CONCAT(start_date, ' ', start_time), '%Y-%m-%d %H:%i:%s') and boardings.status != 'Cancel')")->row_array();
		
		if(!empty($result))
			$insert['inventory'] = $result['id'];

		//Adding In Groomer Database
		$insert['created_by'] = '';
		$insert['booked_from'] = 'mobile';

	   	$this->db->insert('boardings',$insert);
	   	$insert['id'] = $this->db->insert_id();
	   	//pr($insert);exit;
	  
	   	

	   
	   	$payment = array();
	   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	   	$payment['category'] = $postarray['category'];
	   	$payment['transaction_id'] = $chargeArray['id'];
	   	$payment['amount'] = $postarray['cost'];
	   	$payment['stripe_charge'] = $postarray['stripe_charge'];
	   	$payment['commission'] = $postarray['commission'];
	   	$payment['reference_id'] = $insert['id'];
	   	$payment['added_on'] = date('Y-m-d H:i:s');
	   	$payment['status'] = 'success';
	   	$payment['transcation_type'] = 'payment';
	   	$payment['app_user_id'] = $appuser['id'];
	   	$payment['payment_mode'] = 'online';
	   	//Adding Woodles Superadmin Add database 
	   	$this->db2->insert('transactions',$payment);

	   	//Confirmation Email To User
	   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$insert['pet_id']));
		$AppUserDetails = $appuser;
		//$serviceStr = $this->getServiceArray($service);
		$admin_user = $this->db->get_where('admin_users')->row_array();

		if($insert['category'] == 'boarding'){
			$endMsgLine = date(get_option('date_format'), strtotime($insert['end_date'])).'<br>';
		}
		else
			$endMsgLine = date(get_option('date_format'), strtotime($insert['start_date'])).'<br>';


		$emailContent = array('subject' => 'Your '.ucfirst($insert['category']).' Request '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
        	Hello '.$AppUserDetails['username'].',<br><br>
        
	        Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg.'. 
	        <br><br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br><br>
	        
	        If you are having any issues, please contact: 	

                        
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.$shop_Data['address'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

            Thanks,<br><br>
            Team @'.$shop_Data['shopname'].'<br><br>
			'
			);
	                  
	    
	    email($appuser['email'],'support@griffinapps.com', $emailContent);

	    $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

	 //    if(!empty($AppUserDetails['fcm_id'] && $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
		// {
		// 	include_once(APPPATH."libraries/PushNotifications.php");
		// 	$FCMPUSH = new PushNotifications();
		// 	if(!empty($petDetails['pet_cover_photo']))
		// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$postarray);
		// 	else
		// 		$dataArray = array('image'=>'','result'=>$postarray);

		// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
  //           $data = array('body'=>' Your '.$insert['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg,'title'=>ucfirst($insert['category']).' Request '.$notificationsTitle,'badge'=>($badge+1),'data'=>$dataArray);
            
  //           $notifications = array();
  //           $notifications['app_user_id'] = $AppUserDetails['id'];
  //           $notifications['pet_id'] = $petDetails['id'];
  //           $notifications['title'] = $data['title'];
  //           $notifications['body'] = $data['body'];
  //           $notifications['data'] = serialize($dataArray);
  //           $notifications['added_on'] = date('Y-m-d H:i:s');
  //           $notifications['shop_id'] = $shop_id;

  //           $this->db2->insert('notifications',$notifications);
  //           $FCMPUSH->android($data,$AppUserDetails['fcm_id']);
		// }

	    //New Appointment Mail to Groomer

	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br>
			Check In Date : '.date(get_option('date_format'), strtotime($insert['start_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($insert['start_time'])).'<br>
			Check Out Date : '.$endMsgLine.'<br>
			Check Out Time : '.date('h:i a', strtotime($insert['end_time'])).'<br>
			Total Cost : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($insert['amount']).'<br>

	        <br>
	        
	        If you are having any issues, please email us at '.ContactEmail.'
	        <br><br>
	        Thanks,<br><br>
	        Team @'.PROJECTNAME.'<br><br>
	         '
	        );
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email(get_option('email'),'support@griffinapps.com', $emailContent);
	}


	function addPendingAppointment($appuser,$postarray,$finalAmount,$shop_no)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		
		$insert['appointment_date'] = date('Y-m-d',strtotime($postarray['appointment_date']));
		$insert['appointment_time'] = date('H:i:s',strtotime($postarray['appointment_time']));
		$insert['appointment_end_time'] = date('H:i:s',strtotime($postarray['appointment_end_time']));
		$insert['app_user_id'] = $appuser['id'];
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['cost'] = $finalAmount;
		$insert['user_email'] = $appuser['email'];

		if(!empty($appuser['phone']))
			$insert['user_phone_no'] = $appuser['phone'];

		if(date("Y-m-d") == date('Y-m-d',strtotime($postarray['appointment_date'])))
    	{
    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
    		$insert['can_be_canceled'] = 1;
    	}


    	$insert['overlapping_appointment'] = $postarray['overlapping_appointment'];
		$insert['extented_time'] = $postarray['extented_time'];
		
		$insert['status'] = 'Pending';
		$emailMsg = 'has been added successfully';
		$emailMsg1 = 'has been added as follows:';
		$AdminSubj = 'You Have Received A New Appointment Request - '.PROJECTNAME.' ';
		$AdminMsg = 'You have received a new appointment request with overbooking status,you need to confirm it one day before appointment date<br>Details are mentioned below';
		$notificationsTitle = 'Added';


		$insert['payment_method'] = 'online';
		$insert['payment_status'] = 'pending';
		$insert['transfer_status'] = 'pending';

		$insert['created_by'] = '';
		$insert['booked_from'] = 'mobile';

		//Adding In Groomer Database
	   	$this->db->insert('appointments',$insert);
	   	$insert['id'] = $this->db->insert_id();

	   	//Adding service to the appointment
	   	$service = explode(',', $postarray['services']);
	   	foreach ($service as $key ) {
	        $this->db->insert('appointment_services',array('service_id' => $key,'appointment_id'=>$insert['id']));
	    }

	   
	   	$payment = array();
	   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	   	$payment['category'] = 'appointment';
	   	$payment['transaction_id'] = '';
	   	$payment['amount'] = $postarray['cost'];
	   	$payment['stripe_charge'] = $postarray['stripe_charge'];
	   	$payment['commission'] = $postarray['commission'];
	   	$payment['reference_id'] = $insert['id'];
	   	$payment['added_on'] = date('Y-m-d H:i:s');
	   	$payment['status'] = 'pending';
	   	$payment['transcation_type'] = 'payment';
	   	$payment['app_user_id'] = $appuser['id'];
	   	$payment['payment_mode'] = 'online';
	   	//Adding Woodles Superadmin Add database 
	   	$this->db2->insert('transactions',$payment);

	   	//Confirmation Email To User
	   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$insert['pet_id']));
		$AppUserDetails = $appuser;
		$serviceStr = $this->getServiceArray($service);
		$admin_user = $this->db->get_where('admin_users')->row_array();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		

		$emailContent = array('subject' => 'Your Appointment '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
        	Hello '.$AppUserDetails['username'].',<br><br>
        
	        The appointment for your pet '.$petDetails['name'].' '.$emailMsg1.'. 
	        <br><br>
			Date : '.date(get_option('date_format'), strtotime($postarray['appointment_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($postarray['appointment_time'])).'<br>
			Check Out Time : '.date('h:i a', strtotime($postarray['appointment_end_time'])).'<br>
			Appointment Services : '.$serviceStr.'<br>
			
			Amount deducted on confirmation : $'.round($finalAmount,2).'<br>

	        <br><br>
	        
	       If you are having any issues, please contact: 	

            
            <br><br>
            '.$shop_Data['shopname'].'<br>
            '.$shop_Data['phone_number'].'<br>
            '.$shop_Data['address'].'<br>
            '.get_option('shop_email').'<br>
            <br><br>

            Thanks,<br><br>
            Team @'.$shop_Data['shopname'].'<br><br>
			'
			);
	                  
	    
	    email($appuser['email'],'support@griffinapps.com', $emailContent);

	    //New Appointment Mail to Groomer

	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br>
			Date : '.date(get_option('date_format'), strtotime($postarray['appointment_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($postarray['appointment_time'])).'<br>
			Check Out Time : '.date('h:i a', strtotime($postarray['appointment_end_time'])).'<br>
			Appointment Services : '.$serviceStr.'<br>
			
			Amount to be recived  : $'.round($finalAmount,2).'<br>
	        <br>
	        
	        If you are having any issues, please email us at '.ContactEmail.'
	        <br><br>
	        Thanks,<br><br>
	        Team @'.PROJECTNAME.'<br><br>
	        '
	        );
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email(get_option('email'),'support@griffinapps.com', $emailContent);

	}


	function checkAppointmentifAvailable($appuser,$postarray,$shop_no)
	{
		$service = explode(',', $postarray['services']);

	   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$postarray['pet_id']));
	    $appointment['start'] = date('H:i:s',strtotime($postarray['date'].' '.$postarray['appointment_time']));
	    $appointment['end'] = date('H:i:s',strtotime($postarray['date'].' '.$postarray['appointment_end_time']));
	   	$i = 0; $commission = 0;

	   	$this->db->where(array('appointments.appointment_time <'=> date('H:i:s',strtotime($appointment['end'])), 'appointments.appointment_end_time >'=> date('H:i:s',strtotime($appointment['start']))));
				$checkSameSlotAppointment = $this->db->get_where('appointments',array('appointment_date'=>$postarray['date'],'user_id'=>$postarray['user_id'],'status !='=>'Cancel','allocated_schedule_id'=>$postarray['allocated_schedule_id'],'overlapping_appointment'=>0))->result_array(); 
				//pr($checkSameSlotAppointment);
		//$x = 15;
		$x = get_option('stagger_time');
		if(empty($checkSameSlotAppointment))
		{
			$BackAppointmentCheck = date('H:i:s',strtotime($appointment['start'].' -'.$x.' minutes'));
			$AheadAppoinmentCheck = date('H:i:s',strtotime($appointment['start'].' +'.$x.' minutes'));

			$this->db->where(array('appointment_time >'=>$BackAppointmentCheck,'appointment_time <='=>date('H:i:s',strtotime($appointment['start']))));
			$CheckAppointment1 = $this->db->get_where('appointments',array('appointment_date'=>$postarray['date'],'user_id'=>$postarray['user_id'],'status !='=>'Cancel','allocated_schedule_id !='=>$postarray['allocated_schedule_id'],'overlapping_appointment'=>0))->result_array(); 
			
			$this->db->where(array('appointment_time <'=>$AheadAppoinmentCheck,'appointment_time >'=>date('H:i:s',strtotime($appointment['start']))));
			$CheckAppointment2 = $this->db->get_where('appointments',array('appointment_date'=>$postarray['date'],'user_id'=>$postarray['user_id'],'status !='=>'Cancel','allocated_schedule_id !='=>$postarray['allocated_schedule_id'],'overlapping_appointment'=>0))->result_array(); 	
			
			//pr($CheckAppointment1); pr($CheckAppointment2);pr($appointment);exit;

			if(empty($CheckAppointment1) && empty($CheckAppointment2))
				return true;
			else
				return false;
		}

	    return false;
	       
	}


	function addNewAppointment($appuser,$postarray,$chargeArray,$finalAmount,$shop_no)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		$insert = array();
		$postarray['appointment_date'] = $postarray['date'];
		
		$insert['appointment_date'] = date('Y-m-d',strtotime($postarray['appointment_date']));
		$insert['appointment_time'] = date('H:i:s',strtotime($postarray['appointment_time']));
		$insert['appointment_end_time'] = date('H:i:s',strtotime($postarray['appointment_end_time']));
		$insert['app_user_id'] = $appuser['id'];
		$insert['pet_id'] = $postarray['pet_id'];
		$insert['cost'] = $finalAmount;
		$insert['user_email'] = $appuser['email'];
		$insert['allocated_schedule_id'] = $postarray['allocated_schedule_id'];

		if(!empty($appuser['phone']))
			$insert['user_phone_no'] = $appuser['phone'];

		if(date("Y-m-d") == date('Y-m-d',strtotime($postarray['appointment_date'])))
    	{
    		//Appointment Of Same Day Cannot Be Cancel so setting Flag
    		$insert['can_be_canceled'] = 1;
    	}




		$insert['overlapping_appointment'] = $postarray['overlapping_appointment'];
		$insert['extented_time'] = $postarray['extented_time'];
		$insert['user_id'] = $postarray['user_id'];
		$insert['timezone'] = get_option('time_zone');

		$insert['discount_amount'] = $postarray['discountAmount'];
	    $insert['discount_id'] = $postarray['discountId'];


		$insert['commission'] = 0;


		if($insert['overlapping_appointment'] == 1 || $insert['extented_time'] == 1)
		{
			$insert['status'] = 'Pending';
			$emailMsg = 'has been added successfully';
			$emailMsg1 = 'has been added as follows:';
			$AdminSubj = 'You Have Received A New Appointment Request - '.PROJECTNAME.' ';
			$AdminMsg = 'You have received a new appointment request with overbooking status,you need to confirm it one day before appointment date<br>Details are mentioned below';
			$notificationsTitle = 'Added';
		}
		else
		{
			$insert['status'] = 'Confirm';
			$emailMsg = 'has been confirmed successfully';
			$emailMsg1 = 'has been confirmed as follows:';
			$AdminSubj = 'You Have New Appointment - '.PROJECTNAME.'';
			$AdminMsg = 'You have received a new appointment with details mentioned below';
			$notificationsTitle = 'Confirmed';
			
		}

		$insert['payment_method'] = 'online';
		$insert['payment_status'] = 'pending';
		$insert['transfer_status'] = 'pending';
		$insert['created_by'] = '';
		$insert['booked_from'] = 'mobile';
		//Adding In Groomer Database
	   	$this->db->insert('appointments',$insert);
	   	$insert['id'] = $this->db->insert_id();

	   	$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$insert['pet_id']));

	   	/*********** New logic start   ******************/
		// if puppy power ic greater than 1 then create multiple records with isDublicate = 1
		if($insert['overlapping_appointment'] != 1 && $petDetails['puppy_power'] > 1)
		{
			$groomer_schedule  = $this->db->get_where('groomer_schedule',array('user_id'=>$insert['user_id'],'Date(date)'=>$insert['appointment_date']))->result_array();
			$insert1 = $insert;
			$powerCount = 1;
			foreach ($groomer_schedule as $key => $value) {
				if($value['id'] != $insert['allocated_schedule_id']){
					$insert1['allocated_schedule_id'] = $value['id'];
					$insert1['isDuplicate'] = '1'; 
					
					$this->db->insert('appointments',$insert1);
					$powerCount++;
				}
				if ($powerCount == $petDetails['puppy_power']) {
					break;
				}
			}
		}
		/*********** End New logic ******************/

	   	//Adding service to the appointment
	   	$serviceArray = explode(',', $postarray['services']);

	   	
	   	$start = date('Y-m-d H:i:s',strtotime($insert['appointment_date'].' '.$insert['appointment_time']));
	   	$i = 0; $commission = 0;
	   	foreach ($serviceArray as $key ) {

	   		 
	   		$service = $this->db->get_where('services',array('id'=>$key))->row_array();
	   		$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$insert['user_id'],'service_id'=>$key))->row_array();
	   		// pr($groomer_services);exit;
	   		// echo $this->db->last_query();
	   		if(!empty($groomer_services))
	   			$commission += ($service[$petDetails['size'].'_cost'] * ($groomer_services['commission']/100));


	   		if($i > 0)
	   			$start = $end;
	   		
	   		$end = date('Y-m-d H:i:s',strtotime($start.' +'.$service[$petDetails['size'].'_time_estimate'].' minutes'));   
	   		
	        //echo $start; echo $end;exit; 

	        $this->db->insert('appointment_services',array('service_id' => $key,'appointment_id'=>$insert['id'],'start'=>$start,'end' => $end));
	    	
	   		
	   		//echo $groomer_services['pet_multiplier']."<br>new ";
	        //$this->updateGroomerBookings($key,$start,$end,$insert['appointment_date'],$insert['user_id'],$petDetails['size'],$groomer_services['pet_multiplier']);

	    	$i++;

	    }
	    //exit;
	    //Update Commission For appointment 

	    if($commission > 0)
	    {
	    	$this->db->update('appointments',array('commission'=>$commission),array('id'=>$insert['id']));
	    }

	   
	   	$payment = array();
	   	$payment['shop_id'] = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	   	$payment['category'] = 'appointment';
	   	// $payment['transaction_id'] = $chargeArray['id'];
	   	$payment['transaction_id'] = '';
	   	$payment['amount'] = $postarray['cost'];
	   	$payment['stripe_charge'] = $postarray['stripe_charge'];
	   	$payment['commission'] = $postarray['commission'];
	   	$payment['reference_id'] = $insert['id'];
	   	$payment['added_on'] = date('Y-m-d H:i:s');
	   	$payment['status'] = 'pending';
	   	$payment['transcation_type'] = 'payment';
	   	$payment['app_user_id'] = $appuser['id'];
	   	$payment['payment_mode'] = 'online';
	   	$payment['start'] = date('Y-m-d H:i:s',strtotime($insert['appointment_date'].' '.$insert['appointment_time']));
	   	$payment['end'] = date('Y-m-d H:i:s',strtotime($insert['appointment_date'].' '.$insert['appointment_end_time']));
	   	$payment['timezone'] = get_option('time_zone');
	   	$payment['cancel_by_id'] = 0;
	   	$payment['pet_id'] = $postarray['pet_id'];
	   	//Adding Woodles Superadmin Add database 
	   	$this->db2->insert('transactions',$payment);

	   	//Confirmation Email To User
	   	
		$AppUserDetails = $appuser;
		$serviceStr = $this->getServiceArray($serviceArray);
		//pr($serviceStr);exit;
		$admin_user = $this->db->get_where('admin_users',array('id'=>$insert['user_id']))->row_array();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		// if(!empty($AppUserDetails['fcm_id'] &&  $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
		// {
		// 	include_once(APPPATH."libraries/PushNotifications.php");
		// 	$FCMPUSH = new PushNotifications();
		// 	if(!empty($petDetails['pet_cover_photo']))
		// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$postarray);
		// 	else
		// 		$dataArray = array('image'=>'','result'=>$postarray);

		// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
  //           $data = array('body'=>' The appointment for your pet '.$petDetails['name'].' '.$emailMsg,'title'=>'Appointment '.$notificationsTitle,'badge'=>($badge+1),'data'=>$dataArray);
            
  //           $notifications = array();
  //           $notifications['app_user_id'] = $AppUserDetails['id'];
  //           $notifications['pet_id'] = $petDetails['id'];
  //           $notifications['title'] = $data['title'];
  //           $notifications['body'] = $data['body'];
  //           $notifications['data'] = serialize($dataArray);
  //           $notifications['added_on'] = date('Y-m-d H:i:s');
  //           $notifications['shop_id'] = $shop_id;

  //           $this->db2->insert('notifications',$notifications);
  //           $FCMPUSH->android($data,$AppUserDetails['fcm_id']);
		// }
		

		$emailContent = array('subject' => 'Your Appointment '.$emailMsg.' - '.PROJECTNAME.' ','message' =>'
        	Hello '.$AppUserDetails['username'].',<br><br>
        
	        The appointment for your pet '.$petDetails['name'].' '.$emailMsg1.' 
	        <br><br>
			Date : '.date(get_option('date_format'), strtotime($postarray['appointment_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($postarray['appointment_time'])).'<br>
			Check Out Time : '.date('h:i a', strtotime($postarray['appointment_end_time'])).'<br>
			Appointment Services : '.$serviceStr.'<br>
			
			Amount : $'.round($finalAmount,2).'<br>
	        <br>
	        If you are having any issues, please contact: 	

	         <br><br>
			'.$shop_Data['shopname'].'<br>
			'.$shop_Data['phone_number'].'<br>
			'.$shop_Data['address'].'<br>
			'.get_option('shop_email').'<br>
			<br><br>
	        
	        Thanks,
	        <br><br>
	        Team @'.$shop_Data['shopname'].'<br><br>
			
			'
			);
	                  
	    
	    email($appuser['email'],'support@griffinapps.com', $emailContent);

	    //New Appointment Mail to Groomer

	    $emailContent = array('subject' => $AdminSubj,'message' =>'
        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
        
	        '.$AdminMsg.' 
	        <br><br>
	        Customer Name : '.$appuser['username'].'<br>
	        Pet Name : '.$petDetails['name'].'<br>
			Date : '.date(get_option('date_format'), strtotime($postarray['appointment_date'])).'<br>
			Check In Time : '.date('h:i a', strtotime($postarray['appointment_time'])).'<br>
			Check Out Time : '.date('h:i a', strtotime($postarray['appointment_end_time'])).'<br>
			Appointment Services : '.$serviceStr.'<br>
			
			Amount : $'.round($finalAmount,2).'<br>
	        <br>
	        
	        If you are having any issues, please email us at '.ContactEmail.'
	        <br><br>
	        Thanks,<br><br>
	        Team @'.PROJECTNAME.'<br><br>
	         '
	        );
	                  
	    //$emailContent = array('subject' => 'New Password Request','message' => $body);
	    return email($admin_user['email'],'support@griffinapps.com', $emailContent);


	}

	function checkAppointmentForDateAndTimeCheck($postarray,$appuser,$shop_no)
	{
		//echo date('H:i:s')."<br>"; echo date('H:i:s',strtotime($postarray['appointment_time']));exit;
		if(date("Y-m-d") == date('Y-m-d',strtotime($postarray['appointment_date'])))
    	{
    		//today Appointment 

    		if(date('H:i:s') > date('H:i:s',strtotime($postarray['appointment_time'])))
    		{

    			return true;
    		}
    	}

    	if(date("Y-m-d") > date('Y-m-d',strtotime($postarray['appointment_date'])))
    	{
    		//Past Date Appointment
    		return true;
    	}

	    return false;	
	}


	function checkForExtentedTime($appuser,$postarray,$shop_no)
	{
		$return = array();
		$return['shop_appointment_end_time_exceeded'] = 0;
		$return['shop_end_time'] = '';


		$date = $postarray['appointment_date'];
		$time = date('H:i:s',strtotime($postarray['appointment_time']));
		$time2 = date('H:i:s',strtotime($postarray['appointment_end_time']));
		$weekday = date('l',strtotime($date));
		
		$result = $this->db->get_where('shop_availability',array('weekday'=>strtolower($weekday)))->row_array();

		if($result['is_closed'] == 1)
			return true;
		
		$date1 = DateTime::createFromFormat('H:i:s', $time2);
		$date2 = DateTime::createFromFormat('H:i:s', $result['start_time']);
		$date3 = DateTime::createFromFormat('H:i:s', $result['end_time']);
		//pr( $date3); pr( $date1);
		if($date3 < $date1)
		{
			return true;
		}

		return false;

	}



	function getServiceArray($services)
	{
		//pr($services);echo "full service";exit;
		$str = '';
		$count = count($services); 
		$i = 1;
		//pr($services);
		if(!empty($services))
		{
			foreach ($services as $key) {

				$serviceName = $this->db->get_where('services',array('id'=>$key))->row_array();
				//pr($serviceName);
				if($count == $i)
					$str .= $serviceName['name'].'.';
				else
					$str .= $serviceName['name'].',';

				//pr($str);
				
				$i++; 
			}
		}
		//pr($str);exit;
		return $str;
	}


	function cancelPendingAppointment($appuser,$data,$shop_no,$appointment,$payment)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		if($this->db2->update('transactions',array('transaction_id'=>'','status'=>'cancelled','category'=>'appointment','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id'])))
		{
			$this->db->update('appointments',array('payment_status'=>'cancelled','status'=>'Cancel','can_be_canceled'=>1),array('id'=>$appointment['id']));
		
			

			
			
			


			$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$appointment['pet_id']));
			$AppUserDetails = $appuser;

			$admin_user = $this->db->get_where('admin_users')->row_array();
			

			$emailContent = array('subject' => 'Your Appointment Has Been Cancelled Successfully - '.PROJECTNAME.' ','message' =>'
	        	Hello '.$AppUserDetails['username'].',<br><br>
	        
		        The appointment for your pet '.$petDetails['name'].' has been cancelled as follows: 
		        <br><br>
				Date : '.date(get_option('date_format'), strtotime($appointment['appointment_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($appointment['appointment_time'])).'<br>
				Check Out Time : '.date('h:i a', strtotime($appointment['appointment_end_time'])).'<br>
				
				If you are having any issues, please contact:

				<br><br>
				'.$shop_Data['shopname'].'<br>
				'.$shop_Data['phone_number'].'<br>
				'.$shop_Data['address'].'<br>
				'.get_option('shop_email').'<br>
				<br><br>
				
		        
		        Thanks,<br><br> 
	            Team @'.$shop_Data['shopname'].'<br><br> 
				
				'
				);
		                  
		    
		    email($appuser['email'],'support@griffinapps.com', $emailContent);

		    //Cancel Appointment Mail to Groomer

		    $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
			


		    $emailContent = array('subject' =>'Appointment Has Been Cancelled','message' =>'
	        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
	        
		        Appointment has been cancelled by the customer details are as follows 
		        <br><br>
		        Customer Name : '.$appuser['username'].'<br>
		        Pet Name : '.$petDetails['name'].'<br>
				Date : '.date(get_option('date_format'), strtotime($appointment['appointment_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($appointment['appointment_time'])).'<br>
				Check Out Time : '.date('h:i a', strtotime($appointment['appointment_end_time'])).'<br>
				
				
				
		        <br>
		        
		        If you are having any issues, please email us at '.ContactEmail.'
		        <br><br>
		        Thanks,<br><br>
		        Team @'.PROJECTNAME.'<br><br>
		         '
		        );
		                  
		    //$emailContent = array('subject' => 'New Password Request','message' => $body);
		    return email(get_option('email'),'support@griffinapps.com', $emailContent);
		}
	}


	function cancelAppointment($appuser,$data,$shop_no,$appointment,$refundArray,$payment)
	{	
			$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		 $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		//Update Transcation in woodles db and groomer panel
		if($this->db2->update('transactions',array('transaction_id'=>$refundArray['id'],'status'=>'refunded','category'=>'appointment','cancel_by_id'=>$appuser['id'],'cancel_by'=>'customer','shop_id'=>$shop_id,'added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id'])))
		{
			$this->db->update('appointments',array('payment_status'=>'refunded','status'=>'Cancel','can_be_canceled'=>1),array('id'=>$appointment['id']));


		
			//Notify User Email 

			if($appointment['payment_method'] == 'online'){
				$refundString = 'Amount Refunded';
				$refundStringAdmin = 'Amount Refunded';
				$this->db->update('appointments',array('refund_status'=>'success'),array('id'=>$appointment['id']));
			}
			else
			{
				$refundString = 'Amount to be refunded';
				$refundStringAdmin = 'Amount to be refunded';
				$this->db->update('appointments',array('refund_status'=>'pending'),array('id'=>$appointment['id']));
			}


			$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$appointment['pet_id']));
			$AppUserDetails = $appuser;

			$admin_user = $this->db->get_where('admin_users',array('id'=>$appointment['user_id']))->row_array();

			$service = $this->db->get_where('appointment_services',array('appointment_services.appointment_id'=>$appointment['id']))->result_array();
			// if(!empty($service) && $appointment['overlapping_appointment'] == 0)
			// {
				
			// 	$this->cancelUpdateBooking($service,$appointment['user_id'],$appointment['appointment_date'],$appointment,$petDetails['size']);
				
			
			// }	
			

			$emailContent = array('subject' => 'Your Appointment Has Been Cancelled Successfully - '.PROJECTNAME.' ','message' =>'
	        	Hello '.$AppUserDetails['username'].',<br><br>
	        
		        The appointment for your pet '.$petDetails['name'].' has been cancelled as follows: 
		        <br><br>
				Date : '.date(get_option('date_format'), strtotime($appointment['appointment_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($appointment['appointment_time'])).'<br>
				Check Out Time : '.date('h:i a', strtotime($appointment['appointment_end_time'])).'<br>
				'.$refundString.' : $'.round($payment['amount'],2).'<br>
		        <br>
		        If you are having any issues, please contact:

		        <br><br>
				'.$shop_Data['shopname'].'<br>
				'.$shop_Data['phone_number'].'<br>
				'.$shop_Data['address'].'<br>
				'.get_option('shop_email').'<br>
				<br><br>
		        
		        Thanks,
	            <br><br>
                Team @'.$shop_Data['shopname'].'<br><br> 
				
				'
				);
		                  
		    
		    email($appuser['email'],'support@griffinapps.com',$emailContent);

		    //Cancel Appointment Mail to Groomer

		    //$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
			// if(!empty($AppUserDetails['fcm_id'] &&  $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
			// {
			// 	include_once(APPPATH."libraries/PushNotifications.php");
			// 	$FCMPUSH = new PushNotifications();
			// 	if(!empty($petDetails['pet_cover_photo']))
			// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$appointment);
			// 	else
			// 		$dataArray = array('image'=>'','result'=>$appointment);

			// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
	  //           $data = array('body'=>' The appointment for your pet '.$petDetails['name'].' has been cancelled','title'=>'Appointment Cancelled','badge'=>($badge+1),'data'=>$dataArray);
	            
	  //           $notifications = array();
	  //           $notifications['app_user_id'] = $AppUserDetails['id'];
	  //           $notifications['pet_id'] = $petDetails['id'];
	  //           $notifications['title'] = $data['title'];
	  //           $notifications['body'] = $data['body'];
	  //           $notifications['data'] = serialize($dataArray);
	  //           $notifications['added_on'] = date('Y-m-d H:i:s');
	  //           $notifications['shop_id'] = $shop_id;

	  //           $this->db2->insert('notifications',$notifications);
	  //           $FCMPUSH->android($data,$AppUserDetails['fcm_id']);
			// }


		    $emailContent = array('subject' =>'Appointment Has Been Cancelled','message' =>'
	        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
	        
		        Appointment has been cancelled by the customer details are as follows 
		        <br><br>
		        Customer Name : '.$appuser['username'].'<br>
		        Pet Name : '.$petDetails['name'].'<br>
				Date : '.date(get_option('date_format'), strtotime($appointment['appointment_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($appointment['appointment_time'])).'<br>
				Check Out Time : '.date('h:i a', strtotime($appointment['appointment_end_time'])).'<br>
				'.$refundStringAdmin.' : : $'.round($payment['amount'],2).'<br>
				
				
		        <br>
		        
		        If you are having any issues, please email us at '.ContactEmail.'
		        <br><br>
		        Thanks,<br><br>
		        Team @'.PROJECTNAME.'<br><br>
		         '
		        );
		                  
		    //$emailContent = array('subject' => 'New Password Request','message' => $body);
		    return email($admin_user['email'],'support@griffinapps.com',$emailContent);

		}
		


	}


	function cancelPendingBoarding($appuser,$data,$shop_no,$boarding,$payment)
	{
		$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		if($this->db2->update('transactions',array('transaction_id'=>'','status'=>'cancelled','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id'])))
		{
			$this->db->update('boardings',array('payment_status'=>'cancelled','status'=>'Cancel','can_be_canceled'=>1),array('id'=>$boarding['id']));
		
			//Notify User Email 
			//pr($boarding);exit;
			$notificationsTitle = 'Cancelled';
			$emailMsg = 'has been cancelled';
			$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$boarding['pet_id']));
			$AppUserDetails = $appuser;
			$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

			$admin_user = $this->db->get_where('admin_users')->row_array();
			if($boarding['category'] == 'boarding'){
				$endMsgLine = date(get_option('date_format'), strtotime($boarding['end_date'])).'<br>';
			}
			else
				$endMsgLine = date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>';

			



			$emailContent = array('subject' => 'Your '.ucfirst($boarding['category']).' Request Has Been Cancelled Successfully - '.PROJECTNAME.' ','message' =>'
	        	Hello '.$AppUserDetails['username'].',<br><br>
	        
		        The appointment for your pet '.$petDetails['name'].' has been cancelled as follows: 
		        <br><br>
				Check In Date : '.date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($boarding['start_time'])).'<br>
				Check Out Date : '.$endMsgLine.'
				Check Out Time : '.date('h:i a', strtotime($boarding['end_time'])).'<br>	

	        	<br>
	        	If you are having any issues, please contact:

	        	<br><br>
				'.$shop_Data['shopname'].'<br>
				'.$shop_Data['phone_number'].'<br>
				'.$shop_Data['address'].'<br>
				'.get_option('shop_email').'<br>
				<br><br>
			
		        Thanks,
		        <br><br>
                Team @'.$shop_Data['shopname'].'<br><br>
	               
				
				'
				);
		                  
		    
		    email($appuser['email'],'support@griffinapps.com',$emailContent);


		 //    if(!empty($AppUserDetails['fcm_id'] &&  $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
			// {
			// 	include_once(APPPATH."libraries/PushNotifications.php");
			// 	$FCMPUSH = new PushNotifications();
			// 	if(!empty($petDetails['pet_cover_photo']))
			// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$boarding);
			// 	else
			// 		$dataArray = array('image'=>'','result'=>$boarding);

			// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
	  

		    $emailContent = array('subject' =>ucfirst($boarding['category']).' Request Has Been Cancelled','message' =>'
	        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
	        
		        Appointment has been cancelled by the customer details are as follows 
		        <br><br>
		        Customer Name : '.$appuser['username'].'<br>
		        Pet Name : '.$petDetails['name'].'<br>
				Check In Date : '.date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($boarding['start_time'])).'<br>
				Check Out Date : '.$endMsgLine.'<br>
				Check Out Time : '.date('h:i a', strtotime($boarding['end_time'])).'<br>	
				
				
				
				
		        <br>
		        
		        If you are having any issues, please email us at '.ContactEmail.'
		        <br><br>
		        Thanks,<br><br>
		        Team @'.PROJECTNAME.'<br><br>
		         '
		        );
		                  
		    //$emailContent = array('subject' => 'New Password Request','message' => $body);
		    return email(get_option('email'),'support@griffinapps.com',$emailContent);

		}
	
	}


	function cancelBoarding($appuser,$data,$shop_no,$boarding,$refundArray,$payment)
	{
			$shop_Data = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		 $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

		if($this->db2->update('transactions',array('transaction_id'=>$refundArray['id'],'status'=>'refunded','added_on'=>date('Y-m-d H:i:s')),array('id'=>$payment['id'])))
		{
			$this->db->update('boardings',array('payment_status'=>'refunded','status'=>'Cancel','can_be_canceled'=>1,'inventory'=>0),array('id'=>$boarding['id']));
		
			//Notify User Email 
			//pr($boarding);exit;
			$notificationsTitle = 'Cancelled';
			$emailMsg = 'has been cancelled';
			$petDetails = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$boarding['pet_id']));
			$AppUserDetails = $appuser;
			$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);

			if($boarding['overlapping_appointment'] == 0 && $boarding['category'] == 'daycare')
	    	{	

		    		
				$location = $this->db->get_where('daycare_schedule',array('playarea_id'=>$boarding['inventory']))->row_array();
				if(!empty($location))
				{
					//$insertArray['inventory'] = $location['id'];
					$this->db->update('daycare_schedule',array('current_capacity'=>($location['current_capacity']+1)),array('id'=>$location['id']));
				}
						
	    	}

	    	if($boarding['overlapping_appointment'] == 0 && $boarding['category'] == 'boarding')
	    	{	

		    		
				$this->db->update('boarding_schedule',array('is_booked'=>0),array('start<'=>$boarding['end_date'],'end >'=>$boarding['start_date'],'inventory_id'=>$boarding['inventory']));
						
	    	}




			if($boarding['payment_status'] == 'success')
				$this->db->update('boardings',array('payment_status'=>'refunded','status'=>'Cancel','can_be_canceled'=> 1,'refund_status'=>'pending'),array('id'=>$boarding['id']));
			else
				$this->db->update('boardings',array('payment_status'=>'refunded','status'=>'Cancel','can_be_canceled'=> 1,'refund_status'=>'success'),array('id'=>$boarding['id']));
			//$this->db2 = $this->load->database('main', TRUE);
		//Update main database for payment status to be refunded

			$this->db2->update('transactions',array('status'=>'refunded','cancel_by_id'=>$appuser['id'],'cancel_by'=>'customer','added_on'=>date('Y-m-d H:i:s')),array('reference_id'=>$id,'shop_id'=>$shop_id,'category'=>$boarding['category']));
			


			$admin_user = $this->db->get_where('admin_users')->row_array();
			if($boarding['category'] == 'boarding'){
				$endMsgLine = date(get_option('date_format'), strtotime($boarding['end_date'])).'<br>';
			}
			else
				$endMsgLine = date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>';

			if($boarding['payment_method'] == 'online'){
				$refundString = 'Amount Refunded';
				$refundStringAdmin = 'Amount Refunded';
			}
			else
			{
				$refundString = 'Amount to be refunded';
				$refundStringAdmin = 'Amount to be refunded';
			}



			$emailContent = array('subject' => 'Your '.ucfirst($boarding['category']).' Request Has Been Cancelled Successfully - '.PROJECTNAME.' ','message' =>'
	        	Hello '.$AppUserDetails['username'].',<br><br>
	        
		        The appointment for your pet '.$petDetails['name'].' has been cancelled as follows: 
		        <br><br>
				Check In Date : '.date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($boarding['start_time'])).'<br>
				Check Out Date : '.$endMsgLine.'
				Check Out Time : '.date('h:i a', strtotime($boarding['end_time'])).'<br>	
				'.$refundString.' : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($payment['amount']).'<br>

	        	<br>
	        	If you are having any issues, please contact:

	        	<br><br>
				'.$shop_Data['shopname'].'<br>
				'.$shop_Data['phone_number'].'<br>
				'.$shop_Data['address'].'<br>
				'.get_option('shop_email').'<br>
				<br><br>

				Thanks,
		        <br><br>
		       	Team @'.$shop_Data['shopname'].'<br><br>

				'
				);
		                  
		    
		    email($appuser['email'],'support@griffinapps.com',$emailContent);


		 //    if(!empty($AppUserDetails['fcm_id'] &&  $this->Management_model->checkIfAppUserBelongToSameShop($shop_id ,$AppUserDetails['fcm_id'])))
			// {
			// 	include_once(APPPATH."libraries/PushNotifications.php");
			// 	$FCMPUSH = new PushNotifications();
			// 	if(!empty($petDetails['pet_cover_photo']))
			// 		$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$boarding);
			// 	else
			// 		$dataArray = array('image'=>'','result'=>$boarding);

			// 	$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails['id'],$shop_id);
	  //           $data = array('body'=>' Your '.$boarding['category'].' request for your pet '.$petDetails['name'].' '.$emailMsg,'title'=>ucfirst($boarding['category']).' Request '.$notificationsTitle,'badge'=>($badge+1),'data'=>$dataArray);
	            
	  //           $notifications = array();
	  //           $notifications['app_user_id'] = $AppUserDetails['id'];
	  //           $notifications['pet_id'] = $petDetails['id'];
	  //           $notifications['title'] = $data['title'];
	  //           $notifications['body'] = $data['body'];
	  //           $notifications['data'] = serialize($dataArray);
	  //           $notifications['added_on'] = date('Y-m-d H:i:s');
	  //           $notifications['shop_id'] = $shop_id;

	  //           $this->db2->insert('notifications',$notifications);
	  //           $FCMPUSH->android($data,$AppUserDetails['fcm_id']);
			// }

		    //Cancel Appointment Mail to Groomer

		    $emailContent = array('subject' =>ucfirst($boarding['category']).' Request Has Been Cancelled','message' =>'
	        	Hello '.$admin_user['first_name'].' '.$admin_user['last_name'].',<br><br>
	        
		        Appointment has been cancelled by the customer details are as follows 
		        <br><br>
		        Customer Name : '.$appuser['username'].'<br>
		        Pet Name : '.$petDetails['name'].'<br>
				Check In Date : '.date(get_option('date_format'), strtotime($boarding['start_date'])).'<br>
				Check In Time : '.date('h:i a', strtotime($boarding['start_time'])).'<br>
				Check Out Date : '.$endMsgLine.'<br>
				Check Out Time : '.date('h:i a', strtotime($boarding['end_time'])).'<br>	
				'.$refundStringAdmin.'  : '.get_option('currency_symbol').' '.$this->localization->currencyFormat($payment['amount']).'<br>
				
				
				
		        <br>
		        
		        If you are having any issues, please email us at '.ContactEmail.'
		        <br><br>
		        Thanks,<br><br>
		        Team @'.PROJECTNAME.'<br><br>
		         '
		        );
		                  
		    //$emailContent = array('subject' => 'New Password Request','message' => $body);
		    return email(get_option('email'),'support@griffinapps.com',$emailContent);

		}
	}

	function getAppointmentbyId($id)
	{
		return $this->db->get_where('appointments',array('id'=>$id))->row_array();
	}

	function getBoardingbyId($id)
	{
		return $this->db->get_where('boardings',array('id'=>$id))->row_array();
	}

	function getPaymentDetailByAppointmentId($id,$shop_id,$appuser,$category)
	{
		return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$shop_id,'app_user_id'=>$appuser['id'],'transcation_type'=>'payment','is_add_on_payment'=>0))->row_array();
	}


	function checkForCardDuplicate($token,$user_id)
	{
		if($this->db2->get_where('app_users_cards',array('card_fingerprint'=>$token,'app_user_id'=>$user_id))->num_rows() > 0)
			return true;
		else
			return false;
	}

	function checkEmailForEdit($appuser,$email)
	{
		$result = $this->db2->get_where('app_users',array('id !='=>$appuser['id'],'email'=>$email))->result_array();
		if(empty($result))
			return true;
		else
			return false;
	}

	function editProfile($appuser,$postarray,$shop_no)
	{
		$insert =  array();
		$insert['username'] = $postarray['username'];
		$insert['email'] = $postarray['email'];
        // $insert['city']= $postarray['city'];
        // $insert['state']=$postarray['state'];
        
        $insert['dob'] = date('Y-m-d',strtotime($postarray['dob']));
        $insert['gender']= $postarray['gender'];
        //$insert['address']= $postarray['address'];
        $insert['phone']= $postarray['phone'];

        if(isset($postarray['emergency_contact_name']) && $postarray['emergency_contact_name'] != 'null' )
        	$insert['emergency_contact_name'] = $postarray['emergency_contact_name'];
        else
        	$insert['emergency_contact_name'] = '';

        if(isset($postarray['emergency_contact_phone_number']) && $postarray['emergency_contact_phone_number'] != 'null')
        	$insert['emergency_contact_phone_number'] = $postarray['emergency_contact_phone_number'];
        else
        	$insert['emergency_contact_phone_number'] = '';


        if(isset($postarray['veterinarian']) && $postarray['veterinarian'] != 'null')
        	$insert['veterinarian'] = $postarray['veterinarian'];
        else
        	$insert['veterinarian'] = '';


        if(isset($postarray['state']) && $postarray['state'] != 'null' )
        	$insert['state'] = $postarray['state'];
        else
        	$insert['state'] = '';

        if(isset($postarray['city']) && $postarray['state'] != 'null' )
        	$insert['city'] = $postarray['city'];
        else
        	$insert['city'] = '';

        if(isset($postarray['address']) && $postarray['address'] != 'null' )
        	$insert['address'] = $postarray['address'];
        else
        	$insert['address'] = '';

        if(isset($postarray['postal_code']) && $postarray['postal_code'] != 'null' )
        	$insert['postal_code'] = $postarray['postal_code'];
        else
        	$insert['postal_code'] = '';


        if(isset($postarray['sign_upload']) && $postarray['sign_upload'] != 'null' )
        	$insert['sign_upload'] = $postarray['sign_upload'];
        else
        	$insert['sign_upload'] = '';

        $insert['number_type'] = $postarray['number_type'];
        $insert['is_deposited'] = $postarray['is_deposited'];
        $insert['additional_Phone'] = $postarray['additional_Phone'];

        //$insert['added']=date('Y-m-d H:i:s');

        if(isset($postarray['is_deleted']) && $postarray['is_deleted'] == 1)
        {
        	if(!empty($appuser['fb_id']))
	        	$appuser['last_login_by'] = 'facebook';
	        else
	        	$appuser['last_login_by'] = 'normal';
	        
        	$insert['profile'] = '';
        }

        if(!empty($postarray['profile']))
        {
        	$insert['last_login_by'] = 'normal';
        	$insert['profile'] = $postarray['profile']; 
        }

        //  pr($insert);exit;
       
        return $this->db2->update('app_users',$insert,array('id'=>$appuser['id']));

	}


	function checkOldPassword($old_password,$appuser)
	{
		//verify old password
		$old_password = (string)$old_password; //Convert it into string if it has leading zero;
		if(password_verify($old_password,$appuser['password'])){				
			return true;
		}
		else
		{
			return false;
		}	
	}

	function changePassword($appuser,$postarray,$shop_no)
	{
		$options = [
            'cost' => 5,
        ];
		$newPassword =  password_hash($postarray['new_password'], PASSWORD_BCRYPT, $options);
		return $this->db2->update('app_users',array('password'=>$newPassword),array('id'=>$appuser['id']));
	}

	function getAllUserAndShopDetailBeforeAppointment($appuser,$shop_no)
	{
		$return = array();
		$services = $this->GlobalApp_model->getAllServices();
		$tempServices = array();
		
		if(get_option('appointment_switch') == 1)
		{
			if(!empty($services))
			{
				foreach ($services as $key => $value) {
					$temp = array();
					$temp = $value;
					$temp['is_boarding'] = FALSE;
					array_push($tempServices, $temp);
				}
			}
		}	

		$data = array();

		$return['boardingSettings'] = $this->Management_model->getBoardingSettings();
        $return['boardingPrice']=$this->Management_model->getBoardingPrice();
        //	$return['boardingInventory'] = $this->Management_model->getBoardingInventory();
        
        $return['emergencyContactExist'] = 0; 
        if (empty($appuser['emergency_contact_name']) || empty($appuser['emergency_contact_phone_number'])) {
            $return['emergencyContactExist'] = 1;      
        }



        $tempArray = array();
        if(!empty($return['boardingSettings']) && $this->checkDayCareProfileComplete() && get_option('daycare_switch') == 1)
        {
        	
	        	$temp = array();
	        	$temp['id'] = 0;
	        	$temp['name'] = $return['boardingSettings']['dayCareTitle'];
	        	$temp['type'] = 'Day Care';
	        	$temp['image'] = $return['boardingSettings']['dayCareImage'];
	        	$temp['cost'] =  $return['boardingSettings']['normalDayCarePricing'];
	        	$temp['time_estimate'] = 0;
	        	$temp['is_deleted'] = 0;
	        	$temp['description'] = $return['boardingSettings']['dayCareDescription'];
	        	$temp['is_boarding'] = TRUE;
	        	array_push($tempServices, $temp);
	        	array_push($tempArray, $temp);
	        
	   	}

	   	if(!empty($return['boardingSettings']) && $this->checkBoardingProfileIsComplete() && get_option('boarding_switch') == 1)     	
        {	
        		
	        	$temp = array();
	        	$temp['id'] = 0;
	        	$temp['name'] = $return['boardingSettings']['boardingTitle'];
	        	$temp['type'] = 'Boarding';
	        	$temp['image'] = $return['boardingSettings']['boardingImage'];
	        	$temp['cost'] =  0;
	        	$temp['time_estimate'] = 0;
	        	$temp['is_deleted'] = 0;
	        	$temp['description'] = $return['boardingSettings']['boardingDescription'];
	        	$temp['is_boarding'] = TRUE;
	        	array_push($tempServices, $temp);
	        	array_push($tempArray, $temp);
	        
        }



        //$return['userDiscount']  = array('percentage'=>10); 
        $return['userDiscount']  = $this->getUserDiscount($appuser['id']);


        $return['dayCareBoarding']  = $tempArray;  
		$return['services'] = $tempServices;
		$return['commission'] = $this->getAllSuperAdminCommission();
		$return['shopInfo'] = $this->GlobalApp_model->getBrandingInfo();

		if(!empty($return['shopInfo']['company_logo']))
			$return['shopImageName'] = $this->config->item('img_url').$return['shopInfo']['company_logo'].'/shop/500';;
			
		$return['cards'] = $this->getAllCards($appuser['id']);
		$return['pets'] = $this->getAllPetsForAppointmentAddByAppointmentDate($appuser['id'],$shop_no);
		$return['appointments'] = $this->getAllAppointments($appuser['id'],$shop_no,$appuser,0);
		$return['stripePercentage'] = STRIPECOMMISSIONPERCENTAGE;
		$return['stripeCentTobeAdded'] = STRIPECOMMISSIONADDITONALAMOUNT;
		$return['imageSharingPathForGalleryImage'] = 'https://storage.googleapis.com/'.BUCKETNAME.'/'.WatermarkImages;
		$return['shopWorkingDays'] = $this->Management_model->getShopWorkingDaysForDatePicker();
		$return['shop_availability'] = $this->Management_model->getshopAvailabilty();
		$return['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
		return $return;

	}


	function getUserDiscount($app_user_id = 0){
		$todaysDate = date("Y-m-d");
		$this->db->select();
		$this->db->from('discount_users dusers');
		$this->db->join('discounts d',"d.id = dusers.discount_id");
		//$this->db->where(array('dusers.app_user_id' => $appuser['id']));
		$this->db->where(array('dusers.app_user_id' => $app_user_id,'d.is_deleted' => '0','dusers.is_active' => '1'));
		$this->db->where(array('d.start_date_time <='=> $todaysDate,'d.end_date_time >='=> $todaysDate));
		//$this->db->where('d.start_date_time >=',$todaysDate);
		//$this->db->where('d.start_date_time <='=> $todaysDate,'d.end_date_time >='=> $todaysDate);
		$this->db->limit(1);
		$result = $this->db->get()->result_array();
		//return $result;

		if(count($result) == 0){
			$this->db->select();
			$this->db->from('discount_users dusers');
			$this->db->join('discounts d',"d.id = dusers.discount_id");
			//$this->db->where(array('dusers.app_user_id' => $appuser['id']));
			$this->db->where(array('dusers.app_user_id' => $app_user_id,'d.is_deleted' => '0'));
			//$this->db->where('d.start_date_time >=',$todaysDate);
			$this->db->where(array('d.start_date_time <='=> $todaysDate,'d.end_date_time >='=> $todaysDate));
			$result1 = $this->db->get()->result_array();
			return $result1;
		}else{
			return $result;
		}
	}


	function getUserDetails($appuser,$shop_no)
	{
		//return $this->db2->get_where('app_users',array('id'=>$appuser['id']))->row_array();
		$data = $this->db2->get_where('app_users',array('id'=>$appuser['id']))->row_array();
		$AppVersion = $this->db2->get_where('app_version')->row_array();
		$data = array_merge($data,$AppVersion);
		$petData = $this->db2->get_where('pets',array('app_user_id'=>$appuser['id']))->row_array();
		if (!empty($petData)) {
			$data['new_user'] = 'false';  
		}else{
			$data['new_user'] = 'true';
		}
		return $data;
	}

	function getAllSuperAdminCommission()
	{
		return $this->db2->get_where('commissions')->result_array();
	}


	function removePic($appuser,$postarray,$shop_no)
	{
		if($postarray['type'] == 'pet')
		{
			//Removing Pet Images
			$pet = $this->getPetDetails($appuser['id'],$shop_no,$postarray);
			if($pet['pet_cover_photo']){

                // if(file_exists(FCPATH.UPLOAD_PETS.$pet['pet_cover_photo']))
                //     unlink(FCPATH.UPLOAD_PETS.$pet['pet_cover_photo']);
                            
                // if(file_exists(FCPATH.UPLOAD_PETS.'large'.'/'.$pet['pet_cover_photo']))
                //     unlink(FCPATH.UPLOAD_PETS.'large'.'/'.$pet['pet_cover_photo']);
                
                // if(file_exists(FCPATH.UPLOAD_PETS.'medium'.'/'.$pet['pet_cover_photo']))
                //     unlink(FCPATH.UPLOAD_PETS.'medium'.'/'.$pet['pet_cover_photo']);

                // if(file_exists(FCPATH.UPLOAD_PETS.'small'.'/'.$pet['pet_cover_photo']))
                //     unlink(FCPATH.UPLOAD_PETS.'small'.'/'.$pet['pet_cover_photo']);

                if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']))
        			unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']);
                
                return $this->db2->update('pets',array('pet_cover_photo'=>''),array('id'=>$postarray['id']));
            }
		}
		else
		{
			//Removing Customer Image
			if(!empty($appuser['profile'])){

            	//Check if user has already added profile image,then delete

	            // if(file_exists(FCPATH.UPLOAD_PETS.$appuser['profile']))
	            //     unlink(FCPATH.UPLOAD_PETS.$appuser['profile']);
	                        
	            // if(file_exists(FCPATH.UPLOAD_PETS.'large'.'/'.$appuser['profile']))
	            //     unlink(FCPATH.UPLOAD_PETS.'large'.'/'.$appuser['profile']);
	            
	            // if(file_exists(FCPATH.UPLOAD_PETS.'medium'.'/'.$appuser['profile']))
	            //     unlink(FCPATH.UPLOAD_PETS.'medium'.'/'.$appuser['profile']);

	            // if(file_exists(FCPATH.UPLOAD_PETS.'small'.'/'.$appuser['profile']))
	            //     unlink(FCPATH.UPLOAD_PETS.'small'.'/'.$appuser['profile']);

	            if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']))
        				unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']);

            	return $this->db2->update('app_users',array('profile'=>''),array('id'=>$appuser['id']));
            }
		}

		return false;

	}

	function getAllBreeds()
	{
		$return = array();
        $return['all_breeds'] = $this->db2->get_where('breeds')->result_array();
        $return['cat_breeds'] = $this->db2->get_where('breeds',array('is_deleted'=>0,'type'=>'Cat'))->result_array();
        $return['dog_breeds'] = $this->db2->get_where('breeds',array('is_deleted'=>0,'type'=>'Dog'))->result_array();
        $return['petSize'] = $this->db->order_by('id','ASC')->get_where('pet_size_weight')->result_array();

        foreach ($return['petSize'] as $key => $value) {
           if($key == 0){
               $return['petSize'][$key]['weight_text'] = "( up to ".$value['size_weight']." Lbs )";
               $return['petSize'][$key]['size_name'] = ucwords($value['size_name']);

           }else if( count($return['petSize']) == $key+1 ){
               $return['petSize'][$key]['weight_text'] = '( '.$return['petSize'][$key-1]['size_weight']." Lbs & greater )";
               $return['petSize'][$key]['size_name'] = strtoupper($value['size_name']);
           }else{
               $return['petSize'][$key]['weight_text'] = '( '.$return['petSize'][$key-1]['size_weight']." Lbs to ".$value['size_weight']." Lbs )";
               if ($value['size_name'] == 'xl') {
               		$return['petSize'][$key]['size_name'] = strtoupper($value['size_name']);
               }else{
               		$return['petSize'][$key]['size_name'] = ucwords($value['size_name']);
               }
           }
       }
       
       
        return $return;
	}

	function getAppointmentGallery($appuser,$shop_no,$data)
	{
		return $this->db->get_where('pet_appointment_images',array('reference_id'=>$data['id'],'category'=>$data['category']))->result_array();
	}

	function getAllNotification($appuser,$shop_no,$pageData)
	{
		$return = array();
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		$limit = 10;
		
		if($pageData == 0)
		{
			$start = 0;
		}
		else
		{
			$start = $pageData*10;	
		}


		$this->db2->limit($limit,$start);
		$result = $this->db2->order_by('id','DESC')->get_where('notifications',array('app_user_id'=>$appuser['id'],'shop_id'=>$shop_id))->result_array();

		$shopImage = $this->getShopImage();

		if(!empty($result)){
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				$temp['data'] = unserialize($value['data']);
				if($value['pet_id'] > 0){
					$temp['image'] = $this->getPetImageById($value['pet_id']);
					$temp['data']['image'] = $this->getPetImageById($value['pet_id']);
				}
				else
				{
					$temp['image'] = $shopImage;
					$temp['data']['image'] = $shopImage;
				}	
				

				array_push($return,$temp);
			}
		}

		return $return;	
	}




	function updateAllNotifcation($appuser,$shop_no)
	{
		$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
		
		return $this->db2->update('notifications',array('is_read'=>1),array('app_user_id'=>$appuser['id'],'shop_id'=>$shop_id));
	}


	function getPetImageById($id)
	{

		$result = $this->db2->get_where('pets',array('id'=>$id))->row_array();
		if(!empty($result['pet_cover_photo'])){
			// return base_url().UPLOAD_PETS.'/medium/'.$result['pet_cover_photo'];
			return $this->config->item('img_url').$result['pet_cover_photo'].'/pet/500';
		}
		else
			return '';
	}

	function getOnlyImageName($id)
	{
		$result = $this->db2->get_where('pets',array('id'=>$id))->row_array();
		if(!empty($result['pet_cover_photo']))
			return $result['pet_cover_photo'];
		else
			return '';
	}

	function getOnlyShopImage()
	{
		$result = $this->GlobalApp_model->getBrandingInfo();
		if(!empty($result['company_logo']))
			return $result['company_logo'];
		else
			return '';
	}


	function getShopImage()
	{
		$result = $this->GlobalApp_model->getBrandingInfo();
		if(!empty($result['company_logo'])){
			// return base_url().UPLOAD_LOGO.'/medium/'.$result['company_logo'];
			return $this->config->item('img_url').$result['company_logo'].'/shop/500';
		}
		else
			return '';
	}

	function getAllNotificationCount($appuser,$shop_id){
		$this->db2->select('count(id) as Count');
		$result = $this->db2->get_where('notifications',array('app_user_id'=>$appuser['id'],'shop_id'=>$shop_id,'is_read'=>0))->row_array();
		return $result['Count'];
	}


	function checkPetHasAppointmentBoarding($id){
		$check = 0;
		//$this->db2 = $this->load->database('main', TRUE);
		$shops = $this->db2->get_where('shops',array('is_deleted'=>0,'is_blocked'=>0))->result_array();
        if(!empty($shops))
        {
           
            foreach ($shops as $key => $value) 
            {
                if($this->db->db_select($value['dbname']))
                {
                	$this->db->where_not_in('status',array('Cancel','Complete','Checkout'));
                	$appointment = $this->db->get_where('appointments',array('pet_id'=>$id))->result_array();
                	if(!empty($appointment))
                		$check++; 


                	$this->db->where_not_in('status',array('Cancel','Complete','Checkout'));
                	$boardings = $this->db->get_where('boardings',array('pet_id'=>$id))->result_array();
                	if(!empty($boardings))
                		$check++; 
                }
            }
        }

        if($check > 0)
        	return false;

        return true;	        	
	}


	function checkBoardingProfileIsComplete()
	{
		//checking if boarding/day care profile is complete
		$data = array();
		$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
    	//$data['boardingPrice']=$this->Management_model->getBoardingPrice();
    	$data['boardingInventory'] = $this->Management_model->getBoardingInventory();

    	$this->db->where('boarding_schedule.start >=','CURDATE()',FALSE);
    	$data['schedule'] = $this->db->get_where('boarding_schedule')->result_array();

    	//pr($data['boardingSettings']);exit;
    	if(empty($data['boardingInventory']))
    		return false;

    	if($data['boardingSettings']['catBoardingPricing'] == 0 && $data['boardingSettings']['dogBoardingPricing'] == 0)
    		return false;

    	if (empty($data['schedule'])) {
    		return false;
    	}


		return true;

	}

	function checkDayCareProfileComplete()
	{
		$data = array();
		$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
    	//$data['boardingPrice']=$this->Management_model->getBoardingPrice();
    	$data['playareas'] = $this->db->get_where('playareas',array('is_deleted'=>0))->result_array();

    	$this->db->where('daycare_schedule.date >=','CURDATE()',FALSE);
    	$data['schedule'] = $this->db->get_where('daycare_schedule')->result_array();

    	if(empty($data['playareas']))
    		return false;

    	if($data['boardingSettings']['normalDayCarePricing'] == 0)
    		return false;

    	if (empty($data['schedule'])) {
    		return false;
    	}


		return true;
	}

	function checkIFShopIsBlocked($shopno)
	{
		//$this->db2 = $this->load->database('main', TRUE);

		$shop = $this->db2->get_where('shops',array('phone_number'=>$shopno))->row_array();

		if($shop['is_blocked'] == 1)
			return true;
		else
			return false;	
	
	}

	function getShopDetailsForAjax($shopno)
	{
		return $this->db2->get_where('shops',array('phone_number'=>$shopno))->row_array();
	}

	function checkForPendingAppointment($appuser,$cardDetail)
	{
		$check = 0;
		//$this->db2 = $this->load->database('main', TRUE);
		$shops = $this->db2->get_where('shops',array('is_deleted'=>0,'is_blocked'=>0))->result_array();
        if(!empty($shops))
        {
           
            foreach ($shops as $key => $value) 
            {
                if($this->db->db_select($value['dbname']))
                {
                	$this->db->where_in('status',array('Pending'));
                	$appointment = $this->db->get_where('appointments',array('app_user_id'=>$appuser['id']))->result_array();
                	if(!empty($appointment))
                		$check++; 


                	$this->db->where_in('status',array('Pending'));
                	$boardings = $this->db->get_where('boardings',array('app_user_id'=>$appuser['id']))->result_array();
                	if(!empty($boardings))
                		$check++; 
                }
            }
        }


        $appUserCards = $this->getAllCards($appuser['id']);



        if($check > 0 && count($appUserCards) == 1)
        	return true;

        return false;
	}

	function checkUserBelongsToShop($shop_no,$appuser)
	{
		//$this->db2 = $this->load->database('main', TRUE);
		$shop = $this->db2->get_where('shops',array('phone_number'=>$shop_no))->row_array();

		$user_shop = $this->db2->get_where('app_users_shops',array('shop_id'=>$shop['id'],'app_user_id'=>$appuser['id']))->row_array();
		
		if(!empty($user_shop))
			return false;
		else
			return true;

	}

	function getGroomersByService($services,$pet_id,$appuser,$shop_no)
	{
		$return = array();
		// $pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		// $array = explode(',', $services);
	 //   	$time_estimate = 0;
	 //   	foreach ($array as $key ) {
	 //        $array = $this->db->get_where('services',array('id'=>$key))->row_array();
	 //        $time_estimate += $array[$pet['size'].'_time_estimate'];
	 //    }
		// $result = $this->db->query("SELECT admin_users.* , (SELECT appointments.appointment_date FROM `appointments`where appointments.user_id = admin_users.id and appointments.appointment_date >= CURRENT_DATE ORDER by appointments.appointment_date limit 1 ) as a,(select groomer_schedule.date from groomer_schedule where groomer_schedule.date > CURRENT_DATE and groomer_schedule.user_id = admin_users.id order by groomer_schedule.date limit 1) as b FROM admin_users join groomer_schedule on admin_users.id = groomer_schedule.user_id where groomer_schedule.date >= CURRENT_DATE and groomer_schedule.end > CURRENT_TIME GROUP by admin_users.id order by a,b")->result_array();

		$result = $this->db->query("select admin_users.* , sum(TIME_TO_SEC(TIMEDIFF(groomer_schedule.end,groomer_schedule.start))) - (select IFNULL(sum(TIME_TO_SEC(TIMEDIFF(appointments.appointment_end_time,appointments.appointment_time))),0) from appointments where appointments.user_id = admin_users.id and status != 'Cancel') as booking_time from admin_users join groomer_schedule on admin_users.id = groomer_schedule.user_id where groomer_schedule.date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 30 DAY) group by admin_users.id")->result_array();


		//echo $this->db->last_query();exit;
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
				//$temp['profile_image'] = 
				//Appointment logic for free groomer

				array_push($return,$temp);
			}
			return $return;
		}
		else
			return false;

		

	}

	function getCalendarDatesForDayCare($services,$pet_id,$appuser,$shop_no)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		//var_dump($services);
		
	    // $this->db->where('groomer_schedule.end >=','CURTIME()',FALSE);
	    $this->db->where('daycare_schedule.date >=','CURDATE()',FALSE);
	    $this->db->select('sum(daycare_schedule.orginal_capacity) as orginal_capacity,sum(daycare_schedule.current_capacity) as current_capacity,daycare_schedule.start,daycare_schedule.end,daycare_schedule.date');
		$this->db->group_by('date(daycare_schedule.date)');
	    $result = $this->db->get_where('daycare_schedule')->result_array();
	    if(!empty($result))
		{
			foreach ($result as $key => $value) {
			
				
				//echo (strtotime($value['end']) - strtotime($value['start']));exit;
				$temp = array();
				$temp = $value;
				
				$Appointments = $this->db->get_where('boardings',array('start_date'=>$value['date'],'status !='=>'Cancel'))->result_array();
				//$temp['booking_time'] = 0;
				if(!empty($Appointments))
				{
					if($value['current_capacity'] <= 0 )
						$temp['booking_status'] = 'red';	
					else
						$temp['booking_status'] = 'yellow';
				}
				else
				{
					$temp['booking_status'] = 'green';
				}
				
				array_push($return,$temp);



			}
			return $return;		

		}
		return false;
	}

	function getCalendarDatesByGroomerIdAndService($services,$pet_id,$appuser,$shop_no,$user_id)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		//var_dump($services);
		

		$array = explode(',', $services);
	   	$time_estimate = 0;
	   	//pr($array);exit;
	   	foreach ($array as $key ) {
	        $array = $this->db->get_where('services',array('id'=>$key))->row_array();
	        $time_estimate += $array[$pet['size'].'_time_estimate'];
	    }


	    // $this->db->where('groomer_schedule.end >=','CURTIME()',FALSE);
	    // $this->db->where('groomer_schedule.date >=','CURDATE()',FALSE);
	    // $this->db->order_by('groomer_schedule.date');
	    // $result = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id))->result_array();

	    $result = $this->db->query("select groomer_schedule.*,sum(TIME_TO_SEC(TIMEDIFF(groomer_schedule.end,groomer_schedule.start))) as working_hrs, (select IFNULL(sum(TIME_TO_SEC(TIMEDIFF(appointments.appointment_end_time,appointments.appointment_time))),0) from appointments where appointments.user_id = '".$user_id."' and status != 'Cancel' and appointment_date = groomer_schedule.date) as booking_time from groomer_schedule where groomer_schedule.date >= CURDATE() and user_id = '".$user_id."' group by groomer_schedule.date")->result_array();

	    //echo $this->db->last_query();
	    //pr($result);
	    if(!empty($result))
		{
			foreach ($result as $key => $value) {
			
				//$maxPetMultiplier = intval($this->getMaxMulitplierByUserIdAndServices($services,$user_id));
				//echo $maxPetMultiplier."<br>";
				// if($maxPetMultiplier > 1)
				// 	$timeAvailable = (((strtotime($value['end']) - strtotime($value['start']))/60)*$maxPetMultiplier);
				// else
				// 	$timeAvailable = ((strtotime($value['end']) - strtotime($value['start']))/60);
				//echo (strtotime($value['end']) - strtotime($value['start']));exit;
				$temp = array();
				$temp = $value;
				// $temp['working_hrs'] = $timeAvailable; 
				// $Appointments = $this->db->get_where('appointments',array('appointment_date'=>$value['date'],'user_id'=>$user_id,'status !='=>'Cancel'))->result_array();
				// $temp['booking_time'] = 0;
				// if(!empty($Appointments))
				// {
				// 	$availableTime = $timeAvailable;
				// 	foreach ($Appointments as $k => $v) {
				// 		$availableTime -= ((strtotime($v['appointment_end_time']) - strtotime($v['appointment_time'])) / 60);
				// 		$temp['booking_time'] += ((strtotime($v['appointment_end_time']) - strtotime($v['appointment_time'])) / 60);
				// 	}
				// 	$temp['available_time'] = 0;
				// 	if($availableTime > $time_estimate){
				// 		$temp['booking_status'] = 'yellow';
				// 		$temp['available_time'] = $availableTime;
						
				// 	}	
				// 	else
				// 		$temp['booking_status'] = 'red';

						
				// }
				// else
				// {
				// 	$temp['booking_status'] = 'green';
				// 	$temp['available_time'] = $timeAvailable;
				// }

				if($value['available_time'] == $value['booking_time'])
				{
					$temp['booking_status'] = 'red';
				}
				else if($value['booking_time'] == 0)
				{
					$temp['booking_status'] = 'green';
				}
				else
				{
					$temp['booking_status'] = 'yellow';
				}	
				
				array_push($return,$temp);



			}
			return $return;		

		}
		return false;	


	}


	function getMaxMulitplierByUserIdAndServices($services,$user_id)
	{
		$this->db->select('max(groomer_services.pet_multiplier) as pet_max');
		//$this->db->where_in('service_id',$services);
		$result = $this->db->get_where('groomer_services',array('user_id'=>$user_id))->row_array();
		return $result['pet_max'];
	}

	function getWorkingTimeAndSlotForAppointment($date,$services,$pet_id,$appuser,$shop_no,$user_id)
	{
		$return = array();
		$return['available_booking_time'] = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		//var_dump($services);
		

		$array = explode(',', $services);
	   	$time_estimate = 0;
	   	//pr($array);exit;
	   	foreach ($array as $key ) {
	        $service = $this->db->get_where('services',array('id'=>$key))->row_array();
	        //Consider Single Order

	        $time = $service[$pet['size'].'_time_estimate'];

	        //$this->db->get_where('')

	        $time_estimate += $service[$pet['size'].'_time_estimate'];
	    }

	    //Consider Single Order

	    // foreach ($array as $key ) {
	    //     $array = $this->db->get_where('services',array('id'=>$key))->row_array();
	        

	    //     $time = $array[$pet['size'].'_time_estimate'];
	    //     // $this->db->get_where(''
	    //     // if()
	    //     //$this->db->get_where('')

	        
	    // }

	    // $this->db->select('groomer_bookings.*'); 

	    // if(count($array) > 1)
	    // {
	    // 	$this->db->where_in('service_id',$array);
	    // }
	    // else
	    // 	$this->db->where_in('service_id',array($services));

	    // $this->db->order_by('start');

	    // $return['available_time'] = $this->db->get_where('groomer_bookings',array('groomer_bookings.date'=>$date,'user_id'=>$user_id,'groomer_bookings.pet_multiplier > '=>0))->result_array();
	    
	    // $this->db->where(array('date(groomer_schedule.date)'=>$date));
	    // $this->db->order_by('groomer_schedule.date');
	    $return['working_time'] = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date))->row_array();

	    $start = $return['working_time']['start'];
	    $end = $return['working_time']['end'];

	    $diff = $return['working_time']['end'] - $return['working_time']['start'];

	    $datetime1 = new DateTime($start);
		$datetime2 = new DateTime($end);
		$interval = $datetime1->diff($datetime2);
		// echo $interval->format('%h')." Hours ".$interval->format('%i')." Minutes";
		
		$h = $interval->format('%h');
		
		$m = $interval->format('%i');

		if($m > 0)
			$h = $h+1;

		// echo $h;
		// echo $m;exit;

		$availableTime = $this->getAvailableTimeForBookingForSlotSelection($temp['start'],$temp['end'],$date,$user_id,$start,$end,
		    		$array,$pet['size'],$time_estimate);
		//pr($availableTime);exit;

		$availableArray = array();

		if(!empty($availableTime))
		{
			foreach ($availableTime as $key => $value) {
				if(!empty($value['available_time']))
				{
					foreach ($value['available_time'] as $v) {
						# code...
						$temp = array();
						$temp = $v;
						$temp['id'] = $value['id'];
						array_push($availableArray,$temp);
					}
					
				
				}

			}
		}

		//pr($availableArray);exit;
			
			//pr($availableArray);exit;
		usort( $availableArray, array($this,'myCompare') );
		// foreach($availableArray as $i=>$row){
	 
		//pr($availableArray);
		$availableArray2 = $availableArray;
		$n = 0; $len = count($availableArray);
		for ($i = 1; $i < $len; ++$i)
		{
	        if (strtotime($availableArray[$i]['start']) > strtotime($availableArray[$n]['end']))
	                $n = $i;
	        else
	        {
	                if (strtotime($availableArray[$n]['end']) < strtotime($availableArray[$i]['end']))
	                    $availableArray[$n]['end'] = $availableArray[$i]['end'];
	                unset($availableArray[$i]);
	        }
		}

	    //$result[$x]['merged_ids']=implode(', ',$result[$x]['merged_ids']);  // convert final merged_ids subarray to csv string
    	// pr($availableArray);
    	// exit;

		//exit;

		// if(!empty())
		// {

		// }

		$return['available_booking_time'] = array();
	    if($start == $end)
	    {
	    	$return['available_booking_time'] = array();
	    }
	    else
	    {
	    	for($i = 0;$i<$h;$i++)
	    	{
		    	$temp = array();
		    	if($i == 0)
					$temp['start'] = date('Y-m-d H:i:s',strtotime($start));
		    	else
		    		$temp['start'] = $endTime;


		    	$temp['blocker_start'] = '';
		    	$temp['blocker_end'] = '';

		    	$temp['hard_blocker_start'] = '';
		    	$temp['hard_blocker_end'] = '';

		    	$temp['lunch_time_start'] = '';
		    	$temp['lunch_time_end'] = '';
		    	if(!empty($return['working_time']['blocker_start'])){
			    	$temp['blocker_start'] = $date .' '.$return['working_time']['blocker_start'];
		    		$temp['blocker_end'] = $date .' '.$return['working_time']['blocker_end'];
		    	}

		    	if(!empty($return['working_time']['hard_blocker_start'])){
			    	$temp['hard_blocker_start'] = $date .' '.$return['working_time']['hard_blocker_start'];
		    		$temp['hard_blocker_end'] = $date .' '.$return['working_time']['hard_blocker_end'];
		    	}

		    	if(!empty($return['working_time']['lunch_time_start'])){
			    	$temp['lunch_time_start'] = $date .' '.$return['working_time']['lunch_time_start'];
		    		$temp['lunch_time_end'] = $date .' '.$return['working_time']['lunch_time_end'];
		    	}

		    	$temp['end'] = date('Y-m-d H:i:s',strtotime($temp['start'].' +60 minutes'));
		    	$endTime =  date('Y-m-d H:i:s',strtotime($temp['start'].' +60 minutes'));
		    	$temp['available_time'] = array();

		    	foreach ($availableArray as $key => $value) {
		    		$b = array();
		    		if($value['start'] <= $temp['start'] && $temp['end'] <= $value['end']){
		    			$b['start'] = $temp['start'];
		    			$b['end'] = $temp['end'];
		    			if($b['start'] < $b['end'])
		    				array_push($temp['available_time'],$b);	
		    		}
		    		else if($value['start'] >= $temp['start'] && $temp['end'] >= $value['end'])
		    		{
		    			$b['start'] = $value['start'];
		    			$b['end'] = $value['end'];
		    			if($b['start'] < $b['end'])
		    				array_push($temp['available_time'],$b);	
		    		}
		    		else if ($value['start'] <= $temp['start'] && $temp['end'] >= $value['end'])
		    		{
		    			$b['start'] = $temp['start'];
		    			$b['end'] = $value['end'];
		    			if($b['start'] < $b['end'])
		    				array_push($temp['available_time'],$b);	
		    		}
		    		else if($value['start'] >= $temp['start'] && $temp['end'] <= $value['end'])
		    		{
		    			$b['start'] = $value['start'];
		    			$b['end'] = $temp['end'];
		    			if($b['start'] < $b['end'])
		    				array_push($temp['available_time'],$b);	
		    		}



		    		
		    	}

		    	array_push($return['available_booking_time'],$temp);

		    	//echo $temp['end'];exit;
		    	// $temp['available_time'] = $this->getAvailableTimeForBooking($temp['start'],$temp['end'],$date,$user_id,$end,$array,$pet['size']);
		    	// $temp['available_time'] = $this->getAvailableTimeForBookingForSlotSelection($temp['start'],$temp['end'],$date,$user_id,$start,$end,
		    	// 	$array,$pet['size'],$time_estimate);
		    	
		    }	
	    }

	    // if(!empty($groomer_services))
	    // {
	    // 	foreach ($groomer_services as $key => $value) {
	    // 		# code...
	    // 	}
	    // }


	     








	    // $this->db->where('groomer_schedule.end >=','CURTIME()',FALSE);
	    // $this->db->where('groomer_schedule.date >=','CURDATE()',FALSE);
	    // $this->db->order_by('groomer_schedule.date');
	    // $result = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'Date(date)'=>$date))->row_array();
	    // if(!empty($result))
	    // {
	    // 	$working_hrs = (strtotime($result['end']) - strtotime($result['start']))/60;

	    	
	    // }
	    // else
	    // {
	    // 	return false;
	    // }

	    return $return;

	}


	function myCompare($a, $b) {
		//pr($a);pr($b);
	    $aTime = strtotime($a['start']);
	    $bTime = strtotime($b['start']);
	    // pr($aTime);
	    // pr($bTime);

	    if( $aTime === $bTime ) { return 0; }
	    else { return $aTime < $bTime? -1 : 1; }
 	}

	function combinations(array $myArray, $choose) {
		  global $result, $combination;

		  $n = count($myArray);

		  
		  $this->inner(0, $choose, $myArray, $n);
		  return $result;
	}

	function inner ($start, $choose_, $arr, $n) {
	    global $result, $combination;

	    if ($choose_ == 0) array_push($result,$combination);
	    else for ($i = $start; $i <= $n - $choose_; ++$i) {
	           array_push($combination, $arr[$i]);
	           inner($i + 1, $choose_ - 1, $arr, $n);
	           array_pop($combination);
	     }
	}




	function rangesNotOverlapOpen($start_time1,$end_time1,$start_time2,$end_time2)
	{
	  $utc = new DateTimeZone('UTC');

	  $start1 = new DateTime($start_time1,$utc);
	  $end1 = new DateTime($end_time1,$utc);
	  if($end1 < $start1)
	    throw new Exception('Range is negative.');

	  $start2 = new DateTime($start_time2,$utc);
	  $end2 = new DateTime($end_time2,$utc);
	  if($end2 < $start2)
	    throw new Exception('Range is negative.');

	  return ($end1 <= $start2) || ($end2 <= $start1);
	}

	function getAvailableTimeForBookingForSlotSelection($start,$end,$date,$user_id,$schedule_start,$schedule_end,$service,$pet_size,$time_estimate)
	{
		$array = array();
		$return = array();
		$result = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date(date)'=>$date))->result_array();
		//pr($result);exit;
		if(!empty($result))
		{
			foreach ($result as $key => $value) 
			{
				$array = array();

				$array = $value;
				$array['time_estimate'] = $time_estimate;
				//$array['available_time'] = array();
				$array['available_time'] = $this->getAvailableTimeByAfterAppointment($date,$user_id,$value['start'],$value['end'],$service,$pet_size,$time_estimate,$value);			
				//pr($array);exit;
				array_push($return,$array);
				
			}	
		}


		return $return;

	}


	function getAppointmentsForSlots($user_id,$appointment_start,$appointment_end,$value)
	{
		//echo "here";
		$this->db->where("STR_TO_DATE(CONCAT(appointments.appointment_date, ' ', appointments.appointment_time), '%Y-%m-%d %H:%i:%s') <=",$appointment_end,TRUE);
		$this->db->where("STR_TO_DATE(CONCAT(appointments.appointment_date, ' ', appointments.appointment_end_time), '%Y-%m-%d %H:%i:%s') >=",$appointment_start,TRUE);
		$this->db->order_by('appointment_time','ASC','appointment_end_time','ASC');
		return $this->db->get_where('appointments',array('user_id'=>$user_id,'status != '=>'Cancel','overlapping_appointment'=>0))->result_array();
		

	}

	function getAvailableTimeByAfterAppointment($date,$user_id,$schedule_start,$schedule_end,$service,$pet_size,$time_estimate,$groomer_schedule)
	{
		$array = array();
		$this->db->order_by('appointments.appointment_time');
		$appointment = $this->db->get_where('appointments',array('user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$groomer_schedule['id'],'appointment_date'=>$date,'overlapping_appointment'=>0))->result_array();
		 //pr($appointment);
		if(empty($appointment))
		{
			$temp = array();
			$temp['start'] = $schedule_start;
			$temp['end'] = $schedule_end;
			array_push($array,$temp); 
			//return $array;
		}
		else
		{
			$count = count($appointment);
		
			if($count == 1)
			{
				//Single Appointment
				$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[0]['appointment_date'].' '.$appointment[0]['appointment_time']));
				$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[0]['appointment_date'].' '.$appointment[0]['appointment_end_time']));
				// echo "here single";
				// pr($appointmentEndTime2);
				// pr($schedule_start);
				// pr($schedule_end);exit;
				if($appointmentStartTime1 == $schedule_start)
				{
					//echo "here";
					$temp['start'] = $appointmentEndTime2;
					$temp['end'] = $schedule_end;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					//pr($diff);exit;
					//pr($diff);pr($time_estimate);exit;

					//pr($time_estimate);exit;
					//echo "hello";exit;
					//echo $diff;exit;
					if($diff >= $time_estimate)
					{
						//echo "here";exit;
						//pr($temp);exit;
						array_push($array,$temp); 
						 //pr($array);exit;
						 //return $array;
					}
					else
					{
						//Slot is not available
					}

					
					//array_push($array,$temp); 
				}
				else if($appointmentEndTime2 == $schedule_end)
				{
					$temp['start'] = $schedule_start;
					$temp['end'] = $appointmentStartTime1;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						 array_push($array,$temp);  
						 //return $array;
					}
					else
					{
						//Slot is not available

					}
				}
				else
				{	
					//Appointment is in middle 
					$temp['start'] = $schedule_start;
					$temp['end'] = $appointmentStartTime1;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						array_push($array,$temp); 

					}
					else
					{
						
						//Slot is not available

					}
					//pr($array);exit;


					$temp['start'] = $appointmentEndTime2;
					$temp['end'] = $schedule_end;
					$diff = intval(strtotime($temp['end']) - strtotime($temp['start']))/60;
					if($diff >= $time_estimate)
					{
						array_push($array,$temp); 
					}
					else
					{
						//Slot is not available
					}


					//return $array;


				}

				//return $array;	

			}
			else
			{
				//echo "More than one";
				
				for($i=0;$i<$count;$i++)
				{
					//echo $i."<br><br>";

					if($i+1 == $count)
					{
						$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_time']));
						$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_end_time']));

						// echo "here1";

						// echo $appointmentStartTime1;
						// echo $appointmentEndTime2;

						// exit;

						if($schedule_end != $appointmentEndTime2)
						{
							$gapStart = $appointmentEndTime2;
							$gapEnd = $schedule_end;
							//echo "last entry";
							//pr($gapStart);pr($gapEnd);exit;

							$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
							// pr($time_estimate);
							// pr($diff);exit;
							if($diff >= $time_estimate)
							{
								$temp['start'] = $gapStart;
								$temp['end'] = $gapEnd;
								array_push($array,$temp); 
							}
							else
							{
								//Slot is not available
							}

							//pr($array);exit;

							//return $array;
						}
						else
						{
							//Appointment is at the end
						}
					}
					else
					{
						//echo $count;

						$appointmentStartTime1 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_time']));
						$appointmentEndTime2 = date('Y-m-d H:i:s',strtotime($appointment[$i]['appointment_date'].' '.$appointment[$i]['appointment_end_time']));

						$appointmentStartTimeNext1 = date('Y-m-d H:i:s',strtotime($appointment[$i+1]['appointment_date'].' '.$appointment[$i+1]['appointment_time']));
						$appointmentEndTimeNext2 = date('Y-m-d H:i:s',strtotime($appointment[$i+1]['appointment_date'].' '.$appointment[$i+1]['appointment_end_time']));

						// echo "here2";
						// echo $appointmentStartTime1;
						// echo $appointmentStartTimeNext1;
						// exit;

						if($schedule_start != $appointmentStartTime1 && $schedule_start < $appointmentStartTime1)
						{
							//echo "1";
							//Means 1st appointment is greater than schedule time
							
							if($i == 0)
								$gapStart = $schedule_start;
							else
								$gapStart = $appointmentEndTime2;

							if($i == 0)
								$gapEnd = $appointmentStartTime1;
							else
								$gapEnd = $appointmentStartTimeNext1;

							// pr($gapStart);pr($gapEnd);
							$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
							if($diff >= $time_estimate)
							{
								$temp['start'] = $gapStart;
								$temp['end'] = $gapEnd;
								array_push($array,$temp);
								//pr($temp);
								//Add Gap after 1 st slott 
							}
							else
							{
								//Slot is not available
							}

							//pr($array);exit;


							if($appointmentEndTime2 ==  $appointmentStartTimeNext1)
							{
								//No Gap
							}
							else
							{
								//echo '2';
								$gapStart = $appointmentEndTime2;
								$gapEnd = $appointmentStartTimeNext1;
								//pr($gapStart);pr($gapEnd);
								$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
								if($diff >= $time_estimate)
								{
									$temp['start'] = $gapStart;
									$temp['end'] = $gapEnd;
									array_push($array,$temp); 
									//pr($temp);
								}
								else
								{
									//Slot is not available
								}
							}
						}
						else 
						{

							//if()

							//First appointment is at the start


							if($appointmentEndTime2 ==  $appointmentStartTimeNext1)
							{
								//No Gap go for next loop
							}
							else if($appointmentEndTime2 < $appointmentStartTimeNext1)
							{

								$gapStart = $appointmentEndTime2;
								$gapEnd = $appointmentStartTimeNext1;
								//pr($gapStart);pr($gapEnd);exit;

								$diff = intval(strtotime($gapEnd) - strtotime($gapStart))/60;
								// pr($time_estimate);
								// pr($diff);exit;
								if($diff >= $time_estimate)
								{
									$temp['start'] = $gapStart;
									$temp['end'] = $gapEnd;
									
									array_push($array,$temp); 

								}
								else
								{
									//Slot is not available
								}

							}



						}

						

						//return $array;	


					}
				}
				
				//pr($array);exit;
				//return $array;

			}	
		}
		// echo "here";
		// pr($array);exit;

		if(!empty($array))
			return $array;
		else
			return false;

	}

	// function getAvailableTimeForBooking($start,$end,$date,$user_id,$schedule_end,$service,$pet_size)
	// {
	// 	$array = array();
	// 	//echo $start;
	// 	$this->db->group_start();
	// 	$this->db->where(array('start <='=> $end));
	// 	$this->db->where(array('end >= '=>$start));
	// 	$this->db->group_end();
	// 	$this->db->where_in('service_id',$service);
	// 	$this->db->order_by('start');
	// 	$this->db->group_start();
	// 	$this->db->where(array('size '=> $pet_size));
	// 	$this->db->or_where('size IS NULL',null,false);
	// 	$this->db->group_end();
	// 	$result = $this->db->get_where('groomer_bookings',array('date(date)'=>$date,'user_id'=>$user_id,'pet_multiplier >' => 0))->result_array();
	// 	//pr($result);
	// 	 //echo $this->db->last_query();
	// 	if(!empty($result))
	// 	{
	// 		//pr($result);exit;
	// 		foreach ($result as $key => $value) {
	// 			$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$user_id,'service_id'=>$value['service_id']))->row_array();
				
	// 			if($value['pet_multiplier'] == $groomer_services['pet_multiplier'])
	// 			{
	// 				if(($start < $value['start'] && $value['start']  < $end) && ($start < $value['end'] && $value['end'] < $end))
	// 				{
	// 					//contain middle one slot

	// 					$temp['start'] = $value['start'];
	// 					$temp['end'] = $value['end'];
						
	// 					if($temp['start'] != $temp['end'])
	// 					{
	// 						if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 							array_push($array,$temp);
	// 					}	

	// 				}
	// 				else
	// 				{
	// 					if($start >= $value['start'] && $value['end'] >= $end){

	// 						$temp['start'] = $start;
	// 						$temp['end'] = $end;

	// 						if($temp['start'] != $temp['end'])
	// 						{
	// 							if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 								array_push($array,$temp);
	// 						}	
	// 					}
	// 					else if($start <= $value['start'] && $value['end'] >= $end)
	// 					{
	// 						$temp['start'] = $value['start'];
	// 						$temp['end'] = $end;

	// 						if($temp['start'] != $temp['end'])
	// 						{
	// 							if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 								array_push($array,$temp);
	// 						}	
	// 					}
	// 					else
	// 					{
	// 						$temp['start'] = $start;
	// 						$temp['end'] = $value['end'];

	// 						if($temp['start'] != $temp['end'])
	// 						{
	// 							if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 								array_push($array,$temp);
	// 						}	
	// 					}						
	// 				}	
					
	// 			}
	// 			else
	// 			{

	// 				if($pet_size == $value['size'])
	// 				{	

	// 					if(($start < $value['start'] && $value['start']  < $end) && ($start < $value['end'] && $value['end'] < $end))
	// 					{
	// 						//contain middle one slot

	// 						$temp['start'] = $value['start'];
	// 						$temp['end'] = $value['end'];
							
	// 						if($temp['start'] != $temp['end'])
	// 						{
	// 							if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 								array_push($array,$temp);
	// 						}	

	// 					}
	// 					else
	// 					{
	// 						if($start >= $value['start'] && $value['end'] >= $end){

	// 							$temp['start'] = $start;
	// 							$temp['end'] = $end;

	// 							if($temp['start'] != $temp['end'])
	// 							{
	// 								if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 									array_push($array,$temp);
	// 							}	
	// 						}
	// 						else if($start <= $value['start'] && $value['end'] >= $end)
	// 						{
	// 							$temp['start'] = $value['start'];
	// 							$temp['end'] = $end;

	// 							if($temp['start'] != $temp['end'])
	// 							{
	// 								if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 									array_push($array,$temp);
	// 							}	
	// 						}
	// 						else
	// 						{
	// 							$temp['start'] = $start;
	// 							$temp['end'] = $value['end'];

	// 							if($temp['start'] != $temp['end'])
	// 							{
	// 								if($this->checkInArrayIfSameValueExist($array,$temp)) 
	// 									array_push($array,$temp);
	// 							}	
	// 						}
	// 					}							
	// 				}
	// 			}
	// 		}	
	// 			// else
	// 			// {
	// 			// 		$temp['start'] = $value['start'];
	// 			// 		$temp['end'] = $value['end'];
	// 			// }
	// 			//Means No slot available



	// 		// 	if($end < $value['end']) {
	// 		// 		$temp = array();

	// 		// 		if($start < $value['start'])
	// 		// 			$temp['start'] = $value['start'];
	// 		// 		else
	// 		// 			$temp['start'] = $start;

	// 		// 		if($value['end'] < $end)
	// 		// 			$temp['end'] = $value['end'];
	// 		// 		else
	// 		// 			$temp['end'] = $end;

					
	// 		// 		if($end < $schedule_end)
	// 		// 			array_push($array,$temp);

	// 		// 		break;
	// 		// 	}
	// 		// 	else
	// 		// 	{
	// 		// 		//echo "here";exit;
	// 		// 		//pr($value);
	// 		// 		$temp = array();
	// 		// 		$temp['start'] = $start;

	// 		// 		if($end > $schedule_end)
	// 		// 			$temp['end'] = $value['end'];
	// 		// 		else
	// 		// 			$temp['end'] = $end;

					
	// 		// 		array_push($array,$temp);
	// 		// 		break;
	// 		// 	}

	// 		// }
	// 	}
	// 	//pr($array);exit;
	// 	return $array;
	// }


	function updateGroomerBookings($service_id,$start,$end,$date,$user_id,$pet_size,$petM)
	{
		//echo $petM;echo "dasdas";
		// $this->db->where(array('start'=>)
		$this->db->where(array('start <= ' =>$start,'end >=' =>$end));
		$row = $this->db->get_where('groomer_bookings',array('date'=>$date,'user_id'=>$user_id))->result_array();
		//echo $this->db->last_query();exit;
		// pr($row);exit;
		if(!empty($row))
		{
			foreach ($row as $key => $value) {
				$this->db->delete('groomer_bookings',array('id'=>$value['id']));
				$pet_multiplier = $value['pet_multiplier'];



				$temp['user_id'] = $user_id;
				$temp['date'] = $date;
				$temp['service_id'] = $value['service_id'];
				// $temp['last_pet_muliplier'] = $pet_multiplier;
				$temp['size'] = null;
				//echo $petM;echo $pet_size;exit;


				if($value['service_id'] == $service_id)
				{
					
						

					//Same service used
					
					if($value['start'] == $start){
					  	
					  	$temp['start'] = $start;
					  	$temp['end'] = $end; 

					  	// // if($petM > 1)
					  	// 	$temp['size'] = $pet_size;
					  	// else
					  	// 	$temp['size'] = null;

					  	$temp['size'] = $pet_size;

					  	$temp['pet_multiplier'] = $pet_multiplier - 1;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);


					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end'];  
					  	$temp['size'] = null;
					  	$temp['pet_multiplier'] = $pet_multiplier;

					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);
				  
				  	}
				  	elseif($value['end'] == $end)
				  	{

				  		$temp['start'] = $value['start'];
					  	$temp['end'] = $start;
					  	$temp['size'] = null;
					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $start;
					  	$temp['end'] = $end; 
					  	// // if($petM > 1)
					  	// 	$temp['size'] = $pet_size;
					  	// else
					  	// 	$temp['size'] = null;

					  	$temp['size'] = $pet_size;

					  	$temp['pet_multiplier'] = $pet_multiplier-1;

					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end']; 

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	$temp['size'] = null;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);


				  	}
				  	else {
					  	$temp['start'] = $value['start'];
					  	$temp['end'] = $start;  
					  	$temp['size'] = null;
					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $start;
					  	$temp['end'] = $end;  
					  	// // if($petM > 1)
					  	// 	$temp['size'] = $pet_size;
					  	// else
					  	// 	$temp['size'] = null;

					  	$temp['size'] = $pet_size;
					  	$temp['pet_multiplier'] = $pet_multiplier - 1;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end'];  
					  	$temp['size'] = null;
					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);


				  	}
				}
				else
				{
					//other service
					if($value['start'] == $start){

					  	$temp['start'] = $start;
					  	$temp['end'] = $end;  

					  	$temp['pet_multiplier'] = 0;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end'];  

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);
				  
				  	}
				  	elseif($value['end'] == $end)
				  	{

				  		$temp['start'] = $value['start'];
					  	$temp['end'] = $start;

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $start;
					  	$temp['end'] = $end;   

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end']; 

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);
				  	} 	
				  	else 
				  	{
					  	$temp['start'] = $value['start'];
					  	$temp['end'] = $start;  

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $start;
					  	$temp['end'] = $end;  

					  	$temp['pet_multiplier'] = 0;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);

					  	$temp['start'] = $end;
					  	$temp['end'] = $value['end'];  

					  	$temp['pet_multiplier'] = $pet_multiplier;
					  	if($temp['start'] != $temp['end'])
					  		$this->db->insert('groomer_bookings',$temp);
				  	}  

				}
			

			}
		}
	}

	function checkForExtentedTimeForAppointment($date,$services,$pet_id,$appuser,$shop_no,$user_id,$start_time,$end_time)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		
		//pr($pet);//var_dump($services);
		

		$array = explode(',', $services);
	   	$time_estimate = 0;
	   	//pr($array);exit;
	   	$time_estimate = 0;
	   	$start = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));
	   	foreach ($array as $key ) {
	        $temp = $this->db->get_where('services',array('id'=>$key))->row_array();

	        $time_estimate += $temp[$pet['size'].'_time_estimate'];
	        $cost += $temp[$pet['size'].'_cost'];
	    }
	    
	    
	    $end = date('Y-m-d H:i:s',strtotime($start.' + '.$time_estimate.' minutes'));


	    $t = $this->db->get_where('groomer_schedule',array('date'=>$date,'user_id'=>$user_id))->row_array();
	    //pr($t);echo $start;echo $end;exit;
	    if(!empty($t))
	    {
	    	if(strtotime($t['start']) <= strtotime($start) && strtotime($end) <= strtotime($t['end']))
	    	{
	    		return false;
	    	}
	    	
	    }

	    return true; 	
	}

	function getBestAvailableTimeForSlot($date,$services,$pet_id,$appuser,$shop_no,$user_id,$start_time,$end_time)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		
		//pr($pet);//var_dump($services);
		

		$array = explode(',', $services);
	   	$time_estimate = 0;
	   	//pr($array);exit;
	   	$time_estimate = 0;
	   	foreach ($array as $key ) {
	        $temp = $this->db->get_where('services',array('id'=>$key))->row_array();
	        
	        $time_estimate += $temp[$pet['size'].'_time_estimate'];
	        $cost += $temp[$pet['size'].'_cost'];
	    }


	    //if($end)


	    if($return = $this->getBestAvailableSlotWithTime($array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate))
	    {
	    	//echo"ok1";
	    	//pr($return);exit;
	   		$return['time_estimate'] = $time_estimate;
	    	$return['cost'] = $cost;

	    	$temp_start = date('H:i:s',strtotime($return['start']));
	    	$temp_end = date('H:i:s',strtotime($return['end']));


	    	// New logic for blocker (hard blocker and Lunch time)
	    	$str2 = "SELECT * FROM `groomer_schedule` WHERE `user_id` = '".$user_id."' AND `date` = '".$date."' 
	    	AND (
	    			( TIME('".$temp_start."') >= TIME(`hard_blocker_start`)
                 	AND TIME('".$temp_start."') < TIME(`hard_blocker_end`))

		            OR (TIME('".$temp_end."') > TIME(`hard_blocker_start`)
		                 AND TIME('".$temp_end."') <= TIME(`hard_blocker_end`))

            		OR(`hard_blocker_start` >= '".$temp_start."' AND `hard_blocker_end` < '".$temp_end."' )
           )";

           $query2 = $this->db->query($str2);
           $result2 = $query2->result_array();
          
           if(!empty($result2)){
           		return false;
           }

           $str2 = "SELECT * FROM `groomer_schedule` WHERE `user_id` = '".$user_id."' AND `date` = '".$date."' 
	    	AND (
	    			( TIME('".$temp_start."') >= TIME(`lunch_time_start`)
                 	AND TIME('".$temp_start."') < TIME(`lunch_time_end`))

		            OR (TIME('".$temp_end."') > TIME(`lunch_time_start`)
		                 AND TIME('".$temp_end."') <= TIME(`lunch_time_end`))

            		OR(`lunch_time_start` >= '".$temp_start."' AND `lunch_time_end` < '".$temp_end."' )
           )";

           $query2 = $this->db->query($str2);
           $result2 = $query2->result_array();
          
           if(!empty($result2)){
           		return false;
           }

           // New logic for blocker (soft blocker)
	    	$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date, 'blocker_start !='=> '', 'blocker_start <='=>$temp_start,'blocker_end >'=>$temp_start))->row_array();


	    	if (!empty($groomer_schedule)) {
	    		if ($groomer_schedule['blocker_end'] < $end_time){
		    		$return['start'] = $date .' '.$groomer_schedule['blocker_end'];
		    		$return['end'] = date('Y-m-d H:i:s',strtotime($return['start'].' + '.$time_estimate.' minutes'));
		    	}else{
		    		return false;
		    	}
	    	}
	   		return $return;
	    }
	   	else
	    {
	   		return false;	
	    }

	    //Direct Order for services
	    //Means a,b,c
	    // if($r = getBestTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array))
	    // {
	    // 	$return['time_estimate'] = $time_estimate;
	    // 	$return['cost'] = $cost;
	    // 	$return['start'] = $r['start'];
	    // 	$return['end'] = $r['end'];
	    // }

	   
	    

	    
	    

	}


	function getBestAvailableSlotWithTime($array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate)
	{
		$return['working_time'] = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date))->row_array();

	    $start = $return['working_time']['start'];
	    $end = $return['working_time']['end'];

	    $temp = array();
	    $temp['start'] = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));
	    $temp['end'] =date('Y-m-d H:i:s',strtotime($date.' '.$end_time));


	   

		$availableTime = $this->getAvailableTimeForBookingForSlotSelection($temp['start'],$temp['end'],$date,$user_id,$start,$end,
		    		$array,$pet['size'],$time_estimate);




		/*$availableArray = array();
		//pr($availableTime1);exit;
		if(!empty($availableTime))
		{

			foreach ($availableTime as $key => $value) {
				if(!empty($value['available_time']))
				{
					foreach ($value['available_time'] as $v) {
						# code...
						$t = array();
						$t = $v;
						$t['id'] = $value['id'];
						array_push($availableArray,$t);
					}
					
				
				}

			}
		}
		
		$NewavailableArray = array();
		$blocker_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['hard_blocker_start']));
		$blocker_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['hard_blocker_end']));
		$lunch_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['lunch_time_start']));
		$lunch_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['lunch_time_end']));
		$soft_start = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['blocker_start']));
		$soft_end = date('Y-m-d H:i:s',strtotime($date.' '.$return['working_time']['blocker_end']));

		// $soft_start ='2018-11-02 19:00:00';
		// $soft_end = '2018-11-02 19:30:00';

		//pr($return);exit;
		foreach ($availableArray as $key => $v) {
			$tempflag = 0;
			if(($v['start'] < $blocker_start && $v['end'] > $blocker_start) || ($v['start'] < $blocker_end && $v['end'] > $blocker_end)){
				if($v['start'] < $blocker_start){
					$tempflag =1; 
					$t = array();
					$t['start'] = $v['start'];
					$t['end'] = $blocker_start;
					$t['id'] = $v['id'];
					array_push($NewavailableArray,$t);
					
				}
				if($v['end'] > $blocker_end){
					$tempflag =1;
					$t = array();
					$t['start'] = $blocker_end;
					$t['end'] = $v['end'];
					
					$t['id'] = $v['id'];
					array_push($NewavailableArray,$t);
				}
			}
			if(!$tempflag){
				array_push($NewavailableArray,$v);
			}
		}
		//pr($NewavailableArray);
		$availableArray = array();
		foreach ($NewavailableArray as $key => $v) {
			$tempflag = 0;
			if(($v['start'] < $lunch_start && $v['end'] > $lunch_start) || ($v['start'] < $lunch_end && $v['end'] > $lunch_end)){
				if($v['start'] < $lunch_start){
					$tempflag =1; 
					$t = array();
					$t['start'] = $v['start'];
					$t['end'] = $lunch_start;
					$t['id'] = $v['id'];
					array_push($availableArray,$t);	
				}
				if($v['end'] > $lunch_end){
					$tempflag =1;
					$t = array();
					$t['start'] = $lunch_end;
					$t['end'] = $v['end'];
					$t['id'] = $v['id'];
					array_push($availableArray,$t);
				}
			}
			if(!$tempflag){
				array_push($availableArray,$v);
			}
		}

		$servicesOrder = implode (",",$array);

		$appointment = array();
		$appointment['order'] = $servicesOrder;
		
		//New logoc written by ajay-start
		$return['appointments'] = array();
		$dataReturn['overbooking'] = 0;
		$firstSlotId = $availableArray[0]['id'];
		$stagger_time = get_option('stagger_time');
		//pr($availableArray);exit;
		//pr($availableArray);
		if(!empty($availableArray)){
			foreach ($availableArray as $key => $value) {
				
				$appointment['allocated_schedule_id'] = $value['id'];
				$appointment['start'] = $temp['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($temp['start'].' + '.$time_estimate.' minutes'));
				$powerError = 0;
				
				
				if($value['start'] <= $appointment['start'] && $appointment['end'] <= $value['end'])
				{	
					// echo"dsfd";
					// pr($value);
					// pr($appointment);exit;
					$schedule_start = $appointment['start'];
					$schedule_end = date('Y-m-d H:i:s',strtotime($schedule_start.' + 60 minutes'));

					//echo $schedule_end; exit;

					do{

						//check soft blocke
						$flg_done = 0;
						if( $soft_start <= $appointment['start'] && $soft_end > $appointment['start'] ){
							$flg_done = 1;
							$appointment['start'] = $soft_end;
							$appointment['end'] =  date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));

						}
						
						// check stagger time for 2nd schedule slot

						if($firstSlotId != $value['id']){

							$BackAppointmentCheck = date('H:i:s',strtotime($appointment['start'].' -'.$stagger_time.' minutes'));
							$AheadAppoinmentCheck = date('H:i:s',strtotime($appointment['start'].' +'.$stagger_time.' minutes'));

							$this->db->where(array('appointment_time >'=>$BackAppointmentCheck,'appointment_time <='=>date('H:i:s',strtotime($appointment['start']))));
							$CheckAppointment1 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 
							
							$this->db->where(array('appointment_time <'=>$AheadAppoinmentCheck,'appointment_time >'=>date('H:i:s',strtotime($appointment['start']))));
							$CheckAppointment2 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 	
							
							//pr($CheckAppointment1); pr($CheckAppointment2);pr($appointment);exit;

							if(!empty($CheckAppointment1) && !empty($CheckAppointment2)){
								$flg_done = 1;
								$appointment['start'] = date('H:i:s',strtotime($appointment['start'].' +'.$stagger_time.' minutes'));
								$appointment['end'] =  date('H:i:s',strtotime($appointment['end'].' +'.$stagger_time.' minutes'));
							}
						}
						
						// Check schedule time
						$powerError = 0;
						
						if($value['end'] >= $appointment['end'] && $schedule_end >= $appointment['start'])
						{	

							if($pet['puppy_power'] > 1){
								$count = 0;
								foreach ($availableArray as $key1 => $value1){
									if($value1['start'] < $appointment['start'] && $value1['end'] > $appointment['end'] ){
										$count++;
									}
								}

								//Check power
								//$count = 10;
								if($count > $pet['puppy_power']){
									$dataReturn['normal_booking_time'] = $appointment;	
									
									return $dataReturn;
								}else{
									$flg_done = 1;
									$powerError = 1;
								}
							}else{
								$dataReturn['normal_booking_time'] = $appointment;

								return $dataReturn; 
							}
							
						}else{
							$flg_done = 1;
							// increase THE APPOINTMENT START TIME TO BREAK THE LOOP
							$appointment['start'] = $value['end'];
							$appointment['end'] =  date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
						}

						// echo "<br>app start ".$appointment['start'];
						// echo "<br>app end ".$appointment['end'];
						// echo "<br>sc s".$schedule_start;
						
						// echo "<br>sc end".$schedule_end;
						// echo "here<br>";
						// echo"end here";
						// $testI = 1;
						// if($testI == 2){
						// 	echo "stop;;;;"; exit;
						// }
						// $testI = 2;
						// exit;
					}
					while($appointment['start'] < $schedule_end && $powerError == 0);	
				}
			}
		}*/



		//Old logic
		$availableArray = array();

		if(!empty($availableTime))
		{
			foreach ($availableTime as $key => $value) {
				if(!empty($value['available_time']))
				{
					foreach ($value['available_time'] as $v) {
						# code...
						$t = array();
						$t = $v;
						$t['id'] = $value['id'];
						array_push($availableArray,$t);
					}
					
				
				}

			}
		}

		
		//Check For Best Available Slot
		$servicesOrder = implode (",",$array);
		
		$appointment = array();
		$appointment['order'] = $servicesOrder;
		foreach ($availableArray as $key => $value) {
			
			if($value['start'] <= $temp['start'])
			{
				//Chec
				//echo "here";
				//pr($value);pr($temp);
				$appointment['start'] = $temp['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
				$appointment['allocated_schedule_id'] = $value['id'];


				//First Check If Appointment

				$this->db->where(array('appointments.appointment_time <'=> date('H:i:s',strtotime($appointment['end'])), 'appointments.appointment_end_time >'=> date('H:i:s',strtotime($appointment['start']))));

				$checkSameSlotAppointment = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$value['id'],'overlapping_appointment'=>0))->result_array(); 

				$return = array();
				$return = $this->finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate);
				if(!empty($return))
					return $return;
				
			}
			else if($value['start'] > $temp['start'] && $temp['end'] > $value['start'])
			{
				
				$appointment['start'] = $value['start'];
				$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));
				$appointment['allocated_schedule_id'] = $value['id'];
				$return = array();
				$return = $this->finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate);
				if(!empty($return))
					return $return;
			}		
		}


	}


	function finalValidation($appointment,$value,$temp,$array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate)
	{
		do
		{
			//echo "loop".$appointment['start'];
			$this->db->where(array('appointments.appointment_time <'=> date('H:i:s',strtotime($appointment['end'])), 'appointments.appointment_end_time >'=> date('H:i:s',strtotime($appointment['start']))));

			$checkSameSlotAppointment = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id'=>$value['id'],'overlapping_appointment'=>0))->result_array(); 

			// echo $this->db->last_query();
			// pr($checkSameSlotAppointment);

			//$x = 15;
			$x = get_option('stagger_time');
			if(empty($checkSameSlotAppointment))
			{
				
				$BackAppointmentCheck = date('H:i:s',strtotime($appointment['start'].' -'.$x.' minutes'));
				$AheadAppoinmentCheck = date('H:i:s',strtotime($appointment['start'].' +'.$x.' minutes'));

				$this->db->where(array('appointment_time >'=>$BackAppointmentCheck,'appointment_time <='=>date('H:i:s',strtotime($appointment['start']))));
				$CheckAppointment1 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 
				
				$this->db->where(array('appointment_time <'=>$AheadAppoinmentCheck,'appointment_time >'=>date('H:i:s',strtotime($appointment['start']))));
				$CheckAppointment2 = $this->db->get_where('appointments',array('appointment_date'=>$date,'user_id'=>$user_id,'status !='=>'Cancel','allocated_schedule_id !='=>$value['id'],'overlapping_appointment'=>0))->result_array(); 	
				
				//pr($CheckAppointment1); pr($CheckAppointment2);pr($appointment);exit;

				if(empty($CheckAppointment1) && empty($CheckAppointment2))
					return $appointment; 

			}
			$appointment['start'] = date('Y-m-d H:i:s',strtotime($appointment['start'].' +'.$x.' minutes'));
			$appointment['end']	= date('Y-m-d H:i:s',strtotime($appointment['start'].' + '.$time_estimate.' minutes'));	

		}
		while(date('H:i:s',strtotime($temp['end'])) > date('H:i:s',strtotime($appointment['start'])));
	}


	function getNormalTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array)
	{
		//pr($date);exit;
		$count = count($array);
		$return = array();
		$i = 0;
		//pr($pet);exit;
		//echo $count; 
		//
		$start_time = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));
		$end_time = date('Y-m-d H:i:s',strtotime($date.' '.$end_time));
		foreach ($array as $value) {
	    	



		   

			$t = $this->db->query("SELECT *
					FROM `groomer_bookings`
					WHERE `user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					AND ('".$start_time."' < groomer_bookings.end and '".$end_time."' > groomer_bookings.start)
					UNION 
					SELECT *
					FROM `groomer_bookings` Where
					`user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					And ('".$start_time."' >= groomer_bookings.start and '".$end_time."' <= groomer_bookings.end)")->result_array();

		    // echo $this->db->last_query();
		    //pr($t);
		    if(!empty($t))
		    {
		    	foreach ($t as $k => $v) {
		    		
			    	if($v['start'] <= $start_time)
			    	{	
			    		if(empty($v['size']) || ($pet['size'] == $v['size']))
			    		{
			    			//pr($v);
			    			
			    			//$n = 1;
			    			$service = $this->db->get_where('services',array('id'=>$v['service_id']))->row_array();
			    			
			    			if($start_time <= $v['start'])
			    				$start = $v['start'];
			    			else
			    				$start = $start_time;

			    			//pr($start);exit;

			    			//echo $start;exit;
			    			//pr($start);
			    			//pr($pet['size']);exit;
			    			//echo $service[$pet['size'].'_time_estimate'];exit;
			    			$end = date('Y-m-d H:i:s',strtotime($start,strtotime(' + '.$service[$pet['size'].'_time_estimate'].' minutes')));
			    			//pr($end);
			    			//echo $service[$pet['size'].'_time_estimate'];
			    			$return['start'] =  $start;
			    			//Slot is available
			    			//if($this->checkforNextSlot($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array,$value))
			    			//echo "enddasdas";exit;
			    			if($count > 1)
			    			{
				    			for($j=1;$j<$count;$j++)
				    			{
				    				// pr($end);
				    				// echo "end";


				    				//echo "ddas";
				    				if($j==1)
				    					$r['start'] = $end;
				    				else
				    					$r['start'] = $r['end'];

				    				// pr($r['start']);
				    				// echo "start";
				    				
				    				$s = $this->db->get_where('services',array('id'=>$array[$j]))->row_array();
				    				$temp = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));

				    				$a = $this->db->query("SELECT *
							            FROM `groomer_bookings`
										WHERE `user_id` = '".$user_id."'
										AND `date` = '".$date."'
										AND `pet_multiplier` >0
										AND `service_id` = ".$array[$j]."
										AND ('".$r['start']."' < groomer_bookings.end and '".$temp."' > groomer_bookings.start)
										UNION 
										SELECT *
										FROM `groomer_bookings` Where
										`user_id` = '".$user_id."'
										AND `date` = '".$date."'
										AND `pet_multiplier` >0
										AND `service_id` = ".$array[$j]."
										And ('".$r['start']."' >= groomer_bookings.start and '".$temp."' <= groomer_bookings.end)

										")->row_array();

				   					 // echo $this->db->last_query();
				   					
				   					 //  pr($a);exit;
				   					if(!empty($a)){
				   						if(empty($a['size']) || ($pet['size'] == $a['size']))
				   						{
				   							//echo $s[$pet['size'].'_time_estimate'];	
				   							$r['start'] = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));
				   							$r['end'] = $r['start'];
				   							
				   						}
				   						else
				   						{
				   							return false;
				   						}	
				   					}	
				   					else
				   					{
				   						return false;
				   					}			



				    				//Checking For Next Slot

				    			}
			    				$return['end'] = $r['end'];
			    			}
			    			else {
			    				$this->db->where(array('start <='=>$start,'end >=' =>$end));
			    				$r = $this->db->get_where('groomer_bookings',array('user_id'=>$user_id,'service_id'=>$value,'pet_multiplier >'=>0))->row_array();
			    				if(empty($r))
			    					return false;
			    				else
			    					$return['end'] = $end;

				    			
				    			//exit;
				    			return $return;
			    			}

			    		}
			    	}
			    	else
			    	{
			    		return false;
			    	}

		    		

		    	}
		    	
		    }

		    return false;

	    }
	}

	function getBestTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array)
	{
		//pr($pet);
		$count = count($array);
		$return = array();
		$i = 0;
		//echo $count; 
		//
		$start_time = date('Y-m-d H:i:s',strtotime($date.' '.$start_time));
		$end_time = date('Y-m-d H:i:s',strtotime($date.' '.$end_time));
		foreach ($array as $value) {
	    	# code...
		    // $this->db->where(array('user_id'=>$user_id,'date'=>$date,'pet_multiplier >'=>0));
		    // // $this->db->group_start();
		    // // $this->db->where(array('start <='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start <='=>$start_time,'end <='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->group_end();

		    // $this->db->group_start();
		    // $this->db->where(array('start <='=>$end_time,'end >='=> $start_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // // $this->db->or_where(array('start <='=>$start_time,'end <='=> $end_time));
		    // // $this->db->or_where(array('start >='=>$start_time,'end >='=> $end_time));
		    // $this->db->group_end();



		    // $t = $this->db->get_where('groomer_bookings',array('service_id'=>$value))->result_array();

			$t = $this->db->query("SELECT *
					FROM `groomer_bookings`
					WHERE `user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					AND ('".$start_time."' < groomer_bookings.end and '".$end_time."' > groomer_bookings.start)
					UNION All
					SELECT *
					FROM `groomer_bookings` Where
					`user_id` = '".$user_id."'
					AND `date` = '".$date."'
					AND `pet_multiplier` >0
					AND `service_id` = ".$value."
					And ('".$start_time."' >= groomer_bookings.start and '".$end_time."' <= groomer_bookings.end)")->result_array();

		    //echo $this->db->last_query();
		    //pr($t);
		    if(!empty($t))
		    {
		    	foreach ($t as $k => $v) {
		    		if(empty($v['size']) || ($pet['size'] == $v['size']))
		    		{
		    			//pr($v);
		    			//$n = 1;
		    			$service = $this->db->get_where('services',array('id'=>$v['service_id']))->row_array();
		    			
		    			if($start_time <= $v['start'])
		    				$start = $v['start'];
		    			else
		    				$start = $start_time;

		    			//echo $start;
		    			$end = date('Y-m-d H:i:s',strtotime($start.' +'.$service[$pet['size'].'_time_estimate'].' minutes'));
		    			

		    			$this->db->where(array('start <='=>$start,'end >='=>$end));

					   	$x = $this->db->get_where('groomer_bookings',array('user_id'=>$user_id,'pet_multiplier >'=>0))->row_array();
					   	if(empty($x)){
					   		return false;
					   	}

		    			//echo $end;exit;
		    			//pr($pet['size']);exit;
		    			//echo $service[$pet['size'].'_time_estimate'];
		    			$return['start'] =  $start;
		    			//Slot is available
		    			//if($this->checkforNextSlot($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$array,$value))
		    			
		    			if($count > 1)
		    			{
			    			for($j=1;$j<$count;$j++)
			    			{
			    				//echo "ddas";
			    				if($j==1)
			    					$r['start'] = $end;
			    				else
			    					$r['start'] = $r['end'];
			    				
			    				$s = $this->db->get_where('services',array('id'=>$array[$j]))->row_array();
			    				$temp = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));

			    				$a = $this->db->query("SELECT *
						            FROM `groomer_bookings`
									WHERE `user_id` = '".$user_id."'
									AND `date` = '".$date."'
									AND `pet_multiplier` >0
									AND `service_id` = ".$array[$j]."
									AND ('".$r['start']."' < groomer_bookings.end and '".$temp."' > groomer_bookings.start)
									UNION All 
									SELECT *
									FROM `groomer_bookings` Where
									`user_id` = '".$user_id."'
									AND `date` = '".$date."'
									AND `pet_multiplier` >0
									AND `service_id` = ".$array[$j]."
									And ('".$r['start']."' >= groomer_bookings.start and '".$temp."' <= groomer_bookings.end)

									")->row_array();

			   					// echo $this->db->last_query();
			   					
			   					//pr($a);exit;
			   					if(!empty($a)){
			   						if(empty($a['size']) || ($pet['size'] == $a['size']))
			   						{
			   							//echo $s[$pet['size'].'_time_estimate'];	
			   							$oldr = $r['start'];
			   							$r['start'] = date('Y-m-d H:i:s',strtotime($r['start'].' +'.$s[$pet['size'].'_time_estimate'].' minutes'));
			   							
			   							$this->db->where(array('start <='=>$oldr,'end >='=>$r['start']));

					    				$t = $this->db->get_where('groomer_bookings',array('user_id'=>$user_id,'pet_multiplier >'=>0))->row_array();
					    				if(!empty($t)){
					    					$r['end'] = $r['start'];
					    				}
					    				else
					    				{
					    					return false;
					    				}	
			   							
			   						}
			   						else
			   						{
			   							return false;
			   						}	
			   					}	
			   					else
			   					{
			   						return false;
			   					}			



			    				//Checking For Next Slot

			    			}

		    				$return['end'] = $r['end'];
		    				return $return;
		    			}
		    			else
		    			{

		    				

		    				$this->db->where(array('start <='=>$start,'end >=' =>$end));
		    				$r = $this->db->get_where('groomer_bookings',array('user_id'=>$user_id,'pet_multiplier >'=>0))->row_array();
		    				//pr($r);exit;
		    				if(!empty($r))
		    				{	
		    					$return['end'] = $end;
		    					return $return;
		    				}	

		    				
		    			}	
		    			
		    			//exit;
		    			

		    		}

		    		

		    	}
		    	
		    }

		    return false;

	    }
	}

	function checkifAppointmentExitsForthistime($date,$services,$pet_id,$appuser,$shop_no,$user_id,$start_time,$end_time)
	{
		$return = array();
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id)); 
		
		//var_dump($services);exit;
		

		$array = $services;
	   	$time_estimate = 0;
	   	//pr($array);exit;
	   	$time_estimate = 0;
	   	foreach ($array as $key ) {
	        $temp = $this->db->get_where('services',array('id'=>$key))->row_array();
	        $time_estimate += $temp[$pet['size'].'_time_estimate'];
	        $cost += $temp[$pet['size'].'_cost'];
	    }
	    
	    if($return = $this->pc_permute_only($array,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate))
	    {
	    	//echo"ok1";
	    	//pr($return);exit;
	   		$return['time_estimate'] = $time_estimate;
	    	$return['cost'] = $cost;
	   		return $return;
	    }
	   	else
	    {
	   		return false;	
	    }
	}

	function pc_permute_only($items,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$perms = array( )) {
	    $return  = array();
	  	if (empty($items)) { 
	       if($r = $this->getNormalTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$perms))
		    {
		    	//pr($r);
		    	//pr($perms);
		    	$return['start'] = $r['start'];
		    	$return['end'] = $r['end'];

		    	$order = implode (", ",$perms);

		    	$return['order'] = $order;
		    	
		    	return $return;
		    	//exit;

		    }
	    }  else {

	    	if(empty($return))
	    	{
		        for ($i = count($items) - 1; $i >= 0; --$i) {
		             $newitems = $items;
		             $newperms = $perms;
		             list($foo) = array_splice($newitems, $i, 1);
		             array_unshift($newperms, $foo);
		             //pr($newperms);exit;
		            if($r = $this->pc_permute_only($newitems,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$newperms))
		             {
				    	if(!empty($r))
				    		return $r;
				    }
		         }
		    }
		    else
		    {
		    	return $return;
		    }     

	    }
	}


	function pc_permute($items,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$perms = array( )) 
	{
	  $return  = array();
	  if (empty($items)) { 
	       if($r = $this->getBestTime($date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$perms))
		    {
		    	//pr($r);
		    	//pr($perms);
		    	$return['start'] = $r['start'];
		    	$return['end'] = $r['end'];

		    	$order = implode (", ",$perms);

		    	$return['order'] = $order;
		    	
		    	return $return;
		    	//exit;

		    }
	    }  else {

	    	if(empty($return))
	    	{
		        for ($i = count($items) - 1; $i >= 0; --$i) {
		             $newitems = $items;
		             $newperms = $perms;
		             list($foo) = array_splice($newitems, $i, 1);
		             array_unshift($newperms, $foo);
		             //pr($newperms);exit;
		            if($r = $this->pc_permute($newitems,$date,$services,$pet,$appuser,$shop_no,$user_id,$start_time,$end_time,$time_estimate,$newperms))
		             {
				    	if(!empty($r))
				    		return $r;
				    }
		         }
		    }
		    else
		    {
		    	return $return;
		    }     

	    }
	}


	function cancelUpdateBooking($appointment_services,$user_id,$date,$appointment,$pet_size)
	{

		$this->db->delete('groomer_bookings',array('date'=>$date,'user_id'=>$user_id));

		$schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$date))->row_array();
		

		$this->db->select('appointment_services.*');
		$this->db->join('appointment_services','appointments.id=appointment_services.appointment_id');
		$this->db->order_by('appointments.added');
		$result = $this->db->get_where('appointments',array('appointments.id !='=>$appointment['id'],'appointments.user_id'=>$user_id,'appointments.status !=' => 'Cancel'))->result_array();
		//pr($result);

		$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$user_id))->result_array();

		$this->Management_model->addGroomerBookingTime($groomer_services,$date,$schedule['start'],$schedule['end']);



		if(!empty($result))
		{
			//Add Schedule for date first



			foreach ($result as $key => $value) {

				$groom = $this->db->get_where('groomer_services',array('service_id'=>$value['service_id'],'user_id'=>$user_id))->row_array();
				
				$this->updateGroomerBookings($value['service_id'],$value['start'],$value['end'],$date,$user_id,$pet_size,$groom['pet_multiplier']);
			}
		}


		return true;
		

		


	}

	

	function checkInArrayIfSameValueExist($array,$temp)
	{
		if(!empty($array))
		{
			foreach ($array as $key => $value) {
				if($value['start'] == $temp['start'] && $value['end'] == $temp['end'])
				return false;
			}
		}

		return true;
	}

	function getPaymentsDetails($id,$category,$shop_id)
	{
		// echo $id;exit;
		// $shop_id;exit;
		//$this->db2 = $this->load->database('main', TRUE);
		// return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$shop_id,'transcation_type'=>'payment','is_add_on_payment'=>0))->row_array();  old sanjeev
		return $this->db2->get_where('transactions',array('app_user_id'=>$shop_id,'transcation_type'=>'payment'))->row_array();
	}

	function getAdditionalPayment($id,$category,$shop_id)
	{
		return $this->db2->get_where('transactions',array('reference_id'=>$id,'category'=>$category,'shop_id'=>$shop_id,'transcation_type'=>'payment','is_add_on_payment'=>1))->row_array();
	}


	function getBoardingInventoryAvailability($pet_id,$appuser,$shop_no)
	{
		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id));
		$size_priority = getSizePriority($pet['size']);

		$boarding_inventory = $this->db->get_where('boarding_inventory',array('size_priority >='=>$size_priority))->row_array();
		if(!empty($boarding_inventory))
		{
			return false;
		}
		else
		{
			return $pet['size'];
		}


	}

	function getBoardingScheduleAvailabilty($pet_id,$appuser,$shop_no)
	{
		$this->db->where('start >=','CURDATE()',FALSE);
		$boarding_schedule = $this->db->get_where('boarding_schedule')->row_array();

		if(!empty($boarding_schedule))
		{
			return false;
		}
		return true;
	}


	function getBoardingCalendar($pet_id,$appuser,$shop_no)
	{
		$return = array();

		$pet = $this->getPetDetails($appuser['id'],$shop_no,array('id'=>$pet_id));
		$size_priority = getSizePriority($pet['size']);

		$this->db->select('count(inventory_id) as orginal_capacity,sum(boarding_schedule.is_booked) as current_capacity,boarding_schedule.start,boarding_schedule.end');
		$this->db->group_by('date(boarding_schedule.start)');
		$this->db->where(array('size_priority >='=>$size_priority));
		$this->db->where('start >=','CURDATE()',FALSE);
		$result = $this->db->get_where('boarding_schedule')->result_array();
		//pr($result);exit;
		//echo $this->db->last_query();exit;
		if(!empty($result))
		{
			foreach ($result as $key => $value) {
				$temp = array();
				$temp = $value;
			
				unset($temp['end']);

				$temp['checked_in'] = 1;
				
				
				
				$temp['check_out'] = 1;
				$this->db->select('count(inventory_id) as orginal_capacity,sum(boarding_schedule.is_booked) as current_capacity,boarding_schedule.start,boarding_schedule.end');
				$this->db->group_by('date(boarding_schedule.end)');
				$r = $this->db->get_where('boarding_schedule',array('end'=>$value['start'],'size_priority >='=>$size_priority))->row_array();
				
				

				
					
				if($value['current_capacity'] == $value['orginal_capacity'])
				{
					
						
						$temp['booking_status'] = 'red';
						//$temp['available_time'] = $availableTime;
					

						
				}
				else
				{

					

					if( $value['current_capacity'] == 0){
						
						$temp['booking_status'] = 'green';
						//$temp['available_time'] = $availableTime;
						
					}
					else
					{
						$temp['current_capacity'] = $value['orginal_capacity'] - $value['current_capacity'];
						$temp['booking_status'] = 'yellow';
					}

				 	
				//$temp['available_time'] = $timeAvailable;
				}
				
				array_push($return,$temp);


				//$temp['check_out'] = 0;
				
				$end = date('Y-m-d',strtotime($value['end'].' '.' +1 days'));

				$this->db->select('boarding_schedule.start,boarding_schedule.end');
				$this->db->group_by('date(boarding_schedule.end)');
				$this->db->where(array('start'=>$value['end']));
				$r = $this->db->get_where('boarding_schedule',array('size_priority >='=>$size_priority))->row_array();
				if(empty($r))
				{
					$temp = $r;
					$temp['start'] = $value['end'];
					unset($temp['end']);

					$temp['checked_in'] = 0;
					$temp['check_out'] = 1;
					$temp['current_capacity'] = $value['current_capacity'];
					$temp['orginal_capacity'] = $value['orginal_capacity'];

					if($value['current_capacity'] == $value['orginal_capacity'])
					{
							$temp['booking_status'] = 'red';
					}
					else
					{
						if( $value['current_capacity'] == 0){
							
							$temp['booking_status'] = 'green';
							//$temp['available_time'] = $availableTime;
							
						}
						else
						{
							$temp['current_capacity'] = $value['orginal_capacity'] - $value['current_capacity'];
							$temp['booking_status'] = 'yellow';
						}
					}

					array_push($return,$temp);

				}


			}



			

		}
		// pr($return);
		// exit;
		return $return;
	}



	function checkBoardingifAvailable($appuser,$data,$shop_no,$size_priority)
	{
	    $result = $this->db->query("select boarding_inventory.* from boarding_inventory where id not in (SELECT boardings.inventory from boardings where boardings.status != 'Cancel' and boardings.inventory_type = 'boarding' and category = 'boarding' and boardings.start_date < '".$data['end_date']."' and boardings.end_date > '".$data['start_date']."') and size_priority >= '".$size_priority."' order by size_priority asc limit 1")->row_array();

	    if(!empty($result))
	    {
	    	return false;
	    }
	    else
	    {
	    	return true;
	    }
		
	}


	function checkStartDateBoarding($appuser,$data,$shop_no,$size_priority)
	{
		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date = date('Y-m-d',strtotime($data['end_date']));

		$b = $this->db->get_where('boarding_schedule',array('date(start)'=>$start_date,'size_priority >='=>$size_priority))->row_array();
		if(empty($b))
		{
			return $start_date;
		}
		$date1=date_create($end_date);
		$date2=date_create($start_date);
		$diff=date_diff($date1,$date2);
		$difference = $diff->format('%d');

		if($diff > 0)
		{
			 //pr($difference);
			for($i=1;$i<$difference;$i++)
			{
				$newDate = date('Y-m-d',strtotime($start_date.' +'.$i.'days'));
				//pr($newDate);
				//$endDate = date('Y-m-d',strtotime($newDate.' +1 days'));
				$b = $this->db->get_where('boarding_schedule',array('date(start)'=>$newDate,'size_priority >='=>$size_priority))->row_array();
	    		//pr($b);
	    		if(empty($b))
	    		{
	    			return $newDate;
	    		}

			}
		}
		
		return false;


	}

	function checkEndDateBoarding($appuser,$data,$shop_no,$size_priority)
	{

		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date = date('Y-m-d',strtotime($data['end_date']));

		$b = $this->db->get_where('boarding_schedule',array('date(end)'=>$end_date,'size_priority >='=>$size_priority))->row_array();
		if(empty($b))
		{
			return $end_date;
		}
		$date1=date_create($end_date);
		$date2=date_create($start_date);
		$diff=date_diff($date1,$date2);
		$difference = $diff->format('%d');

		if($diff > 0)
		{
			 //pr($difference);
			for($i=2;$i<$difference+1;$i++)
			{
				$newDate = date('Y-m-d',strtotime($start_date.' +'.$i.'days'));
				//pr($newDate);
				//$endDate = date('Y-m-d',strtotime($newDate.' +1 days'));
				$b = $this->db->get_where('boarding_schedule',array('date(end)'=>$newDate,'size_priority >='=>$size_priority))->row_array();
	    		//pr($b);
	    		if(empty($b))
	    		{
	    			return $newDate;
	    		}

			}
		}
		
		return false;
	}

	function getSingleAppointmentId($id)
	{
		//get single appointment infos
		$this->db->select('appointments.* ,');
		$this->db->order_by('appointments.id','desc');
		$query = $this->db->get_where('appointments',array('appointments.id'=>$id))->row_array();
		
		if(!empty($query))
		{
			
			$query = groomingTimeConverter($query);

			
			
			$query['petName'] = $this->Management_model->getPetName($query['pet_id']);
			$query['username'] = $this->Management_model->getUsername($query['app_user_id']);
			 $query['spayed'] ='';
			 $query['description'] = '';
            $data = $this->db2->select('spayed,description')->get_where('pets',array('id'=>$query['pet_id']))->row_array();
            if(!empty($data)){
                $query['spayed'] =$data['spayed'];
                $query['description'] = $data['description'];
            }
		}

		return $query;
	}
	function getPetSize()
    {
        $result = $this->db2->query('SHOW COLUMNS FROM pets WHERE field="size"')->row_array();
        $enum = substr($result['Type'], 6, -2);
        $values = explode("','", $enum);
        
        return $values;
    }
     function getBreedTypeWithSelected($type,$id)
    {
        //$this->db2 = $this->load->database('main', TRUE);
        $pets = $this->db2->get_where('breeds',array('type'=>$type,'is_deleted'=>0))->result_array();
        //$app_users = $this->db2->get_where('app_users',array('id'=>$id))->row_array();
        $string = '';
        if(!empty($pets))
        {
            foreach ($pets as $key => $value) {
                if($value['id'] == $id)
                    $string .= '<option value="'.$value['id'].'" data-type="'.$value['type'].'" selected="selected">'.$value['name'].'</option>';    
                else
                    $string .= '<option value="'.$value['id'].'" data-type="'.$value['type'].'">'.$value['name'].'</option>';
            }   
        }
        
        return $string;
    }
    function getEmojiForDropDown()
    {
        //$this->db2 = $this->load->database('main', TRUE);
        return $this->db2->get_where('behaviors',array('is_deleted'=>0))->result_array();

    }
}