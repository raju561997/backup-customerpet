<?php

Class Pdf_Model extends CI_Model{
    function __construct()
        {
            parent::__construct();
            $this->load->library("Pdf");
        }
	public function printmeasurement($data){
    $pdf = new PDF();
    // pr($data);exit;
    $pdf->AddPage();
    $title = 'Measurements';
    $pdf->SetTitle($title);

  	$txt = 'Measurements';
    $pdf->SetFont('Arial','B',14);
    $pdf->Text(95,10,$txt,'C');
    $pdf->SetXY(5,15);
    $txt = 'Name:';
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(16,8,$txt,0,0,'');

    $name = strtoupper($data['customerdetails']['name']);
    $pdf->SetFont('Arial','',13);
    $pdf->Cell(45,8,$name,0,1,'L');

    // $pdf->SetXY(75,15);
    // $txt = 'Phone number:';
    // $pdf->SetFont('Arial','B',13);
    // $pdf->Cell(40,8,$txt,0,0,'');

    // $phone = $data['customerdetails']['mobile_no'];
    // $pdf->SetFont('Arial','',13);
    // $pdf->Cell(40,8,$phone,0,1,'C');

    $pdf->SetXY(5,30);
    $txt = 'Sr No';
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(15,8,$txt,"B,L,T",0,'');

    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(45,8,'Title',1,0,'C');

    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(110,8,'Description',1,0,'C');


    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(30,8,'Order No',1,0,'C');
    //$pdf = new PDF();

    // $pdf->AddPage();

    // $pdf->SetXY(10,5);

    // $pdf->SetLineWidth(0.5);

    // $pdf->Cell(190,10,'',0,1,'C');

    // // $pdf->Line(200,15,15,15);

    // $pdf->Ln(7);

    // $pdf->SetX(25);

    // $pdf->SetFont('Arial','B',14);
    // $txt = 'Tailor App';
    // $pdf->Cell(160,8,$txt,1,1,'C');

    // $pdf->SetXY(35,20);

    // $txt = 'Name:';
    // $pdf->SetFont('Arial','B',13);
    // $pdf->Cell(15,10,$txt,"T,L B",0,'');

    // $pdf->SetFont('Arial','',13);
    // $pdf->Cell(145,10, $data['customerdetails']['name'],"R,T,B",1,'');

    // $pdf->SetXY(35,30);
    // $txt = 'Phone Number:';
    // $pdf->SetFont('Arial','B',13);
    // $pdf->Cell(40,10,$txt,"L,B",0,'');

    // $pdf->SetFont('Arial','',13);
    // $pdf->Cell(65,10,$data['customerdetails']['mobile_no'],'B',0,'');

   	$xyz = 38;$height = 8;$i=1;
    foreach($data['data'] as $data){
   		$pdf->SetXY(5,$xyz);
    	$pdf->SetFont('Arial','',12);
      $title = $data['title'];
      $description = trim($data['description']);
      $description = trim(preg_replace('/\s\s+/', ', ', $description));
      // pr($description);exit;
      // $pdf->MultiCell(50,8,$title,"T,B,L,R",'C');
      // $description = trim($data['description']);

      $multiple2 = ceil(strlen($description)/70);
      $x = ($height*$multiple2);

      $xyz += $x;
      
      $pdf->SetFont('Arial','',12);
      $pdf->Cell(15,$x,$i,"T,L B",0,'C');  

      $pdf->Cell(45,$x,$title,"T,B,L,R",0,'C');

      $pdf->SetX(175);
      $pdf->Cell(30,$x,$data['order_number'],"T,B,L,R",0,'C');

      $pdf->SetX(65);
      $pdf->SetFont('Arial','',12);
      // $description = trim($data['description']);
      $pdf->MultiCell(110,8,$description,"T,B,L,R",'C');
     
     
     
      $i++;

    }
    $pdf->Output();
  }

    function printInvoice($data){
      $data = $data['order'];
      $pdf = new PDF();
      $pdf->AddPage();
      $title = 'Invoice';
      $pdf->SetTitle($title);
      $pdf->SetXY(10,5);
      $pdf->SetLineWidth(0.6);
      $pdf->Cell(190,10,'',0,1,'C');

      $pdf->Ln(7);

      $pdf->SetX(25);

      $pdf->SetFont('Arial','B',17);
      $txt = '';
      $pdf->Cell(35,12,$txt,'T',0,'C');

      $txt = 'Fashion Makers';
      $pdf->Cell(90,12,$txt,'T',0,'C');
      
      $pdf->SetFont('Arial','B',13);
      $txt = 'Bill No:';
      $pdf->Cell(16,12,$txt,'T',0,'C');
      
      $pdf->SetFont('Arial','',13);
      $txt = $data['id'];
      $pdf->Cell(19,12,$txt,'T',1,'L');

      $pdf->Line(90,32,120,32); 

      $pdf->SetX(25);

      $txt = 'Name:';
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(18,10,$txt,"L",0,'C');

      $pdf->SetFont('Arial','',13);
      $pdf->Cell(142,10,strtoupper($data['customer_name']['name']),"R",1,'');

      $pdf->SetX(25);
      $txt = 'Phone:';
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(20,10,$txt,"L,B",0,'C');
      
      $pdf->SetFont('Arial','',13);
      $pdf->Cell(80,10,$data['customer_name']['mobile_no'],'B',0,'');

      $txt = 'Date:';
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(14,10,$txt,'B',0,'');

      $pdf->SetFont('Arial','',13);
      $pdf->Cell(46,10,$data['delivery_date'],",RB",1,'');

      $pdf->SetX(25);

      $pdf->SetX(25);

      $txt = 'Sr No';
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(16,8,$txt,1,0,'');
      
      $pdf->Cell(79,8,'Praticular',1,0,'C');

      $pdf->Cell(25,8,'Quantity',"B,R,T",0,'C');

      $pdf->Cell(40,8,'Amount',"B,R,T",1,'C');

      $y = 70;
      $j = 0;
      $i = 1;

      foreach($data['product'] as $data1){
        $pdf->SetX(25);
        $cut = strripos(trim($data1) ,' ');

        $bdtext1 = substr($data1,0,$cut);
        $bdtext2 = substr($data1,$cut);

        $cut1 = strrpos(trim($bdtext1) ,'(');
        $quan = substr($bdtext1,$cut1+1);
        $particular = substr($bdtext1,0,$cut1);

        $cut2 = strrpos(trim($quan) ,')');
        $quan2 = substr($quan,0,$cut2); 

        $txt = $i;
        $pdf->SetFont('Arial','',13);
        $pdf->Cell(16,8,$txt,"L",0,'C');

        $name = explode(' ', $data1);

        $pdf->SetFont('Arial','',13);
        $pdf->Cell(79,8,' '.$particular,"L,R",0,'L');

        $j += $quan2;

        $pdf->SetFont('Arial','',13);
        $pdf->Cell(25,8,$quan2,"R",0,'C');

        $pdf->SetFont('Arial','',13);
        $pdf->Cell(40,8,$bdtext2,"R",1,'C');

        $i++;
        $y+= 8;

      }

      // $pdf->SetX(25);
      // $pdf->Cell(160,80,'','LR',1,'C');

      $pdf->SetXY(25,165);
      $txt = '';
      $pdf->SetFont('Arial','',13);
      $pdf->Cell(16,8,$txt,1,0,'C');

      
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(79,8,'Total',1,0,'C');

      $pdf->Cell(25,8,$j,1,0,'C');

      $pdf->Cell(40,8,$data['total_cost'],1,0,'C');


      $pdf->Line(25,22,25,166); 

      $pdf->Line(41,55,41,166); 

      $pdf->Line(120,55,120,166); 

      $pdf->Line(145,55,145,166);      

      $pdf->Line(185,22,185,166);    

      $pdf->Output();

        
    }


     function printdashboardorders($data){
      // pr($data);exit;
   
      $pdf = new PDF();
      $pdf->AddPage();
      $title = 'Orders';
      $pdf->SetTitle($title);
      $pdf->SetXY(10,5);
      $pdf->SetX(10);

      $txt = 'Orders';
      $pdf->SetFont('Arial','B',14);
      $pdf->Text(95,10,$txt,'C');

       $pdf->SetY(15);


      $txt = 'Sr No';
      $pdf->SetFont('Arial','B',13);
      $pdf->Cell(15,8,$txt,1,0,'');
      
      $pdf->Cell(45,8,'Name',1,0,'C');

      $pdf->Cell(30,8,'Delivery Date',"B,R,T",0,'C');

      $pdf->Cell(75,8,'Products',"B,R,T",0,'C');
      $pdf->Cell(30,8,'Cost',"B,R,T",1,'C');


      $xyz = 38;$height = 8;$i=1;
    foreach($data as $data){
    
      $pdf->SetFont('Arial','',12);
      
      $strlen =  strlen($data['customer_name']['name']);
      if($strlen > 22){
        $str = strtok($data['customer_name']['name'], " ");
        $title = ucfirst($str);
      }else {
        $title = ucfirst($data['customer_name']['name']);
      }
      // pr($strlen);exit;
        // strtok($value, " ");
      $product_name = trim($data['product_name']);
      $product_name = trim(preg_replace('/\s\s+/', ', ', $product_name));
    
      $multiple2 = ceil(strlen($product_name)/30);
      $x = ($height*$multiple2);
      // pr($x);
      $xyz += $x;
      
      $pdf->SetFont('Arial','',12);
      $pdf->Cell(15,$x,$i,"T,B,L,R",0,'C');  

      $pdf->Cell(45,$x,$title,"T,B,L",0,'C');
      // $pdf->SetX(60);
      $pdf->Cell(30,$x,$data['delivery_date'],"T,B,L,R",0,'C');
      $pdf->SetX(175);
      $pdf->Cell(30,$x,$data['total_cost'],"T,B,L,R",0,'C');

       $pdf->SetX(100);
      $pdf->SetFont('Arial','',12);
      // $description = trim($data['description']);
      $pdf->MultiCell(75,8,$product_name,"T,B",'C');
     
     
     
      $i++;

    }
      // exit;
      $pdf->Output();

        
    }

    public function downloadContract($data){
      // pr($data);exit;
      $pdf = new PDF();
      //$pdf->AddFont('newcour','','cour.php');
      $pdf->AddPage();
      $pdf->SetAutoPageBreak('true','1');
      $title = '2nd Form';
      //$pdf->SetTitle($title);
      $today   =  date("d.m.Y");
      $pdf->SetFillColor(204, 220, 255);


      //$dae = Date('Y-m-d', $today);

      $pdf->SetFont('Arial','B',12);
      $pdf->SetLineWidth(0.5);
      // $pdf->SetXY(5,5);
      $txt = $data['shop_Data']['shopname'];
      $pdf->Cell(190,5,$txt,0,1,'C');
      
      $pdf->Cell(190,5,$data['shop_Data']['address'],0,1,'C');

      $txt = $data['shop_Data']['city'].', '.$data['shop_Data']['state'].' '.$data['shop_Data']['postal_code'];
      $pdf->Cell(190,5,$txt,0,1,'C');
      $shop_no = format_telephone($data['shop_Data']['phone_number']);
      $pdf->Cell(190,5,$shop_no,0,1,'C');

      $pdf->Ln(10);
      
      $pdf->SetFont('Arial','B',12);
      $pdf->SetLineWidth(0.5);

      $txt = 'Customer:';
      $pdf->Cell(60,6,$txt,'T,L',0,'L',1);
      
      $txt = 'Phone:';
      $pdf->Cell(60,6,$txt,'T',0,'L',1);

      $txt = 'Address:';
      $pdf->Cell(70,6,$txt,'T,R',1,'L',1);



      $pdf->SetFont('Arial','',12);

      $txt = $data['appUser'][0]['username'];
      $pdf->Cell(60,5,$txt,'L',0,'L',1);
      

    
      $Phone = format_telephone($data['appUser'][0]['phone']);
      $pdf->Cell(60,5,$Phone,'',0,'L',1);

      $txt ='';
      $txt = $data['appUser'][0]['address'];
      $pdf->Cell(70,5,$txt,'R',1,'L',1);



      $txt = '';
      $pdf->Cell(60,5,$txt,'L',0,'L',1);
      
      
      if(!empty($Phone)){
          $txt = '(primary)';
      }
      $pdf->Cell(60,5,$txt,'',0,'L',1);

      $txt = $data['appUser'][0]['city'].', '.$data['appUser'][0]['state'].' '.$data['appUser'][0]['postal_code'];
      $txt = trim("$txt",",");
      $pdf->Cell(70,5,$txt,'R',1,'L',1);

      $pdf->SetFont('Arial','B',12);

      $txt = 'Email:';
      $pdf->Cell(60,5,$txt,'L',0,'L',1);

      $pdf->SetFont('Arial','',12);


      $mobile = format_telephone($data['appUser'][0]['phone']);
      $pdf->Cell(60,5,$mobile,'',0,'L',1);

      $txt = '';
      $pdf->Cell(70,5,$txt,'R',1,'L',1);

     
      $pdf->Cell(60,5,$data['appUser'][0]['email'],'L,B',0,'L',1);
      
      
      if(!empty($mobile)){
          $txt = '(Mobile)';
      }
      $pdf->Cell(60,5,$txt,'B',0,'L',1);

      $txt = '';
      $pdf->Cell(70,5,$txt,'R,B',1,'L',1);

      $pdf->Ln(5);

      $pdf->SetFont('Arial','B',12);

      $txt = 'Pet:';
      $pdf->Cell(45,8,$txt,'T,L,R,B',0,'C');
      
      $txt = 'DOB:';
      $pdf->Cell(32,8,$txt,'TRB',0,'C');

      $txt = 'Breed:';
      $pdf->Cell(58,8,$txt,'TRB',0,'C');

      // $txt = 'Sex:';
      // $pdf->Cell(32,8,$txt,'TRB',0,'C');
      
      $txt = 'Color:';
      $pdf->Cell(55,8,$txt,'T,R,B',1,'C');




        //horizontal line
      //Customer
      $pdf->SetLineWidth(0.3);
      $pdf->SetDash(0.1,0.1);
      $pdf->Line(32,45,11,45);
      
      //Phone line
      $pdf->Line(85,45,71,45);

      //Address line
      $pdf->Line(148,45,131,45);

      //Email line
      $pdf->Line(24,60.8,11,60.8);

      //pet line
      $pdf->Line(37,77,28,77);

      //dob line
      $pdf->Line(77,77,65,77);

      //breed line
      $pdf->Line(125,77,107,77);

      // //sex line
      // $pdf->Line(143,77,133,77);

      //color line
      $pdf->Line(180,77,165,77);




      $pdf->SetLineWidth(0.1);
      $pdf->SetDash(0.1,0.1); //5mm on, 5mm off
      //customer line
      $pdf->Line(68,40,68,66); 

      //phone line
      $pdf->Line(120,40,120,66);











      $pdf->SetFont('Arial','',13);

      foreach ($data['pet_id'] as $key => $value) {
          $pet = $this->Entry_model->getPetsViewDetails($value);

          $txt = $pet['name'];

          if(strtolower($pet['gender']) == 'male'){
              $txt .= ' (M)';
          }elseif(strtolower($pet['gender']) == 'female'){
              $txt .= ' (F)';
          }
          $pdf->Cell(45,8,$txt,',LR',0,'C');
          
          $txt = '';
          if(!empty($pet['birth_date'])){ 
              $txt = date(get_option('date_format'), strtotime($pet['birth_date']));
          }
          
          $pdf->Cell(32,8,$txt,'R',0,'C');

          $txt = ucwords($pet['bread']);
          $pdf->Cell(58,8,$txt,'R',0,'C');

          // $txt = ucwords($pet['gender']);
          // $pdf->Cell(32,8,$txt,'R',0,'C');
          
          $txt = ucwords($pet['color']);
          $pdf->Cell(55,8,$txt,'R',1,'C');
      }

      $pdf->Cell(45,0,'',',B',0,'C');
      $pdf->Cell(32,0,'','B',0,'C');
      $pdf->Cell(35,0,'','B',0,'C');
      $pdf->Cell(32,0,'','B',0,'C');
      $pdf->Cell(46,0,'','B',1,'C');
      

      $pdf->Ln(5);

      

      $pdf->SetFont('Arial','B',12);
      $txt = 'Terms';
      $pdf->Cell(190,12,$txt,0,1,'C');

      
      $pdf->SetFont('Arial','',12);

      // echo $data['contractData']['content'];
      // $txt = $data['contractData']['content'];
      // $txt = str_replace("&nbsp;", " " ,$data['contractData']['content']);
      // $txt = str_replace( '<li>', "0xE2 0x80 0xA2;" , $txt );
      // $txt = str_replace( '</li>', "\n" , $txt );
      // $txt = str_replace( '</ul>', "" , $txt );
      $txt = strip_tags($data['contractData']['content'],'<b><u><i><br><p><ul></ol><li>');
      $txt =  str_replace("&nbsp;", " " ,$txt);
      // $txt =  str_replace("Â  Â ", " " ,$txt);
      $pdf->WriteHTML(trim($txt));
      // $pdf->MultiCell(190,5,$txt,0,1);
      // $txt = $data['contractData']['content'];

      // $uls = preg_match_all('/<ul>(.*?)<\/ul>/s', $txt, $matches);
      // // print_r($matches[1]);
      // preg_match_all('/<li>(.*?)<\/li>/s', $matches[1][0], $lis);
      // foreach ($lis as $key => $value) {
      //     // echo "<br>1.";
      //     // print_r($value);
      //     $array = $value;

      // }
      // // pr($array);exit;
      // $column_width = $this->w-30;
      // $z = $this->x;


      
      // // $new = array($array);

      // $blt_array['text'] = $array;

      // $blt_array['bullet'] =  chr(149);
      // $blt_array['margin'] = ' ';
     
      //  $bak_x = $this->x;
      //  for ($i=0; $i<count($blt_array['text']); $i++)
      // {
      //     // pr($blt_array['text']);
      //     //Get bullet width including margin
      //     // $blt_width = $this->GetStringWidth($blt_array['bullet'] . $blt_array['margin'])+$pdf->cMargin*2;
          
      //     // SetX
      //     // $this->SetX($bak_x);
          
      //    // pr($blt_width);exit;
          
      //     //Output bullet
      //     $pdf->Cell(5,$h,$blt_array['bullet'] . $blt_array['margin']);
      //     // $new1 = strip_tags($blt_array['text'],'<b><u><i>');
      //     //Output text
      //     // $this->MultiCell($w-$blt_width,$h,$blt_array['text'][$i]);
      //      $pdf->MultiCell($w-5,$h,strip_tags($blt_array['text'][$i]));

          
          
      //     //Insert a spacer between items if not the last item
        
          
      //     //Increment bullet if it's a number
      //     if (is_numeric($blt_array['bullet']))
      //         $blt_array['bullet']++;
      // }
      // $this->x= $bak_x;
      // pr($bak_x);exit;
      // // $newArray = $this->MultiCellBltArray($column_width-$this->x,6,$new);

      //  pr($newArray);exit;



      // pr($value);exit;
      // exit;
      // $ab = explode("<li>", $txt);
      // // pr($ab);
      //  // $column_width = $this->w-30;

      // $newArray = array();

      // foreach ($ab as $key => $value) {
      //     // pr($value);
          
      //     // pr($value);exit;
      //     if(preg_match('<li>', $value)){
      //         $array =array();
      //         // echo "dsf";
      //         $array = explode('</li>', $value);
      //         $newArray[] = $array[0];
              
      //         $temp = $array[1];
      //         // pr($array[1]);
      //         //  pr($array[0]);exit;
      //         if(!empty($temp)){
      //             $newArray[] = $array[1];
      //         }



      //     }else{
      //         $newArray[] = $value;
      //     }
              
              
              
              
      //         //  
      // }
      // pr($newArray);exit;









      // $pdf->WriteHTML(trim($txt));
      // $pdf->MultiCell(190,5,$txt,0,1);

       $pdf->Ln(20);

      $pdf->SetFont('Arial','B',12);
      $txt = 'Owner/Guardian';
      $pdf->Cell(55,9,$txt,'T',0,'L');
      
      $pdf->Cell(5,9,'',0,0,'L');

      $txt = 'Date';
      $pdf->Cell(23,9,$txt,'T',0,'C');

      $pdf->Cell(12,9,'',0,0,'L');

      $txt = 'Staff';
      $pdf->Cell(65,9,$txt,'T',0,'C');

      $pdf->Cell(5,9,'',0,0,'L');

      $txt = 'Date';
      $pdf->Cell(25,9,$txt,'T',0,'C');

      //$pdf->SetFillColor(204, 220, 255);
     // $pdf->Cell(0,10,'asch kes svsds sd  la aaas',0,1,'L');

      // $pdf->SetLineWidth(0.1);
      // $pdf->SetDash(0.1,0.1); //5mm on, 5mm off
      // //customer line
      // $pdf->Line(68,40,68,66); 

      // //phone line
      // $pdf->Line(120,40,120,66);

      //veterian line
      //$pdf->Line(68,91,68,133); 

      //phone line
      //$pdf->Line(129,91,129,133);

      // //horizontal line
      // //Customer
      // $pdf->SetLineWidth(0.3);
      // $pdf->SetDash(0.1,0.1);
      // $pdf->Line(32,45,11,45);
      
      // //Phone line
      // $pdf->Line(85,45,71,45);

      // //Address line
      // $pdf->Line(148,45,131,45);

      // //Email line
      // $pdf->Line(24,60.8,11,60.8);

      // //pet line
      // $pdf->Line(37,77,28,77);

      // //dob line
      // $pdf->Line(77,77,65,77);

      // //breed line
      // $pdf->Line(111,77,98,77);

      // //sex line
      // $pdf->Line(143,77,133,77);

      // //color line
      // $pdf->Line(183,77,171,77);




      $pdf->Output();
  }

}
