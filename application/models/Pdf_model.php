<?php

Class Pdf_Model extends CI_Model{
	function __construct()
    {
        parent::__construct();
        $this->load->library("Pdf");
    }

    public function downloadContract($data){
      // pr($data);exit;
      $pdf = new PDF();
      //$pdf->AddFont('newcour','','cour.php');
      $pdf->AddPage();
      $pdf->SetAutoPageBreak('true','1');
      $title = '2nd Form';
      //$pdf->SetTitle($title);
      $today   =  date("d.m.Y");
      $pdf->SetFillColor(204, 220, 255);


      //$dae = Date('Y-m-d', $today);

      $pdf->SetFont('Arial','B',12);
      $pdf->SetLineWidth(0.5);
      // $pdf->SetXY(5,5);
      $txt = ucfirst($data['shop_Data']['shopname']);
      $pdf->Cell(190,5,$txt,0,1,'C');
      
      $pdf->Cell(190,5,ucwords($data['shop_Data']['address']),0,1,'C');

      $txt = ucwords($data['shop_Data']['city']).', '.ucwords($data['shop_Data']['state']).' '.$data['shop_Data']['postal_code'];
      $pdf->Cell(190,5,$txt,0,1,'C');
      $shop_no = format_telephone($data['shop_Data']['phone_number']);
      $pdf->Cell(190,5,$shop_no,0,1,'C');

      $pdf->Ln(10);
      
      $pdf->SetFont('Arial','B',12);
      $pdf->SetLineWidth(0.5);

      $txt = 'Customer:';
      $pdf->Cell(60,6,$txt,'T,L',0,'L',1);
      
      $txt = 'Phone:';
      $pdf->Cell(60,6,$txt,'T',0,'L',1);

      $txt = 'Address:';
      $pdf->Cell(70,6,$txt,'T,R',1,'L',1);



      $pdf->SetFont('Arial','',12);

      $txt = $data['appUser'][0]['username'];
      $pdf->Cell(60,5,$txt,'L',0,'L',1);
      

    
      $Phone = format_telephone($data['appUser'][0]['phone']);
      $pdf->Cell(60,5,$Phone,'',0,'L',1);

      $txt ='';
      $txt = $data['appUser'][0]['address'];
      $pdf->Cell(70,5,$txt,'R',1,'L',1);



      $txt = '';
      $pdf->Cell(60,5,$txt,'L',0,'L',1);
      
      
      if(!empty($Phone)){
          $txt = '(primary)';
      }
      $pdf->Cell(60,5,$txt,'',0,'L',1);

      $txt = $data['appUser'][0]['city'].', '.$data['appUser'][0]['state'].' '.$data['appUser'][0]['postal_code'];
      $txt = trim("$txt",",");
      $pdf->Cell(70,5,$txt,'R',1,'L',1);

      $pdf->SetFont('Arial','B',12);

      $txt = 'Email:';
      $pdf->Cell(60,5,$txt,'L',0,'L',1);

      $pdf->SetFont('Arial','',12);


      $mobile = format_telephone($data['appUser'][0]['phone']);
      $pdf->Cell(60,5,$mobile,'',0,'L',1);

      $txt = '';
      $pdf->Cell(70,5,$txt,'R',1,'L',1);

     
      $pdf->Cell(60,5,$data['appUser'][0]['email'],'L,B',0,'L',1);
      
      
      if(!empty($mobile)){
          $txt = '(Mobile)';
      }
      $pdf->Cell(60,5,$txt,'B',0,'L',1);

      $txt = '';
      $pdf->Cell(70,5,$txt,'R,B',1,'L',1);

      $pdf->Ln(5);

      $pdf->SetFont('Arial','B',12);

      $txt = 'Pet:';
      $pdf->Cell(45,8,$txt,'T,L,R,B',0,'C');
      
      $txt = 'DOB:';
      $pdf->Cell(32,8,$txt,'TRB',0,'C');

      $txt = 'Breed:';
      $pdf->Cell(58,8,$txt,'TRB',0,'C');

      // $txt = 'Sex:';
      // $pdf->Cell(32,8,$txt,'TRB',0,'C');
      
      $txt = 'Color:';
      $pdf->Cell(55,8,$txt,'T,R,B',1,'C');




        //horizontal line
      //Customer
      $pdf->SetLineWidth(0.3);
      $pdf->SetDash(0.1,0.1);
      $pdf->Line(32,45,11,45);
      
      //Phone line
      $pdf->Line(85,45,71,45);

      //Address line
      $pdf->Line(148,45,131,45);

      //Email line
      $pdf->Line(24,60.8,11,60.8);

      //pet line
      $pdf->Line(37,77,28,77);

      //dob line
      $pdf->Line(77,77,65,77);

      //breed line
      $pdf->Line(125,77,107,77);

      // //sex line
      // $pdf->Line(143,77,133,77);

      //color line
      $pdf->Line(180,77,165,77);




      $pdf->SetLineWidth(0.1);
      $pdf->SetDash(0.1,0.1); //5mm on, 5mm off
      //customer line
      $pdf->Line(68,40,68,66); 

      //phone line
      $pdf->Line(120,40,120,66);











      $pdf->SetFont('Arial','',13);

      foreach ($data['pet_id'] as $key => $value) {
          $pet = $this->Entry_model->getPetsViewDetails($value);

          $txt = $pet['name'];

          if(strtolower($pet['gender']) == 'male'){
              $txt .= ' (M)';
          }elseif(strtolower($pet['gender']) == 'female'){
              $txt .= ' (F)';
          }
          $pdf->Cell(45,8,$txt,',LR',0,'C');
          
          $txt = '';
          if(!empty($pet['birth_date'])){ 
              $txt = date(get_option('date_format'), strtotime($pet['birth_date']));
          }
          
          $pdf->Cell(32,8,$txt,'R',0,'C');

          $txt = ucwords($pet['bread']);
          $pdf->Cell(58,8,$txt,'R',0,'C');

          // $txt = ucwords($pet['gender']);
          // $pdf->Cell(32,8,$txt,'R',0,'C');
          
          $txt = ucwords($pet['color']);
          $pdf->Cell(55,8,$txt,'R',1,'C');
      }

      $pdf->Cell(45,0,'',',B',0,'C');
      $pdf->Cell(32,0,'','B',0,'C');
      $pdf->Cell(35,0,'','B',0,'C');
      $pdf->Cell(32,0,'','B',0,'C');
      $pdf->Cell(46,0,'','B',1,'C');
      

      $pdf->Ln(5);

      

      $pdf->SetFont('Arial','B',12);
      $txt = 'Terms';
      $pdf->Cell(190,12,$txt,0,1,'C');

      
      $pdf->SetFont('Arial','',12);

      // echo $data['contractData']['content'];
      // $txt = $data['contractData']['content'];
      // $txt = str_replace("&nbsp;", " " ,$data['contractData']['content']);
      // $txt = str_replace( '<li>', "0xE2 0x80 0xA2;" , $txt );
      // $txt = str_replace( '</li>', "\n" , $txt );
      // $txt = str_replace( '</ul>', "" , $txt );
      // $txt = strip_tags($data['contractData']['content'],'<b><u><i><br><p><ul></ol><li>');
      // $txt =  str_replace("&nbsp;", " " ,$txt);
      // // $txt =  str_replace("Â  Â ", " " ,$txt);
      // $pdf->WriteHTML(trim($txt));
      $txt=strip_tags($data['contractData']['content']);
      $pdf->Cell(190,12,$txt,0,1,'L');
      // $pdf->MultiCell(190,5,$txt,0,1);
      // $txt = $data['contractData']['content'];

      // $uls = preg_match_all('/<ul>(.*?)<\/ul>/s', $txt, $matches);
      // // print_r($matches[1]);
      // preg_match_all('/<li>(.*?)<\/li>/s', $matches[1][0], $lis);
      // foreach ($lis as $key => $value) {
      //     // echo "<br>1.";
      //     // print_r($value);
      //     $array = $value;

      // }
      // // pr($array);exit;
      // $column_width = $this->w-30;
      // $z = $this->x;


      
      // // $new = array($array);

      // $blt_array['text'] = $array;

      // $blt_array['bullet'] =  chr(149);
      // $blt_array['margin'] = ' ';
     
      //  $bak_x = $this->x;
      //  for ($i=0; $i<count($blt_array['text']); $i++)
      // {
      //     // pr($blt_array['text']);
      //     //Get bullet width including margin
      //     // $blt_width = $this->GetStringWidth($blt_array['bullet'] . $blt_array['margin'])+$pdf->cMargin*2;
          
      //     // SetX
      //     // $this->SetX($bak_x);
          
      //    // pr($blt_width);exit;
          
      //     //Output bullet
      //     $pdf->Cell(5,$h,$blt_array['bullet'] . $blt_array['margin']);
      //     // $new1 = strip_tags($blt_array['text'],'<b><u><i>');
      //     //Output text
      //     // $this->MultiCell($w-$blt_width,$h,$blt_array['text'][$i]);
      //      $pdf->MultiCell($w-5,$h,strip_tags($blt_array['text'][$i]));

          
          
      //     //Insert a spacer between items if not the last item
        
          
      //     //Increment bullet if it's a number
      //     if (is_numeric($blt_array['bullet']))
      //         $blt_array['bullet']++;
      // }
      // $this->x= $bak_x;
      // pr($bak_x);exit;
      // // $newArray = $this->MultiCellBltArray($column_width-$this->x,6,$new);

      //  pr($newArray);exit;



      // pr($value);exit;
      // exit;
      // $ab = explode("<li>", $txt);
      // // pr($ab);
      //  // $column_width = $this->w-30;

      // $newArray = array();

      // foreach ($ab as $key => $value) {
      //     // pr($value);
          
      //     // pr($value);exit;
      //     if(preg_match('<li>', $value)){
      //         $array =array();
      //         // echo "dsf";
      //         $array = explode('</li>', $value);
      //         $newArray[] = $array[0];
              
      //         $temp = $array[1];
      //         // pr($array[1]);
      //         //  pr($array[0]);exit;
      //         if(!empty($temp)){
      //             $newArray[] = $array[1];
      //         }



      //     }else{
      //         $newArray[] = $value;
      //     }
              
              
              
              
      //         //  
      // }
      // pr($newArray);exit;









      // $pdf->WriteHTML(trim($txt));
      // $pdf->MultiCell(190,5,$txt,0,1);

      $pdf->Ln(20);

      $pdf->SetFont('Arial','B',12);
      $txt = 'Owner/Guardian';
      $pdf->Cell(55,9,$txt,'T',0,'L');
      
      $pdf->Cell(5,9,'',0,0,'L');

      $txt = 'Date';
      $pdf->Cell(30,9,$txt,'T',0,'C');

      $pdf->Cell(12,9,'',0,0,'L');

      // $txt = 'Staff';
      // $pdf->Cell(65,9,$txt,'T',0,'C');

      // $pdf->Cell(5,9,'',0,0,'L');

      // $txt = 'Date';
      // $pdf->Cell(25,9,$txt,'T',0,'C');

      //$pdf->SetFillColor(204, 220, 255);
     // $pdf->Cell(0,10,'asch kes svsds sd  la aaas',0,1,'L');

      // $pdf->SetLineWidth(0.1);
      // $pdf->SetDash(0.1,0.1); //5mm on, 5mm off
      // //customer line
      // $pdf->Line(68,40,68,66); 

      // //phone line
      // $pdf->Line(120,40,120,66);

      //veterian line
      //$pdf->Line(68,91,68,133); 

      //phone line
      //$pdf->Line(129,91,129,133);

      // //horizontal line
      // //Customer
      // $pdf->SetLineWidth(0.3);
      // $pdf->SetDash(0.1,0.1);
      // $pdf->Line(32,45,11,45);
      
      // //Phone line
      // $pdf->Line(85,45,71,45);

      // //Address line
      // $pdf->Line(148,45,131,45);

      // //Email line
      // $pdf->Line(24,60.8,11,60.8);

      // //pet line
      // $pdf->Line(37,77,28,77);

      // //dob line
      // $pdf->Line(77,77,65,77);

      // //breed line
      // $pdf->Line(111,77,98,77);

      // //sex line
      // $pdf->Line(143,77,133,77);

      // //color line
      // $pdf->Line(183,77,171,77);
      
      $txt = date(get_option('date_format'), strtotime(date("Y-m-d")))
      ;
      $pdf->SetFont('Arial','B',12);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetXY(72, 133);
      $pdf->Cell(200,0,$txt,0,0,'L');

      $txt = $data['appUser'][0]['username'];
      $pdf->SetFont('Arial','B',12);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetXY(15, 133);
      $pdf->Cell(200,0,$txt,0,0,'L');

      $txt = 'THIS IS A COMPUTER GENERATED CONTRACT AND DOES NOT REQUIRE A SIGNATURE';
      $pdf->SetFont('Arial','BI',10);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetXY(5, 160);
      $pdf->Cell(200,0,$txt,0,0,'C');


      // $pdf->Output();

      $filename = 'petContract_'.generateRandomStringContest(6).'.pdf';
      $pdf->Output(F,FCPATH."assets/pdf_contract/" .$filename); 
      return $filename;
  }

}