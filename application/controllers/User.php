<?php
// require(APPPATH.'libraries/REST_Controller.php');
// require(APPPATH . '/libraries/ImplementJwt.php');

//include Stripe PHP library
require 'vendor/autoload.php';
require APPPATH .'third_party/stripe/init.php';
// use Plivo\RestClient;

use Google\Cloud\Core\ServiceBuilder;

use google\appengine\api\cloud_storage\CloudStorageTools;
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
function __construct()
    {
        parent::__construct();
        $this->allow('login','fblogin','signup','fbsignup','forgotPassword','homePageDetails','getPetDetails','getAllPets','editNewPet','addNewPet','deletePet','addPetNote','editPetNote','deletePetNot','paymentMethods','getAllTransactions','addCard','makeDefaultCard','deleteCard','getActiveAppointments','cancelAppointment','editProfile','changePassword','getAllUserAndShopDetailBeforeAppointment','getUserDetails','removePic','updateAllNotifcation','getAllNotification','getAppointmentForNotification','getPetForNotification','changeShopNumber','getCalendarDatesByGroomerIdAndService','getGroomersByService','getWorkingTimeAndSlotForAppointment','addNewAppointment','getBestAvailableTimeForSlot','getBestAvailableTimeForSlot','getCalendarDatesForDayCare','addNewDayCare','getBoardingCalendar','validateBoarding','dashboard','getShopNameAjax','getsizebyweight','addveterinarian','addGroomerNote','addfiles','deletePetImage','deletePetNotes','getPetNotesAjax','editPetNotes','viewAppointment','getAllservices','viewBoarding','invoice','getAppUserPetsById','getAlltermservices','showMaintermServicePreview','showtermServicPreview','wallet','getCheckoutAppointments','viewCheckoutAppointment','viewCheckoutBoarding','moveAppointment','resetPassword','editPet','contract','addPetNotes','contractUpload','test','addNewPetData','addNewPet2','addVaccination','delete_vaccination',' index','check', 'payment_success', 'payment_error', 'help','downloadContract','pay','testing','paymentPage');

        $this->load->model('pdf_model');
		$this->load->model('GlobalApp_model');
        $this->load->model('Appuser_model');
        $this->load->model('Entry_model');
		$this->load->model('Management_model');
		$this->load->model('Payment_model');
	}

	public function testing()
	{
		echo 'ddd';
		// $stripe = new \Stripe\StripeClient(
		// 	'sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9'
		//   );
		//   $stripe->paymentMethods->all([
		// 	'customer' => 'cus_Ii695YfTLieYMk',
		// 	'type' => 'card',
		//   ]);
		// 	pr($stripe);
		$stripe = new \Stripe\StripeClient("sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9");
		// $ch = $stripe->charges->capture(
		// 	'ch_1I6fxbBF9FUEVqNaFCAiiX2B',
		// 	[],
		// 	['api_key' => 'sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9']
		//   );
		//   pr($ch);
		//   $ch = $stripe->charges->capture(
		// 	'ch_1I6fxbBF9FUEVqNaFCAiiX2B',
		// 	[],
		// 	['stripe_account' => 'acct_1I6XtKBF9FUEVqNa']
		//   );
		//   pr($ch);
			
		\Stripe\Stripe::setApiKey("sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9");
		$a = \Stripe\Customer::create(
		["email" => "person@example.edu"],
		["stripe_account" => "acct_1I6XtKBF9FUEVqNa"]
		);

		// Fetching an account just needs the ID as a parameter
		 \Stripe\Account::retrieve("acct_1I6XtKBF9FUEVqNa");
		 pr($a);
		// $stripe = array(
		// 	"secret_key"      => "sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9",
		// 	"publishable_key" => "pk_test_51I6XtKBF9FUEVqNarmzOzYYMjwQivW8v5t95n4HYa7oZpFBCKEKfg8S6Y8yQ4hW7Ow2OO9ZVKQsqGnzYr9CsqbBz00jGCO4E10"
		//   );
		  
		//   \Stripe\Stripe::setApiKey($stripe['secret_key']);
		// $stripe1 = new \Stripe\StripeClient(
		// 	'sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9'
		//   );
		//   $stripe1->paymentMethods->all([
		// 	'customer' => 'cus_Ii6GTe4ZC0lMwx',
		// 	'type' => 'card',
		//   ]);
		//   pr($stripe1); 	
		// \Stripe\Stripe::setApiKey('sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9');

		// $payment_method = \Stripe\PaymentMethod::create([
		// 'customer' => 'cus_Ip1wCoduF4ZvE9',
		// 'payment_method' => 'pm_1IDNseBF9FUEVqNa8c3ZEpOl',
		// ], [
		// 'stripe_account' => '{{CONNECTED_STRIPE_ACCOUNT_ID}}',
		// ]);
	  
		//   pr($payment_method);

		$stripe = new \Stripe\StripeClient(
			'sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9'
		  );
		  $stripe->paymentMethods->retrieve(
			'pm_1EUq6lJAJfZb9HEBD2w5v8TP',
			[]
		  );

		  pr($stripe);

	}

	public function paymentPage()
	{
		$data['header'] = FALSE;
		$data['_view']  = 'paymentPage';
		$data['footer'] = FALSE;
		$data['shopname'] = 'Groom Shop';
		$this->load->view('stripe/basetemplate',$data);
	}	

	public function pay($shop_id='0',$invoice){
		$invoice_no = get_decode($invoice);
		$shop_no =   get_decode($shop_id);
		$shop_detail = $this->Payment_model->getShopDetailByshopId($shop_no);
		$user_detail = $this->Payment_model->getUserDetailByInvoiceId($invoice_no);
		$pay_amount =  $this->Payment_model->getPayAmount($invoice_no);
		$allDetails = $this->Payment_model->invoice($shop_no,$invoice_no);
		if(empty($shop_detail) || empty($user_detail) || $pay_amount <= 0 || empty($allDetails) ){
			show_404();
		}
		// pr($user_detail);exit;
		// $stripe = new \Stripe\StripeClient(
		// 	'sk_test_51I6XtKBF9FUEVqNaZxn5RECA5IFHhm2wwhb7Ag779PLi64ru2zuBJHE5Ftbm2D8OGDECEGmQZNkYbUdifLUFQDxJ00W6Ojwim9'
		//   );
		//   $stripe->paymentMethods->all([
		// 	'customer' => 'cus_Ii6GTe4ZC0lMwx',
		// 	'type' => 'card',
		//   ]);
		//  pr($stripe);exit;	

		pr($allDetails);exit;
		$data['header'] = TRUE;
		$data['_view']  = 'payment';
		$data['footer'] = TRUE;
		$data['pay_amount'] = number_format((float)$pay_amount, 2, '.', '');
		$data['shop_detail'] = $shop_detail;
		$data['user_detail'] = $user_detail;
		$this->load->view('stripe/basetemplate',$data);
	}



	public function index()
	{
		$this->load->view('product_form');		
	}

	public function check()
	{
		// print_r($this->input->post());exit;
		//check whether stripe token is not empty
		if(!empty($_POST['stripeToken']))
		{
			//get token, card and user info from the form
			$token  = $_POST['stripeToken'];
			$name = $_POST['name'];
			$email = $_POST['email'];
			$card_num = $_POST['card_num'];
			$card_cvc = $_POST['cvc'];
			$card_exp_month = $_POST['exp_month'];
			$card_exp_year = $_POST['exp_year'];
			
			
			
			//set api key
			$stripe = array(
			  "secret_key"      => "sk_test_H71AOOHCoDMaglev1DekjvgT00HczinL6i",
			  "publishable_key" => "pk_test_aV6D6FeMYKDBjfXGNWCFPlGE008s1K1ioN"
			);
			
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			
			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $email,
				'source'  => $token
			));
			
			//item information
			$itemName = "Stripe Donation";
			$itemNumber = "PS123456";
			$itemPrice = 50;
			$currency = "inr";
			$orderID = "SKA92712382139";
			
			//charge a credit or a debit card
			$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemNumber,
				'metadata' => array(
					'item_id' => $itemNumber
				)
			));
			
			//retrieve charge details
			$chargeJson = $charge->jsonSerialize();

			//check whether the charge is successful
			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
			{
				//order details 
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");
			
				
				//insert tansaction data into the database
				$dataDB = array(
					'name' => $name,
					'email' => $email, 
					'card_num' => $card_num, 
					'card_cvc' => $card_cvc, 
					'card_exp_month' => $card_exp_month, 
					'card_exp_year' => $card_exp_year, 
					'item_name' => $itemName, 
					'item_number' => $itemNumber, 
					'item_price' => $itemPrice, 
					'item_price_currency' => $currency, 
					'paid_amount' => $amount, 
					'paid_amount_currency' => $currency, 
					'txn_id' => $balance_transaction, 
					'payment_status' => $status,
					'created' => $date,
					'modified' => $date
				);

				if ($this->db->insert('orders', $dataDB)) {
					if($this->db->insert_id() && $status == 'succeeded'){
						$data['insertID'] = $this->db->insert_id();
						$this->load->view('payment_success', $data);
						// redirect('Welcome/payment_success','refresh');
					}else{
						echo "Transaction has been failed";
					}
				}
				else
				{
					echo "not inserted. Transaction has been failed";
				}

			}
			else
			{
				echo "Invalid Token";
				$statusMsg = "";
			}
		}
	}

	public function payment_success()
	{
		$this->load->view('payment_success');
	}

	public function payment_error()
	{
		$this->load->view('payment_error');
	}

	public function help()
	{
		$this->load->view('help');
	}
	

   function test($name,$id){
	echo $name;
	echo $id;
   }

   function addVaccination()
   {
	   $this->form_validation->set_rules('medication_name', 'medication_name', 'required|trim');
	   $this->form_validation->set_rules('date_of', 'date_of', 'required');
	   $this->form_validation->set_rules('expiry_on', 'expiry_on', 'required');
	   if($this->form_validation->run()){
		   $data['medication_name'] = $_POST['medication_name'];
		   $data['date_of'] = date('Y-m-d H:i:s',strtotime($_POST['date_of']));
		   $data['expiry_on'] = date('Y-m-d H:i:s',strtotime($_POST['expiry_on']));
		   $data['shop_id'] =$this->session->userdata('shop_id');
		   $data['added_on'] = date('Y-m-d H:i:s');
		   $data['added_by_user'] = $this->session->userdata('id');
		   $data['added_by'] = 'customer';
		   $data['image'] = $_POST['add_vaccination_file'];
		   $data['pet_id'] = 0;
		   $this->db2->insert('pets_notes', $data);
		   $insert_id = $this->db2->insert_id();
		   if($insert_id > 0){
					$add_row = '<tr class="bounceInUp">
					<td class="text-truncate">'.$_POST['medication_name'].'</td>
					<td class="text-truncate">'.date('M d Y',strtotime($_POST['date_of'])).'</td>
					<td class="text-truncate">'.date('M d Y',strtotime($_POST['expiry_on'])).'</td>
					<td class="text-truncate"><span class="btn btn-sm round btn-outline-danger valid pets_notes_delete" data-pets_notes_id ="'.$insert_id.'" id="pets_notes_'.$insert_id.'"> Delete</span></td>
				</tr>';
				$msg = array('error' => 0, 'msg' => 'Vaccinations Record Added Successfully.' ,'row' => $add_row ,'vaccine_pet_id' => $insert_id);
		   }else
		   {
			 $msg = array('error' => 400, 'msg' => 'Vaccinations Record Not Added');
		   }
       
		   
		   
	   }else
	   {
		  $msg = array('error' => 0, 'msg' => validation_errors());
	   }
	   echo json_encode($msg);
   }

   function delete_vaccination()
   {
	  $this->form_validation->set_rules('id', 'Id', 'required|numeric');
	  if($this->form_validation->run()){
		if($this->db2->delete('pets_notes', array('id' => $_POST['id']))){
			$msg = array('error' => 0, 'msg' => 'Vaccination Record Deleted Successfully' ,'pets_notes_id' => $_POST['id']);
		}
	  }else
	  {
		 $msg = array('error' => 400,'msg' => validation_errors()); 
	  }
	   echo json_encode($msg);
   }
	

	//login
	function login()
	{
		$data = $this->data;
		if(!$this->session->userdata('id'))
		{
			if($this->input->post('isValidShop') == 1){
					$this->session->set_flashdata('error', 'Unable to login');
					// header('Location: '. INSTALL_FOLDER);
					$this->load->view('user/login');exit;
			}
			$this->form_validation->set_rules('emailORphone', 'emailORphone', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('shopno', 'shopno', 'required');
            if($this->form_validation->run())
            {
				$link=''; //link is sub dimain of shop
                $emailORphone =  $this->input->post('emailORphone');
				$password = $this->input->post('password');
				$shop_no = $this->input->post('shopno');
				if($userdetails = $this->Appuser_model->login($emailORphone, $password,$shop_no,$link)){
					$this->session->set_flashdata('success', 'Welcome, you have logged in successfully.');
					$userdetails['shop_no'] = $shop_no;
					$shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
					$userdetails['shop_id'] = $shop_id;
					$this->session->set_userdata($userdetails);
					redirect('user/dashboard');
				}else{
					$this->session->set_flashdata('error', 'Incorrect User Credentials');
					// header('Location: '. INSTALL_FOLDER);
					 $this->load->view('user/login');
				}
            }
            else
            {
				$this->load->view('user/login',$data);
				// header('Location: '. INSTALL_FOLDER);
			}
		}elseif(!empty($Token)){
					$emailORphone =  $this->input->post('emailORphone');
					$password = $this->input->post('password');
					$shop_no = $this->input->post('shopno');
					
					// $where = "phone='$emailORphone' OR email='$emailORphone' AND 'link'='$link'";
					$where = "phone='$emailORphone' OR email='$emailORphone'";
					$result	= $this->db2->get_where($this->table_name,$where)->row_array();

					
					if(empty($this->post('emailORphone'))) {
						$msg = array('err'=>1002, 'msg'=>'Please Enter Email Address or  Phone Number');
					}elseif(empty($this->post('password'))){
						$msg = array('err'=>1002, 'msg'=>'Please Enter Password');
					}
					// elseif(empty($result)){
					// 	$msg = array('err'=>102, 'msg'=>'Please Enter Shop Number');
					// }
					else
					{
						if($token = $this->Appuser_model->login($emailORphone, $password,$shop_no,$link))
						{
							$this->session->set_flashdata('success', 'Welcome, you have logged in successfully.');
							$msg = array('err'=>0, 'msg'=>'login success', 'token'=>$token);	
						}
						else
						{
							$msg = array('err'=>1002, 'msg'=>'login failed.');
						}
					}
					$this->response($msg);
		}
		else
		{
			// header('Location: '. INSTALL_FOLDER);
			 redirect('User/dashboard');
		}
	}
	function dashboard()
    {
		if(!empty($this->session->userdata())){
			//user Dashboard
			$data = $this->data;
			$data['header'] = TRUE;
			$data['_view'] = 'dashboard';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['parent_tab'] = 'dashboard';
			$data['active_tab'] = 'dashboard';
			$data['user']=$this->data['userdata'];
			$data['breadCrumbname']='Dashboard'; //Breadcrumb Name
			$this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
	}

	function contract()
    {
		if(!empty($this->session->userdata())){
			//user contract
			$data = $this->data;
			$data['header'] = TRUE;
			$data['_view'] = 'contract';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['active_tab'] = 'contract';
			$data['user']=$this->data['userdata'];
			$data['pet'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id']))->result_array();
			$this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
	}

	function contractUpload($id,$type)
    {
		if(!empty($this->session->userdata())){
			$data = $this->data;
			$data['id'] = $id;
			$data['header'] = true;
			$data['_view'] = 'contractUpload';
			$data['sidebar'] = true;
			$data['footer'] = true;
			$data['active_tab'] = 'contract';
			$data['pet'] = $this->Appuser_model->getPets($this->session->userdata('id'),$this->session->userdata('shop_no'),$data['id']);
			if($type=='Grooming'){
				$data['contract'] = $this->db->get_where('contracts',array('allow_type'=>$type))->row_array();
			}else{
				$data['contract'] = $this->db->get_where('contracts',array('allow_type'=>'Boarding, Daycare'))->row_array();
			}
			$this->load->view('user/basetemplate', $data);
		}else{
			$this->load->view('user/login');
		}
	}

	//download contract from customers
    public function downloadContract($id,$type) {
		// pr($this->input->post());exit;
        if($this->input->post())
		{
			
	        $this->form_validation->set_rules('allow_type', 'Type', 'required');
	       	$this->form_validation->set_rules('pet_id[]', 'Pet', 'required');

	        if ($this->form_validation->run()== TRUE) {
	        	$data = $this->input->post();
	        	
	        	$temparr = array('grooming','daycare','boarding');
	        	if(in_array(strtolower($data['allow_type']), $temparr)){
	        		$this->db->select('*');
					$this->db->from('contracts');
					$this->db->like('allow_type', $data['allow_type']);
					$data['contractData'] =  $this->db->get()->row_array();
	        	}else{
	        		$data['contractData'] = $this->db->get_where('contracts',array('id' => $data['allow_type']))->row_array();
	        	}

	        	if(!empty($data['contractData'])){
					if(empty($data['app_user_id'])){
						$pet1 = $this->Entry_model->getPetsViewDetails($data['pet_id'][0]);
						$data['appUser'] = $this->Management_model->getSingleUserData($pet1['app_user_id']);
					}else{
						$data['appUser'] = $this->Management_model->getSingleUserData($data['app_user_id']);
					}

					$data['shop_Data'] = $this->Entry_model->getShopdetailByphoneNo($this->session->userdata('shop_no'));

					if($filename=$this->pdf_model->downloadContract($data))
					{
						if($this->Appuser_model->contractUpload($data['pet_id'][0],$data['allow_type'],$filename)){
							$this->session->set_flashdata('success', 'Contract Details Uploaded successfully');
							redirect('user/contract');
							// $this->message->save_success(referrer());
						}
						
					}
				}else{
					$this->message->custom_error_msg(referrer(),'No active contract found for '.$data['allow_type'].'. Please create new contract (Management->Contracts) ');
				}
			}
			else 
	        {
	            $error = validation_errors();
	            $this->message->custom_error_msg(referrer(),$error);
	    	}
		}
		else 
        {
            $error = validation_errors();
            $this->message->custom_error_msg(referrer(),$error);
    	}
    }

    function logout()
    {
      	if(!empty($this->session->userdata())){
      		if($this->Appuser_model->logout($this->session->userdata('id'),$this->session->userdata('shop_no')))
	        {
	            $this->session->sess_destroy();
				// header('Location: '. INSTALL_FOLDER);
				redirect('User/login');
	        }
		}else{
			redirect('user/login');
		}
		
		// else{
	    //     $shop_no =  trim($this->uri->segment(1));
	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no)){
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));
		// 	}
	    //     $appuser = $this->GlobalApp_model->checkToken();
	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }
	    //     $this->db2->update('app_users',array('fcm_id' => ''),array('id'=>$appuser['id']));

	    //     if($this->Appuser_model->logout($appuser['id'],$shop_no))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'logout successfully','errcode' => 0));
	    //     }
	    // }
    }

    function getAllPets()
    {
        if(!empty($this->session->userdata())){
	        $data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'pets';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getAllPets'; 
	        $data['user']=$this->data;
			$data['pets'] = $this->Appuser_model->getAllPets($this->session->userdata('id'),$this->session->userdata('shop_no'));
	        $this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
		// else{
	    // 	$shop_no =  trim($this->uri->segment(1));

	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no)){
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));
		// 	}
	    //     $appuser = $this->GlobalApp_model->checkToken();

	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }

	    //     if($appuser['is_banned'] != 0)
	    //     {
	    //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	    //     }

	    //     if($result = $this->Appuser_model->getAllPets($appuser['id'],$shop_no))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'Details fetch successfully','errcode' => 0,'result'=>$result));
	    //     }
	    //     else
	    //     {
	    //         $this->response(array('error' => false,'message' => 'No Records Found','errcode' => 0));
	    //     }
	    // }
    }

    function paymentMethods()
    {

    	if(!empty($this->session->userdata())){
	        $data['header'] = TRUE;
	        $data['_view'] = 'paymentMethods';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'paymentMethods';
	        $data['user']=$this->data['userdata'];
	        $this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
    }

	function getAllTransactions(){
		if(!empty($this->session->userdata())){
	        $data['header'] = TRUE;
	        $data['_view'] = 'transactions';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getAllTransactions';
	        $this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
	}

    function getPetDetails()
    {
        if(!empty($this->session->userdata())){
			$data = $this->data;
			$id = $_GET['id'];
	        // $id = get_decode($id);
			$data['pet'] = $this->Appuser_model->getPetDetails($this->session->userdata('id'),$this->session->userdata('shop_no'),array('id'=>$id));
			$data['appointmentExpiry'] = $this->Appuser_model->appointmentExpiry($id);
			$data['count_appointment'] = $this->db->where(array('pet_id'=>$id,'app_user_id'=> $this->session->userdata('id')))->count_all_results('appointments'); 
			$data['count_daycare'] = $this->db->where(array('pet_id'=> $id,'app_user_id'=> $this->session->userdata('id'),'category'=>'daycare'))->count_all_results('boardings');
			$data['count_boarding'] = $this->db->where(array('pet_id'=> $id,'app_user_id'=> $this->session->userdata('id'),'category'=>'boarding'))->count_all_results('boardings');
	        $data['header'] = TRUE;
	        $data['_view'] = 'viewPet';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getAllPets';
			$data['veterinarian'] = $this->Management_model->getVeterinarian($id);
	        $data['groomNotes'] = $this->Management_model->getPetGroomerNotesNew($id);
			// $data['petNotes'] = $this->Management_model->getAllPetNotes($id,$this->session->userdata('shop_id'));
			$data['petNotes'] = $this->Entry_model->getpetNotesDetails($id,$this->session->userdata('shop_id'));
			// pr($data['petNotes']);exit;
            // $data['uploadedfiles'] =  $this->Management_model->getPetuploadedfiles($id);
        	$data['petBehavior'] = $this->Management_model->getPetBehaviorWithEmoji($id);
        	$data['petBehaviorNote'] = $this->Management_model->getPetBehavior($id);
			if(!empty($data['petBehaviorNote']))
			{
				$data['petBehaviorNote']['username'] = $this->Management_model->getGroomerName($data['petBehaviorNote']['user_id']);
			}
			// pr($data['petBehaviorNote']);exit;
        	// $data['appointments'] = $this->Management_model->getAllPetsAppointments($id);
			// $data['boardings'] = $this->Management_model->getAllPetsBoardings($id);
        	// $data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#00c0ef','Inprocess'=>'#f39c12','Checkout'=>'#4966aa');
			$data['medicationNotes'] =  $this->Management_model->getPetMedicationNotesNew($id);
			// pr($data['medicationNotes']);exit;
        	// $data['Pet_image'] = $this->Management_model->getGalleryImage($id);
        	// $data['allAppointments'] = array_merge($data['appointments'] , $data['boardings']);
			// $activeAppointment = $this->Management_model->getActiveAppointmentForSamePet($data['pet']['id']);
			// $activeBoarding = $this->Management_model->getActiveBoardingForSamePet($id);
			$data['appointments'] = $this->Management_model->getSamePetAppointments($id,$this->session->userdata('id'));
			$data['boarding'] = $this->Management_model->getSamePetBoarding($id,$this->session->userdata('id'));
			$data['lastAppointments'] = date("d/m/Y", strtotime($this->Management_model->lastAppointments($id,$this->session->userdata('id'))));
			$this->load->view('user/basetemplate', $data);
			
		}else{
			redirect('user/login');
		}

		// else{
        // 	$shop_no =  trim($this->uri->segment(1));

	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	    //     $appuser = $this->GlobalApp_model->checkToken();

	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }

	    //     if($appuser['is_banned'] != 0)
	    //     {
	    //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	    //     }

	    //     $data = $this->post();



	    //     if (!array_key_exists('id', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	    //     }

	    //     if($result = $this->Appuser_model->getPetDetails($appuser['id'],$shop_no,$data))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'Details fetch successfully','errcode' => 0,'result'=>$result));
	    //     }
        // }
    }

    function getUserDetails()
    {
    	if(!empty($this->session->userdata())){
	        $data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'profile';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['parent_tab'] = 'profile';
	        $data['active_tab'] = 'profile';
	        $data['user']= $this->Appuser_model->getUserDetails($this->session->userdata('id'),$this->session->userdata('shop_no'));
	        $data['breadCrumbname']='Edit User';
	        $data['breadCrumbParentname']='profile';
	        $data['parent_url'] = 'profile';
	        $data['curenturl']= 'profile';
	        $this->load->view('user/basetemplate', $data);
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $result = $this->Appuser_model->getUserDetails($appuser,$shop_no);
	        if($result)
	        {
	            $this->response(array('error' => false,'message' => 'Data Fetched Successfully','errcode' => 0,'result'=>$result));
	        }
		}
    }

    function getAllBreeds($type)
    {
    	if(!empty($this->session->userdata())){
	        $data = $this->Appuser_model->getAllBreeds($this->session->userdata('id'),$this->session->userdata('shop_no'));
	      
	    	$string = '';
	    	if(!empty($data))
	    	{
	    		foreach ($data['all_breeds'] as $key => $value) {
	    			if($value['type'] == $type){
	    				
		    			$string .= '<option value="'.$value['id'].'" data-type="'.$value['type'].'">'.$value['name'].'</option>';
		    		}
	    		}
	    		$msg = array('error' => true,'message' => 'All breeds','errcode' => 0,'result'=>$string);
	    	}
	    	else
	    	{
	    		$msg = array('error' => true,'message' => 'breeds','errcode' => 100);
	    	}
	    	echo json_encode($msg);
        }else{
	        $shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }
	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }
	        $result = $this->Appuser_model->getAllBreeds($appuser,$shop_no);
	        if($result)
	        {
	            $this->response(array('error' => false,'message' => 'Data Fetched Successfully','errcode' => 0,'result'=>$result));
	        }
	    }
    }

    function cancelAppointment()
    {
    	if(!empty($this->session->userdata())){
    		$data = $this->input->post();
	        if($data['category'] == 'appointment')
	        {
	            $appointment = $this->Appuser_model->getAppointmentbyId($data['id']);
	            // $payment = $this->Appuser_model->getPaymentDetailByAppointmentId($id,$this->session->userdata('registered_shop'),$this->session->userdata(),$data['category']);
	            // if($this->Appuser_model->cancelAppointment($this->session->userdata(),$data,$this->session->userdata('shop_no'),$appointment,array('id'=>NULL),$payment))
	            // {
	            //     $this->session->set_flashdata('success', 'Appointment canceled successfully');
	            // }
	           
	            if($this->Management_model->changeAppointmentStatus($appointment['id'],'Cancel','credit'))
	            {
	                $this->session->set_flashdata('success', 'Appointment canceled successfully');
	            }
	            
	        }
	        else
	        {
	            $boarding = $this->Appuser_model->getBoardingbyId($data['id']);
	            // $payment = $this->Appuser_model->getPaymentDetailByAppointmentId($id,$this->session->userdata('registered_shop'),$this->session->userdata(),$data['category']);
	            // if($this->Appuser_model->cancelBoarding($this->session->userdata(),$data,$this->session->userdata('shop_no'),$appointment,array('id'=>NULL),$payment))
	            // {
	            //     $this->session->set_flashdata('success', 'Appointment canceled successfully');
	            // }
	            if($this->Management_model->changeBoardingStatus($boarding['id'],'Cancel','credit'))
	            {
	                $this->session->set_flashdata('success', 'Appointment canceled successfully');
	            }
	        } 
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        }

	        if (!array_key_exists('category', $data)) {
	            $this->response(array('error' => true, 'message' => 'require category','errcode' => 400));
	        }

	        if($data['category'] == 'appointment')
	        {

	            $appointment = $this->Appuser_model->getAppointmentbyId($data['id']);
	            //pr($appointment);exit;
	            if(empty($appointment))
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment does not exist','errcode' => 400));
	            }


	            if($appointment['can_be_canceled'] == 1)
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment cannot cancel. ','errcode' => 400));
	            }

	            if($appointment['status'] == 'refunded')
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment has been canceled already. ','errcode' => 400));
	            }

	            if($appointment['status'] == 'completed')
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment that has been completed cannot be canceled ','errcode' => 400));
	            }

	            $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	            $payment = $this->Appuser_model->getPaymentDetailByAppointmentId($data['id'],$shop_id,$appuser,$data['category']);


	            if($this->Appuser_model->cancelAppointment($appuser,$data,$shop_no,$appointment,array('id'=>NULL),$payment))
	            {
	                    $this->response(array('error' => false,'message' => 'Appointment canceled successfully','errcode' => 0));
	            }

	          
	        }
	        else
	        {
	            $boarding = $this->Appuser_model->getBoardingbyId($data['id']);

	            if(empty($boarding))
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment does not exist','errcode' => 400));
	            }


	            if($boarding['can_be_canceled'] == 1)
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment cannot cancel. ','errcode' => 400));
	            }

	            if($boarding['status'] == 'refunded')
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment has been canceled already. ','errcode' => 400));
	            }

	            if($boarding['status'] == 'completed')
	            {
	                $this->response(array('error' => true, 'message' => 'Appointment that has been completed cannot be canceled ','errcode' => 400));
	            }

	            $shop_id = $this->GlobalApp_model->getShopIdByShopPhoneNumber($shop_no);
	            $payment = $this->Appuser_model->getPaymentDetailByAppointmentId($data['id'],$shop_id,$appuser,$data['category']);


	            /*---------------------------
	                Code for cancel appoinment directly (without refund)
	            ---------------------------*/

	            if($this->Appuser_model->cancelBoarding($appuser,$data,$shop_no,$boarding,array('id'=>NULL),$payment))
	            {
	                    $this->response(array('error' => false,'message' => 'Appointment canceled successfully','errcode' => 0));
	            }

	        }    
	    }   
    }

    function getAppointmentGallery()
    {
    	if(!empty($this->session->userdata())){
	        $id = get_decode($id);
	        $id == TRUE || $this->message->norecord_found(referrer());
	        $data = $this->data;
	        // $data = $this->get();
	        $data = $_GET;
	        $data['company_name'] = get_option('company_name');
	        $data['image_logo'] = '';
	        // $company_logo = get_option('company_logo');
	        // if(!empty($company_logo)){
	        //     $image_logo = loadCloudImage(SHOP_IMAGES.$company_logo);
	        //     if ($_SERVER['SERVER_NAME'] == 'localhost') {
	        //         $image_logo = "http://lh3.googleusercontent.com/fOWH0cp4--MXcctiinE9D6PI4VnxoOkXd4ImT_6Bt6bx_8wLm_GmEG5Ri7TeOnEpX9rG1c9uv9rIPhm1-VTtF580CBJW9Cp21T2ERw=s100";
	        //     }
	        //     $imagedata = file_get_contents($image_logo);
	        //     $data['image_logo'] = 'data:image/jpeg;base64,' . base64_encode($imagedata);
	        // }
	        $data['header'] = TRUE;
	        $data['_view'] = 'appointment_gallery';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['parent_tab'] = 'entry';
	        $data['active_tab'] = 'appointments';
	        $data['user']=$this->data;
	        $data['pageViewTitle'] = 'Appointment Gallery';
	        $data['appointments']['category'] = 'appointment';
	        $data['appointment_image'] = $this->Appuser_model->getAppointmentGallery($this->session->userdata(),$this->session->userdata('shop_no'),$data);
	        $this->load->view('user/basetemplate', $data);
	    
		}else{
			$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->get();

	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'id is required','errcode' => 400));
	        }

	        if (!array_key_exists('category', $data)) {
	            $this->response(array('error' => true, 'message' => 'category is required','errcode' => 400));
	        }

	        $result = $this->Appuser_model->getAppointmentGallery($appuser,$shop_no,$data);
	        if($result)
	        {
	            $this->response(array('error' => false,'message' => 'Data Fetched Successfully','errcode' => 0,'result'=>$result));
	        }
		}
	}

	function addCard()
    {
		
        // require_once(FCPATH.'vendor/autoload.php'); 
      if(!empty($this->session->userdata())){
		
		        if($this->input->post())
		        {   
					 $this->form_validation->set_rules('cardHolderName', 'cardHolderName', 'required');
					 $this->form_validation->set_rules('cardNumber', 'cardNumber', 'required');
					 $this->form_validation->set_rules('cardExpriry', 'cardExpriry', 'required');
					 $this->form_validation->set_rules('cvv', 'cvv', 'required');
		            // $this->form_validation->set_rules('token', 'Token', 'required');
		            if ($this->form_validation->run())
		            {
						$data = $this->input->post();
						$result = $this->Appuser_model->getAllCards($this->session->userdata('id'));
						$shop = $this->GlobalApp_model->getShopdetailByphoneNo($this->session->userdata('shop_no'));
						
		                if(empty($result))
		                {
		                    //Customer does not exist on stripe first register him on stripe   
		                    try
		                    {
		                        \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
				                $Customer = \Stripe\Customer::create(array(
				                    "description" => "Customer Has added card for ".$shop['shopname'],
				                    "email"=>$this->session->userdata('email') // User Email Address
				                ));
				                $CustomerArray = $Customer->__toArray(true);
				                $userdetails = $this->session->userdata();
				                $userdetails['customerStripeToken'] = $CustomerArray['id'];
				                $this->session->set_userdata($userdetails);
				                //Update customer id in app_user table
				                $this->db2->update('app_users',array('customerStripeToken'=>$CustomerArray['id']),array('id'=>$this->session->userdata('id')));

		                    }
		                    catch (Exception $e)
		                    {
		                        $error = $e->getMessage();
		                        $this->message->custom_error_msg('user/cards',$error);
		                    }

		                }

		                //Adding Card making it default in stripe
		                if(!empty($this->session->userdata('customerStripeToken')))
		                {
		                    try
		                    {
		                        
		                        \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
		                        $token = \Stripe\Token::retrieve($data['token']);

		                        $tokenArray = $token->__toArray(true);

		                        if($this->Appuser_model->checkForCardDuplicate($tokenArray['card']['fingerprint'],$this->session->userdata('id')))
		                        {
		                            $this->session->set_flashdata('error', 'Card Already Exist');
		                        }


		                        \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
		                        $card = \Stripe\Customer::retrieve($this->session->userdata('customerStripeToken'));
		                        $cardArray = $card->sources->create(array("source" => $data['token']));

		                        $cardArray1 = $cardArray->__toArray(true);

		                        if($this->Appuser_model->addCard($this->session->userdata('id'),$cardArray1,$result))
		                        {
		                             $this->message->save_success('user/cards');
		                        }
		                        else
		                            $this->session->set_flashdata('error', 'Card Addition failed');
		                            

		                    }
		                    catch (Exception $e)
		                    {
		                        $error = $e->getMessage();
		                        $this->message->custom_error_msg('user/cards',$error);
		                    }    
						}
						
						// $data['app_user_id']= $this->session->userdata('id');
						// unset($data['cvv']);
						// $this->db2->insert('app_users_cards',$data);
					}
					
		        }else{
						$data = $this->data;
						$data['header'] = TRUE;
						$data['_view'] = 'addCard';
						$data['sidebar'] = TRUE;
						$data['footer'] = TRUE;
						$data['parent_tab'] = 'entry';
						$data['active_tab'] = 'getAllCards';
						$data['user']=$this->data['userdata'];
						$data['breadCrumbParentname']='Pets List';
						$data['parent_url'] = 'getAllCards';
						$data['curenturl']= 'addCard';
						$this->load->view('user/basetemplate', $data);
		   			}
	   		}else{
				$shop_no =  trim($this->uri->segment(1));

		        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
		            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

		        $appuser = $this->GlobalApp_model->checkToken();
		        $shop = $this->GlobalApp_model->getShopdetailByphoneNo($shop_no);
		        
		        if(empty($appuser))
		        {
		            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
		        }

		        if($appuser['is_banned'] != 0)
		        {
		            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
		        }

		        $data = $this->post();

		        if (!array_key_exists('token', $data)) {
		            $this->response(array('error' => true, 'message' => 'require token','errcode' => 400));
		        }

		        
		        $result = $this->Appuser_model->getAllCards($appuser['id']);

		        if(empty($result))
		        {
		            //Customer does not exist on stripe first register him on stripe   
		            try
		            {
		                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
		                $Customer = \Stripe\Customer::create(array(
		                    "description" => "Customer Has added card for ".$shop['shopname'],
		                    "email"=>$appuser['email'] // User Email Address
		                ));

		                $CustomerArray = $Customer->__toArray(true);
		                $appuser['customerStripeToken'] = $CustomerArray['id'];
		                //Update customer id in app_user table
		                $this->db2->update('app_users',array('customerStripeToken'=>$CustomerArray['id']),array('id'=>$appuser['id']));
		                

		            }
		            catch (Exception $e)
		            {
		                $error = $e->getMessage();
		                $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
		            }

		        }

		        //Adding Card making it default in stripe

		        if(!empty($appuser['customerStripeToken']))
		        {
		            try
		            {
		                
		                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
		                $token = \Stripe\Token::retrieve($data['token']);

		                $tokenArray = $token->__toArray(true);

		                if($this->Appuser_model->checkForCardDuplicate($tokenArray['card']['fingerprint'],$appuser['id']))
		                {
		                    $this->response(array('error' => true, 'message' => 'Card Already Exist','errcode' => 410)); 
		                }


		                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
		                $card = \Stripe\Customer::retrieve($appuser['customerStripeToken']);
		                $cardArray = $card->sources->create(array("source" => $data['token']));

		                $cardArray1 = $cardArray->__toArray(true);

		                if($this->Appuser_model->addCard($appuser['id'],$cardArray1,$result))
		                {
		                    $this->response(array('error' => false, 'message' => 'card added successfully','errcode' => 0)); 
		                }
		                else
		                    $this->response(array('error' => true, 'message' => 'Card Addition failed','errcode' => 400)); 


		            }
		            catch (Exception $e)
		            {
		                $error = $e->getMessage();
		                $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
		            }    
		        }
		}
		
	}

	function makeDefaultCard()
    {
        require_once(FCPATH.'vendor/autoload.php');
      
      	if(!empty($this->session->userdata())){
	        if($this->input->post())
			{   
		        $data = $this->input->post();

		        if(!empty($data)){
		            $cardDetail = $this->GlobalApp_model->getCardDetailsById($data['id']);
		            if(empty($cardDetail))
		            {
		                $this->session->set_flashdata('error', 'Card not found');
		            }

		            try 
		            {
		                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);

		                $cu = \Stripe\Customer::retrieve($this->session->userdata('customerStripeToken'));           
		                $cu->default_source = $cardDetail['cardToken'];
		                $card = $cu->save();

		                $result = $card->__toArray(true);

		                if(!empty($result['id']))
		                {
		                    if($this->Appuser_model->makeDefaultCard($this->session->userdata(),$cardDetail))
		                    {
		                        $this->session->set_flashdata('success',  'Card set as default for payments');
		                    }
		                    else
		                        $this->session->set_flashdata('error', 'Card set as default failed');

		                }

		            }
		            catch (Exception $e)
		            {
		                $error = $e->getMessage();
		                $this->message->custom_error_msg('user/cards',$error);
		            } 
		        }
		    }
		}else{

	        $shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        }

	        $cardDetail = $this->GlobalApp_model->getCardDetailsById($data['id']);

	        if(empty($cardDetail))
	        {
	            $this->response(array('error' => true, 'message' => 'Card not found','errcode' => 402));
	        }

	        try 
	        {
	            \Stripe\Stripe::setApiKey(STRIPESECRETKEY);

	            $cu = \Stripe\Customer::retrieve($appuser['customerStripeToken']);           
	            $cu->default_source = $cardDetail['cardToken'];
	            $card = $cu->save();

	            $result = $card->__toArray(true);

	            if(!empty($result['id']))
	            {
	                if($this->Appuser_model->makeDefaultCard($appuser,$cardDetail))
	                {
	                    $this->response(array('error' => false, 'message' => 'Card set as default for payments','errcode' => 0)); 
	                }
	                else
	                    $this->response(array('error' => true, 'message' => 'Card set as default failed','errcode' => 400)); 
	            }

	        }
	        catch (Exception $e)
	        {
	            $error = $e->getMessage();
	            $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
	        } 
		}
    }

    function deleteCard()
    {
    	if(!empty($this->session->userdata())){
	        
	        if($this->input->post()){
	            
	            $data = $this->input->post();
	            $cardDetail = $this->GlobalApp_model->getCardDetailsById($data['id']);

	            if(empty($cardDetail))
	            {
	                $this->session->set_flashdata('error', 'Card not found');
	            }
	            if($this->Appuser_model->checkForPendingAppointment($this->session->userdata(),$cardDetail))
	            {
	                $this->session->set_flashdata('error', 'Pending appointment exist. Cannot delete card');
	            }
	            try 
	            {
	                require_once(FCPATH.'vendor/autoload.php');
	                \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	                $customer = \Stripe\Customer::retrieve($this->session->userdata('customerStripeToken'));
	                $card = $customer->sources->retrieve($cardDetail['cardToken'])->delete();
	                $CustomerArray = $card->__toArray(true);
	                $customer = \Stripe\Customer::retrieve($this->session->userdata('customerStripeToken'));
	                $resultArray = $customer->__toArray(true);
	                if($CustomerArray['deleted'] == true)
	                {
	                    $this->Appuser_model->deleteCard($this->session->userdata('id'),$cardDetail['id'],$CustomerArray,$resultArray);
	                    $this->session->set_flashdata('success',  'Card deleted successfully');
	                }
	                else
	                {
	                    $this->session->set_flashdata('error', 'Card delete failed');
	                }
	            }
	            catch (Exception $e)
	            {
	                $error = $e->getMessage();
	                $this->message->custom_error_msg('user/cards',$error);
	            }
	        }
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }
	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }
	        $data = $this->post();
	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        }
	        $cardDetail = $this->GlobalApp_model->getCardDetailsById($data['id']);
	        if(empty($cardDetail))
	        {
	            $this->response(array('error' => true, 'message' => 'Card not found','errcode' => 402));
	        }
	        if($this->Appuser_model->checkForPendingAppointment($appuser,$cardDetail))
	        {
	            $this->response(array('error' => true, 'message' => 'Pending appointment exist. Cannot delete card','errcode' => 423));
	        }
	        //if need to delete actuall card from stripe also
	        try 
	        {
	            require_once(FCPATH.'vendor/autoload.php');
	            \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	            $customer = \Stripe\Customer::retrieve($appuser['customerStripeToken']);
	            $card = $customer->sources->retrieve($cardDetail['cardToken'])->delete();

	            $CustomerArray = $card->__toArray(true);


	            $customer = \Stripe\Customer::retrieve($appuser['customerStripeToken']);
	            $resultArray = $customer->__toArray(true);
	            if($CustomerArray['deleted'] == true)
	            {
	                $this->Appuser_model->deleteCard($appuser['id'],$cardDetail['id'],$CustomerArray,$resultArray);
	                $this->response(array('error' => false, 'message' => 'Card deleted successfully','errcode' => 0)); 
	            }
	            else
	            {
	                 $this->response(array('error' => true, 'message' => 'Card delete failed','errcode' => 400));
	            }
	        }
	        catch (Exception $e)
	        {
	            $error = $e->getMessage();
	            $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
	        }
	    }
    }

    function editProfile()
    {
    	if(!empty($this->session->userdata())){
	        if($this->input->post())
	        {   

	            $data = $this->input->post();
	            // pr($data);exit;
	            $this->form_validation->set_rules('username','Username','required');
	          

	            $this->form_validation->set_rules('phone','phone','required');
	            
	            if ($this->form_validation->run()== TRUE) {
	        
				    $data['phone'] = preg_replace("/[^a-zA-Z0-9]+/", "", $data['phone']);
				   
				    $data['emergency_contact_phone_number'] = preg_replace("/[^a-zA-Z0-9]+/", "", $data['emergency_contact_phone_number']);
				    if(isset($data['phone1'])){
	        		$newarray = [];
		        	
			        	foreach ($data['phone1'] as $key4 => $value4) {
			        		$value4 = preg_replace("/[^a-zA-Z0-9]+/", "", $value4);
			        	    $i = 0;
		               	 	foreach ($data['addtional_number_type1'] as $key5 => $value5) {
		               	 	
		               	 		if($key5 > 99){
		               	 			$key5 = $i;
		               	 		}
		               	 		if($key4 == $key5){
		               	 		
			               	 	  $newarray[] = array($value4,$value5);
			               	 	}
			               	 	$i++;
		               	 	}
		               	 	
		               	 	
		               	}
		        		$data['additional_Phone'] = serialize($newarray);
	        			
	        		}else{
	                   $data['additional_Phone'] ='';
	               	}
	               	if(empty($data['is_deposited'])){
	               		$data['is_deposited'] = 0;
	               	}else{
	               		$data['is_deposited'] = 1;
	               	}
	                if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	                {
	                    if(!empty($this->session->userdata('profile')) && isset($data['is_deleted']) && $data['is_deleted'] == 1)
	                    {
	                        $this->Management_model->cloud_image_delete(PETS_IMAGES.$this->session->userdata('profile'));

	                    }  
	                    $data['profile'] = upload_pet_photo(); //Upload New Image

	                }
	                else
	                {
	                    if(!empty($this->session->userdata('profile')) && (isset($data['is_deleted']) && $data['is_deleted'] == 1)){
	                            $this->Management_model->cloud_image_delete(PETS_IMAGES.$this->session->userdata('profile'));
	                            $insert['profile'] = '';
	                        } 
	                }
	                // pr($data);exit;
	              	   $sign_upload = $this->input->post('sign_upload');
				        if(isset($sign_upload) && !empty($sign_upload))
				        {   

				        	$data['sign_upload'] = upload_pet_photo_base_64(null,$this->input->post('sign_upload'));
				           

				        }
	                if($this->Appuser_model->editProfile($this->session->userdata(),$data,$this->session->userdata('shop_no')))
	                {
	                	$app_users = $this->db2->get_where('app_users',array('id'=>$this->session->userdata('id')))->row_array();   
	                	
	                	$this->session->set_userdata($app_users);
	                	
	                    $this->message->update_success('user/editProfile/');
	                   
	                }
	            }
	        }else{
	            $data = $this->data;
	            $data['header'] = TRUE;
	            $data['_view'] = 'editProfile';
	            $data['sidebar'] = TRUE;
	            $data['footer'] = TRUE;
	            $data['parent_tab'] = 'profile';
	            $data['active_tab'] = 'profile';
	            $data['arrDiscount'] = $this->Management_model->getAvailableDiscounts();
	            $data['app_users']=$this->session->userdata();
	            $data['arrDiscountActive'] = $this->Management_model->getActiveDiscount($data['app_users']['app_user_id']);
	            // pr($this->session->userdata());exit;
	            $data['breadCrumbname']='Edit User';
	            $data['breadCrumbParentname']='profile';
	            $data['parent_url'] = 'dashboard';
	            $data['curenturl']= 'profile';
	            $this->load->view('user/basetemplate', $data);
	        }
	    }
	    else{
	    	$shop_no =  trim($this->uri->segment(1));
	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        if (!array_key_exists('username', $data)) {
	            $this->response(array('error' => true, 'message' => 'require name','errcode' => 400));
	        }

	        if (!array_key_exists('email', $data)) {
	            $this->response(array('error' => true, 'message' => 'require email','errcode' => 400));
	        }

	        if (!array_key_exists('phone', $data)) {
	            $this->response(array('error' => true, 'message' => 'require phone','errcode' => 400));
	        }

	        if (!array_key_exists('dob', $data)) {
	            $this->response(array('error' => true, 'message' => 'require dob','errcode' => 400));
	        }

	        if (!array_key_exists('address', $data)) {
	            $this->response(array('error' => true, 'message' => 'require address','errcode' => 400));
	        }

	        if (!array_key_exists('city', $data)) {
	            $this->response(array('error' => true, 'message' => 'require city','errcode' => 400));
	        }

	        if (!array_key_exists('state', $data)) {
	            $this->response(array('error' => true, 'message' => 'require state','errcode' => 400));
	        }

	        if(!$this->Appuser_model->checkEmailForEdit($appuser,$data['email']))
	        {
	            $this->response(array('error' => true, 'message' => 'Email Already Exist','errcode' => 101));
	        }


	        if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	        {
	            //Check If new profile image has been added 
	            if(!empty($appuser['profile']) && isset($data['is_deleted']) && $data['is_deleted'] == 1)
	            {
	                // if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']))
	                // unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']);
	                $this->Management_model->cloud_image_delete(PETS_IMAGES.$appuser['profile']);
	            }  
	            $data['profile'] = upload_pet_photo(); //Upload New Image
	        }
	        else
	        {
	            if(!empty($appuser['profile']) && (isset($data['is_deleted']) && $data['is_deleted'] == 1)){

	                // if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']))
	                //     unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$appuser['profile']);

	                $this->Management_model->cloud_image_delete(PETS_IMAGES.$appuser['profile']);

	                $insert['profile'] = '';
	            }   
	        }
	        if($this->Appuser_model->editProfile($appuser,$data,$shop_no))
	        {
	            $this->response(array('error' => false,'message' => 'Profile edited successfully','errcode' => 0));
	        }
	    }
	}

function addNewPetData()
{
	// pr($this->input->post());exit;
	if(!empty($this->session->userdata())){
		if($this->input->post())
		{
			$this->form_validation->set_rules('name', lang('petName'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('type', lang('petType'), 'required');
			if ($this->form_validation->run()== TRUE) {
				$data = $this->input->post();
				if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
				{
					$data['pet_cover_photo'] = upload_pet_photo();
				}
				else
				{
					$data['pet_cover_photo'] = '';
				}
				if(empty($data['weight'])){
					$data['weight'] = 0;
				}
				if(empty($data['spayed'])){
					$data['spayed'] = 'false';
				}
				if(!isset($data['mix_breed'])){
					$data['mix_breed'] = 0;
				}
				$vacc_id = $this->input->post('vacc_id[0]');
				if($this->Appuser_model->addNewPet($this->session->userdata('id'),$this->session->userdata('shop_no'),$data,$vacc_id))
				{
					$this->session->set_flashdata('success', 'Added New Pet Successfully');
					$this->message->save_success('user/getAllPets');
				}else{
					$this->session->set_flashdata('failed', 'Added New Pet Failed');
				}
			}else{
				$error = validation_errors();
	            $this->message->custom_error_msg('user/getAllPets',$error);
			}
		}
	}else{
		redirect('user/login');
	}
 }


	function addNewPet()
    {	
    	if(!empty($this->session->userdata())){
	        if($this->input->post())
	        {
	        	pr($_POST);exit;
	            $this->form_validation->set_rules('name', lang('petName'), 'trim|required|xss_clean');
	            $this->form_validation->set_rules('type', lang('petType'), 'required');
	            if ($this->form_validation->run()== TRUE) {
	                $data = $this->input->post();
	                if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	                {
	                    $data['pet_cover_photo'] = upload_pet_photo();
	                }
	                else
	                {
	                    $data['pet_cover_photo'] = '';
	                }
	                if(empty($data['weight'])){
	                	$data['weight'] = 0;
	                }
	                if(!isset($data['spayed'])){
	                	$data['spayed'] = 'false';
	                }
	                if(!isset($data['mix_breed'])){
	                	$data['mix_breed'] = 0;
	                }
	                if($this->Appuser_model->addNewPet($this->session->userdata('id'),$this->session->userdata('shop_no'),$data))
	                {
	                    $this->message->save_success('user/getAllPets');
	                }
	            }else{
	                    $error = validation_errors();
	                    $this->message->custom_error_msg('user/getAllPets',$error);
	            }
	        }else{
	            $data['header'] = TRUE;
	            $data['_view'] = 'addNewPet';
	            $data['sidebar'] = TRUE;
	            $data['petSize'] = $this->Appuser_model->getPetSize();
	            $data['footer'] = TRUE;
	            $data['parent_tab'] = 'entry';
	            $data['active_tab'] = 'getAllPets';
	            $data['user']=$this->data['userdata'];
	            $data['breadCrumbParentname']='Pets List';
	            $data['parent_url'] = 'getAllPets';
	            $data['curenturl']= 'addNewPet';
	            $this->load->view('user/basetemplate', $data);
	        }
		}else{
			redirect('user/login');
		}

		// else{
	    // 	$shop_no =  trim($this->uri->segment(1));

	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	    //     $appuser = $this->GlobalApp_model->checkToken();

	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }

	    //     if($appuser['is_banned'] != 0)
	    //     {
	    //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	    //     }

	    //     $data = $this->post();

	    //     if ($data == null) {
	    //         $this->response(array('error' => true, 'message' => 'invalid_json','errcode' => 400));
	    //     }

	    //     if (!array_key_exists('name', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require name','errcode' => 400));
	    //     }

	    //     if (!array_key_exists('type', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require type','errcode' => 400));
	    //     }


	    //     if (!array_key_exists('gender', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require gender','errcode' => 400));
	    //     }


	    //     if (!array_key_exists('weight', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require weight','errcode' => 400));
	    //     }

	    //     if (!array_key_exists('breed_id', $data)) {
	    //         $this->response(array('error' => true, 'message' => 'require breed_id','errcode' => 400));
	    //     }

	    //     //pr($_FILES);exit;

	    //     if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	    //     {
	    //         $data['pet_cover_photo'] = upload_pet_photo();
	    //     }
	    //     else
	    //     {
	    //         $data['pet_cover_photo'] = '';
	    //     }

	  
	    //     if($this->Appuser_model->addNewPet($appuser['id'],$shop_no,$data))
	    //     {
	    //         $this->response(array('error' => false, 'message' => 'Pet added successfully','errcode' => 0));
	    //     }
		// }
    }

	public function editPet()
	{  
		if(!empty($this->session->userdata())){
			if (isset($_POST['name']) && !empty($_POST['name'])) {
			
				$this->form_validation->set_rules('name', lang('petName'), 'trim|required|xss_clean');
	            $this->form_validation->set_rules('type', lang('petType'), 'required');
			
				$data = $this->input->post();
				if ($this->form_validation->run() == true) {
					// pr($data);exit;
					$this->Appuser_model->editNewPet($this->session->userdata('id'),$this->session->userdata('shop_no'),$data);
					$this->session->set_flashdata('success', 'Pet Details Updated successfully');
					redirect($_SERVER['HTTP_REFERER']);
				}else{
					$this->session->set_flashdata('failed', 'Pet Details Not Updated');
				}
			}
			$data = $this->data;
			$data['id'] = $_GET['id'];
			$data['header'] = true;
			$data['_view'] = 'editPet';
			$data['sidebar'] = true;
			$data['footer'] = true;
			$data['active_tab'] = 'getAllPets';
			$data['pet'] = $this->Appuser_model->getPets($this->session->userdata('id'),$this->session->userdata('shop_no'),$data['id']);
			$this->load->view('user/basetemplate', $data);
		}else{
			$this->load->view('user/login');
		}
	}

    // function editPet()
    // {
    // 	if(!empty($this->session->userdata())){
	//         // if($this->input->post())
	//         // {
	//         // 	// pr($_POST);exit;
	//         //     $this->form_validation->set_rules('name', lang('petName'), 'trim|required|xss_clean');
	//         //     $this->form_validation->set_rules('type', lang('petType'), 'required');
	//         //     if ($this->form_validation->run()== TRUE) {
	//         //         $data = $this->input->post();
	//         //         if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	//         //         {
	//         //             $pet = $this->db2->get_where('pets',array('id'=>$data['id']))->row_array();
	//         //             if(!empty($pet))
	//         //             {
	//         //                 if(!empty($pet['pet_cover_photo']) && isset($data['is_deleted']) && $data['is_deleted'] == 1)
	//         //                 {     
	//         //                     $this->Management_model->cloud_image_delete(PETS_IMAGES.$pet['pet_cover_photo']);
	//         //                 }
	//         //                 $data['pet_cover_photo'] = upload_pet_photo();   
	//         //             }
	//         //         }
	//         //         else
	//         //         {
	//         //             if(!empty($pet['pet_cover_photo']) && isset($data['is_deleted']) && $data['is_deleted'] == 1)
	//         //             {
	//         //                 $this->Management_model->cloud_image_delete(PETS_IMAGES.$pet['pet_cover_photo']);
	//         //             }
	//         //             $data['pet_cover_photo'] = '';
	//         //         }
	//         //         // if(empty($data['weight'])){
	//         //         // 	$data['weight'] = 0;
	//         //         // }
	//         //         // if(!isset($data['spayed'])){
	//         //         // 	$data['spayed'] = 'false';
	//         //         // }
	//         //         // if(!isset($data['mix_breed'])){
	//         //         // 	$data['mix_breed'] = 0;
	//         //         // }
	//         //         if($this->Appuser_model->editNewPet($this->session->userdata('id'),$this->session->userdata('shop_no'),$data))
	//         //         {
	//         //             $this->message->update_success('user/getAllPets/'.get_encode($data['id']));
	//         //         }
	//         //     }else{
	//         //             $error = validation_errors();
	//         //             $this->message->custom_error_msg('user/getAllPets',$error);
	//         //     }
	//         // }else{
	//             $data['header'] = TRUE;
	//             $data['_view'] = 'editPet';
	//             // $id = get_decode($_GET['id']);
	// 			// $data['pet'] = $this->Appuser_model->getPetDetails($this->session->userdata('id'),$this->session->userdata('shop_no'),$id);
	// 			// // pr( $data['pet']);exit;
	//             // // $data['petbreeds'] = $this->Appuser_model->getBreedTypeWithSelected($data['pet']['type'],$data['pet']['breed_id']);
	//             // $data['petSize'] = $this->Appuser_model->getPetSize();
	//             // $data['emoji'] = $this->Appuser_model->getEmojiForDropDown();
	// 	        // $data['petBehavior'] = $this->Management_model->getPetBehavior($data['pet']['id']);

	//             $data['sidebar'] = TRUE;
	//             $data['footer'] = TRUE;
	//             // $data['parent_tab'] = 'entry';
	//             $data['active_tab'] = 'getAllPets';
	//             // $data['user']=$this->data['userdata'];
	//             // $data['breadCrumbParentname']='Pets List';
	//             // $data['parent_url'] = 'getAllPets';
	//             // $data['curenturl']= 'editPet';
	//             $this->load->view('user/basetemplate', $data);
	//         // }
	// 	}
	// 	// else{
	//     // 	$shop_no =  trim($this->uri->segment(1));
	//     //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	//     //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	//     //     $appuser = $this->GlobalApp_model->checkToken();

	//     //     if(empty($appuser))
	//     //     {
	//     //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	//     //     }

	//     //     if($appuser['is_banned'] != 0)
	//     //     {
	//     //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	//     //     }

	//     //     $data = $this->post();
	       
	//     //     if ($data == null) {
	//     //         $this->response(array('error' => true, 'message' => 'invalid_json','errcode' => 400));
	//     //     }

	//     //     if (!array_key_exists('id', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	//     //     }

	//     //     if (!array_key_exists('name', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require name','errcode' => 400));
	//     //     }

	//     //     if (!array_key_exists('type', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require type','errcode' => 400));
	//     //     }


	//     //     if (!array_key_exists('gender', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require gender','errcode' => 400));
	//     //     }


	//     //     if (!array_key_exists('weight', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require weight','errcode' => 400));
	//     //     }

	//     //     if (!array_key_exists('breed_id', $data)) {
	//     //         $this->response(array('error' => true, 'message' => 'require breed_id','errcode' => 400));
	//     //     }

	//     //     if(isset($_FILES['employee_photo']) && !empty($_FILES['employee_photo']['name']))
	//     //     {
	//     //         $pet = $this->db2->get_where('pets',array('id'=>$data['id']))->row_array();
	                    
	//     //         if(!empty($pet))
	//     //         {
	               
	//     //             if(!empty($pet['pet_cover_photo']) && isset($data['is_deleted']) && $data['is_deleted'] == 1)    
	//     //             { 
	//     //                 // if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']))
	//     //                 //     unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']);
	//     //                 $this->Management_model->cloud_image_delete(PETS_IMAGES.$pet['pet_cover_photo']);
	//     //             }
	//     //             $data['pet_cover_photo'] = upload_pet_photo();   
	//     //         }
	//     //     }
	//     //     else
	//     //     {
	//     //         if(!empty($pet['pet_cover_photo']) && isset($data['is_deleted']) && $data['is_deleted'] == 1)
	//     //         {
	                
	//     //             // if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']))
	//     //             //     unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$pet['pet_cover_photo']);
	//     //             $this->Management_model->cloud_image_delete(PETS_IMAGES.$pet['pet_cover_photo']);
	//     //         }
	            

	//     //         $data['pet_cover_photo'] = '';
	//     //     }
	//     //     if($this->Appuser_model->editNewPet($appuser['id'],$shop_no,$data))
	//     //     {
	//     //         $this->response(array('error' => false, 'message' => 'Pet updated successfully','errcode' => 0));
	//     //     }
	//     // }
    // }

    function deletePet()
    {
    	if(!empty($this->session->userdata())){
			$data = $this->data;
			$data['id'] = $_GET['id'];
			// pr($data['id']);exit;

			if(!$this->Appuser_model->checkPetHasAppointmentBoarding($data['id']))
			{
				$this->session->set_flashdata('error', 'Pet cannot be deleted as it has appointment/boading/daycare');
			}
			if($this->Appuser_model->deletePet($this->session->userdata('id'),$this->session->userdata('shop_no'),$data))
			{
				$this->message->delete_success('User/getAllPets');

			}
	    }else{
			$this->load->view('user/login');
	    	// $shop_no =  trim($this->uri->segment(1));

	        // if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	        //     $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        // $appuser = $this->GlobalApp_model->checkToken();

	        // if(empty($appuser))
	        // {
	        //     $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        // }

	        // if($appuser['is_banned'] != 0)
	        // {
	        //     $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        // }

	        // $data = $this->post();

	        // if (!array_key_exists('id', $data)) {
	        //     $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        // }

	        // if(!$this->Appuser_model->checkPetHasAppointmentBoarding($data['id']))
	        // {
	        //     $this->response(array('error' => true, 'message' => 'Pet cannot be deleted as it has appointment/boading/daycare','errcode' => 421));
	        // }

	        // if($this->Appuser_model->deletePet($appuser['id'],$shop_no,$data))
	        // {
	        //     $this->response(array('error' => false, 'message' => 'Pet deleted successfully','errcode' => 0));
	        // }
	    }
    }

    function addPetNote($id)
    {   //Add New Medication Note
    	if(!empty($this->session->userdata())){
    		$formType = $_POST['formType'];
	    	if(isset($_POST['formType']))
	    	{
	    		unset($_POST['formType']);
	    	}



	    	if($formType == 'edit'){
		    	$id = get_decode($id);
		        $id == TRUE || $this->message->norecord_found(referrer());
		    }else{
		    	$id = 1000000000;
		    }

	        if($this->input->post())
	        {
	        	// pr($_POST);exit;
	            $this->form_validation->set_rules('medication_name', lang('username'), 'trim|required|xss_clean');
	            $this->form_validation->set_rules('expiry_on', lang('expiry_on'), 'trim|required|xss_clean');
	            if ($this->form_validation->run()== TRUE) {
	                $data = $this->input->post();
	                $data['pet_id'] = $id;
	                if(isset($data['avatar_data1']))
	                {
	                    $avatarData = $data['avatar_data1'];
	                    unset($data['avatar_data1']);

	                    if(!empty($avatarData))
	                        $data['image'] = upload_pet_photo_base_64(null,$avatarData);
	                }
	                if($this->Appuser_model->addPetNote($this->session->userdata('id'),$data,$this->session->userdata('shop_no')))
	                {
	                	if($formType == 'add'){
							$vacc_id = $this->db2->insert_id();
					    	$msg = array('errcode' => 1,'vaccId' =>$vacc_id,'message'=>'vaccination Added');
					    }else{
		                	$msg = array('errcode' => 0,'message'=>'Customer Added');
		                	// $this->message->update_success('user/getPetDetails/'.get_encode($data['id']).'/vaccination_records');
		              	}
	                   	
	                }
	            }else{
	                $msg = array('errcode' => 1000,'message'=>'Paramter not found');
	            }

	        } else
	    	{
	    		$msg = array('errcode' => 1000,'message'=>'Paramter not found');
	    	} 
	    	 echo json_encode($msg);exit;  
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if (!array_key_exists('pet_id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require pet_id','errcode' => 400));
	        }

	        if (!array_key_exists('medication_name', $data)) {
	            $this->response(array('error' => true, 'message' => 'require medication_name','errcode' => 400));
	        }

	        if (!array_key_exists('expiry_on', $data)) {
	            $this->response(array('error' => true, 'message' => 'require medication_name','errcode' => 400));
	        }

	        if($this->Appuser_model->addPetNote($appuser['id'],$data,$shop_no))
	        {
	            $this->response(array('error' => false, 'message' => 'Note added successfully','errcode' => 0));
	        }
	    }
    }

    function editPetNote()
    {
        //Edit Medication Note
        if(!empty($this->session->userdata())){
	        if($this->input->post())
	        {
	            $this->form_validation->set_rules('medication_name', lang('username'), 'trim|required|xss_clean');
	            $this->form_validation->set_rules('expiry_on', lang('expiry_on'), 'trim|required|xss_clean');
	            if ($this->form_validation->run()== TRUE) {
	                $data = $this->input->post();
	                // if(isset($data['avatar_data1']))
	                // {
	                //     $avatarData = $data['avatar_data1'];
	                //     unset($data['avatar_data1']);

	                //     if(!empty($avatarData))
	                //         $data['image'] = upload_pet_photo_base_64(null,$avatarData);
	                // }
	                if($this->Appuser_model->editPetNote($this->session->userdata('id'),$data,$this->session->userdata('shop_no')))
	                {
	                    $this->message->update_success('user/getPetDetails/'.get_encode($data['id']));
	                }
	            }else{
	                $error = validation_errors();
	                $this->message->custom_error_msg('user/pets',$error);
	            }
	        }  
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if (!array_key_exists('pet_id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require pet_id','errcode' => 400));
	        }

	        if (!array_key_exists('medication_name', $data)) {
	            $this->response(array('error' => true, 'message' => 'require medication_name','errcode' => 400));
	        }

	        if (!array_key_exists('expiry_on', $data)) {
	            $this->response(array('error' => true, 'message' => 'require medication_name','errcode' => 400));
	        }

	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        }

	        if($this->Appuser_model->editPetNote($appuser['id'],$data,$shop_no))
	        {
	            $this->response(array('error' => false, 'message' => 'Note edit successfully','errcode' => 0));
	        }
	    }      
    }

    function deletePetNote()
    {   //Delete Pet Note
    	if(!empty($this->session->userdata())){
    		if($this->input->post())
	        {
	            $data = $this->input->post();
		        if($this->Appuser_model->deletePetNote($this->session->userdata('id'),$data,$this->session->userdata('shop_no')))
		        {
		            $this->message->delete_success('user/pets/'.get_encode($data['id']));
		        }
		    }
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if (!array_key_exists('id', $data)) {
	            $this->response(array('error' => true, 'message' => 'require id','errcode' => 400));
	        }

	        if($this->Appuser_model->deletePetNote($appuser['id'],$data,$shop_no))
	        {
	            $this->response(array('error' => false, 'message' => 'Note deleted successfully','errcode' => 0));
	        }
	    }
    }

    function changePassword()
    {
    	if(!empty($this->session->userdata())){
	        if($this->input->post())
	        {   
	            $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|xss_clean');
	            $this->form_validation->set_rules('new_password','New Password', 'trim|required|xss_clean');
	            $this->form_validation->set_rules('confirm_password','Confirm New Password', 'trim|required|xss_clean');
	       
	            if ($this->form_validation->run() == TRUE)
	            {
	                $data = $this->input->post();
	                if (!$this->Appuser_model->checkOldPassword(trim($data['current_password']),$this->session->userdata())) {
			            //Verify if old password is correct
			            $this->session->set_flashdata('error', 'Old password incorrect');
			        }
	                if($this->Appuser_model->changePassword($this->session->userdata(),$data,$this->session->userdata('shop_no')))
	                {
	                    $this->message->save_success('user/changePassword');
	                }else
	                {
	                    $this->message->custom_error_msg('user/changePassword', 'Cannot change the password, Please try again'); 
	                }   
	            }
	        }else{
				$data = $this->data;
				$data['header'] = TRUE;
				$data['_view'] = 'changePassword';
				$data['title'] = 'Change Password';
				$data['sidebar'] = TRUE;
				$data['footer'] = TRUE;
				$data['active_tab'] = 'change_password';
				$this->load->view('user/basetemplate', $data);
			}
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        if (!array_key_exists('password', $data)) {
	            $this->response(array('error' => true, 'message' => 'require old password','errcode' => 400));
	        }

	        if (!array_key_exists('new_password', $data)) {
	            $this->response(array('error' => true, 'message' => 'require new password','errcode' => 400));
	        }

	        if(empty($appuser['password']))
	        {
	            $this->response(array('error' => true, 'message' => 'this fb account cannot change password','errcode' => 111));
	        }


	        if (!$this->Appuser_model->checkOldPassword(trim($data['password']),$appuser)) {
	            //Verify if old password is correct
	            $this->response(array('error' => true, 'message' => 'Old password incorrect','errcode' => 419));
	        }


	        if($this->Appuser_model->changePassword($appuser,$data,$shop_no))
	        {
	            $this->response(array('error' => false,'message' => 'Password Changed successfully','errcode' => 0));
	        }

	    }
    }    

    function changeShopNumber(){
    	if(!empty($this->session->userdata())){
    		if($this->input->post())
	        {   
	            $this->form_validation->set_rules('new_shop_no', 'new_shop_no', 'trim|required|xss_clean');
	           
	            if ($this->form_validation->run() == TRUE)
	            {
	                $data = $this->input->post();
	                if(!$this->GlobalApp_model->checkShopPhoneNo($data['new_shop_no'])){
		            	$this->session->set_flashdata('error', 'Invalid Shop details');
	                }
			        else
			        {
			            if($this->GlobalApp_model->changeShopNumber($this->session->userdata(),$data['new_shop_no'],$this->session->userdata('shop_no')))
			            {
			                $this->message->update_success('user/shopprofile');
			            }
			            else
			            {
			                 $this->session->set_flashdata('error', 'Shop switching failed');
			            }
			        }
	            }
	        }else{
	        	$data['header'] = TRUE;
	            $data['_view'] = 'shopprofile';
	            $data['sidebar'] = TRUE;
	            $data['footer'] = TRUE;
	            $data['parent_tab'] = 'entry';
	            $data['active_tab'] = 'shop';
	            $data['user']=$this->data['userdata'];
	            $data['breadCrumbParentname']='Shop';
	            $data['parent_url'] = 'shop';
	            $data['curenturl']= 'shopprofile';
	            $this->load->view('user/basetemplate', $data);
	        }
    	}else{
    		$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        

	        if (!array_key_exists('new_shop_no', $data)) {
	            $this->response(array('error' => true, 'message' => 'new_shop_no is required','errcode' => 404));
	        }

	        if(!$this->GlobalApp_model->checkShopPhoneNo($data['new_shop_no']))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 112));
	        else
	        {
	            if($this->GlobalApp_model->changeShopNumber($appuser,$data['new_shop_no'],$shop_no))
	            {
	                $this->response(array('error' => false,'message' => 'Shop Switched Successfully','errcode' => 0));
	            }
	            else
	            {
	                 $this->response(array('error' => true,'message' => 'Shop switching failed','errcode' => 428));
	            }
	        }
    	}
    }

	// Forgot password 
	function forgotPassword(){
		
		if(!$this->session->userdata('id')){

			$this->form_validation->set_rules('shopno', 'shopno', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			
			if(!$this->form_validation->run())
			{
				$data['_view'] = 'forgotPass';
				$this->load->view('user/basetemplate', $data);
			}
			else
			{
				$data = $this->data;
				$email =   $this->input->post('email');
				$shopno =  $this->input->post('shopno');

				if(!$this->GlobalApp_model->checkShopPhoneNo($shopno)){
					$this->session->set_flashdata('error', 'Invalid Shop details');
				}

				$appuser = $this->Appuser_model->forgotPasswordEmailCheck($email);
			
				if (!$appuser) {
					$this->session->set_flashdata('error', 'Email Address does not exist');
				}

				if($this->Appuser_model->checkUserBelongsToShop($shopno,$appuser))
				{
					$this->session->set_flashdata('error', 'App User Not Registerd with the shop');
				}
				
				if($this->Appuser_model->forgotPassword($data,$appuser,$shopno)) {
					//Validating User by email and shop no and sending reset password email

					$this->session->set_flashdata('success', 'Reset Password mail sent successfully');
					header('Location: '. INSTALL_FOLDER);

				}else{
					$this->session->set_flashdata('error', 'Invalid Email Please try again');
					header('Location: '. INSTALL_FOLDER);
				}
			}
		}
		else{
			
		}
	}

	// Reset Password after forgot password
    public function resetPassword($token = '')
    {
        $data = $this->data;
        $userdata = $this->Appuser_model->getUserByToken($token);
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[new_password]');
        if ($this->form_validation->run()) {
            if ($this->Appuser_model->resetPassword($this->input->post())) {
                $this->session->set_flashdata('success', 'Password changed successfully!');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong, please try again later.');
            }
			// header('Location: '. INSTALL_FOLDER);
			$this->load->view('user/login');
        } else {
			$data['header'] = true;
            $data['_view'] = "resetPassword";
            $data['userdata'] = $userdata;
            $data['token'] = $token;
            if (empty($data['userdata'])) {
                show_error('Link has been expired!');
            }
            $this->load->view('user/basetemplate', $data);
        }
	}

    function addNewDayCare(){
    	if(!empty($this->session->userdata())){

    		if($this->input->post())
			{

				$data = $this->input->post();
				

				if (isset($data['app_user_idTemp']) && !empty($data['app_user_idTemp'])) {
					$data['app_user_id'] = $data['app_user_idTemp'];
					$_POST['app_user_id'] = $data['app_user_idTemp'];
				}

				if (isset($data['pet_idTemp']) && !empty($data['pet_idTemp'])) {
					$data['pet_id'] = $data['pet_idTemp'];
					$_POST['pet_id'] = $data['pet_idTemp'];
					
				}

				unset($data['app_user_idTemp']);
				unset($data['pet_idTemp']);

					//For New Boarding/Day Care Request
				$this->form_validation->set_rules('pet_id', lang('pet'), 'trim|required|xss_clean');
		        $this->form_validation->set_rules('app_user_id', lang('appointmentedBy'), 'trim|required|xss_clean');
			   
		        if ($this->form_validation->run()== TRUE) {

		        	if($result=$this->Management_model->addNewBoarding($data,'daycare'))
		        	{
		        		
		        			$this->message->custom_success_msg('User/daycare','Day Care Request confirmed successfully');
		        		
		        	}
		        }
		        else
		        {
		        	$this->message->custom_error_msg(referrer(),'Something Went Wrong');
		        }

			}
			else
			{
				$data = $this->data;
				$data['header'] = TRUE;
				$data['_view'] = 'addNewDaycare';
				$data['sidebar'] = TRUE;
				$data['footer'] = TRUE;
				
				$data['parent_tab'] = 'entry';
				$data['user']=$this->data;
				 //Breadcrumb Name
				$data['breadCrumbParentname']='entry';

				if(!empty($date))
	            $data['date'] = $date;
	            else
	            $data['date'] = date('Y-m-d');
				
				$data['curenturl']= 'addNewDaycare';

				
	          
	            
	            $data['allpets'] = $this->db2->get_where('pets',array('app_user_id'=>$this->session->userdata('id'),'is_deceased'=>0,'is_deleted'=>0))->result_array();
	            
	            $data['app_users_details'] = $this->session->userdata();
	           
	            
	            
				$data['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
				$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
	        	$data['boardingInventory'] = $this->Management_model->getBoardingInventory();
	        	$data['playareas'] = $this->db->get_where('playareas',array('is_deleted'=>0))->result_array();
	        	
	        	$data['category'] = 'daycare'; 

        		$data['active_tab'] = 'daycare'; 
				$data['breadCrumbname']='daycare';
				$data['parent_url'] = 'daycare';
        	
        		$this->db->where('date(daycare_schedule.date) >=',date('Y-m-d'));
        		$data['daycare_schedule'] = $this->db->get_where('daycare_schedule')->result_array();

	        		// if($data['boardingSettings']['normalDayCarePricing'] == 0)
		        	// 	$this->message->custom_error_msg(referrer(),'You must add daycare price');

		        	// if(empty($data['playareas']))
		        	// 	$this->message->custom_error_msg(referrer(),'You must add playarea locations');

		        	// if(empty($data['daycare_schedule']))
	        		// 	$this->message->custom_error_msg(referrer(),'You must add schedule');

	     //    		if($data['userdata']['daycare_switch'] == 0 || $data['userdata']['daycare_switch'] == 0)
						// $this->message->custom_error_msg(referrer(), 'Daycare service is disabled.');


	        	
	        	$data['boardingAdditionalParametersForPriceCalculation'] = $this->Management_model->getboardingAdditionalParametersForPriceCalculation();

			
				
				$this->load->view('user/basetemplate', $data);
			}

    	}else{
    		$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        if(empty($data['category'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'Atleast one category is needed','errcode' => 407));
	        }

	        if(empty($data['pet_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'pet_id missing','errcode' => 404));
	        }

	        if(empty($data['date'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'date missing','errcode' => 404));
	        }

	        if (empty($appuser['customerStripeToken']) && $this->Appuser_model->checkUserCard($appuser)) {
	            $this->response(array('error' => true, 'message' => 'User default credit card details not found','errcode' => 110));
	        }

	        if(empty($data['start_time']))
	        {
	            $this->response(array('error' => true, 'message' => 'time missing','errcode' => 404));
	        }

	        if(empty($data['end_time']))
	        {
	            $this->response(array('error' => true, 'message' => 'appointment_end_time missing','errcode' => 404));
	        }
	        
	        if(empty($data['daycareCount']))
	        {
	            $this->response(array('error' => true, 'message' => 'daycareCount missing','errcode' => 404));
	        }

	        if(!$this->GlobalApp_model->checkShopCanTakeBoarding($shop_no))
	        {    //check if shop can take boarding and day care request
	            $this->response(array('error' => true,'message' => 'Shop cannot take Boarding request completed profile','errcode'=>412));
	        }

	        if($data['daycareCount'] > 0)      
	        {    
	            for($i=0;$i<$data['daycareCount'];$i++)            
	            {
	                if($this->Management_model->checkBoardingExistForPet($data['pet_id'],date('H:i:s',strtotime($data['start_time'][$i])),date('H:i:s',strtotime($data['end_time'][$i])),$data['date'][$i],$data['date'][$i],$data['category']))
	                {
	                    $this->response(array('error' => true, 'message' => 'Appointment already exist for pet','errcode' => 406));
	                }
	            }    
	        }    


	        $data['overlapping_appointment'] = 0;
	       
	        $finalAmount = floatval($data['cost'] + $data['stripe_charge'] + $data['commission']);

	            if($finalAmount > 0 )
	            {
	                    try 
	                    {
	                        \Stripe\Stripe::setApiKey(STRIPESECRETKEY);
	                        
	                        $charge = \Stripe\Charge::create(array(
	                          "amount" => ($finalAmount*100),
	                          "currency" => "usd",
	                          "customer" => $appuser['customerStripeToken'], // obtained with Stripe.js
	                          "description" => "Charge for Appointment "
	                        ));

	                        $chargeArray = $charge->__toArray(true);
	                        
	                        if($this->Appuser_model->addNewDayCare($appuser,$data,$chargeArray,$finalAmount,$shop_no))
	                        {
	                            $this->response(array('error' => false,'message' => 'Appointment Added successfully','errcode' => 0));   
	                        }
	                        else
	                        {
	                            $this->response(array('error' => true,'message' => 'Something went wrong','errcode' => 405));  
	                        }

	                    }
	                    catch (Exception $e)
	                    {
	                        $error = $e->getMessage();
	                        $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
	                    }
	            }
    	}
    }

    function addNewBoarding(){
    	if(!empty($this->session->userdata())){

    		if($this->input->post())
			{

				$data = $this->input->post();
				

				if (isset($data['app_user_idTemp']) && !empty($data['app_user_idTemp'])) {
					$data['app_user_id'] = $data['app_user_idTemp'];
					$_POST['app_user_id'] = $data['app_user_idTemp'];
				}

				if (isset($data['pet_idTemp']) && !empty($data['pet_idTemp'])) {
					$data['pet_id'] = $data['pet_idTemp'];
					$_POST['pet_id'] = $data['pet_idTemp'];
					
				}

				unset($data['app_user_idTemp']);
				unset($data['pet_idTemp']);

					//For New Boarding/Day Care Request
				$this->form_validation->set_rules('pet_id', lang('pet'), 'trim|required|xss_clean');
		        $this->form_validation->set_rules('app_user_id', lang('appointmentedBy'), 'trim|required|xss_clean');
			   
			        if ($this->form_validation->run()== TRUE) {

			        	 
			        if ($this->form_validation->run()== TRUE) {

			        	if($result=$this->Management_model->addNewBoarding($data,'boarding'))
			        	{
			        			$this->message->custom_success_msg('User/boardings','Boarding Request confirmed successfully');
			        	}
			        }
			        else
			        {
			        	$this->message->custom_error_msg(referrer(),'Something Went Wrong');
			        }
				}
			}
			else
			{
				$data = $this->data;
				$data['header'] = TRUE;
				$data['_view'] = 'addNewBoarding';
				$data['sidebar'] = TRUE;
				$data['footer'] = TRUE;
				
				$data['parent_tab'] = 'entry';
				$data['user']=$this->data;
				 //Breadcrumb Name
				$data['breadCrumbParentname']='entry';

				if(!empty($date))
	            $data['date'] = $date;
	            else
	            $data['date'] = date('Y-m-d');
				
				$data['curenturl']= 'addNewBoarding';

				
	          
	            
	            $data['allpets'] = $this->db2->get_where('pets',array('app_user_id'=>$this->session->userdata('id'),'is_deceased'=>0,'is_deleted'=>0))->result_array();
	            
	            $data['app_users_details'] = $this->session->userdata();
	           
	            
	            
				$data['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
				$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
	        	$data['boardingInventory'] = $this->Management_model->getBoardingInventory();
	        	$data['playareas'] = $this->db->get_where('playareas',array('is_deleted'=>0))->result_array();
	        	
	        	$data['category'] = 'boarding'; 
        	
        		$data['active_tab'] = 'boardings'; 
				$data['breadCrumbname']='boardings';
				$data['parent_url'] = 'boardings';
        	
        		$this->db->where('date(boarding_schedule.start) >=',date('Y-m-d'));
        		$data['boarding_schedule'] = $this->db->get_where('boarding_schedule')->result_array();

        		// if($data['boardingSettings']['catBoardingPricing'] == 0 && $data['boardingSettings']['dogBoardingPricing'] == 0)
        		// 	$this->message->custom_error_msg(referrer(),'You must add boarding price');

        		// if(empty($data['boardingInventory']))
        		// 	$this->message->custom_error_msg(referrer(),'You must add boarding locations');

        		// if(empty($data['boarding_schedule']))
        		// 	$this->message->custom_error_msg(referrer(),'You must add schedule');

        		// if($data['userdata']['boarding_switch'] == 0 || $data['userdata']['daycare_switch'] == 0)
					// $this->message->custom_error_msg(referrer(), 'Boarding service is disabled.');


	        	
	        	$data['boardingAdditionalParametersForPriceCalculation'] = $this->Management_model->getboardingAdditionalParametersForPriceCalculation();

			
				
				$this->load->view('user/basetemplate', $data);
			}

    	}else{
    		$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        

	        if(empty($data['pet_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'pet_id missing','errcode' => 404));
	        }

	        if(empty($data['start_date']))
	        {
	             $this->response(array('error' => true, 'message' => 'start_date missing','errcode' => 404));
	        }

	         if(empty($data['end_date']))
	        {
	             $this->response(array('error' => true, 'message' => 'end_date missing','errcode' => 404));
	        }

	        if(empty($data['category']))
	        {
	            $this->response(array('error' => true, 'message' => 'category missing','errcode' => 404));
	        }


	        if($data['start_date'] >= $data['end_date'])
	            $this->response(array('error' => true, 'message' => 'end_date should be greater than normal date','errcode' => 404));

	        if(!$this->GlobalApp_model->checkShopCanTakeBoarding($shop_no))
	        {    //check if shop can take boarding and day care request
	            $this->response(array('error' => true,'message' => 'Shop cannot take Boarding request completed profile','errcode'=>412));
	        }

	     

	        if (empty($appuser['customerStripeToken']) && $this->Appuser_model->checkUserCard($appuser)) {
	            $this->response(array('error' => true, 'message' => 'User default credit card details not found','errcode' => 110));
	        }

	       
	        
	        $data['start_time'] = "11:00:00";
	        $data['end_time'] = "11:00:00";

	        if($this->Management_model->checkBoardingExistForPet($data['pet_id'],date('H:i:s',strtotime($data['start_time'])),date('H:i:s',strtotime($data['end_time'])),$data['start_date'],$data['end_date'],$data['category']))
	        {
	            $this->response(array('error' => true, 'message' => 'Appointment already exist for pet','errcode' => 406));
	        }

	        $pet = $this->Appuser_model->getPetDetails($appuser['id'],$shop_no,array('id'=>$data['pet_id']));
	        $size_priority = getSizePriority($pet['size']);

	        if($date = $this->Appuser_model->checkStartDateBoarding($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'No booking available for check in date '.date('d M Y',strtotime($date)),'errcode' => 461));
	        }

	        if($date = $this->Appuser_model->checkEndDateBoarding($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'No booking available for check out date'.date('d M Y',strtotime($date)),'errcode' => 462));
	        }


	        if($this->Appuser_model->checkBoardingifAvailable($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'Sorry Slot has got booked','errcode' => 450));
	        }


	        $data['overlapping_appointment'] = 0;
	       
	        $finalAmount = floatval($data['cost'] + $data['stripe_charge'] + $data['commission']);

	            if($finalAmount > 0 )
	            {
	               
	                    try 
	                    {
	                        
	                        if($this->Appuser_model->addNewBoarding($appuser,$data,$chargeArray,$finalAmount,$shop_no))
	                        {
	                            $this->response(array('error' => false,'message' => 'Appointment Added successfully','errcode' => 0));   
	                        }
	                        else
	                        {
	                            $this->response(array('error' => true,'message' => 'Something went wrong','errcode' => 405));  
	                        }



	                    }
	                    catch (Exception $e)
	                    {
	                        $error = $e->getMessage();
	                        $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
	                    }
	            }
    	}
    }

    function addNewAppointment($id = null){
    	
    	if(!empty($this->session->userdata())){
    		
    		if($this->input->post())
			{	
				$data = $this->input->post();
				
					//For New Appointment
				$this->form_validation->set_rules('pet_id', lang('pet'), 'trim|required|xss_clean');
		        $this->form_validation->set_rules('app_user_id', lang('appointmentedBy'), 'trim|required|xss_clean');
		    
		        if ($this->form_validation->run()== TRUE) {

					$data['appointment_date'] = createDate($data['appointment_date']);
		            $data['app_user_id'] =  $this->session->userdata('id');
		            $data['user_phone_no'] = preg_replace("/[^a-zA-Z0-9]+/", "", $data['user_phone_no']);
		            $this->db2->update('app_users',array('phone'=>$data['user_phone_no']),array('id'=>$this->session->userdata('id')));

		            $data['bookingArray'] = trim($data['bookingArray']);
		            $AppointmentIds = explode(" ", $data['bookingArray']);

		           
		            $data['deposit'] = floatval($data['deposit']/count($AppointmentIds));
		           
		            foreach ($AppointmentIds as $appointmentId) {
		            	$insertArray = array();
		            	$insertArray = $this->Appuser_model->getSingleAppointmentId($appointmentId);
		            	
		            
		            	$pet = $this->Appuser_model->getPetDetails($this->session->userdata('id'),$this->session->userdata('shop_no'),array('id'=>$insertArray['pet_id']));
		            	$this->db->update('appointments',array('isDuplicate'=>'0','status'=>'Confirm', 'deposit'=>$data['deposit']),array('id'=>$appointmentId));

		            	//Sync Appointment data for superadmin in main db
		        		
		        		$insert = array();
		        		$insert['category'] = 'appointment';
		        		$insert['shop_id'] = $this->session->userdata('shop_id');
		        		$insert['amount'] = $insertArray['cost'];
		        		$insert['reference_id'] = $appointmentId;
		        		$insert['stripe_charge'] =0;
		   				$insert['commission'] = 0;
		        		$insert['added_on'] = date('Y-m-d H:i:s');
		        		$insert['status'] = 'pending';
		        		$insert['transcation_type']='payment';
		        		$insert['app_user_id'] = $insertArray['app_user_id'];
		        		$insert['payment_mode'] = 'offline';
		        		$insert['start'] = date('Y-m-d H:i:s',strtotime($insertArray['appointment_date'].' '.$insertArray['appointment_time']));
	                    $insert['end'] = date('Y-m-d H:i:s',strtotime($insertArray['appointment_date'].' '.$insertArray['appointment_end_time']));
					   	$insert['timezone'] = get_option('time_zone');
					   	$insert['cancel_by_id'] = 0;
					   	$insert['pet_id'] = $insertArray['pet_id'];
					   	$insert['type'] = 'full';
					   	$insert['ref_app_id'] = 0;
					   	$insert['invoice_id'] = $appointmentId;
					   	if($data['deposit'] > 0){
					   		$insert['amount'] = $insert['amount'] - $data['deposit'];
					   		$insert['type'] = 'balance';
					   	}
					  
		        		$this->db2->insert('transactions',$insert);

		        		$this->Management_model->sendAppointmentEmailToUser($insertArray,$pet);
		        		

		        		if($data['deposit'] > 0){
		        			$insert['amount'] = $data['deposit'];
		        			$insert['type'] = 'deposit';
		        			$insert['payment_source'] = $data['payment_source'];
		        			$insert['other_source'] = $data['other_source'];
		        			$insert['status'] = 'success';
		        			$this->db2->insert('transactions',$insert);
		        		}	
		            }
		        	$this->message->save_success('User/appointments');
		        }
		        else 
		        {
		            $error = validation_errors();
		            $this->message->custom_error_msg('User/addNewAppointment',$error);
	        	}
		    }
		    else 
		    {
		    	//new appointment view
		    	$data = $this->data;
		    	$data['pageViewTitle'] = 'addNewAppointment';
		    	$data['services'] = $this->GlobalApp_model->getAllServices();
		    	$data['petSize'] = $this->Appuser_model->getPetSize();
	         
	            $data['breadCrumbname']='Add New Appointment';
	           

	            $data['groomers'] = $this->db->query("Select admin_users.* , sum(TIME_TO_SEC(TIMEDIFF(groomer_schedule.end,groomer_schedule.start))) - (select IFNULL(sum(TIME_TO_SEC(TIMEDIFF(appointments.appointment_end_time,appointments.appointment_time))),0) from appointments where appointments.user_id = admin_users.id and status != 'Cancel') as booking_time from admin_users join groomer_schedule on admin_users.id = groomer_schedule.user_id where admin_users.is_deleted !='1' AND admin_users.groomer !='0' AND groomer_schedule.date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 30 DAY) group by admin_users.id")->result_array();
	            $id = get_decode($id);
	            $data['petDetails'] = $this->Appuser_model->getPetDetails($this->session->userdata('id'),$this->session->userdata('shop_no'),array('id'=>$id));
	            // pr($data['petDetails']);exit;
	          	$i = 0;
	            foreach ($data['groomers'] as $key => $value) {
	            	
		            $query =$this->db->select('users_modules.module_id')
						->from('users_modules')
						->where_in('module_id',array(5,6))
						->where('users_modules.user_id', $value['id'])
						->get()->row_array();
		             	if(!empty($query)){
		             		
		             		$groomers[$i] = $value;

		             	}
		        		
						$i++;
	            }
	            $data['pageViewTitle'] = 'add_reservation';
	            $data['groomers'] =$groomers;
		        $data['header'] = TRUE;
				$data['_view'] = 'addNewAppointment';
				$data['sidebar'] = TRUE;
				$data['footer'] = TRUE;
				$data['parent_tab'] = 'entry';
				$data['active_tab'] = 'getAllAppointments';
				$data['user']=$this->data['userdata'];
				$data['breadCrumbParentname']='Appointments List';
				$data['parent_url'] = 'getAllAppointments';
				$data['curenturl']= 'addNewAppointment';
				$data['shopWorkingDays'] = $this->Management_model->getShopWorkingDaysForDatePicker();
				$data['shopAvailabilty'] = $this->Management_model->getshopAvailabilty();

		        $this->load->view('user/basetemplate', $data);
	    	}  
    	}else{
    		$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        if(empty($data['services'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'Atleast one service is needed','errcode' => 407));
	        }

	        if(empty($data['pet_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'pet_id missing','errcode' => 404));
	        }

	        if(empty($data['user_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'user_id missing','errcode' => 404));
	        }

	        if(empty($data['date'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'date missing','errcode' => 404));
	        }

	        if(empty($data['appointment_time'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'start time missing','errcode' => 404));
	        }

	        if(empty($data['appointment_end_time']))
	        {
	             $this->response(array('error' => true, 'message' => 'end time missing','errcode' => 404));
	        }

	        if(empty($data['allocated_schedule_id']))
	        {
	             $this->response(array('error' => true, 'message' => 'allocated_schedule id missing','errcode' => 404));
	        }

	        if(!$this->GlobalApp_model->checkShopCanTakeAppointment($shop_no))
	        {
	            $this->response(array('error' => true,'message' => 'Shop cannot take appointment has not completed profile','errcode'=>201));
	        }



	        if (empty($appuser['customerStripeToken']) && $this->Appuser_model->checkUserCard($appuser)) {
	            $this->response(array('error' => true, 'message' => 'User default credit card details not found','errcode' => 110));
	        }

	        if(!$this->Appuser_model->checkAppointmentifAvailable($appuser,$data,$shop_no))
	        {
	            $this->response(array('error' => true, 'message' => 'Sorry Slot has got booked','errcode' => 450));
	        }
	               
	        //exit;
	        if($this->Management_model->checkBoardingExistForPet($data['pet_id'],date('H:i:s',strtotime($data['appointment_time'])),date('H:i:s',strtotime($data['appointment_end_time'])),$data['date'],$data['date'],'appointment'))
	        {
	            $this->response(array('error' => true, 'message' => 'Appointment already exist for pet','errcode' => 406));
	        }

	        $data['overlapping_appointment'] = 0;
	        $data['extented_time'] = 0;
	       
            $finalAmount = floatval($data['cost'] + $data['stripe_charge'] + $data['commission']);

            if($finalAmount > 0 )
            {
                    try 
                    {
                        if($this->Appuser_model->addNewAppointment($appuser,$data,$chargeArray,$finalAmount,$shop_no))
                        {
                            $this->response(array('error' => false,'message' => 'Appointment Added successfully','errcode' => 0));   
                        }
                        else
                        {
                            $this->response(array('error' => true,'message' => 'Something went wrong','errcode' => 405));  
                        }

                    }
                    catch (Exception $e)
                    {
                        $error = $e->getMessage();
                        $this->response(array('error' => true, 'message' => $error,'errcode' => 405));
                    }
            }
    	}
    }

    function getBoardingCalendar()
    {	
    	if(!empty($this->session->userdata())){
    		if($this->input->post())
	        {
	            $data = $this->input->post();
	            if($result = $this->Appuser_model->getBoardingInventoryAvailability($data['pet_id'],$this->session->userdata(),$this->session->userdata('shop_no')))
		        {
		            $this->session->set_flashdata('error', 'No Location/kennels found for your pet with size');
		        }
		        if($result = $this->Appuser_model->getBoardingScheduleAvailabilty($data['pet_id'],$this->session->userdata(),$this->session->userdata('shop_no')))
		        {
		            $this->session->set_flashdata('error', 'No bookings are available');
		        }


		        if($result = $this->Appuser_model->getBoardingCalendar($data['pet_id'],$this->session->userdata(),$this->session->userdata('shop_no')))
		        {
		        	$this->session->set_flashdata('success', 'Calendar Data Fetch Successfully');
		        }
		    }else{
		    	$data = $this->data;
				$data['header'] = TRUE;
				$data['_view'] = 'getBoardingCalendar';
				$data['sidebar'] = TRUE;
				$data['footer'] = TRUE;
				$data['active_tab'] = 'getBoardingCalendar'; 
				$data['parent_tab'] = 'getBoardingCalendar';
				$data['user']=$this->data;
				$data['breadCrumbname']='getBoardingCalendar';
				$data['company_name'] = get_option('company_name');
				 //Breadcrumb Name
				$data['petlist'] = $this->Management_model->getPetsHavingBirthdayToday();
				// $data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#00c0ef','Inprocess'=>'#f39c12');
				// $data['boardings'] = $this->Management_model->getBoardingCalendarView($date); 

				// if(!empty($date))
				// 	$data['date'] = $date;
				// else
				// 	$data['date'] = date('Y-m-d');

				// $data['category'] = $category;


				$this->load->view('user/basetemplate', $data);
		    }
    	}else{
	        $shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        if(empty($data['category'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'Atleast one category is needed','errcode' => 407));
	        }

	        

	        if(empty($data['pet_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'pet_id missing','errcode' => 404));
	        }

	        if($result = $this->Appuser_model->getBoardingInventoryAvailability($data['pet_id'],$appuser,$shop_no))
	        {
	            $this->response(array('error' => true,'message' => 'No Location/kennels found for your pet with size '.$result,'errcode' => 481));
	        }

	        if($result = $this->Appuser_model->getBoardingScheduleAvailabilty($data['pet_id'],$appuser,$shop_no))
	        {
	            $this->response(array('error' => true,'message' => 'No bookings are available','errcode' => 482));
	        }


	        if($result = $this->Appuser_model->getBoardingCalendar($data['pet_id'],$appuser,$shop_no))
	        {
	            $this->response(array('error' => false,'message' => 'Calendar Data Fetch Successfully','errcode' => 0,'result'=>$result));
	        }
	        else
	            $this->response(array('error' => true,'message' => 'No Schedule or Inventory Found','errcode' => 480));

	    }
    }

    function getActiveAppointments()
    {
    	if(!empty($this->session->userdata())){
    		$get =$_GET;
	        $data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'activeAppointments';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getActiveAppointments'; 
	        $data['user']=$this->data;
	        $data['appointments'] = $this->Appuser_model->getAllAppointments($this->session->userdata('id'),$this->session->userdata('shop_no'),'',$get['pageData']);
	        
	        if(isset($get['status']) && !empty($get['status']))
	             $data['appointments'] = $this->Appuser_model->getAllAppointmentsforFilter($this->session->userdata('id'),$this->session->userdata('shop_no'),$this->session->userdata(),$get['pageData'],$get['status']);
	        else    
	             $data['appointments'] = $this->Appuser_model->getAllAppointments($this->session->userdata('id'),$this->session->userdata('shop_no'),$this->session->userdata(),$get['pageData']);

	        //   pr($data['appointments']);exit;
	        $this->load->view('user/basetemplate', $data);

	       
		}else{
			redirect('user/login');
		}
		
		// else{

	    //     $shop_no =  trim($this->uri->segment(1));

	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	    //     $appuser = $this->GlobalApp_model->checkToken();

	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }

	    //     if($appuser['is_banned'] != 0)
	    //     {
	    //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	    //     }

	    //     $get = $this->get();
	       
	    //     if(empty($get))
	    //     {
	    //          $this->response(array('error' => true, 'message' => 'Require pageData as get parameter ','errcode' => 400));
	    //     }
	        
	    //     if(isset($get['status']) && !empty($get['status']))
	    //         $result = $this->Appuser_model->getAllAppointmentsforFilter($appuser['id'],$shop_no,$appuser,$get['pageData'],$get['status']);
	    //     else    
	    //         $result = $this->Appuser_model->getAllAppointments($appuser['id'],$shop_no,$appuser,$get['pageData']);

	        
	        
	    //     if(!empty($result))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'Details fetch successfully','errcode' => 0,'result'=>$result));
	    //     }
	    //     else
	    //     {
	    //         $this->response(array('error' => false,'message' => 'No Appointments found','errcode' => 0));
	    //     }
	    // }
    }

	function getCheckoutAppointments()
    {
    	if(!empty($this->session->userdata())){
    		$get =$_GET;
    		
	        $data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'checkoutAppointments';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getCheckoutAppointments'; 
	        $data['user']=$this->data;
			$data['CheckoutAppointments'] = $this->Appuser_model->getCheckoutAppointments1($this->session->userdata('id'),$this->session->userdata('shop_no'));
			$data['CancelAppointments'] = $this->Appuser_model->getCancelAppointments($this->session->userdata('id'),$this->session->userdata('shop_no'));

			// $data['appointments'] = $this->Appuser_model->getCheckoutAppointments($this->session->userdata('id'),$this->session->userdata('shop_no'),'',$get['pageData']);
	        // pr($data['CancelAppointments']);exit;
	        $this->load->view('user/basetemplate', $data);

	       
		}else{
			redirect('user/login');
		}
		
		// else{

	    //     $shop_no =  trim($this->uri->segment(1));

	    //     if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	    //         $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	    //     $appuser = $this->GlobalApp_model->checkToken();

	    //     if(empty($appuser))
	    //     {
	    //         $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	    //     }

	    //     if($appuser['is_banned'] != 0)
	    //     {
	    //         $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	    //     }

	    //     $get = $this->get();
	       
	    //     if(empty($get))
	    //     {
	    //          $this->response(array('error' => true, 'message' => 'Require pageData as get parameter ','errcode' => 400));
	    //     }
	        
	    //     if(isset($get['status']) && !empty($get['status']))
	    //         $result = $this->Appuser_model->getAllAppointmentsforFilter($appuser['id'],$shop_no,$appuser,$get['pageData'],$get['status']);
	    //     else    
	    //         $result = $this->Appuser_model->getCheckoutAppointments($appuser['id'],$shop_no,$appuser,$get['pageData']);

	        
	        
	    //     if(!empty($result))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'Details fetch successfully','errcode' => 0,'result'=>$result));
	    //     }
	    //     else
	    //     {
	    //         $this->response(array('error' => false,'message' => 'No Appointments found','errcode' => 0));
	    //     }
	    // }
    }

    function validateBoarding(){
    	if(!empty($this->session->userdata())){
    	   $post = $this->input->post();
	       $result  = array();
	       $return  = array();
	       
	       
	       if(!empty($post))
	       {
	           $pet_id = $post['pet_id'];
	           $start_time = date('H:i:s', strtotime($post['start_time']));
	           $end_time = date('H:i:s', strtotime($post['end_time']));

	           $start_date = createDate($post['start_date']);
	           $end_date = createDate($post['end_date']);
	           $category = $post['category'];
	           $return['overbookingReason'] = '';
	           
				if($this->Management_model->checkBoardingExistForSamePet($pet_id,$start_time,$end_time,$start_date,$end_date,$category,0))
				{
					$return['already_appointment_exist'] = 1;
				}else
				{
					$return['already_appointment_exist'] = 0;
				}

	            
	            if($category == 'daycare')
	            {
	                $playareas = $this->db->get_where('daycare_schedule',array('date'=>$start_date,'current_capacity >'=> 0))->row_array();
	               
	                if(!empty($playareas))
	                {
	                    $return['overbooking'] = 0;
	                }
	                else
	                {
	                    $return['overbooking'] = 1;
	                    $return['overbookingReason'] = 'Playarea is full';

	                    $this->db->select('daycare_schedule.*,playareas.name as name ');
	    				$this->db->join('playareas','playareas.id = daycare_schedule.playarea_id');
	                    $playareas = $this->db->get_where('daycare_schedule',array('date'=>$start_date))->result_array();


	                    
	                   	$string = '<select class="form-control playareaHtml">';
						foreach ($playareas as $row) {
							$string .= '<option value="'.$row['playarea_id'].'">'.$row['name'].'</option>';
						}
						$string .= '</select>';
						$return['inventoryHtml'] = $string;
	                }
	            }
	            else
	            {
	            	$startDate = date('Y-m-d H:i:s', strtotime($start_date.' '.$start_time));
					$start_date = server_date($startDate,'Y-m-d');

					$endDate = date('Y-m-d H:i:s', strtotime($end_date.' '.$end_time));
					$end_date = server_date($endDate,'Y-m-d');

	                $pet = $this->Entry_model->getPetsViewDetails($pet_id);
	                $size_priority = getSizePriority($pet['size']);


	                $result = $this->db->query("select boarding_inventory.* from boarding_inventory where id not in (SELECT boardings.inventory from boardings where boardings.status not in ('Cancel', 'Checkout') and boardings.inventory_type = 'boarding' and category = 'boarding' and boardings.start_date < '".$end_date."' and boardings.end_date > '".$start_date."') and size_priority >= '".$size_priority."' order by size_priority asc limit 1")->row_array();


	                if(!empty($result))
	                {
	                    $return['overbooking'] = 0;
	                }
	                else
	                {
	                    $return['overbooking'] = 1;
	                    $return['overbookingReason'] = 'Inventory is full';

	                    $inventoryDetails = $this->db->query("select boarding_inventory.* from boarding_inventory where id in (SELECT boardings.inventory from boardings where boardings.status not in ('Cancel', 'Checkout') and boardings.inventory_type = 'boarding' and category = 'boarding' and (boardings.start_date BETWEEN '" . $start_date . "' AND  '" . $end_date . "' OR boardings.end_date BETWEEN '" . $start_date . "' AND  '" . $end_date . "') OR boardings.start_date > '" . $start_date . "' AND  boardings.end_date > '" . $end_date . "' GROUP BY inventory HAVING COUNT(inventory) ='1') order by size_priority asc ")->result_array();

		            	
			           	$checkFlag = 0;
			           	$string = '';
			           	if(!empty($inventoryDetails)){
			           		$string = '<select class="form-control playareaHtml">';
				            foreach ($inventoryDetails as $key => $value) {
				                $string .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
				            }
				            $string .= '</select>';
			           	}
			            $return['inventoryHtml'] = $string;
	                }
	            }
		        
	            $msg = array('error'=>0,'msg'=>'verification done successfully','result'=>$return);
	           	echo json_encode($msg);
	       }
	   }else{
	   		$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $data = $this->post();

	        if ($data == null) {
	            $this->response(array('error' => true,'message' => 'invalid_json','errcode' => 400));
	        }

	        

	        if(empty($data['pet_id'])) 
	        {
	            $this->response(array('error' => true, 'message' => 'pet_id missing','errcode' => 404));
	        }

	        if(empty($data['start_date']))
	        {
	             $this->response(array('error' => true, 'message' => 'start_date missing','errcode' => 404));
	        }

	         if(empty($data['end_date']))
	        {
	             $this->response(array('error' => true, 'message' => 'end_date missing','errcode' => 404));
	        }

	        if(empty($data['category']))
	        {
	            $this->response(array('error' => true, 'message' => 'category missing','errcode' => 404));
	        }


	        if($data['start_date'] >= $data['end_date'])
	            $this->response(array('error' => true, 'message' => 'end_date should be greater than normal date','errcode' => 404));


	     

	        if (empty($appuser['customerStripeToken']) && $this->Appuser_model->checkUserCard($appuser)) {
	            $this->response(array('error' => true, 'message' => 'User default credit card details not found','errcode' => 110));
	        }

	        // if(!$this->Appuser_model->checkAppointmentifAvailable($appuser,$data,$shop_no))
	        // {
	        //     $this->response(array('error' => true, 'message' => 'Sorry Slot has got booked','errcode' => 450));
	        // }

	        
	        $data['start_time'] = "11:00:00";
	        $data['end_time'] = "11:00:00";


	               

	        if($this->Management_model->checkBoardingExistForPet($data['pet_id'],date('H:i:s',strtotime($data['start_time'])),date('H:i:s',strtotime($data['end_time'])),$data['start_date'],$data['end_date'],$data['category']))
	        {
	            $this->response(array('error' => true, 'message' => 'Appointment already exist for pet','errcode' => 406));
	        }

	        $pet = $this->Appuser_model->getPetDetails($appuser['id'],$shop_no,array('id'=>$data['pet_id']));
	        $size_priority = getSizePriority($pet['size']);

	        if($date = $this->Appuser_model->checkStartDateBoarding($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'No booking available for check in date '.date('d M Y',strtotime($date)),'errcode' => 461));
	        }

	        if($date = $this->Appuser_model->checkEndDateBoarding($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'No booking available for check out date'.date('d M Y',strtotime($date)),'errcode' => 462));
	        }


	        if($this->Appuser_model->checkBoardingifAvailable($appuser,$data,$shop_no,$size_priority))
	        {
	            $this->response(array('error' => true, 'message' => 'Sorry Slot has got booked','errcode' => 450));
	        }
	        else
	        {
	             $this->response(array('error' => false,'message' => 'Boarding is available','errcode' => 0));  
	        }
	   }
    }

    //extra functions for web panel

    // booking boarding and daycare
    function checkBoardingTimeValidtiy()
    {
    	if(!empty($this->session->userdata())){
	    	$start_date = createDate($this->input->post('start_date'));
	    	$end_date = createDate($this->input->post('end_date'));
	    	$time = $this->input->post('start_time');
	    	$time2 = $this->input->post('end_time');
	    	$category = $this->input->post('category');
	    	$pet_id = $this->input->post('pet_id');
	    	
	    	if(empty($start_date))
	    	{
	    		$msg = array('err' => 1000, 'message' => 'Please enter check in date');
	    		echo json_encode($msg);
	    		exit;
	    	}

	    	if($category == 'boarding')
	    	{
	    		//check for boarding times	
	    		if(date("Y-m-d") > date('Y-m-d',strtotime($start_date)))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'Check in date can be today or greater than it');
	    			echo json_encode($msg);
	    			exit;
	    		}


	    		if(empty($end_date))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'Check out date is required');
	    			echo json_encode($msg);
	    			exit;
	    		}


	    		if(date('Y-m-d',strtotime($end_date)) < date('Y-m-d',strtotime($start_date)))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'Check out date should be greater than check in date ');
	    			echo json_encode($msg);
	    			exit;
	    		}
	    		
	    		$pet = $this->Entry_model->getPetsViewDetails($pet_id);
	    		$size_priority = getSizePriority($pet['size']);
	    		
	    		$b = $this->db->get_where('boarding_schedule',array('date(start)'=>$start_date))->result_array();

	    		if(empty($b))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'No schedule found for check in date '.date(get_option('date_format'),strtotime($start_date)));
	    			echo json_encode($msg);
	    			exit;
	    		}

	    		$c = $this->db->get_where('boarding_schedule',array('date(end)'=>$end_date))->result_array();
	    		if(empty($c))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'No schedule found for check out date '.date(get_option('date_format'),strtotime($end_date)));
	    			echo json_encode($msg);
	    			exit;
	    		}

			    $date1=date_create($end_date);
				$date2=date_create($start_date);
				$diff=date_diff($date1,$date2);
				$difference = $diff->format('%d');

				if($diff > 0)
				{
					for($i=1;$i<$difference;$i++)
					{
						$newDate = date('Y-m-d',strtotime($start_date.' +'.$i.'days'));
						$b = $this->db->get_where('boarding_schedule',array('date(start)'=>$newDate))->result_array();
			    		if(empty($b))
			    		{
			    			$msg = array('err' => 1000, 'message' => 'No schedule found for check in  on '.date(get_option('date_format'),strtotime($newDate)));
			    			echo json_encode($msg);
			    			exit;
			    		}
					}
				}

	    		$msg = array('err' => 0, 'message' => 'Boarding Slot is available');
	    		echo json_encode($msg);
	    		exit;
	    	}
	    	else if($category == 'daycare' && !empty($time))
	    	{
	    		//check for day care timings

	    		if(date("Y-m-d") > strtotime($start_date))
	    		{
	    			$msg = array('err' => 1000, 'message' => 'Check in date be today or greater than it');
	    			echo json_encode($msg);
	    			exit;
	    		}

		    	$result = $this->db->get_where('daycare_schedule',array('date'=>$start_date))->row_array();
		    
		    	if(!empty($result))
		    	{
		    		$result = daycare_scheduleTimeConverter($result);

		    		$date1 = date('Y-m-d H:i:s', strtotime($start_date.' '.$time));
					$date2 = date('Y-m-d H:i:s', strtotime($result['start']));
					$date3 = date('Y-m-d H:i:s', strtotime($result['end']));
					$date4 = date('Y-m-d H:i:s', strtotime($start_date.' '.$time2));
					
		    		if($date1 >=  $date4)
					{
						$msg = array('err' => 1000, 'message' => 'Check out time should be greater than check in time');
						echo json_encode($msg);
						exit;
					}

					if(!empty($time2))	 
					{	
						
						if ($date1 >= $date2 && $date1 < $date3 && $date4 > $date2 && $date3 >= $date4)
						{
						   $msg = array('err' => 0, 'message' => 'Day Care Slot is availablt');
						}
						else
						{
							$msg = array('err' => 1000, 'message' => 'Day care is available from '.date('h:i a',strtotime($result['start'])).' to '.date('h:i a',strtotime($result['end'])));
						}

						echo json_encode($msg);
		    			exit;
					}
				}
				else
				{
					$msg = array('err' => 1000, 'message' => 'Day Care is not available on '.date(get_option('date_format'), strtotime($start_date)));
					echo json_encode($msg);
		    		exit;
				}	    		
	    	}
	    	else
	    	{  
	    	}
	    }
    }

    function addNewBoardingAjax()
    {
    	if(!empty($this->session->userdata())){
	    	$msg = '';
	    	$data = $this->input->post();

	    	if($data['amount'] < $data['deposit']){
	       		$msg = array('errcode' => 1000,'message'=>'Deposit must be less than service cost.');
	       	}
	       	$appUser = $this->Management_model->getSingleUserData($data['app_user_id']);
	       	if($data['deposit']<0 || empty($data['deposit'])){
	       		$data['deposit'] = 0;
	       	}
	       	if($appUser[0]['is_deposited'] ==1 && $data['deposit'] == 0){
	       		$msg = array('errcode' => 1000,'message'=>'Deposit is mandatory.');
	       	}
	       	
	       	if(!empty($msg)){
	       		echo json_encode($msg);exit;  
	       	}


			if($this->input->post())
			{	
				//For New Appointment

				$this->form_validation->set_rules('pet_id', lang('pet'), 'trim|required|xss_clean');
		        $this->form_validation->set_rules('app_user_id', lang('appointmentedBy'), 'trim|required|xss_clean');
		      	
		        if ($this->form_validation->run()== TRUE) {

					if($result=$this->Management_model->addNewBoardingAjax($data,$data['category'],$appUser))
		        	{
		        		$msg = array('errcode' => 0,'message'=>"Appointment Booked successfully");
		        	}else{
		        		
		        		$msg = array('errcode' => 1000,'message'=>'Something Went Wrong');
		        	}
		        }
		        else 
		        {
		            $error = validation_errors();
		            $msg = array('errcode' => 1000,'message'=>$error);
	        	}
		    }
		    else
		    {
		    	$msg = array('errcode' => 1000,'message'=>'Paramter not found');
		    }
		    
		    echo json_encode($msg);exit;
		}
    }

    //appointment booking
    function appointmentBookingAjax($id = null)
	{	
		if(!empty($this->session->userdata())){	
			$msg = '';
	    	$data = $this->input->post();
	    	if($data['cost'] < $data['deposit']){
	       		$msg = array('errcode' => 1000,'message'=>'Deposit must be less than service cost.');
	       	}
	       	$appUser = $this->Management_model->getSingleUserData($data['app_user_id']);
	       	if($data['deposit']<0 || empty($data['deposit'])){
	       		$data['deposit'] = 0;
	       	}
	       	if($appUser[0]['is_deposited'] ==1 && $data['deposit'] == 0){
	       		$msg = array('errcode' => 1000,'message'=>'Deposit is mandatory.');
	       	}
	       
	       	if(!empty($msg)){
	       		echo json_encode($msg);exit;  
	       	}


			if($this->input->post())
			{	

				//For New Appointment
				$this->form_validation->set_rules('pet_id', lang('pet'), 'trim|required|xss_clean');
		        $this->form_validation->set_rules('app_user_id', lang('appointmentedBy'), 'trim|required|xss_clean');
		      	
		        if ($this->form_validation->run()== TRUE) {

					$insertArray = $data;
		           	$insertArray['appointment_date'] = createDate($insertArray['appointment_date']);
		            $insertArray['app_user_id'] =  $this->Global_model->getAppUserIdByPetId($insertArray['pet_id']);

		            $insertArray['user_phone_no'] = $appUser[0]['phone'];
	            	$insertArray['user_email'] = $appUser[0]['email'];
										
		            unset($insertArray['services']);
		            unset($insertArray['id']);

		            unset($insertArray['payment_source']);
		            unset($insertArray['other_source']);

		            $time = date("H:i:s", strtotime($insertArray['appointment_time']));
	        		$insertArray['appointment_time'] = $time;
	        		$insertArray['status'] = 'Confirm';
	        		$insertArray['extented_time'] = 0;
	        		$insertArray['created_by'] = $this->session->userdata('username');
	        		$insertArray['appointment_end_time'] = date("H:i:s", strtotime($insertArray['appointment_end_time']));
					$insertArray['timezone'] = get_option('time_zone');

	    			if($insertArray['overlapping_appointment'] == 1)
	    				 $insertArray['allocated_schedule_id'] = 0;


	    			$schedule_date = $insertArray['appointment_date'];

	        		$start = date('Y-m-d H:i:s',strtotime($insertArray['appointment_date'].' '.$insertArray['appointment_time']));

	            	$insertArray['discount_amount'] = 0;
	                $insertArray['discount_id'] = 0;

	            	if(empty($insertArray['deposit'])){
	            		$insertArray['deposit'] = 0 ;
	            	}
	            	$insertArray['credited_amount'] = 0 ;
	            	$insertArray['booked_from'] = 'desktop';
	            	$insertArray['inserted_cost'] = $insertArray['cost'];
	            	
	            	$insertArray['invoice_no'] = $this->Management_model->getinvoiceno();
	            	$insertArray['sms_code'] = $this->Management_model->randomString();
	            	$insertArray['sms_code'] = $insertArray['sms_code'].$this->session->userdata('shop_id').'G';
	            	//this is for convert system time into UTC time 
	            	$insertArray = groomingServerTimeConverter($insertArray);

	        		$this->db->insert('appointments',$insertArray);
	        		$appointmentId = $this->db->insert_id();
	        		
	        		$pet = $this->Entry_model->getPetsViewDetails($insertArray['pet_id']);

	        		/*********** New logic start   ******************/
	        		// if puppy power ic greater than 1 then create multiple records with isDublicate = 1
	        		if($insertArray['overlapping_appointment'] != 1 && $pet['puppy_power'] > 1)
	        		{
	        			$groomer_schedule  = $this->db->get_where('groomer_schedule',array('user_id'=>$insertArray['user_id'],'Date(date)'=>$schedule_date))->result_array();
	        			$insertArray1 = $insertArray;
	        			$powerCount = 1;
	        			foreach ($groomer_schedule as $key => $value) {
	        				
	        				if($value['id'] != $insertArray['allocated_schedule_id']){
	        					
	        					$insertArray1['allocated_schedule_id'] = $value['id'];
	        					$insertArray1['isDuplicate'] = '1'; 
	        					$powerCount++;
	        					$this->db->insert('appointments',$insertArray1);
	        				}
	        				if ($powerCount == $pet['puppy_power']) {
	    						break;
	    					}
	        			}
	        		}
	        		/*********** End New logic ******************/

	        		$i = 0; $commission = 0;
	        		foreach ($data['services'] as $key ) {
	        			
	        			$service = $this->db->get_where('services',array('id'=>$key))->row_array();
	   					$groomer_services = $this->db->get_where('groomer_services',array('user_id'=>$insertArray['user_id'],'service_id'=>$key))->row_array();

	   					if(!empty($groomer_services))
	   						$commission += ($service[$pet['size'].'_cost'] * ($groomer_services['commission']/100));

	        			if($i > 0)
	   						$start = $end;
	   		
	   					$end = date('Y-m-d H:i:s',strtotime($start.' +'.$service[$pet['size'].'_time_estimate'].' minutes'));


	        			$this->db->insert('appointment_services',array('service_id' => $key,'appointment_id'=>$appointmentId,'start'=>$start,'end' => $end));

	        		
	        			$i++;		
	        			 
	        		}

	        		if($commission > 0)
				    {
				    	$this->db->update('appointments',array('commission'=>$commission),array('id'=>$appointmentId));
				    }

	        		//Sync Appointment data for superadmin in main db
	        		$insertArray['id'] = $appointmentId;
	        		$insert = array();
	        		$insert['category'] = 'appointment';
	        		$insert['shop_id'] = $this->session->userdata('shop_id');
	        		$insert['amount'] = $insertArray['cost'];
	        		$insert['reference_id'] = $appointmentId;
	        		$insert['stripe_charge'] =0;
	   				$insert['commission'] = 0;
	        		$insert['added_on'] = server_date(date('Y-m-d H:i:s'),'Y-m-d H:i:s');
	        		$insert['status'] = 'pending';
	        		$insert['transcation_type']='payment';
	        		$insert['app_user_id'] = $insertArray['app_user_id'];
	        		$insert['payment_mode'] = 'offline';
	        		$insert['start'] = $start;
				   	$insert['end'] = $end;
				   	$insert['timezone'] = get_option('time_zone');
				   	$insert['cancel_by_id'] = 0;
				   	$insert['pet_id'] = $insertArray['pet_id'];
				   	$insert['type'] = 'full';
				   	$insert['ref_app_id'] = 0;
				   	$insert['invoice_id'] = $insertArray['invoice_no'];
				   	if($data['deposit'] > 0){
				   		$insert['amount'] = $insert['amount'] - $data['deposit'];
				   		$insert['type'] = 'balance';
				   	}

	        		$this->db2->insert('transactions',$insert);

	        		$this->Management_model->sendAppointmentEmailToUser($insertArray,$pet);

	        		if($data['deposit'] > 0){
	        			$insert['amount'] = $data['deposit'];
	        			$insert['type'] = 'deposit';
	        			$insert['payment_source'] = $data['payment_source'];
	        			$insert['other_source'] = $data['other_source'];
	        			$insert['status'] = 'success';
	        			$this->db2->insert('transactions',$insert);
	        		}
		        	$msg = array('errcode' => 0,'message'=>"Appointment Booked successfully");
		        }
		        else 
		        {
		            $error = validation_errors();
		            $msg = array('errcode' => 1000,'message'=>$error);
	        	}
		    }
		    else
		    {
		    	$msg = array('errcode' => 1000,'message'=>'Paramter not found');
		    }
		    
		    echo json_encode($msg);exit;    
		}   
    }

    function getGroomersOnDate(){
		if($this->input->post())
		{	
			$data = $this->input->post();
			// $date = date("Y-m-d",strtotime($data['date']));
			$date = date("Y-m-d");
			$data['groomers'] = $this->db->query("Select admin_users.* , sum(TIME_TO_SEC(TIMEDIFF(groomer_schedule.end,groomer_schedule.start))) - (select IFNULL(sum(TIME_TO_SEC(TIMEDIFF(appointments.appointment_end_time,appointments.appointment_time))),0) from appointments where appointments.user_id = admin_users.id and status != 'Cancel') as booking_time from admin_users join groomer_schedule on admin_users.id = groomer_schedule.user_id where admin_users.is_deleted !='1' AND admin_users.groomer !='0' AND groomer_schedule.date = '$date' group by admin_users.id")->result_array();
			// echo $this->db->last_query();exit;
			$i = 0;
            foreach ($data['groomers'] as $key => $value) {
            	
	            $query =$this->db->select('users_modules.module_id')
					->from('users_modules')
					->where_in('module_id',array(5,6))
					->where('users_modules.user_id', $value['id'])
					->get()->row_array();
	             	if(!empty($query)){
	             		
	             		$groomers[$i] = $value;

	             	}
	        		
					$i++;
            }
            $data['groomers'] =$groomers;

			$result = '';
			foreach ($data['groomers'] as $key => $value) {
				$result .='<option value="'.$value['id'].'">'.$value['first_name'].' '.$value['last_name'].'</option>';
			}
			$msg = array('errcode' => 0,'result'=>$result);
	    }
	    else
	    {
	    	$msg = array('errcode' => 1000,'message'=>'Paramter not found');
	    }
	    echo json_encode($msg);
	}

	function checkTimeValidtiy()
    {

    	$date = createDate($this->input->post('appointment_date'));
    	$time = $this->input->post('appointment_time');
    	$time = date('H:i:s',strtotime($time));
    	$time2 = $this->input->post('end_time');
    	$time2 = date('H:i:s',strtotime($time2));
    	$user_id = $this->input->post('user_id');
    	$services = $this->input->post('services');
    	$pet_id = $this->input->post('pet_id');

    	if(empty($pet_id) || $pet_id == null)
    	{
    		$msg = array('err' => 1000, 'message' => 'Please Select Pet');
			echo json_encode($msg);
			exit;
    	}

    	if(empty($user_id) || $user_id == null)
		{
			$msg = array('err' => 1000, 'message' => 'Please Select Groomer');
			echo json_encode($msg);
			exit;
		}

		if(empty($date))
		{
			$msg = array('err' => 1000, 'message' => 'Please Select Date');
			echo json_encode($msg);
			exit;
		}
		
	    if($time > $time2)
		{
			$msg = array('err' => 1000, 'message' => 'End time should be greater than start time');
			echo json_encode($msg);
			exit;
		}
	    		
    	
		$pet = $this->db2->get_where('pets',array('id'=>$pet_id))->row_array() ;
		$i ++;
		$time_estimate = 0;

		foreach ($services as $key => $value) {
	        $temp = $this->db->get_where('services',array('id'=>$value))->row_array();
	        $time_estimate += $temp[$pet['size'].'_time_estimate'];
    	}
    	

		$start = date('Y-m-d H:i:s',strtotime($date.' '.$time));
		$end = date('Y-m-d H:i:s',strtotime($start.' +'.$time_estimate.' minutes'));
		$time2 = date('H:i:s',strtotime($start.' +'.$time_estimate.' minutes'));


		$groomer_schedule = $this->db->get_where('groomer_schedule',array('date'=>$date,'user_id'=>$user_id))->row_array();

		$groomer_schedule = groomer_scheduleTimeConverter($groomer_schedule);
		
		if($time >= $groomer_schedule['blocker_start'] && $time < $groomer_schedule['blocker_end']){
			$msg = array('err' => 1000, 'message' => 'Groomer Blocked '.date('h:i a',strtotime($groomer_schedule['blocker_start'])).' to '.date('h:i a',strtotime($groomer_schedule['blocker_end'])).'.');
			 echo json_encode($msg);
			 exit;
		}

		
		if(($time >= $groomer_schedule['hard_blocker_start'] && $time < $groomer_schedule['hard_blocker_end']) || ($time2 > $groomer_schedule['hard_blocker_start'] && $time2 <= $groomer_schedule['hard_blocker_end']) || ($groomer_schedule['hard_blocker_start'] >= $time && $groomer_schedule['hard_blocker_end'] < $time2)){
			
			$msg = array('err' => 1000, 'message' => 'Groomer Blocked '.date('h:i a',strtotime($groomer_schedule['hard_blocker_start'])).' to '.date('h:i a',strtotime($groomer_schedule['hard_blocker_end'])).'.');
			echo json_encode($msg);
			exit;
		}


		if(($time >= $groomer_schedule['lunch_time_start'] && $time < $groomer_schedule['lunch_time_end']) || ($time2 > $groomer_schedule['lunch_time_start'] && $time2 <= $groomer_schedule['lunch_time_end']) || ($groomer_schedule['lunch_time_start'] >= $time && $groomer_schedule['lunch_time_end'] < $time2)){
			
			$msg = array('err' => 1000, 'message' => 'Lunch time '.date('h:i a',strtotime($groomer_schedule['lunch_time_start'])).' to '.date('h:i a',strtotime($groomer_schedule['lunch_time_end'])).'.');
			echo json_encode($msg);
			exit;
		}
		

		if(empty($groomer_schedule))
		{
			$msg =  array('err' => 1000, 'message' => 'Groomer is not available on following day');
			echo json_encode($msg);
			exit;
		}
		else
		{
			
			if($groomer_schedule['start'] <= $start && $groomer_schedule['end'] > $start)
			{

				
			}else{
				$msg = array('err' => 1000, 'message' => 'Groomer is available '.date('h:i a',strtotime($groomer_schedule['start'])).' to '.date('h:i a',strtotime($groomer_schedule['end'])).'.');
				echo json_encode($msg);
				exit;
			}
		}
		
		$msg = array('err' => 0, 'message' => 'Groomer  available');
		echo json_encode($msg);
		exit;	
	
    }

    function appointmentTimeNew(){

	  	$post = $this->input->post();
	  	$shop_no =  $_SESSION['shopno'];
	  	$data = array();
	  	$data['date'] = date('Y-m-d',strtotime($post['appointment_date']));
	  	$data['start_time'] = $post['appointment_time'];
	  	$data['services'] = implode(",", $post['services']);
	  	$data['app_user_id'] = $post['app_user_id'];
	  	$data['pet_id'] = $post['pet_id'];
	  	$data['user_id'] = $post['user_id'];
	  	
	 
	 

			$services = $post['services'];
			$new = array();
			$temp = 0;
			$tempArr = array();
			foreach ($services as $key => $value) {
			   $a =	explode(" ",$value);
			   $new[$a[1]] = $a[0];
			   if($temp == 0){
					$temp  = $a[0];
				}
				if($temp == $a[0]){
					$tempArr[$temp][] = $a[1];
					continue;
				}else{
					$temp = $a[0];
					$tempArr[$temp][] = $a[1];
				}
			}
	       	$post['extented_time'] = 0;
	       	$post['booked_from'] = 'desktop';


	       	
	       	$resultArr = array();
	       	$i = 0;
		   	foreach ($tempArr as $key => $value) {

		   		$result = array();
		   		$start_time = date('H:i:s',strtotime($data['start_time'][$i]));
		   		$user_id = $data['user_id'][$i];

		   		$groomer_schedule = $this->db->get_where('groomer_schedule',array('user_id'=>$user_id,'date'=>$data['date']))->row_array();
	  			$groomer_schedule = groomer_scheduleTimeConverter($groomer_schedule);

		   		
		   		$result = $this->Management_model->getBestAvailableTimeForSlot($data['date'],$value,$key,$appuser,$shop_no,$user_id,$start_time,$data['end_time']);
		   		
		   		$resultArr[] = $result;
		   		
		   		$result['appointment_end_timeExceed'] = 0;
		   		$petdetails = $this->Entry_model->getPetsViewDetails($key);
		   		if(!empty($result['already_appointment_exist'])){
		   			$result['overbooking'] = 0;
		   			$result['petdetails'] = $petdetails;
		   			$resultArr[$i] = $result;
		   		}else{
		   			if(isset($result['normal_booking_time']) && !empty($result['normal_booking_time'])){
		   				$result['overbooking'] = 0;
			   			$result['already_appointment_exist'] = 0;
			   			$result['appointment_time'] = date('h:i A',strtotime($result['normal_booking_time']['start']));
			   			$result['appointment_end_time'] = date('h:i A',strtotime($result['normal_booking_time']['end']));
			   		}else{
			   			$result['overbooking'] = 1;
			   			$result['already_appointment_exist'] = 0;
			   			$appointment_end_time = date('H:i:s',strtotime($start_time.' + '.$result['time_estimate'].' minutes'));
			   			$endTime = date('Y-m-d H:i:s',strtotime($data['date'].' '.$appointment_end_time));
			   			

			   			if($groomer_schedule['end'] < $endTime){
			   				$result['appointment_end_timeExceed'] = 1;
			   				$result['overbookingReason'] = "Close to end of groomer’s schedule";
			   			}
			   			$result['appointment_time'] = date('h:i A',strtotime($start_time));
			   			$result['appointment_end_time'] = date('h:i A',strtotime($start_time.' + '.$result['time_estimate'].' minutes'));
			   		}
			   		
		   			$result['petdetails'] = $petdetails;
		   			$result['appointmentId'] = $appointmentId;
		   			$resultArr[$i] = $result;
		   			
		   		}
		   		$i++;	
		   	}
		   
		  
	       	$msg = array('error'=>0,'msg'=>'verification done successfully','result'=>$resultArr);
	        echo json_encode($msg);exit;
  	}
    //functions not used for web panel 
    function getAllUserAndShopDetailBeforeAppointment()
    {
    	if(!empty($this->session->userdata())){
	        
	    }else{
	    	$shop_no =  trim($this->uri->segment(1));

	        if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no))
	            $this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));

	        $appuser = $this->GlobalApp_model->checkToken();
	        

	        if(empty($appuser))
	        {
	            $this->response(array('error' => true,'message' => 'You are not authorize to access api','errcode'=>104));
	        }

	        if($appuser['is_banned'] != 0)
	        {
	            $this->response(array('error' => true, 'message' => 'You have been blocked. Please contact support for more details ','errcode' => 103));
	        }

	        $shopBlocked = 0;
	        if($this->Appuser_model->checkIFShopIsBlocked($shop_no))
	        {
	            $shopBlocked = 1;   
	        }

	        $result = $this->Appuser_model->getAllUserAndShopDetailBeforeAppointment($appuser,$shop_no);
	        if($result)
	        {
	            $this->response(array('error' => false,'message' => 'Data Fetched Successfully','errcode' => 0,'result'=>$result,'shopBlocked'=>$shopBlocked));
	        }
	    }
    }

    function getShopNameAjax()
    {
    	$msg = array();
    	//Validating Shop no if exist or not
    	$phone_number = $this->input->post('shopno');
    	$result = $this->db2->get_where('shops',array('phone_number'=>trim($phone_number)))->row_array();
    	if(!empty($result))
    	{
    		//if the shop exists
    		
    		if($result['is_deleted'] == 1){
    			$msg = array('err' => 1000,'message'=>'Invalid Number');
    			
    		}
    		else{
    			$msg = array('err' => 0);
    		}
    	}
    	else
    	{
    		$msg = array('err' => 1000,'message'=>'Invalid Number');
    	
    	}

    	echo json_encode($msg);	
    }

    //get size by weight for pets 
   	function getsizebyweight($weight){
        if(!empty($weight)){
            $arrPetSize = $this->db->order_by('id','ASC')->get_where('pet_size_weight')->result_array();
            $size = $arrPetSize[4]['size_name'];
            if($weight >= 0 && $weight < $arrPetSize[0]['size_weight'] ) {
                $size = $arrPetSize[0]['size_name'];
            }else if($weight >= $arrPetSize[0]['size_weight'] && $weight < $arrPetSize[1]['size_weight'] ) {
                 $size = $arrPetSize[1]['size_name'];
            }else if($weight >= $arrPetSize[1]['size_weight'] && $weight < $arrPetSize[2]['size_weight'] ) {
                 $size = $arrPetSize[2]['size_name'];
            }else if($weight >= $arrPetSize[2]['size_weight'] && $weight < $arrPetSize[3]['size_weight'] ) {
                 $size = $arrPetSize[3]['size_name'];
            }else if($weight >= $arrPetSize[3]['size_weight'] && $weight < $arrPetSize[4]['size_weight'] ) {
                $size = $arrPetSize[4]['size_name'];
            }
            echo json_encode(strtolower($size));
        }
          
    }

   
    function addveterinarian(){
    	if(!empty($this->session->userdata())){
			
	     	if(!empty($_POST)){
				// $_POST['pet_id']= $_GET['id'];
				$get_vet = $this->db2->get_where('veterinarian',array('shop_id'=>$this->session->userdata('shop_id'),'pet_id'=>$_POST['pet_id']))->row_array();
				// pr($get_vet);exit;
				$vetArray = array();
				$vetArray['veterinarian'] = $_POST['veterinarian'];
				$vetArray['business_name'] = $_POST['vet_business_name'];
				$vetArray['phone'] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST['vet_phone_number']);
				$vetArray['phone1'] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST['vet_phone_number1']);
				$vetArray['fax'] = $_POST['fax'];
				$vetArray['address'] = $_POST['vet_address'];
				// pr($get_vet);exit;

	            if(!empty($get_vet)){
	               $this->db2->update('veterinarian',$vetArray,array('shop_id'=>$this->session->userdata('shop_id'),'pet_id' =>$_POST['pet_id']));
	            }else{
	                if(empty($_POST['veterinarian']) && empty($_POST['vet_business_name']) && empty($_POST['vet_phone_number'])){
	                    $this->message->custom_error_msg('User/getPetDetails/'.$_POST['pet_id'].'/add_veterinarian', 'Cannot add Veterinarian Details,Please try again');
	                    
	                }else{
	                    $vetArray['added_on'] = date('Y-m-d :H:i:s');
	                    $vetArray['shop_id'] = $this->session->userdata('shop_id');
	                    $vetArray['pet_id'] = $_POST['pet_id'];
	                  
	                    $this->db2->insert('veterinarian',$vetArray);
	                    
	                }
	            }
	            // echo referrer();exit;
	            // pr(get_encode($_POST['pet_id']));exit;
				   $this->message->save_success('User/getPetDetails?id='.$_POST['pet_id'].'');
	                // $this->message->save_success(referrer().'/addveterinarian');
	       }   
	    }else{
			redirect('User/login');
	    }    
   	}

   	//add notes from all views 
	function addGroomerNote(){
       if($this->input->post())
       {
           
           $this->form_validation->set_rules('notes', 'Notes', 'required');
           if ($this->form_validation->run()== TRUE) {
			   $data = $this->input->post();
			   $data['shop_id'] = $this->session->userdata('shop_id');
			//    echo $data['shop_id'];exit;
               $data['added_on'] = date('Y-m-d H:i:s');
               $tableName = 'groomer_notes';
               if ($data['groomerType'] == 'medication notes') {
                   $data['added_by'] = 'shop';
                   $tableName = 'medication_notes';
               }else {
                   $data['note_type'] = $data['groomerType'];
                   if($data['note_type'] == 'behavior_note'){

                      $behavior_note = $data['notes'];

                      $behavior = $this->Management_model->getPetBehavior($_POST['pet_id']);

                      $this->Management_model->updatePetBehavior($behavior['behavior_id'],$behavior_note,$_POST['pet_id'],$_POST['user_id']);

                        //   if (strpos(referrer(), "getPetDetails") !== false){
	                    //    	 $this->message->save_success('User/getPetDetails/'.get_encode($_POST['pet_id']).'/additional_notes');

	                    //    }else if (strpos(referrer(), "viewAppointment") !== false){
	                    //       $this->message->save_success('User/viewAppointment/'.get_encode($_POST['appointment_id1']).'/additional_notes');

	                    //    }else if (strpos(referrer(), "viewBoarding") !== false){
	                    //       $this->message->save_success('User/viewBoarding/'.get_encode($_POST['appointment_id1']).'/additional_notes');

	                    //    }else if (strpos(referrer(), "viewDaycare") !== false){
	                    //       $this->message->save_success('User/viewDaycare/'.get_encode($_POST['appointment_id1']).'/additional_notes');
	                    //    }else{
	                          $this->message->save_success(referrer());
                       	//   }
                     }
                  }
              unset($data['groomerType']);
              unset($data['appointment_id1']);
// pr($data);exit;
               if($data['note_type'] != 'behavior_note' && $this->db2->insert($tableName,$data))
               {

               		// if (strpos(referrer(), "getPetDetails") !== false){
                    //   	$this->message->save_success('User/getPetDetails/'.get_encode($_POST['pet_id']).'/additional_notes');

	                // }else if (strpos(referrer(), "viewAppointment") !== false){
	                //       $this->message->save_success('User/viewAppointment/'.get_encode($_POST['appointment_id1']).'/additional_notes');

	                // }else if (strpos(referrer(), "viewBoarding") !== false){
	                //       $this->message->save_success('User/viewBoarding/'.get_encode($_POST['appointment_id1']).'/additional_notes');

	                // }else if (strpos(referrer(), "viewDaycare") !== false){
	                //           $this->message->save_success('User/viewDaycare/'.get_encode($_POST['appointment_id1']).'/additional_notes');
	                // }else{
	                      $this->message->save_success(referrer());
	                // }
               }
               else
               {
                   $this->message->custom_error_msg(referrer(), 'Cannot add new note,Please try again');
               }
           }
           else
           {
               $error = validation_errors();
               $this->message->custom_error_msg(referrer(),$error);
           }
       }
       else
      {
          $error = validation_errors();
          $this->message->custom_error_msg(referrer(),$error);
      }
    }

   function addfiles(){
        
        if($this->input->post())
        {
            if($this->input->post()){
                $data = $this->input->post();


                $data['file_name'] = upload_pet_pdf($_FILES['file_name']);

                $data['shop_id'] = $_SESSION['shop_id'];
                $data['uploaded_on'] = date('Y-m-d');
                $end = date('Y-m-d', strtotime('+1 years'));
                $data['expires_on'] = $end;
            
                if($this->db2->insert('file_upload',$data))
                {
                    
                    $this->message->save_success('User/getPetDetails/'.get_encode($_POST['pet_id']).'/extra_documents');
                }
                else
                {
                    $this->message->custom_error_msg(referrer(), 'Cannot add file,Please try again');
                }
            }
            else
            {
                $error = validation_errors();
                $this->message->custom_error_msg(referrer(),$error);
            }
        }
        else
       {
           $error = validation_errors();
           $this->message->custom_error_msg(referrer(),$error);
       }
    }

    function deletePetImage($id)
    {
    	//Delete Pet Image
    	
    	
    	//Checking User access for delete action

	    	$id = get_decode($id);
	    	$id == TRUE || $this->message->norecord_found('User/getAllPets');

	        //delete
	        $pet = $this->db2->get_where('pets',array('id'=>$id))->row() ;
	        
	        // if(file_exists("gs://".BUCKETNAME."/".PETS_IMAGES.$pet->pet_cover_photo))
        		// unlink("gs://".BUCKETNAME."/".PETS_IMAGES.$pet->pet_cover_photo);


		    if(!empty($pet->pet_cover_photo)){
		    	$this->Management_model->cloud_image_delete(PETS_IMAGES.$pet->pet_cover_photo);
		    }
        	

	      
	        if($this->db2->update('pets',array('pet_cover_photo'=>null) ,array('id' => $id)))
				$msg = array('err' => 0,'message'=>'Image deleted successfully','Title' => 'Success');
			else
				$msg = array('err' => 1000,'message'=>'Image cannot be deleted' ,'Title' => 'Error Occured');
				
		
		echo json_encode($msg);
    }

	function addPetNotes($id = null)
	{ //New Notes
 
		$formType = $_POST['formType'];
		if(isset($_POST['formType']))
		{
			unset($_POST['formType']);
		}
 
		//Adding New Pets to User
		if($this->input->post())
		{
			$this->form_validation->set_rules('medication_name', lang('username'), 'trim|required|xss_clean');
		   
			$this->form_validation->set_rules('expiry_on', lang('expiry_on'), 'trim|required|xss_clean');
			
 
			$postarray = $this->input->post();
			if ($this->form_validation->run()== TRUE) {
 
				$postarray['pet_id'] = $this->input->post('pet_id');
				$postarray['shop_id']= $this->session->userdata('shop_id');
				if(empty($postarray['date_of'])){
					$postarray['date_of'] = date('Y-m-d H:i:s');
				}else{
					$postarray['date_of'] = date('Y-m-d', strtotime(str_replace('.', '/', $postarray['date_of'])));
				}
				$postarray['expiry_on'] = date('Y-m-d', strtotime(str_replace('.', '/', $postarray['expiry_on'])));
				$postarray['added_on'] = date('Y-m-d H:i:s');
				$postarray['added_by_user'] = $this->session->userdata('id');
				$postarray['added_by'] = 'shop';
				$postarray['image'] = '';
			
				if(!empty($_FILES['add_vaccination_file']['name']))
				{
					$postarray['image'] = upload_pet_pdf($_FILES['add_vaccination_file']);
				}
 
				if(isset($postarray['vaccine_image']))
					unset($postarray['vaccine_image']);
				//Upload New Image
			
				if(time() > strtotime($postarray['expiry_on']))
				{
					$postarray['is_expired'] = 1;
				}
				else
				{
					$postarray['is_expired'] = 0;
				}
				unset($postarray['add_vaccination_file']);
 
				if($this->db2->insert('pets_notes',$postarray))
				{	
					if($formType == 'add'){
						$vacc_id = $this->db2->insert_id();
						$msg = array('errcode' => 1,'vaccId' =>$vacc_id,'message'=>'vaccination Added');
						$this->message->save_success(referrer(),$msg);
					}
					else{
						
						$petDetails = $this->Entry_model->getPetsViewDetails($postarray['pet_id']);
						$AppUserDetails = $this->Management_model->getSingleUserData($petDetails['app_user_id']);
						if(!empty($AppUserDetails[0]['fcm_id'] && $this->Management_model->checkIfAppUserBelongToSameShop($this->session->userdata('shop_id') ,$AppUserDetails[0]['id'])))
						{
							
							include_once(APPPATH."libraries/PushNotifications.php");
							$FCMPUSH = new PushNotifications();
							if(!empty($petDetails['pet_cover_photo']))
								$dataArray = array('image'=>base_url().UPLOAD_PETS.$petDetails['pet_cover_photo'],'result'=>$this->Management_model->getPetForNotification($id,$this->session->userdata('shop_id')));
							else
								$dataArray = array('image'=>'','result'=>$this->Management_model->getPetForNotification($id,$this->session->userdata('shop_id')));
 
							$badge = $this->Management_model->getBagdeCountForUser($AppUserDetails[0]['fcm_id'],$postarray['shop_id']);
							$data = array('body'=>'New Vaccination '.$postarray['medication_name'].'  for your pet '.$petDetails['name'].' has been added','title'=>'Vaccination Record Added','badge'=>($badge+1),'data'=>$dataArray);
							
							$insert = array();
							$insert['app_user_id'] = $AppUserDetails[0]['id'];
							$insert['pet_id'] = $petDetails['id'];
							$insert['title'] = $data['title'];
							$insert['body'] = $data['body'];
							$insert['data'] = serialize($dataArray);
							$insert['added_on'] = date('Y-m-d H:i:s');
							$insert['shop_id'] = $postarray['shop_id'];
							$insert['notification_type'] = 'pets';
							$data['data']['notification'] =  $insert;
							$this->db2->insert('notifications',$insert);
							$FCMPUSH->android($data,$AppUserDetails[0]['fcm_id']);
						}
 
						$msg = array('errcode' => 0,'message'=>'Customer Added');
					}
				}
			}
			else{
				
				$msg = array('errcode' => 1000,'message'=>'validation_errors');
			}    
		}
		else
		{
			$msg = array('errcode' => 1000,'message'=>'Paramter not found');
		} 
		echo json_encode($msg);exit;  
	}
 
	function editPetNotes($id = null)
	{
		$formType = $_POST['formType'];
		if(isset($_POST['formType']))
		{
			unset($_POST['formType']);
		}

		//Edit New Pets to User
		if($this->input->post())
		{
			$this->form_validation->set_rules('medication_name', lang('username'), 'trim|required|xss_clean');
		   
			$this->form_validation->set_rules('expiry_on', lang('expiry_on'), 'trim|required|xss_clean');
		
			$postarray = $this->input->post();
			$id = $this->input->post('id');
			if ($this->form_validation->run()== TRUE) {
				
				if(empty($postarray['date_of'])){
					$postarray['date_of'] = date('Y-m-d H:i:s');
				}else{
					$postarray['date_of'] = date('Y-m-d', strtotime(str_replace('.', '/', $postarray['date_of'])));
				}
				$postarray['expiry_on'] = date('Y-m-d', strtotime(str_replace('.', '/', $postarray['expiry_on'])));
				$postarray['added_on'] = date('Y-m-d H:i:s');
				$postarray['added_by_user'] = $this->session->userdata('id');
				$postarray['added_by'] = 'shop';
 
 
				if(time() > strtotime($postarray['expiry_on']))
				{
					$postarray['is_expired'] = 1;
				}
				else
				{
					$postarray['is_expired'] = 0;
				}
				$postarray['image'] = '';
 
				// pr($_FILES['edit_vaccination_file']);exit;
				if(!empty($_FILES['edit_vaccination_file']['name']))
				{
					$postarray['image'] = upload_pet_pdf($_FILES['edit_vaccination_file']);
				}
				if(empty($_FILES['edit_vaccination_file']['name']) && !empty($postarray['edit_vaccfile_name'])){
					$postarray['image'] =  $postarray['edit_vaccfile_name'];
				}
 
				if(isset($_FILES['edit_vaccination_file'])){
					unset($postarray['edit_vaccination_file']);
					// unset($postarray['edit_vaccfile_name']);
					
				}
				if(isset($postarray['edit_vaccfile_name'])){
					unset($postarray['edit_vaccfile_name']);
				}
 
				if(isset($postarray['edit_vaccine_image']))
					unset($postarray['edit_vaccine_image']);
 
				unset($postarray['appointment_id1']);
				unset($postarray['edit_vaccination_file']);

				// pr($postarray);exit;
				if($this->db2->update('pets_notes',$postarray,array('id'=>$id)))
				{
					$this->message->update_success(referrer());     	
				}
			}
			else{
				$error = validation_errors();
				$this->message->custom_error_msg(referrer(),$error);
			}    
 
		}
	}

   function deletePetNotes($id = null,$pet_id = null,$appointment_id1 = null)
    {
    	//Delete Pet from the shop
		$id = $_GET['id'];
		$petNotes = $this->Entry_model->getPetNotesAjax($id);

        if ($this->db2->delete('pets_notes', array('id' => $id))){
        	$this->message->delete_success(referrer());
        }
	}
	
    function getPetNotesAjax($id = null)
    {
    	//get pet not based on id for edit model
    	$id = get_decode($id);
        if(!$id)
        	$msg = array('err' => 1000,'message'=>'You are not authenticated to use this operations.','Title' => 'Error Occured');
    	else
    	{
    		$petNotes = $this->Entry_model->getPetNotesAjax($id);
    		
    		if(!empty($petNotes))
    			$msg = array('err' => 0,'message'=>'pets details fetched successfully','result' => $petNotes);
    		else
    			$msg = array('err' => 1000,'message'=>'pets details not found');
    	}

    	echo json_encode($msg);	
	}
	
    function viewAppointment($id = null)
    {
    	if(!empty($this->session->userdata())){	
    		// $data = $this->data;
	    	
	    	$data['services'] = $this->Entry_model->getAllServices();
            $data['breadCrumbname']='View Appointment';


	    	if(!empty($id)){
	    		//edit appointment view
	            $id = get_decode($id);
	            $data['id'] = $id;
	            
	           // pr($data);exit;
	            $data['appointments'] = $this->Appuser_model->getSingleAppointmentId($id);
	            // pr($data);exit;
	            $data['petDetails'] = $this->Entry_model->getPetsViewDetails($data['appointments']['pet_id']);

	            // $data['pets'] = $this->db2->get_where('pets',array('app_user_id'=>$data['appointments']['app_user_id'],'is_deceased'=>0))->result_array();


	            $shop_id = $this->session->userdata('shop_id');
	            // $data['customerdetails'] = $this->db2->select('email,number_type,phone')->get_where('app_users',array('id'=>$data['appointments']['app_user_id'],'registered_shop'=>$shop_id))->row_array();

	            $data['petNotes'] = $this->Entry_model->getpetNotesDetails($data['appointments']['pet_id']);

	            // $data['appointments']['groomerName'] = $this->Management_model->getGroomerName($data['appointments']['user_id']);
	            $data['appointmentService']=$this->Entry_model->getAppointmentServiceById($id);

	            $data['appointmentServiceInfo'] = array();

                if(!empty($data['appointments'])){
                    $pet_size = $this->db2->select('size')->get_where('pets',array('id' => $data['appointments']['pet_id']))->row_array();

                    $size_cost = $pet_size['size'].'_cost';
                    $i=0;

                    foreach($data['appointmentService'] as  $value){
                        $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
                        if(!empty($serviceData)){
                            $data['appointmentServiceInfo'][$i]['service_name'] = $serviceData['name'];
                            $data['appointmentServiceInfo'][$i]['service_cost'] = $serviceData[$size_cost];
                            $i++;
                        }
                    }
                }

	            $data['pageViewTitle'] = 'viewAppointment';
	            // $data['status']=$this->getStatus();
	            
	            $data['payments'] = $this->Management_model->getPaymentsDetails($id,'appointment');
	            $data['payouts'] = $this->Management_model->getPayoutsDetails($id,'appointment');

	            $data['additional_payment'] = $this->Management_model->getAdditionalPayment($id,'appointment');

	            
            	if($data['appointments']['add_on_service_cost'] != 0){
            		$data['add_on_services'] = $this->Management_model->getAddOnServices($id,'appointment');
            		$data['appointmentAddOnService']=$this->Entry_model->getAppointmentAddOnServiceById($id);
            		foreach($data['appointmentAddOnService'] as  $value){
                        $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
                        if(!empty($serviceData)){
                            $data['appointmentServiceInfo'][$i]['service_name'] = $serviceData['name'].'*';
                            $data['appointmentServiceInfo'][$i]['service_cost'] = $serviceData[$size_cost];
                            $i++;
                        }
                    }

            	}	

	            // $data['petSize'] = $this->Appuser_model->getPetSize();
	            if($data['appointments']['overlapping_appointment'] == 1)
	            {
	            	$data['overlapping'] = $this->Entry_model->getoverlappingDetails($data['appointments'],$data['appointmentService']);
	            }

	            if($data['appointments']['extented_time'] == 1)
	            {
	            	$data['extented_time'] = $this->Entry_model->getTimeExtendedDetails($data['appointments']);
	            }
	          
	            $data['groomNotes'] =  $this->Management_model->getPetGroomerNotesNew($data['appointments']['pet_id']);
        		$data['medicationNotes'] =  $this->Management_model->getPetMedicationNotesNew($data['appointments']['pet_id']);
				$data['petBehavior'] = $this->Management_model->getPetBehaviorWithEmoji($data['appointments']['pet_id']);
	          
	            $data['vaccinationMsg'] = $this->Management_model->vaccinationMessage($data['appointments']['pet_id']);


                $data['petBehaviorNote'] = $this->Management_model->getPetBehavior($data['appointments']['pet_id']);
                if(!empty($data['petBehaviorNote']))
                {
                    $data['petBehaviorNote']['username'] = $this->Management_model->getGroomerName($data['petBehaviorNote']['user_id']);
                }



	            $data['data_bodymsg1'] = ''; 
		    	if($data['appointments']['payment_method'] == 'online'){
		    		if ($data['payments']['stripe_charge'] > 0) {
		    			$data['data_bodymsg1'] = '<br>If you cancel the Appointment then '.get_option('currency_symbol').' '.$this->localization->currencyFormat($data['payments']['stripe_charge']).' amount will be deducted from your account'; 
		    		}
		    	}
	           
					$data['active_tab'] = 'appointments';
					$data['parent_url'] = 'appointments';

				
	        }

	        $data['header'] = TRUE;
			$data['_view'] = 'viewAppointment';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['parent_tab'] = 'entry';
			$data['active_tab'] = 'getAllAppointments';	
			// $data['user']=$this->data['userdata'];
			$data['company_name'] = get_option('company_name');
			$data['image_logo'] = '';
			// $company_logo = get_option('company_logo');
			// if(!empty($company_logo)){
			// 	$image_logo = loadCloudImage(SHOP_IMAGES.$company_logo);
			// 	if ($_SERVER['SERVER_NAME'] == 'localhost') {
			// 		$image_logo = "http://lh3.googleusercontent.com/fOWH0cp4--MXcctiinE9D6PI4VnxoOkXd4ImT_6Bt6bx_8wLm_GmEG5Ri7TeOnEpX9rG1c9uv9rIPhm1-VTtF580CBJW9Cp21T2ERw=s100";
			// 	}
			// 	$imagedata = file_get_contents($image_logo);
			// 	$data['image_logo'] = 'data:image/jpeg;base64,' . base64_encode($imagedata);
			// }

			$data['breadCrumbParentname']='Appointments List';
			$data['curenturl']= 'viewAppointment/'.$this->encrypt->encode(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
			$data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#00c0ef','Inprocess'=>'#f39c12','Checkout'=>'#4966aa','Move'=>'#6a73a5','Undo'=> '#b38282');
	        $data['appointment_image'] = $this->Entry_model->getAppointmentImage($id,'appointment');
	      
	        $temp = date('Y-m-d h:i:s'); 
    		$data['time'] = date('M d Y h:i a',strtotime($temp));
	       
	         $this->load->view('user/basetemplate',$data);
	    }else{

	    }
   }
	   
   function viewCheckoutAppointment($id = null)
   {
	   if(!empty($this->session->userdata())){	
		//    $data = $this->data;
		   
		   $data['services'] = $this->Entry_model->getAllServices();
		   $data['breadCrumbname']='View Appointment';


		   if(!empty($id)){
			   //edit appointment view
			   $id = get_decode($id);
			   $data['id'] = $id;
			   
			  // pr($data);exit;
			   $data['appointments'] = $this->Appuser_model->getSingleAppointmentId($id);
			   // pr($data);exit;
			   $data['petDetails'] = $this->Entry_model->getPetsViewDetails($data['appointments']['pet_id']);

			   // $data['pets'] = $this->db2->get_where('pets',array('app_user_id'=>$data['appointments']['app_user_id'],'is_deceased'=>0))->result_array();


			   $shop_id = $this->session->userdata('shop_id');
			   // $data['customerdetails'] = $this->db2->select('email,number_type,phone')->get_where('app_users',array('id'=>$data['appointments']['app_user_id'],'registered_shop'=>$shop_id))->row_array();

			   $data['petNotes'] = $this->Entry_model->getpetNotesDetails($data['appointments']['pet_id']);

			   // $data['appointments']['groomerName'] = $this->Management_model->getGroomerName($data['appointments']['user_id']);
			   $data['appointmentService']=$this->Entry_model->getAppointmentServiceById($id);

			   $data['appointmentServiceInfo'] = array();

			   if(!empty($data['appointments'])){
				   $pet_size = $this->db2->select('size')->get_where('pets',array('id' => $data['appointments']['pet_id']))->row_array();

				   $size_cost = $pet_size['size'].'_cost';
				   $i=0;

				   foreach($data['appointmentService'] as  $value){
					   $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
					   if(!empty($serviceData)){
						   $data['appointmentServiceInfo'][$i]['service_name'] = $serviceData['name'];
						   $data['appointmentServiceInfo'][$i]['service_cost'] = $serviceData[$size_cost];
						   $i++;
					   }
				   }
			   }

			   $data['pageViewTitle'] = 'viewAppointment';
			   // $data['status']=$this->getStatus();
			   
			   $data['payments'] = $this->Management_model->getPaymentsDetails($id,'appointment');
			   $data['payouts'] = $this->Management_model->getPayoutsDetails($id,'appointment');

			   $data['additional_payment'] = $this->Management_model->getAdditionalPayment($id,'appointment');

			   
			   if($data['appointments']['add_on_service_cost'] != 0){
				   $data['add_on_services'] = $this->Management_model->getAddOnServices($id,'appointment');
				   $data['appointmentAddOnService']=$this->Entry_model->getAppointmentAddOnServiceById($id);
				   foreach($data['appointmentAddOnService'] as  $value){
					   $serviceData = $this->db->select('id,name,small_cost,medium_cost,large_cost,xl_cost,xxl_cost')->get_where('services',array('id' => $value))->row_array();
					   if(!empty($serviceData)){
						   $data['appointmentServiceInfo'][$i]['service_name'] = $serviceData['name'].'*';
						   $data['appointmentServiceInfo'][$i]['service_cost'] = $serviceData[$size_cost];
						   $i++;
					   }
				   }

			   }	

			   // $data['petSize'] = $this->Appuser_model->getPetSize();
			   if($data['appointments']['overlapping_appointment'] == 1)
			   {
				   $data['overlapping'] = $this->Entry_model->getoverlappingDetails($data['appointments'],$data['appointmentService']);
			   }

			   if($data['appointments']['extented_time'] == 1)
			   {
				   $data['extented_time'] = $this->Entry_model->getTimeExtendedDetails($data['appointments']);
			   }
			 
			   $data['groomNotes'] =  $this->Management_model->getPetGroomerNotesNew($data['appointments']['pet_id']);
			   $data['medicationNotes'] =  $this->Management_model->getPetMedicationNotesNew($data['appointments']['pet_id']);
			   $data['petBehavior'] = $this->Management_model->getPetBehaviorWithEmoji($data['appointments']['pet_id']);
			 
			   $data['vaccinationMsg'] = $this->Management_model->vaccinationMessage($data['appointments']['pet_id']);


			   $data['petBehaviorNote'] = $this->Management_model->getPetBehavior($data['appointments']['pet_id']);
			   if(!empty($data['petBehaviorNote']))
			   {
				   $data['petBehaviorNote']['username'] = $this->Management_model->getGroomerName($data['petBehaviorNote']['user_id']);
			   }



			   $data['data_bodymsg1'] = ''; 
			   if($data['appointments']['payment_method'] == 'online'){
				   if ($data['payments']['stripe_charge'] > 0) {
					   $data['data_bodymsg1'] = '<br>If you cancel the Appointment then '.get_option('currency_symbol').' '.$this->localization->currencyFormat($data['payments']['stripe_charge']).' amount will be deducted from your account'; 
				   }
			   }
			  
				   $data['active_tab'] = 'appointments';
				   $data['parent_url'] = 'appointments';

			   
		   }

		   $data['header'] = TRUE;
		   $data['_view'] = 'viewAppointment';
		   $data['sidebar'] = TRUE;
		   $data['footer'] = TRUE;
		   $data['parent_tab'] = 'entry';
		   $data['active_tab'] = 'getCheckoutAppointments';	
		//    $data['user']=$this->data['userdata'];
		   $data['company_name'] = get_option('company_name');
		   $data['image_logo'] = '';
		//    $company_logo = get_option('company_logo');
		//    if(!empty($company_logo)){
		// 	   $image_logo = loadCloudImage(SHOP_IMAGES.$company_logo);
		// 	   if ($_SERVER['SERVER_NAME'] == 'localhost') {
		// 		   $image_logo = "http://lh3.googleusercontent.com/fOWH0cp4--MXcctiinE9D6PI4VnxoOkXd4ImT_6Bt6bx_8wLm_GmEG5Ri7TeOnEpX9rG1c9uv9rIPhm1-VTtF580CBJW9Cp21T2ERw=s100";
		// 	   }
		// 	   $imagedata = file_get_contents($image_logo);
		// 	   $data['image_logo'] = 'data:image/jpeg;base64,' . base64_encode($imagedata);
		//    }

		   $data['breadCrumbParentname']='Appointments List';
		   $data['curenturl']= 'viewAppointment/'.$this->encrypt->encode(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
		   $data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#00c0ef','Inprocess'=>'#f39c12','Checkout'=>'#4966aa','Move'=>'#6a73a5','Undo'=> '#b38282');
		   $data['appointment_image'] = $this->Entry_model->getAppointmentImage($id,'appointment');
		 
		   $temp = date('Y-m-d h:i:s'); 
		   $data['time'] = date('M d Y h:i a',strtotime($temp));
		  
			$this->load->view('user/basetemplate', $data);
	   }else{

	   }
   }

    function getAllservices()
    {
    	// pr($this->session->userdata());exit;
        if(!empty($this->session->userdata())){
	        $data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'services';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['active_tab'] = 'getAllservices'; 
			$data['user']=$this->data;
			$data['smallDog'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Dog','size'=>'small'))->result_array();
			$data['mediumDog'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Dog','size'=>'medium'))->result_array();
			$data['largeDog'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Dog','size'=>'large'))->result_array();
			$data['xlDog'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Dog','size'=>'xl'))->result_array();
			$data['xxlDog'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Dog','size'=>'xxl'))->result_array();
			$data['BoardingServiceDog'] = $this->db->get_where('boardings_settings',array('id'=>11))->row_array();
			
			$data['smallCat'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Cat','size'=>'small'))->result_array();
			$data['mediumCat'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Cat','size'=>'medium'))->result_array();
			$data['largeCat'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Cat','size'=>'large'))->result_array();
			$data['xlCat'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Cat','size'=>'xl'))->result_array();
			$data['xxlCat'] = $this->db2->get_where('pets',array('app_user_id' => $this->session->userdata['id'],'type'=>'Cat','size'=>'xxl'))->result_array();

			$data['petService'] = $this->db->get_where('services',array())->result_array();
			$data['BoardingServiceCat'] = $this->db->get_where('boardings_settings',array('id'=>10))->row_array();
			$data['daycarService'] = $this->db->get_where('boardings_settings',array('id'=>1))->row_array();

			$where = "CURDATE() between `start_date_time` and `end_date_time`";
			$app_user_id = $this->session->userdata('id');
			$this->db->select();
			$this->db->from('discount_users dusers');
			$this->db->join('discounts d',"d.id = dusers.discount_id");
			// $this->db->like('allow_type', $allow_type);
			$this->db->where(array('dusers.app_user_id' => $app_user_id,'d.is_deleted' => '0','dusers.is_active'=>'1'));
			$this->db->where($where);
			$this->db->order_by('d.discount_percentage', 'desc');
			$this->db->limit(1);
			$data['discount'] = $this->db->get()->row_array();

	        $this->load->view('user/basetemplate', $data);
	    }else{
	    	redirect('user/login');
	    }
    }

    function viewService($id = 0)
	{
		if(!empty($this->session->userdata())){	
			$data=$this->data;
			$id = get_decode($id);
			$data['id']=$id;
			$data['service']= $this->Management_model->getSingleService($id);
			if(empty($data['service']))
			{
				$this->message->custom_error_msg('User/getAllservices', 'Service not found'); 
			}
			$data['header'] = TRUE;
			$data['_view'] = 'viewService';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['parent_tab'] = 'management';
			$data['active_tab'] = 'getAllservices';
			$data['user']=$this->data;
			$data['petSize'] = $this->Appuser_model->getPetSize();
			$data['breadCrumbname']='View Service';
			$data['breadCrumbParentname']='Services';
			$data['parent_url'] = 'services';
			$data['curenturl']= 'viewService';
			$this->load->view('user/basetemplate', $data);
		}else{
		    	
		}
	}

	function viewBoarding($id)
    {
    	if(!empty($this->session->userdata())){
	    	$id = get_decode($id);
			
			$id == TRUE || $this->message->norecord_found('User/getAllAppointments');

	    	$data = $this->data;
	    	$data['id'] = $id;
	    	$data['company_name'] = get_option('company_name');
			$data['image_logo'] = '';
			$company_logo = get_option('company_logo');
			if(!empty($company_logo)){
				$image_logo = loadCloudImage(SHOP_IMAGES.$company_logo);
				if ($_SERVER['SERVER_NAME'] == 'localhost') {
					$image_logo = "http://lh3.googleusercontent.com/fOWH0cp4--MXcctiinE9D6PI4VnxoOkXd4ImT_6Bt6bx_8wLm_GmEG5Ri7TeOnEpX9rG1c9uv9rIPhm1-VTtF580CBJW9Cp21T2ERw=s100";
				}
				$imagedata = file_get_contents($image_logo);
				$data['image_logo'] = 'data:image/jpeg;base64,' . base64_encode($imagedata);
			}
	    	$data['header'] = TRUE;
			$data['_view'] = 'viewBoarding';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['parent_tab'] = 'entry';
			$data['user']=$this->data;
		
			$data['curenturl']= 'viewBoarding';
			$data['boardingDetails'] = $this->Management_model->getBoardingDayCareData($id);
			$data['petDetails'] = $this->db2->get_where('pets',array('id' => $data['boardingDetails']['pet_id']))->row_array();
			$data['petNotes'] = $this->Entry_model->getpetNotesDetails($data['boardingDetails']['pet_id']);
			$data['payments'] = $this->Management_model->getPaymentsDetails($id,$data['boardingDetails']['category']);
			$data['payouts'] = $this->Management_model->getPayoutsDetails($id,$data['boardingDetails']['category']);
			
			$data['category'] = $data['boardingDetails']['category'];

			$data['groomNotes'] =  $this->Management_model->getPetGroomerNotesNew($data['boardingDetails']['pet_id']);
	        $data['medicationNotes'] =  $this->Management_model->getPetMedicationNotesNew($data['boardingDetails']['pet_id']);

	        $shop_id = $this->session->userdata('shop_id');
		    $data['customerdetails'] = $this->db2->select('email,number_type,phone')->get_where('app_users',array('id'=>$data['boardingDetails']['app_user_id'],'registered_shop'=>$shop_id))->row_array();

			$data['petBehavior'] = $this->Management_model->getPetBehaviorWithEmoji($data['boardingDetails']['pet_id']); 
			
			$data['petBehaviorNote'] = $this->Management_model->getPetBehavior($data['boardingDetails']['pet_id']);
	        if(!empty($data['petBehaviorNote']))
	        {
	            $data['petBehaviorNote']['username'] = $this->Management_model->getGroomerName($data['petBehaviorNote']['user_id']);
	        }

			$data['vaccinationMsg'] = $this->Management_model->vaccinationMessage($data['boardingDetails']['pet_id']);
			
			$data['additional_payment'] = $this->Management_model->getAdditionalPayment($id,$data['boardingDetails']['category']); 
			
				$data['active_tab'] = 'getAllAppointments';
				$data['parent_url'] = 'boardings';
				
			
			
	    	$data['data_bodymsg1'] = ''; 
	    	if($data['boardingDetails']['payment_method'] == 'online'){
	    		if ($data['payments']['stripe_charge'] > 0) {
	    			$data['data_bodymsg1'] = '<br>If you cancel the Appointment then '.get_option('currency_symbol').' '.$this->localization->currencyFormat($data['payments']['stripe_charge']).' amount will be deducted from your account'; 
	    		}
	    	}

	    	

		    $temp = date('Y-m-d h:i:s'); 
	    	$data['time'] = date('M d Y h:i a',strtotime($temp));

			$data['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
			$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
	       	$data['boardingPrice']=$this->Management_model->getBoardingPrice();
	        $data['boardingInventory'] = $this->Management_model->getBoardingInventory();
			$data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#4966aa','Inprocess'=>'#f39c12','Checkout'=>'#4966aa','Move'=>'#6a73a5','Undo'=> '#b38282');
			$data['appointment_image'] = $this->Entry_model->getAppointmentImage($id,'boarding');

		

			
			$this->load->view('user/basetemplate', $data);
		}else{

		}
	}

	function viewCheckoutBoarding($id)
    {
    	if(!empty($this->session->userdata())){
	    	$id = get_decode($id);
			
			$id == TRUE || $this->message->norecord_found('User/getAllAppointments');

	    	$data = $this->data;
	    	$data['id'] = $id;
	    	$data['company_name'] = get_option('company_name');
			$data['image_logo'] = '';
			$company_logo = get_option('company_logo');
			if(!empty($company_logo)){
				$image_logo = loadCloudImage(SHOP_IMAGES.$company_logo);
				if ($_SERVER['SERVER_NAME'] == 'localhost') {
					$image_logo = "http://lh3.googleusercontent.com/fOWH0cp4--MXcctiinE9D6PI4VnxoOkXd4ImT_6Bt6bx_8wLm_GmEG5Ri7TeOnEpX9rG1c9uv9rIPhm1-VTtF580CBJW9Cp21T2ERw=s100";
				}
				$imagedata = file_get_contents($image_logo);
				$data['image_logo'] = 'data:image/jpeg;base64,' . base64_encode($imagedata);
			}
	    	$data['header'] = TRUE;
			$data['_view'] = 'viewBoarding';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['parent_tab'] = 'entry';
			$data['active_tab'] = 'getCheckoutAppointments';	
			$data['user']=$this->data;
		
			$data['curenturl']= 'viewBoarding';
			$data['boardingDetails'] = $this->Management_model->getBoardingDayCareData($id);
			$data['petDetails'] = $this->db2->get_where('pets',array('id' => $data['boardingDetails']['pet_id']))->row_array();
			$data['petNotes'] = $this->Entry_model->getpetNotesDetails($data['boardingDetails']['pet_id']);
			$data['payments'] = $this->Management_model->getPaymentsDetails($id,$data['boardingDetails']['category']);
			$data['payouts'] = $this->Management_model->getPayoutsDetails($id,$data['boardingDetails']['category']);
			
			$data['category'] = $data['boardingDetails']['category'];

			$data['groomNotes'] =  $this->Management_model->getPetGroomerNotesNew($data['boardingDetails']['pet_id']);
	        $data['medicationNotes'] =  $this->Management_model->getPetMedicationNotesNew($data['boardingDetails']['pet_id']);

	        $shop_id = $this->session->userdata('shop_id');
		    $data['customerdetails'] = $this->db2->select('email,number_type,phone')->get_where('app_users',array('id'=>$data['boardingDetails']['app_user_id'],'registered_shop'=>$shop_id))->row_array();

			$data['petBehavior'] = $this->Management_model->getPetBehaviorWithEmoji($data['boardingDetails']['pet_id']); 
			
			$data['petBehaviorNote'] = $this->Management_model->getPetBehavior($data['boardingDetails']['pet_id']);
	        if(!empty($data['petBehaviorNote']))
	        {
	            $data['petBehaviorNote']['username'] = $this->Management_model->getGroomerName($data['petBehaviorNote']['user_id']);
	        }

			$data['vaccinationMsg'] = $this->Management_model->vaccinationMessage($data['boardingDetails']['pet_id']);
			
			$data['additional_payment'] = $this->Management_model->getAdditionalPayment($id,$data['boardingDetails']['category']); 
			
			$data['active_tab'] = 'viewCheckoutAppointment';
			$data['parent_url'] = 'boardings';
				
			
			
	    	$data['data_bodymsg1'] = ''; 
	    	if($data['boardingDetails']['payment_method'] == 'online'){
	    		if ($data['payments']['stripe_charge'] > 0) {
	    			$data['data_bodymsg1'] = '<br>If you cancel the Appointment then '.get_option('currency_symbol').' '.$this->localization->currencyFormat($data['payments']['stripe_charge']).' amount will be deducted from your account'; 
	    		}
	    	}

	    	

		    $temp = date('Y-m-d h:i:s'); 
	    	$data['time'] = date('M d Y h:i a',strtotime($temp));

			$data['shop_boarding_availability'] = $this->Management_model->getshopAvailabiltyForBoarding();
			$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
	       	$data['boardingPrice']=$this->Management_model->getBoardingPrice();
	        $data['boardingInventory'] = $this->Management_model->getBoardingInventory();
			$data['colorArray'] = array('Pending' => '#F23939','Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#4966aa','Inprocess'=>'#f39c12','Checkout'=>'#4966aa','Move'=>'#6a73a5','Undo'=> '#b38282');
			$data['appointment_image'] = $this->Entry_model->getAppointmentImage($id,'boarding');

		

			
			$this->load->view('user/basetemplate', $data);
		}else{

		}
	}
	
	// function invoice(){
	// 	if(!empty($this->session->userdata())){
	// 		$data['header'] = TRUE;
	// 		$data['_view'] = 'invoice';
	// 		$data['sidebar'] = TRUE;
	// 		$data['footer'] = TRUE;
	// 		$data['active_tab'] = 'getAllAppointments'; 
	// 		$this->load->view('user/basetemplate', $data);
	// 	}else{
	// 		redirect('user/login');
	// 	}
	// }



	function invoice($invoice_no = null){
		// $data1 = $this->data;
			
		$invoice_no = $_GET['id'];
		$data = $this->Management_model->invoice($invoice_no);

		if(APPLICATION_ENV != 'development'){

			$query0 = $this->db->query("SELECT COUNT(id) as count_rows FROM appointments where invoice_no ='".$invoice_no."' and isDuplicate ='0'")->row_array();
			$query1 = $this->db->query("SELECT COUNT(id) as count_rows FROM boardings where invoice_no ='".$invoice_no."'")->row_array();
			$queryCount = $query0['count_rows'] + $query1['count_rows'];

			$query0 = $this->db->query("SELECT COUNT(id) as count_rows FROM appointments where invoice_no ='".$invoice_no."' and isDuplicate ='0' and status = 'Checkout' and is_paid='1'")->row_array();
			$query1 = $this->db->query("SELECT COUNT(id) as count_rows FROM boardings where invoice_no ='".$invoice_no."' and status = 'Checkout' and is_paid='1'")->row_array();
			$resultCount = $query0['count_rows'] + $query1['count_rows'];
			
			if($queryCount == $resultCount){
				$invoiceid = $invoice_no.'_'.$this->session->userdata('shop_id');     
				$dataresult = invoiceData($invoiceid,'fetch','');
				if(!empty($dataresult)){
					$data =$dataresult;
				}
			}
		}
		// $data = array_merge($data1, $data);
		$data['boardingSettings'] = $this->Management_model->getBoardingSettings();
		$data['colorArray'] = array('Confirm'=>'#219427','Cancel'=>'#60605B','Complete'=>'#00c0ef','Inprocess'=>'#f39c12','Checkout'=>'#4966aa');
		$data['petSize'] = $this->Management_model->getPetSize();
		
		
		$data['header'] = TRUE;
		$data['_view'] = 'invoice';
		$data['sidebar'] = TRUE;
		$data['footer'] = TRUE;
		$data['active_tab'] = 'getAllAppointments'; 
		$this->load->view('user/basetemplate', $data);

 	}


	
    function getAppUserPetsById()
   	{
   	   if(!empty($this->session->userdata())){
	       $id = $_POST['app_user_id'];
	   		$tempArr = $_POST['tempArr'];
	       $pets = $this->db2->get_where('pets',array('app_user_id'=>$id,'is_deceased'=>0,'is_deleted'=>0))->result_array();
	       $app_users = $this->db2->get_where('app_users',array('id'=>$id))->row_array();
	       $despositevalue = $app_users['is_deposited'];
	       $string = '';
	       $string .= '<select class="form-control " name="pet_id" id="pet_id" multiple="multiple" >';
	       if(!empty($pets))
	       {	
	       		
	           foreach ($pets as $key => $value) {
	           		if(!empty($value['pet_cover_photo'])) {
	                	$logo = loadCloudImage(PETS_IMAGES.$value['pet_cover_photo']);
	                }else{
	                	$logo = base_url().'assets/default-p.png"';
	                }

	           		if(count($pets)==1 || in_array($value['id'], $tempArr)){
	                	$string .= '<option value="'.$value['id'].'" data-size="'.$value['size'].'" data-name ="'.$value['name'].'" data-encodedidhref ="'.base_url('User/getPetDetails/'. str_replace(array('+', '/', '='),  array('-', '_', '~'), $this->encrypt->encode($value['id']))).'" data-puppyPower="'.$value['puppy_power'].'" data-type="'.$value['type'].'" data-logo="'.$logo.'" selected>'.$value['name'].'</option>';
	           		}else{
	           			$string .= '<option value="'.$value['id'].'" data-size="'.$value['size'].'" data-name ="'.$value['name'].'" data-encodedidhref ="'.base_url('User/getPetDetails/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encrypt->encode($value['id']))).'"  data-puppyPower="'.$value['puppy_power'].'" data-type="'.$value['type'].'" data-logo="'.$logo.'">'.$value['name'].'</option>';
	           		}
	           	}
	           $string .= '</select>';
	           $string .= '<label id="pet_id-error" class="help-block animated fadeInDown" for="pet_id"></label>';
	           $msg = array('error' => true,'errcode' => 0,'result'=>$string,'despositevalue' => $despositevalue);   
	       }
	       else
	       {
	       		$string .= '</select>';
	       		$string .= '<label id="pet_id-error" class="help-block animated fadeInDown" for="pet_id"></label>';
	           $msg = array('error' => true,'errcode' =>100,'result'=>$string);
	       }
	     
	       echo json_encode($msg);  
	    }else{

	    }      
   	}

   	function getAlltermservices(){
   		if(!empty($this->session->userdata())){
   			
   			$data = $this->data;
	        $data['header'] = TRUE;
	        $data['_view'] = 'getAlltermservices';
	        $data['sidebar'] = TRUE;
	        $data['footer'] = TRUE;
	        $data['parent_tab'] = 'getAlltermservices';
	        $data['active_tab'] = 'getAlltermservices';
	        $data['user']=$this->data['userdata'];
	        $data['parent_url'] = 'getAlltermservices';
	        $data['curenturl']= 'getAlltermservices';
	        $data['pageurls']=$this->Appuser_model->homePageDetails($this->session->userdata('id'),$this->session->userdata('shop_no'));
	        $this->load->view('user/basetemplate', $data);
	    }else{
			redirect('user/login');
	    }
   	}
   	function showtermServicPreview()
  	{	
  		if(!empty($this->session->userdata())){
	    	$data = array();
	  		$result = $this->db->get_where('options',array('name'=>'termConditions'))->row_array();
	  		$data['termsConditions'] = $result['value'];
	  		$data['_view'] = 'showPreview';
			$this->load->view('user/basetemplate', $data);
		}else{

		}
  	}

  	function showMaintermServicePreview()
  	{
  		if(!empty($this->session->userdata())){
	  		$data = array();
	  		$result = $this->db2->get_where('options',array('name'=>'termConditions'))->row_array();
	  		$data['termsConditions'] = $result['value'];
	  		$data['_view'] = 'showSuperAdminTermsAndCondtions';
			$this->load->view('user/basetemplate', $data);
		}else{

		}
	  }
	  
  	// function getalltransactions()
    // {
    // 	if(!empty($this->session->userdata())){
    	
    // 	//All Transcation done by users
	//     	$data = $this->data;
	// 		$data['header'] = TRUE;
	// 		$data['_view'] = 'transactions';
	// 		$data['sidebar'] = TRUE;
	// 		$data['footer'] = TRUE;
	// 		$data['active_tab'] = 'getalltransactions'; 
	// 		$data['parent_tab'] = 'getalltransactions';
	// 		$data['user']=$this->data;
	// 		$data['parent_url'] = 'getalltransactions';
	// 		$data['curenturl']= 'getalltransactions';
	// 		$data['payments'] = $this->Management_model->getAllPayments();
	// 		$data['payouts'] = $this->Management_model->getAllPayouts();
	// 		$data['credits'] = $this->Management_model->getAllcredits('','');
	// 		$data['transactions'] = array_merge($data['payments'],$data['credits'],$data['payouts']);
	// 		// $data['post'] = array();
	// 		// if($this->input->post())
	// 		// {
	// 		// 	$data['post'] = $this->input->post();
	// 		// 	$start_date = createDate($data['post']['start_date']);
	//   //   		$end_date = createDate($data['post']['end_date']);
	// 		// 	$data['transactionsFiltered'] = $this->Management_model->getPaymentsByFilter($start_date,$end_date);	
	// 		// }
	// 		$this->load->view('user/basetemplate', $data);
	// 	}else{
			
	// 	}
    // }
 	
 	function wallet(){
 		if(!empty($this->session->userdata())){
    	
    		$data = $this->data;
			$data['header'] = TRUE;
			$data['_view'] = 'wallet';
			$data['sidebar'] = TRUE;
			$data['footer'] = TRUE;
			$data['active_tab'] = 'wallet'; 
			$data['user']=$this->data;
			$data['credits'] = $this->Management_model->getAllcredits('','');
			$data['appUser'] = $this->Management_model->getSingleUserData($this->session->userdata('id'));

			$this->load->view('user/basetemplate', $data);
		}else{
			redirect('user/login');
		}
		
		// else{
		// 	$shop_no =  trim($this->uri->segment(1));

		// 	if(!$this->GlobalApp_model->checkShopPhoneNo($shop_no)){
		// 		$this->response(array('error' => true,'message' => 'Invalid Shop details','errcode' => 102));
		// 	}

		// 	$appuser = $this->GlobalApp_model->checkToken();
		// 	$result = $this->Management_model->getAllcredits('','');
		// 	$result1 = $this->Management_model->getSingleUserData($appuser['id']);

	    //     if(!empty($result) || !empty($result1))
	    //     {
	    //         $this->response(array('error' => false,'message' => 'Details fetch successfully','errcode' => 0,'result'=>$result));
	    //     }
	    //     else
	    //     {
	    //         $this->response(array('error' => false,'message' => 'No data found','errcode' => 0));
	    //     }
		// }
 	}
	//move appointment
	function moveAppointment()
  	{	

  		if($this->input->post())
		{	
			$data = $this->input->post();

			
      		$appointments = $this->Entry_model->AppointmentWithServerdate($data['appointment_id']);
      		$appointments['invoice_items'] =unserialize($appointments['invoice_items']);
      		
      		$result = $this->db->query('SELECT * FROM appointments WHERE STR_TO_DATE(CONCAT("'.$appointments['appointment_date'].'", " " ,"'.$appointments['appointment_time'].'"), "%Y-%m-%d %H:%i:%s") < STR_TO_DATE(CONCAT(appointment_date, " ", appointment_end_time), "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE(CONCAT("'.$appointments['appointment_date'].'", " ","'.$appointments['appointment_end_time'].'"), "%Y-%m-%d %H:%i:%s") > STR_TO_DATE(CONCAT(appointment_date, " ", appointment_time), "%Y-%m-%d %H:%i:%s") and isDuplicate = "1" and (pet_id = '.$appointments['pet_id'].' ) and status not in ("Cancel","Checkout")')->result_array();
      		
			$data['appointment_date'] = date('Y-m-d',strtotime($data['appointment_date']));
			$data['appointment_time'] = date("H:i:s", strtotime($data['appointment_time']));
			$data['appointment_end_time'] = date("H:i:s", strtotime($data['A_time_end']));

			//New logic (By kshitija)
			//creating invoice items array for appointments
			$serviceids = array();
			$i = 0;
			foreach ($appointments['invoice_items']['services'] as $key => $value) {
				$serviceids[$i] = $value['id'];
				$i++;
			}
			
			$data['invoice_items'] = serialize($this->Management_model->serviceArray($serviceids,$appointments['upgrade_size'],$data['user_id']));
			

			$data = groomingServerTimeConverter($data);
			
			$updateArr = array('user_id'=>$data['user_id'], 'overlapping_appointment'=>$data['overlapping_appointment'],'appointment_time'=>$data['appointment_time'],'appointment_date'=>$data['appointment_date'],'appointment_end_time'=>$data['appointment_end_time'],'invoice_items' => $data['invoice_items']);
			
			if($this->db->update('appointments',$updateArr,array('id'=>$data['appointment_id'])))
			{
				if(!empty($result)){
					foreach ($result as $key => $value) {
						$this->db->update('appointments',$updateArr,array('id'=>$value['id']));
					}
				}
				$appointment = $this->Entry_model->getSingleAppointmentId($data['appointment_id']);
				$this->Management_model->sendMoveAppointmentEmailToUser($appointment,$appointmentService);
				
				$this->message->update_success(referrer());
			}
			else
			{
				$this->message->custom_error_msg(referrer(), 'Cannot complete request at this moment.please try again'); 
			}	
	    }
	    else
	    {
	    	$this->message->custom_error_msg(referrer(), 'Cannot access function directly');
	    }    

  	}
  	//move boarding
  	function moveBoarding(){
        if($this->input->post())
        {    
            $data = $this->input->post();

            $resultData = $this->db->select('category')->get_where('boardings',array('id' => $data['appointment_id']))->row_array();

            $startDate = date('Y-m-d',strtotime($data['start_date']));
            $endDate = date('Y-m-d',strtotime($data['end_date']));
           
            $this->db->where('date(boarding_schedule.start) =',"'$startDate'",false);
               $data['boarding_schedule'] = $this->db->get_where('boarding_schedule')->result_array();

           if(empty($data['boarding_schedule'])){
               $this->message->custom_error_msg(referrer(),'You must add schedule');
           }else if(empty($endDate) && !isset($data['appointment_id']) && empty($data['appointment_id']) ){
               $this->message->custom_error_msg(referrer(),'Something went wrong, please try again');
           }else{
               
               $updateArr = array('start_date' => $startDate, 'end_date' => $endDate,'amount' => $data['amount']);
               	//New logic (By kshitija)
				//creating invoice items array along with cost, date and overstay fields
               $data = boardingServerTimeConverter($data);
               $updateArr['invoice_items']["appointment"] = array("amount"=>$data['amount'], "start_date"=>$startDate.' '.$data['start_time'],"end_date"=>$endDate.' '.$data['end_time'], "overstay"=>0); 
               $updateArr['invoice_items'] = serialize($updateArr['invoice_items']);

			   

               $this->db->update('boardings',$updateArr,array('id' =>$data['appointment_id']));

               $appointment = $this->Entry_model->getSingleboardingId($data['appointment_id']);
               $this->Management_model->sendMoveBoardingEmailToUser($appointment);

               $this->message->update_success('Admin/boardings');
           }
        }
        else
        {
            $this->message->custom_error_msg(referrer(), 'Cannot access function directly');
        }
    }
   
	// move daycare
    function moveDaycare(){

		if($this->input->post()){
			$data = $this->input->post();

		   $resultData = $this->db->select('category')->get_where('boardings',array('id' => $data['appointment_id']))->row_array();

			$startDate = date('Y-m-d',strtotime($data['start_date']));
			$this->db->where('date(daycare_schedule.date) =',"'$startDate'",false);
			  $data['daycare_schedule'] = $this->db->get_where('daycare_schedule')->result_array();

			  if(empty($data['daycare_schedule'])){
				  $this->message->custom_error_msg(referrer(),'You must add schedule');
			  }else if(empty($startDate) && !isset($data['appointment_id']) && empty($data['appointment_id']) ){
				  $this->message->custom_error_msg(referrer(),'Something went wrong, please try again');
			   }else{

				   $updateArray['start_date'] = createDate($data['start_date']);
				   $updateArray['end_date']   = createDate($data['start_date']);
					   
				   $updateArray['start_time'] = date("H:i", strtotime($data['start_time']));
				   $updateArray['end_time'] = date("H:i", strtotime($data['end_time']));
				   $updateArray['amount'] = $data['amount'];
				  
					  


					  //this is for convert system time into UTC time 
				   $updateArray = boardingServerTimeConverter($updateArray);

				   //New logic (By kshitija)
				   //creating invoice items array along with cost, date and overstay fields
				   $updateArray['invoice_items']["appointment"] = array("amount"=>$updateArray['amount'], "start_date"=>$updateArray['start_date'].' '.$updateArray['start_time'],   
				   "end_date"=>$updateArray['end_date'].' '.$updateArray['end_time'], "overstay"=>0);
				   $updateArray['invoice_items'] = serialize($updateArray['invoice_items']);
				   
				   $this->db->update('boardings',$updateArray,array('id' =>$data['appointment_id']));

				   $appointment = $this->Entry_model->getSingleboardingId($data['appointment_id']);
					  $this->Management_model->sendMoveBoardingEmailToUser($appointment);
				   
					  $this->message->update_success(referrer());
			   }

	   }else{
			$this->message->custom_error_msg(referrer(), 'Cannot access function directly');
	   }
   }
  
}


	