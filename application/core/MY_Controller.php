<?php
class MY_Controller extends CI_Controller
{
	public $allowedActions = array();
	public $allowedAdmin = array();
	public $allowedUser = array();

	public $allowedSuperadmin = array();



	
	public $assertion = TRUE;
	
    function __construct()
    {
        parent::__construct();

        $this->db2 = $this->load->database('main', TRUE);

        $site = $this->config->item('site');
        

        // pr($_SESSION);exit;
		// if(isset($_SESSION['last-activity']) && time() - $_SESSION['last-activity'] > 700) {
		//     // session inactive more than 10 min
		//     unset($_SESSION['last-activity']);
		//     session_destroy();
		//     header('location: /');
		// }

		// $_SESSION['last-activity'] = time(); // update last activity time stamp

		// if(time() - $_SESSION['created'] > 600) {
		//     // session started more than 10 min ago
		//     session_regenerate_id(true); // change session id and invalidate old session
		//     $_SESSION['created'] = time(); // update creation time
		// }

       
        
        //shop config are set here
  //       if($this->session->userdata('type') == 'admin'){

  //       	$lang = $this->db->get_where('language', array("active" => 1))->row()->name;
			
		//     //Setting 

		// 	if(empty($lang))
		// 	{
		// 		$lang = 'english';
		// 	}
		// 	$this->config->set_item('language', $lang);
		// 	$this->lang->load($lang.'_menu', $lang,$lang );
		// 	$this->lang->load($lang.'_body', $lang,$lang );
		// 	$this->lang->load($lang.'_msg', $lang,$lang );
		// 	$this->lang->load($lang.'_title', $lang,$lang );
	 //        $this->data = $site;	

  //       	$this->load->model('Admin_user_model');
  //           $id = gettingUrlId();
		//     if($this->session->userdata('shopno')){
	 //    	 	if($id != $this->session->userdata('shopno')){
	 //    	 		$url = $this->uri->segment(3);

	 //    	 		if(!empty($url))
		//     			header('Location: '. INSTALL_FOLDER .$this->session->userdata('shopno').'/Admin/'.$this->uri->segment(3));
		//     		else
		//     			header('Location: '. INSTALL_FOLDER);
	 //    		} 
		//     }

		//     //Setting Timezone as per shop settings. default timezone can be change in profile -> localization
		//     $timezone = get_option('time_zone');
		// 	date_default_timezone_set($timezone);

		//     //Validating logged in user url.Logged in user's will be not be able to access other shop detail by changing url.

		//     //Checking default language 

		//     $this->data['userdata'] = $this->Admin_user_model->getUserData($this->session->userdata('id'));
		    

		//     // $boardingSwitch = get_option('boarding_switch');

		//     // $daycareSwitch = get_option('daycare_switch');
		//     $this->data['userdata']['appointment_switch']  = get_option('appointment_switch');
		//     $this->data['userdata']['boarding_switch']  =  get_option('boarding_switch');;
		//     $this->data['userdata']['daycare_switch']  = get_option('daycare_switch');
		//     $this->data['showBankError'] = 0;

		//     if($this->session->userdata('is_owner') != 1 )
  //           {
	 //              // $this->db->where('date','CURDATE()',false);
	 //             // $this->data['trackingDetails'] = $this->db->get_where('groomer_tracking',array('user_id'=>$this->session->userdata('id'), 'date'=>date('Y-m-d')))->row_array();
	            
	 //            /*  $this->data['trackingDetails'] = $this->db->order_by('id','DESC')->get_where('groomer_tracking',array('user_id'=>$this->session->userdata('id'), 'date'=>date('Y-m-d'),'check_out' => 0 ),1)->row_array(); */

	 //            $data_record = $this->data['trackingDetails'] = $this->db->order_by('id','DESC')->get_where('groomer_tracking',array('user_id'=>$this->session->userdata('id'),'check_out' => 0,'date'=>date('Y-m-d')),1)->row_array();




	 //            if(count($data_record )){

	 //                $records = $this->db->get_where('groomer_tracking',array('user_id'=>$this->session->userdata('id'),'check_out' => 1,'date' => date('Y-m-d')))->result_array();

	 //                $this->data['trackingDetails']['totalTodaysTime'] = '00:00:00';
	 //                $totalSeconds = 0;
	 //                foreach($records as $record){
	 //                    $timeFirst  = strtotime($record['start']);
	 //                    $timeSecond = strtotime($record['end']);
	 //                    $differenceInSeconds = $timeSecond - $timeFirst;
	 //                    $totalSeconds = $totalSeconds + $differenceInSeconds;
	 //                }
	 //                $this->data['trackingDetails']['totalTodaysTime'] = floor($totalSeconds / 3600) . gmdate(":i:s", $totalSeconds % 3600);
	 //            }


  //              //pr($this->data['trackingDetails']);
  //              //echo  $this->db->last_query();exit;
  //           }
		//     //pr($_SERVER);exit;

		// }

		//superadmin config    
   //      if($this->session->userdata('type') == 'super_admin')
   //      {

   //      	$this->config->load('superAdmin',TRUE);

   //      	$this->data = $this->config->item('superAdmin');

        	

   //      	$lang = $this->db2->get_where('language', array("active" => 1))->row()->name;
			
		 //    //Setting 

			// if(empty($lang))
			// {
			// 	$lang = 'english';
			// }
			// $this->config->set_item('language', $lang);

			// $this->lang->load($lang.'_menu', $lang,$lang );
			// $this->lang->load($lang.'_body', $lang,$lang );
			// $this->lang->load($lang.'_msg', $lang,$lang );
			// $this->lang->load($lang.'_title', $lang,$lang );

	       
   //      	$this->load->model('Super_admin_model');
   //      	$this->data['userdata'] = $this->Super_admin_model->getUserData($this->session->userdata('id'));
       	
        	

   //      }


	        	
	        
		
    }
	
	function allow(){
		$this->allowedActions = array_merge($this->allowedActions, func_get_args());
	}
	
	function allowAdmin(){
		// $this->allowedAdmin = explode(',', $this->allowedAdmin);
		//  func_get_args() = explode(',', func_get_args());
		 // pr(func_get_args()[0]);exit;
		 // pr($this->allowedAdmin);exit;
		$this->allowedAdmin = array_merge($this->allowedAdmin, func_get_args());
		// pr($this->allowedAdmin);
		// pr(func_get_args());
	}
	
	// function allowUser(){
	// 	$this->allowedUser = array_merge($this->allowedUser, func_get_args());
	// }

	// function allowSuperadmin(){
	// 	$this->allowedSuperadmin = array_merge($this->allowedSuperadmin, func_get_args());
	// }

	

	

	function assert($msg){
		if($this->assertion){
			echo '<pre>';
			throw new Exception($msg);
			echo '</pre>';
			exit;
		}
	}
}