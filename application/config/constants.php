<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('CI_BOOTSTRAP_REPO',			'https://github.com/waifung0207/ci_bootstrap_3');
define('CI_BOOTSTRAP_VERSION',		'Build 20160406');	// will follow semantic version (e.g. v1.x.x) after first stable launch

define('UPLOAD_DEMO_COVER_PHOTO',	'assets/uploads/demo/cover_photos');
define('UPLOAD_DEMO_BLOG_POST',		'assets/uploads/demo/blog_posts');
// define('EMAIL_FROM',		'ajay.s@infiny.in');

defined('ContactEmail') OR define('ContactEmail','support@griffinapps.com');
defined('PROJECTNAME') OR define('PROJECTNAME','Pet Commander');
defined("COMPANYNAME") OR define('COMPANYNAME','Griffin Apps, LLC'); 

//Willams Stripe Api keys 22 Dec.This are testing mode keys
defined('STRIPESECRETKEY') OR define('STRIPESECRETKEY','sk_test_Qgi93BVhZT6HSYordfZ8KDkT');
defined('STRIPEPUBLISKEY') OR define('STRIPEPUBLISKEY','pk_test_SFEQaDycGUC877Zn1KzHYNyp');

//Stripe Commission constant
defined('STRIPECOMMISSIONPERCENTAGE') or define('STRIPECOMMISSIONPERCENTAGE',0.029);
defined('STRIPECOMMISSIONADDITONALAMOUNT') or define('STRIPECOMMISSIONADDITONALAMOUNT',0.30);

defined('FCMKEY') or define('FCMKEY','AAAAQOoTwag:APA91bHkEIECmGPI29UCRGzf5wkbmPbUe5a4faastEoBP-II0xjEcek3GFm1Vub3o6WUNA29zZzB88yGo0XgdzQAK3a50xEeTryvlk9bZLCkKyL_DbwwkKAXU7eWK3mhLEuEEALoqFGY');
define('INSTANCE_NAME',    '/cloudsql/pet-commander-beta-app:us-central1:griffin');
//defined('BUCKETNAME') or define('BUCKETNAME','pet-commander-beta-app');
//Willams Stripe Api keys 22 Dec.This are live mode keys
//defined('STRIPESECRETKEY') OR define('STRIPESECRETKEY','sk_live_XuUh3vIcYhNt4Fjf2A8gWXLP');
//defined('STRIPEPUBLISKEY') OR define('STRIPEPUBLISKEY','pk_live_KzmFRmRuZID67g20xydJUhZ0');

// define('SMTP_HOST',"ssl://smtp.googlemail.com");
// define('SMTP_PORT',"465");
// define('SMTP_USER',"sanjeev.m@infiny.in");
// define('SMTP_PASSWORD',"sanju9876543");
// define('EMAIL_FROM',"sanjeev.m@infiny.in");