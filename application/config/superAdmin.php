<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Site (by CI Bootstrap 3)
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views when calling 
| MY_Controller's render() function. 
|
| Each of them can be overrided from child controllers.
|
*/

//$this->CI =& get_instance();


$CI = &get_instance();
$CI->db2 = $CI->load->database('main', TRUE);
if ($CI->db2->database != '') {

    $lang = $CI->db2->get_where('language', array("active" => 1))->row()->name;
    $menuGroup = $CI->db2->get_where('module_group')->result_array();   
}


if(empty($lang))
{
    $lang = 'english';
}

$CI->lang->load($lang.'_menu', $lang,$lang );

//Setting default english language


//Creating Menu for Shop Users

if(!empty($menuGroup)){
    $menu = array();
    if(!empty($menuGroup))
    {

        foreach ($menuGroup as $key => $value) {
            $menu[$value['group_name']] = array();
            $tempName = $value['language_variable'];
            $array = array(
                'name'      =>  $tempName,
                'url'       => $value['url'],
                'icon'      => $value['icon'],


            );
            $child = array();
            $children = $CI->db2->get_where('admin_modules',array('group_id' =>$value['id']))->result_array();
            if(!empty($children)){
                foreach ($children as $key1 => $value1) {
                   $tempKey = $value1['language_variable'];
                   $child[$tempKey] = $value1['url'];
                   
                }
                $array['children'] = $child;

            }


            //array_push($bigArray, $array);
            $menu[$value['group_name']] = $array;

        }
    }
    
}






//Comman View Configs

$config['superAdmin'] = array(

    // Site name
    'name' => 'Super Admin Panel',

    // Default page title
    // (set empty then MY_Controller will automatically generate one based on controller / action)
    'title' => '',

    // Default meta data (name => content)
    'meta'  => array(
        'author'        => PROJECTNAME,
        'description'   => 'Admin Model'
    ),

    // Default scripts to embed at page head / end
    'scripts' => array(
        'head'  => array(
            'assets/js/jQuery-2.2.0.min.js',
            'assets/js/bootstrap.min.js',
        ),
        'foot'  => array(


            'assets/plugin/select2/select2.full.min.js',
            'assets/plugin/input-mask/jquery.inputmask.js',
            'assets/plugin/input-mask/jquery.inputmask.date.extensions.js',
            'assets/plugin/input-mask/jquery.inputmask.extensions.js',
            //date range picker
            //'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js',
            'assets/plugin/daterangepicker/moment.min.js',
            'assets/plugin/daterangepicker/daterangepicker.js',
            'assets/plugin/datepicker/bootstrap-datepicker.js',
            'assets/plugin/colorpicker/bootstrap-colorpicker.min.js',
            'assets/plugin/timepicker/bootstrap-timepicker.min.js',
            'assets/plugin/slimScroll/jquery.slimscroll.min.js',
            'assets/plugin/fastclick/fastclick.js',
            'assets/js/app.min.js',
            'assets/js/admin.js',
            'assets/js/cart.js',
            'assets/js/jquery.PrintArea.js',
            'assets/js/printThis.js',

            //data tables
            'assets/plugin/datatables/jquery.dataTables.min.js',
            'assets/plugin/datatables/dataTables.bootstrap.js',
            'assets/plugin/datatables/dataTables.buttons.min.js',
            'assets/plugin/datatables/buttons.bootstrap.min.js',
            'assets/plugin/datatables/jszip.min.js',
            'assets/plugin/datatables/pdfmake.min.js',
            'assets/plugin/datatables/vfs_fonts.js',
            'assets/plugin/datatables/buttons.html5.min.js',
            'assets/plugin/datatables/buttons.print.min.js',
            'assets/plugin/datatables/dataTables.fixedHeader.min.js',
            'assets/plugin/datatables/dataTables.keyTable.min.js',
            'assets/plugin/datatables/dataTables.responsive.min.js',
            'assets/plugin/datatables/responsive.bootstrap.min.js',
            'assets/plugin/datatables/dataTables.scroller.min.js',

            'assets/plugin/iCheck/icheck.min.js',

            //form validation
            'assets/plugin/jquery-validation/jquery.validate.min.js',
            'assets/plugin/jquery-validation/additional-methods.min.js',
            'assets/js/forms_validation.js',
            'assets/js/custom-file-input.js',

            'assets/js/ckeditor.js',
            'assets/plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',

            //graphChart
            'assets/plugin/morris/morris.min.js',
            'assets/plugin/morris/raphael.min.js',

            //calendar
            'assets/plugin/fullcalendar/fullcalendar.min.js',
            'assets/plugin/fullcalendar/moment.min.js',

            //Editor wys
            'assets/plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        ),
    ),


    'stylesheets' => array(
        'screen' => array(

            'assets/css/bootstrap/css/bootstrap.css',
            'assets/plugin/select2/select2.min.css',
            'assets/css/AdminLTE.css',
            'assets/css/custom.css',
            'assets/css/skins.css',
            //data tables
            'assets/plugin/datatables/jquery.dataTables.min.css',
            'assets/plugin/datatables/buttons.bootstrap.min.css',
            'assets/plugin/datatables/fixedHeader.bootstrap.min.css',
            'assets/plugin/datatables/responsive.bootstrap.min.css',
            'assets/plugin/datatables/scroller.bootstrap.min.css',

            'assets/plugin/daterangepicker/daterangepicker-bs3.css',
            'assets/plugin/datepicker/datepicker3.css',
            'assets/plugin/colorpicker/bootstrap-colorpicker.min.css',
            'assets/plugin/timepicker/bootstrap-timepicker.min.css',

            'assets/plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',

            //'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
            //'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
            'https://fonts.googleapis.com/icon?family=Material+Icons',
            'assets/css/font-awesome.min.css',
            'assets/css/ionicons.min.css',

            //graphChart
            'assets/plugin/morris/morris.css',

            //calendar
            'assets/plugin/fullcalendar/fullcalendar.min.css',
            //'assets/plugin/fullcalendar/fullcalendar.print.css',

            //Editor wys
            'assets/plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
            'https://fonts.googleapis.com/icon?family=Material+Icons',
            'assets/css/customCss.css'
        )
    ),


    // Multilingual settings (set empty array to disable this)
    'multilingual' => array(),

    // AdminLTE settings
    'adminlte' => array(
        
    ),

  
   
    // default page when redirect non-logged-in user
    'login_url' => 'User',

    // restricted pages to specific groups of users, which will affect sidemenu item as well
    // pages out of this array will have no restriction (except required admin user login)
    'page_auth' => array(


    ),


    // For debug purpose (available only when ENVIRONMENT = 'development')
    'debug' => array(
        'view_data'     => FALSE,   // whether to display MY_Controller's mViewData at page end
        'profiler'      => FALSE,   // whether to display CodeIgniter's profiler at page end
    ),
);


//appending menu to config array.
$config['superAdmin']['menu']=$menu;

