<?php defined('BASEPATH') OR exit('No direct script access allowed');

		/*
		| -------------------------------------------------------------------
		| DATABASE CONNECTIVITY SETTINGS
		| -------------------------------------------------------------------
		| This file will contain the settings needed to access your database.
		|
		| For complete instructions please consult the 'Database Connection'
		| page of the User Guide.
		|
		| -------------------------------------------------------------------
		| EXPLANATION OF VARIABLES
		| -------------------------------------------------------------------
		|
		|	['dsn']      The full DSN string describe a connection to the database.
		|	['hostname'] The hostname of your database server.
		|	['username'] The username used to connect to the database
		|	['password'] The password used to connect to the database
		|	['database'] The name of the database you want to connect to
		|	['dbdriver'] The database driver. e.g.: mysqli.
		|			Currently supported:
		|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
		|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
		|	['dbprefix'] You can add an optional prefix, which will be added
		|				 to the table name when using the  Query Builder class
		|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
		|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
		|	['cache_on'] TRUE/FALSE - Enables/disables query caching
		|	['cachedir'] The path to the folder where cache files should be stored
		|	['char_set'] The character set used in communicating with the database
		|	['dbcollat'] The character collation used in communicating with the database
		|				 NOTE: For MySQL and MySQLi databases, this setting is only used
		| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
		|				 (and in table creation queries made with DB Forge).
		| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
		| 				 can make your site vulnerable to SQL injection if you are using a
		| 				 multi-byte character set and are running versions lower than these.
		| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
		|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
		|	['encrypt']  Whether or not to use an encrypted connection.
		|
		|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
		|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
		|
		|				'ssl_key'    - Path to the private key file
		|				'ssl_cert'   - Path to the public key certificate file
		|				'ssl_ca'     - Path to the certificate authority file
		|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
		|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
		|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
		|
		|	['compress'] Whether or not to use client compression (MySQL only)
		|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
		|							- good for ensuring strict SQL while developing
		|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
		|	['failover'] array - A array with 0 or more data for connections if the main should fail.
		|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
		| 				NOTE: Disabling this will also effectively disable both
		| 				$this->db->last_query() and profiling of DB queries.
		| 				When you run a query, with this setting set to TRUE (default),
		| 				CodeIgniter will store the SQL statement for debugging purposes.
		| 				However, this may cause high memory usage, especially if you run
		| 				a lot of SQL queries ... disable this to avoid that problem.
		|
		| The $active_group variable lets you choose which connection group to
		| make active.  By default there is only one group (the 'default' group).
		|
		| The $query_builder variables lets you determine whether or not to load
		| the query builder class.
						*/

		if(APPLICATION_ENV == 'development')
		{
			$instance_name = "";
			$Chostname = 'localhost'; 
			$Cusername = 'root';
			$Cpassword = '';
			$Cdbname = 'groomShop2';
			$CglobalDb = 'woodlesSuperAdmin';
		}
		else
		{
			$instance_name = "/cloudsql/pet-commander-beta-app:us-central1:griffin";

			$Chostname = '/cloudsql/pet-commander-beta-app:us-central1:griffin'; 
			$Cusername = 'griffin';
			$Cpassword = '5oqVzVmN$DF^X$';
			$Cdbname = 'groomShop2';
			$CglobalDb = 'griffinsuperAdmin';
		}
		

		// $con=mysqli_init();
  //       if (!$con)
  //       {
  //               die("mysqli_init failed");
  //       }
  //       if (!mysqli_real_connect($con,'localhost', 'griffin', 'griffin@123!@#', 'groomShop2', null, $instance_name))
  //       {
  //               die("Connect Error: " . mysqli_connect_error());
  //       }
  //       mysqli_close($con);
  //       exit;






		$CI =& get_instance();
		$keyId =  $CI->uri->segment(1); //fetch phone number from url
		$admin = array();
		$shopDbInfo  = array();

	
		if(empty($keyId) || !ctype_digit($keyId)){
			//validating phone no whether digit. if the phone is not valid then selecting default database

			$admin['hostname']=$Chostname;
			$admin['username']=$Cusername;
			$admin['password']=$Cpassword;
			$admin['database']=$Cdbname;

		} else {
			
			//valid phone number with digits

			$servername = $Chostname;
			$username = $Cusername;
			$password = $Cpassword;
			$dbname = $CglobalDb;

			
			$conn = new mysqli('127.0.0.1', $username, $password, $dbname,null,$instance_name);
			
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			}

			$phone_number =  mysqli_real_escape_string($conn, $keyId);

			$sql = "SELECT * FROM shops where phone_number = ".trim($phone_number)."";
			$result = $conn->query($sql);
			//echo $sql;exit;

			//fetching shop database creds based on phone number

			if (!$result) {
    			trigger_error('Invalid query: ' . $conn->error);
    			exit;
			}

			if ($result->num_rows > 0) {
			   
			   //setting shop based cred for default database
			    $shopDbInfo = $result->fetch_assoc(); 
			    $admin['hostname']=$shopDbInfo['dbhostname'];
				$admin['username']=$shopDbInfo['dbusername'];
				$admin['password']=$shopDbInfo['dbpassword'];
				$admin['database']=$shopDbInfo['dbname'];    
			    
			} else {

				//if shop database not found selecting database for showing an error

			    $admin['hostname']=$Chostname;
				$admin['username']=$Cusername;
				$admin['password']=$Cpassword;
				$admin['database']=$Cdbname;
			}
			$conn->close();
		}
		

		// for localhost 
		$admin['hostname'] = '127.0.0.1';
		$admin['username'] = 'root';
		$admin['password'] = 'root';
		$admin['database'] = 'groomShop2';
		$Chostname = 'localhost';
		$Cusername = 'root';
		$Cpassword = 'root';
		$CglobalDb = 'woodlesSuperAdmin';

		//for dev
		// $admin['hostname'] = '172.104.27.81';
		// $admin['username'] = 'fastforward';
		// $admin['password'] = 'F#4TAz7nABbiwCQS';
		// $admin['database'] = 'groomShop2';
		// $Chostname = '172.104.27.81';
		// $Cusername = 'fastforward';
		// $Cpassword = 'F#4TAz7nABbiwCQS';
		// $CglobalDb = 'woodlesSuperAdmin';


		$active_group = 'default';
		$query_builder = TRUE;

		$db['default'] = array(
		'dsn'	=> '',
		'hostname' => $admin['hostname'],
		'username' => $admin['username'],
		'password' => $admin['password'],
		'database' => $admin['database'],
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		// 'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8mb4',
		'dbcollat' => 'utf8mb4_unicode_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => TRUE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
		);

		$db['main'] = array(
		'dsn'	=> '',
		'hostname' => $Chostname,
		'username' => $Cusername,
		'password' => $Cpassword,
		'database' => $CglobalDb,
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8mb4',
		'dbcollat' => 'utf8mb4_unicode_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => TRUE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
		);
		