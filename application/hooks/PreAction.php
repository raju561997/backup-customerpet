<?php
class PreAction{
	function validate_user(){
	
		$this->CI =& get_instance();
		
		if(!$this->CI->session->userdata('id') && !in_array($this->CI->router->method, $this->CI->allowedActions)){
			header('Location: '. INSTALL_FOLDER);
		}
		else if($this->CI->session->userdata('id')){
			switch($this->CI->session->userdata('type')){
			
			case 'user':
				if(!in_array($this->CI->router->method, $this->CI->allowedUser) && !in_array($this->CI->router->method, $this->CI->allowedActions))
					header('Location: '. INSTALL_FOLDER);
				break;
			case 'admin':
			// echo $this->CI->router->method;
			// pr($this->CI->allowedAdmin);
			// pr( $this->CI->allowedActions);exit;
				if(!in_array($this->CI->router->method, $this->CI->allowedAdmin) && !in_array($this->CI->router->method, $this->CI->allowedActions)){

					$this->CI->session->set_flashdata('error','Sorry, You do not have permission to access this module!');
					redirect('Admin/dashboard');
				}
				break;
			case 'superadmin':
				if(!in_array($this->CI->router->method, $this->CI->allowedSuperadmin) && !in_array($this->CI->router->method, $this->CI->allowedActions))
					redirect('Superadmin/dashboard');
				break;	
			
			
			}
		}
		
	}
}

// class PreAction{
// 	function validate_user(){
// 		$this->CI =& get_instance();
// 		if(!$this->CI->session->userdata('id') && !in_array($this->CI->router->method, $this->CI->allowedActions)){
// 			redirect('admin/login');
// 		}
// 		elseif($this->CI->session->userdata('id')){
// 			switch($this->CI->session->userdata('user_type')){
// 			case 'ADMIN':
// 				if(!in_array($this->CI->router->method, $this->CI->allowedAdmin) && !in_array($this->CI->router->method, $this->CI->allowedActions))
// 					redirect('admin/dashboard');
// 				break;
// 			case 'STAFF':
// 				if(!in_array($this->CI->router->method, $this->CI->allowedStaff) && !in_array($this->CI->router->method, $this->CI->allowedActions))
// 					redirect('admin/dashboard');
// 				break;
// 			}
// 		}
		
// 	}
// }
?>