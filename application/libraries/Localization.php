<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 *	@author : CodesLab
 *  @support: support@codeslab.net
 *	date	: 05 June, 2015
 *	http://www.codeslab.net
 *  version: 1.0
 */

class Localization
{


    public function currencyFormat($currency)
    {
        $currency_format = get_option('currency_format');

        // if ( intval($currency) == $currency ) {
        //  $n = 0;
        // }
        // else {
        //   $n = 2;  
        // }

        $n = 2;
    
        if(!empty($currency_format)){

            if($currency_format == 1){
                return number_format($currency, $n, '.', '');
            }
            elseif($currency_format == 2)
            {
                return number_format($currency, $n, '.', ',');
            }
            elseif($currency_format == 3)
            {
                return number_format($currency, $n, ',', '');
            }
            elseif($currency_format == 4)
            {
                return number_format($currency, $n, ',', '.');
            }else{
                return number_format($currency);
            }

        }else{
            return number_format($currency, $n, '.', ',');
        }

    }
    public function currencyFormat1($currency)
    {
        $currency_format = get_option('currency_format');

        // if ( intval($currency) == $currency ) {
        //  $n = 0;
        // }
        // else {
        //   $n = 2;  
        // }

        $n = 2;
        $data = 0;
        if(!empty($currency_format)){

            if($currency_format == 1){
                $data = number_format($currency, $n, '.', '');
            }
            elseif($currency_format == 2)
            {
                $data = number_format($currency, $n, '.', ',');
            }
            elseif($currency_format == 3)
            {
                $data = number_format($currency, $n, ',', '');
            }
            elseif($currency_format == 4)
            {
                $data = number_format($currency, $n, ',', '.');
            }else{
                $data = number_format($currency);
            }
        }else{
            $data = number_format($currency, $n, '.', ',');
        }
        $data = str_replace(".00", "", $data);
        return $data;

    }

    public function dateFormat($date=null)
    {
        return date(get_option('date_format'), strtotime($date));
    }





}