<?php
require 'vendor/autoload.php';
use Google\Cloud\Core\ServiceBuilder;

use google\appengine\api\cloud_storage\CloudStorageTools;

use Google\Cloud\Datastore\DatastoreClient;


// Authenticate using a keyfile path

function upload_product_photo()
{
    if (isset($_FILES['p_image']['name']) && $_FILES['p_image']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_PRODUCT;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('p_image')) {
            $error = $CI->upload->display_errors();
            return json_encode(array(false, $error));
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            return json_encode(array(true, $fdata['file_name']));
            // uploading successfull, now do your further actions
        }
    }
    return false;

}


function invoiceData($id,$operation,$insertArray)
{


        
        # Instantiates a client
        $datastore = new DatastoreClient([
            'projectId' => BUCKETNAME
        ]);

        // Create/Edit an entity
        # The kind for the new entity
        $kind = 'invoice';

        # The name/ID for the new entity
        $name = $id;
        // pr($name);exit;
        # The Cloud Datastore key for the new entity
        $taskKey = $datastore->key($kind, $name);

        $result = array();
        if($operation == 'insert'){
            # Prepares the new entity
            $date = date("Y-m-d H:i:s");
            $task = $datastore->entity($taskKey, ['invoice_data' => $insertArray,'date'=>$date]);
            //pr($task);exit;
            # Saves the entity
            $datastore->upsert($task);
        }else{
            #If you know the ID of the entity, you can look it up
            $entity = $datastore->lookup($taskKey);
            if(!empty($entity)){
                $result = $entity['invoice_data'];
            }
            
        }
        
        
        return $result;
       
        #Delete entity 
        #$datastore->delete($taskKey);
         
        
}



function upload_bill()
{
    if (isset($_FILES['bill']['name']) && $_FILES['bill']['name'] != '') {
        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_BILL;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if ($CI->upload->do_upload('bill')) {
            $fdata = $CI->upload->data();
            return $fdata['file_name'];
        }
    }
    return false;

}


function handel_upload_logo()
{
    if (isset($_FILES['company_logo']['name']) && $_FILES['company_logo']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_LOGO;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('company_logo')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('Admin/settings?tab=company',$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();





            chmod(FCPATH.UPLOAD_LOGO.$fdata['file_name'] , 0777); 

            if(!is_dir(FCPATH.UPLOAD_LOGO.'large')){
                 mkdir(FCPATH.UPLOAD_LOGO.'large');
            }
            chmod(FCPATH.UPLOAD_LOGO.'large/',0777);

            if(!is_dir(FCPATH.UPLOAD_LOGO.'medium')){
                 mkdir(FCPATH.UPLOAD_LOGO.'medium');
            }
            chmod(FCPATH.UPLOAD_LOGO.'medium/',0777);

            if(!is_dir(FCPATH.UPLOAD_LOGO.'small')){
                 mkdir(FCPATH.UPLOAD_LOGO.'small');
            }
            chmod(FCPATH.UPLOAD_LOGO.'small/',0777);
                        

            $result = $CI->db->get_where('options',array('name'=>'company_logo'))->row_array();

            if((!empty($result)) && (!empty($result['value']))){
                unlink(UPLOAD_LOGO.$result['value']);
                unlink(UPLOAD_LOGO.'small/'.$result['value']);
                unlink(UPLOAD_LOGO.'medium/'.$result['value']);
                unlink(UPLOAD_LOGO.'large/'.$result['value']);
            }    


            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               generateThumbnail(FCPATH.UPLOAD_LOGO.$fdata['file_name'],FCPATH.UPLOAD_LOGO. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
               chmod(FCPATH.UPLOAD_LOGO.$size['suffix'].'/'.$fdata['file_name'], 0777);
            }



            $CI->db->where('name', 'company_logo');
           return $CI->db->update('options', array(
                'value' => $fdata['file_name']));

            // uploading successfull, now do your further actions
        }
    }
    return false;

}

function handel_upload_logo_base_64($avatar_data)
{
    if(isset($avatar_data) && !empty($avatar_data))
    {   
        $CI =& get_instance();
        $CI->load->database(); 
        $avatar = $avatar_data;

        $file = explode("data:image/jpeg;base64,", $avatar);
        $t = generateRandomString(8);
        $fdata['file_name'] = $t.'.jpeg';
        uploadtoCloud(SHOP_IMAGES.$fdata['file_name'],$file);
        
        // $temp_file_path = FCPATH.UPLOAD_LOGO.$fdata['file_name'];
        // file_put_contents($temp_file_path, base64_decode($file[1]));
        // $image_info = getimagesize($temp_file_path);

        
   
        // $default_bucket = CloudStorageTools::getDefaultGoogleStorageBucketName();
        // // $fp = fopen("gs://pet-commander-application.appspot.com/".$fdata['file_name'], 'w');
        // // fwrite($fp, base64_decode($file[1]));
        // // fclose($fp);

        // $options = ['gs' => ['Content-Type' => 'image/jpeg','acl' => 'public-read']];
        // $context = stream_context_create($options);
        // file_put_contents("gs://pet-commander-application.appspot.com/". $fdata['file_name'], base64_decode($file[1]), 0, $context);
        
         // if(!is_dir(FCPATH.UPLOAD_LOGO.'large')){
         //         mkdir(FCPATH.UPLOAD_LOGO.'large');
         //    }
         //    chmod(FCPATH.UPLOAD_LOGO.'large/',0777);

         //    if(!is_dir(FCPATH.UPLOAD_LOGO.'medium')){
         //         mkdir(FCPATH.UPLOAD_LOGO.'medium');
         //    }
         //    chmod(FCPATH.UPLOAD_LOGO.'medium/',0777);

         //    if(!is_dir(FCPATH.UPLOAD_LOGO.'small')){
         //         mkdir(FCPATH.UPLOAD_LOGO.'small');
         //    }
         //    chmod(FCPATH.UPLOAD_LOGO.'small/',0777);
                        

         //    $result = $CI->db->get_where('options',array('name'=>'company_logo'))->row_array();

         //    if((!empty($result)) && (!empty($result['value']))){
         //        unlink(UPLOAD_LOGO.$result['value']);
         //        unlink(UPLOAD_LOGO.'small/'.$result['value']);
         //        unlink(UPLOAD_LOGO.'medium/'.$result['value']);
         //        unlink(UPLOAD_LOGO.'large/'.$result['value']);
         //    }    


         //    $sizesArray = array();
         //    $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
         //    $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
         //    $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
         //    foreach($sizesArray as $size){
         //       generateThumbnail(FCPATH.UPLOAD_LOGO.$fdata['file_name'],FCPATH.UPLOAD_LOGO. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
         //       chmod(FCPATH.UPLOAD_LOGO.$size['suffix'].'/'.$fdata['file_name'], 0777);
         //    }



            $CI->db->where('name', 'company_logo');
           return $CI->db->update('options', array(
                'value' => $fdata['file_name']));

    }
    else
    {
        return false;
    }    


}

// function uploadtoCloudImageFile($FileName,$file)
// {
//     $cloud = new ServiceBuilder([
//              'keyFilePath' => FCPATH.ClientJsonFileName
//         ]);

//     $storage = $cloud->storage();
//     //$bucket = $storage->bucket('pet-commander-application.appspot.com');
//     // $bucket->upload(
//     //     fopen('/data/file.txt', 'r'),
//     //     [
//     //         'predefinedAcl' => 'publicRead'
//     //     ]
//     // );

//     $objectName = $_FILES[];
//     $content = base64_decode($file[1]);
//     $metadata = ['contentType' => 'image/jpeg'];
//     return $storage->bucket(BUCKETNAME)->upload($content, [
//         'name' => $FileName,
//         'metadata' => $metadata,
//         'predefinedAcl' => 'publicRead'
//     ]);
// }


function uploadtoCloud($FileName,$file)
{
    $cloud = new ServiceBuilder([
             'keyFilePath' => FCPATH.ClientJsonFileName
        ]);

    $storage = $cloud->storage();
    //$bucket = $storage->bucket('pet-commander-application.appspot.com');
    // $bucket->upload(
    //     fopen('/data/file.txt', 'r'),
    //     [
    //         'predefinedAcl' => 'publicRead'
    //     ]
    // );

    $objectName = $FileName;
    $content = base64_decode($file[1]);
    $metadata = ['contentType' => 'image/jpeg'];
    return $storage->bucket(BUCKETNAME)->upload($content, [
        'name' => $FileName,
        'metadata' => $metadata,
        'predefinedAcl' => 'publicRead'
    ]);
}

function upload_pet_pdf($file)
{
    $t = generateRandomString(10);
    
    if($file['type'] == 'application/pdf'){
        $file_name = $t.'.pdf';
    }else{
       $file_name = $t.'.jpeg'; 
    }
    
    $temp = pdfuploadtoCloud(PETS_IMAGES.$file_name,$file);  
    return $file_name;
}

function pdfuploadtoCloud($FileName,$file)
{
    
    $cloud = new ServiceBuilder([
             'keyFilePath' => FCPATH.ClientJsonFileName
        ]);

    $storage = $cloud->storage();
    
    
    $content = fopen($file['tmp_name'], 'r');
    $metadata = ['contentType' => $file['type'],'Disposition' => 'inline'];
    
    return $storage->bucket(BUCKETNAME)->upload($content, [
        'name' => $FileName,
        'metadata' => $metadata,
        'predefinedAcl' => 'publicRead'
    ]);
}

function uploadtoCloudWatermarkImage($FileName,$file,$address,$cityStateZipCode,$company_logo)
{
    $CI =& get_instance();
    $cloud = new ServiceBuilder([
             'keyFilePath' => FCPATH.ClientJsonFileName
        ]);

    $storage = $cloud->storage();
    //$bucket = $storage->bucket('pet-commander-application.appspot.com');
    // $bucket->upload(
    //     fopen('/data/file.txt', 'r'),
    //     [
    //         'predefinedAcl' => 'publicRead'
    //     ]
    // );

    $image = $FileName;

    /*
     * set the watermark font size
     * possible values 1,2,3,4, and 5
     * where 5 is the biggest
     */

    //set the watermark text


    //$text = $this->session->userdata('shop_name');
    
    /*
     * position the watermark
     * 10 pixels from the left
     * and 10 pixels from the top
     */
    $shop_name = $CI->session->userdata('shop_name');
    $shopno = $CI->session->userdata('shopno');
    $fontSize = 10;
    $px = imagefontwidth($fontSize);
        //pr($file[1]);
    //create a new image
    $newImg = imagecreatefromstring(base64_decode($file[1]));
    
    //set the watermark font color to red
    $fontColor = imagecolorallocate($newImg, 0, 0, 0);

    // $w = imagesx($newImg) - ((strlen($shop_name)*$px)+30);
    // $h = imagesy($newImg) - 75;
    // $w1 = imagesx($newImg) - ((strlen($address)*$px)+30);
    // $h1 = imagesy($newImg) - 60;
    // $w2 = imagesx($newImg) - ((strlen($cityStateZipCode)*$px)+30);
    // $h2 = imagesy($newImg) - 45;
    // $w3 = imagesx($newImg) - ((strlen('Contact No: '.$shopno)*$px)+30);
    // $h3 = imagesy($newImg) - 30;


    $w4 = 0;
    $h4 = imagesy($newImg) - 70;
  
    $logoWidth = 0;
    $logoHeight = 0;
    if (!empty(get_option('company_logo'))) {
        $logoWidth = 50;
        $logoHeight = 50;
    }

   

    $watermark_div_x = 10+$logoWidth+10+((strlen($shop_name)*$px)+10)+10;
    $watermark_div_y = 10+$logoHeight+10;

    $dst1 = imagecreatetruecolor (500, 500);
    $white = imagecolorallocate($dst1, 255, 255, 255);
    imagefilledrectangle($dst1, 0, 0, 499, 499, $white);
    //$black = imagecolorallocate($dst1, 0, 0, 0);
    imagecopy($newImg, $dst1, $w4, $h4, 0, 0, $watermark_div_x, $watermark_div_y);
    

    putenv('GDFONTPATH=' . realpath('.'));

    // Name the font to be used (note the lack of the .ttf extension)
    $font = 'arial';

    if(!empty(get_option('company_logo'))){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://storage.googleapis.com/'.BUCKETNAME.'/'.SHOP_IMAGES.$company_logo); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // good edit, thanks!
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); // also, this seems wise considering output is image.
        $data = curl_exec($ch);
        curl_close($ch);

        $stamp = imagecreatefromstring($data);
        $dst2 = imagecreatetruecolor ($logoWidth, $logoHeight );
        imagecopyresampled($dst2, $stamp, 0, 0, 0, 0, $logoWidth, $logoHeight, imagesx($stamp), imagesy($stamp));
        imagecopy($newImg, $dst2, 10, ($h4+10), 0, 0, $logoWidth, $logoHeight);
        
    }


    imagestring($newImg, $fontSize, (10+$logoWidth+10), ($h4+30), $shop_name, $fontColor);
    imagealphablending($newImg, false);
    imagesavealpha($newImg, true);

  

    //output the new image with a watermark to a file
    //imagejpeg($newImg,"add-a-text-watermark-to-an-image-with-php_01.jpg",100);
    /*
     * PNG file appeared to have
     * a better quality than the JPG
     */
    ob_start();
    imagepng($newImg);
    $imagedata = ob_get_contents();
    ob_end_clean();


    $objectName = $FileName;
    //$content = base64_decode($file[1]);
    //pr($imagedata);exit;
    $metadata = ['contentType' => 'image/jpeg'];
    return $storage->bucket(BUCKETNAME)->upload($imagedata, [
        'name' => $FileName,
        'metadata' => $metadata,
        'predefinedAcl' => 'publicRead'
    ]);
}

function handel_upload_login_logo()
{
    if (isset($_FILES['login_logo']['name']) && $_FILES['login_logo']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_LOGO;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('login_logo')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/settings?tab=company',$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            $CI->db->where('name', 'login_logo');
            $CI->db->update('options', array(
                'value' => $fdata['file_name']
            ));

            // uploading successfull, now do your further actions
        }
    }
    return false;

}

function handel_upload_icon()
{
    if (isset($_FILES['icon']['name']) && $_FILES['icon']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_LOGO;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('icon')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/settings?tab=company',$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            $CI->db->where('name', 'icon');
            $CI->db->update('options', array(
                'value' => $fdata['file_name']
            ));

            // uploading successfull, now do your further actions
        }
    }
    return false;

}

function handel_upload_invoice_logo()
{
    if (isset($_FILES['invoice_logo']['name']) && $_FILES['invoice_logo']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = UPLOAD_LOGO; //were u want to upload
        $config['allowed_types'] = '*'; // file type
        $config['max_size'] = '2024'; //size of this file


        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('invoice_logo')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/settings?tab=invoice',$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();

            chmod(FCPATH.UPLOAD_LOGO.$fdata['file_name'] , 0777); 

            if(!is_dir(FCPATH.UPLOAD_LOGO.'large')){
                 mkdir(FCPATH.UPLOAD_LOGO.'large');
            }
            chmod(FCPATH.UPLOAD_LOGO.'large/',0777);

            if(!is_dir(FCPATH.UPLOAD_LOGO.'medium')){
                 mkdir(FCPATH.UPLOAD_LOGO.'medium');
            }
            chmod(FCPATH.UPLOAD_LOGO.'medium/',0777);

            if(!is_dir(FCPATH.UPLOAD_LOGO.'small')){
                 mkdir(FCPATH.UPLOAD_LOGO.'small');
            }
            chmod(FCPATH.UPLOAD_LOGO.'small/',0777);
                        


            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               generateThumbnail(FCPATH.UPLOAD_LOGO.$fdata['file_name'],FCPATH.UPLOAD_LOGO. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
               chmod(FCPATH.UPLOAD_LOGO.$size['suffix'].'/'.$fdata['file_name'], 0777);
            }



            $CI->db->where('name', 'invoice_logo');
           return $CI->db->update('options', array(
                'value' => $fdata['file_name']));
            
        }
    }
    return false;

}

function upload_employee_photo($employee_id = null, $id = null)
{

    if (isset($_FILES['employee_photo']['name']) && $_FILES['employee_photo']['name'] != '') {

        $CI =& get_instance();
        $config['upload_path'] = UPLOAD_EMPLOYEE.$employee_id;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('employee_photo')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/employee/employeeDetails/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();




            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        }
    }
    return false;

}


function uploadTempStripeDocument()
{
     if (isset($_FILES['employee_photo']['name']) && $_FILES['employee_photo']['name'] != '') {
        //echo $_FILES['employee_photo']['name'];
        // $CI =& get_instance();
        // $config['upload_path'] = SHOP_IMAGES;
        // $config['allowed_types'] = '*';
        // $config['max_size'] = '2024';
        // $config['encrypt_name'] = TRUE;

        // $CI->load->library('upload', $config);
        // $CI->upload->initialize($config);

        // if (!$CI->upload->do_upload('employee_photo')) {
        //     // $error = $CI->upload->display_errors();
        //     // $CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
        //     // return false;
        //     // uploading failed. $error will holds the errors.
        // } else {
        //     $fdata = $CI->upload->data();
        //     chmod(FCPATH.SHOP_IMAGES.$fdata['file_name'] , 0777); 
        //     return $fdata['file_name'];

        //     // uploading successfull, now do your further actions
        // }

        $cloud = new ServiceBuilder([
                        'keyFilePath' => FCPATH.ClientJsonFileName
                ]);

        $storage = $cloud->storage();
        $t = generateRandomString(8);
        $fdata['file_name'] = $t.'.jpeg';

        $objectName = SHOP_IMAGES.$fdata['file_name'];

       
        $file_name = SHOP_IMAGES.$fdata['file_name'];
        //tmp moving file
        //move_uploaded_file($_FILES["employee_photo"]["tmp_name"],FCPATH.'assets/uploads/'.$fdata['file_name']);
        // $obj = new Google_Service_Storage_StorageObject();
        // $obj->setName($file_name);
        // $f = fopen($_FILES['images[]']['name'], 'r');
        // $storage->objects->insert(
        //  BUCKETNAME,
        //  $obj,
        //  ['name' => $file_name, 'data' => file_get_contents($f), 'uploadType' => 'media']
        // );

         $options = [
              'resumable' => true,
              'name' => $file_name,
              'metadata' => [
                  'contentLanguage' => 'en',
                  'contentType' => 'image/jpeg'
              ],
              'predefinedAcl' => 'publicRead'
          ];
     
          $object = $storage->bucket(BUCKETNAME)->upload(
              fopen($_FILES['employee_photo']['tmp_name'], 'r'),
              $options
          );

          return $fdata['file_name'];
    }
    return false;
}


function upload_appointment_complete_image_base_64($avatar,$address,$cityState,$company_logo)
{
    $file = explode("data:image/jpeg;base64,", $avatar);
    $t = generateRandomString(8);
    $fdata['file_name'] = $t.'.jpeg';
    uploadtoCloud(PETS_IMAGES.$fdata['file_name'],$file);
    uploadtoCloudWatermarkImage(WatermarkImages.$fdata['file_name'],$file,$address,$cityState,$company_logo);
    // $temp_file_path = FCPATH.UPLOAD_PETS.$fdata['file_name'];
    // file_put_contents($temp_file_path, base64_decode($file[1]));
    // $image_info = getimagesize($temp_file_path); 
    // chmod(FCPATH.UPLOAD_PETS.$fdata['file_name'] , 0777); 

    // if(!is_dir(FCPATH.UPLOAD_PETS.'large')){
    //      mkdir(FCPATH.UPLOAD_PETS.'large');
    // }
    // chmod(FCPATH.UPLOAD_PETS.'large/',0777);

    // if(!is_dir(FCPATH.UPLOAD_PETS.'medium')){
    //      mkdir(FCPATH.UPLOAD_PETS.'medium');
    // }
    // chmod(FCPATH.UPLOAD_PETS.'medium/',0777);

    // if(!is_dir(FCPATH.UPLOAD_PETS.'small')){
    //      mkdir(FCPATH.UPLOAD_PETS.'small');
    // }
    // chmod(FCPATH.UPLOAD_PETS.'small/',0777);

    // if(!is_dir(FCPATH.UPLOAD_PETS.'xlarge')){
    //      mkdir(FCPATH.UPLOAD_PETS.'xlarge');
    // }
    // chmod(FCPATH.UPLOAD_PETS.'xlarge/',0777);


    // $sizesArray = array();
    // $sizesArray[] = array('height' => $image_info[1], 'width' => $image_info[0], 'suffix' => 'xlarge');
    // $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
    // $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
    // $sizesArray[] = array('height' => 250, 'width' => 250, 'suffix' => 'small');
    // foreach($sizesArray as $size){
    //            generateThumbnail(FCPATH.UPLOAD_PETS.$fdata['file_name'],FCPATH.UPLOAD_PETS. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
    //            chmod(FCPATH.UPLOAD_PETS.$size['suffix'].'/'.$fdata['file_name'], 0777);
    //         }

            

    //echo "file uploaded";
    return $fdata['file_name'];
    



}


function upload_staff_photo_base_64($id=null,$avatar)
{
    $file = explode("data:image/jpeg;base64,", $avatar);
    $t = generateRandomString(8);
    $fdata['file_name'] = $t.'.jpeg';
    uploadtoCloud(SHOP_IMAGES.$fdata['file_name'],$file);
    //echo "file uploaded";
    return $fdata['file_name'];
}

function upload_pet_photo_base_64($id=null,$avatar)
{
        //print_r($avatar);
        //$file = explode("data:image/jpeg;base64,", $avatar);
        $file = explode(";base64,", $avatar);
        $t = generateRandomString(8);
        $fdata['file_name'] = $t.'.jpeg';
        uploadtoCloud(PETS_IMAGES.$fdata['file_name'],$file);
        // uploadtoCloudWatermarkImage(WatermarkImages.$fdata['file_name'],$file);
        // $temp_file_path = FCPATH.UPLOAD_PETS.$fdata['file_name'];
        // file_put_contents($temp_file_path, base64_decode($file[1]));
        // $image_info = getimagesize($temp_file_path); 
        //     chmod(FCPATH.UPLOAD_PETS.$fdata['file_name'] , 0777); 

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'large')){
        //          mkdir(FCPATH.UPLOAD_PETS.'large');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'large/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'medium')){
        //          mkdir(FCPATH.UPLOAD_PETS.'medium');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'medium/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'small')){
        //          mkdir(FCPATH.UPLOAD_PETS.'small');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'small/',0777);
                        


        //     $sizesArray = array();
        //     $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
        //     $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
        //     $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
        //     foreach($sizesArray as $size){
        //        generateThumbnail(FCPATH.UPLOAD_PETS.$fdata['file_name'],FCPATH.UPLOAD_PETS. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
        //        chmod(FCPATH.UPLOAD_PETS.$size['suffix'].'/'.$fdata['file_name'], 0777);
        //     }

            

    //echo "file uploaded";
    return $fdata['file_name'];
}


function upload_pet_photo($employee_id = null, $id = null)
{
    
    if (isset($_FILES['employee_photo']['name']) && $_FILES['employee_photo']['name'] != '' && $_FILES['employee_photo']['name'] != null ) 
    {
       
        // $CI =& get_instance();
        // $config['upload_path'] = UPLOAD_PETS;
        // $config['allowed_types'] = '*';
        // $config['max_size'] = '252408';
        // $config['encrypt_name'] = TRUE;

        // $CI->load->library('upload', $config);
        // $CI->upload->initialize($config);

        // if (!$CI->upload->do_upload('employee_photo')) {
        //     //$error = $CI->upload->display_errors();
        //     //$CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
        //     //echo $error;
        //     //return false;
        //     // uploading failed. $error will holds the errors.
        // } else {
        //     $fdata = $CI->upload->data();
        //     chmod(FCPATH.UPLOAD_PETS.$fdata['file_name'] , 0777); 

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'large')){
        //          mkdir(FCPATH.UPLOAD_PETS.'large');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'large/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'medium')){
        //          mkdir(FCPATH.UPLOAD_PETS.'medium');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'medium/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'small')){
        //          mkdir(FCPATH.UPLOAD_PETS.'small');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'small/',0777);
                        


        //     $sizesArray = array();
        //     $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
        //     $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
        //     $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
        //     foreach($sizesArray as $size){
        //        generateThumbnail(FCPATH.UPLOAD_PETS.$fdata['file_name'],FCPATH.UPLOAD_PETS. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
        //        chmod(FCPATH.UPLOAD_PETS.$size['suffix'].'/'.$fdata['file_name'], 0777);
        //     }


                $cloud = new ServiceBuilder([
                        'keyFilePath' => FCPATH.ClientJsonFileName
                ]);

                $storage = $cloud->storage();
                $t = generateRandomString(8);
                $fdata['file_name'] = $t.'.jpeg';

                $objectName = PETS_IMAGES.$fdata['file_name'];

               
                $file_name = PETS_IMAGES.$fdata['file_name'];
                // $obj = new Google_Service_Storage_StorageObject();
                // $obj->setName($file_name);
                // $f = fopen($_FILES['images[]']['name'], 'r');
                // $storage->objects->insert(
                //  BUCKETNAME,
                //  $obj,
                //  ['name' => $file_name, 'data' => file_get_contents($f), 'uploadType' => 'media']
                // );

                 $options = [
                      'resumable' => true,
                      'name' => $file_name,
                      'metadata' => [
                          'contentLanguage' => 'en',
                          'contentType' => 'image/jpeg'
                      ],
                      'predefinedAcl' => 'publicRead'
                  ];
             
                  $object = $storage->bucket(BUCKETNAME)->upload(
                      fopen($_FILES['employee_photo']['tmp_name'], 'r'),
                      $options
                  );



            //echo "file uploaded";exit;
            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        // }
    }

    

    return false;

}


function upload_pet_photo1($employee_id = null, $id = null)
{
    
    if (isset($_FILES['vaccination_image']['name']) && $_FILES['vaccination_image']['name'] != '' && $_FILES['vaccination_image']['name'] != null ) 
    {
       
        // $CI =& get_instance();
        // $config['upload_path'] = UPLOAD_PETS;
        // $config['allowed_types'] = '*';
        // $config['max_size'] = '252408';
        // $config['encrypt_name'] = TRUE;

        // $CI->load->library('upload', $config);
        // $CI->upload->initialize($config);

        // if (!$CI->upload->do_upload('vaccination_image')) {
        //     //$error = $CI->upload->display_errors();
        //     //$CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
        //     //echo $error;
        //     //return false;
        //     // uploading failed. $error will holds the errors.
        // } else {
        //     $fdata = $CI->upload->data();
        //     chmod(FCPATH.UPLOAD_PETS.$fdata['file_name'] , 0777); 

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'large')){
        //          mkdir(FCPATH.UPLOAD_PETS.'large');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'large/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'medium')){
        //          mkdir(FCPATH.UPLOAD_PETS.'medium');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'medium/',0777);

        //     if(!is_dir(FCPATH.UPLOAD_PETS.'small')){
        //          mkdir(FCPATH.UPLOAD_PETS.'small');
        //     }
        //     chmod(FCPATH.UPLOAD_PETS.'small/',0777);
                        


        //     $sizesArray = array();
        //     $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
        //     $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
        //     $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
        //     foreach($sizesArray as $size){
        //        generateThumbnail(FCPATH.UPLOAD_PETS.$fdata['file_name'],FCPATH.UPLOAD_PETS. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
        //        chmod(FCPATH.UPLOAD_PETS.$size['suffix'].'/'.$fdata['file_name'], 0777);
        //     }


                $cloud = new ServiceBuilder([
                        'keyFilePath' => FCPATH.ClientJsonFileName
                ]);

                $storage = $cloud->storage();
                $t = generateRandomString(8);
                $fdata['file_name'] = $t.'.jpeg';

                $objectName = PETS_IMAGES.$fdata['file_name'];

               
                $file_name = PETS_IMAGES.$fdata['file_name'];
                // $obj = new Google_Service_Storage_StorageObject();
                // $obj->setName($file_name);
                // $f = fopen($_FILES['images[]']['name'], 'r');
                // $storage->objects->insert(
                //  BUCKETNAME,
                //  $obj,
                //  ['name' => $file_name, 'data' => file_get_contents($f), 'uploadType' => 'media']
                // );

                 $options = [
                      'resumable' => true,
                      'name' => $file_name,
                      'metadata' => [
                          'contentLanguage' => 'en',
                          'contentType' => 'image/jpeg'
                      ],
                      'predefinedAcl' => 'publicRead'
                  ];
             
                  $object = $storage->bucket(BUCKETNAME)->upload(
                      fopen($_FILES['vaccination_image']['tmp_name'], 'r'),
                      $options
                  );



            //echo "file uploaded";exit;
            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        // }
    }

    

    return false;

}

function shop_images_for_services_base64($employee_id = null,$avatar)
{
    $file = explode("data:image/jpeg;base64,", $avatar);
    $t = generateRandomString(8);
    $fdata['file_name'] = $t.'.jpeg';
    uploadtoCloud(SHOP_IMAGES.$fdata['file_name'],$file);
    // $temp_file_path = FCPATH.SHOP_IMAGES.$fdata['file_name'];
    // file_put_contents($temp_file_path, base64_decode($file[1]));
    // $image_info = getimagesize($temp_file_path); 
    // chmod(FCPATH.SHOP_IMAGES.$fdata['file_name'] , 0777);
    // if(!is_dir(FCPATH.SHOP_IMAGES.'large')){
    //      mkdir(FCPATH.SHOP_IMAGES.'large');
    // }
    // chmod(FCPATH.SHOP_IMAGES.'large/',0777);

    // if(!is_dir(FCPATH.SHOP_IMAGES.'medium')){
    //      mkdir(FCPATH.SHOP_IMAGES.'medium');
    // }
    // chmod(FCPATH.SHOP_IMAGES.'medium/',0777);

    // if(!is_dir(FCPATH.SHOP_IMAGES.'small')){
    //      mkdir(FCPATH.SHOP_IMAGES.'small');
    // }
    // chmod(FCPATH.SHOP_IMAGES.'small/',0777);
                        


    // $sizesArray = array();
    // $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
    // $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
    // $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
    // foreach($sizesArray as $size){
    //    generateThumbnail(FCPATH.SHOP_IMAGES.$fdata['file_name'],FCPATH.SHOP_IMAGES. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
    //    chmod(FCPATH.SHOP_IMAGES.$size['suffix'].'/'.$fdata['file_name'], 0777);
    // }




    return $fdata['file_name']; 

}

function shop_images_for_services($employee_id = null, $id = null)
{

    if (isset($_FILES['employee_photo']['name']) && $_FILES['employee_photo']['name'] != '') {
        //echo $_FILES['employee_photo']['name'];
        $CI =& get_instance();
        $config['upload_path'] = SHOP_IMAGES;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';
        $config['encrypt_name'] = TRUE;

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('employee_photo')) {
            // $error = $CI->upload->display_errors();
            // $CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
            // return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            chmod(FCPATH.SHOP_IMAGES.$fdata['file_name'] , 0777); 

            if(!is_dir(FCPATH.SHOP_IMAGES.'large')){
                 mkdir(FCPATH.SHOP_IMAGES.'large');
            }
            chmod(FCPATH.SHOP_IMAGES.'large/',0777);

            if(!is_dir(FCPATH.SHOP_IMAGES.'medium')){
                 mkdir(FCPATH.SHOP_IMAGES.'medium');
            }
            chmod(FCPATH.SHOP_IMAGES.'medium/',0777);

            if(!is_dir(FCPATH.SHOP_IMAGES.'small')){
                 mkdir(FCPATH.SHOP_IMAGES.'small');
            }
            chmod(FCPATH.SHOP_IMAGES.'small/',0777);
                        


            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               generateThumbnail(FCPATH.SHOP_IMAGES.$fdata['file_name'],FCPATH.SHOP_IMAGES. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
               chmod(FCPATH.SHOP_IMAGES.$size['suffix'].'/'.$fdata['file_name'], 0777);
            }




            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        }
    }
    return false;

}



   function generateThumbnail($fullSizePath,$thumbnailPath,$imageName, $height, $width,$suffix)
   {

       include_once(APPPATH."libraries/phpthumb/phpthumb.class.php");

       $phpThumb = new phpThumb();

       
       if($suffix == 'large'||$suffix == 'medium'||$suffix == 'small')
       {
            //$phpThumb->setParameter('h', $height);
            $phpThumb->setParameter('w', $width);

            $phpThumb->setParameter('far', '1');
            
       }
       else
       {
           //$phpThumb->setParameter('w', $width);

           $phpThumb->setParameter('far', '1');

           $phpThumb->setParameter('q', 60); 
       }
      

       // $phpThumb->setParameter('zc', '1');
       
        




       $phpThumb->setSourceFilename($fullSizePath);


               

       if ($phpThumb->GenerateThumbnail())
       {
           $phpThumb->RenderToFile($thumbnailPath.'/'.$imageName);
       }

       //echo $thumbnailPath;exit;

         //echo 'Failed bla:<pre>'.implode("\n\n", $phpThumb->debugmessages).'</pre>';

         return $phpThumb->purgeTempFiles();
         //echo "adsdasad";exit;
   }


function uploadBoardingImage()
{
    if ($_FILES['employee_photo']['name'] != '' && !empty($_FILES['employee_photo']['name']) ) {

       
        //echo "here";

        $CI =& get_instance();
        $config['upload_path'] = SHOP_IMAGES;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';
        $config['encrypt_name'] = TRUE;

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('employee_photo')) {
            // $error = $CI->upload->display_errors();
            // $CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
            // return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();

            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               generateThumbnail(FCPATH.SHOP_IMAGES.$fdata['file_name'],FCPATH.SHOP_IMAGES. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
               chmod(FCPATH.SHOP_IMAGES.$size['suffix'].'/'.$fdata['file_name'], 0777);
            }



           
            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        }
    }
}

function uploadDayCareBoardingImage()
{
    if($_FILES['branding_photo']['name'] != '' && !empty($_FILES['branding_photo']['name'])) {
       
        $CI =& get_instance();
        $config['upload_path'] = SHOP_IMAGES;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';
        $config['encrypt_name'] = TRUE;

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('branding_photo')) {
            // $error = $CI->upload->display_errors();
            // $CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
            // return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();

            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               generateThumbnail(FCPATH.SHOP_IMAGES.$fdata['file_name'],FCPATH.SHOP_IMAGES. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
               chmod(FCPATH.SHOP_IMAGES.$size['suffix'].'/'.$fdata['file_name'], 0777);
            }



           return $fdata['file_name'];
        }      
    }
}


function uploadDayCareBoardingImages_base64($avatar)
{
    $file = explode("data:image/jpeg;base64,", $avatar);
    $t = generateRandomString(8);
    $fdata['file_name'] = $t.'.jpeg';
    uploadtoCloud(SHOP_IMAGES.$fdata['file_name'],$file);
    // $temp_file_path = FCPATH.SHOP_IMAGES.$fdata['file_name'];
    // file_put_contents($temp_file_path, base64_decode($file[1]));
    // $image_info = getimagesize($temp_file_path); 
    // chmod(FCPATH.SHOP_IMAGES.$fdata['file_name'] , 0777);

    // if(!is_dir(FCPATH.SHOP_IMAGES.'large')){
    //     mkdir(FCPATH.SHOP_IMAGES.'large');
    // }
    // //chmod(FCPATH.SHOP_IMAGES.'large/',0777);

    // if(!is_dir(FCPATH.SHOP_IMAGES.'medium')){
    //      mkdir(FCPATH.SHOP_IMAGES.'medium');
    // }
    // //chmod(FCPATH.SHOP_IMAGES.'medium/',0777);

    // if(!is_dir(FCPATH.SHOP_IMAGES.'small')){
    //      mkdir(FCPATH.SHOP_IMAGES.'small');
    // }
    // //chmod(FCPATH.SHOP_IMAGES.'small/',0777);


    // $sizesArray = array();
    // $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
    // $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
    // $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
    // foreach($sizesArray as $size){
    //    generateThumbnail(FCPATH.SHOP_IMAGES.$fdata['file_name'],FCPATH.SHOP_IMAGES. $size['suffix'],$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
    //    chmod(FCPATH.SHOP_IMAGES.$size['suffix'].'/'.$fdata['file_name'], 0777);
    // }

    return $fdata['file_name'];

}    



function shop_images($employee_id = null, $id = null)
{

    if (isset($_FILES['employee_photo']['name']) && $_FILES['employee_photo']['name'] != '') {

        $CI =& get_instance();
        $config['upload_path'] = UPLOAD_PETS;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2024';
        $config['encrypt_name'] = TRUE;

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('employee_photo')) {
            // $error = $CI->upload->display_errors();
            // $CI->message->custom_error_msg('admin/entry/addNewPet/'. str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id))  ,$error);
            // return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();

            $sizesArray = array();
            $sizesArray[] = array('height' => 500, 'width' => 500, 'suffix' => 'large');
            $sizesArray[] = array('height' => 100, 'width' => 100, 'suffix' => 'medium');
            $sizesArray[] = array('height' => 50, 'width' => 50, 'suffix' => 'small');
            foreach($sizesArray as $size){
               $this->generateThumbnail(UPLOAD_PETS.$fdata['file_name'],UPLOAD_PETS. $size['suffix'].'/'.$fdata['file_name'], $size['height'], $size['width'],$size['suffix']);
            }




            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        }
    }
    return false;

}

function upload_employee_file($employee_id, $type, $id = null)
{
    if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {

        $CI =& get_instance();

        $config['upload_path'] = UPLOAD_EMPLOYEE.$employee_id;
        $config['allowed_types'] = $type;
        $config['max_size'] = '2024';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('file')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/employee/employeeDetails/'.str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id)),$error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            return $fdata['file_name'];

            // uploading successfull, now do your further actions
        }
    }
    return false;

}


function upload_attachment()
{
    if (isset($_FILES['attachment']['name']) && $_FILES['attachment']['name'] != '') {

        $CI =& get_instance();
        $CI->load->database();
        $config['upload_path'] = ATTACHMENT;
        $config['allowed_types'] = '*';
        $config['max_size'] = '9024';

        $CI->load->library('upload', $config);
//        $CI->upload->initialize($config);

        $files           = $_FILES;
        $filesCount = count($_FILES['attachment']['name']);

        for($i = 0; $i < $filesCount; $i++){
            $fileName = date("Y-m-d").time().'@@@'.$files['attachment']['name'][$i];
            $_FILES['attachment']['name'] = $fileName;
            $_FILES['attachment']['type'] = $files['attachment']['type'][$i];
            $_FILES['attachment']['tmp_name'] = $files['attachment']['tmp_name'][$i];
            $_FILES['attachment']['error'] = $files['attachment']['error'][$i];
            $_FILES['attachment']['size'] = $files['attachment']['size'][$i];

            $CI->upload->initialize($config);

            if(!$CI->upload->do_upload('attachment')){
                $error[$i] = '<strong>'.$files['attachment']['name'][$i].'</strong> '.$CI->upload->display_errors();
            }else{
                $fileData = $CI->upload->data();
                $uploadData[$i]['file_name'] = $fileData['file_name'];
                $uploadData[$i]['file_ext'] = $fileData['file_ext'];
                $uploadData[$i]['file_size'] = $fileData['file_size'];
            }
        }

        if(empty($error))
            $error =0;

        $data = array(
            'success' =>$uploadData,
            'error' =>$error
        );

        return json_encode($data);


    }
    return false;

}

function loadCloudImage($path)
{

    // $cloud = new ServiceBuilder([
 
    $options = ['size' => 100, 'crop' => false];
    $image_file = "gs://".BUCKETNAME."/".$path;
     try {
        $image_url = CloudStorageTools::getImageServingUrl($image_file, $options);
    }
    catch(Exception $e) {
        $image_url = base_url().'assets/default-p.png';
    }

    return  $image_url ;
    
}

function loadCloudImageVariableSize($path,$size)
{

    $options = ['size' => $size, 'crop' => false];
    $image_file = "gs://".BUCKETNAME."/".$path;
    try {
        $image_url = CloudStorageTools::getImageServingUrl($image_file, $options);
    }
    catch(Exception $e) {
        $image_url = base_url().'assets/default-p.png';
    }
    return  $image_url ;
    
}


function getWatermarkImage($img)
{
   //the image to be watermarked
    $image = $img;
    echo $image;

    /*
     * set the watermark font size
     * possible values 1,2,3,4, and 5
     * where 5 is the biggest
     */
    $fontSize = 4;

    //set the watermark text
    $text = "ConsistentCoder.com";

    /*
     * position the watermark
     * 10 pixels from the left
     * and 10 pixels from the top
     */
    $xPosition = 10;
    $yPosition = 10;

    //create a new image
    //$newImg = imagecreatefromjpeg($image);
    //pr($newImg);
    //echo $newImg;exit;
    //set the watermark font color to red
    $fontColor = imagecolorallocate($newImg, 255, 0, 0);

    //write the watermark on the created image
    imagestring($newImg, $fontSize, $xPosition, $yPosition, $text, $fontColor);

    //output the new image with a watermark to a file
    //imagejpeg($newImg,"add-a-text-watermark-to-an-image-with-php_01.jpg",100);
    /*
     * PNG file appeared to have
     * a better quality than the JPG
     */
    return  imagepng($newImg,"add-a-text-watermark-to-an-image-with-php_01.png");

    /*
     * destroy the image to free
     * any memory associated with it
     */
    
}
