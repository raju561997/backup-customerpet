<?php
function pr(&$a)
{
    echo '<pre>';
    print_r($a);
    echo '</pre>';
}

function vd(&$a)
{
    echo '<pre>';
    var_dump($a);
    echo '</pre>';
}

function my_error_handler()
{
 
  if (http_response_code() == 500 || http_response_code() == '500')
  {
		$CI =& get_instance();
		echo $CI->load->view("/admin/custom500view",[], TRUE);
  
  }
}

function get_zip_info($zip) { 
    //Function to retrieve the contents of a webpage and put it into $pgdata
  $pgdata =""; //initialize $pgdata
    // Open the url based on the user input and put the data into $fd:
  $fd = fopen("http://zipinfo.com/cgi-local/zipsrch.exe?zip=$zip","r"); 
  while(!feof($fd)) {//while loop to keep reading data into $pgdata till its all gone
    $pgdata .= fread($fd, 1024); //read 1024 bytes at a time
  }
  fclose($fd); //close the connection
  if (preg_match("/is not currently assigned/", $pgdata)) {
    $city = "N/A";
    $state = "N/A";
  } else {
    $citystart = strpos($pgdata, "Code</th></tr><tr><td align=center>");
    $citystart = $citystart + 35;
    $pgdata    = substr($pgdata, $citystart);
    $cityend   = strpos($pgdata, "</font></td><td align=center>");
    $city      = substr($pgdata, 0, $cityend);
  
    $statestart = strpos($pgdata, "</font></td><td align=center>");
    $statestart = $statestart + 29;
    $pgdata     = substr($pgdata, $statestart);
    $stateend   = strpos($pgdata, "</font></td><td align=center>");
    $state      = substr($pgdata, 0, $stateend);
  }
  $zipinfo['zip']   = $zip;
  $zipinfo['city']  = $city;
  $zipinfo['state'] = $state;
  return $zipinfo;
}

function random_password( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}

function random_code($limit = 8)
{
    return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}
function getRealPOST() {
   $pairs = explode("&", file_get_contents("php://input"));
   $vars = array();
   foreach ($pairs as $pair) {
       $nv = explode("=", $pair);
       $value = "";
       if(isset($nv[1])) {
           $value = urldecode($nv[1]);
       }
       $name = urldecode($nv[0]);
       if(preg_match('/\[\]$/', $name ,$matches))
        {
            $name = str_replace('[]', '', $name);
            if(!isset($vars[$name])){
                $vars[$name] = array();
            }
            $vars[$name][] = $value;
            
        } else {
            $vars[$name] = $value;
        }
   }
   return $vars;
}

function generateItemUrl($id, $itemTitle)
{
    $url = base_url()."item/listing_view?item_id=".$id;

    $itemId = $id;
    $idLen = strlen($itemId);
    
//    $CI =& get_instance();
//    $CI->load->model('Item_model');
//    $itemTitle = $CI->Item_model->getItemTitle($itemId);
    
    $generatedItemTitle = url_title($itemTitle);

    $patterns = array();
    $patterns[0] = '/listing_view\?/';
    $patterns[1] = '/item_id=/';
    $replacements = array();
    $replacements[1] = $itemId.'/';
    $replacements[0] = $generatedItemTitle;

    $newUrl = preg_replace($patterns, $replacements, $url);
    $len = strlen($newUrl);
    $endPoint = $len-$idLen;
    $generatedUrl = substr($newUrl,0,$endPoint);

    return $generatedUrl;
}

function makeClickableLinks($text)
{
    $text = html_entity_decode($text);
    $text = " ".$text;
    $text = preg_replace('/(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)/',
            '<a href="\\1" target=_blank>\\1</a>', $text);
    $text = preg_replace('/(((f|ht){1}tps://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)/',
            '<a href="\\1" target=_blank>\\1</a>', $text);
    $text = preg_replace('/([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)/',
    '\\1<a href="http://\\2" target=_blank>\\2</a>', $text);
    $text = preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/',
    '<a href="mailto:\\1" target=_blank>\\1</a>', $text);
    return $text;
}


function format_telephone($phone_number)
{
// echo $phone_number;exit;
	$txt = str_replace("() -", "", $phone_number);

    $txt = str_replace("()", "", $txt);
    $txt = str_replace("-", "", trim($txt));
   
    if(!empty($txt) && strlen($txt) == 10){
    	$cleaned = preg_replace('/[^[:digit:]]/', '', $txt);
	    preg_match('/(\d{3})(\d{3})(\d{4})/', $cleaned, $matches);
	    return "({$matches[1]}) {$matches[2]}-{$matches[3]}";
    }else{
    	return $txt;
    }
    
}

function generateRandomStringContest($length = 10)
{
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function is_ajax()
{
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest");
}


function is_valid_url($url)
{
    return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

function load_img($filename, $params = array(), $tag = true)
{
    if($tag == true)
    {
        $params_str = "";
        foreach($params as $key=>$param)
        {
            $params_str .= $key.'="'.$param.'" ';
        }
        return '<img src="'.base_url()."resources/images/".$filename.'" '.$params_str.'>';
    }
    else
        return base_url()."resources/images/".$filename;
}

////function to load he external js files not pertaining to controllers and action
//file name would be relative to js folder
function load_js($js_file=NULL,$tag=true)
{
	if($js_file!=NULL){
        if(file_exists("./resources/js/".$js_file))
            return '<script type="text/javascript" src="'.base_url()."resources/js/".$js_file.'"></script>';
	}
}

function load_css($css_file=NULL)
{
	if($css_file!=NULL){
        if(file_exists("./resources/css/".$css_file))
            return '<link  type="text/css" rel="stylesheet" href="'.base_url()."resources/css/".$css_file.'" />';
	}
}

function getIndex(&$arrayName,$index)
{
	if(isset($arrayName[$index]))
		return $arrayName[$index];
	else
		return FALSE;
}

function formatDate($dt)
{
	$tstamp = strtotime($dt);
	return date("D jS M, Y ",$tstamp);
}

function formatTime($tstamp)
{
	return date("g:i a",$tstamp);
}

function ago($time)
{    
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $difference     = $now - $time;
    $tense         = "ago";

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
        $periods[$j].= "s";
    }

    return "$difference $periods[$j]";
}

function formatDateTime($tstamp)
{    
	return date("D jS M, Y (g:i a)",$tstamp);
}

function get_listing_count($userid)
{
    $CI =& get_instance();
    
    $CI->load->model('Item_model');
    
    $data = $CI->Item_model->get_listing_count($userid);
    return $data;
}

function lq()
{
	// helper to show last query
	$CI =& get_instance();
	echo "<pre class='smalltext'>".htmlentities($CI->db->last_query())."</pre>";
}

function getCountryList($index=null)
{
	$countries = array(
		'AF'=>'Afghanistan',
		'AL'=>'Albania',
		'DZ'=>'Algeria',
		'AS'=>'American Samoa',
		'AD'=>'Andorra',
		'AO'=>'Angola',
		'AI'=>'Anguilla',
		'AQ'=>'Antarctica',
		'AG'=>'Antigua and Barbuda',
		'AR'=>'Argentina',
		'AM'=>'Armenia',
		'AW'=>'Aruba',
		'AU'=>'Australia',
		'AT'=>'Austria',
		'AZ'=>'Azerbaijan',
		'BS'=>'Bahamas',
		'BH'=>'Bahrain',
		'BD'=>'Bangladesh',
		'BB'=>'Barbados',
		'BY'=>'Belarus',
		'BE'=>'Belgium',
		'BZ'=>'Belize',
		'BJ'=>'Benin',
		'BM'=>'Bermuda',
		'BT'=>'Bhutan',
		'BO'=>'Bolivia',
		'BA'=>'Bosnia and Herzegovina',
		'BW'=>'Botswana',
		'BV'=>'Bouvet Island',
		'BR'=>'Brazil',
		'IO'=>'British Indian Ocean Territory',
		'BN'=>'Brunei Darussalam',
		'BG'=>'Bulgaria',
		'BF'=>'Burkina Faso',
		'BI'=>'Burundi',
		'KH'=>'Cambodia',
		'CM'=>'Cameroon',
		'CA'=>'Canada',
		'CV'=>'Cape Verde',
		'KY'=>'Cayman Islands',
		'CF'=>'Central African Republic',
		'TD'=>'Chad',
		'CL'=>'Chile',
		'CN'=>'China',
		'CX'=>'Christmas Island',
		'CC'=>'Cocos (Keeling) Islands',
		'CO'=>'Colombia',
		'KM'=>'Comoros',
		'CG'=>'Congo',
		'CD'=>'Congo, the Democratic Republic of the',
		'CK'=>'Cook Islands',
		'CR'=>'Costa Rica',
		'CI'=>'Cote D\'Ivoire',
		'HR'=>'Croatia',
		'CU'=>'Cuba',
		'CY'=>'Cyprus',
		'CZ'=>'Czech Republic',
		'DK'=>'Denmark',
		'DJ'=>'Djibouti',
		'DM'=>'Dominica',
		'DO'=>'Dominican Republic',
		'EC'=>'Ecuador',
		'EG'=>'Egypt',
		'SV'=>'El Salvador',
		'GQ'=>'Equatorial Guinea',
		'ER'=>'Eritrea',
		'EE'=>'Estonia',
		'ET'=>'Ethiopia',
		'FK'=>'Falkland Islands (Malvinas)',
		'FO'=>'Faroe Islands',
		'FJ'=>'Fiji',
		'FI'=>'Finland',
		'FR'=>'France',
		'GF'=>'French Guiana',
		'PF'=>'French Polynesia',
		'TF'=>'French Southern Territories',
		'GA'=>'Gabon',
		'GM'=>'Gambia',
		'GE'=>'Georgia',
		'DE'=>'Germany',
		'GH'=>'Ghana',
		'GI'=>'Gibraltar',
		'GR'=>'Greece',
		'GL'=>'Greenland',
		'GD'=>'Grenada',
		'GP'=>'Guadeloupe',
		'GU'=>'Guam',
		'GT'=>'Guatemala',
		'GN'=>'Guinea',
		'GW'=>'Guinea-Bissau',
		'GY'=>'Guyana',
		'HT'=>'Haiti',
		'HM'=>'Heard Island and Mcdonald Islands',
		'VA'=>'Holy See (Vatican City State)',
		'HN'=>'Honduras',
		'HK'=>'Hong Kong',
		'HU'=>'Hungary',
		'IS'=>'Iceland',
		'IN'=>'India',
		'ID'=>'Indonesia',
		'IR'=>'Iran, Islamic Republic of',
		'IQ'=>'Iraq',
		'IE'=>'Ireland',
		'IL'=>'Israel',
		'IT'=>'Italy',
		'JM'=>'Jamaica',
		'JP'=>'Japan',
		'JO'=>'Jordan',
		'KZ'=>'Kazakhstan',
		'KE'=>'Kenya',
		'KI'=>'Kiribati',
		'KP'=>'Korea, Democratic People\'s Republic of',
		'KR'=>'Korea, Republic of',
		'KW'=>'Kuwait',
		'KG'=>'Kyrgyzstan',
		'LA'=>'Lao People\'s Democratic Republic',
		'LV'=>'Latvia',
		'LB'=>'Lebanon',
		'LS'=>'Lesotho',
		'LR'=>'Liberia',
		'LY'=>'Libyan Arab Jamahiriya',
		'LI'=>'Liechtenstein',
		'LT'=>'Lithuania',
		'LU'=>'Luxembourg',
		'MO'=>'Macao',
		'MK'=>'Macedonia, the Former Yugoslav Republic of',
		'MG'=>'Madagascar',
		'MW'=>'Malawi',
		'MY'=>'Malaysia',
		'MV'=>'Maldives',
		'ML'=>'Mali',
		'MT'=>'Malta',
		'MH'=>'Marshall Islands',
		'MQ'=>'Martinique',
		'MR'=>'Mauritania',
		'MU'=>'Mauritius',
		'YT'=>'Mayotte',
		'MX'=>'Mexico',
		'FM'=>'Micronesia, Federated States of',
		'MD'=>'Moldova, Republic of',
		'MC'=>'Monaco',
		'MN'=>'Mongolia',
		'MS'=>'Montserrat',
		'MA'=>'Morocco',
		'MZ'=>'Mozambique',
		'MM'=>'Myanmar',
		'NA'=>'Namibia',
		'NR'=>'Nauru',
		'NP'=>'Nepal',
		'AN'=>'Netherlands Antilles',
		'NL'=>'Netherlands',
		'NC'=>'New Caledonia',
		'NZ'=>'New Zealand',
		'NI'=>'Nicaragua',
		'NE'=>'Niger',
		'NG'=>'Nigeria',
		'NU'=>'Niue',
		'NF'=>'Norfolk Island',
		'MP'=>'Northern Mariana Islands',
		'NO'=>'Norway',
		'OM'=>'Oman',
		'PK'=>'Pakistan',
		'PW'=>'Palau',
		'PS'=>'Palestinian Territory, Occupied',
		'PA'=>'Panama',
		'PG'=>'Papua New Guinea',
		'PY'=>'Paraguay',
		'PE'=>'Peru',
		'PH'=>'Philippines',
		'PN'=>'Pitcairn',
		'PL'=>'Poland',
		'PT'=>'Portugal',
		'PR'=>'Puerto Rico',
		'QA'=>'Qatar',
		'RE'=>'Reunion',
		'RO'=>'Romania',
		'RU'=>'Russian Federation',
		'RW'=>'Rwanda',
		'SH'=>'Saint Helena',
		'LC'=>'Saint Lucia',
		'PM'=>'Saint Pierre and Miquelon',
		'KN'=>'Saint Kitts and Nevis',
		'VC'=>'Saint Vincent and the Grenadines',
		'WS'=>'Samoa',
		'SM'=>'San Marino',
		'ST'=>'Sao Tome and Principe',
		'SA'=>'Saudi Arabia',
		'SN'=>'Senegal',
		'CS'=>'Serbia and Montenegro',
		'SC'=>'Seychelles',
		'SL'=>'Sierra Leone',
		'SG'=>'Singapore',
		'SK'=>'Slovakia',
		'SI'=>'Slovenia',
		'SB'=>'Solomon Islands',
		'SO'=>'Somalia',
		'ZA'=>'South Africa',
		'GS'=>'South Georgia and the South Sandwich Islands',
		'ES'=>'Spain',
		'LK'=>'Sri Lanka',
		'SD'=>'Sudan',
		'SR'=>'Suriname',
		'SJ'=>'Svalbard and Jan Mayen',
		'SZ'=>'Swaziland',
		'SE'=>'Sweden',
		'CH'=>'Switzerland',
		'SY'=>'Syrian Arab Republic',
		'TW'=>'Taiwan, Province of China',
		'TJ'=>'Tajikistan',
		'TZ'=>'Tanzania, United Republic of',
		'TH'=>'Thailand',
		'TL'=>'Timor-Leste',
		'TG'=>'Togo',
		'TK'=>'Tokelau',
		'TO'=>'Tonga',
		'TT'=>'Trinidad and Tobago',
		'TN'=>'Tunisia',
		'TR'=>'Turkey',
		'TM'=>'Turkmenistan',
		'TC'=>'Turks and Caicos Islands',
		'TV'=>'Tuvalu',
		'UG'=>'Uganda',
		'UA'=>'Ukraine',
		'GB'=>'United Kingdom',
		'US'=>'United States',
		'AE'=>'United Arab Emirates',
		'UM'=>'United States Minor Outlying Islands',
		'UY'=>'Uruguay',
		'UZ'=>'Uzbekistan',
		'VU'=>'Vanuatu',
		'VE'=>'Venezuela',
		'VN'=>'Viet Nam',
		'VG'=>'Virgin Islands, British',
		'VI'=>'Virgin Islands, U.s.',
		'WF'=>'Wallis and Futuna',
		'EH'=>'Western Sahara',
		'YE'=>'Yemen',
		'ZM'=>'Zambia',
		'ZW'=>'Zimbabwe'
	);

	if(is_null($index))
		return $countries;
	else
	{
		if(strlen($index)>2)
		{
			$key = array_search($index,$countries);
			return $key;
		}
		else
		{
			if(isset($countries[$index]))
				return $countries[$index];
			else
				return "";
		}

	}
}

function getCurrentDateTime()
{
	return date("Y-m-d G:i:s");
}

function trim_text($string, $limit = 10, $dots = TRUE)
{    
    //echo $dots;
    $stringlen = strlen($string);
    

    if($stringlen > $limit)
    {
        if($dots)
        {
            $temp_string = substr($string, 0, ($limit - 3));
            $new_string = $temp_string."...";
            echo $new_string;
        }
        else
        {
            $temp_string = substr($string, 0, $limit);
            $new_string = $temp_string;
            echo $new_string;
        }
    }
    else
    {
        echo $string;
    }

}

function recursive_remove_directory($directory, $empty=FALSE)
{
	// if the path has a slash at the end we remove it here
	if(substr($directory,-1) == '/')
	{
		$directory = substr($directory,0,-1);
	}

	// if the path is not valid or is not a directory ...
	if(!file_exists($directory) || !is_dir($directory))
	{
		// ... we return false and exit the function
		return FALSE;

	// ... if the path is not readable
	}elseif(!is_readable($directory))
	{
		// ... we return false and exit the function
		return FALSE;

	// ... else if the path is readable
	}else{

		// we open the directory
		$handle = opendir($directory);

		// and scan through the items inside
		while (FALSE !== ($item = readdir($handle)))
		{
			// if the filepointer is not the current directory
			// or the parent directory
			if($item != '.' && $item != '..')
			{
				// we build the new path to delete
				$path = $directory.'/'.$item;

				// if the new path is a directory
				if(is_dir($path)) 
				{
					// we call this function with the new path
					recursive_remove_directory($path);

				// if the new path is a file
				}else{
					// we remove the file
					unlink($path);
				}
			}
		}
		// close the directory
		closedir($handle);

		// if the option to empty is not set to true
		if($empty == FALSE)
		{
			// try to delete the now empty directory
			if(!rmdir($directory))
			{
				// return false if not possible
				return FALSE;
			}
		}
		// return success
		return TRUE;
	}
}

function getOriginal($url_title) {
    
    $title_parts =  explode("-",$url_title);
    return implode(" ",$title_parts);
}

function normalize_str($str)
{
$invalid = array('á'=>'a', 'Á'=>'a', 'č'=>'c', 'Č'=>'c', 'ď'=>'d', 'Ď'=>'d',
			'é'=>'e','ě'=>'e','É'=>'e','Ě'=>'e','í'=>"i",'Í'=>'i','ň'=>'n','Ň'=>'n',
			'ó'=>'o','Ó'=>'o','ř'=>'r','Ř'=>'r','š'=>'s','Š'=>'s','ť'=>'t','Ť'=>'t','ú'=>'u',
			'Ú'=>'u','ů'=>'u','Ů'=>'u','ž'=>'z','Ž'=>'z'
			);

 
$str = str_replace(array_keys($invalid), array_values($invalid), $str);
 
return $str;
}
function is_allowed()
{
	return TRUE;
}


function email($to, $mail_details,$shop_id)
{
	
	// pr($shop_id);exit;
	
	$CI =& get_instance();
	$CI->load->library('email');
	//$CI->load->library('database');
    
	
	// $return = array();
	// $result=$CI->db->get_where('options')->result_array();

	
	//email config based on user type
	// if($CI->router->class == 'Superadmin')
 //    {
        //if it is super admin panel
        $CI->db2 = $CI->load->database('main', TRUE);
        $result = $CI->db2->get_where('options')->result_array();
    // }
    // else
    // { 
    //     //other than superadmin panel
    //     $CI->load->database();
    //     $result = $CI->db->get_where('options')->result_array();
        
    // }

    if(!empty($result)) {
		foreach ($result as $key => $value) {
			$return[$value['name']] = $value['value'];
		}
	}


	$config = array();
	$config['smtp_host'] = $return['smtp_host'];
	$config['smtp_port'] = $return['smtp_port'];
	$config['smtp_timeout'] = '30';
	$config['smtp_user'] = $return['smtp_username'];
	$config['smtp_pass'] = $return['smtp_password'];

	$config['mailtype'] = 'html';

	$config['protocol'] = "smtp";
	$config['charset'] = 'iso-8859-1';
	//$config['charset'] = "utf-8";
	$config['mailtype'] = "html";
	$config['newline'] = "\r\n";

	$CI->email->initialize($config);
	$CI->email->set_newline("\r\n");
	$CI->email->set_crlf( "\r\n" );

    $CI->email->from($return['smtp_username']);
    $CI->email->to($to); 
    $CI->email->bcc('blackhole-emails@griffinapps.com');
    $CI->email->subject($mail_details['subject']);
    $CI->email->message($mail_details['message']); 
    //$CI->email->send(FALSE);
    //echo $CI->email->print_debugger();exit;
    // return $CI->email->send();

    $CI->email->send();
	echo $CI->email->print_debugger();echo "asdasdfs"; exit;

  if($shop_id > 0){
    $result = $CI->db2->get_where('shops',array('id' => $shop_id))->row_array();

    $email_count = $result['email_count']+1;

    $CI->db2->update('shops',array('email_count'=>$email_count),array('id'=>$shop_id));
  }

    return true;

    
}

// function email($to, $from='', $mail_details)
// {
// 	$CI =& get_instance();
// 	$CI->load->library('email');
    
// 	$config['mailtype'] = 'html';

// 	$config['protocol'] = "smtp";
// 	$config['smtp_host'] = SMTP_HOST;
// 	$config['smtp_port'] = SMTP_PORT;
// 	$config['smtp_user'] = SMTP_USER; 
// 	$config['smtp_pass'] = SMTP_PASSWORD;
// 	$config['charset'] = "utf-8";
// 	$config['mailtype'] = "html";
// 	//$config['smtp_crypto'] = "tls"; 

// 	$config['newline'] = "\r\n";

// 	$CI->email->initialize($config);
// 	$CI->email->set_newline("\r\n");
// 	$CI->email->set_crlf( "\r\n" );

//     $CI->email->from(EMAIL_FROM);
//     $CI->email->to($to); 
    
//     $CI->email->subject($mail_details['subject']);
//     $CI->email->message($mail_details['message']); 

//    	$CI->email->send();
//     // $CI->email->send(FALSE);
//     echo $CI->email->print_debugger();echo "asdasdfs";
//     exit;
// }


function addhttp($url,$secure='') {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http{$secure}://" . $url;
    }
    return $url;
}

function array_count_values_of($value, $array) {
	$counts = array_count_values($array);
	if(isset($counts[$value]))
		return $counts[$value];
	else
		return 0;
}


function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}


function simpleNumericRandomString($length = 6)
{
	$characters = '0123456789';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function recurse_copy($src,$dst) { 
    if(file_exists($src))
	{
		$dir = opendir($src); 
		@mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				if ( is_dir($src . '/' . $file) ) { 
					recurse_copy($src . '/' . $file,$dst . '/' . $file); 
				} 
				else { 
					copy($src . '/' . $file,$dst . '/' . $file); 
				} 
			} 
		} 
		closedir($dir); 
	}
} 

function parse_read_template($content){
  return preg_replace_callback('/\<\?php echo \$([a-zA-Z\d_]+)\; ?\?\>/', function ($matches){
	if(count($matches) >= 2 ){
      return '{' . $matches[1] . '}';
    }
  }, $content);
 }
 
 function _ToCamelCase($string, $capitalizeFirstCharacter = false) 
{

    $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

    if (!$capitalizeFirstCharacter) {
        $str[0] = strtolower($str[0]);
    }

    return $str;
}

function fsmodify($obj) {
   $chunks = explode('/', $obj);
   chmod($obj, is_dir($obj) ? 0755 : 0644);
   chown($obj, $chunks[2]);
   chgrp($obj, $chunks[2]);
}


function fsmodifyr($dir) 
{
   if($objs = glob($dir."/*")) {        
	   foreach($objs as $obj) {
		   fsmodify($obj);
		   if(is_dir($obj)) fsmodifyr($obj);
	   }
   }

   return fsmodify($dir);
}

function getIconsList(){
	return array(
			'clip-settings' => 'clip-settings',
			'clip-camera' => 'clip-camera',
			'clip-tag' => 'clip-tag',
			'clip-bulb' => 'clip-bulb',
			'clip-paperplane' => 'clip-paperplane',
			'clip-bubble' => 'clip-bubble',
			'clip-banknote' => 'clip-banknote',
			'clip-music' => 'clip-music',
			'clip-date' => 'clip-date',
			'clip-shirt' => 'clip-shirt',
			'clip-clip' => 'clip-clip',
			'clip-calendar' => 'clip-calendar',
			'clip-vynil' => 'clip-vynil',
			'clip-truck' => 'clip-truck',
			'clip-note' => 'clip-note',
			'clip-world' => 'clip-world',
			'clip-key' => 'clip-key',
			'clip-pencil' => 'clip-pencil',
			'clip-images' => 'clip-images',
			'clip-list' => 'clip-list',
			'clip-earth' => 'clip-earth',
			'clip-pictures' => 'clip-pictures',
			'clip-cog' => 'clip-cog',
			'clip-home' => 'clip-home',
			'clip-home-2' => 'clip-home-2',
			'clip-pencil-3' => 'clip-pencil-3',
			'clip-images-3' => 'clip-images-3',
			'clip-eyedropper' => 'clip-eyedropper',
			'clip-droplet' => 'clip-droplet',
			'clip-droplet-2' => 'clip-droplet-2',
			'clip-image' => 'clip-image',
			'clip-music-2' => 'clip-music-2',
			'clip-camera-2' => 'clip-camera-2',
			'clip-camera-3' => 'clip-camera-3',
			'clip-headphones' => 'clip-headphones',
			'clip-headphones-2' => 'clip-headphones-2',
			'clip-gamepad' => 'clip-gamepad',
			'clip-connection' => 'clip-connection',
			'clip-headphones-2' => 'clip-headphones-2',
			'clip-new' => 'clip-new',
			'clip-book' => 'clip-book',
			'clip-file' => 'clip-file',
			'clip-file-2' => 'clip-file-2',
			'clip-file-plus' => 'clip-file-plus',
			'clip-file-minus' => 'clip-file-minus',
			'clip-file-check' => 'clip-file-check',
			'clip-file-remove' => 'clip-file-remove',
			'clip-file-3' => 'clip-file-3'
		);
}

function parse_write_template($content, $data){
	return preg_replace_callback('/{([a-zA-Z\d_]+)}/', function ($matches) use ($data){
    if(count($matches) >= 2 ){
    	$str = $data[$matches[1]];
    	return $str;
    }
    }, $content);
}


function jvzipnVerification() {
    $secretKey = JVZOO_SECRET;
    $pop = "";
    $ipnFields = array();
    foreach ($_POST AS $key => $value) {
        if ($key == "cverify") {
            continue;
        }
        $ipnFields[] = $key;
    }
    sort($ipnFields);
    foreach ($ipnFields as $field) {
        // if Magic Quotes are enabled $_POST[$field] will need to be
        // un-escaped before being appended to $pop
        $pop = $pop . $_POST[$field] . "|";
    }
    $pop = $pop . $secretKey;
    $calcedVerify = sha1(mb_convert_encoding($pop, "UTF-8"));
    $calcedVerify = strtoupper(substr($calcedVerify,0,8));
    return $calcedVerify == $_POST["cverify"];
}

function get_lat_long($address){


    $address = str_replace(" ", "+", $address);
    //print_r('https://maps.google.com/maps/api/geocode/json?address=&sensor=false&key=AIzaSyALkoaMWFanUs8JqVIk0lHKIsRt-1MiWS4');
    // $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false&key=".GmapKey);
    // $json = json_decode($json,true);
    $url = "https://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false&key=".GmapKey;
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);
	$json = json_decode($response,true);
    //pr($json);exit;
    // pr($json);
    // //$status=$json->status;
    // // echo $status;
    // // exit;
    if(empty($json['results']))
    {
    	//exit;
    	return FALSE;
    }
    else
    {	
	    $lat = $json['results'][0]['geometry']['location']['lat'];
	    $long = $json['results'][0]['geometry']['location']['lng'];
	    $returnArray['place_id'] = $json['results'][0]['place_id'];
	    $returnArray['lat']= $lat;
	    $returnArray['long']= $long;
	    //pr($returnArray);exit;
	    return $returnArray;
	}    
}

function getUrlForWeather($placeId)
{
	$ch = curl_init();  	 	
    curl_setopt($ch, CURLOPT_URL,'https://forecast7.com/api/getUrl/'.$placeId);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HEADER, false); 
    // curl_setopt($ch, CURLOPT_POST, count($postData));
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
    $output=curl_exec($ch);
    // pr($output);
    // exit;
    return $output;
    curl_close($ch);
}





function subscribeEmail($type='',$name='',$email='',$url='',$form_details=array()){
	$postData = '';
	switch ($type) {
	    case "iContact":
	    	$serializedArray = unserialize($form_details);
	    	$serializedArray['fields_email'] = $email;
	    	$serializedArray['fields_fname'] = $name;
	    	$serializedArray['fields_lname'] = '';
			foreach($serializedArray as $k => $v) 
			{ 
				$postData .= $k . '='.urlencode($v).'&'; 
			}
			rtrim($postData, '&');
			$ch = curl_init();  
		 	
		    curl_setopt($ch, CURLOPT_URL,$url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
		 
		    $output=curl_exec($ch);
		    curl_close($ch);
	        break;
	    case "Mail Chimp":
	    	$serializedArray = unserialize($form_details);
	    	$serializedArray['EMAIL'] = $email;
	    	$serializedArray['FNAME'] = $name;
	    	$serializedArray['LNAME'] = '';
			foreach($serializedArray as $k => $v) 
			{ 
				$postData .= $k . '='.urlencode($v).'&'; 
			}
			rtrim($postData, '&');
			$ch = curl_init();  
		 	
		    curl_setopt($ch, CURLOPT_URL,$url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
		 
		    $output=curl_exec($ch);
		    curl_close($ch);
	        break;
	    case "Constant Contacts":
	    	$serializedArray = unserialize($form_details);
	    	$serializedArray['email'] = $email;
	    	$serializedArray['first_name'] = $name;
			foreach($serializedArray as $k => $v) 
			{ 
				$postData .= $k . '='.urlencode($v).'&'; 
			}
			rtrim($postData, '&');
			$ch = curl_init();  
		 	
		    curl_setopt($ch, CURLOPT_URL,$url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
		 
		    $output=curl_exec($ch);
		    curl_close($ch);
	        break;
	    case "Get Response":
	    	$serializedArray = unserialize($form_details);
	    	$serializedArray['email'] = $email;
	    	$serializedArray['name'] = $name;
			foreach($serializedArray as $k => $v) 
			{ 
				$postData .= $k . '='.urlencode($v).'&'; 
			}
			rtrim($postData, '&');
			$ch = curl_init();  
		 	
		    curl_setopt($ch, CURLOPT_URL,$url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
		 
		    $output=curl_exec($ch);
		    curl_close($ch);
	        break;
	    case "Get Response":
	    	$serializedArray = unserialize($form_details);
	    	$serializedArray['email'] = $email;
	    	$serializedArray['name'] = $name;
			foreach($serializedArray as $k => $v) 
			{ 
				$postData .= $k . '='.urlencode($v).'&'; 
			}
			rtrim($postData, '&');
			$ch = curl_init();  
		 	
		    curl_setopt($ch, CURLOPT_URL,$url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    curl_setopt($ch, CURLOPT_HEADER, false); 
		    curl_setopt($ch, CURLOPT_POST, count($postData));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
		 
		    $output=curl_exec($ch);
		    curl_close($ch);
	        break;
	    case "Aweber":
	    	$serializedArray = unserialize($form_details);
	    	$account = $aweber->getAccount($serializedArray['accessToken'],$serializedArray['accessSecret']);
			$listURL = "/accounts/".$account['data']['id']."/lists/".ltrim('awlist',$serializedArray['listname']);
			$list = $account->loadFromUrl($listURL);
			$params = array(
				'email' => $email,
				'ip_address' => '127.0.0.1',
				'ad_tracking' => '',
				'misc_notes' => '',
				'name' => $name
			);
			$subscribers = $list->subscribers;
			$new_subscriber = $subscribers->create($params);
	        break;
	    default:
	        echo "Wrong method";
	}
}

function getAllData($tableName = '',$conditions=array(),$joins=array(),$fields=''){
	$CI =& get_instance();
	if($tableName == '')
		return array();
	if($fields != '')
		$CI->db->select($fields);
	if(!empty($joins)){
		foreach ($joins as $key => $join) {
			$CI->db->join($key,$join);	
		}
	}
	return $CI->db->get_where($tableName,$conditions)->result_array();
}

function getSingleData($tableName = '',$conditions = array()){
	$CI =& get_instance();
	if($tableName == '')
		return array();

	return $CI->db->get_where($tableName,$conditions)->row_array();
}

function getSingleDataJoin($tableName = '',$conditions = array(),$joins=array(),$fields='',$order_by=array()){
	$CI =& get_instance();
	if($tableName == '')
		return array();

	if($fields != '')
		$CI->db->select($fields);
	if(!empty($joins)){
		foreach ($joins as $key => $join) {
			$CI->db->join($key,$join);	
		}
	}
	if(!empty($order_by)){
		foreach ($order_by as $key => $order) {
			$CI->db->order_by($key,$order);	
		}
	}

	return $CI->db->get_where($tableName,$conditions)->row_array();
}

function singleInsert($tableName='',$data=array()){
	$CI =& get_instance();
	if($tableName == '')
		return false;
	if(empty($data))
		return false;

	$CI->db->insert($tableName,$data);
	return $CI->db->insert_id();
}

function singleDelete($tableName='',$conditions=array()){
	$CI =& get_instance();
	if($tableName == '')
		return false;
	if(empty($conditions))
		return false;
	
	return $CI->db->delete($tableName,$conditions);
}

function singleEdit($tableName='',$data=array(),$conditions=array()){
	$CI =& get_instance();
	if($tableName == '')
		return false;
	if(empty($conditions))
		return false;
	if(empty($data))
		return false;
	
	return $CI->db->update($tableName,$data,$conditions);
}

function multipleDelete($multipleConditions=array()){
	$CI =& get_instance();
	if(empty($multipleConditions))
		return false;
	foreach ($multipleConditions as $key => $condn) {
		$CI->db->delete($key,$condn);
	}
	return true;
}

function imageUpload($files,$formField='',$uploadTo = '',$imageName='',$allowExtensions=array()){
	if($files[$formField]['name'] != '' && $formField!='')
	{
	  $imageExtesion = pathinfo($files[$formField]['name'], PATHINFO_EXTENSION);
	  if(in_array($imageExtesion, $allowExtensions)){
		if(move_uploaded_file($files[$formField]["tmp_name"], $uploadTo.$imageName.'.'.$imageExtesion))
		   return $imageName.'.'.$imageExtesion;
		}
		else
			return false;
	}
	return false;
}

function pinpaymentcurlrequest($request,$post)
{
	/*$test = false;
	$auth['u'] = 'p6lFpogno4ZUg8y_VNjpNA'; //Private Key from PIN
	$auth['p'] = ''; //API calls use empty password*/

	//testing keys

	$test = true;
	$auth['u'] = 'pTHDwjo9hHA8v97Ea05eyQ'; //Private Key from PIN
	$auth['p'] = ''; //API calls use empty password
	
	switch ($request) {
    case "charges":
    //charge url
        $url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/charges';
        break;
    case "customers":
    //Customer Url
       	$url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/customers';
        break;
    case "recipients":
    //recipients url
        $url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/recipients';
        break;
    case "transfers":
    //transfer Url
        $url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/transfers';
        break;
    default:
        $url ="";

    }
        
    if($url != "")
    {    
        $ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
		curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

		// Execute
		$response = curl_exec($ch);

		// Check if any error occurred
		if(!curl_errno($ch)){$info = curl_getinfo($ch);}

		// Close handle
		curl_close($ch);
		$response = json_decode($response, true);

		return $response;
	}
	else
	{
		return false;
	}
}	

function pinpaymentchargerefund($chargeToken)
{
	if(!empty($chargeToken))
	{	
		//echo "here";

		/*$test = false;
		$array=array('amount'=> 68);
		$auth['u'] = 'p6lFpogno4ZUg8y_VNjpNA'; //Private Key from PIN
		$auth['p'] = '';
		$url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/charges/'.$chargeToken.'/refunds';
		$ch = curl_init($url);*/

		//testing keys
		$test = true;
		$array=array('amount'=> 68);
		$auth['u'] = 'pTHDwjo9hHA8v97Ea05eyQ'; //Private Key from PIN
		$auth['p'] = '';
		$url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/charges/'.$chargeToken.'/refunds';
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
		curl_setopt($ch, CURLOPT_POST,1); //tell it we are posting
		curl_setopt($ch, CURLOPT_POSTFIELDS, $array); //tell it what to post
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

		// Execute
		$response = curl_exec($ch);

		// Check if any error occurred
		if(!curl_errno($ch)){$info = curl_getinfo($ch);}

		// Close handle
		curl_close($ch);
		$response = json_decode($response, true);

		//echo "refund";
		//exit();
		return $response;
	}
	else
		return false;	
}

function pinpaymentBalanceAvailable()
{
	/*$test = false;
	$auth['u'] = 'p6lFpogno4ZUg8y_VNjpNA'; //Private Key from PIN
	$auth['p'] = ''; //API calls use empty password*/

	//testing keys

	$test = true;
	$auth['u'] = 'pTHDwjo9hHA8v97Ea05eyQ'; //Private Key from PIN
	$auth['p'] = ''; //API calls use empty password

	$url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/balance';
	$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
		//curl_setopt($ch, CURLOPT_POST,1); //tell it we are posting
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $array); //tell it what to post
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

		// Execute
		$response = curl_exec($ch);

		// Check if any error occurred
		if(!curl_errno($ch)){$info = curl_getinfo($ch);}

		// Close handle
		curl_close($ch);
		$response = json_decode($response, true);

		//echo "refund";
		//exit();
		return $response;
}
function getAmountAfterCommision($price)
{
	return ($price+($price*SITE_COMMISSION));
}

function getAmountAfterAddingTax($price)
{
	return $price;
}

function getMaxCancelDate($date)
{
	$slot_date = new DateTime($date);
    $slot_date->modify('+1 day');
    return $slot_date->format('Y-m-d H:i:s');
}

function getStripeBalance()
{
	include_once(FCPATH."application/libraries/stripe/init.php");
		//date_default_timezone_set('Australia/Melbourne');
	\Stripe\Stripe::setApiKey("sk_test_OrnYB4VLUUSFxNCUKzB3YjVo");
	$cu = \Stripe\Balance::retrieve();
	$chargeArray = $cu->__toArray(true);
	return $chargeArray['available'][0]['amount'];
}

function getUTCDate()
{
	echo "date";
}

function gettingUrlId(){
	$request_url =  $_SERVER['REQUEST_URI'];
	$root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

	$cid_url = str_replace($root, "", $request_url);
	preg_match("/^[0-9]+/", $cid_url, $p);
	if(is_array($p) && isset($p[0])){
		$cid = $p[0] ;
	} else {
		$cid = '';
	}
	return $cid;
}

function arrayToObject($array) {
    if (!is_array($array)) {
        return $array;
    }
    
    $object = new stdClass();
    if (is_array($array) && count($array) > 0) {
        foreach ($array as $name=>$value) {
            $name = trim($name);
            if (!empty($name)) {
                $object->$name = arrayToObject($value);
            }
        }
        return $object;
    }
    else {
        return FALSE;
    }
}


function merge_by_id(array $arr1, array $arr2) {

    // $result = [];
    //pr($arr1);pr($arr2);
    // if(!empty($arr1) && empty($arr2))
    // 	return  $arr1;

    // if(empty($arr1) && !empty($arr2))
    // 	return  $arr2;


    // foreach ($arr1 as $value) {

    //     $key = array_search($value['id'], array_column($arr2, 'id'));
    //   	//echo $key;
    //     if($key !== false && $key > 0) {
    //     	if(array_key_exists($key, $arr2)  && isset($arr2[$key])){
    //         	$result[] = array_merge($value, $arr2[$key]);
    //         }	
    //         unset($arr2[$key]);
    //     } else {
    //         $result[] = $value;
    //     }
    // }
    // //exit;
    // $result = array_merge($result, $arr2);
     //return $result;

   	$data = array();
    $arrayAB = array_merge($arr1,$arr2);
    foreach ($arrayAB as $value) {
      $id = $value['id'];
      if (!isset($data[$id])) {
        $data[$id] = array();
      }
      $data[$id] = array_merge($data[$id],$value);
    }
    return $data;

   
}


function statusDisplay($status)
{
	
	if($status == 'Confirm')
		return 'Confirmed';

	if($status == 'Cancel')
		return 'Cancelled';

	if($status == 'Complete')
		return 'Completed';

	if($status == 'Inprocess')
		return 'Checked In';

	if($status == 'Checkout')
		return 'Checked Out';

	return $status;

}

function paymentStatusDisplay($paymentStatus)
{
	if($paymentStatus == 'Offline' || $paymentStatus == 'offline')
		return 'In-house';

	return $paymentStatus;
}

function dateformat_PHP_to_jQueryUI($php_format)
{
    $SYMBOLS_MATCHING = array(
        // Day
        'd' => 'dd',
        'D' => 'D',
        'j' => 'd',
        'l' => 'DD',
        'N' => '',
        'S' => '',
        'w' => '',
        'z' => 'o',
        // Week
        'W' => '',
        // Month
        'F' => 'MM',
        'm' => 'mm',
        'M' => 'M',
        'n' => 'm',
        't' => '',
        // Year
        'L' => '',
        'o' => '',
        'Y' => 'yyyy',
        'y' => 'yy',
        // Time
        'a' => '',
        'A' => '',
        'B' => '',
        'g' => '',
        'G' => '',
        'h' => '',
        'H' => '',
        'i' => '',
        's' => '',
        'u' => ''
    );
    $jqueryui_format = "";
    $escaping = false;
    for($i = 0; $i < strlen($php_format); $i++)
    {
        $char = $php_format[$i];
        if($char === '\\') // PHP date format escaping character
        {
            $i++;
            if($escaping) $jqueryui_format .= $php_format[$i];
            else $jqueryui_format .= '\'' . $php_format[$i];
            $escaping = true;
        }
        else
        {
            if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
            if(isset($SYMBOLS_MATCHING[$char]))
                $jqueryui_format .= $SYMBOLS_MATCHING[$char];
            else
                $jqueryui_format .= $char;
        }
    }
    return $jqueryui_format;
}


function momentFormat($date)
{

	if($date == 'd M Y')
	{
		return 'DD MMM YYYY';
	}

	if($date == 'd/m/Y')
	{
		return 'DD/MM/YYYY';
	}

	if($date == 'd.m.Y')
	{
		return 'DD.MM.YYYY';
	}

	if($date == 'd-m-Y')
	{
		return 'DD-MM-YYYY';
	}

	if($date == 'm/d/Y')
	{
		return 'MM/DD/YYYY';
	}

	if($date == 'Y/m/d')
	{
		return 'YYYY/MM/DD';
	}

	if($date == 'Y-m-d')
	{
		return 'YYYY-MM-DD';
	}

	if($date == 'M d Y')
	{
		return 'MMM DD YYYY';
	}


}

function createDate($date)
{
	if(!empty($date)){
		$date = DateTime::createFromFormat(get_option('date_format'), $date);
		return $date->format('Y-m-d');
	}
	return '';	
}


function getSizePriority($size)
{
	// $array = array('size','medium','large','xl','xxl'); is it typo error in size or small?
	$array = array('small','medium','large','xl','xxl');
	return array_search($size, $array);
}


function roundToQuarterHour() {
	$time = date('H:i');
	$time = strtotime($time);
	
	$round = 15*60;
	$rounded = round($time / $round) * $round;
	$newtime = date("h:i A", $rounded);
	
	return $newtime;
}


function daycare_scheduleTimeConverter($temp) {
	$temp['start'] = user_date($temp['start'],'Y-m-d H:i:s');
	$temp['end'] = user_date($temp['end'],'Y-m-d H:i:s');
	return $temp;
}

function groomer_scheduleTimeConverter($temp) {
	if(!empty($temp['blocker_start'])){
		$startDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['blocker_start']));
		$startDate = user_date($startDate,'Y-m-d H:i:s');
		$endDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['blocker_end']));
		$endDate = user_date($endDate,'Y-m-d H:i:s');
		$temp['blocker_start'] = date('H:i:s', strtotime($startDate));
		$temp['blocker_end'] = date('H:i:s', strtotime($endDate));
	}
	
	if(!empty($temp['hard_blocker_end'])){
		$startDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['hard_blocker_start']));
		$startDate = user_date($startDate,'Y-m-d H:i:s');
		$endDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['hard_blocker_end']));
		$endDate = user_date($endDate,'Y-m-d H:i:s');
		$temp['hard_blocker_start'] = date('H:i:s', strtotime($startDate));
		$temp['hard_blocker_end'] = date('H:i:s', strtotime($endDate));
	}

	if(!empty($temp['lunch_time_start'])){
		$startDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['lunch_time_start']));
		$startDate = user_date($startDate,'Y-m-d H:i:s');
		$endDate = date('Y-m-d H:i:s', strtotime($temp['date'].' '.$temp['lunch_time_end']));
		$endDate = user_date($endDate,'Y-m-d H:i:s');
		$temp['lunch_time_start'] = date('H:i:s', strtotime($startDate));
		$temp['lunch_time_end'] = date('H:i:s', strtotime($endDate));
	}

	$temp['start'] = user_date($temp['start'],'Y-m-d H:i:s');
	$temp['end'] = user_date($temp['end'],'Y-m-d H:i:s');
	return $temp;
}

function groomingTimeConverter($temp) {
	$startDate = date('Y-m-d H:i:s', strtotime($temp['appointment_date'].' '.$temp['appointment_time']));
	$startDate = user_date($startDate,'Y-m-d H:i:s');
	$endDate = date('Y-m-d H:i:s', strtotime($temp['appointment_date'].' '.$temp['appointment_end_time']));
	$endDate = user_date($endDate,'Y-m-d H:i:s');
	$temp['appointment_date'] = date('Y-m-d', strtotime($startDate));
	$temp['appointment_time'] = date('H:i:s', strtotime($startDate));
	$temp['appointment_end_time'] = date('H:i:s', strtotime($endDate));
	$temp['appointment_date'] = date('Y-m-d', strtotime($startDate));

	if(!empty($temp['check_in_time'])){
		$temp['check_in_time'] = user_date($temp['check_in_time'],'Y-m-d H:i:s');
	}

	if(!empty($temp['check_out_time'])){
		$temp['check_out_time'] = user_date($temp['check_out_time'],'Y-m-d H:i:s');
	}

	return $temp;
}


function groomingServerTimeConverter($temp){
	//this is for convert system time into UTC time 
	$startDate = date('Y-m-d H:i:s', strtotime($temp['appointment_date'].' '.$temp['appointment_time']));
	$startDate = server_date($startDate,'Y-m-d H:i:s');
	$endDate = date('Y-m-d H:i:s', strtotime($temp['appointment_date'].' '.$temp['appointment_end_time']));
	$endDate = server_date($endDate,'Y-m-d H:i:s');
	$temp['appointment_date'] = date('Y-m-d', strtotime($startDate));
	$temp['appointment_time'] = date('H:i:s', strtotime($startDate));
	$temp['appointment_end_time'] = date('H:i:s', strtotime($endDate));
	return $temp;
}

function daycareBoardingTimeConverter($temp) {
	$startDate = date('Y-m-d H:i:s', strtotime($temp['start_date'].' '.$temp['start_time']));
   	$startDate = user_date($startDate,'Y-m-d H:i:s');
   	$endDate = date('Y-m-d H:i:s', strtotime($temp['end_date'].' '.$temp['end_time']));
   	$endDate = user_date($endDate,'Y-m-d H:i:s');
   	$temp['start_date'] = date('Y-m-d', strtotime($startDate));
   	$temp['start_time'] = date('H:i:s', strtotime($startDate));
   	$temp['end_date'] = date('Y-m-d', strtotime($endDate));
   	$temp['end_time'] = date('H:i:s', strtotime($endDate));
   	if(!empty($temp['check_in_time'])){
		$temp['check_in_time'] = user_date($temp['check_in_time'],'Y-m-d H:i:s');
	}

	if(!empty($temp['check_out_time'])){
		$temp['check_out_time'] = user_date($temp['check_out_time'],'Y-m-d H:i:s');
	}
	return $temp;
}
function boardingServerTimeConverter($temp){
	//this is for convert system time into UTC time 
	$startDate = date('Y-m-d H:i:s', strtotime($temp['start_date'].' '.$temp['start_time']));
	$startDate = server_date($startDate,'Y-m-d H:i:s');

	$endDate = date('Y-m-d H:i:s', strtotime($temp['end_date'].' '.$temp['end_time']));
	$endDate = server_date($endDate,'Y-m-d H:i:s');

	$temp['start_date'] = date('Y-m-d', strtotime($startDate));
	$temp['end_date'] = date('Y-m-d', strtotime($endDate));
	$temp['start_time'] = date('H:i:s', strtotime($startDate));
	$temp['end_time'] = date('H:i:s', strtotime($endDate));
	return $temp;
}
function user_date($date, $format = 'Y-m-d H:i:s', $serverTimeZone = 'UTC', $userTimeZone = 'UTC')
{
	if(empty($userTimeZone)){
		$userTimeZone = date_default_timezone_get();
	}
	
    try {
        $dateTime = new DateTime ($date, new DateTimeZone($serverTimeZone));
        $dateTime->setTimezone(new DateTimeZone($userTimeZone));
        return $dateTime->format($format);
    } catch (Exception $e) {
        return 'ttt';
    }
}

function server_date($date, $format = 'Y-m-d H:i:s',  $serverTimeZone = 'UTC', $userTimeZone ='UTC')
{
	if(empty($userTimeZone)){
		$userTimeZone = date_default_timezone_get();
	}
    try {
        $dateTime = new DateTime ($date, new DateTimeZone($userTimeZone));
        $dateTime->setTimezone(new DateTimeZone($serverTimeZone));
        return $dateTime->format($format);
    } catch (Exception $e) {
        return 'ttt';
    }
}





