<?php

/**
 * Get option value
 * @param  string $name Option name
 * @return mixed
 */
function get_option($name)
{
    $CI =& get_instance();
    
    //fetching config details from database according to panel
    if($CI->router->class == 'Superadmin')
    {
        //if it is super admin panel
        $CI->db2 = $CI->load->database('main', TRUE);
        $result = $CI->db2->get_where('options', array(
            'name' => $name
        ))->row();
    }
    else
    { 
        //other than superadmin panel
        $CI->load->database();
        $result = $CI->db->get_where('options', array(
            'name' => $name
        ))->row();
        
    }
    if(empty($result)){
       $result = (object)array(
         'value' => ''
       );
    }


    return $result->value;
}

function get_orderID($id)
{
   return $order_id = INVOICE_PRE + $id;
}

function get_encode($id)
{
    $CI =& get_instance();
    return str_replace(array('+', '/', '='), array('-', '_', '~'), $CI->encrypt->encode($id));
}

function get_decode($id)
{
    $CI =& get_instance();
    return $CI->encrypt->decode(str_replace(array('-', '_', '~','%7E'), array('+', '/', '=','='), $id));
}